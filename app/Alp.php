<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class Alp extends Model
{
    protected $table = 'alp';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'alp_request',
        'alp_response',
        'alp_result',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
