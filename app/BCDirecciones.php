<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCDirecciones extends Model
{
    protected $table = 'bc_direcciones';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'dom_calle',
        'dom_calle_segunda_linea',
        'dom_ciudad',
        'dom_colonia',
        'dom_cp',
        'dom_deleg',
        'dom_estado',
        'dom_fax',
        'dom_fecha',
        'dom_fecha_reporte',
        'dom_indicador_espec',
        'dom_origen',
        'dom_tel_casa',
        'dom_tel_casa_ext',
        'dom_tipo',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
