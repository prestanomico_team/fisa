<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCEmpleos extends Model
{
    protected $table = 'bc_empleos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'emp_razon_social',
        'emp_dir1',
        'emp_dir2',
        'emp_colonia',
        'emp_deleg',
        'emp_ciudad',
        'emp_estado',
        'emp_cp',
        'emp_num_tel',
        'emp_tel_ext',
        'emp_fax',
        'emp_cargo',
        'emp_fecha_contrat',
        'emp_clave_moneda',
        'emp_monto_sueldo',
        'emp_periodo_pago',
        'emp_num_empleado',
        'emp_fecha_ult_dia',
        'emp_fecha_reporte',
        'emp_fecha_verificacion',
        'emp_modo_verificacion',
        'emp_origen_razon_social',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
