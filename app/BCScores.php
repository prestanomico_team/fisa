<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCScores extends Model
{
    protected $table = 'bc_scores';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'bc_score',
        'exclusion',
        'bc_razon1',
        'bc_razon2',
        'bc_razon3',
        'bc_error',
        'icc_score',
        'icc_exclusion',
        'icc_razon1',
        'icc_razon2',
        'icc_razon3',
        'icc_error',
        'micro_valor',
        'micro_razon1',
        'micro_razon2',
        'micro_razon3',
        'score_no_hit',
        'score_no_hit_razon1',
        'exclusion_no_hit',
        'score_no_hit_error',
        'estimador_ingresos_valor',
        'estimador_ingresos_razon1',
        'estimador_ingresos_razon2',
        'estimador_ingresos_razon3',
        'exclusion_estimador_ingresos',
        'estimador_ingresos_error'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
