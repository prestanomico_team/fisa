<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class C_BANK extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'C_BANK';
    public $timestamps = false;
    protected $primaryKey = 'ID_BANK';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_BANK',
        'DESCRIPTION',
        'PRIORITY',
        'CREATE_DATE',
        'UPDATE_DATE',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
