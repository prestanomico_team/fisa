<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class C_CUSTOMER_RELATION_TYPE extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'C_CUSTOMER_RELATION_TYPE';
    public $timestamps = false;
    protected $primaryKey = 'ID_CUSTOMER_RELATION_TYPE';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_CUSTOMER_RELATION_TYPE',
        'DESCRIPTION',
        'FAMILY_MEMBER',
        'CREATE_DATE',
        'UPDATE_DATE',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
