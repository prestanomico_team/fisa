<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo que se conecta con la tabla carga_documentos
 *
 * Guarda el listado de documentos que tiene que cargar cada solicitud.
 */
class CargaDocumentos extends Model
{

    protected $table = 'carga_documentos';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'aplica_facematch',
        'id_front',
        'id_back',
        'selfie',
        'facematch_completo',
        'aplica_referencias',
        'referencias_completo',
        'aplica_cuenta_clabe',
        'cuenta_clabe_completo',
        'aplica_comprobante_domicilio',
        'device',
        'browser',
        'platform',
        'latitud',
        'longitud',
        'location_error',
        'latitud_reverse',
        'longitud_reverse',
        'reverse_address',
        'reverse_error',
        'distancia',
        'comprobante_domicilio_completo',
        'aplica_comprobante_ingresos',
        'aplica_certificados_deuda',
        'frecuencia',
        'tipo_comprobante',
        'detalle_documento',
        'numero_comprobantes',
        'numero_certificados_deuda',
        'comprobante_ingresos_completo',
        'certificados_deuda_completo',
        'certificados',
        'solicitud',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
