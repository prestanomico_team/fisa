<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogoExperian extends Model
{
    protected $table = 'catalogos_experian';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'campo',
        'valor_prestanomico',
        'valor_experian'
    ];
}
