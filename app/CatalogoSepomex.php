<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogoSepomex extends Model
{
    protected $table = 'catalogo_sepomex';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [

    ];
}
