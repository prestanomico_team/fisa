<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla catalogos_t24
 */
class CatalogosT24 extends Model
{
    protected $table = 'catalogos_t24';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [

    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
