<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class ClienteEstrella extends Model
{
    protected $table = 'cliente_estrella';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'id_prospect',
        'fecha_campana',
        'vigencia',
        'numcreditoactual',
        'nombre',
        'segundonombre',
        'apellidopaterno',
        'apellidomaterno',
        'fechadenacimiento',
        'email',
        'rfc',
        'telefono',
        'genero',
        'estadocivil',
        'estadodenacimiento',
        'gradodeestudios',
        'tipodevivienda',
        'teimpoderecidencia',
        'telefonodecasa',
        'num_dependientes',
        'gastos_familiares',
        'empresa',
        'puestoqueocupa',
        'ingresoreal',
        'tiempolaborandoenlaempresa',
        'telefonodecompania',
        'calle',
        'numerointdedomicilio',
        'numerodedomicilio',
        'colonia',
        'delegacion',
        'ciudad',
        'estado',
        'codigopostal',
        'tienecreditohipotecario',
        'tienecreditoautomotriz',
        'tienetarjeta',
        'ultimoscuatrodigitosdelatarjeta',
        'encontrado_bc',
        'monto',
        'tasa',
        'plazo',
        'pago',
        'finalidad',
        'procesado',
        'uuid',
        'email_enviado',
        'id_email'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
