<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Modelo que se conecta con la tabla catalogos_t24
 */
class ClienteT24 extends Model
{
    use Notifiable;

    protected $table = 'clientes_t24';
    protected $connection = 'mysql_aws_prestanomico';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'id_cliente_t24',
        'prefijo',
        'apellido_paterno',
        'apellido_materno',
        'nombre',
        'segundo_nombre',
        'email',
        'rfc',
        'rfc_homoclave',
        'curp',
        'sexo',
        'fecha_nacimiento',
        'lugar_nacimiento',
        'id_panel'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

    public function routeNotificationForSlack()
    {

    }

}
