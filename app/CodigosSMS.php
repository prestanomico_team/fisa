<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigosSMS extends Model
{
    // Nombre de la tabla
    protected $table = 'codigos_enviados';

    // Atributos que son asignables
    protected $fillable = [
        'prospecto_id',
        'nombre',
        'email',
        'celular',
        'codigo',
        'enviado',
        'descripcion',
        'verificado',
    ];
}
