<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultaBcTest extends Model
{
    protected $table = 'consulta_bc_test';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [

    ];
}
