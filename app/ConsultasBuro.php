<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultasBuro extends Model
{
    protected $table = 'consultas_buro';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'orden',
        'tipo',
        'cadena_original',
        'folio_consulta',
        'fecha_consulta',
        'no_consulta',
        'INTL',
        'PN',
        'PA',
        'PE',
        'TL',
        'IQ',
        'RS',
        'HI',
        'HR',
        'CR',
        'SC',
        'ERRR',
        'ES',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Obtiene el prospecto relacionado a la consulta a buro
     *
     * @return object Datos del prospecto
     */
    public function prospecto() {
        return $this->hasOne('App\Prospecto', 'id', 'prospecto_id')
            ->select('id', 'nombres', 'apellido_paterno', 'apellido_materno');
    }

    /**
     * Obtiene la solicitud relacionada a la consulta a buró
     *
     * @return object Datos de la solicitud
     */
    public function solicitud() {
        return $this->hasOne('App\Solicitud', 'id', 'solicitud_id')
            ->select('id', 'rfc', 'user_ip');
    }

    /**
     * Obtiene el domicilio de la solicitud relacionada a la consulta a buró
     *
     * @return object Datos de la solicitud
     */
    public function domicilio_solicitud() {
        return $this->hasOne('App\DomicilioSolicitud', 'solicitud_id', 'solicitud_id')
            ->selectRaw("solicitud_id, CONCAT(calle, ' ', num_exterior, ' INT. ', num_interior) AS 'calle-no', colonia, ciudad, codigo_estado AS 'edo.'");
    }

    /**
     * Obtiene los datos de autenticación a buró
     *
     * @return object Datos autenticación
     */
    public function autenticacion_solicitud() {
        return $this->hasOne('App\Solicitud', 'id', 'solicitud_id')
            ->selectRaw("id, IF(autorizado = 1, 'S', 'N') AS 'autoriza',
                IF(encontrado = 1, 'S', 'N') AS 'id. autentica',
                IF(credito_hipotecario = 1, 'V', 'F') AS 'hipotecario',
                IF(credito_automotriz = 1, 'V', 'F') AS 'automotriz',
                IF(credito_bancario = 1, 'V', 'F') AS 'tarjeta_credito'");
    }

    /**
     * Obtiene los dígitos de la TDC de autenticación a buró
     *
     * @return object Dígitos de la TDC
     */
    public function digitos_tdc() {
        return $this->hasOne('App\Solicitud', 'id', 'solicitud_id')
            ->select('id', 'ultimos_4_digitos', 'rfc');
    }
}
