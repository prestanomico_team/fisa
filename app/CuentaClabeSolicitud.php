<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaClabeSolicitud extends Model
{
    protected $table = 'cuenta_clabe_solicitud';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'solicitud_id',
        'banco_id',
        'banco',
        'clabe_interbancaria',
        'numero_cuenta',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
