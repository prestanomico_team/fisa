<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosAdicionales extends Model
{
    protected $table = 'datos_adicionales';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'nombre_empresa'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
