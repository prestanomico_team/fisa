<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosConvenio extends Model
{
    
    // Nombre de la tabla
    protected $table = 'datos_convenio';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'empresa',
        'sucursal',
        'embajador',
        'telefono',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
