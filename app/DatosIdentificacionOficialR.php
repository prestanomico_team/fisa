<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla datos_
 */
class DatosIdentificacionOficialR extends Model
{
    protected $table = 'datos_id_oficial_r';

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $guarded = [];


    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
