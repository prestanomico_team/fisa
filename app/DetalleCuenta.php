<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCuenta extends Model
{
    protected $table = 'detalle_cuentas_mr';
    protected $connection = 'mysql_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'ID_PROSPECT',
        'ID_SOLIC',
        'NO_CUENTA',
        'CUENTA_FECHA_ACTUALIZACION',
        'CUENTA_IMPUGNADO',
        'CUENTA_MEMBER_CODE',
        'CUENTA_NOMBRE_USUARIO',
        'CUENTA_NUM_TEL',
        'CUENTA_NUM_CUENTA',
        'CUENTA_RESPONSABILIDAD',
        'CUENTA_TIPO',
        'CUENTA_CONTRATO_PRODUCTO',
        'CUENTA_MONEDA',
        'CUENTA_IMPORTE_EVALUO',
        'CUENTA_NUM_PAGOS',
        'CUENTA_FRECUENCIA_PAGOS',
        'CUENTA_MONTO_PAGAR',
        'CUENTA_FECHA_APERTURA',
        'CUENTA_FECHA_ULT_PAGO',
        'CUENTA_FECHA_ULT_COMPRA',
        'CUENTA_FECHA_CIERRE',
        'CUENTA_FECHA_REPORTE',
        'CUENTA_MODO_REPORTE',
        'CUENTA_ULT_FECHA_CERO',
        'CUENTA_GARANTIA',
        'CUENTA_CRED_MAX_AUT',
        'CUENTA_SALDO_ACTUAL',
        'CUENTA_LIMITE_CREDITO',
        'CUENTA_SALDO_VENCIDO',
        'CUENTA_NUM_PAGOS_VENCIDOS',
        'CUENTA_MOP',
        'CUENTA_HIST_PAGOS',
        'CUENTA_HIST_PAGOS_FECHA_RECIENTE',
        'CUENTA_HIST_PAGOS_FECHA_ANTIGUA',
        'CUENTA_CLAVE_OBSERVACION',
        'CUENTA_TOTAL_PAGOS',
        'CUENTA_TOTAL_PAGOS_MOP2',
        'CUENTA_TOTAL_PAGOS_MOP3',
        'CUENTA_TOTAL_PAGOS_MOP4',
        'CUENTA_TOTAL_PAGOS_MOP5_PLUS',
        'CUENTA_SALDO_MOROSIDAD_MAS_ALTA',
        'CUENTA_FECHA_MOROSIDAD_MAS_ALTA',
        'CUENTA_CLASIF_PUNTUALIDAD_DE_PAGO',
        'CUENTA_FECHA_INICIO_REESTRUCTURA',
        'CUENTA_MONTO_ULTIMO_PAGO'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
