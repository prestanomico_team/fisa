<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleSolicitud extends Model
{
    protected $table = 'detalle_solicitudes_mr';
    protected $connection = 'mysql_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'ID_PROSPECT',
        'ID_SOLIC',
        'FECHA_REGISTRO',
        'STATUS',
        'ULT_ACT',
        'PRESTAMO',
        'PLAZO',
        'FINALIDAD',
        'NOS_GUSTARIA',
        'NOMBRE',
        'APELLIDO_P',
        'APELLIDO_M',
        'GENERO',
        'FEC_NACIMIENTO',
        'LUGAR_NACIMIENTO',
        'EDAD',
        'EMAIL',
        'CELULAR',
        'TEL_DOMICILIO',
        'TEL_OFICINA',
        'RFC',
        'CURP',
        'ESTADO_CIVIL',
        'RESIDENCIA',
        'NUM_DEPENDIENTES',
        'NIVEL_ESTUDIOS',
        'OCUPACION',
        'INGRESO',
        'GASTOS_FAMILIARES',
        'CRED_HIPOTECARIO',
        'CRED_AUTOMOTRIZ',
        'CRED_TDCBANCARIO',
        'ULT4_TDC',
        'ORIGEN',
        'DOM_CALLE',
        'NUM_EXT',
        'NUM_INT',
        'DOM_COLONIA',
        'DEL_MUNIC',
        'DOM_CIUDAD',
        'DOM_ESTADO',
        'COD_POSTAL',
        'BCSCORE',
        'NUM_CTAS_HIPOTECA',
        'NUM_CTAS_AUTOMOTRIZ',
        'NUM_CTAS_CONSUMO',
        'INGRESO_DISPONIBLE',
        'ATP',
        'USO_LINEAS',
        'MOP_02',
        'MOP_03',
        'NUM_CONSULTASBC_3M',
        'MICROSCORE',
        'ICC',
        'IP_ADDRESS',
        'DOM_ANIOS',
        'ANTIG_EMPLEO',
        'PEOR_MOP',
        'FECHA_MASREC_PEOR_MOP',
        'NUM_M_DE_PEOR_MOP',
        'NUM_CONSULTAS_3M',
        'FECHA_CONSULTA_REC',
        'FECHA_REGISTRO_BURO',
        'FECHA_APERT_L_ANTIGUA',
        'CVE_ACT_REP_BURO',
        'CVE_FRAUDE_REP_BURO',
        'CVE_EXTRAVIO_REP_BURO',
        'ALP',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
