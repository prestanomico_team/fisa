<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperianJourney extends Model {

    // Nombre de la tabla
    protected $table = 'experian_journey';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_request',
        'prospecto_response',
        'instrumentar_bc_request',
        'instrumentar_bc_response',
        'evaluar_mko_request',
        'evaluar_mko_response',
        'evaluar_mko_status',
        'generar_oferta_request',
        'generar_oferta_response',
        'generar_oferta_status',
        'consultar_oferta_request',
        'consultar_oferta_response',
        'consultar_oferta_status',
        'marcar_oferta_request',
        'marcar_oferta_response',
    ];

}
