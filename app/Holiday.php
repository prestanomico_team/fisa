<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Modelo que se conecta con la tabla catalogos_t24
 */
class Holiday extends Model
{
    use Notifiable;

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [

    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];


}
