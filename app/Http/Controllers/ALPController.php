<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Prospecto;
use App\Solicitud;
use App\FicoUtil;
use App\PlazaCobertura;
use App\SoapHttpClient;
use App\Sepomex;
use App\DomicilioSolicitud;
use App\ConsultaBcTest;
use App\DetalleCuenta;
use App\DetalleConsulta;
use App\DetalleSolicitud;
use App\PlantillaComunicacion;
use App\OfertaPredominante;
use App\Situacion;
use App\RespuestaMaquinaRiesgo;
use App\CatalogoSepomex;
use App\Plazo;
use App\Alp;
use App\TipoTasa;
use App\MotivoRechazo;
use Cookie;
use DOMDocument;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Repositories\SolicitudRepository;
use GoogleTagManager;

class ALPController extends Controller
{
    private $configuracionProducto;
    private $bcScore;
    private $edadMinima;
    private $edadMaxima;
    private $campoCobertura;
    private $alp;
    private $storedProcedure;
    private $proceso_simplificado;
    private $tipo_monto_simplificado;
    private $simplificado_monto_minimo;
    private $simplificado_monto_maximo;
    private $carga_identificacion_selfie_simplificado;
    private $facematch_simplificado;
    private $carga_identificacion_selfie;
    private $facematch;
    private $solicitudRepository;

    /**
     * Constructor de la clase
     */
    public function __construct(request $request)
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        if ($request->has('solicitud_id')) {
            $solicitud_id = $request->solicitud_id;
        }

        if ($solicitud_id != null) {

            $solicitud = Solicitud::with('producto')
                ->where('id', $solicitud_id)
                ->get()
                ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }
        $solicitud_id = $request->solicitud_id;
        if($solicitud_id != null) {
            $solicitud = Solicitud::with('producto')
                ->where('id', $solicitud_id)
                ->get()
                ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }
        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );

        $this->alp = $this->configuracionProducto['consulta_alp'];
        $this->storedProcedure = $this->configuracionProducto['stored_procedure'];
        $this->proceso_simplificado = $this->configuracionProducto['proceso_simplificado'];
        $this->tipo_monto_simplificado = $this->configuracionProducto['tipo_monto_simplificado'];
        $this->carga_identificacion_selfie_simplificado = $this->configuracionProducto['carga_identificacion_selfie_simplificado'];
        $this->facematch_simplificado = $this->configuracionProducto['facematch_simplificado'];
        $this->carga_identificacion_selfie = $this->configuracionProducto['carga_identificacion_selfie'];
        $this->facematch = $this->configuracionProducto['facematch'];
        if ($this->tipo_monto_simplificado == 'definido') {
            $this->simplificado_monto_maximo = $this->configuracionProducto['simplificado_monto_maximo'];
            $this->simplificado_monto_minimo = $this->configuracionProducto['simplificado_monto_minimo'];
        } else {
            $this->simplificado_monto_maximo = $this->configuracionProducto['monto_maximo'];
            $this->simplificado_monto_minimo = $this->configuracionProducto['monto_minimo'];
        }

        $this->solicitudRepository = new SolicitudRepository;
    }

    /**
     * Realizá la conexión con FICO y determina el status final de la solicitud
     * en base a los cálculos realizados en las llamadas a BC y el guardado de
     * datos adicionales, realiza el llamado a la máquina de riesgos la cual determina
     * el status final de la solicitud así como la tasa, monto, plazo y pago de la
     * oferta.
     *
     * Status finales:
     * Invitación a Continuar
     * Invitación a Continuar con cuestionario dinámico
     * Oferta diferida
     * Rechazado
     *
     * @param  request $request Array con los datos capturados
     *
     * @return json             Respuesta con el status final de la solicitud
     */
    public function solicitudFicoAlp(request $request) {

        if ($request->has("cliente_estrella")) {
            if ($request->input("cliente_estrella") == true) {
                $this->storedProcedure = env('SP_CLIENTEESTRELLA');
            }
        }

        $prospect = Prospecto::find($request->input("prospecto_id"));
        $solic = Solicitud::with('producto', 'bc_score')->find($request->input("solicitud_id"));
        $id_prospecto = $solic->prospecto_id;
        $id_solicitud = $solic->id;
        $eventTM = [];

        $detalleSolicitud = $this->saveDetalleSolicitud($solic, $prospect, $id_prospecto, $id_solicitud);
        $alp = Alp::updateOrCreate([
            'prospecto_id' => $id_prospecto,
            'solicitud_id' => $id_solicitud
        ]);

        if ($this->alp == true && ($solic->bc_score !== -9 && $solic->bc_score !== -8)) {

            //update the solics ult_punto_reg
            $this->solicitudRepository->setUltPuntoReg(
                $solic->id,
                $solic->status,
                'FICO ALP',
                1,
                'Ejecutanto stored procedure para FICO ALP'
            );

            $is_debug = App::environment(['local', 'desarrollo', 'desarrollos']) ? true : false; // Uses false for PROD environment
            $xml = $this->getALPData($id_prospecto, $id_solicitud);
            $alp->alp_request = $xml;
            $alp->save();

            //update the solics ult_punto_reg
            $this->solicitudRepository->setUltPuntoReg(
                $solic->id,
                $solic->status,
                'FICO ALP',
                1,
                'Iniciando la consulta FICO ALP'
            );

            $fico = new FicoUtil($is_debug);
            $ALPres = $fico->request($xml);

            if ($ALPres->code == '00') {
                //FICO ALP was obtained without error
                $xml_data = simplexml_load_string($ALPres->result) or die("Error: Cannot create object");
                // save the result to calculos
                $alp->alp_response = $xml_data->asXML();
                $alp->save();
                //create the string val for alp_score
                $alp_decision = $xml_data->attributes()->DECISION;
                $alp_score = $xml_data->attributes()->SCORE;
                //set the ALP score value to the solic
                $alp_result = $alp_decision.'_'.$alp_score;
                $alp->alp_result = $alp_result;
                $alp->save();
                //update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solic->id,
                    $solic->status,
                    'FICO ALP',
                    1,
                    'Se recibió score ALP de FICO',
                    ['DECISION' => $alp_decision, 'ALP' => $alp_score]
                );
                $updateSolicitud = $this->updateDetalleSolicitud($id_prospecto, $id_solicitud, $alp_decision.'_'.$alp_score);

            } else {
                //there was an error obtaining ALP from Fico SOAP service
                $desc = $ALPres->code." - ".$ALPres->result;
                $mnsj_str = [
                  'stat'    => "Error ALP",
                  'message' => $desc
                ];

                //update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solic->id,
                    $solic->status,
                    'FICO ALP',
                    0,
                    'Se recibió un error de FICO',
                    ['Error' => $desc]
                );
                // save the message to the ult_mensj_a_usuario field
                $this->solicitudRepository->ult_mensj_a_usuario = self::setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                // ****** temporary - set score to 0 until this error is resolved with fico
                // save the result to calculos
                $alp->alp_request = 'Error';
                $alp->alp_result = 'ERROR_0';
                $alp->save();

                return response()->json($mnsj_str);
            }

        } else {

            //update the solics ult_punto_reg
            $this->solicitudRepository->setUltPuntoReg(
                $solic->id,
                $solic->status,
                'FICO ALP',
                1,
                'No aplica consulta FICO ALP'
            );

            $alp->alp_request = 'No Aplica';
            $alp->alp_response = 'No Aplica';
            $alp->alp_result = '0';
            $alp->save();

        }

        // Realizando el llamado al store procedure para que nos regrese el status
        // final de la solicitud, si aplica el cuestionario de datos adicionales
        // y la plantilla de comunicación que se le enviará al prospecto
        //
        // Id status    Mensaje
        // 1            Rechazado
        // 2            Oferta diferida
        // 3            Invitación a continuar

        //update the solics ult_punto_reg
        $this->solicitudRepository->setUltPuntoReg(
            $solic->id,
            $solic->status,
            'Stored Procedure',
            1,
            'Ejecutando el stored procedure'
        );

        try {
            $resultado = DB::connection('mysql_maquina_riesgos')
                ->select("CALL {$this->storedProcedure}({$id_prospecto}, {$id_solicitud})");
        } catch (\Exception $e) {

            //update the solics ult_punto_reg
            $this->solicitudRepository->setUltPuntoReg(
                $solic->id,
                $solic->status,
                'Stored Procedure',
                1,
                'Error al ejecutar el stored procedure: '. $e->getMessage()
            );

            // Regresa un error si la ejecución del stored procedure falla
            $mnsj_str = [
                'success' => false,
                'error_message' => $e->getMessage()
            ];

            $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                'prospecto_id'  => $id_prospecto,
                'solicitud_id'  => $id_solicitud,
                'tipo_oferta'   => 'oferta_normal',
            ], [
                'ejecucion_sp'        => 0,
                'status_ejecucion_sp' => $e->getMessage()
            ]);

            return response()->json($mnsj_str);
        }

        // Si el store procedure arroja un resultado se mostrara el modal segun
        // el cálculo
        if (count($resultado) > 0) {

            $eventTM[] = ['event'=> 'EvaluadoMR', 'userId' => $prospect->id, 'solicId' => $solic->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
            if (!isset($resultado[0]->TipoError)) {

                //update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solic->id,
                    $solic->status,
                    'Stored Procedure',
                    1,
                    'El stored procedure se ejecuto con éxito. Obteniendo plantilla de comunicación'
                );

                $modal = 'modalOferta';
                $substatus = null;

                $tipoTasa = TipoTasa::where('id_prospect', $id_prospecto)
                    ->where('id_solic', $id_solicitud)
                    ->first();

                if (isset($tipoTasa->Grupo_Tasa)) {
                    $tipoTasa = $tipoTasa->Grupo_Tasa;
                } else {
                    $tipoTasa = 'Tasa_Champion';
                }

                foreach ($resultado as $key => $oferta) {
                    $decisiones = [
                        '1' => 'Rechazado',
                        '2' => 'Oferta Diferida',
                        '3' => 'Invitación a Continuar'
                    ];

                    // Experimento 1:
                    // A los prospectos a los que la oferta sea entre $4,000 y $7,000 pesos
                    // la oferta no seá minima y el porcentaje que se calcule sea menor
                    // o igual a 50% se pasaran por el programa "Proceso Simplificado de
                    // Invitación a Continuar" se les debe de mandar un modal diferente
                    // en el que solo se pida IFE y descargar el APP

                    if ($this->proceso_simplificado == 1
                        && (($oferta->Monto_Mod >= $this->simplificado_monto_minimo && $oferta->Monto_Mod <= $this->simplificado_monto_maximo)
                        && $oferta->OfertaMinima == 0 && $oferta->Decision == 3)) {

                        $simplificado = 1;
                        $subir_documentos = $this->carga_identificacion_selfie_simplificado;
                        $carga_identificacion_selfie = $this->carga_identificacion_selfie_simplificado;
                        $facematch = $this->facematch_simplificado;
                        $plantillaOriginal = $oferta->Plantilla_Comunicacion;

                        // Plantilla de comunicación para el proceso Simplificado
                        $id_plantilla = 99;

                        $mnsj_str['simplificado'] = $simplificado;
                        $mnsj_str['pasa_simplificado'] = 1;
                        $mnsj_str['facematch'] = $facematch;
                        $mnsj_str['oferta_minima'] = $oferta->OfertaMinima;
                        $mnsj_str['carga_identificacion_selfie'] = $carga_identificacion_selfie;

                    } elseif ($this->carga_identificacion_selfie == 1 && $oferta->Decision == 3) {

                        $simplificado = 0;
                        $subir_documentos = $this->carga_identificacion_selfie;
                        $carga_identificacion_selfie = $this->carga_identificacion_selfie;
                        $facematch = $this->facematch;
                        // Plantilla de comunicación para la carga de documentos
                        $id_plantilla = 89;

                        $mnsj_str['simplificado'] = $simplificado;
                        $mnsj_str['facematch'] = $facematch;
                        $mnsj_str['pasa_simplificado'] = 0;
                        $mnsj_str['oferta_minima'] = $oferta->OfertaMinima;
                        $mnsj_str['carga_identificacion_selfie'] = $carga_identificacion_selfie;


                    } else {

                        $simplificado = 0;
                        $subir_documentos = 0;
                        $carga_identificacion_selfie = 0;
                        $facematch = 0;
                        $id_plantilla = $oferta->Plantilla_Comunicacion;
                        $mnsj_str['simplificado'] = $simplificado;
                        $mnsj_str['facematch'] = $facematch;
                        $mnsj_str['pasa_simplificado'] = 0;
                        $mnsj_str['oferta_minima'] = $oferta->OfertaMinima;
                        $mnsj_str['carga_identificacion_selfie'] = $carga_identificacion_selfie;

                    }

                    // Obteniendo la plantilla de comunicación
                    $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                                ->where('plantilla_id', $id_plantilla)
                                ->get()
                                ->toArray();

                    $monto = $this->montoOferta($oferta->Monto_Mod);
                    $plazo = $oferta->Plazo_Mod;
                    $tasa = $this->tasaOferta($oferta->Tasa_Mod);
                    $pago = $this->pagoOferta($oferta->Pago_Mod, $oferta->Plazo_Mod, $oferta->Monto_Mod);

                    $oferta->Monto_Mod = $monto;
                    $oferta->Pago_Mod = $pago;
                    $oferta->Tasa_Mod = $tasa;

                    // Identificando el tipo de Oferta y el tipo de Población
                    if (isset($oferta->Tipo_Oferta) && $oferta->Decision == 3) {
                        $tipoOferta = $oferta->Tipo_Oferta;
                    } elseif ($oferta->Decision != 3) {
                        $tipoOferta = null;
                    } else {
                        $tipoOferta = 'Oferta Normal';
                    }

                    if (isset($oferta->Tipo_Poblacion) && $oferta->Decision == 3) {
                        $tipoPoblacion = $oferta->Tipo_Poblacion;
                    } elseif ($oferta->Decision != 3) {
                        $tipoPoblacion = null;
                    } else {
                        $tipoPoblacion = 'Oferta Normal';
                    }

                    $tipoOferta = str_slug($tipoOferta, '_');
                    $tipoPoblacion = str_slug($tipoPoblacion, '_');
                    $mnsj_str['tipo_oferta'] = $tipoOferta;
                    $mnsj_str['tipo_poblacion'] = $tipoPoblacion;

                    $mnsj_str['success'] = true;
                    $mnsj_str['plantilla'] = $plantilla[0];
                    $mnsj_str['resultado'][$tipoOferta] = $oferta;
                    $mnsj_str['prospecto_id'] = $id_prospecto;
                    $mnsj_str['solicitud_id'] = $id_solicitud;
                    $mnsj_str['decision'] = $oferta->Decision;
                    $decision = $decisiones[$oferta->Decision];
                    $decision_ultimo_punto = $decision;
                    $pantallas = null;

                    if ($oferta->PantallaExtra == 1) {

                        $pantallas = $oferta->CodigoPantallasExtras;
                        $pantallas = explode(',', $pantallas);
                        $pantallas = array_filter($pantallas, 'strlen');

                        $cuestionario = Situacion::whereIN('situacion', $pantallas)
                            ->with('cuestionario')
                            ->get()
                            ->toArray();

                        $mnsj_str['cuestionario'] = $cuestionario;
                        $pantallas = implode(',', $pantallas);
                        $decision_ultimo_punto = 'Cuestionario Incompleto';

                        $datosOferta[$key] = [
                            'monto'         => $monto,
                            'plazo'         => $plazo,
                            'tasa'          => $tasa,
                            'pago_estimado' => $pago,
                            'tipo_oferta'   => $tipoOferta,
                            'modelo'        => 'Prestanómico'
                        ];

                        if ($tipoPoblacion == 'oferta_doble') {
                            $modal = 'modalDobleOferta';
                        }

                    } else {

                        $datosOferta[$key] = [
                            'monto'         => $monto,
                            'plazo'         => $plazo,
                            'tasa'          => $tasa,
                            'pago_estimado' => $pago,
                            'tipo_oferta'   => $tipoOferta,
                            'modelo'        => 'Prestanómico'
                        ];

                        if ($tipoPoblacion == 'oferta_doble') {
                            $modal = 'modalDobleOferta';
                        }

                    }

                    // Actualizando el campo ult_punto_reg de la solicitud
                    $this->solicitudRepository->setUltPuntoReg(
                        $solic->id,
                        $decision,
                        $decision_ultimo_punto,
                        1,
                        'El stored procedure se ejecuto con éxito. Guardando el resultado en la tabla respuestas_maquinas_riesgos',
                        ['tipo_oferta' => $tipoOferta]
                    );

                    $datos = [
                        'ejecucion_sp'                  => 1,
                        'status_ejecucion_sp'           => 'Resultado Máquina de Riesgos',
                        'decision'                      => $decision,
                        'stored_procedure'              => $oferta->Clave_producto,
                        'plantilla_comunicacion'        => $id_plantilla,
                        'pantallas_extra'               => $oferta->PantallaExtra,
                        'situaciones'                   => $pantallas,
                        'monto'                         => $monto,
                        'plazo'                         => $plazo,
                        'pago'                          => $pago,
                        'tasa'                          => $tasa,
                        'tipo_tasa'                     => $tipoTasa,
                        'simplificado'                  => $simplificado,
                        'facematch'                     => $facematch,
                        'carga_identificacion_selfie'   => $carga_identificacion_selfie,
                        'subir_documentos'              => $subir_documentos,
                        'oferta_minima'                 => $oferta->OfertaMinima,
                        'tipo_poblacion'                => $tipoPoblacion,
                    ];

                    $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                        'prospecto_id'  => $id_prospecto,
                        'solicitud_id'  => $id_solicitud,
                        'tipo_oferta'   => $tipoOferta
                    ], $datos);

                    $datosOferta[$key]['oferta_id'] = $resultadoMR->id;

                    $decisionFinal = $decision;
                    if ($decision == 'Invitación a Continuar') {
                        $decisionFinal = 'Aprobado Prestanómico';
                    }

                    $datosOP = [
                        'ejecucion_modelo'              => 1,
                        'decision'                      => $decisionFinal,
                        'modeloEvaluacion'              => 'Prestanómico',
                        'ofertaEvaluacion'              => 'Prestanómico',
                        'plantilla_comunicacion'        => $id_plantilla,
                        'tipo_poblacion'                => $tipoPoblacion,
                        'monto'                         => $monto,
                        'plazo'                         => $plazo,
                        'pago'                          => $pago,
                        'tasa'                          => $tasa,
                        'tipo_tasa'                     => $tipoTasa,
                        'pantallas_extra'               => $oferta->PantallaExtra,
                        'situaciones'                   => $pantallas,
                        'simplificado'                  => $simplificado,
                        'facematch'                     => $facematch,
                        'carga_identificacion_selfie'   => $carga_identificacion_selfie,
                        'subir_documentos'              => $subir_documentos,
                        'oferta_minima'                 => $oferta->OfertaMinima,
                    ];

                    ofertaPredominante::updateOrCreate([
                        'prospecto_id'  => $id_prospecto,
                        'solicitud_id'  => $id_solicitud,
                        'tipo_oferta'   => $tipoOferta,
                        'oferta_id'     => $resultadoMR->id
                    ], $datosOP);

                }

                $solic->status = $decision;
                $solic->sub_status = $decision_ultimo_punto;
                $solic->save();

                if ($decision != 'Invitación a Continuar') {
                    Cookie::queue(Cookie::forget('producto'));
                    Cookie::queue(Cookie::forget('logo'));
                    Cookie::queue(Cookie::forget('checa_calificas'));
                }

                $motivos = [];

                switch ($decision) {
                    case 'Invitación a Continuar':
                        $eventTM[] = ['event'=> 'PreAprobado', 'userId'=> $prospect->id, 'solicId' => $solic->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                        if ($solic->producto[0]['no_aplica_oferta'] == false) {
                            $motivos = MotivoRechazo::pluck('motivo');
                            $modal = view("modals.{$modal}", ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render();
                        } else {
                            $modal = view("modals.modalNoAplicaOferta", ['datosOferta' => $datosOferta])->render();
                        }
                        $mnsj_str['modal'] = $modal;
                        $mnsj_str['eventTM'] = $eventTM;
                        break;
                    case 'Rechazado':
                        $eventTM[] = ['event'=> 'RechazadoMR', 'userId'=> $prospect->id, 'solicId' => $solic->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                        $tituloModal = $plantilla[0]['modal_encabezado'];
                        $imgModal = $plantilla[0]['modal_img'];
                        $cuerpoModal = $plantilla[0]['modal_cuerpo'];
                        $modal = view("modals.modalStatusSolicitud", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();
                        $mnsj_str['modal'] = $modal;
                        $mnsj_str['eventTM'] = $eventTM;
                        Auth::guard('prospecto')->logout();
                        break;
                    case 'Oferta Diferida':
                        $eventTM[] = ['event'=> 'RechazadoMR', 'userId'=> $prospect->id, 'solicId' => $solic->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                        $tituloModal = $plantilla[0]['modal_encabezado'];
                        $imgModal = $plantilla[0]['modal_img'];
                        $cuerpoModal = $plantilla[0]['modal_cuerpo'];
                        $modal = view("modals.modalStatusSolicitud", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();
                        $mnsj_str['modal'] = $modal;
                        $mnsj_str['eventTM'] = $eventTM;
                        Auth::guard('prospecto')->logout();
                        break;
                }

                return response()->json($mnsj_str);

            } else {

                // Regresa un error si la ejecución del stored procedure falla
                $mnsj_str = [
                    'success' => false,
                    'error_message' => 'Error al ejecutar máquina riesgos: '.$resultado[0]->DescripcionError
                ];

                return response()->json($mnsj_str);
            }

        } else {

            // Regresa un error si la ejecución del stored procedure no arroja
            // nigún resultado
            $mnsj_str = [
                'success' => false,
                'error_message' => 'La máquina de riesgos no arrojo ningun resultado'
            ];

            return response()->json($mnsj_str);

        }
    }

    /**
     * Determina el contenido del monto a mostrar en el modal oferta
     *
     * @param  string $monto Valor regresado por la máquina de riesgos
     * @return retun         Valor a mostrar en el modal oferta
     */
    public function montoOferta($monto) {

        switch ($monto) {
            case '-1':
                $monto = 'Por definir en tu oferta final';
                break;

            default:
                $monto = '$'.number_format($monto, 2, ".", ",");
            break;
        }

        return $monto;

    }

    /**
     * Determina el contenido de la tasa a mostrar en el modal oferta
     *
     * @param  string $tasa Valor regresado por la máquina de riesgos
     * @return retun        Valor a mostrar en el modal oferta
     */
    public function tasaOferta($tasa) {

        switch ($tasa) {
            case '-1':
                $tasa = '15 puntos menos que la tasa de la tarjeta que deseas pagar';
                break;

            default:
                $tasa = number_format($tasa, 2, ".", ",").'%';
            break;
        }

        return $tasa;

    }

    /**
     * Determina el contenido del pago a mostrar en el modal oferta
     *
     * @param  string $pago  Valor regresado por la máquina de riesgos
     * @param  string $plazo Valor regresado por la máquina de riesgos
     * @param  string $monto Valor regresado por la máquina de riesgos
     * @return retun         Valor a mostrar en el modal oferta
     */
    public function pagoOferta($pago, $plazo, $monto) {

        switch ($pago) {
            case '-1':
                $pago = 'Por definir en función de la tasa asignada a tu préstamo';
                break;
            default:
                $etiquetafin = '';
                $etiquetainicio = '';
                if (strrpos($plazo, "quincenas") !== false ) {
                    $etiquetafin = ' quincenal';
                } elseif (strrpos($plazo, "meses") !== false ) {
                    $etiquetafin = ' mensual';
                }
                if ($monto == '-1') {
                    $etiquetainicio = 'Tu pago quincenal por cada mil pesos de préstamo, sería aproximadamente de ';
                }
                $pago = $etiquetainicio.'$'.number_format($pago, 2, ".", ",").$etiquetafin;
            break;
        }

        return $pago;

    }

    public function updateDetalleSolicitud($id_prospecto, $id_solicitud, $alp) {

        $detalleSolicitd = DetalleSolicitud::updateOrCreate(
            [
                'ID_PROSPECT'       => $id_prospecto,
                'ID_SOLIC'          => $id_solicitud
            ], [
                'ALP'               => $alp
            ]
        )->save();

    }

    public function saveDetalleSolicitud($solic, $prospect, $id_prospecto, $id_solicitud) {

        // Obteniendo el origen de la Solicitud
        $origen = mb_strtoupper($prospect->referencia);
        if (isset($solic->producto)) {
            if (count($solic->producto) == 1) {
                if ($solic->producto[0]->pivot['lead'] != null) {
                    $origen = mb_strtoupper($solic->producto[0]->pivot['lead']);
                }
            }
        }

        $age = self::apiGetAge($solic->fecha_nacimiento);

        // Obteniendo los Scores de buró de crédito
        $bc_score = 0;
        $micro_score = 0;
        if (isset($solic->bc_score->bc_score)) {
            $bc_score = $solic->bc_score->bc_score;
            $micro_score = $solic->bc_score->micro_valor;
        }

        // Obteniendo el domicilio del prospecto
        $domicilio = DomicilioSolicitud::where('prospecto_id', $id_prospecto)
            ->where('solicitud_id', $id_solicitud)
            ->get();

        $calle = '';
        $num_exterior = '';
        $num_interior = '';
        $colonia = '';
        $delegacion = '';
        $ciudad = '';
        $estado = '';
        $cp = '';
        if (count($domicilio) == 1) {
            $calle = $domicilio[0]->calle;
            $num_exterior = $domicilio[0]->num_exterior;
            $num_interior = $domicilio[0]->num_interior;
            $colonia = $domicilio[0]->colonia;
            $delegacion = $domicilio[0]->delegacion;
            $ciudad = $domicilio[0]->ciudad;
            $estado = $domicilio[0]->codigo_estado;
            $cp = $domicilio[0]->cp;
        }

        $detalleSolicitd = DetalleSolicitud::updateOrCreate(
            [
                'ID_PROSPECT'           => $id_prospecto,
                'ID_SOLIC'              => $id_solicitud
            ], [
                'FECHA_REGISTRO'        => $solic->created_at,
                'STATUS'                => $solic->status,
                'ULT_ACT'               => $solic->updated_at,
                'PRESTAMO'              => $solic->prestamo,
                'PLAZO'                 => mb_strtoupper(self::getPlazoLabel($solic->plazo)),
                'FINALIDAD'             => mb_strtoupper($solic->finalidad),
                'NOS_GUSTARIA'          => mb_strtoupper($solic->finalidad_custom),
                'NOMBRE'                => mb_strtoupper($prospect->nombres),
                'APELLIDO_P'            => mb_strtoupper($prospect->apellido_paterno),
                'APELLIDO_M'            => mb_strtoupper($prospect->apellido_materno),
                'GENERO'                => mb_strtoupper($solic->sexo),
                'EMAIL'                 => mb_strtoupper($prospect->email),
                'CELULAR'               => $prospect->celular,
                'EDAD'                  => $age,
                'CRED_HIPOTECARIO'      => mb_strtoupper($solic->credito_hipotecario),
                'CRED_AUTOMOTRIZ'       => mb_strtoupper($solic->credito_automotriz),
                'CRED_TDCBANCARIO'      => mb_strtoupper($solic->credito_bancario),
                'ORIGEN'                => ($origen != '') ? $origen : 'SITIO',
                'RFC'                   => mb_strtoupper($solic->rfc),
                'ESTADO_CIVIL'          => mb_strtoupper(self::getEstadoCivilDesc($solic->estado_civil)),
                'RESIDENCIA'            => mb_strtoupper($solic->tipo_residencia),
                'NUM_DEPENDIENTES'      => $solic->numero_dependientes,
                'NIVEL_ESTUDIOS'        => mb_strtoupper($solic->nivel_estudios),
                'OCUPACION'             => mb_strtoupper($solic->ocupacion),
                'INGRESO'               => $solic->ingreso_mensual,
                'GASTOS_FAMILIARES'     => $solic->gastos_familiares,
                'FEC_NACIMIENTO'        => $solic->fecha_nacimiento,
                'BCSCORE'               => $bc_score,
                'NUM_CTAS_HIPOTECA'     => 0,
                'NUM_CTAS_AUTOMOTRIZ'   => 0,
                'NUM_CTAS_CONSUMO'      => 0,
                'INGRESO_DISPONIBLE'    => 0,
                'ATP'                   => 0,
                'USO_LINEAS'            => 0,
                'MOP_02'                => 0,
                'MOP_03'                => 0,
                'NUM_CONSULTASBC_3M'    => 0,
                'MICROSCORE'            => $micro_score,
                'ICC'                   => 0,
                'IP_ADDRESS'            => $solic->user_ip,
                'DOM_ANIOS'             => $solic->antiguedad_domicilio,
                'ANTIG_EMPLEO'          => $solic->antiguedad_empleo,
                'ULT4_TDC'              => $solic->ultimos_4_digitos,
                'PEOR_MOP'              => null,
                'FECHA_MASREC_PEOR_MOP' => null,
                'NUM_M_DE_PEOR_MOP'     => 0,
                'NUM_CONSULTAS_3M'      => 0,
                'FECHA_CONSULTA_REC'    => null,
                'LUGAR_NACIMIENTO'      => mb_strtoupper($solic->lugar_nacimiento_ciudad.', '.$solic->lugar_nacimiento_estado),
                'FECHA_REGISTRO_BURO'   => null,
                'FECHA_APERT_L_ANTIGUA' => null,
                'CVE_ACT_REP_BURO'      => '',
                'CVE_FRAUDE_REP_BURO'   => '',
                'CVE_EXTRAVIO_REP_BURO' => '',
                'TEL_DOMICILIO'         => $solic->telefono_casa,
                'TEL_OFICINA'           => $solic->telefono_empleo,
                'CURP'                  => mb_strtoupper($solic->curp),
                'DOM_CALLE'             => $calle,
                'NUM_EXT'               => $num_exterior,
                'NUM_INT'               => $num_interior,
                'DOM_COLONIA'           => $colonia,
                'DEL_MUNIC'             => $delegacion,
                'DOM_CIUDAD'            => $ciudad,
                'DOM_ESTADO'            => $estado,
                'COD_POSTAL'            => $cp,
                'ALP'                   => '',
                'APLICA_3MUDIS'         => 0
            ]
        )->save();

        return $detalleSolicitd;
    }

    public function getALPData($id_prospecto, $id_solicitud) {

        $resultado = DB::connection('mysql_maquina_riesgos')
            ->select("CALL SP_VarALP_V1({$id_prospecto}, {$id_solicitud})");

        $resultado = collect($resultado)->last();
        $resultado = collect($resultado);

        $applicationData = $resultado->only([
            'Lender',
            'CUSTOMER_NO',
            'APPLICATION_NO',
            'APPLICATION_DATE',
            'APPLICATION_TYPE',
        ]);

        $loanData = $resultado->except([
            'Lender',
            'APPLICATION_DATE',
            'APPLICATION_TYPE',
            'ID_PROSPECT',
            'ID_SOLIC',
            'F_EJECUCION',
        ]);

        $doc = new DOMDocument();
        $application = $doc->createElement('Application');
        $doc->appendChild($application);
        $application->setAttribute('Lender', 'prestanomico');
        foreach ($applicationData as $attribute => $value) {
            $application->setAttribute($attribute, $value);
        }

        $loan = $application->appendChild($doc->createElement('LoanApplication'));
        foreach ($loanData as $attribute => $value) {
            $loan->setAttribute($attribute, $value);
        }
        $xml = $doc->saveXML();
        return $xml;


    }

    /**
     * Obtiene la edad del prospecto en base a su fecha de nacimiento
     *
     * @param  date        Fecha de nacimiento
     *
     * @return integer     Edad del prospecto
     */
    private function apiGetAge($fecha_nacimiento)
    {
        $age = Carbon::createFromFormat('Y-m-d', $fecha_nacimiento)->age;
        return $age;
    }

}
