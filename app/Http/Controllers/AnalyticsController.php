<?php
/**
* AnalyticsController.php
*
* PHP version 7
*
* @category Controllers
* @package  App\Http\Controllers
* @author   Héctor Urbano Maza <hector.urbano@prestanomico.com>
* @license  https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio/licence.txt Licence
* @link     https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio
*/

namespace App\Http\Controllers;

use App\Analytic;
use Illuminate\Http\Request;
use App;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_SegmentDimensionFilter;
use Google_Service_AnalyticsReporting_SegmentFilterClause;
use Google_Service_AnalyticsReporting_OrFiltersForSegment;
use Google_Service_AnalyticsReporting_SimpleSegment;
use Google_Service_AnalyticsReporting_SegmentFilter;
use Google_Service_AnalyticsReporting_SegmentDefinition;
use Google_Service_AnalyticsReporting_DynamicSegment;
use Google_Service_AnalyticsReporting_Segment;

/**
* AnalyticsController Se encarga de obtener los datos de google analytics
*
* @category Controllers
* @package  App\Http\Controllers
* @author   Héctor Urbano Maza <hector.urbano@prestanomico.com>
* @license  https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio/licence.txt Licence
* @link     https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio
*/
class AnalyticsController extends Controller
{
    // Arreglo que tiene la relacion del nombre de la dimension y el nombre del campo
    // en la tabla analytics
    private $_campos = [
        'ga:dimension1'                 => 'client_id',
        'ga:dimension2'                 => 'prospecto_id',
        'ga:dimension3'                 => 'solicitud_id',
        'ga:dateHourMinute'             => 'fecha_hora',
        'ga:browser'                    => 'navegador',
        'ga:browserVersion'             => 'navegador_version',
        'ga:operatingSystem'            => 'sistema_operativo',
        'ga:operatingSystemVersion'     => 'sistema_version',
        'ga:deviceCategory'             => 'tipo_dispositivo',
        'ga:language'                   => 'idioma',
        'ga:country'                    => 'pais',
        'ga:city'                       => 'ciudad',
        'ga:region'                     => 'estado',
        'ga:sessionDurationBucket'      => 'duracion_sesion',
        'ga:networkLocation'            => 'proveedor_internet',
        'ga:campaign'                   => 'campaña',
        'ga:source'                     => 'origen',
        'ga:medium'                     => 'medio',
        'ga:mobileDeviceBranding'       => 'marca',
        'ga:mobileDeviceModel'          => 'modelo',
        'ga:mobileDeviceMarketingName'  => 'modelo_detalle',
        'ga:adContent'                  => 'contenido_anuncio',
    ];

    // Arreglo que contiene las dimensiones que contiene cada reporte
    private $_informes_generales = [
        0 => [
            'dimension1' => 'ga:dimension1',
            'dimension2' => 'ga:dimension2',
            'dimension3' => 'ga:dimension3',
            'dimension4' => 'ga:browser',
            'dimension5' => 'ga:browserVersion',
            'dimension6' => 'ga:operatingSystem',
            'dimension7' => 'ga:operatingSystemVersion'
        ],
        1 => [
            'dimension1' => 'ga:dimension1',
            'dimension2' => 'ga:dimension2',
            'dimension3' => 'ga:dimension3',
            'dimension4' => 'ga:deviceCategory',
            'dimension5' => 'ga:country',
            'dimension6' => 'ga:city',
            'dimension7' => 'ga:region'
        ],
        2 => [
            'dimension1' => 'ga:dimension1',
            'dimension2' => 'ga:dimension2',
            'dimension3' => 'ga:dimension3',
            'dimension4' => 'ga:campaign',
            'dimension5' => 'ga:source',
            'dimension6' => 'ga:medium',
            'dimension7' => 'ga:adContent'
        ],
        3 => [
            'dimension1' => 'ga:dimension1',
            'dimension2' => 'ga:dimension2',
            'dimension3' => 'ga:dimension3',
            'dimension4' => 'ga:language',
            'dimension5' => 'ga:sessionDurationBucket',
            'dimension6' => 'ga:networkLocation'
        ],
        4 => [
            'dimension1' => 'ga:dimension1',
            'dimension2' => 'ga:dimension2',
            'dimension3' => 'ga:dimension3',
            'dimension4' => 'ga:mobileDeviceBranding',
            'dimension5' => 'ga:mobileDeviceModel',
            'dimension6' => 'ga:mobileDeviceMarketingName',
        ]
    ];

    /**
    * Obtiene los datos de google analytics de los clientes que se han
    * registrado en el sitio y los almancena en la tabla analytics
    *
    * @return void
    */
    public function getDataAnalytics(Request $request)
    {
        try {
            $client = new Google_Client();
            $analytics = $this->conexionAnalytics($client);

            $accounts = $analytics->management_accounts
                ->listManagementAccounts();

            if (count($accounts->getItems()) > 0) {
                $items = $accounts->getItems();
                $firstAccountId = $items[0]->getId();

                // Obteniendo las propiedades del usuario autorizado.
                $properties = $analytics->management_webproperties
                    ->listManagementWebproperties($firstAccountId);

                if (count($properties->getItems()) > 0) {
                    $items = $properties->getItems();
                    $firstPropertyId = $items[0]->getId();

                    // Obteniendo la lista de vistas del usuario autorizado.
                    $profiles = $analytics->management_profiles
                        ->listManagementProfiles($firstAccountId, $firstPropertyId);

                    if (count($profiles->getItems()) > 0) {

                        // Obteniendo los reportes generales
                        $reports = $this->creaReportes($client, $this->_informes_generales);
                        $status_reporte = [];
                        $clientes_actualizados = [];
                        $no_reporte = 1;

                        foreach ($reports as $report) {
                            $rows = $report['rows'];

                            $nr = $no_reporte - 1;

                            $prospect_datos = Analytic::whereRaw(
                                '(datos_obtenidos = "Sin datos"
                                OR datos_obtenidos LIKE "Reporte_%")
                                AND (created_at >= (NOW() - INTERVAL 30 HOUR))'
                            )->get();

                            foreach ($prospect_datos as $prospect) {
                                $client_id = $prospect->client_id;
                                $prospect_id = $prospect->prospecto_id;
                                $solicitud_id = $prospect->solicitud_id;
                                $tipo_dispositivo = $prospect->tipo_dispositivo;
                                $id = $prospect->id;

                                $prospecto_analytics = array_where(
                                    $rows,
                                    function ($row) use ($client_id, $prospect_id, $solicitud_id
                                ) {
                                        return ($row->dimensions[0] == $client_id
                                            && $row->dimensions[1] == $prospect_id
                                            && $row->dimensions[2] == $solicitud_id
                                        );
                                    }
                                );

                                $key = array_keys($prospecto_analytics);
                                if (count($prospecto_analytics) > 0) {
                                    $dimensions = $rows[$key[0]]->getDimensions();
                                    $actualizados = [];
                                    $tipo_dispositivo = '';

                                    foreach ($dimensions as $key => $dimension) {

                                        $nombre_dimension = $report['dimensionHeaders'][$key];
                                        $campo = $this->_campos[$nombre_dimension];

                                        $actualizados = array_merge(
                                            $actualizados,
                                            [$campo => $dimension]
                                        );
                                    }

                                    $actualizados = array_merge(
                                        $actualizados,
                                        ['datos_obtenidos' => 'Reporte_'.$no_reporte]
                                    );

                                    if (($prospect->datos_obtenidos == 'Sin datos' && $no_reporte == 1)
                                        || ($prospect->datos_obtenidos == 'Reporte_'.$nr)) {
                                        Analytic::where('prospecto_id', $prospect_id)
                                                ->where('client_id', $client_id)
                                                ->where('id', $id)
                                                ->update($actualizados);

                                        $clientes_actualizados[$prospect_id][$client_id][$no_reporte] = 'Reporte Actualizado';
                                    }

                                    if (($prospect->tipo_dispositivo == 'mobile'
                                        || $prospect->tipo_dispositivo == 'tablet')
                                        && $prospect->datos_obtenidos == 'Reporte_5') {
                                            Analytic::where('prospecto_id', $prospect_id)
                                                    ->where('solicitud_id', $solicitud_id)
                                                    ->where('client_id', $client_id)
                                                    ->where('id', $id)
                                                    ->update([
                                                        'datos_obtenidos' => 'Datos completos']);
                                    } elseif ($prospect->tipo_dispositivo == 'desktop'
                                        && $prospect->datos_obtenidos == 'Reporte_4') {
                                            Analytic::where('prospecto_id', $prospect_id)
                                                    ->where('solicitud_id', $solicitud_id)
                                                    ->where('client_id', $client_id)
                                                    ->where('id', $id)
                                                    ->update([
                                                        'datos_obtenidos' => 'Datos completos']);
                                    }
                                } elseif ($prospect->client_id == 0) {
                                    Analytic::where('prospecto_id', $prospect_id)
                                            ->where('solicitud_id', $solicitud_id)
                                            ->where('client_id', $client_id)
                                            ->update([
                                                'datos_obtenidos' => 'Sin datos en Analytics'
                                            ]);
                                }
                            }

                            $no_reporte += 1;
                        }

                        return response()->json($clientes_actualizados);
                    } else {
                        throw new Exception('No views (profiles) found for this user.');
                    }
                } else {
                    throw new Exception('No properties found for this user.');
                }
            } else {
                throw new Exception('No accounts found for this user.');
            }
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * Crea la conexión hacia Google Analytics
     *
     * @param  object $client Objeto Google Client
     *
     * @return object         Objeto Analytics
     */
    private function conexionAnalytics($client)
    {
        if (App::environment(['produccion'])) {
            $KEY_FILE_LOCATION = storage_path().'/client_secrets_monte.json';
        } else {
            $KEY_FILE_LOCATION = storage_path().'/client_secrets.json';
        }
        // Crea y configura un nuevo objeto cliente.
        $client->setApplicationName("Reporte 1 Analytics");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_Analytics($client);

        return $analytics;
    }

    /**
     * Crea los reportes que se obtendran de Google Analytics
     * @param  object $client    Objeto Google Client
     * @param  array  $informes  Informes que se van a crear
     *
     * @return array             Informes Google Analytics creados
     */
    private function creaReportes($client, $informes)
    {
        $reports = [];
        foreach ($informes as $dimensiones_informe) {
            $dimensiones = [];
            foreach ($dimensiones_informe as $dimension) {
                $d = new Google_Service_AnalyticsReporting_Dimension();
                $d->setName($dimension);
                $dimensiones[] = $d;
            }

            $analytics = new Google_Service_AnalyticsReporting($client);
            $view_id = env('ID_REPORTE');

            // Creando el objeto DateRange.
            $dateRange = new Google_Service_AnalyticsReporting_DateRange();
            $start = $this->startdateRange();
            $dateRange->setStartDate($start);
            $dateRange->setEndDate("today");

            // Creando el objeto Metricas.
            $metrics = new Google_Service_AnalyticsReporting_Metric();
            $metrics->setExpression("ga:users");
            $metrics->setAlias("users");

            // Creando el objeto ReportRequest.
            $request = new Google_Service_AnalyticsReporting_ReportRequest();
            $request->setViewId($view_id);
            $request->setDateRanges($dateRange);
            $request->setMetrics([$metrics]);
            $request->setDimensions($dimensiones);
            $request->setPageSize(10000);

            $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
            $body->setReportRequests(array($request));

            $resultado = $analytics->reports->batchGet($body);

            for ($reportIndex = 0; $reportIndex < count($resultado); $reportIndex++) {
                $report = $resultado[ $reportIndex ];
                $header = $report->getColumnHeader();
                $dimensionHeaders = collect($header->getDimensions());
                $rows = $report->getData()->getRows();

                $reports[] = [
                    'headers' => $header,
                    'dimensionHeaders' => $dimensionHeaders,
                    'rows' => $rows
                ];
            }
        }

        return $reports;
    }

    /**
     * Obtiene el rango inicial de la fecha del reporte
     *
     * @return string Rango
     */
    private function startdateRange()
    {
        $hora = date('G');
        if ($hora == 0 || $hora == 1 || $hora == 2 || $hora == 3 || $hora == 4) {
            return 'yesterday';
        } else {
            return 'today';
        }
    }
}
