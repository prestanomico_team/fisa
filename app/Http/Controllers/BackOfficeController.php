<?php
namespace App\Http\Controllers;

use App\Analytic;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;
use App\Solicitud;
use App\Prospecto;
use App\User;
use App\DomicilioSolicitud;
use App\Sepomex;
use App\RespuestaCuestionarioDinamico;
use App\Situacion;
use App\CuestionarioDinamico;
use App\RespuestaMaquinaRiesgo;
use Excel;
use Carbon\Carbon;
use Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class BackOfficeController extends Controller
{
    public function panelRegistroPrueba(request $request)
    {
        $mock_persons = MockPerson::orderBy('nombre', 'ASC')->get();
        return view('crm.register.register_test', [
            "mocks"  => $mock_persons
        ]);
    }

    /**
     * Muestra la vista principal del panel, que contiene la lista de prospectos
     *
     * @param  request $request Arreglo con los datos que se envian
     *
     * @return view             Vista principal del panel
     */

    public function panelDashboard(request $request)
    {
        // Obteniendo la ip del usuario para determinar si puede visualizar
        // el panel
        $user_ip = self::getUserIP();
        $authorized_ips = json_decode(env('APP_PANEL_IPS'));

        $filtered = false;
        $filter_type = '';
        $filter_val = '';
        if ($request->input('filter_prospect_id') != '') {
            $filtered = true;
            $filter_type = 'id';
            $filter_val = $request->input('filter_prospect_id');
            //filter the list by id
            $prospects = Prospecto::with('ultima_solicitud')->where('id', $filter_val)->limit(10)->paginate(10);
        } elseif ($request->input('filter_prospect_email') != '') {
            $filtered = true;
            $filter_type = 'email';
            $filter_val = $request->input('filter_prospect_email');
            //filter the list by email
            $prospects = Prospecto::with('ultima_solicitud')->where('email', 'like', '%'.$filter_val.'%')->paginate(20);

        } elseif ($request->input('filter_prospect_name') != '') {

            $filtered = true;
            $filter_type = 'name';
            $filter_val = $request->input('filter_prospect_name');
            $prospects = Prospecto::all()->filter(function($record) use($filter_val) {
            if(stripos($record->nombres, $filter_val) !== false) {
                    return $record;
                }
            });
            $prospects = $prospects->paginate(10);


        } else {
            //don't filter the list
            $prospects = Prospecto::orderBy('updated_at', 'desc')->paginate(10);
        }

        return view('crm.dashboard', [
            'prospects'         => $prospects,
            'filtered'          => $filtered,
            'filter_type'       => $filter_type,
            'filter_val'        => $filter_val,
            'can_prospectos'    => Auth::user()->can('prospectos'),
            'can_solicitudes'   => Auth::user()->can('solicitudes')
        ]);
    }

    public function countReports(Request $request) {

        $start_date = Carbon::now()->format('Y-m-01');
        $end_date = Carbon::now()->format('Y-m-d 23:59:59');

        $total = Solicitation::selectRaw('created_at')
            ->where('created_at', '>=', $start_date.' 00:00:00')
            ->where('created_at', '<=', $end_date)
            ->orderBy('created_at', 'desc')
            ->get();

        $registros = count($total);
        $total = $total->chunk(10000);

        $bloques = null;
        foreach ($total as $key => $bloque) {
            $bloques[$key]['inicio'] = $bloque->first();
            $bloques[$key]['fin'] = $bloque->last();
        }

        return response()->json([
            'response' => 'success',
            'bloques' => $bloques,
            'total' => $registros
        ]);

    }

    public function downloadReport(Request $request) {

        $start_date = $request->input('fin');
        $end_date = $request->input('inicio');
        $month = Carbon::now()->format('m');
        $parte = $request->input('parte');
        $year = Carbon::now()->format('Y');
        $filas = null;
        $fileName = null;
        if ($request->input('full_report_type') == 'solics') {

            $solics = Solicitud::with('prospecto', 'domicilio', 'producto', 'bc_score')
                ->where('created_at', '>=', $start_date)
                ->where('created_at', '<=', $end_date)
                ->orderBy('created_at', 'desc');

            Excel::create('tabla-completa-solicitudes-'.$month.'-'.$year.'-'.$parte, function ($excel) use ($solics) {
                //Datos de Solicitud
                $excel->sheet('Datos', function ($sheet) use ($solics) {
                    // Set auto size for sheet
                    $sheet->setAutoSize(false);
                    $sheet->appendRow(array(
                        'SOLIC ID',
                        'PORSPECT ID',
                        'FECHA REGISTRO',
                        'PRODUCTO',
                        'STATUS',
                        'ULTIMO PUNTO REGISTRO',
                        'ÚLTIMA ACTUALIZACIÓN',
                        'PRESTAMO',
                        'PLAZO',
                        'FINALIDAD',
                        'NOS GUSTARÍA CONOCERTE',
                        'NOMBRES',
                        'APELLIDO PATERNO',
                        'APELLIDO MATERNO',
                        'FECHA DE NACIMIENTO',
                        'GENERO',
                        'RFC',
                        'CURP',
                        'EMAIL',
                        'CELULAR',
                        'EDAD',
                        'ESTADO CIVIL',
                        'LUGAR DE NACIMIENTO',
                        'NIVEL DE ESTUDIOS',
                        'OCUPACION',
                        'INGRESO',
                        'GASTOS FAMILIARES',
                        'TELEFONO OFICINA',
                        '# DEPENDIENTES',
                        'ANTIGUEDAD EMPLEO',
                        'CREDITO HIPOTECARIO',
                        'CREDITO AUTOMOTRIZ',
                        'CREDITO BANCARIO',
                        'ULT 4 DIGITOS TDC',
                        'ENCONTRADO BC',
                        'BC SCORE',
                        'MICRO SCORE',
                        'RESIDENCIA',
                        'CALLE',
                        'NUM EXT',
                        'NUM INT',
                        'COLONIA',
                        'DELEGACION/MUNICIPIO',
                        'CIUDAD',
                        'DOMICILIO ESTADO',
                        'CODIGO POSTAL',
                        'DOMICILIO AÑOS',
                        'TELEFONO DOMICILIO',
                        'ORIGEN'
                    ));

                    $solics->chunk(1000, function($rows) use ($sheet)
                    {
                        foreach ($rows as $sol)
                        {
                            $sol = self::decryptSolicitud($sol);
                            $pro = self::decryptProspect($sol->prospecto);

                            $referencia = null;
                            $producto = $sol->producto;
                            if (count($producto) != 0) {
                                $nombre_producto = mb_strtoupper($producto[0]->nombre_producto);
                                if ($producto[0]->pivot['lead'] != null) {
                                    $referencia = mb_strtoupper($producto[0]->pivot['lead']);
                                }
                            } else {
                                $nombre_producto = 'MERCADO ABIERTO';
                            }

                            $age = '';
                            if ($sol->fecha_nac_yyyy != '' && $sol->fecha_nac_mm != '' && $sol->fecha_nac_dd != '') {
                                $bdate = $sol->fecha_nac_yyyy.'-'.$sol->fecha_nac_mm.'-'.$sol->fecha_nac_dd;
                                if (date_create($bdate)) {
                                    $age = date_diff(date_create($bdate), date_create($sol->created_at))->y;
                                }
                            }

                            $current_upr = ($sol->ult_punto_reg == '') ? '[]' : $sol->ult_punto_reg;
                            $ult_punto_data = json_decode($current_upr);
                            $last_successfull = 'none';
                            $last_failed = 'none';

                            foreach ($ult_punto_data as $pr) {
                                if ($pr->success) {
                                    $last_successfull = $pr->punto;
                                } else {
                                    $last_failed = $pr->punto;
                                }
                            }

                            if (count($sol->domicilio) == 1) {
                                $calle = $sol->domicilio['calle'];
                                $num_exterior = $sol->domicilio['num_exterior'];
                                $num_interior = $sol->domicilio['num_interior'];
                                $colonia = $sol->domicilio['colonia'];
                                $delegacion = $sol->domicilio['delegacion'];
                                $ciudad = $sol->domicilio['ciudad'];
                                $estado = $sol->domicilio['codigo_estado'];
                                $cp = $sol->domicilio['cp'];
                            }

                            if ($referencia == null) {
                                $referencia = $pro->referencia;
                            }

                            $sheet->appendRow(array(
                                $sol->id,
                                $sol->prospect_id,
                                $sol->created_at,
                                $nombre_producto,
                                $sol->type,
                                $last_successfull,
                                $sol->updated_at,
                                $sol->prestamo,
                                self::getPlazoLabel($sol->plazo),
                                $sol->finalidad,
                                $sol->finalidad_custom,
                                $pro->nombre,
                                $pro->apellido_p,
                                $pro->apellido_m,
                                $sol->sexo,
                                $pro->email,
                                $pro->cel,
                                $age,
                                self::getYesOrNo($sol->credito_hipo),
                                self::getYesOrNo($sol->credito_auto),
                                self::getYesOrNo($sol->credito_banc),
                                $referencia,
                                self::getYesOrNo($sol->encontrado),
                                $sol->str_response,
                                $sol->rfc,
                                self::getEstadoCivilDesc($sol->estado_civil),
                                $sol->tipo_residencia,
                                $sol->numero_dependientes,
                                $sol->nivel_estudios,
                                $sol->ocupacion,
                                $sol->ingreso_mensual,
                                $sol->gastos_familiares,
                                (!empty($sol->fecha_nac_dd))? $sol->fecha_nac_dd.'/'.$sol->fecha_nac_mm.'/'.$sol->fecha_nac_yyyy : '',
                                isset($sol->bc_score) ? $sol->bc_score->bc_score : '',
                                $sol->calc_cuentas_hipo,
                                $sol->calc_cuentas_auto,
                                $sol->calc_cuentas_consumo,
                                $sol->calc_ingr_disp,
                                $sol->calc_atp,
                                $sol->calc_uso_lineas,
                                $sol->calc_mop2,
                                $sol->calc_mop3,
                                $sol->calc_consultas,
                                isset($sol->bc_score) ? $sol->bc_score->micro_valor : '',
                                $sol->icc_score,
                                $sol->user_ip,
                                $sol->antiguedad_domicilio,
                                $sol->antiguedad_empleo,
                                $sol->credito_banc_num,
                                $sol->peor_mop,
                                self::displayBuroDate($sol->fecha_peor_mop),
                                $sol->num_meses_desde_peor_mop,
                                $sol->calc_consultas,
                                $sol->fecha_consulta_mas_reciente,
                                (!empty($sol->lugar_nac_ciudad))? $sol->lugar_nac_ciudad.', '.$sol->lugar_nac_estado : '',
                                '',
                                self::displayBuroDate($sol->fecha_apertura_linea_mas_antigua),
                                $sol->claves_actividad_reportadas,
                                $sol->claves_fraude_reportadas,
                                $sol->claves_extravio_reportadas,
                                $sol->tel_casa,
                                $sol->telefono_empleo,
                                $sol->curp,
                                $calle,
                                $num_exterior,
                                $num_interior,
                                $colonia,
                                $delegacion,
                                $ciudad,
                                $estado,
                                $cp,
                            ));
                        }
                    });
                });
            })->store('csv', storage_path('excel/'))->export('csv');

        }

    }

    public function toViewFullCsvReport(request $request)
    {
        $cur_mon = date('m/Y');
        return redirect('panel/report-completo-csv/'.$cur_mon);
    }

    public function postToViewFullCsvReport(request $request)
    {
        $cur_mon = '';
        if ($request->input('full_report_month') != 'Mes' && $request->input('full_report_year') != 'Año') {
            $cur_mon = $request->input('full_report_month').'/'.$request->input('full_report_year');
        }
        return redirect('panel/report-completo-csv/'.$cur_mon);
    }

    /**
     * Genera el reporte a descargar en la vista Tabla Local cuando el método es
     * POST y la vista Tabla local cuando el método es GET
     *
     * @param  request $request  Arreglo que contiene los datos que se envían en la solicitud
     * @param  integer  $month   Mes del que se desea obtener la información
     * @param  integer  $year    Año del que se desea obtener la información
     *
     * @return excel/view        Reporte en formato CSV/Vista de la vista tabla local
     */
    public function viewCsvReportFull(request $request, $month, $year)
    {
        ini_set('memory_limit','1024M');
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $start_month_str = $year.'-'.$month.'-1 00:00:00';
        $days_in_m = date('t', mktime(0, 0, 0, $month, 1, $year));
        $first_next_m = $days_in_m + 1; // will give us the 1st day of the next month as cut off (less than date)
        $end_date = date('Y-m-d', strtotime($start_month_str. ' + '.$first_next_m.' days'));
        $start_date = date('Y-m-d', strtotime($start_month_str));
        $display_start_date = '1/'.$month.'/'.$year;
        $display_end_date = $days_in_m.'/'.$month.'/'.$year;

        if ($request->isMethod('get')) {

            return view('crm.full_data_table_ok', [
                'display_start_date' => $display_start_date,
                'display_end_date' => $display_end_date,
                'url_month' => $month,
                'url_year' => $year,
                'data' => [],
            ]);

        } else {

            set_time_limit(0);
            if ($request->input('full_report_type') == 'solics') {

                $solics = Solicitud::with('prospecto', 'domicilio', 'producto', 'bc_score')
                    ->where('updated_at', '>=', $start_date)
                    ->where('updated_at', '<=', $end_date)
                    ->orderBy('updated_at', 'desc');

                Excel::create('tabla-completa-solicitudes-'.$month.'-'.$year, function ($excel) use ($solics) {
                    //Datos de Solicitud
                    $excel->sheet('Datos', function ($sheet) use ($solics) {
                        // Set auto size for sheet
                        $sheet->setAutoSize(true);
                        $sheet->appendRow(array(
                            'SOLICITUD ID',
                            'PORSPECTO ID',
                            'FECHA REGISTRO',
                            'PRODUCTO',
                            'STATUS',
                            'ULTIMO PUNTO REGISTRO',
                            'ÚLTIMA ACTUALIZACIÓN',
                            'PRESTAMO',
                            'PLAZO',
                            'FINALIDAD',
                            'NOS GUSTARÍA CONOCERTE',
                            'NOMBRE',
                            'APELLIDO P',
                            'APELLIDO M',
                            'GENERO',
                            'EMAIL',
                            'CELULAR',
                            'EDAD',
                            'CRED HIPOTECARIO',
                            'CRED AUTOMOTRIZ',
                            'CRED BANCARIO (TDC)',
                            '4 ULT TDC',
                            'ORIGEN',
                            '(LOG DE CONSOLA) ENCONTRADO',
                            'RFC',
                            'ESTADO CIVIL',
                            'RESIDENCIA',
                            '# DEPENDIENTES',
                            'NIVEL DE ESTUDIOS',
                            'OCUPACION',
                            'INGRESO',
                            'GASTOS FAMILIARES',
                            'FECHA DE NACIMIENTO',
                            'BC SCORE',
                            'MICRO SCORE',
                            'IP ADDRESS',
                            'DOMICILIO AÑOS',
                            'ANTIGUEDAD EMPLEO',
                            'LUGAR DE NACIMIENTO',
                            'TELEFONO DOMICILIO',
                            'TELEFONO OFICINA',
                            'CURP',
                            'CALLE',
                            'NUM EXT',
                            'NUM INT',
                            'COLONIA',
                            'DELEGACION/MUNICIPIO',
                            'CIUDAD',
                            'DOMICILIO ESTADO',
                            'CODIGO POSTAL'
                        ));

                        $solics->chunk(1000, function($rows) use ($sheet)
                        {
                            foreach ($rows as $sol)
                            {

                                $referencia = null;
                                $producto = $sol->producto;
                                if (count($producto) != 0) {
                                    $nombre_producto = mb_strtoupper($producto[0]->nombre_producto);
                                    if ($producto[0]->pivot['lead'] != null) {
                                        $referencia = mb_strtoupper($producto[0]->pivot['lead']);
                                    }
                                } else {
                                    $nombre_producto = 'MERCADO ABIERTO';
                                }

                                $calle = '';
                                $num_exterior = '';
                                $num_interior = '';
                                $colonia = '';
                                $delegacion = '';
                                $ciudad = '';
                                $estado = '';
                                $cp = '';
                                if (isset($sol->domicilio['calle'])) {
                                    $calle = $sol->domicilio['calle'];
                                    $num_exterior = $sol->domicilio['num_exterior'];
                                    $num_interior = $sol->domicilio['num_interior'];
                                    $colonia = $sol->domicilio['colonia'];
                                    $delegacion = $sol->domicilio['delegacion'];
                                    $ciudad = $sol->domicilio['ciudad'];
                                    $estado = $sol->domicilio['codigo_estado'];
                                    $cp = $sol->domicilio['cp'];
                                }

                                if ($referencia == null) {
                                    if (isset($sol->prospecto->referencia)) {
                                        $referencia = $sol->prospecto->referencia;
                                    }
                                }

                                $sheet->appendRow(array(
                                    $sol->id,
                                    $sol->prospecto_id,
                                    $sol->created_at,
                                    $nombre_producto,
                                    $sol->status,
                                    $sol->sub_status,
                                    $sol->updated_at,
                                    $sol->prestamo,
                                    self::getPlazoLabel($sol->plazo),
                                    $sol->finalidad,
                                    $sol->finalidad_custom,
                                    $sol->prospecto->nombres,
                                    $sol->prospecto->apellido_paterno,
                                    $sol->prospecto->apellido_materno,
                                    $sol->sexo,
                                    $sol->prospecto->email,
                                    $sol->prospecto->celular,
                                    ($sol->fecha_nacimiento != '0000-00-00') ? Carbon::parse($sol->fecha_nacimiento)->age : '',
                                    self::getYesOrNo($sol->credito_hipotecario),
                                    self::getYesOrNo($sol->credito_automotriz),
                                    self::getYesOrNo($sol->credito_bancario),
                                    $sol->ultimos_4_digitos,
                                    $referencia,
                                    self::getYesOrNo($sol->encontrado),
                                    $sol->rfc,
                                    self::getEstadoCivilDesc($sol->estado_civil),
                                    $sol->tipo_residencia,
                                    $sol->numero_dependientes,
                                    $sol->nivel_estudios,
                                    $sol->ocupacion,
                                    $sol->ingreso_mensual,
                                    $sol->gastos_familiares,
                                    ($sol->fecha_nacimiento != '0000-00-00') ? $sol->fecha_nacimiento : '',
                                    isset($sol->bc_score) ? $sol->bc_score->bc_score : '',
                                    isset($sol->bc_score) ? $sol->bc_score->micro_valor : '',
                                    $sol->user_ip,
                                    $sol->antiguedad_domicilio,
                                    $sol->antiguedad_empleo,
                                    (!empty($sol->lugar_nacimiento_ciudad)) ? $sol->lugar_nacimiento_ciudad.', '.$sol->lugar_nacimiento_estado : '',
                                    $sol->telefono_casa,
                                    $sol->telefono_empleo,
                                    $sol->curp,
                                    $calle,
                                    $num_exterior,
                                    $num_interior,
                                    $colonia,
                                    $delegacion,
                                    $ciudad,
                                    $estado,
                                    $cp,
                                ));
                            }
                        });
                    });
                })->store('csv', storage_path('excel/'))->export('csv');
            }

        }
    }

    public function viewCsvReportDaily()
    {
        return view('crm.full_data_table_daily');
    }

    public function downloadReportDaily(Request $request)
    {

        ini_set('memory_limit','768M');
        $dates = $request->dateRange;
        $range = explode(' ', $dates);
        $startDate = $range['0'];
        $endDate = $range['2'];
        $sDate= Carbon::parse($range['0'])->startOfDay();
        $eDate= Carbon::parse($range['2'])->endOfDay();

        $diff = $eDate->diffInDays($sDate);

        if ($diff <= 30) {

            $solics = Solicitud::with('prospecto', 'domicilio', 'producto', 'bc_score','convenio')
            ->where('updated_at', '>=', $sDate)
            ->where('updated_at', '<=', $eDate)
            ->orderBy('updated_at', 'desc');

            Excel::create('tabla-diarias-solicitudes-'.$startDate.'-'.$endDate, function ($excel) use ($solics) {
                //Datos de Solicitud
                $excel->sheet('Datos', function ($sheet) use ($solics) {
                    // Set auto size for sheet
                    $sheet->setAutoSize(true);
                    $sheet->appendRow(array(
                        'SOLICITUD ID',
                        'PROSPECTO ID',
                        'FECHA REGISTRO',
                        'PRODUCTO',
                        'STATUS',
                        'ULTIMO PUNTO REGISTRO',
                        'ÚLTIMA ACTUALIZACIÓN',
                        'PRESTAMO',
                        'PLAZO',
                        'FINALIDAD',
                        'NOS GUSTARÍA CONOCERTE',
                        'NOMBRE',
                        'APELLIDO P',
                        'APELLIDO M',
                        'GENERO',
                        'EMAIL',
                        'CELULAR',
                        'EDAD',
                        'CRED HIPOTECARIO',
                        'CRED AUTOMOTRIZ',
                        'CRED BANCARIO (TDC)',
                        '4 ULT TDC',
                        'ORIGEN',
                        '(LOG DE CONSOLA) ENCONTRADO',
                        'RFC',
                        'ESTADO CIVIL',
                        'RESIDENCIA',
                        '# DEPENDIENTES',
                        'NIVEL DE ESTUDIOS',
                        'OCUPACION',
                        'INGRESO',
                        'GASTOS FAMILIARES',
                        'FECHA DE NACIMIENTO',
                        'BC SCORE',
                        'MICRO SCORE',
                        'IP ADDRESS',
                        'DOMICILIO AÑOS',
                        'ANTIGUEDAD EMPLEO',
                        'LUGAR DE NACIMIENTO',
                        'TELEFONO DOMICILIO',
                        'TELEFONO OFICINA',
                        'CURP',
                        'CALLE',
                        'NUM EXT',
                        'NUM INT',
                        'COLONIA',
                        'DELEGACION/MUNICIPIO',
                        'CIUDAD',
                        'DOMICILIO ESTADO',
                        'CODIGO POSTAL',
                        'DISPOSITIVO ENTRADA',
                        'DISPOSITIVO SALIDA',
                        'USUARIO',
                        '# EJECUTIVO'
                    ));

                    $solics->chunk(1000, function($rows) use ($sheet)
                    {
                        foreach ($rows as $sol)
                        {
                            $tipoDispositivo = Solicitud::with('primerDispositivo', 'ultimoDispositivo')->find($sol->id);
                            $referencia = null;
                            $producto = $sol->producto;
                            if (count($producto) != 0) {
                                $nombre_producto = mb_strtoupper($producto[0]->nombre_producto);
                                if ($producto[0]->pivot['lead'] != null) {
                                    $referencia = mb_strtoupper($producto[0]->pivot['lead']);
                                }
                            } else {
                                $nombre_producto = 'MERCADO ABIERTO';
                            }

                            $calle = '';
                            $num_exterior = '';
                            $num_interior = '';
                            $colonia = '';
                            $delegacion = '';
                            $ciudad = '';
                            $estado = '';
                            $cp = '';
                            if (isset($sol->domicilio['calle'])) {
                                $calle = $sol->domicilio['calle'];
                                $num_exterior = $sol->domicilio['num_exterior'];
                                $num_interior = $sol->domicilio['num_interior'];
                                $colonia = $sol->domicilio['colonia'];
                                $delegacion = $sol->domicilio['delegacion'];
                                $ciudad = $sol->domicilio['ciudad'];
                                $estado = $sol->domicilio['codigo_estado'];
                                $cp = $sol->domicilio['cp'];
                            }

                            if ($referencia == null) {
                                if (isset($sol->prospecto->referencia)) {
                                    $referencia = $sol->prospecto->referencia;
                                }
                            }

                            $sheet->appendRow(array(
                                $sol->id,
                                $sol->prospecto_id,
                                $sol->created_at,
                                $nombre_producto,
                                $sol->status,
                                $sol->sub_status,
                                $sol->updated_at,
                                $sol->prestamo,
                                self::getPlazoLabel($sol->plazo),
                                $sol->finalidad,
                                $sol->finalidad_custom,
                                $sol->prospecto->nombres,
                                $sol->prospecto->apellido_paterno,
                                $sol->prospecto->apellido_materno,
                                $sol->sexo,
                                $sol->prospecto->email,
                                $sol->prospecto->celular,
                                ($sol->fecha_nacimiento != '0000-00-00') ? Carbon::parse($sol->fecha_nacimiento)->age : '',
                                self::getYesOrNo($sol->credito_hipotecario),
                                self::getYesOrNo($sol->credito_automotriz),
                                self::getYesOrNo($sol->credito_bancario),
                                $sol->ultimos_4_digitos,
                                $referencia,
                                self::getYesOrNo($sol->encontrado),
                                $sol->rfc,
                                self::getEstadoCivilDesc($sol->estado_civil),
                                $sol->tipo_residencia,
                                $sol->numero_dependientes,
                                $sol->nivel_estudios,
                                $sol->ocupacion,
                                $sol->ingreso_mensual,
                                $sol->gastos_familiares,
                                ($sol->fecha_nacimiento != '0000-00-00') ? $sol->fecha_nacimiento : '',
                                isset($sol->bc_score) ? $sol->bc_score->bc_score : '',
                                isset($sol->bc_score) ? $sol->bc_score->micro_valor : '',
                                $sol->user_ip,
                                $sol->antiguedad_domicilio,
                                $sol->antiguedad_empleo,
                                (!empty($sol->lugar_nacimiento_ciudad))? $sol->lugar_nacimiento_ciudad.', '.$sol->lugar_nacimiento_estado : '',
                                $sol->telefono_casa,
                                $sol->telefono_empleo,
                                $sol->curp,
                                $calle,
                                $num_exterior,
                                $num_interior,
                                $colonia,
                                $delegacion,
                                $ciudad,
                                $estado,
                                $cp,
                                isset($tipoDispositivo->primerDispositivo->tipo_dispositivo) ? $tipoDispositivo->primerDispositivo->tipo_dispositivo : '',
                                isset($tipoDispositivo->ultimoDispositivo->tipo_dispositivo) ? $tipoDispositivo->ultimoDispositivo->tipo_dispositivo : '',                                
                                isset($sol->convenio['email']) ? self::getEmbajador($sol->convenio['email']) : '',
                                isset($sol->convenio['email']) ? self::getIdEmbajador($sol->convenio['email']) : ''
                            ));
                        }
                    });
                });
            })->store('csv', storage_path('excel/'))->export('csv');
        } else {
            $error = 'No se pueden seleccionar mas de 30 días';
            return view('crm.full_data_table_daily')->with(['error' => $error]);
        }

    }

    public function cleanEncryptProspects(request $request)
    {
        //get all prospects
        $prospects = Prospecto::all();
        //test each prospect row for encrypted values
        foreach ($prospects as $pr) {
            $pr = self::verifyEncryptProspect($pr);
            if ($pr) {
                $pr->save();
            }
        }
    }

    public function cleanEncryptSolics(request $request)
    {
        //get all prospects
        $solics = Solicitud::all();
        //test each solic row for encrypted values
        foreach ($solics as $solic) {
            $solic = self::verifyEncryptSolicitud($solic);
            if ($solic) {
                $solic->save();
            }
        }
    }

    public function testGetMonthsHist(request $request, $reciente, $solicitud, $max)
    {
        $res = self::sCalcGetMonthsHistToCount($reciente, $solicitud, $max);
        var_dump($res);
    }

    public function showRegErrorLogs(request $request)
    {
        $path = '../storage/logs/';
        if ($handle = opendir($path)) {
            $logs_arr = [];
            while (($file = readdir($handle)) !== false) {
                if ($file !== '.' && $file !== '..' && $file !== 'laravel.log') {
                    $content = file_get_contents($path.$file);
                    if (strpos($content, 'Data :') !== false) {
                        $strings_arr = explode('Data :', $content);
                        $file_split = explode('_ERROR', $file);

                        //10_03_2017_12_04_50
                        $dt = explode('_', $file_split[0]);
                        $date_time_str = $dt[0].'/'.$dt[1].'/'.$dt[2].' '.$dt[3].':'.$dt[4].':'.$dt[5];

                        //only get the error logs for the past month
                        $one_month_ts = strtotime("-1 month");
                        $log_date_time = \DateTime::createFromFormat("d/m/Y H:i:s", $date_time_str);
                        $log_timestamp = $log_date_time->getTimestamp();
                        if ($log_timestamp > $one_month_ts) {
                            $name_str = trim(str_replace('_', ' ', $file_split[1]));
                            $name_str = str_replace('.txt', '', $name_str);
                            $json = '{"datetime" : "'.$date_time_str.'", "name" : "'.$name_str.'", "console" : '.$strings_arr[0].', "data" : '.$strings_arr[1].'}';
                            array_push($logs_arr, array('date_time' => $date_time_str, 'value' => $json));
                        }
                    }
                }
            }
            closedir($handle);
            //sort logs_arr by date
            $logs_arr_sorted = [];
            usort($logs_arr, function ($a, $b) {
                $dateobj_a = \DateTime::createFromFormat("d/m/Y H:i:s", $a["date_time"]);
                $dateobj_b = \DateTime::createFromFormat("d/m/Y H:i:s", $b["date_time"]);
                $t1 = $dateobj_a->getTimestamp();
                $t2 = $dateobj_b->getTimestamp();
                return ($t2 - $t1);
            });
            //we just want the values
            foreach ($logs_arr as $lg) {
                array_push($logs_arr_sorted, $lg['value']);
            }
            return view('crm.register_error_log', [
        'logs' => $logs_arr_sorted
      ]);
        } else {
            echo 'could not open directory for logs';
        }
    }

    public function codigosPostalesConsole(request $request)
    {
        return view('crm.cp_console', [
            'data' => ''
        ]);
    }

    public function coberturaCodigos(request $request)
    {
        if (isset($request->cp)) {
            $cp = $request->cp;
            $codigos = Sepomex::selectRaw("codigo, estado, municipio, GROUP_CONCAT(colonia_asentamiento SEPARATOR ', ') as colonias, cobertura")
                ->whereRaw("codigo LIKE '%". $cp ."%'")
                ->groupBy('codigo')
                ->paginate(15);

            $codigos->withPath('/panel/cobertura?cp='.$cp);


        } else {
            $codigos = Sepomex::selectRaw("codigo, estado, municipio, GROUP_CONCAT(colonia_asentamiento SEPARATOR ', ') as colonias, cobertura")
                ->groupBy('codigo')
                ->paginate(15);
            $cp = '';
        }

        return view('crm.cobertura_codigos', [
            'codigos' => $codigos,
            'cp'      => $cp
        ]);
    }

    public function buscarCobertura(request $request)
    {
        if (strlen($request->cp) > 0 ) {
            $codigos = Sepomex::selectRaw("codigo, estado, municipio, GROUP_CONCAT(colonia_asentamiento SEPARATOR ', ') as colonias, cobertura")
                ->whereRaw("codigo LIKE '%". $request->cp ."%'")
                ->groupBy('codigo')
                ->paginate(15);

            $codigos->withPath('/panel/cobertura?cp='.$request->cp);

        } else {
            $codigos = Sepomex::selectRaw("codigo, estado, municipio, GROUP_CONCAT(colonia_asentamiento SEPARATOR ', ') as colonias, cobertura")
                ->groupBy('codigo')
                ->paginate(15);

            $codigos->withPath('/panel/cobertura');
        }

        $results['view'] =  view('crm.codigos')
            ->with('codigos', $codigos)
            ->render();

        return response()->json($results);

    }

    public function codigosPostalesGetState(request $request)
    {
        $path = '../storage/sepomex/CSV/';
        $estado = $request->input('estado');
        $valid_states = [
      "Aguascalientes","Baja California Sur","Baja California","Campeche","Chiapas","Chihuahua","Distrito Federal","Coahuila de Zaragoza","Colima","Durango","Guanajuato","Guerrero","Hidalgo","Jalisco","México","Michoacán de Ocampo","Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí","Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz de Ignacio de la Llave","Yucatán","Zacatecas"
    ];
        if (in_array($estado, $valid_states)) {
            $fname = $path.urldecode($estado).'.csv';
            $estado_data = [];
            $cp_count = 0;
            if (($handle = fopen($fname, "r")) !== false) {
                //get each row
                while (($row = fgetcsv($handle, 1000, ",", '"')) !== false) {
                    $cp_count++;
                    array_push($estado_data, $row);
                }
                $data_obj = json_encode($estado_data);
                echo '{
          "stat" : 1,
          "estado" : "'.$estado.'",
          "cp_count" : '.$cp_count.',
          "data" : '.$data_obj.'
        }';
            }
        } else {
            echo '{
        "stat" : 0,
        "message" : "could not find data for '.$estado.'"
      }';
        }
    }

    public function codigosPostalesUpdateVerify(request $request)
    {
        $row_cp = $request->input('cp');
        $row_colonia = $request->input('colonia');
        $row_deleg_munic = $request->input('deleg_munic');
        $row_ciudad = $request->input('ciudad');
        $row_estado = $request->input('estado');
        $matches = DB::table('codigospostales')->where('cp', '=', $row_cp)->get();
        $added_id = false;//default
    $exact_match = false;//default
    if ($matches) {//found one or more entries for this cp
      //check if any match this row exactly
      foreach ($matches as $m) {
          //use deleg as row_ciudad value if ciudad is empty
          if ($row_ciudad == '') {
              $row_ciudad = $row_deleg_munic;
          }
          if ($m->cp == $row_cp && $m->colonia == $row_colonia && $m->deleg_munic == $row_deleg_munic && $m->ciudad == $row_ciudad && $m->estado == $row_estado) {
              $exact_match = true;
          }
      }
    }
        if (!$matches || !$exact_match) {
            //no matches or no exact match found, create new entry for this row
            $added_id = DB::table('codigospostales')->insertGetId([
        'cp' => $row_cp,
        'colonia' => $row_colonia,
        'deleg_munic' => $row_deleg_munic,
        'ciudad' => $row_ciudad,
        'estado' => $row_estado
      ]);
        }
        if ($added_id) {
            echo '{
          "stat" : "no matches found",
          "message" : "added new cp row to database",
          "id" : '.$added_id.'
        }';
        } else {
            //this row is up to date, no change necessary
            echo '{
        "stat" : "cp up to date",
        "message" : "no changes necessary"
      }';
        }
    }

}
