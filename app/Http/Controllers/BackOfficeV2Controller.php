<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;
use App\MockPerson;
use App\Solicitud;
use App\Prospecto;
use App\User;
use App\DomicilioSolicitud;
use App\Sepomex;
use App\RespuestaCuestionarioDinamico;
use App\Situacion;
use App\CuestionarioDinamico;
use App\Producto;
use App\Etiquetas;
use Excel;
use Carbon;
use Illuminate\Contracts\Encryption\DecryptException;

class BackOfficeV2Controller extends Controller
{
    /**
     * Muestra la vista principal del panel, que contiene la lista de prospectos
     *
     * @return view             Vista principal del panel
     */

    public function panelDashboard()
    {
        return view('crm.dashboardv2');
    }

    public function detalleProspecto($prospecto_id)
    {
        return view('crm.detalleProspecto', [
            'prospecto_id' => $prospecto_id
        ]);
    }

    public function detalleSolicitud($solicitud_id)
    {
        return view('crm.detalleSolicitud', [
            'solicitud_id' => $solicitud_id
        ]);
    }

    public function getDetalleProspecto($prospect_id)
    {
        $prospecto = Prospecto::with('solicitudes.producto')
            ->find($prospect_id);

        foreach ($prospecto->solicitudesV2 as $solicitud) {
            $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
            $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
            $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
            $solicitud->encontrado = ($solicitud->encontrado) ? 'Si' : 'No';
            $solicitud->created_at_disp = self::displayMySqlDate($solicitud->created_at->toDateTimeString());
            $solicitud->updated_at_disp = self::displayMySqlDate($solicitud->updated_at->toDateTimeString());

            // Obteniendo el score
            if (isset($solicitud->bc_score->bc_score)) {
                $solicitud->score_bc = $solicitud->bc_score->bc_score;
                $solicitud->score_micro = $solicitud->bc_score->micro_valor;
            } else {
                $solicitud->score_bc = 0;
                $solicitud->score_micro = 0;
            }

            // Obteniendo el producto
            if (isset($solicitud->producto[0])) {
                $solicitud->nombre_producto = $solicitud->producto[0]->nombre_producto;
            } else {
                $solicitud->nombre_producto = 'MERCADO ABIERTO';
            }
        }

        return response()
                ->json($prospecto)
                ->header('Content-Type', 'json');
    }

    public function getDetalleSolicitud($solicitud_id) {

        $solicitud = Solicitud::with(
                'prospecto', 'domicilio', 'datos_adicionales', 'producto',
                'respuesta_maquina_riesgos', 'respuesta_modelo_predominante', 'consultas_buro', 'bc_direcciones',
                'bc_empleos', 'bc_consultas', 'bc_cuentas', 'bc_hawk', 'bc_resumen_buro',
                'bc_score', 'alp', 'convenio', 'tracking', 'log')
            ->select('id', 'prospecto_id', 'status', 'sub_status', 'sexo', 'fecha_nacimiento',
                'lugar_nacimiento_ciudad', 'lugar_nacimiento_estado', 'rfc',
                'curp', 'telefono_casa', 'estado_civil', 'nivel_estudios', 'autorizado',
                'encontrado', 'user_ip', 'finalidad_custom', 'ocupacion', 'fuente_ingresos',
                'credito_hipotecario', 'credito_automotriz', 'credito_bancario', 'ultimos_4_digitos',
                'tipo_residencia', 'ingreso_mensual', 'antiguedad_empleo', 'telefono_empleo',
                'antiguedad_domicilio', 'gastos_familiares', 'numero_dependientes',
                'prestamo', 'plazo', 'finalidad', 'pago_estimado', 'finalidad_custom',
                'created_at', 'updated_at')
            ->find($solicitud_id);
        try {
            $solicitud->prospecto->encryptd = decrypt($solicitud->prospecto->encryptd);
        } catch (DecryptException $e) {
            $solicitud->prospecto->encryptd = '';
        }

        $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
        $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
        $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
        $solicitud->encontrado = ($solicitud->encontrado)? 'Si' : 'No';
        $solicitud->sexo = ($solicitud->sexo != '')? ($solicitud->sexo == 'M')? 'Masculino' : 'Femenino' : '';
        $solicitud->estado_civil = self::getEstadoCivilDesc($solicitud->estado_civil);
        $solicitud->fecha_nac = $solicitud->fecha_nacimiento;
        $solicitud->autorizado = self::getYesOrNo($solicitud->autorizado);
        $solicitud->credito_hipotecario = self::getYesOrNo($solicitud->credito_hipotecario);
        $solicitud->credito_automotriz = self::getYesOrNo($solicitud->credito_automotriz);
        $solicitud->credito_bancario = self::getYesOrNo($solicitud->credito_bancario);
        $solicitud->ingreso_mensual = '$'.number_format($solicitud->ingreso_mensual, 0, '.', ',');
        $solicitud->antiguedad_empleo  = (isset($solicitud->tipo_residencia) && $solicitud->tipo_residencia != '')? self::getAntiguedadLabel($solicitud->antiguedad_empleo) : '';
        $solicitud->antiguedad_domicilio  = (isset($solicitud->tipo_residencia) && $solicitud->tipo_residencia != '')? self::getAntiguedadLabel($solicitud->antiguedad_domicilio) : '';
        $solicitud->gastos_familiares = '$'.number_format($solicitud->gastos_familiares, 0, '.', ',');
        $solicitud->created_at_disp = self::displayMySqlDate($solicitud->created_at->toDateTimeString());
        
        // Obteniendo datos FICO
        if (isset($solicitud->alp->alp_request)) {

            $solicitud->fico_tabla = 1;
            $solicitud->fico = 0;

        }

        $etiquetas = Etiquetas::all()->pluck('etiqueta', 'descripcion');
        $solicitud->etiquetas = $etiquetas;

        if (isset($solicitud->bc_direcciones)) {
            $solicitud->bc_direccionesF = $solicitud->bc_direcciones->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_direccionesF = array_map('array_filter', $solicitud->bc_direccionesF);
        }

        if (isset($solicitud->bc_empleos)) {
            $solicitud->bc_empleosF = $solicitud->bc_empleos->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_empleosF = array_map('array_filter', $solicitud->bc_empleosF);
        }

        if (isset($solicitud->bc_consultas)) {
            $solicitud->bc_consultasF = $solicitud->bc_consultas->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_consultasF = array_map('array_filter', $solicitud->bc_consultasF);
        }

        if (isset($solicitud->bc_cuentas)) {
            $solicitud->bc_cuentasF = $solicitud->bc_cuentas->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_cuentasF = array_map('array_filter', $solicitud->bc_cuentasF);
        }

        if (isset($solicitud->bc_hawk)) {
            $solicitud->bc_hawkF = $solicitud->bc_hawk->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_hawkF = array_map('array_filter', $solicitud->bc_hawkF);
        }

        if (isset($solicitud->bc_resumen_buro)) {
            $solicitud->bc_resumen_buroF = $solicitud->bc_resumen_buro->toArray();
            // Quitando las columnas que tienen valores null
            $solicitud->bc_resumen_buroF = array_filter($solicitud->bc_resumen_buroF);
        }

        if (isset($solicitud->consultas_buro)) {

            $respuesta = collect($solicitud->consultas_buro)
                ->where('orden', 'Segunda')
                ->where('tipo', 'Respuesta')
                ->values();

            $PN = [];
            if (count($respuesta) > 0 && $respuesta[0]['PN'] != '') {
                $segmento[] = $respuesta[0]['PN'];
                $PN = app(BuroCreditoController::class)
                    ->analizaSegmento($segmento, 'Respuesta', 'PN');

                $solicitud->bc_datospersonales = $PN;
            }

        }

        // Obteniendo los cuestionarios dinámico
        if (isset($solicitud->respuesta_modelo_predominante[0])) {

            if ($solicitud->respuesta_modelo_predominante[0]->pantallas_extra == 1) {
                if ($solicitud->respuesta_modelo_predominante[0]->cuestionario_dinamico_guardado == 1) {

                    $respuestas = RespuestaCuestionarioDinamico::where('solicitud_id', $solicitud->id)
                        ->get();
                    $respuestas = $respuestas->groupBy('situacion_id')->toArray();

                    $situaciones_id = array_keys($respuestas);
                    $situaciones_text = explode(',', $solicitud->respuesta_modelo_predominante[0]->situaciones);

                    $situaciones = Situacion::select('id', 'encabezado')
                        ->whereIN('situacion', $situaciones_text)
                        ->get();
                    $situaciones = $situaciones->keyBy('id')->toArray();

                    $preguntas = CuestionarioDinamico::select('id', 'pregunta')
                        ->whereIN('situacion_id', $situaciones_id)
                        ->get();
                    $preguntas = $preguntas->keyBy('id')->toArray();

                    $cuestionariosDinamicos = [
                        'situaciones' => $situaciones,
                        'preguntas'   => $preguntas,
                        'respuestas'  => $respuestas
                    ];

                    $solicitud->cuestionarios_dinamicos = $cuestionariosDinamicos;
                }
            }
        }

        if ($solicitud->log->exists()) {
            $solicitud->ultimo_mensaje_usuario = json_decode($solicitud->log['ultimo_mensaje_usuario'], true);
        }

        return response()
                ->json($solicitud)
                ->header('Content-Type', 'json');
    }

}
