<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Cookie;
use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Etiquetas;
use App\Solicitud;
use App\Prospecto;
use App\DomicilioSolicitud;
use App\ConsultaBcTest;
use App\DetalleCuenta;
use App\DetalleConsulta;
use App\DatosBuro;
use App\ConsultasBuro;
use App\BCScores;
use App\BCDirecciones;
use App\BCEmpleos;
use App\BCConsultas;
use App\BCCuentas;
use App\BCHawk;
use App\BCResumenBuro;
use App\BCDatosPersonales;
use App\Alp;
use App\Tb1_CtasModAcep_MR;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Repositories\SolicitudRepository;
use GoogleTagManager;
use Excel;
use Validator;

class BuroCreditoController extends Controller
{
    private $configuracionProducto;
    private $bcScore;
    private $edadMinima;
    private $edadMaxima;
    private $campoCobertura;
    private $alp;
    private $microScore;
    private $sinHistorial;
    private $sinCuentasRecientes;
    private $solicitudRepository;

    /**
     * Constructor de la clase
     */
    public function __construct(request $request)
    {
        if (!$request->is('api/*')) {
            $solicitud_id = null;
            $producto = null;
            $version = null;
            $solicitud_id = $request->solicitud_id;

            if ($solicitud_id != null) {

                $solicitud = Solicitud::with('producto')
                    ->where('id', $solicitud_id)
                    ->get()
                    ->toArray();

                if (count($solicitud[0]['producto']) == 1) {
                    $producto = $solicitud[0]['producto'][0]['id'];
                    $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
                }

            }

            $this->configuracionProducto = App::call(
                'App\Http\Controllers\ProductoController@obtenerConfiguracion',
                [
                    'id_producto' => $producto,
                    'version' => $version
                ]
            );
            $this->bcScore = $this->configuracionProducto['bc_score'];
            $this->condicion = $this->configuracionProducto['condicion'];
            $this->microScore = $this->configuracionProducto['micro_score'];
            $this->edadMinima = $this->configuracionProducto['edad_minima'];
            $this->edadMaxima = $this->configuracionProducto['edad_maxima'];
            $this->campoCobertura = $this->configuracionProducto['campo_cobertura'];
            $this->storedProcedure = $this->configuracionProducto['stored_procedure'];
            $this->alp = $this->configuracionProducto['consulta_alp'];
            $this->sinHistorial = $this->configuracionProducto['sin_historial'];
            $this->sinCuentasRecientes = $this->configuracionProducto['sin_cuentas_recientes'];
            $this->solicitudRepository = new SolicitudRepository;
        }
    }

    /**
     * Realiza la primera llamada a buro de crédito, la procesa y verifica que
     * el BC Score y Micro Score pasen el punto de corte establecido en la configuración
     * del producto.
     *
     * @param  Request $request Datos enviados del formulario
     *
     * @return json             Resultado de realizar y procesar la primera llamada
     * a buró.
     */
    public function primeraLlamada(Request $request) {

        $claveProducto = env('CVE_PRODUCTO_LLAMADA_1');
        $INTL_USER = env('INTL1_USER');
        $INTL_PASS = env('INTL1_PASS');
        $INTL_SERVER = env('INTL_SERVER');
        $INTL_PORT = env('INTL_PORT');
        $eventTM = null;

        $prospecto_id = $request->input("prospecto_id");
        $solicitud_id = $request->input("solicitud_id");
        $seconds = $request->input("seconds");

        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::with('domicilio', 'producto')->find($solicitud_id);
        $redireccion = $solicitud->producto[0]['redireccion'];
        // Verificando si la consulta a buró de crédito esta autorizada.
        if ($solicitud->autorizado == false) {

            $mnsj_str = [
                'stat'      => 'ERROR',
                'message'   => 'El usuario no autorizó la consulta a Buró de Crédito'
            ];

            // Actualizando el campo ult_mensj_a_usuario de la solicitud
            return response()->json($mnsj_str);

        }

        // Verificando si el prospecto esta en una plaza cubierta y esta en el
        // rango de edad.
        $coberturaEdad = $this->checkCoberturaEdad($solicitud);

        // Generando el string de consulta para la primera llamada a BC
        $cadenaPrimeraLLamadaBC = $this->makeRequestBC(
            'Primera',
            $claveProducto,
            $INTL_USER,
            $INTL_PASS,
            $INTL_SERVER,
            $INTL_PORT,
            $prospecto_id,
            $solicitud_id
        );

        if (isset($cadenaPrimeraLLamadaBC['maximosReintentos'])) {

            $mnsj_str = [
                'stat'              => 'Error BC',
                'success'           => false,
                'message'           => 'Máximo de intentos de consulta a buró.',
                'maximosReintentos' => true,
                'redirect'          => $redireccion
            ];

            $solicitud->sub_status = 'Primera Llamada';
            $solicitud->save();
            // Actualizando el ult_mensj_a_usuario de la solicitud
            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                'Consulta BC',
                'Primera Llamada',
                1,
                'Máximo de intentos de consulta a buró.',
                ['error' => $mnsj_str]
            );
            session()->flash('nueva_solicitud', true);
            return response()->json($mnsj_str);
        }

        // Si la cadena se genera correctamente continuamos con el proceso
        // Actualizando el campo ult_punto_reg
        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            'Consulta BC',
            'Primera Llamada Cadena',
            1,
            'Se generó la cadena para consulta BC Score y Micro Score con éxito'
        );

        // Obteniendo la IP del usuario
        $user_ip = self::getUserIP();
        $solicitud->user_ip = $user_ip;
        $solicitud->sub_status = 'Primera Llamada Cadena';
        $solicitud->save();

        $respuestaTelnet = $this->telnetBC('Primera', $cadenaPrimeraLLamadaBC, $solicitud->rfc, $INTL_SERVER, $INTL_PORT, $seconds);

        // Verificando si la respuesta fue exitosa
        if ($respuestaTelnet['success'] == true) {

            $respuestaString = $respuestaTelnet['content'];

            // Procesando la respuesta de la primera llamada a BC por segmentos
            $procesaRespuestaBC =  $this->procesaRespuestaBC('Primera', $respuestaString);

            // Verificando si buró respondió con un error
            if (isset($procesaRespuestaBC['ERRR'])) {

                $consultaBuro_id = $respuestaTelnet['consultaBuro_id'];
                $consultaBuro = ConsultasBuro::find($consultaBuro_id);

                $consultaBuro->ERRR = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ERRR', []));
                $consultaBuro->ES = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ES', []));
                $consultaBuro->save();

                $segmentoERRR = $procesaRespuestaBC['ERRR'];
                $analizaERRR = $this->analizaSegmento($segmentoERRR, 'Respuesta', 'ERRR');

                $mnsj_str = [];
                $mnsj_str['stat'] = 'Error BC';
                foreach ($analizaERRR as $key => $val) {
                    $mnsj_str[$key] = $val;
                }
                $modal = '';
                if (isset($mnsj_str[0]['Ultima información valida del Cliente'])) {
                    $ultimaInformacionValida = $mnsj_str[0]['Ultima información valida del Cliente'];

                    if (strpos($ultimaInformacionValida, 'PN04') === false) {
                        $stat = 'No Encontrado';
                        $modal = view("modals.modalErrorBC")->render();
                    } else {
                        $stat = 'Error en Captura';
                        $rfc = substr($solicitud->rfc, 0, 10);
                        $homoclave = substr($solicitud->rfc, 10, 12);
                        $datos = [
                            'rfc'               => $rfc,
                            'homoclave'         => $homoclave,
                            'fecha_nacimiento'  => Carbon::createFromFormat('Y-m-d', $solicitud->fecha_nacimiento)->format('d-m-Y')
                        ];
                        $modal = view("modals.modalCorreccionRFC", $datos)->render();
                    }
                } elseif (isset($mnsj_str[0]['Falta campo requerido'])) {
                    $faltaCampo = $mnsj_str[0]['Falta campo requerido'];
                    if (strpos($faltaCampo, 'PN05') === false) {
                        $stat = 'No Encontrado';
                        $modal = view("modals.modalErrorBC")->render();
                    } else {
                        $stat = 'Error en Captura';
                        $rfc = substr($solicitud->rfc, 0, 10);
                        $homoclave = substr($solicitud->rfc, 11, 13);
                        $datos = [
                            'rfc'               => $rfc,
                            'homoclave'         => $homoclave,
                            'fecha_nacimiento'  => $solicitud->fecha_nacimiento
                        ];
                        $modal = view("modals.modalCorreccionRFC", $datos)->render();
                    }
                } else {
                    $stat = 'No Autenticado';
                    $modal = view("modals.modalErrorBC")->render();
                }

                // Actualizando el ult_mensj_a_usuario de la solicitud
                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
                // Actualizando el ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    'Consulta BC',
                    'Primera Llamada',
                    1,
                    'Se recibió una error en la primera llamada a buró de crédito.',
                    ['error' => $mnsj_str]
                );
                $solicitud->sub_status = 'Primera Llamada';
                $solicitud->save();

                if (count($solicitud->producto) > 0) {
                    $lead = $solicitud->producto[0]['pivot']['lead'];
                    $lead_id = $solicitud->producto[0]['pivot']['lead_id'];
                    $tipo_proceso = ($solicitud->producto[0]['proceso_experian'] == 1) ? 'primera_llamada_bc_experian' : 'primera_llamada_bc';
                } else {
                    $lead = null;
                    $lead_id = null;
                    $tipo_proceso = 'primera_llamada_bc';
                }

                return response()->json([
                    'success'           => false,
                    'stat'              => $stat,
                    'message'           => $analizaERRR,
                    'modal'             => $modal,
                    'edad_maxima'       => $this->edadMaxima,
                    'edad_minima'       => $this->edadMinima,
                    'siguiente_paso'    => $tipo_proceso
                ]);

    		} else {

                // De lo contrario se analiza el resultado que arroja buro
                $consultaBuro_id = $respuestaTelnet['consultaBuro_id'];
                $consultaBuro = ConsultasBuro::find($consultaBuro_id);

                // INTL
                $segmentoINTL = $procesaRespuestaBC['INTL'];
                $analizaINTL = $this->analizaSegmento($segmentoINTL, 'Respuesta', 'INTL');
                $consultaBuro->INTL = implode(PHP_EOL, array_get($procesaRespuestaBC, 'INTL', []));

                // Datos del Usuario
                $segmentoPN = $procesaRespuestaBC['PN'];
                $analizaPN = $this->analizaSegmento($segmentoPN, 'Respuesta', 'PN');
                $consultaBuro->PN = implode(PHP_EOL, array_get($procesaRespuestaBC, 'PN', []));

                // Direcciones
                $segmentoPA = $procesaRespuestaBC['PA'];
                $analizaPA = $this->analizaSegmento($segmentoPA, 'Respuesta', 'PA');
                $this->guardaDirecciones($analizaPA, $prospecto_id, $solicitud_id);
                $consultaBuro->PA = implode(PHP_EOL, array_get($procesaRespuestaBC, 'PA', []));

                // Scores
                $segmentoSC = $procesaRespuestaBC['SC'];
                $analizaScore = $this->analizaSegmento($segmentoSC, 'Respuesta', 'SC');
                $consultaBuro->SC = implode(PHP_EOL, array_get($procesaRespuestaBC, 'SC', []));

                // Cierre
                $segmentoES = $procesaRespuestaBC['ES'];
                $analizaES = $this->analizaSegmento($segmentoES, 'Respuesta', 'ES');
                $consultaBuro->ES = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ES', []));

                if (isset($analizaES[0]['cierre_num_control'])) {
                    $consultaBuro->folio_consulta = $analizaES[0]['cierre_num_control'];
                }
                $consultaBuro->save();

                $datosScores = [];
                foreach ($analizaScore as $score) {
                    $nombre = $this->getNombreScore($score['micro_codigo_prod']);
                    $datosScores[$nombre] = $score;
                }

                // Verificando si el BC SCORE o MICRO VALOR existen en el segmento SC
                if (isset($datosScores['BC SCORE']['micro_valor']) && isset($datosScores['SCORE BURO MICRO']['micro_valor'])) {

                    $eventTM[] = ['event'=> 'EncontradoBC', 'userId' => $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];

                    // Actualizando el campo ult_punto_reg de la solicitud
                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        'Consulta BC',
                        'Primera Llamada',
                        1,
                        'Se recibió una respuesta de la primera llamada sin errores, iniciando Check Encontrado.'
                    );
                    // Actualizando el status de la solicitud a Hit BC
                    $solicitud->status = 'Hit BC';
                    $solicitud->save();

                    // Actualizando los campos de bc_score y micro_valor
                    $this->guardaScores($datosScores, $prospecto_id, $solicitud_id, false);
                    $bc_score = array_get($datosScores, 'BC SCORE.micro_valor', null);
                    $solicitud->encontrado = array_get($analizaINTL, 'clave_respuesta', null);
                    $solicitud->save();

                    $regla = null;
                    $regla_string = null;
                    $regla_string_failed = null;
                    if ($this->microScore == null) {
                        $regla = $datosScores['BC SCORE']['micro_valor'] >= $this->bcScore;
                        $regla_string = 'Bc Score es mayor o igual a '.$this->bcScore;
                        $regla_string_failed = 'Bc Score no es mayor o igual a '.$this->bcScore;
                    } else {
                        $regla = intval($datosScores['BC SCORE']['micro_valor']). ' >= ' .$this->bcScore. ' ' .$this->condicion. ' ' .intval($datosScores['SCORE BURO MICRO']['micro_valor']). ' >= ' .$this->microScore;
                        $regla = eval('return '.$regla.';');
                        $regla_string = 'Bc Score es mayor o igual a '.$this->bcScore. ' '.$this->condicion. ' Micro Score es mayor o igual a '.$this->microScore;
                        $regla_string_failed = 'Bc Score no es mayor o igual a '.$this->bcScore. ' '.$this->condicion. ' Micro Score no es mayor o igual a '.$this->microScore;
                    }

                    // Si no cumple el con la cobertura y edad No Califica.
                    if ($coberturaEdad['success'] == false) {

                        $sub_status = '';
                        if (($datosScores['BC SCORE']['micro_valor'] == -8 || $datosScores['BC SCORE']['micro_valor'] == -9)) {
                            $sub_status = "Check Bc Score Elegible NC_".$coberturaEdad['reasonA'];
                        } else {
                            if (!$regla) {
                                $coberturaEdad['reason'] .= 'Score';
                                $coberturaEdad['reasonA'] .= 'S';
                            }
                            $sub_status = "Check Bc Score Normal NC_".$coberturaEdad['reasonA'];
                        }

                        $mnsj_str = [
                            'stat'          => 'No Califica',
                            'encontrado'    => $solicitud->encontrado,
                            'prospecto_id'  => $prospecto_id,
                            'solicitud_id'  => $solicitud_id,
                            'bc_score'      => $bc_score,
                            'califica'      => 0,
                            'edad'          => $coberturaEdad['edad'],
                            'estado'        => $coberturaEdad['estado'],
                            'delegacion'    => $coberturaEdad['delegacion'],
                            'result'        => 'No califica por su: '.$coberturaEdad['reason']
                        ];


                        // Actualizando el campo ult_punto_reg de la solicitud
                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            $sub_status,
                            0,
                            'No Califica (Edad, Ciudad o Estado)',
                            ['fallado' => 'No califica por su: '.$coberturaEdad['reason']]
                        );
                        // Guardando el mensaje en el campo ult_mensj_a_usuario
                        $this->solicitudRepository->setUltMnsjUsr(
                            $solicitud,
                            json_encode($mnsj_str)
                        );
                        $solicitud->sub_status = $sub_status;
                        $solicitud->save();

                        $modal = view("modals.modalPlazaCobertura")->render();
                        $mnsj_str['modal'] = $modal;
                        session()->flash('nueva_solicitud', true);
                        return response()->json($mnsj_str);
                    }

                    // Verificando si el BC SCORE no resulta -8, si es asi no
                    // hay segunda llamada a buró y se considera como Datos Elegible
                    $reglaSinCuentasRecientes = null;
                    $reglaSinHistorial = null;
                    $reglaString = null;
                    $pasaSinHistorial = null;

                    if ($this->sinCuentasRecientes == true && $datosScores['BC SCORE']['micro_valor'] == -8) {
                        $reglaSinCuentasRecientes = intval($datosScores['BC SCORE']['micro_valor']). ' == -8 && '.$this->sinCuentasRecientes. ' == true';
                        $reglaSinCuentasRecientes = eval('return '.$reglaSinCuentasRecientes.';');
                        $reglaString = 'Bc Score resultó -008';
                    }

                    if ($this->sinHistorial == true && $datosScores['BC SCORE']['micro_valor'] == -9) {
                        $reglaSinHistorial = intval($datosScores['BC SCORE']['micro_valor']). ' == -9 && '.$this->sinHistorial. ' == true';
                        $reglaSinHistorial = eval('return '.$reglaSinHistorial.';');
                        $reglaString = 'Bc Score resultó -009';
                        $pasaSinHistorial = rand(1, 10)/10;
                    }

                    // Verificando si el BC SCORE no resulta -8 o -9, si es asi no
                    // hay segunda llamada a buró y se considera como Datos Elegible
                    if ($reglaSinCuentasRecientes == true || ($reglaSinHistorial == true && $pasaSinHistorial <= 1)) {

                        if ($pasaSinHistorial !== null) {
                            $reglaString .= ' avanza por porcentaje: '.($pasaSinHistorial*100);
                        }

                        $mnsj_str = [
                            'stat'          => 'Datos Elegible',
                            'encontrado'    => $solicitud->encontrado,
                            'prospecto_id'  => $prospecto_id,
                            'solicitu_id'   => $solicitud_id,
                            'califica'      => 1,
                            'result'        => $reglaString.'. Saltando segunda llamada y enviando a datos adicionales.',
                            'success'       => true
                        ];

                        // Actualizando el campo ult_punto_reg de la solicitud
                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'Check Bc Score Elegible',
                            1,
                            $reglaString.'. Saltando segunda llamada y enviando a datos adicionales.'
                        );

                        $solicitud->sub_status = 'Check Bc Score Elegible';
                        $solicitud->save();
                        // Guardando el mensaje en el campo ult_mensj_a_usuario
                        return response()->json($mnsj_str);

                    // Si el BC SCORE no es -8 o -9 se verifica que pase el punto de corte establecido en
                    // la configuración del prodcuto.
                    } elseif ($regla) {

                        if (count($solicitud->producto) > 0) {
                            $lead = $solicitud->producto[0]['pivot']['lead'];
                            $lead_id = $solicitud->producto[0]['pivot']['lead_id'];
                        } else {
                            $lead = null;
                            $lead_id = null;
                        }

                        if ($lead == null && $lead_id == null) {
                            $lead = 'none';
                            $lead_id = 'none';
                        }

                        // Se encontró al usuario en BC
                        $solicitud->sub_status = 'Check Encontrado';
                        $solicitud->save();

                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'Check Encontrado',
                            1,
                            'Se encontró el prospecto en BC'
                        );

                        $mnsj_str = [
                            'success'           => true,
                            'stat'              => 'Califica BC Score',
                            'message'           => $regla_string,
                            'califica'          => 1,
                            'encontrado'        => $solicitud->encontrado,
                            'lead'              => $lead,
                            'lead_id'           => $lead_id,
                            'datos'             => $datosScores,
                            'prospect_id'       => $prospecto_id,
                            'solic_id'          => $solicitud_id,
                        ];

                        $eventTM[] = ['event'=> 'PasaBC', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                        $mnsj_str['eventTM'] = $eventTM;

                        return response()->json($mnsj_str);

                    // Si no pasa el corte no califica.
                    } else {

                        if ($pasaSinHistorial !== null) {
                            $regla_string_failed = 'Bc Score resultó -009, rechazado por porcentaje: '.($pasaSinHistorial*100);
                        }

                        $mnsj_str = [
                            'success'           => false,
                            'stat'              => 'No Califica',
                            'message'           => $regla_string_failed,
                        ];
                        // Actualizando el campo ult_punto_reg de la solicitud
                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'Check Bc Score Normal NC_S',
                            0,
                            $regla_string_failed
                        );
                        // Actualizando el campo ult_mensj_a_usuario de la solicitud
                        $this->solicitudRepository->setUltMnsjUsr(
                            $solicitud,
                            json_encode($mnsj_str)
                        );
                        $solicitud->sub_status = 'Check Bc Score Normal NC_S';
                        $solicitud->save();

                        $eventTM[] = ['event'=> 'RechazadoBC', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];

                        Cookie::queue(Cookie::forget('producto'));
                        Cookie::queue(Cookie::forget('logo'));
                        Cookie::queue(Cookie::forget('checa_calificas'));

                        $modal = view("modals.modalNoCalifica")->render();
                        $mnsj_str['modal'] = $modal;
                        $mnsj_str['eventTM'] = $eventTM;
                        session()->flash('nueva_solicitud', true);
                        return response()->json($mnsj_str);

                    }

                // Verificando si el BC SCORE contiene un error
                } elseif (isset($datosScores['BC SCORE']['micro_codigo_error'])) {

                    $this->guardaScores($datosScores, $prospecto_id, $solicitud_id, true);

                    $bc_error = $datosScores['BC SCORE']['micro_codigo_error'];
                    $bc_error = $bc_error.': '.__('descripciones_bc.error_score_codes.'.$bc_error);

                    $mnsj_str = [
                        'stat'      => 'No Encontrado',
                        'success'   => false,
                        'message'   => 'Error BC primera llamada. Error BC SCORE:'.$bc_error
                    ];

                    $solicitud->encontrado = 0;
                    $solicitud->sub_status = 'Primera Llamada';
                    $solicitud->save();

                    // Actualizando el ult_mensj_a_usuario de la solicitud
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    // Actualizando el ult_punto_reg de la solicitud
                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        'Consulta BC',
                        'Primera Llamada',
                        0,
                        'Se recibió una error en la primera llamada a buró de crédito.',
                        ['error' => $mnsj_str]
                    );

                    $eventTM[] = ['event'=> 'PasaBC', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                    session()->flash('nueva_solicitud', true);
                    $modal = view("modals.modalErrorBC")->render();
                    return response()->json([
                        'success'   => false,
                        'stat'      => 'No Encontrado',
                        'message'   => $mnsj_str,
                        'modal'     => $modal,
                        'eventTM'   => $eventTM
                    ]);

                } else {

                    $mnsj_str = [
                        'stat'      => 'No Encontrado',
                        'success'   => false,
                        'message'   => 'Error BC primera llamada: No existen BC SCORE y MICRO SCORE'
                    ];

                    $solicitud->encontrado = 0;
                    $solicitud->sub_status = 'Primera Llamada';
                    $solicitud->save();

                    // Actualizando el ult_mensj_a_usuario de la solicitud
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        'Consulta BC',
                        'Primera Llamada',
                        0,
                        'Se recibió una error en la primera llamada a buró de crédito. No existen BC SCORE y MICRO SCORE',
                        ['error' => $mnsj_str]
                    );

                    $eventTM[] = ['event'=> 'PasaBC', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
                    session()->flash('nueva_solicitud', true);
                    $modal = view("modals.modalErrorBC")->render();
                    $mnsj_str['modal'] = $modal;
                    $mnsj_str['eventTM'] = $eventTM;
                    return response()->json($mnsj_str);

                }

            }

        } else {

            $mnsj_str = [
                'stat'      => 'Error BC No Response',
                'success'   => false,
                'message'   => 'Error al obtener respuesta por parte de buró de crédito.',
                'seconds'   => 9,
            ];

            $solicitud->sub_status = 'Primera Llamada';
            $solicitud->encontrado = 0;
            $solicitud->save();
            // Actualizando el campo ult_mensj_a_usuario de la solicitud
            $this->solicitudRepository->setUltMnsjUsr(
                $solicitud,
                json_encode($mnsj_str)
            );
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                'Consulta BC',
                'Primera Llamada',
                0,
                'Error al obtener respuesta por parte de buró de crédito.',
                ['error' => $mnsj_str]
            );

            return response()->json($mnsj_str);

        }

    }

    /**
     * Realiza la segunda llamada a buro de crédito, la procesa y verifica que
     * el BC Score y Micro Score pasen el punto de corte.
     *
     * @param  Request $request Datos enviados del formulario
     *
     * @return json             Resultado de realizar y procesar la primera llamada
     * a buró.
     */
    public function segundaLlamada(Request $request) {

        $claveProducto = env('CVE_PRODUCTO_LLAMADA_2');
        $INTL_USER = env('INTL2_USER');
        $INTL_PASS = env('INTL2_PASS');
        $INTL_SERVER = env('INTL_SERVER');
        $INTL_PORT = env('INTL_PORT');

        $prospecto_id = $request->input("prospecto_id");
        $solicitud_id = $request->input("solicitud_id");
        $es_elegible = $request->input("elegible");
        $seconds = 6;
        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::find($solicitud_id);

        // Generando la cadena de consulta a la segunda llamada
        $cadenaSegundaLLamadaBC = $this->makeRequestBC(
            'Segunda',
            $claveProducto,
            $INTL_USER,
            $INTL_PASS,
            $INTL_SERVER,
            $INTL_PORT,
            $prospecto_id,
            $solicitud_id
        );

        if (isset($cadenaPrimeraLLamadaBC['maximosReintentos'])) {

            $mnsj_str = [
                'stat'              => 'Error BC',
                'success'           => false,
                'message'           => 'Máximo de intentos de consulta a buró.',
                'maximosReintentos' => true
            ];

            $solicitud->sub_status = 'Segunda Llamada';
            $solicitud->save();
            // Actualizando el ult_mensj_a_usuario de la solicitud
            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            session()->flash('nueva_solicitud', true);
            return response()->json($mnsj_str);
        }

        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            'Segunda Llamada Cadena',
            1,
            'Se generó la cadena para consulta Reporte Completo con éxito.'
        );

        // Realizando la consulta a buró de crédito
        $respuestaTelnet = $this->telnetBC('Segunda', $cadenaSegundaLLamadaBC, $solicitud->rfc, $INTL_SERVER, $INTL_PORT, $seconds);

        if ($respuestaTelnet['success'] == true) {

            $respuestaString = $respuestaTelnet['content'];
            $procesaRespuestaBC =  $this->procesaRespuestaBC('Segunda', $respuestaString);

            if (isset($procesaRespuestaBC['ERRR'])) {

                $consultaBuro_id = $respuestaTelnet['consultaBuro_id'];
                $consultaBuro = ConsultasBuro::find($consultaBuro_id);

                $consultaBuro->ERRR = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ERRR', []));
                $consultaBuro->ES = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ES', []));
                $consultaBuro->save();

                $segmentoERRR = $procesaRespuestaBC['ERRR'];
                $analizaERRR = $this->analizaSegmento($segmentoERRR, 'Respuesta', 'ERRR');

                $mnsj_str = [];
                $mnsj_str['stat'] = 'Error BC';
                foreach ($analizaERRR as $key => $val) {
                    $mnsj_str[$key] = $val;
                }

                $solicitud->sub_status = 'Segunda Llamada';
                $solicitud->save();
                // Actualizando el ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Segunda Llamada',
                    0,
                    'Se recibió un error en la segunda llamada a buró de crédito.',
                    ['error' => $mnsj_str]
                );

                return response()->json([
                    'success'   => false,
                    'stat'      => 'No Encontrado',
                    'message'   => 'Se recibió una respuesta erronea de la segunda consulta a BC.'
                ]);

    		} else {

                $solicitud->sub_status = 'Segunda Llamada';
                $solicitud->save();
                //update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Segunda Llamada',
                    1,
                    'Se recibió una respuesta de la segunda consulta a BC.'
                );

                $consultaBuro_id = $respuestaTelnet['consultaBuro_id'];
                $consultaBuro = ConsultasBuro::find($consultaBuro_id);

                // INTL
                $segmentoINTL = $procesaRespuestaBC['INTL'];
                $analizaINTL = $this->analizaSegmento($segmentoINTL, 'Respuesta', 'INTL');
                $consultaBuro->INTL = implode(PHP_EOL, array_get($procesaRespuestaBC, 'INTL', []));

                // Datos del Usuario
                $segmentoPN = $procesaRespuestaBC['PN'];
                $analizaPN = $this->analizaSegmento($segmentoPN, 'Respuesta', 'PN');
                // Guardando el campo CURP
                $nacionalidad = null;
                $paisNacimiento = null;
                if (isset($analizaPN[0]['curp'])) {
                    $curp = $analizaPN[0]['curp'];
                    $entidadNacimiento = substr($curp, 11, 2);
                    if ($entidadNacimiento != 'NE') {
                        $nacionalidad = 'MX';
                        $paisNacimiento = 'MX';
                    }
                    $solicitud->curp = $curp;
                    $solicitud->nacionalidad= $nacionalidad;
                    $solicitud->pais_nacimiento = $paisNacimiento;
                }
                $this->guardaDatosPersonales($analizaPN, $prospecto_id, $solicitud_id, $nacionalidad, $paisNacimiento);
                $consultaBuro->PN = implode(PHP_EOL, array_get($procesaRespuestaBC, 'PN', []));

                // Direcciones
                if (isset($procesaRespuestaBC['PA'])) {
                    $segmentoPA = $procesaRespuestaBC['PA'];
                    $analizaPA = $this->analizaSegmento($segmentoPA, 'Respuesta', 'PA');
                    $this->guardaDirecciones($analizaPA, $prospecto_id, $solicitud_id);
                    $consultaBuro->PA = implode(PHP_EOL, array_get($procesaRespuestaBC, 'PA', []));
                }

                // Direcciones Empleo
                if (isset($procesaRespuestaBC['PE'])) {
                    $segmentoPE = $procesaRespuestaBC['PE'];
                    $analizaPE = $this->analizaSegmento($segmentoPE, 'Respuesta', 'PE');
                    $this->guardaEmpleos($analizaPE, $prospecto_id, $solicitud_id);
                    $consultaBuro->PE = implode(PHP_EOL, array_get($procesaRespuestaBC, 'PE', []));
                }

                // Cuentas
                if (isset($procesaRespuestaBC['TL'])) {
                    $segmentoTL = $procesaRespuestaBC['TL'];
                    $analizaTL = $this->analizaSegmento($segmentoTL, 'Respuesta', 'TL');
                    $this->guardaCuentas($analizaTL, $prospecto_id, $solicitud_id);
                    $this->guardaCuentasAnterior($analizaTL, $prospecto_id, $solicitud_id);
                    $consultaBuro->TL = implode(PHP_EOL, array_get($procesaRespuestaBC, 'TL', []));
                }

                // Consultas
                $segmentoIQ = $procesaRespuestaBC['IQ'];
                $analizaIQ = $this->analizaSegmento($segmentoIQ, 'Respuesta', 'IQ');
                $this->guardaConsultas($analizaIQ, $prospecto_id, $solicitud_id);
                $this->guardaConsultasAnterior($analizaIQ, $prospecto_id, $solicitud_id);
                $consultaBuro->IQ = implode(PHP_EOL, array_get($procesaRespuestaBC, 'IQ', []));

                // HI | HR
                $analizaHR = [];
                $analizaHI = [];
                if (isset($procesaRespuestaBC['HI'])) {
                    $segmentoHI = $procesaRespuestaBC['HI'];
                    $analizaHI = $this->analizaSegmento($segmentoHI, 'Respuesta', 'HI');
                    $consultaBuro->HI = implode(PHP_EOL, array_get($procesaRespuestaBC, 'HI', []));
                }
                if (isset($procesaRespuestaBC['HR'])) {
                    $segmentoHR = $procesaRespuestaBC['HR'];
                    $analizaHR = $this->analizaSegmento($segmentoHR, 'Respuesta', 'HR');
                    $consultaBuro->HR = implode(PHP_EOL, array_get($procesaRespuestaBC, 'HR', []));
                }
                $datosHawk = [];
                $datosHawk = array_merge($datosHawk, $analizaHI);
                $datosHawk = array_merge($datosHawk, $analizaHR);
                $this->guardaHawk($datosHawk, $prospecto_id, $solicitud_id);

                // Resumen
                $segmentoRS = $procesaRespuestaBC['RS'];
                $analizaRS = $this->analizaSegmento($segmentoRS, 'Respuesta', 'RS');
                $this->guardaResumen($analizaRS, $prospecto_id, $solicitud_id);
                $consultaBuro->RS = implode(PHP_EOL, array_get($procesaRespuestaBC, 'RS', []));

                // Cierre
                $segmentoES = $procesaRespuestaBC['ES'];
                $analizaES = $this->analizaSegmento($segmentoES, 'Respuesta', 'ES');
                $consultaBuro->ES = implode(PHP_EOL, array_get($procesaRespuestaBC, 'ES', []));

                if (isset($analizaES[0]['cierre_num_control'])) {
                    $consultaBuro->folio_consulta = $analizaES[0]['cierre_num_control'];
                }
                $consultaBuro->save();

                $solicitud->status = 'Reporte Completo';
                $solicitud->sub_status = 'Reporte Completo';
                $solicitud->save();
                // update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Reporte Completo',
                    1,
                    'Se recibió el reporte completo sin errores.'
                );
                // add the report values to the HAWK RESPONSE ARRAY
                $HawkResponse_arr['calculos'] = null;

                $stat = 'Segunda Llamada BC OK';
                if ($es_elegible == 1) {
                    $stat = 'Segunda Llamada BC OK Elegible';
                }

                return response()->json([
                    'success'       => true,
                    'stat'          => $stat,
                    'prospecto_id'  => $prospecto_id,
                    'solicitud_id'  => $solicitud_id,
                ]);

            }

        } else {

            $mnsj_str = [
                'stat'      => 'Error BC',
                'success'   => false,
                'message'   => 'Error al obtener respuesta por parte de buró de crédito.'
            ];

            $solicitud->sub_status = 'Segunda Llamada';
            $solicitud->save();
            // Actualizando el ult_mensj_a_usuario de la solicitud

            // Actualizando el ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Segunda Llamada',
                0,
                'Error al obtener respuesta por parte de buró de crédito.',
                ['error' => $mnsj_str]
            );

            return response()->json($mnsj_str);

        }

    }

    /**
     * Construye la cadena de consulta para buro de crédito
     *
     * @param  string $orden         Primera o Segunda llamada
     * @param  string $claveProducto Clave del producto a consultar en buro de crédito
     * @param  string $INTL_USER     Usuario para autenticar en buro de crédito
     * @param  string $INTL_PASS     Contraseña del usuario para autenticar en buro de crédito
     * @param  string $INTL_SERVER   Servidor de buro de crédito
     * @param  string $INTL_PORT     Puerto del servidor de buro de crédito
     *
     * @return array                 Resultado de generar la cadena de consulta
     */
    public function makeRequestBC($orden, $claveProducto, $INTL_USER, $INTL_PASS, $INTL_SERVER, $INTL_PORT, $prospecto_id, $solicitud_id) {

        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::with('domicilio', 'producto')->find($solicitud_id);
        $domicilio_solicitud  = DomicilioSolicitud::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->get();

        if ($orden == 'Primera') {
            // Cambiando al nuevo status
            $solicitud->status = 'Consulta BC';
        } else {
            $solicitud->status = 'Hit BC';
        }

        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            $orden.' Llamada Inicio',
            1,
            'Se inició la '.$orden.' consulta a BC'
        );
        $solicitud->sub_status = $orden.' Llamada Inicio';
        $solicitud->save();

        // Creando el número de referencia de la solicitud (hasta 25 caracteres)
        $extraReferencia = '';
        if ($orden == 'Segunda') {
            $extraReferencia = 'FR';
        }
        $ref_id = str_pad($extraReferencia.$solicitud_id, 25, " ", STR_PAD_LEFT);
        // Creando la cadena de llamado a Buro
        $bcs_string = '';

        // =========== SEGMENTO AUTENTICADOR =====================
        // AU - TIPO DE REPORTE (value always needs to be RCN)
        $seg_aut = 'AU03RCN';
        // 00 - TIPO DE SALIDA (value always needs to be 1 for this report)
        $seg_aut .= '00011';
        // 01 - REFERENCIA DEL OPERADOR (25 chars padded with spaces on left)
        $seg_aut .= '0125'.$ref_id;
        // 02 - CUENTA CON TARJETA DE CRÉDITO
        $solic_credito_tdc = ($solicitud->credito_bancario == 1) ? 'V' : 'F';
        $seg_aut .= '0201'.$solic_credito_tdc;
        // 04 - ÚLTIMOS CUATRO DÍGITOS DE LA TARJETA
        if ($solic_credito_tdc == 'V') {
            $seg_aut .= '0404'. $solicitud->ultimos_4_digitos;
        }
        //07 - HA EJERCIDO UN CRÉDITO HIPOTECARIO
        $solic_credito_hipotecario = ($solicitud->credito_hipotecario == 1) ? 'V' : 'F';
        $seg_aut .= '0701'.$solic_credito_hipotecario;
        //11 - HA EJERCIDO UN CRÉDITO AUTOMOTRIZ EN LOS ÚLTIMOS 24 MESES
        $solic_credito_automotriz = ($solicitud->credito_automotriz == 1) ? 'V' : 'F';
        $seg_aut .= '1101'.$solic_credito_automotriz;

        //=========== SEGMENTO ENCABEZADO =======================
        // INTL - ETIQUETA DEL SEGMENTO
        $seg_intl = 'INTL';
        // INTL - VERSION
        $seg_intl .= '13';
        // REFERENCIA INTERNA
        $seg_intl .= $ref_id;
        // CLAVE DEL PRODUCTO
        $seg_intl .= $claveProducto;
        // CLAVE DEL PAÍS
        $seg_intl .= 'MX';
        // RESERVADO
        $seg_intl .= '0000';
        // CLAVE DEL USUARIO O MEMBER CODE
        $seg_intl .= $INTL_USER;
        // CONTRASEÑA O PASSWORD DE ACCESO
        $seg_intl .= $INTL_PASS;
        // TIPO DE RESPONSABILIDAD
        $seg_intl .= 'I';
        // TIPO DE CONTRATO O PRODUCTO
        $seg_intl .= 'PL'; // (Prestamo Personal)
        // MONEDA DEL CRÉDITO
        $seg_intl .= 'MX';
        // IMPORTE DEL CONTRATO
        $seg_intl .= '000000000';
        // IDIOMA
        $seg_intl .= 'SP';
        // TIPO DE SALIDA
        $seg_intl .= '01';
        // TAMAÑO DEL BLOQUE DEL REGISTRO DE RESPUESTA
        $seg_intl .= ' ';
        // IDENTIFICACIÓN DE LA IMPRESORA
        $seg_intl .= '    '; //4 blancos o espacios
        // RESERVADO PARA USO FUTURO
        $seg_intl .= '0000000';

        // =========== SEGMENTO NOMBRE ===========================
        // PN - APELLIDO PATERNO
        $apellido_p_str = $this->cleanStr($prospecto->apellido_paterno);
        $apellido_p_str = self::cleanStrBC($apellido_p_str);
        $apellido_p_len = str_pad(strlen($apellido_p_str), 2, "0", STR_PAD_LEFT);
        $seg_nom = 'PN'.$apellido_p_len.$apellido_p_str;
        // 00 - APELLIDO MATERNO
        $apellido_m_str = $this->cleanStr($prospecto->apellido_materno);
        $apellido_m_str = self::cleanStrBC($apellido_m_str);
        $apellido_m_len = str_pad(strlen($apellido_m_str), 2, "0", STR_PAD_LEFT);
        $seg_nom .= '00'.$apellido_m_len.$apellido_m_str;
        // 02 - PRIMER NOMBRE
        // split the nombre to see if they have a second name
        $nombre_arr = explode(' ', trim($prospecto->nombres), 2);
        $primer_nombre_str = $this->cleanStr($nombre_arr[0]);
        $primer_nombre_str = self::cleanStrBC($primer_nombre_str);
        $primer_nombre_length = str_pad(strlen($primer_nombre_str), 2, "0", STR_PAD_LEFT);
        $seg_nom .= '02'.$primer_nombre_length.$primer_nombre_str;
        // check if we have a second name
        if (count($nombre_arr) > 1) {
            //use any remaining words for the second name
            $segundo_nombre = $this->cleanStr($nombre_arr[1]);
            $segundo_nombre = self::cleanStrBC($segundo_nombre);
            $segundo_nombre_length = str_pad(strlen($segundo_nombre), 2, "0", STR_PAD_LEFT);
            //03 - SEGUNDO NOMBRE
            $seg_nom .= '03'.$segundo_nombre_length.$segundo_nombre;
        }
        // 04 - FECHA DE NACIMIENTO
        if ($solicitud->fecha_nacimiento) {
            $fecha_nacimiento =  Carbon::createFromFormat('Y-m-d', $solicitud->fecha_nacimiento);
            $seg_nom .= '0408'.$fecha_nacimiento->format('dmY');
        }
        // 05 - RFC
        $rfc_str = $this->cleanStr($solicitud->rfc);
        $rfc_length = str_pad(strlen($rfc_str), 2, "0", STR_PAD_LEFT);
        $seg_nom .= '05'.$rfc_length.$rfc_str;
        // 11 - ESTADO CIVIL
        if ($solicitud->estado_civil) {
            $estado_civil_str = $this->cleanStr($solicitud->estado_civil);
            $seg_nom .= '1101'.$estado_civil_str;
        }
        // 12 - GÉNERO
        if ($solicitud->sexo) {
            $sexo_str = $this->cleanStr($solicitud->sexo);
            $seg_nom .= '1201'.$sexo_str;
        }
        // 15 - CURP
        if ($solicitud->curp) {
            $curp_str = $this->cleanStr($solicitud->curp);
            $curp_length = str_pad(strlen($curp_str), 2, "0", STR_PAD_LEFT);
            $seg_nom .= '15'.$curp_length.$curp_str;
        }

        if (count($domicilio_solicitud) == 1) {
            //=========== SEGMENTO DIRECCION ========================
            // PA - PRIMER LÍNEA DE DIRECCIÓN
            $street = self::cleanStr($domicilio_solicitud[0]->calle);
            if ($domicilio_solicitud[0]->num_exterior == '') {
                $street .= ' SN';
            } else {
                $street .= ' '.self::cleanStr($domicilio_solicitud[0]->num_exterior);
            }
            if ($domicilio_solicitud[0]->num_interior != '') {
                $street .= ' '.self::cleanStr($domicilio_solicitud[0]->num_interior);
            }
            $street = self::cleanStrBC($street);
            if (strlen($street) >= 40) {
                $primeraLinea = substr($street, 0, 40);
                $seg_dir = 'PA40'.$primeraLinea;
                $segundaLinea = substr($street, 41, 80);
                $street_length = str_pad(strlen($segundaLinea), 2, "0", STR_PAD_LEFT);
                $seg_dir .= '00'.$street_length.$segundaLinea;
            } else {
                $street_length = str_pad(strlen($street), 2, "0", STR_PAD_LEFT);
                $seg_dir = 'PA'.$street_length.$street;
            }

            //01 - COLONIA O POBLACIÓN
            $colonia_str = self::cleanStr($domicilio_solicitud[0]->colonia);
            $colonia_str = self::cleanStrBC($colonia_str);
            if (strlen($colonia_str) >= 40) {
                $colonia_str = substr($colonia_str, 0, 40);
                $colonia_length = 40;
            } else {
                $colonia_length = str_pad(strlen($colonia_str), 2, "0", STR_PAD_LEFT);
            }
            $seg_dir .= '01'.$colonia_length.$colonia_str;
            //02 - DELEGACIÓN O MUNICIPIO
            if ($domicilio_solicitud[0]->delegacion != '') {
                $deleg_str = self::cleanStr($domicilio_solicitud[0]->delegacion);
                $deleg_length = str_pad(strlen($deleg_str), 2, "0", STR_PAD_LEFT);
                $seg_dir .= '02'.$deleg_length.$deleg_str;
            }
            //03 - CIUDAD
            if ($domicilio_solicitud[0]->ciudad != '') {
                $ciudad_str = self::cleanStr($domicilio_solicitud[0]->ciudad);
                $ciudad_length = str_pad(strlen($ciudad_str), 2, "0", STR_PAD_LEFT);
                $seg_dir .= '03'.$ciudad_length.$ciudad_str;
            }
            //04 - ESTADO
            $estado_str = self::cleanStr($domicilio_solicitud[0]->codigo_estado);
            $estado_length = str_pad(strlen($estado_str), 2, "0", STR_PAD_LEFT);
            $seg_dir .= '04'.$estado_length.$estado_str;
            //05 - CÓDIGO POSTAL
            $seg_dir .= '0505'.str_pad($domicilio_solicitud[0]->cp, 5, "0", STR_PAD_LEFT);
        }
        //07 - NÚMERO DE TELÉFONO
        if ($solicitud->tel_casa != '') {
            $tel_casa_length = str_pad(strlen($solicitud->tel_casa), 2, "0", STR_PAD_LEFT);
            if ($solicitud->tel_casa != '') {
                $seg_dir .= '07'.$tel_casa_length.$solicitud->tel_casa;
            }
        }
        //13 - ORIGEN DEL DOMICILIO
        $seg_dir .= '1302MX';

        //=========== SEGMENTO DE CIERRE ========================
        //ES - LONGITUD DEL REGISTRO
        $whole_length = str_pad((strlen($seg_aut.$seg_intl.$seg_nom.$seg_dir) + 9), 5, "0", STR_PAD_LEFT);
        $seg_cier = 'ES05'.$whole_length;
        //00 - MARCA DE FIN
        $seg_cier .= '00021**';

        //join all segments to form the final string
        $bcs_string = $seg_aut.$seg_intl.$seg_nom.$seg_dir.$seg_cier;

        $llamada = ConsultasBuro::updateOrCreate([
            'prospecto_id'          => $prospecto_id,
            'solicitud_id'          => $solicitud_id,
            'orden'                 => $orden,
            'tipo'                  => 'Llamada',
        ], [
            'cadena_original'       => $bcs_string,
        ]);

        $llamada->fecha_consulta =  Carbon::now();
        $llamada->no_consulta = $llamada->no_consulta + 1;


        if ($llamada->no_consulta <= 3) {
            $llamada->save();
            return [
                'prospecto_id'  => $prospecto_id,
                'solicitud_id'  => $solicitud_id,
                'bc_string'     => $bcs_string,
                'command_str'   => '( echo -n -e "'.$bcs_string.chr(19).'"; ) | nc -v -q 0 '.$INTL_SERVER.' '.$INTL_PORT
            ];
        } else {
            return [
                'maximosReintentos' => true
            ];
        }

    }

    // Ejecuta el script que realiza el llamado a buro de credito
    public function telnetBC($orden, $datosConexion, $rfc = null, $INTL_SERVER, $INTL_PORT, $seconds) {

        if (App::environment(['local', 'desarrollo'])) {
            $consulta = ConsultaBcTest::where('rfc', $rfc)->get();

            if (count($consulta) == 1) {

                if ($orden == 'Primera') {
                    $content = $consulta[0]->primera_llamada;
                } elseif ($orden == 'Segunda') {
                    $content = $consulta[0]->segunda_llamada;
                } else {
                    $content = null;
                }

                $respuesta = ConsultasBuro::updateOrCreate([
                    'prospecto_id'          => $datosConexion['prospecto_id'],
                    'solicitud_id'          => $datosConexion['solicitud_id'],
                    'tipo'                  => 'Respuesta',
                    'orden'                 => $orden,
                ], [
                    'fecha_consulta'        => Carbon::now(),
                    'cadena_original'       => $content,
                ]);

                $respuesta->no_consulta =  $respuesta->no_consulta + 1;
                $respuesta->save();

                return [
                    'success'           => true,
                    'content'           => $content,
                    'consultaBuro_id'   => $respuesta->id
                ];

            } else {
                return [
                    'success' => false,
                    'content' => null,
                    'seconds' => 9,
                ];
            }

        } else {

            try {

                $old_path = getcwd();

                if ($orden == 'Primera') {
                    chdir('../scripts/bc_score');
                } elseif ($orden == 'Segunda') {
                    chdir('../scripts/full_report');
                }

                $new_path = getcwd();
                $nfilename = $datosConexion['prospecto_id'].'_'.$datosConexion['solicitud_id'];
                $scriptfile = fopen($nfilename.".sh", "w") or die("Unable to create file!");
                // add the command to the script file
                $command = '#!/bin/bash
                    '.$datosConexion['command_str'];
                fwrite($scriptfile, $command);
                fclose($scriptfile);
                // add execute permissions to the new file
                chmod($nfilename.".sh", 0775);
                // execute the new file
                $output = shell_exec('./'.$nfilename.'.sh');
                // check response for error and retry as needed
                $error_respuesta = false;
                // check if response was a sistema BC error and is not NO AUTENTICADO
                if (substr($output, 0, 4) == "ERRR" && strrpos($output, "NO AUTENTICADO") === false && !in_array(substr($output, 4, 2), ['AR','UR'])) {
                    $error_respuesta = true;
                } elseif ($output == '') {
                    $error_respuesta = true;
                }
                unlink('./'.$nfilename.'.sh');

            } catch (\Exception $e) {
                $error_respuesta = true;
                $output = '';
            }

            /*
            $segundos = 3;
            $error_respuesta = false;

            try {

                $fp = fsockopen($INTL_SERVER, $INTL_PORT, $errno, $errstr, 60);
                if ($fp) {
                    stream_set_timeout($fp, $segundos);
                    fwrite($fp, $datosConexion['bc_string'].chr(19));
                    $output = stream_get_contents($fp);
                    fclose($fp);
                } else {
                    $error_respuesta = true;
                    $output = "{$errno}: {$errstr}";
                }

                if (substr($output, 0, 4) == "ERRR" && strrpos($output, "NO AUTENTICADO") === false && !in_array(substr($output, 4, 2), ['AR','UR'])) {
                    $error_respuesta = true;
                } elseif ($output == '') {
                    $error_respuesta = true;
                }

            } catch (\Exception $e) {
                $error_respuesta = true;
                $output = '';
            }
            */

            $respuesta = ConsultasBuro::updateOrCreate([
                'prospecto_id'          => $datosConexion['prospecto_id'],
                'solicitud_id'          => $datosConexion['solicitud_id'],
                'tipo'                  => 'Respuesta',
                'orden'                 => $orden,
            ], [
                'fecha_consulta'        => Carbon::now(),
                'cadena_original'       => $output,
            ]);

            $respuesta->no_consulta =  $respuesta->no_consulta + 1;
            $respuesta->save();

            // check if all three attempts retuned an error
            if ($error_respuesta) {
                return [
                    'success'           => false,
                    'content'           => $output,
                    'consultaBuro_id'   => null
                ];
            } else {
                return [
                    'success'           => true,
                    'content'           => $output,
                    'consultaBuro_id'   => $respuesta->id
                ];
            }
        }

    }

    public function procesaRespuestaBC($orden, $dataString) {

   		$inicioString = substr($dataString, 0, 4);
   		$bloques = array();
   		if ($inicioString == 'INTL') {
			$cadenaINTL = substr($dataString, 0, 49);
			$bloques['INTL'][0] = $cadenaINTL;
			$subCadena = substr($dataString, 49);
			$paso = 2;
			$secciones = array('INTL', 'PN', 'PA', 'PE', 'TL', 'IQ', 'RS', 'HI', 'HR', 'CR', 'SC', 'ES');
		} else {
			$subCadena = $dataString;
			$paso = 4;
			$secciones = array('ERRR', 'ES');
		}

		$inicio = 0;
		$tamañoCadena = strlen($subCadena);
		$bloque = '';
		$etiquetaA = '';
		$etiqueta = '';
		$cadenaBloque = '';
		$i = 0;

		while ($inicio <= $tamañoCadena) {
			$etiqueta = substr($subCadena, $inicio, $paso);
			if (in_array($etiqueta, $secciones) || ($bloque == 'ES' && $valor == '**')) {
				$bloque = $etiqueta;
				if ($etiqueta == $etiquetaA) {
					$bloques[$etiquetaA][$i] = $cadenaBloque;
					$i = $i + 1;
				} elseif($etiquetaA != '') {
					$bloques[$etiquetaA][$i] = $cadenaBloque;
					$i = 0;
				}
				$cadenaBloque = '';
				$etiquetaA = $etiqueta;
			}
			if ($paso == 4) {
				$paso = $paso + 2;
				$etiqueta = substr($subCadena, $inicio, $paso);
			}
            if ($bloque == 'CR') {
                // Haciendo una cadena a partir del segmento CR
                $cadenaRestante = substr($subCadena, $inicio);
                // Buscando el segmento de SC en la cadena restante.
                preg_match_all('/SC\d{2}/', $cadenaRestante, $matchSC, PREG_OFFSET_CAPTURE);
                // Buscando el segmento de ES en la cadena restante.
                preg_match_all('/ES\d{2}/', $cadenaRestante, $matchES, PREG_OFFSET_CAPTURE);
                // ** En el indice 0 de las coincidencias es donde se almacena el
                // resultado de la búsqueda.
                if (count($matchSC[0]) > 0) {
                    // Si el segmento SC existe se obtiene la posición de la
                    // primera coincidencia y para obtener la posición final del
                    // segmento CR
                    $finalCR = $matchSC[0][0][1];
                    $cadenaBloque = substr($cadenaRestante, 0, $finalCR);
                    $inicio += $finalCR;
                } elseif (count($matchES[0]) > 0) {
                    // Si el segmento ES existe se obtiene la posición de la
                    // primera coincidencia y para obtener la posición final del
                    // segmento CR
                    $finalCR = $matchES[0][0][1];
                    $cadenaBloque = substr($cadenaRestante, 0, $finalCR);
                    $inicio += $finalCR;
                }

            } else {
    			$inicio = $inicio + $paso;
    			$paso = 2;
    			$tamaño = substr($subCadena, $inicio, $paso);
    			$inicio = $inicio + $paso;
    			$paso = intval($tamaño);
    			$valor = substr($subCadena, $inicio, $paso);
    			$cadenaBloque = $cadenaBloque.$etiqueta.$tamaño.$valor;
    			$inicio = $inicio + $paso;
    			$paso = 2;
            }
		}

		if (!isset($bloques['ES'])) {
			$bloques['ERRR'][0] = 'ERRRAR25ERROALPROCESARRESPUESTA  ';
		}

		return $bloques;
	}

    function analizaSegmento($datosSegmento, $tipo = null, $segmento = null) {

        $resultadoSegmento = [];
        if ($segmento == 'INTL') {

            $resultadoSegmento['etiqueta'] = substr($datosSegmento[0], 0, 4);
            $resultadoSegmento['version'] = substr($datosSegmento[0], 4, 2);
            $resultadoSegmento['referencia'] = substr($datosSegmento[0], 6, 25);
            $resultadoSegmento['clave_pais'] = substr($datosSegmento[0], 31, 2);
            $resultadoSegmento['reservado4'] = substr($datosSegmento[0], 33, 4);
            $resultadoSegmento['member_code'] = substr($datosSegmento[0], 37, 10);
            $resultadoSegmento['clave_respuesta'] = substr($datosSegmento[0], 47, 1);
            $resultadoSegmento['reservado1'] = substr($datosSegmento[0], 48, 1);

        } else {

            if ($segmento == null) {
                $tamaño_etiqueta = 2;
                $etiquetas = Etiquetas::where('tipo', $tipo)
                    ->get();
            } elseif ($segmento == 'ERRR') {
                $tipo_error = substr($datosSegmento[0], 4, 2);
                $tamaño_etiqueta = 4;
                $etiquetas = Etiquetas::where('tipo', $tipo)
                    ->where('segmento', $tipo_error )
                    ->get();
            } else {
                $tamaño_etiqueta = 2;
                $etiquetas = Etiquetas::where('tipo', $tipo)
                    ->where('segmento', $segmento)
                    ->get();
            }

            $etiquetas = $etiquetas->values();
            foreach ($datosSegmento as $key => $segmento) {

                $largo = strlen($segmento);
                $inicio = 0;
                $j = 0;

                while ($inicio < $largo) {

                    $paso = 4;
                    $etiqueta = substr($segmento, $inicio, $tamaño_etiqueta);
                    if ($etiqueta == 'ERRR') {
                        $valor = substr($segmento, $inicio + $tamaño_etiqueta, 2);
                        $inicio = $inicio + $paso;
                    } else {
                        $caracteres = intval(substr($segmento, $inicio + $tamaño_etiqueta, 2));
                        $inicio = $inicio + $paso;
                        $paso = $caracteres;
                        $valor = substr($segmento, $inicio, $paso);
                        $inicio = $inicio + $paso;
                    }

                    if ($etiqueta == 'HR') {
                        $resultadoSegmento[$key]['hawk_tipo'] = 'respuesta';
                    }
                    if ($etiqueta == 'HI') {
                        $resultadoSegmento[$key]['hawk_tipo'] = 'solicitud';
                    }

                    $resultado = $etiquetas->where('etiqueta', $etiqueta)
                        ->values();

                    if (count($resultado) == 1) {
                        $descripcion =  $resultado[0]->descripcion;
                        $resultadoSegmento[$key][$descripcion] = $valor;
                    } else {
                        $resultadoSegmento[$key][$etiqueta] = $valor;
                    }

                    $tamaño_etiqueta = 2;
                }
            }
        }

        return $resultadoSegmento;
    }

    public function getNombreScore($val) {
        $codigoScore = [
            '004' => 'SCORE BURO MICRO', // ICC SCORE
            '007' => 'BC SCORE',
            '009' => 'SCORE BURO MICRO',
        ];
        if (isset($codigoScore[$val])){
            return $codigoScore[$val];
        } else {
            return '';
        }
    }

    public function guardaScores($datosScores, $prospecto_id, $solicitud_id, $error) {

        if ($error == false) {

            $bc_score = array_get($datosScores, 'BC SCORE.micro_valor', null);
            $bc_razon1 = array_get($datosScores, 'BC SCORE.micro_razon1', null);
            $bc_razon2 = array_get($datosScores, 'BC SCORE.micro_razon2', null);
            $bc_razon3 = array_get($datosScores, 'BC SCORE.micro_razon3', null);
            $micro_valor = array_get($datosScores, 'SCORE BURO MICRO.micro_valor', null);
            $micro_razon1 = array_get($datosScores, 'SCORE BURO MICRO.micro_razon1', null);
            $micro_razon2 = array_get($datosScores, 'SCORE BURO MICRO.micro_razon2', null);
            $micro_razon3 = array_get($datosScores, 'SCORE BURO MICRO.micro_razon3', null);

            $codigo_exclusion = ($bc_score < 0) ? $bc_score .': '. __('descripciones_bc.exclusion_codes.'.$bc_score) : 'none';
            $bc_razon1 = ($bc_razon1) != null ? $bc_razon1.': '.__('descripciones_bc.bc_reason_codes.'.$bc_razon1) : 'none';
            $bc_razon2 = ($bc_razon2) != null ? $bc_razon2.': '.__('descripciones_bc.bc_reason_codes.'.$bc_razon2) : 'none';
            $bc_razon3  = ($bc_razon3) != null ? $bc_razon3.': '.__('descripciones_bc.bc_reason_codes.'.$bc_razon3) : 'none';
            $micro_razon1 = ($micro_razon1) != null ? $micro_razon1.': '.__('descripciones_bc.micro_reason_codes.'.$micro_razon1) : 'none';
            $micro_razon2 = ($micro_razon2) != null ? $micro_razon2.': '.__('descripciones_bc.micro_reason_codes.'.$micro_razon2) : 'none';
            $micro_razon3 = ($micro_razon3) != null ? $micro_razon3.': '.__('descripciones_bc.micro_reason_codes.'.$micro_razon3) : 'none';

            BCScores::updateOrCreate([
                'prospecto_id'      => $prospecto_id,
                'solicitud_id'      => $solicitud_id,
            ], [
                'encontrado'    => 1,
                'bc_score'      => $bc_score,
                'exclusion'     => $codigo_exclusion,
                'bc_razon1'     => $bc_razon1,
                'bc_razon2'     => $bc_razon2,
                'bc_razon3'     => $bc_razon3,
                'micro_valor'   => $micro_valor,
                'micro_razon1'  => $micro_razon1,
                'micro_razon2'  => $micro_razon2,
                'micro_razon3'  => $micro_razon3,
            ]);

        } else {

            $bc_error = $datosScores['BC SCORE']['micro_codigo_error'];
            $bc_error = $bc_error.': '.__('descripciones_bc.error_score_codes.'.$bc_error);

            BCScores::updateOrCreate([
                'prospecto_id'      => $prospecto_id,
                'solicitud_id'      => $solicitud_id,
            ], [
                'encontrado'    => 0,
                'bc_error'      => $bc_error,
            ]);

        }
    }

    public function guardaDatosPersonales($datosPersonales, $prospecto_id, $solicitud_id, $nacionalidad, $paisNacimiento) {

        $datosPersonalesBC = new BCDatosPersonales;
        $campos = $datosPersonalesBC->getFillable();
        $datoPersonalBC = null;
        $array_datosPersonalesBC = null;
        foreach ($campos as $key => $value) {
            $datoPersonalBC[$value] = null;
        }

        BCDatosPersonales::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosPersonales as $key => $datoPersonal) {
            $datoPersonal = $datoPersonal;
            $datoPersonal['prospecto_id'] = $prospecto_id;
            $datoPersonal['solicitud_id'] = $solicitud_id;
            $datoPersonal['nacionalidad_curp'] = $nacionalidad;
            $datoPersonal['pais_nacimiento_curp'] = $paisNacimiento;
            $datoPersonal['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $datoPersonal['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $resultado = array_merge($datoPersonalBC, $datoPersonal);
            $array_datosPersonalesBC[] = $resultado;

        }
        BCDatosPersonales::insert($array_datosPersonalesBC);

    }

    public function guardaDirecciones($datosDirecciones, $prospecto_id, $solicitud_id) {

        $direccionBC = new BCDirecciones;
        $campos = $direccionBC->getFillable();
        $direccionBC = null;
        $array_direccionesBC = null;
        foreach ($campos as $key => $value) {
            $direccionBC[$value] = null;
        }

        BCDirecciones::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosDirecciones as $key => $direccion) {
            $direccion = $direccion;
            $direccion['prospecto_id'] = $prospecto_id;
            $direccion['solicitud_id'] = $solicitud_id;
            $direccion['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $direccion['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $resultado = array_merge($direccionBC, $direccion);
            $array_direccionesBC[] = $resultado;

        }

        BCDirecciones::insert($array_direccionesBC);

    }

    public function guardaEmpleos($datosEmpleos, $prospecto_id, $solicitud_id) {

        $empleoBC = new BCEmpleos;
        $campos = $empleoBC->getFillable();
        $empleoBC = null;
        $array_empleosBC = null;
        foreach ($campos as $key => $value) {
            $empleoBC[$value] = null;
        }

        BCEmpleos::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosEmpleos as $key => $empleo) {
            $empleo['prospecto_id'] = $prospecto_id;
            $empleo['solicitud_id'] = $solicitud_id;
            $empleo['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $empleo['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $resultado = array_merge($empleoBC, $empleo);
            $array_empleosBC[] = $resultado;

        }
        BCEmpleos::insert($array_empleosBC);

    }

    public function guardaCuentas($datosCuentas, $prospecto_id, $solicitud_id) {

        $cuentaBC = new BCCuentas;
        $campos = $cuentaBC->getFillable();
        $cuentaBC = null;
        $array_cuentasBC = null;
        foreach ($campos as $key => $value) {
            $cuentaBC[$value] = null;
        }

        BCCuentas::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosCuentas as $key => $cuenta) {
            $cuenta['prospecto_id'] = $prospecto_id;
            $cuenta['solicitud_id'] = $solicitud_id;
            $cuenta['no_cuenta'] = $key + 1;
            $cuenta['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $cuenta['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            $resultado = array_merge($cuentaBC, $cuenta);
            $array_cuentasBC[] = $resultado;

        }
        BCCuentas::insert($array_cuentasBC);

    }

    public function guardaConsultas($datosConsultas, $prospecto_id, $solicitud_id) {

        $consultaBC = new BCConsultas;
        $campos = $consultaBC->getFillable();
        $consultaBC = null;
        $array_consultasBC = null;
        foreach ($campos as $key => $value) {
            $consultaBC[$value] = null;
        }

        BCConsultas::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosConsultas as $key => $consulta) {
            $consulta['prospecto_id'] = $prospecto_id;
            $consulta['solicitud_id'] = $solicitud_id;
            $consulta['no_consulta'] = $key + 1;
            $consulta['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $consulta['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $resultado = array_merge($consultaBC, $consulta);
            $array_consultasBC[] = $resultado;

        }
        BCConsultas::insert($array_consultasBC);

    }

    public function guardaHawk($datosHawk, $prospecto_id, $solicitud_id) {

        $hawkBC = new BCHawk;
        $campos = $hawkBC->getFillable();
        $hawkBC = null;
        $array_hawksBC = null;
        foreach ($campos as $key => $value) {
            $hawkBC[$value] = null;
        }

        BCHawk::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosHawk as $key => $hawk) {
            $hawk['prospecto_id'] = $prospecto_id;
            $hawk['solicitud_id'] = $solicitud_id;
            $hawk['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $hawk['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            $resultado = array_merge($hawkBC, $hawk);
            $array_hawksBC[] = $resultado;

        }
        if ($array_hawksBC != null) {
            BCHawk::insert($array_hawksBC);
        }

    }

    public function guardaResumen($datosResumen, $prospecto_id, $solicitud_id) {

        $resumenBC = new BCResumenBuro;
        $campos = $resumenBC->getFillable();
        $resumenBC = null;
        $array_resumenBC = null;
        foreach ($campos as $key => $value) {
            $resumenBC[$value] = null;
        }

        BCResumenBuro::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->delete();

        foreach ($datosResumen as $key => $resumen) {
            $resumen = (array) $resumen;
            $resumen['prospecto_id'] = $prospecto_id;
            $resumen['solicitud_id'] = $solicitud_id;
            $resumen['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $resumen['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            $resultado = array_merge($resumenBC, $resumen);
            $array_resuemnBC[] = $resultado;

        }
        BCResumenBuro::insert($array_resuemnBC);

    }

    public function guardaCuentasAnterior($cuentas, $prospecto_id, $solicitud_id) {

        DetalleCuenta::where('ID_PROSPECT', $prospecto_id)
            ->where('ID_SOLIC', $solicitud_id)
            ->delete();

        $detalleCuenta = new DetalleCuenta;
        $campos = $detalleCuenta->getFillable();
        $detalleCuenta = null;
        $array_detalleCuenta = null;
        foreach ($campos as $key => $value) {
            $detalleCuenta[$value] = null;
        }

        foreach ($cuentas as $key => $cuenta) {
            $cuenta = array_change_key_case($cuenta, CASE_UPPER);
            // $cuenta['CUENTA_FRECUENCIA_PAGOS'] = $cuenta['CUENTA_FREGUENCIA_PAGOS'];
            // unset($cuenta['CUENTA_FREGUENCIA_PAGOS']);

            // Verificando si viene el encabezado Cuenta Clave Member Code
            if (isset($cuenta['CUENTA_CLAVE_MEMBER_CODE'])) {
                $cuenta['CUENTA_MEMBER_CODE'] = $cuenta['CUENTA_CLAVE_MEMBER_CODE'];
                unset($cuenta['CUENTA_CLAVE_MEMBER_CODE']);
            }

            $cuenta['ID_PROSPECT'] = $prospecto_id;
            $cuenta['ID_SOLIC'] = $solicitud_id;
            $cuenta['NO_CUENTA'] = $key + 1;
            $cuenta['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $cuenta['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            $resultado = array_merge($detalleCuenta, $cuenta);
            $array_detalleCuenta[] = $resultado;
        }
        DetalleCuenta::insert($array_detalleCuenta);

    }

    public function guardaConsultasAnterior($consultas, $prospecto_id, $solicitud_id) {

        DetalleConsulta::where('ID_PROSPECT', $prospecto_id)
            ->where('ID_SOLIC', $solicitud_id)
            ->delete();

        $detalleConsulta = new DetalleConsulta;
        $campos = $detalleConsulta->getFillable();
        $detalleConsulta = null;
        $array_detalleConsulta = null;
        foreach ($campos as $key => $value) {
            $detalleConsulta[$value] = null;
        }

        foreach ($consultas as $key => $consulta) {
            $consulta = array_change_key_case($consulta, CASE_UPPER);
            /* if (isset($consulta['CONSULTA_RESEVADO2'])) {
                $consulta['CONSULTA_RESERVADO2'] = $consulta['CONSULTA_RESEVADO2'];
                unset($consulta['CONSULTA_RESEVADO2']);
            } */
            if (isset($consulta['CONSULTA_RESERVADO1'])) {
                unset($consulta['CONSULTA_RESERVADO1']);
            }
            if (isset($consulta['CONSULTA_RESPONSABILIDAD'])) {
                $consulta['CONSULTA_REPONSABILIDAD'] = $consulta['CONSULTA_RESPONSABILIDAD'];
                unset($consulta['CONSULTA_RESPONSABILIDAD']);
            }

            $consulta['ID_PROSPECT'] = $prospecto_id;
            $consulta['ID_SOLIC'] = $solicitud_id;
            $consulta['NO_CONSULTA'] = $key + 1;
            $consulta['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $consulta['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            $resultado = array_merge($detalleConsulta, $consulta);
            $array_detalleConsulta[] = $resultado;
        }
        DetalleConsulta::insert($array_detalleConsulta);

    }

    private function checkCoberturaEdad($solicitud)
    {

        // Obteniendo la edad del prospecto
        $edad = Carbon::createFromFormat('Y-m-d', $solicitud->fecha_nacimiento)->age;
        // Obteniendo la cobertura del domicilio del prospecto
        $cobertura = isset($solicitud->domicilio->cobertura) ?  $solicitud->domicilio->cobertura : 0;
        $estado = isset($solicitud->domicilio->estado) ?  $solicitud->domicilio->estado : '-';
        $delegacion = isset($solicitud->domicilio->delegacion) ?  $solicitud->domicilio->delegacion : '-';
        $reasonA = '';
        $reason = '';

        // Verificando si la edad esta en el rango y el domicilio registrado tiene cobertura
        if (($edad >= $this->edadMinima && $edad <= $this->edadMaxima) && $cobertura == 1) {
            return [
                'success' => true,
                'reason'  => 'Edad y plaza Ok'
            ];
        } else {
            $reason = '';
            if ($edad < $this->edadMinima || $edad > $this->edadMaxima) {
                $reasonA .= 'E';
                $reason .= 'Edad,';
            } elseif ($solicitud->domicilio->cobertura == 0) {
                $reasonA .= 'P';
                $reason .= 'Plaza,';
            }

            return [
                'success'       => false,
                'reason'        => $reason,
                'reasonA'       => $reasonA,
                'edad'          => $edad,
                'estado'        => $estado,
                'delegacion'    => $delegacion,
            ];
        }

    }

    public function datosConsultaBC (Request $request) {
        ini_set('memory_limit','512M');
        $reglas = [
            'solicitud_id'  => 'required|integer',
        ];

        $mensajes = [
            'required'          => 'El parámetro es requerido',
            'integer'           => 'El parámetro debe ser un número entero',
        ];

        $validaciones = Validator::make($request->toArray(), $reglas, $mensajes);

        if ($validaciones->fails()) {
            return response()->json($validaciones->errors(), 400, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
        }

        $solicitud_id = $request->solicitud_id;
        $solicitud = Solicitud::with('prospecto', 'bc_score', 'respuesta_segunda_llamadabc', 'bc_cuentas')
            ->find($solicitud_id);

        if (isset($solicitud->bc_cuentas[0]['solicitud_id'])) {

            $cuentas =  Tb1_CtasModAcep_MR::select('CUENTA_NOMBRE_USUARIO', 'CUENTA_CONTRATO_PRODUCTO',
                'CUENTA_FECHA_APERTURA', 'MesesAntigCuenta', 'Cuenta_MOP_V2', 'MMop6', 'Cuenta_Hist_Pagos',
                'Cuenta_Limite_Credito', 'Sdo_Act2', 'UsoLRev', 'Cuenta_Clave_Observacion', 'cuenta_fecha_cierre',
                'Cuenta_Cred_Max_Aut', 'PagoMensualBuro2')
                ->where('id_solic', $solicitud_id)
                ->where('id_prospect', $solicitud->prospecto->id)
                ->get();

            $bc_cuentas['lineas_abiertas'] = collect($cuentas)->where('cuenta_fecha_cierre', null)
                ->groupBy('CUENTA_NOMBRE_USUARIO')
                ->toArray();


            $bc_cuentas['lineas_cerradas'] = collect($cuentas)->where('cuenta_fecha_cierre', '!=', null)
                ->groupBy('CUENTA_NOMBRE_USUARIO')
                ->toArray();

            return response()->view('crm.buro.reporte_buro_po', ['bc_cuentas' => $bc_cuentas, 'solicitud' => $solicitud]);

        } else {

            return response()->json(['message' => 'La solicitud no cuenta con el reporte completo de buró'], 400);

        }

    }

    public function reporteConsultas (Request $request) {
        ini_set('memory_limit','512M');
        $validaciones = $this->validaFechas($request);

        if ($validaciones->fails()) {
            return response()->json($validaciones->errors(), 400, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
        }

        $fecha_inicio = $request->fecha_inicio.' 00:00:00';
        $fecha_fin = $request->fecha_fin.' 23:59:59';

        $consultas_buro = ConsultasBuro::with('prospecto', 'solicitud')
            ->selectRaw("folio_consulta, DATE_FORMAT(consultas_buro.updated_at, '%Y-%m-%d') as 'fecha',
                DATE_FORMAT(consultas_buro.updated_at, '%H:%i:%s') as 'hora',
                IF(folio_consulta IS NOT NULL, 1, 0) AS 'hit',
                IF(folio_consulta IS NOT NULL AND no_consulta > 1, no_consulta-1, IF(folio_consulta IS NOT NULL AND no_consulta = 1, 0, no_consulta)) AS 'no_hit',
                IF(orden = 'Primera', 'Autenticador', 'Reporte ordinario') AS 'medio',
                'Habilitado' AS 'ind_hab_deshab',
                IF(bc_score >= 560 AND micro_valor >= 600, 'Aprobado', 'Rechazado') AS 'status_ko',
                consultas_buro.prospecto_id,
                consultas_buro.solicitud_id,
                bc_score,
                micro_valor")
            ->leftJoin('bc_scores', 'bc_scores.solicitud_id', 'consultas_buro.solicitud_id')
            ->where('tipo', 'Respuesta')
            ->whereBetween('consultas_buro.updated_at', [$fecha_inicio, $fecha_fin]);

        Excel::create('reporte_consultas', function ($excel) use ($consultas_buro) {
            $excel->sheet('Datos', function ($sheet) use ($consultas_buro) {

                $sheet->setAutoSize(false);
                $sheet->appendRow(array(
                    'Folio Consulta',
                    'RFC',
                    'Tipo Consulta',
                    'Fecha',
                    'Hora',
                    'Medio',
                    'Ind. Hab/Deshab',
                    'Usuario',
                    'Status K.O.',
                    'Nombre Cliente',
                    'BC Score',
                    'External ID',
                    'Micro Score',
                    'Prospecto Id',
                    'Solicitud Id',
                    'Hit',
                    'No Hit'
                ));

                $consultas_buro->chunk(1000, function($rows) use ($sheet) {
                    foreach ($rows as $consulta) {
                        $sheet->appendRow(array(
                            $consulta->folio_consulta,
                            $consulta->solicitud['rfc'],
                            'Buró de Crédito',
                            $consulta->fecha,
                            $consulta->hora,
                            $consulta->medio,
                            $consulta->ind_hab_deshab,
                            ($consulta->medio == 'Autenticador') ? env('INTL1_USER') : env('INTL2_USER'),
                            $consulta->status_ko,
                            trim($consulta->prospecto['nombres'].' '.$consulta->prospecto['apellido_paterno'].' '.$consulta->prospecto['apellido_materno']),
                            $consulta->bc_score,
                            $consulta->solicitud['user_ip'],
                            $consulta->micro_valor,
                            $consulta->prospecto_id,
                            $consulta->solicitud_id,
                            $consulta->hit,
                            $consulta->no_hit,
                        ));
                    }
                });

            });
        })->download('csv');

    }

    public function reporteAutenticados (Request $request) {
        ini_set('memory_limit','512M');
        $validaciones = $this->validaFechas($request);

        if ($validaciones->fails()) {
            return response()->json($validaciones->errors(), 400, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
        }

        $fecha_inicio = $request->fecha_inicio.' 00:00:00';
        $fecha_fin = $request->fecha_fin.' 23:59:59';

        $consultas_buro = ConsultasBuro::with('prospecto', 'autenticacion_solicitud', 'digitos_tdc', 'domicilio_solicitud')
            ->selectRaw("prospecto_id, solicitud_id, folio_consulta as 'folio_consulta',
                DATE_FORMAT(consultas_buro.updated_at, '%Y-%m-%d') as 'Fecha',
                DATE_FORMAT(consultas_buro.updated_at, '%H:%i') as 'Hora'")
            ->where('tipo', 'Respuesta')
            ->whereBetween('updated_at', [$fecha_inicio, $fecha_fin]);

        Excel::create('reporte_autenticados', function ($excel) use ($consultas_buro) {
            $excel->sheet('Datos', function ($sheet) use ($consultas_buro) {

                $sheet->setAutoSize(false);
                $sheet->appendRow(array(
                    'No. Solicitud',
                    'Fecha',
                    'Hora',
                    'Nombre(s)',
                    'Paterno',
                    'Materno',
                    'RFC',
                    'Calle - No.',
                    'Colonia',
                    'Ciudad',
                    'Edo.',
                    'Tarjeta Crédito',
                    'Ultimos 4 dígitos',
                    'Hipotecario',
                    'Automotriz',
                    'Autoriza',
                    'Id Autentica',
                    'No. Control BC'
                ));

                $consultas_buro->chunk(1000, function($rows) use ($sheet) {
                    foreach ($rows as $consulta) {
                        $sheet->appendRow(array(
                            $consulta->solicitud_id,
                            $consulta->Fecha,
                            $consulta->Hora,
                            $consulta->prospecto['nombres'],
                            $consulta->prospecto['apellido_paterno'],
                            $consulta->prospecto['apellido_materno'],
                            $consulta->digitos_tdc['rfc'],
                            $consulta->domicilio_solicitud['calle-no'],
                            $consulta->domicilio_solicitud['colonia'],
                            $consulta->domicilio_solicitud['ciudad'],
                            $consulta->domicilio_solicitud['edo.'],
                            $consulta->autenticacion_solicitud['tarjeta_credito'],
                            $consulta->digitos_tdc['ultimos_4_digitos'],
                            $consulta->autenticacion_solicitud['hipotecario'],
                            $consulta->autenticacion_solicitud['automotriz'],
                            $consulta->autenticacion_solicitud['autoriza'],
                            ($consulta->folio_consulta != '') ? 'S' : 'N',
                            $consulta->folio_consulta,
                        ));
                    }
                });

            });
        })->download('csv');

    }

    public function validaFechas($request) {

        $reglas = [
            'fecha_inicio'  => 'required|date|before_or_equal:fecha_fin',
            'fecha_fin'     => 'required|date|after_or_equal:fecha_inicio',
            'fecha_max'     => 'before_or_equal:fecha_inicio'
        ];

        $mensajes = [
            'required'          => 'El parámetro es requerido',
            'date'              => 'El parámetro no tiene un formato de fecha válido (Y-m-d)',
            'before_or_equal'   => 'El parámetro fecha_inicio debe ser menor o igual a fecha_fin',
            'after_or_equal'    => 'El parámetro fecha_fin debe ser mayor o igual a fecha_inicio'
        ];

        $validaciones = Validator::make($request->toArray(), $reglas, $mensajes);
        if ($validaciones->fails())  {
            return $validaciones;
        } else {

            $fecha_inicio = Carbon::createFromFormat('Y-m-d', $request->fecha_inicio);
            $fechas['fecha_maxima'] = $fecha_inicio->addMonths(2)->format('Y-m-d');
            $fechas['fecha_fin'] = $request->fecha_fin;

            $reglas = [
                'fecha_fin'  => 'before_or_equal:fecha_maxima'
            ];

            $mensajes = [
                'before_or_equal'   => 'El máximo periodo de tiempo para obtener datos es de 2 meses',
            ];

            $validaciones = Validator::make($fechas, $reglas, $mensajes);

            return $validaciones;

        }

    }

    public function updateDataT24 () {

        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout' => 10,
                    'default_socket_timeout' => 10
                ]);
        });

        $response = $soapWrapper->call('T24.CustomerInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company' => env('COMPANY_T24'),
                    'password' => env('PASSWORD_T24'),
                    'userName' => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'CUSTOMERPRSTINPUTWSType' => [
                    'id' => '100152',
                    'gNAME1' => [
                        'NAME1' => 'SARABIAS'
                    ]
                ]
            ]
        ]);

    }

}
