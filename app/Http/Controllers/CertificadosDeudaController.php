<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Validator;
use Auth;
use App\Solicitud;
use App\Repositories\SolicitudRepository;
use Image;
use Illuminate\Support\Facades\Storage;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use App\ClientesAlta;
use App\Jobs\CertificadosDeuda;
use Log;

class CertificadosDeudaController extends Controller
{
    private $solicitudRepository;

    public function __construct()
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

        public function index() {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id);
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            return view('webapp.certificados_deuda', ['pasos_habilitados' => $pasos_habilitados]);
        } else {
            return $siguientePaso;
        }

        }

        public function save(Request $request) {

        ini_set('memory_limit','256M');
        if (Auth::guard('prospecto')->check()) {
            
            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;
            $id = 0;
            
            if ($request->hasFile('file')) {

                $files = $request->file('file');
                $finfo = finfo_open(FILEINFO_MIME); // Devuelve el tipo mime del tipo extensión

                foreach ($files as $key => $file) {

                    $tipo_archivo = finfo_file($finfo, $file);
                    $archivo = explode(";", $tipo_archivo);
                    
                    if ($archivo[0] == "image/icon" || $archivo[0] =="image/jpeg" || $archivo[0]== "image/png" || $archivo[0]== "image/tiff" || $archivo[0]== "image/bpm" ||$archivo[0]== "application/pdf") {

                        if ($file->isValid()) {
                            $id = $id + 1;
                            

                            $extension = $file->getClientMimeType();
                            $comprobante = $file;
                            if (strpos($extension, 'image') !== false) {
                                $extension = 'jpg';
                            } elseif (strpos($extension, 'pdf') !== false) {
                                $extension = 'pdf';
                            }
                            if ($request->tipo_archivo == 'certificados') {
                                $tipo_documento = 'anexo-cd-';
                                $id = str_pad($id, 2, "0", STR_PAD_LEFT);
                            } else {
                                $tipo_documento = 'anexo-solcred';
                                $id = "";
                            }
                            
                            $root = "proceso_simplificado";
                            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}{$id}.jpg")) {
                                Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}{$id}.jpg");
                            }
                            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}{$id}.pdf")) {
                                Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}{$id}.pdf");
                            }
                            Storage::disk('s3')->put("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}{$id}.{$extension}", file_get_contents($comprobante));
                            
                        }
                    } else {
                        return response()->json([
                            'success'           => false,
                            'message'           => 'Archivo no valido',
                            'archivo_invalido'            => true,
                        ]);
                    }

                }
                
                
                if ($request->tipo_archivo == 'certificados') {
                    
                    CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'certificados'   => 1,
                        'numero_certificados_deuda'     => $id
                    ]);
                } else {
                    
                    CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'solicitud'   => 1,
                    ]);
                }

                $resultado = CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->first();

                if ($resultado->certificados == 1 && $resultado->solicitud == 1) {

                    CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'certificados_deuda_completo'   => 1,
                    ]);

                    $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->orderBy('id', 'desc')
                        ->first();

                    $job = (new CertificadosDeuda($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                    dispatch($job);

                    $log = [
                        'success' => true,
                        'msj'     => 'Certificados deuda cargados',
                    ];
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
                    $solicitud->save();

                    $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, false);

                    //$siguientePaso = null;
                    if ($siguientePaso == null) {

                        if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                            $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                        } else {
                            $id_plantilla = 89;
                        }

                        $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                                    ->where('plantilla_id', $id_plantilla)
                                    ->get()
                                    ->toArray();

                        $tituloModal = $plantilla[0]['modal_encabezado'];
                        $imgModal = $plantilla[0]['modal_img'];
                        $cuerpoModal = $plantilla[0]['modal_cuerpo'];

                        $producto = null;
                        if ($solicitud->producto()->exists()) {
                            $producto = $solicitud->producto[0]['alias'];
                            $redireccion = $solicitud->producto[0]['redireccion'];
                        }

                        $faltanDocumentos = $this->solicitudRepository->faltanDocumentos($prospecto_id, $solicitud_id, $producto);
                        if ($faltanDocumentos != null) {
                            $cuerpoModal = str_replace('{{ lista_documentos }}', $faltanDocumentos, $cuerpoModal);
                        } else {
                            $tituloModal = __('messages.titulo_modal_consolida_deudas');
                            $cuerpoModal = __('messages.cuerpo_modal_consolida_deudas');
                        }
                        $modal = view("modals.modalStatusSolicitudDocumentos", [
                            'tituloModal' => $tituloModal,
                            'imgModal' => $imgModal,
                            'cuerpoModal' => $cuerpoModal,
                            'continua' => false,
                            'redireccion' => $redireccion
                        ])->render();

                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'Oferta Aceptada - Carga Documentos Completada',
                            1,
                            'Se ha completado la carga de documentos en el Panel Operativo'
                        );
                        $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                        $solicitud->save();

                        Auth::guard('prospecto')->logout();

                        return response()->json([
                            'success'           => true,
                            'message'           => 'Comprobantes de ingresos guardado exitosamente',
                            'modal'             => $modal,
                        ]);

                    } else {

                        return response()->json([
                            'success'           => true,
                            'message'           => 'Comprobantes de ingresos guardado exitosamente',
                            'siguiente_paso'    => $siguientePaso,
                        ]);

                    }
                } else {

                    return response()->json([
                        'success'           => true,
                        'tipo_archivo'      => $request->tipo_archivo,
                        'carga_completada'  => 0,
                    ]);
                }
                
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'Sesión expirada',
            ]);

        }

        }

        /**
        * Genera el job para procesamiento del alta de Certificados de deuda mediante el
        * botón que esta en la vista Alta de clientes T24
        *
        * @param  Request $request Object con los parámetros que son enviados
        *
        * @return json             Resultado de generar el job
        */
        public function procesar(Request $request) {

        $clienteAlta = ClientesAlta::find($request->idAltaCliente);
        $job = (new CertificadosDeuda($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
        dispatch($job);

        return response()->json(['success' => true]);

        }

        public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

        }

        public function siguientePaso($prospecto_id, $solicitud_id, $redirect = true) {

        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        if ($siguientePaso != 'certificados_deuda' && $redirect == true) {
            return redirect('/webapp/'.$siguientePaso);
        } elseif ($siguientePaso != 'certificados_deuda' && $redirect == false) {
            return $siguientePaso;
        } else {
            return null;
        }

        }
    }
