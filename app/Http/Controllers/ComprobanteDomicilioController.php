<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Validator;
use Auth;
use App\Solicitud;
use App\Repositories\SolicitudRepository;
use Image;
use Illuminate\Support\Facades\Storage;
use App\PlantillaComunicacion;
use App\ClientesAlta;
use App\CargaDocumentos;
use App\Jobs\ComprobantesDomicilio;
use Log;
use App\Repositories\CurlCaller;
use Jenssegers\Agent\Agent;

class ComprobanteDomicilioController extends Controller
{
    private $solicitudRepository;

    public function __construct()
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function index() {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id);
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {

            $agent = new Agent();

            $device = $agent->device();
            $platform = $agent->platform();
            $browser = $agent->browser();

            $response = $this->getLatLonDomicilio($solicitud->domicilio);
            return view('webapp.comprobante_domicilio', [
                'domicilio' => $solicitud->domicilio,
                'latitud' => $response['latitud'],
                'longitud' => $response['longitud'],
                'location_error' => $response['message'],
                'device' => $device,
                'platform' => $platform,
                'browser' => $browser,
                'pasos_habilitados' => $pasos_habilitados
            ]);

        } else {
            return $siguientePaso;
        }

    }

    public function save(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;

            if ($request->hasFile('file')) {

                $files = $request->file('file');
                $finfo = finfo_open(FILEINFO_MIME); // Devuelve el tipo mime del tipo extensión

                foreach ($files as $key => $file) {

                    $tipo_archivo = finfo_file($finfo, $file);
                    $archivo = explode(";", $tipo_archivo);
                    
                    if ($archivo[0] == "image/icon" || $archivo[0] =="image/jpeg" || $archivo[0]== "image/png" || $archivo[0]== "image/tiff" || $archivo[0]== "image/bpm" ||$archivo[0]== "application/pdf") {
                       
                        if ($file->isValid()) {

                            $extension = $file->getClientMimeType();
                            $comprobante = $file;
                            if (strpos($extension, 'image') !== false) {
                                $extension = 'jpg';
                            } elseif (strpos($extension, 'pdf') !== false) {
                                $extension = 'pdf';
                            }

                            $tipo_documento = 'comprobante_domicilio';
                            $detalle_documento = '';
                            if ($key == 0) {
                                $detalle_documento = 'front';
                            } else {
                                $detalle_documento = 'back';
                            }

                            $root = "proceso_simplificado";

                            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.{$extension}")) {
                                Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.{$extension}");
                            }

                            Storage::disk('s3')->put("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.{$extension}", file_get_contents($comprobante));

                        }
                    } else {
                        return response()->json([
                            'success'           => false,
                            'message'           => 'Archivo no valido',
                            'archivo_invalido'            => true,
                        ]);
                    }

                }

                $reverse_address = null;
                $reverse_error = null;
                if ($request->reverse_error != '') {
                    $reverse_error = $request->reverse_error.PHP_EOL;
                }

                if ($request->has('latitud_reverse') && $request->has('longitud_reverse')) {
                    $latitud_reverse = $request->latitud_reverse;
                    $longitud_reverse = $request->longitud_reverse;
                    if ($latitud_reverse != '' && $longitud_reverse != '') {
                        $response = $this->reverseDomicilio($latitud_reverse, $longitud_reverse);
                        if ($response['message'] != null) {
                            $reverse_error .= $response['message'];
                        } else {
                            $reverse_address = $response['reverse_address'];
                        }
                    }
                }

                CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'comprobante_domicilio_completo'    => 1,
                        'latitud'                           => $request->latitud,
                        'longitud'                          => $request->longitud,
                        'location_error'                    => $request->location_error,
                        'latitud_reverse'                   => $request->latitud_reverse,
                        'longitud_reverse'                  => $request->longitud_reverse,
                        'reverse_address'                   => $reverse_address,
                        'reverse_error'                     => $reverse_error,
                        'distancia'                         => $request->distancia,
                        'device'                            => $request->device,
                        'browser'                           => $request->browser,
                        'platform'                          => $request->platform,
                    ]);

                $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $job = (new ComprobantesDomicilio($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                dispatch($job);

                $log = [
                    'success' => true,
                    'msj'     => 'Comprobante de domicilio cargado',
                ];
                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
                $solicitud->save();

                $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, false);
                if ($siguientePaso == null) {

                    if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                        $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                    } else {
                        $id_plantilla = 89;
                    }
                    $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                                ->where('plantilla_id', $id_plantilla)
                                ->get()
                                ->toArray();

                    $tituloModal = $plantilla[0]['modal_encabezado'];
                    $imgModal = $plantilla[0]['modal_img'];
                    $cuerpoModal = $plantilla[0]['modal_cuerpo'];

                    $producto = null;
                    if ($solicitud->producto()->exists()) {
                        $producto = $solicitud->producto[0]['alias'];
                        $redireccion = $solicitud->producto[0]['redireccion'];
                    }

                    $faltanDocumentos = $this->solicitudRepository->faltanDocumentos($prospecto_id, $solicitud_id, $producto);
                    if ($faltanDocumentos != null) {
                        $cuerpoModal = str_replace('{{ lista_documentos }}', $faltanDocumentos, $cuerpoModal);
                    } else {
                        $tituloModal = __('messages.titulo_modal_carga_documentos_terminada');
                        $cuerpoModal = __('messages.cuerpo_modal_carga_documentos_terminada');
                    }

                    $modal = view("modals.modalStatusSolicitudDocumentos", [
                        'tituloModal' => $tituloModal,
                        'imgModal' => $imgModal,
                        'cuerpoModal' => $cuerpoModal,
                        'continua' => false,
                        'redireccion' => $redireccion
                    ])->render();

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Oferta Aceptada - Carga Documentos Completada',
                        1,
                        'Se ha completado la carga de documentos en el Panel Operativo'
                    );
                    $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                    $solicitud->save();

                    Auth::guard('prospecto')->logout();

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Comprobantes de domicilio guardado exitosamente',
                        'modal'             => $modal,
                    ]);

                } else {

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Comprobantes de domicilio guardado exitosamente',
                        'siguiente_paso'    => $siguientePaso,
                    ]);

                }

            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'Sesión expirada',
            ]);

        }

    }

    public function getLatLonDomicilio($domicilio) {

        $curl = new CurlCaller();
        $url = env('GMAPS_URL');
        $key = env('GMAPS_KEY');
        $address = "$domicilio->calle+$domicilio->num_exterior+$domicilio->colonia+$domicilio->delegacion+$domicilio->cp";
        $address = str_replace(' ', '+', $address);

        $response = $curl->getLatLonDomicilio($url, $key, $address);
        $latitud = null;
        $longitud = null;
        $message = null;
        if ($response !== null || $response !== '') {
            $response = json_decode($response);
            if (json_last_error() === JSON_ERROR_NONE) {
                if ($response->status == 'OK') {
                    if (isset($response->results[0]->geometry->location)){
                        $latitud = $response->results[0]->geometry->location->lat;
                        $longitud = $response->results[0]->geometry->location->lng;
                    } else {
                        $message = 'Los valores de latitud/longitud del domicilio registrado en la solicitud no se pudieron obtener: La respuesta no contiene el nodo location';
                    }
                } else {
                    $message = 'Los valores de latitud/longitud del domicilio registrado en la solicitud no se pudieron obtener: Error Servicio';
                }
            } else {
                $message = 'Los valores de latitud/longitud del domicilio registrado en la solicitud no se pudieron obtener: Error al parsear la respuesta a json';
            }
        } else {
            $message = 'Los valores de latitud/longitud del domicilio registrado en la solicitud no se pudieron obtener: El servicio no respondio';
        }

        return [
            'latitud'   => $latitud,
            'longitud'  => $longitud,
            'message'   => $message,
        ];

    }

    public function reverseDomicilio($latitud_reverse, $longitud_reverse) {

        $curl = new CurlCaller();
        $url = env('GMAPS_URL');
        $key = env('GMAPS_KEY');
        $latlng = "{$latitud_reverse},{$longitud_reverse}";

        $response = $curl->getLatLonDomicilio($url, $key, $latlng);
        $reverse_address = null;
        $message = null;

        if ($response !== null || $response !== '') {
            $response = json_decode($response);
            if (json_last_error() === JSON_ERROR_NONE) {
                if ($response->status == 'OK') {
                    if (isset($response->results[0]->formatted_address)){
                        $reverse_address = $response->results[0]->formatted_address;
                    } else {
                        $message = 'El valor de reverse_address no se pudo obtener: La respuesta no contiene el nodo location';
                    }
                } else {
                    $message = 'El valor de reverse_address no se pudo obtener: '.$response->error_message;
                }
            } else {
                $message = 'El valor de reverse_address no se pudo obtener: Error al parsear la respuesta a json';
            }
        } else {
            $message = 'El valor de reverse_address no se pudo obtener: El servicio no respondio';
        }

        return [
            'reverse_address'   => $reverse_address,
            'message'           => $message,
        ];

    }

    /**
     * Genera el job para procesamiento del alta de Comprobantes de Domicilio mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function procesar(Request $request) {

        $clienteAlta = ClientesAlta::find($request->idAltaCliente);
        $job = (new ComprobantesDomicilio($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
        dispatch($job);

        return response()->json(['success' => true]);

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    public function siguientePaso($prospecto_id, $solicitud_id, $redirect = true) {

        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        if ($siguientePaso != 'comprobante_domicilio' && $redirect == true) {

            // Si siguientePaso es diferente de null quiere decir que aún hay
            // documentos por cargar y se puede redireccionar, de lo contrario
            // cerramos la sesión y redirigimos al simulador
            if ($siguientePaso != null) {
                return redirect("/webapp/{$siguientePaso}");
            } else {
                Auth::guard('prospecto')->logout();
                return redirect("/");
            }

        } elseif ($siguientePaso != 'comprobante_domicilio' && $redirect == false) {
            return $siguientePaso;
        } else {
            return null;
        }

    }

}
