<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Validator;
use Auth;
use Illuminate\Validation\Rule;
use App\Situacion;
use App\RespuestaCuestionarioDinamico;
use App\RespuestaMaquinaRiesgo;
use App\Solicitud;
use App\CuentaClabeSolicitud;
use App\ClientesAlta;
use App\Repositories\CurlCaller;
use App\Jobs\AltaCuentaClabe;
use App\W_LOAN_USER_ACCOUNT;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use App\Repositories\SolicitudRepository;
use App\C_BANK;

class CuentaClabeController extends Controller
{
    private $curl;
    private $solicitudRepository;

    public function __construct(CurlCaller $curl)
    {
        $this->curl = $curl;
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function index() {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id);
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            $bancos = C_BANK::pluck('DESCRIPTION', 'ID_BANK');
            return view('webapp.cuenta_clabe', ['bancos' => $bancos, 'pasos_habilitados' => $pasos_habilitados]);
        } else {
            return $siguientePaso;
        }

    }

    public function save(request $request)
    {
        if (Auth::guard('prospecto')->check()) {

            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;

            $validaciones = $this->validaciones($request->all());
            if ($validaciones->fails()) {
                return response()->json([
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ]);
            }

            $validacionCuenta = $this->validaCuenta($request);

            if ($validacionCuenta->statusCode == 404 || $validacionCuenta->statusCode == 400) {
                return response()->json([
                    'success'   => false,
                    'msg'       => $validacionCuenta->msg,
                    'errores'   => ['clabe_interbancaria' => $validacionCuenta->msg]
                ]);
            }

            $altaCliente = ClientesAlta::select('facematch', 'no_solicitud_t24', 'documentos_facematch', 'created_at')
                ->where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->first();

            $cuentaClabe = W_LOAN_USER_ACCOUNT::where('ID_LOAN_APP', $altaCliente->no_solicitud_t24)
                ->get()
                ->first();

            if (isset($cuentaClabe->BANK_KEY)) {

                /*
                $solicitud = Solicitud::find($solicitud_id);

                CuentaClabeSolicitud::updateOrCreate([
                    'solicitud_id'          => $solicitud->id
                ], [
                    'banco_id'              => $cuentaClabe->BANK_KEY,
                    'banco'                 => $cuentaClabe->BANK_NAME,
                    'clabe_interbancaria'   => $cuentaClabe->ACCOUNT,
                ]);

                $job = (new AltaCuentaClabe($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                dispatch($job);
                */

            } else {

                $cuentaClable = CuentaClabeSolicitud::updateOrCreate([
                    'solicitud_id'          => $solicitud_id
                ], [
                    'banco_id'              => $request->banco,
                    'banco'                 => $request->descripcion_banco,
                    'clabe_interbancaria'   => $request->clabe_interbancaria,
                ]);

                $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->orderBy('id', 'desc')
                    ->first();


                $job = (new AltaCuentaClabe($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                dispatch($job);

            }

            CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->update(['cuenta_clabe_completo' => 1]);

            $log = [
                'success' => true,
                'msj'     => 'Cuenta CLABE guardada',
            ];
            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
            $solicitud->save();

            $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, false);

            if ($siguientePaso == null) {

                if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                    $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                } else {
                    $id_plantilla = 89;
                }

                $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                            ->where('plantilla_id', $id_plantilla)
                            ->get()
                            ->toArray();
                $tituloModal = $plantilla[0]['modal_encabezado'];
                $imgModal = $plantilla[0]['modal_img'];
                $cuerpoModal = $plantilla[0]['modal_cuerpo'];

                $producto = null;
                if ($solicitud->producto()->exists()) {
                    $producto = $solicitud->producto[0]['alias'];
                }

                $faltanDocumentos = $this->solicitudRepository->faltanDocumentos($prospecto_id, $solicitud_id, $producto);

                if ($faltanDocumentos != null) {
                    $cuerpoModal = str_replace('{{ lista_documentos }}', $faltanDocumentos, $cuerpoModal);
                } else {
                    $tituloModal = __('messages.titulo_modal_carga_documentos_terminada');
                    $cuerpoModal = __('messages.cuerpo_modal_carga_documentos_terminada');
                }
                $modal = view("modals.modalStatusSolicitudDocumentos", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();

                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Oferta Aceptada - Carga Documentos Completada',
                    1,
                    'Se ha completado la carga de documentos en el Panel Operativo'
                );
                $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                $solicitud->save();

                Auth::guard('prospecto')->logout();

                return response()->json([
                    'success'           => true,
                    'siguiente_paso'    => $siguientePaso,
                    'modal'             => $modal,
                    'message'           => 'Cuenta Clabe guardada con éxito'
                ]);

            } else {

                return response()->json([
                    'success'           => true,
                    'siguiente_paso'    => $siguientePaso,
                    'message'           => 'Cuenta Clabe guardada con éxito'
                ]);

            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true
            ]);

        }

    }

    /**
     * Genera el job para procesamiento del alta de la Cuenta Clabe mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function procesar(Request $request) {

        $clienteAlta = ClientesAlta::find($request->idAltaCliente);
        $job = (new AltaCuentaClabe($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
        dispatch($job);

        return response()->json(['success' => true]);

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    function validaciones($datos) {

        $reglas = [
            'banco'                 => 'required|digits:5',
            'descripcion_banco'     => 'required',
            'clabe_interbancaria'   => 'required|digits:18'
        ];
        $mensajes = [
            'required'              => 'El campo es obligatorio.',
            'alpha_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'digits'                => 'El campo debe tener :digits números.',
        ];

        $validaciones = Validator::make($datos, $reglas, $mensajes);
        return $validaciones;

    }

    function validaCuenta(request $request) {

        $datos['bk'] = $request->banco;
        $datos['oc'] = false;
        $datos['cb'] = $request->clabe_interbancaria;
        $url = env('URL_CUENTACLABE');

        $validacion = $this->curl->callCurlVaidacionClabe($url, $datos);

        return json_decode($validacion);
    }

    public function siguientePaso($prospecto_id, $solicitud_id, $redirect = true) {

        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        if ($siguientePaso != 'cuenta_clabe' && $redirect == true) {
            return redirect('/webapp/'.$siguientePaso);
        } elseif ($siguientePaso != 'cuenta_clabe' && $redirect == false) {
            return $siguientePaso;
        } else {
            return null;
        }

    }

}
