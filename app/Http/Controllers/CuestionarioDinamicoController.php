<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Validator;
use Illuminate\Validation\Rule;
use App\Situacion;
use App\RespuestaCuestionarioDinamico;
use App\RespuestaMaquinaRiesgo;
use App\RespuestaEvaluacionExperian;
use App\OfertaPredominante;
use App\Solicitud;
use App\Prospecto;
use Auth;
use App\Repositories\SolicitudRepository;
use App\MotivoRechazo;

class CuestionarioDinamicoController extends Controller
{
    private $solicitudRepository;

    /**
     * Constructor de la clase
     */
    public function __construct(request $request)
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    /**
     * Guarda las respuestas del cuestionario dinámico
     *
     * @param  request $request Datos capturados en el cuestionario dinámico
     *
     * @return json             Resultado del guardado del cuestionario dinámico
     */
    public function save(request $request)
    {
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            $solicitud_id = $solicitud->id;

            $situaciones = null;
            // Obteniendo las situaciones que incluye el cuestionario dinámico
            if ($request->situaciones != '') {
                $situaciones = $request->situaciones;
                $situaciones = explode('|', $situaciones);
                $situaciones = array_filter($situaciones, 'strlen');
            }

            if ($request->has('tipo_poblacion')) {
                $tipo_poblacion = $request->tipo_poblacion;
            } else {
                $tipo_poblacion = 'oferta_normal';
            }

            $modelo = $request->modelo;

            // Realizando la validación de los datos
            $reglas = $this->reglasValidacion($situaciones);
            $mensajes = $this->mensajesValidacion();
            $respuestas = $request->except(['situaciones', 'prospecto_id', 'solicitud_id', 'tipo_poblacion']);

            $validacion = Validator::make($respuestas, $reglas, $mensajes)
                ->validate();

            // Guardado de respuestas
            $situaciones = Situacion::pluck('id','situacion')->toArray();
            $guardaRespuestas = $this->guardaRespuestas($respuestas, $situaciones, $prospecto_id, $solicitud_id);

            if ($guardaRespuestas['success'] == true) {

                $ofertaPredominante = OfertaPredominante::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->get();

                OfertaPredominante::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'cuestionario_dinamico_guardado' => 1,
                        'status_guardado'                => 'Cuestionario Dinámico Guardado'
                    ]);

                if ($ofertaPredominante[0]['decision'] == 'Aprobado Ambos Modelos') {

                    RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->update([
                            'cuestionario_dinamico_guardado' => 1,
                            'status_guardado'                => 'Cuestionario Dinámico Guardado'
                        ]);

                    RespuestaEvaluacionExperian::where('prospecto_id', $prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->update([
                            'cuestionario_dinamico_guardado' => 1,
                            'status_guardado'                => 'Cuestionario Dinámico Guardado'
                        ]);

                } else {

                    if ($modelo == 'Prestanómico') {

                        RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
                            ->where('solicitud_id', $solicitud_id)
                            ->update([
                                'cuestionario_dinamico_guardado' => 1,
                                'status_guardado'                => 'Cuestionario Dinámico Guardado'
                            ]);

                    } else {

                        RespuestaEvaluacionExperian::where('prospecto_id', $prospecto_id)
                            ->where('solicitud_id', $solicitud_id)
                            ->update([
                                'cuestionario_dinamico_guardado' => 1,
                                'status_guardado'                => 'Cuestionario Dinámico Guardado'
                            ]);

                    }

                }


                // Si el guardado y validación fue exitoso se cambia actualiza el status
                // final de la solicitud a Invitación a Continuar y el ultimo punto
                // de registro a Cuestionario Completo
                $solicitud = Solicitud::find($solicitud_id);
                $solicitud->status = 'Invitación a Continuar';
                $solicitud->sub_status = 'Cuestionario Completo';

                // Actualizando el utlimo punto de registro de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Cuestionario Completo',
                    1,
                    'El cuestionario dinámico se guardo con éxito'
                );
                $solicitud->save();

                $motivos = MotivoRechazo::pluck('motivo');
                foreach ($ofertaPredominante as $key => $resultado) {

                    $tipo_oferta = ($resultado->tipo_oferta == null) ? 'oferta_normal' : $resultado->tipo_oferta;

                    $datosOferta[$key] = [
                        'monto'         => $resultado->monto,
                        'plazo'         => $resultado->plazo,
                        'tasa'          => $resultado->tasa,
                        'pago_estimado' => $resultado->pago,
                        'tipo_oferta'   => $resultado->tipo_oferta,
                        'modelo'        => $resultado->modeloEvaluacion,
                        'oferta_id'     => $resultado->oferta_id
                    ];
                }
                $modal = 'modalOferta';
                
                if ($resultado->plantilla_comunicacion == 90) {
                    $modal = 'modalOfertaReactiva';
                }
               
                if ($tipo_poblacion == 'oferta_doble') {
                    $modal = 'modalDobleOferta';
                    if ($resultado->plantilla_comunicacion == 90) {
                        $modal = 'modalDobleOfertaReactiva';
                    }
                    return response()->json([
                        'success'           => true,
                        'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                        'prospecto_id'      => $prospecto_id,
                        'solicitud_id'      => $solicitud_id,
                        'tipo_poblacion'    => $tipo_poblacion,
                        'modal'             => view("modals.{$modal}", ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
                    ]);

                } else {

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                        'prospecto_id'      => $prospecto_id,
                        'solicitud_id'      => $solicitud_id,
                        'tipo_poblacion'    => $tipo_poblacion,
                        'modal'             =>view("modals.{$modal}", ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
                    ]);

                }

            } else {

                $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                    'prospecto_id'  => $prospecto_id,
                    'solicitud_id'  => $solicitud_id
                ], [
                    'cuestionario_dinamico_guardado'    => 1,
                    'status_guardado'                   => $guardaRespuestas['message'],
                ]);

                return response()->json([
                    'success' => false,
                    'message' => $guardaRespuestas['message'],
                ]);
            }
        } else {

            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);

        }

    }

    /**
     * Realiza el guardado de las respuestas del cuestionario dinámico
     * @param  object  $respuestas   Respuestas del cuestionario dinámico
     * @param  array   $situaciones  Situaciones que incluye el cuestionario dinámico
     * @param  integer $prospecto_id Id del prospecto
     * @param  integer $solicitud_id Id de la solicitud
     *
     * @return json                  Resultado del guardado de las respuestas
     */
    public function guardaRespuestas($respuestas, $situaciones, $prospecto_id, $solicitud_id)
    {
        try {

            foreach ($respuestas as $key => $respuesta) {

                $ids = explode('_', $key);

                if (count($ids) == 2) {

                    $situacion = $ids[0];
                    $pregunta_id = $ids[1];
                    $situdacion_id = $situaciones[$situacion];

                    $res = RespuestaCuestionarioDinamico::updateorCreate([
                        'prospecto_id'  => $prospecto_id,
                        'solicitud_id'  => $solicitud_id,
                        'situacion_id'  => $situdacion_id,
                        'pregunta_id'   => $pregunta_id
                    ], [
                        'respuesta'     => mb_strtoupper($respuesta)
                    ]);

                    if ($situdacion_id == 8) {

                        $solicitud = Solicitud::find($solicitud_id);

                        if ($solicitud->exists()) {
                            $solicitud->telefono_casa = $respuesta;
                            $solicitud->save();
                        }


                    }

                }

            }

            return [
                'success' => true,
            ];

        } catch (\Exception $e) {

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];

        }
    }

    /**
     * Realiza la validación de las respuestas del cuestionario dinámico
     * @param  array $situaciones Situaciones que incluye el cuestionario dinámico
     *
     * @return json               Resultado de la validación
     */
    public function reglasValidacion($situaciones)
    {
        $reglas = null;
        // Sección: ATRASO EN CUENTA REPORTADO EN BURO
        if (in_array('A', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿qué fue lo que pasó con
            //  esa(s) cuenta(s)?
            $reglas ['A_1']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Llegaste a un acuerdo con esa
            // institución?
            $reglas ['A_2']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Tienes algún documento que demuestre
            // la liquidación de ese adeudo o bien el acuerdo con la institución?
            $reglas ['A_3']  = [
                'required_if:A_2,SI'
            ];
            // Validacion para la pregunta: ¿Cuál documento?
            $reglas ['A_4']  = [
                'required_if:A_3,SI',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
        }
        // Sección: ACLARACION SOBRE INGRESO Y/O GASTO FAMILIAR
        if (in_array('B', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['B_5']  = [
                'required',
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['B_6']  = [
                'required_if:B_5,SI',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['B_7']  = [
                'required_if:B_5,SI',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: Del total de gastos familiares que
            // nos indicas en tu solicitud, ¿cuánto es el gasto que te corresponde
            // cubrir a ti?
            $reglas ['B_8']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:50'
            ];
        }

        // Sección: ACLARACION SOBRE DEUDA RECIENTE
        if (in_array('C', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿cuál ha sido el propósito
            // de los créditos que has adquirido recientemente?
            $reglas ['C_9']  = [
                'required',
                'min:10',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Has tenido algún cambio importante
            // en tu nivel o fuente de ingreso?
            $reglas ['C_10']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Has tenido algún cambio importante
            // en tu nivel de gastos?
            $reglas ['C_11']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
        }

        // Sección: PAGOS A PRESTAMOS ACTUALES
        if (in_array('D', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['D_12']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['D_13']  = [
                'required_if:D_12,SI',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['D_14']  = [
                'required_if:D_12,SI',
                'alpha_numeric_spaces_not_html',
                'max:50'
            ];
            // Validacion para la pregunta: ¿Alguno de los préstamos a tu nombre
            // es pagado parcial o totalmente por otra persona?
            $reglas ['D_15']  = [
                'required',
            ];
            // Validacion para la pregunta: Cuéntanos más sobre ese préstamo y
            // quién lo paga
            $reglas ['D_16']  = [
                'required_if:D_15,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: FUENTE ACTUAL DE INGRESOS
        if (in_array('E', $situaciones)) {
            // Validacion para la pregunta: Coméntanos por favor algo sobre tu
            // ocupación y cuál es tu fuente de ingresos
            $reglas ['E_17']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];

        }

        // Sección: PROPOSITO DEL PRESTAMO
        if (in_array('F', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿para qué vas a usar el
            // dinero si tu préstamo es aprobado?
            $reglas ['F_18']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: NOS GUSTARIA ENTENDER TU NEGOCIO
        if (in_array('G', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿cuál es tu negocio?
            $reglas ['G_19']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Desde cuándo lo tienes? ¿O cuándo
            // lo vas a iniciar?
            $reglas ['G_20']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Tienes un local fijo?
            $reglas ['G_21']  = [
                'required',
                'min:2',
                'max:25',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta:¿Para qué vas a utilizar los recursos
            // del préstamo?
            $reglas ['G_22']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: TELEFONO FIJO
        if (in_array('H', $situaciones)) {
            // Validacion para la pregunta: Por favor indícanos un teléfono fijo
            // en donde podamos localizarte si es necesario (puede ser de tu
            // trabajo o un familiar cercano)
            $reglas ['H_23']  = [
                'required',
                'digits:10',
            ];
        }

        // Sección:
        if (in_array('BD', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['BD_24']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['BD_25']  = [
                'required_if:BD_24,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['BD_26']  = [
                'required_if:BD_24,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: Del total de gastos familiares que
            // nos indicas en tu solicitud, ¿cuánto es el gasto que te
            // corresponde cubrir a ti?
            $reglas ['BD_27']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Alguno de los préstamos a tu nombre
            // es pagado parcial o totalmente por otra persona?
            $reglas ['BD_28']  = [
                'required',
            ];
            // Validacion para la pregunta: Cuéntanos más sobre ese préstamo y
            // quién lo paga
            $reglas ['BD_29']  = [
                'required_if:BD_28,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: REFERENCIAS LABORALES
        if (in_array('I', $situaciones)) {

            $reglas ['I_30']  = [
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            $reglas ['I_31']  = [
                'digits:10',
            ];
        }

        return $reglas;
    }

    /**
     * Establece los mensajes para cada situación de las validaciones
     *
     * @return array Arreglo con los mensajes
     */
    public function mensajesValidacion()
    {
        return [
            'required'                       => '* El campo es requerido',
            'required_if'                    => '* El campo es requerido',
            'min'                            => '* Al menos :min caracteres',
            'max'                            => '* Menos de :max caracteres',
            'digits'                         => '* Deben ser 10 dígitos',
            'alpha_numeric_spaces_not_html'  => '* El campo solo permite letras, números y espacios'
        ];
    }

}
