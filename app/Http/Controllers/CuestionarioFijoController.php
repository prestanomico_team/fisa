<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Illuminate\Validation\Rule;
use App\RespuestaCuestionarioFijo;

class CuestionarioFijoController extends Controller
{
    /**
     * Guarda las respuestas del cuestionario fijo
     *
     * @param  request $request Datos capturados en el cuestionario fijo
     * @return json             Resultado del guardado del cuestionario fijo
     */
    public function save(request $request)
    {
        try {
            if ($request->prospecto_id_CF != '' && $request->solicitud_id_CF != '') {
                $contactoSms = 0;
                if (isset($request->contactoSms)) {
                    $contactoSms = 1;
                }
                $contactoWhatsapp = 0;
                if (isset($request->contactoWhatsapp)) {
                    $contactoWhatsapp = 1;
                }
                $contactoCorreo = 0;
                if (isset($request->contactoCorreo)) {
                    $contactoCorreo = 1;
                }

                $respuesta = RespuestaCuestionarioFijo::updateorCreate([
                    'prospecto_id' => $request->prospecto_id_CF,
                    'solicitud_id' => $request->solicitud_id_CF
                ], [
                    'telefono_contactar'     => mb_strtoupper($request->telefonoContactar),
                    'fecha_hora_contacto'   => $request->fechaContacto,
                    'sms'                   => $contactoSms,
                    'whatsapp'              => $contactoWhatsapp,
                    'email'                 => $contactoCorreo
                ]);
            }

            return [
                'success' => true
            ];

        } catch (\Exception $e) {

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];

        }
    }

    /**
     * Muestra la vista del calendario de citas
     *
     * @return view           Vista del calendario de citas
     */
    public function calendarioCitas()
    {
        return response()->view('crm.calendario-citas');
    }

    /**
     * Obtiene las citas agendadas de la tabla respuestas_cuestionario_fijo
     *
     * @return json           Citas agendadas
     */
    public function obtenerCitas()
    {

        $respuestas = RespuestaCuestionarioFijo::with('prospecto', 'solicitud')
            ->where('fecha_hora_contacto', '!=', '0000-00-00 00:00:00')
            ->get()
            ->toArray();

        $citas = [];

        foreach ($respuestas as $key => $respuesta) {

            $nombre_prospecto = mb_strtoupper(decrypt($respuesta['prospecto']['nombre']));
            $nombre_prospecto .= ' '.mb_strtoupper(decrypt($respuesta['prospecto']['apellido_p']));
            $nombre_prospecto .= ' '.mb_strtoupper(decrypt($respuesta['prospecto']['apellido_m']));
            $medio = null;

            if ($respuesta['sms'] == 1) {
                $medio[] = 'SMS';
            }
            if ($respuesta['whatsapp'] == 1) {
                $medio[] = 'WHATSAPP';
            }
            if ($respuesta['email'] == 1) {
                $medio[] = 'E-MAIL';
            }
            if (count($medio) >= 1) {
                $medio = implode(', ', $medio);
            } else {
                $medio = 'NA';
            }

            $descripcion = '<b>CELUAR:</b> '.decrypt($respuesta['prospecto']['cel']).'<br>';
            $descripcion .= '<b>TEL. CASA:</b> '.decrypt($respuesta['solicitud']['tel_casa']).'<br>';
            $descripcion .= '<b>TEL. EMPLEO:</b> '.decrypt($respuesta['solicitud']['telefono_empleo']).'<br>';
            $descripcion .= '<b>TEL. DE CONTACTO:</b> '.mb_strtoupper($respuesta['telefono_contactar']).'<br>';
            $descripcion .= '<b>E-MAIL:</b> '.$respuesta['prospecto']['email'].'<br>';
            $descripcion .= '<b>CANALES ALTERNOS:</b> '.$medio.'<br>';
            $descripcion .= '<b>ID PROSPECTO:</b> '.$respuesta['prospecto_id'].'<br>';
            $descripcion .= '<b>ID SOLICITUD:</b> '.$respuesta['solicitud_id'].'<br>';

            $citas[] = [
                'title'         => 'Llamada a prospecto: '.$nombre_prospecto,
                'description'   => $descripcion,
                'start'         => $respuesta['fecha_hora_contacto'],
                'end'           => $respuesta['fecha_hora_contacto']
            ];
        }

        return response()->json($citas);
    }
}
