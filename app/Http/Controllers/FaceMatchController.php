<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Jobs\ProcesaFaceMatch;
use App\Jobs\ProcesaCargaIdentificacionSelfie;
use App\Solicitud;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\MB_UPLOADED_DOCUMENT;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use Log;
use Image;
use App;
use Auth;
use Jenssegers\Agent\Agent;
use App\Repositories\SolicitudRepository;
use Session;
use Validator;
use Illuminate\Validation\Rule;

class FaceMatchController extends Controller
{
    private $solicitudRepository;

    public function __construct(DocumentosRepository $documentos)
    {
        $this->_documentos = $documentos;
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function capturaIdentificacionFront() {

        $agent = new Agent();
        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        // Verificando que estamos en el paso correcto
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, '/id_front');

        if ($siguientePaso == null) {
            $respuestaMR = RespuestaMaquinaRiesgo::select('simplificado')
                ->where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->firstOrFail();

            $modalInicio = false;
            if (Session::get('isRedirected')) {
                Session::forget('isRedirected');
                $modalInicio = true;
            }

            return view('webapp.idFront', [
                'desktop' => $agent->isDesktop(),
                'simplificado' => $respuestaMR->simplificado,
                'modalInicio' => $modalInicio,
                'pasos_habilitados' => $pasos_habilitados
            ]);
        } else {
            return $siguientePaso;
        }
    }

    public function capturaIdentificacionBack() {

        $agent = new Agent();
        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, '/id_back');
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            return view('webapp.idBack', ['desktop' => $agent->isDesktop(), 'pasos_habilitados' => $pasos_habilitados]);
        } else {
            return $siguientePaso;
        }

    }

    public function capturaSelfie() {

        $agent = new Agent();
        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, '/selfie');
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            return view('webapp.selfie', ['desktop' => $agent->isDesktop(), 'pasos_habilitados' => $pasos_habilitados]);
        } else {
            return $siguientePaso;
        }

    }

    public function subirDocumento(Request $request) {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;

        $validaciones = $this->validaciones($request->all());
        if ($validaciones->fails()) {
            return response()->json([
                'success'   => false,
                'errores'   => $validaciones->errors()
            ]);
        }

        $tipo_documento = $request->get('tipo_documento');
        $detalle_documento = $request->get('detalle_documento');
        $image = Image::make($request->get('imgBase64'));
        $root = "proceso_simplificado";

        if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg")) {
            Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg");
        }
        Storage::disk('s3')->put("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg", $image->encode());

        $termina = false;
        $update = '';
        $paso = '';

        if ($tipo_documento == 'identificacion_oficial') {
            if ($detalle_documento == 'front') {
                $paso = '/id_front';
                $update = ['id_front' => true];
            } elseif ($detalle_documento == 'back') {
                $paso = '/id_back';
                $update = ['id_back' => true];
            }  elseif ($detalle_documento == 'photo') {
                $termina = true;
                $paso = '/selfie';
                $update = ['selfie' => true, 'facematch_completo' => true];
            }

            CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->update($update);

        }

        if ($termina == true) {

            $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->orderBy('id', 'desc')
                ->first();

            if ($clienteAlta->facematch === null) {

                if ($clienteAlta->aplica_facematch == true) {
                    $job = (new ProcesaFaceMatch($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                    dispatch($job);
                } elseif ($clienteAlta->solo_carga_identificacion_selfie == true) {
                    $job = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                    dispatch($job);
                }

                // Actualizando ult_punto_reg solicitud
                if ($solicitud->sub_status == 'Oferta Aceptada - FaceMatch') {

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Oferta Aceptada - Carga Documentos',
                        1,
                        'El FaceMatch se completo con éxito, se actualiza el sub status'
                    );

                    $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos';
                    $solicitud->save();

                }

                $log = [
                    'success' => true,
                    'msj'     => 'FaceMatch completado',
                ];
                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
                $solicitud->save();

                $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, $paso, false);

                // Si ya no hay pasos por completar en la carga de documentos
                // se obtiene el modal final, se consulta el método faltanDocumentos
                // para obtener el listado de los documentos que hacen falta y mostrarlos en
                // el modal, si regresa null, quiere decir que no hace falta ningun documento
                // y se muestra el modal de solicitud terminada.
                if ($siguientePaso == null) {

                    if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                        $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                    } else {
                        $id_plantilla = 89;
                    }

                    $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                                ->where('plantilla_id', $id_plantilla)
                                ->get()
                                ->toArray();
                    $tituloModal = $plantilla[0]['modal_encabezado'];
                    $imgModal = $plantilla[0]['modal_img'];
                    $cuerpoModal = $plantilla[0]['modal_cuerpo'];

                    $producto = null;
                    if ($solicitud->producto()->exists()) {
                        $producto = $solicitud->producto[0]['alias'];
                    }

                    $faltanDocumentos = $this->solicitudRepository->faltanDocumentos($prospecto_id, $solicitud_id, $producto);

                    if ($faltanDocumentos != null) {
                        $cuerpoModal = str_replace('{{ lista_documentos }}', $faltanDocumentos, $cuerpoModal);
                    } else {
                        $tituloModal = __('messages.titulo_modal_carga_documentos_terminada');
                        $cuerpoModal = __('messages.cuerpo_modal_carga_documentos_terminada');
                    }
                    $modal = view("modals.modalStatusSolicitudDocumentos", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Oferta Aceptada - Carga Documentos Completada',
                        1,
                        'Se ha completado la carga de documentos en el Panel Operativo'
                    );
                    $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                    $solicitud->save();

                    Auth::guard('prospecto')->logout();

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Imagen guardada correctamente',
                        'modal'             => $modal,
                        'termina_proceso'   => true,
                    ]);

                } else {

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Imagen guardada correctamente',
                        'siguiente_paso'    => $siguientePaso,
                        'termina_proceso'   => true,
                    ]);

                }

            }

        } else {

            $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, $paso, false);
            return response()->json([
                'success'           => true,
                'message'           => 'Imagen guardada correctamente',
                'termina_proceso'   => false,
                'siguiente_paso'    => $siguientePaso
            ]);

        }

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    public function procesaFaceMatch(Request $request) {

        $id = $request->idAltaCliente;
        $clienteAlta = ClientesAlta::findOrFail($id);

        if ($clienteAlta->aplica_facematch == true) {
            $job = (new ProcesaFaceMatch($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
            dispatch($job);
        } elseif ($clienteAlta->solo_carga_identificacion_selfie == true) {
            $job = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
            dispatch($job);
        }

        return response()->json([
            'success' => true
        ]);

    }

    /**
     * Consulta cual es el siguiente paso en la carda de documentos, si no nos
     * encontramos en el paso correcto nos redirige al siguiente punto.
     *
     * @param  integer  $prospecto_id  Id del prospecto
     * @param  integer  $solicitud_id  Id de la solicitud
     * @param  string   $paso          Paso del facematch en el que nos encontramos
     * @param  boolean  $redirect      Determina si regresa redirect o string
     *
     * @return string/redirect         Valor regresado
     */
    public function siguientePaso($prospecto_id, $solicitud_id, $paso, $redirect = true) {

        // Consultando cual es el siguiente paso en la carga de documentos
        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        // Si no estamos en el paso facematch y redirect es verdadero, nos redirige al siguiente paso
        if (!str_contains($siguientePaso, 'facematch'.$paso) && $redirect == true) {

            // Si siguientePaso es diferente de null quiere decir que aún hay
            // documentos por cargar y se puede redireccionar, de lo contrario
            // cerramos la sesión y redirigimos al simulador
            if ($siguientePaso != null) {
                return redirect("/webapp/{$siguientePaso}");
            } else {
                Auth::guard('prospecto')->logout();
                return redirect("/");
            }

        // Si no estamos en el paso facematch y redirect es falso, solo regresamos
        // cual es el siguiente paso
    } elseif (!str_contains($siguientePaso, 'facematch'.$paso) && $redirect == false) {
            return $siguientePaso;
        // Si estamos en el paso facematch no regresamos nada
        } else {
            return null;
        }

    }

    function validaciones($datos) {

        $reglas = [
            'tipo_documento'    => [
                                    'required',
                                    Rule::in(['identificacion_oficial']),
                                ],
            'detalle_documento' => [
                                    'required',
                                    Rule::in(['front', 'back', 'photo']),
                                ],
            'imgBase64'         => 'required|base64image',
        ];
        $mensajes = [
            'required'              => 'El campo es obligatorio.',
            'alpha_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'in'                    => 'El valor enviado no se encuentra en las opciones válidas',
            'base64image'           => 'No es una imagen válida, solo se admiten imagenes base64 en formato png, jpg, jpeg, svg, bmp o gif',
        ];

        $validaciones = Validator::make($datos, $reglas, $mensajes);
        return $validaciones;

    }

}
