<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Producto;
use App\Plazo;
use App\Ocupacion;
use App\Finalidad;
use App\UserConvenio;
use App\Solicitud;
use Cookie;
use Crypt;
use Validator;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Encryption\DecryptException;
use DB;
use Illuminate\Support\Arr;
use Excel;
use App\CatalogoSepomex;
use App\DatosConvenio;
use Bitly;
use URL;
class ProductoController extends Controller
{
    /**
     * Obtiene la configuración del producto ya sea por Id o por Cookie
     *
     * @param  Integer $id_producto Id del producto
     * @param  Integer $version     Versión del producto del que se desea
     * obtener la configuración
     *
     * @return array Configuración del producto
     */
    public function obtenerConfiguracion($id_producto = null, $version = null) {

        $configuracion = Producto::with('plazos', 'finalidades', 'ocupaciones')
            ->get();

        if ($id_producto == null) {

            $cookies = self::getCookieProducto();
            $producto = $cookies['producto'];
            $checa_calificas =  $cookies['checa_calificas'];

            if (isset($producto)) {

                $configuracion = $configuracion->where('alias', $producto)
                    ->values()
                    ->toArray();

            } else {

                $configuracion = $configuracion->where('default', '1')
                    ->values()
                    ->toArray();

            }

        } else {

            $configuracion = $configuracion->where('id', $id_producto)
                ->values()
                ->toArray();

        }

        $id = $configuracion[0]['id'];
        $version_producto = Producto::find($id)->currentVersion();

        $configuracion[0]['version_producto'] = $version_producto;
        $logo = $configuracion[0]['logo'];

        Cookie::queue('logo', $logo, 180);

        return $configuracion[0];

    }

    /**
     * Genera la lista de plazos, duración y finalidades de forma correcta
     * para el select correspondiente
     *
     * @param  array $configuracion  Configuracíon del producto
     *
     * @return array                 Listas de plazos, duración y finalidades
     */
    public function obtenerListas($configuracion) {

        // Obteniendo la lista de plazos
        
        $plazos = $configuracion['plazos'];
        $plazo_default = null;
        foreach ($plazos as $key => $value) {
            $plazos[$key]['duracion_plazo'] = $value['duracion'].' '.$value['plazo'];
            if ($value['pivot']['default'] == 1) {
                $plazo_default = $value['id'];
            }
        }
        $lista_plazos = Arr::pluck($plazos, 'duracion_plazo', 'id');

        // Obteniendo la etiqueta plazos
        $plazos_collection = collect($plazos)->groupBy('plazo');
        $plazos_keys = $plazos_collection->keys();
        $plazos_collection = $plazos_collection[$plazos_keys[0]];
        $etiqueta_plazos = 'De '.$plazos_collection->min('duracion')
            .' a '.$plazos_collection->max('duracion')
            .' '.$plazos_keys[0];

        // Obteniendo la lista de finalidades
        $finalidades = $configuracion['finalidades'];
        $lista_finalidades = Arr::pluck($finalidades, 'finalidad', 'id');

        return [
            'finalidades'       => $lista_finalidades,
            'plazos'            => $lista_plazos,
            'plazo_default'     => $plazo_default,
            'etiqueta_plazos'   => $etiqueta_plazos
        ];

    }

    /**
     * Obtiene la cookie producto si ya esta establecida
     *
     * @return string Valor de la cookie que contiene el nombre del producto
     */
    public function getCookieProducto() {

        $producto = Cookie::get('producto');
        $checa_calificas = isset($_COOKIE['checa_calificas']) ? $_COOKIE['checa_calificas'] : null;

        if (isset($producto) && isset($checa_calificas)) {
            try {

                $producto = Crypt::decrypt($producto);
                $cookies = [
                    'producto'          => $producto,
                    'checa_calificas'   => $checa_calificas,
                ];
                return $cookies;

            } catch (DecryptException $e) {

                $cookies = [
                    'producto'          => $producto,
                    'checa_calificas'   => $checa_calificas,
                ];
                return $cookies;

            }
        } else {
            return [
                'producto'          => null,
                'checa_calificas'   => null,
            ];
        }

    }

    /**
     * Obtiene la vista necesaria con la configuración del producto
     *
     * @param  Request $request Datos enviados en la petición GET
     *
     * @return view             Vista con los datos de la configuración
     */
    public function obtenerVista(Request $request) {

        if ($request->has('utm_source') && $request->has('utm_campaign') && $request->has('r_parameter')) {

            $campaña = $request->utm_campaign;
            $r_parameter = $request->r_parameter;

            if ($campaña == 'askrobin') {
                Cookie::queue('campaña', $campaña, 180);
                Cookie::queue('r_parameter', $r_parameter, 180);
            }
        }

        if ($request->has('utm_source') && $request->has('utm_campaign') && $request->has('utm_medium')) {

            $source = mb_strtolower($request->utm_source);

            if (strpos($source, 'adsalsa') !== false) {
                Cookie::queue('campaña', 'adsalsa', 180);
            }

            if (strpos($source, 'antevenio') !== false) {
                Cookie::queue('campaña', 'antevenio', 180);
            }

            if (strpos($source, 'sem-financiera') !== false) {
                Cookie::queue('campaña', 'sem', 180);
            }

        }

        if (count($request->segments()) > 0) {
            $vista = $request->segments();
            $vista = $vista[0];

            $prod_id = Producto::where('alias', $vista)
                ->get()
                ->first();
            //TODO: Se hace validación para que cuando se ingrese a la url de ReactivaNL
            // se redireccione el producto a MA    
            if ($prod_id->alias != 'ReactivaNL') {
                
                $id_producto = $prod_id->id;
                $lifetime = Carbon::now()->addYear()->diffInMinutes();
                $cookie = Cookie::make('producto', $vista, $lifetime);
                $cookie2 = Cookie::make('checa_calificas',1, $lifetime);

            } else {
                Cookie::queue(Cookie::forget('producto'));
                Cookie::queue(Cookie::forget('logo'));
                return redirect()->route('index');
            }

        } else {

            $vista = 'index';
            $id_producto = null;
            Cookie::queue(Cookie::forget('producto'));
            Cookie::queue(Cookie::forget('checa_calificas'));

        }

        $configuracion = self::obtenerConfiguracion($id_producto);

        if ($configuracion['alias'] == 'dentalia' || $configuracion['alias'] == 'bedu') {
             switch ($configuracion['alias']) {
                case 'dentalia':
                    $sucursales = UserConvenio::where('sucursal','!=','Prestanomico')
                        ->where('empresa','LIKE','%'.$configuracion['alias'].'%')
                        ->get()
                        ->toArray();
                    break;
                case 'bedu':
                    $sucursales = UserConvenio::selectRaw('concat(sucursal," - ",embajador) as sucursal, id')
                        ->where('sucursal','!=','Prestanomico')
                        ->where('empresa','LIKE','%'.$configuracion['alias'].'%')
                        ->get()
                        ->toArray();
                    break;
            }

            $configuracion['sucursales'] = Arr::pluck($sucursales, 'sucursal', 'id');
        }
        if ($configuracion['alias'] == 'comunidar') {
            $ejecutivos = UserConvenio::where('empresa', 'Corporativo')
                ->get()
                ->toArray();

            $configuracion['ejecutivos'] = Arr::pluck($ejecutivos, 'email', 'id');

        }

        $listas = self::obtenerListas($configuracion);

        if ($id_producto === null) {
            return view($vista)
            ->with([
                'configuracion' => [
                    'configuracion'     => $configuracion,
                    'finalidades'       => $listas['finalidades'],
                    'plazos'            => $listas['plazos'],
                    'plazo_default'     => $listas['plazo_default'],
                    'etiqueta_plazos'   => $listas['etiqueta_plazos']
                ]
            ]);
        } else {

            $response = new Response(view($vista)->with([
                'configuracion' => [
                    'configuracion'     => $configuracion,
                    'finalidades'       => $listas['finalidades'],
                    'plazos'            => $listas['plazos'],
                    'plazo_default'     => $listas['plazo_default'],
                    'etiqueta_plazos'   => $listas['etiqueta_plazos']
                ]
            ]));

            return $response->cookie($cookie)->cookie($cookie2);
        }

    }

    /**
     * Obtiene la vista para el producto de renovaciones
     *
     * @param  Request $request Datos enviados en la petición GET
     *
     * @return view             Vista con los datos de la configuración
     */
    public function renovaciones(Request $request) {

        if (count($request->segments()) > 0) {
            $vista = 'index-renovaciones';
        }

        $configuracion = self::obtenerConfiguracion();
        $listas = self::obtenerListas($configuracion);

        if ($request->has('uuid')) {

            $configuracion = [
                'configuracion'     => $configuracion,
                'finalidades'       => $listas['finalidades'],
                'plazos'            => $listas['plazos'],
                'plazo_default'     => $listas['plazo_default'],
                'etiqueta_plazos'   => $listas['etiqueta_plazos'],
                'uuid'              => $request->uuid,
            ];

        } else {

            $configuracion = [
                'configuracion'     => $configuracion,
                'finalidades'       => $listas['finalidades'],
                'plazos'            => $listas['plazos'],
                'plazo_default'     => $listas['plazo_default'],
                'etiqueta_plazos'   => $listas['etiqueta_plazos']
            ];

        }

        return view($vista)
            ->with(['configuracion' => $configuracion]);
    }

    /**
     * Obtiene la vista para el producto de sindicalizados
     *
     * @param  Request $request Datos enviados en la petición GET
     *
     * @return view             Vista con los datos de la configuración
     */
    public function sindicalizados(Request $request) {

        if (count($request->segments()) > 0) {
            $vista = 'index-sindicalizados';
        }

        $prod = Producto::where('alias', 'sindicalizados')
                ->get()
                ->first();

        $configuracion = self::obtenerConfiguracion($prod->id);

        $listas = self::obtenerListas($configuracion);

        $configuracion = [
            'configuracion'     => $configuracion,
            'finalidades'       => $listas['finalidades'],
            'plazos'            => $listas['plazos'],
            'plazo_default'     => $listas['plazo_default'],
            'etiqueta_plazos'   => $listas['etiqueta_plazos']
        ];

        return view($vista)
            ->with(['configuracion' => $configuracion]);
    }
    /**
     * Muestra la lista de productos.
     *
     * @return view Vista con el listado de productos.
     */
    public function index() {

        $productos = Producto::all();
        $plazos = Plazo::all();
        $finalidades = Finalidad::all();
        $ocupaciones = Ocupacion::all();

        return view(
            'producto.index',
            [
                'productos'     => $productos,
                'plazos'        => $plazos,
                'finalidades'   => $finalidades,
                'ocupaciones'   => $ocupaciones,
            ]
        );
    }

    /**
     * Muestra la vista para crear un nuevo producto.
     *
     * @return view Vista para crear un nuevo producto.
     */
    public function nuevo() {

        $finalidades = Finalidad::orderBy('id', 'asc')->get();
        $plazos = Plazo::orderBy('id', 'asc')->get();
        $ocupaciones = Ocupacion::orderBy('id', 'asc')->get();

        return view(
            'producto.nuevo',
            [
                'plazos'        => $plazos,
                'finalidades'   => $finalidades,
                'ocupaciones'   => $ocupaciones,
            ]
        );
    }

    /**
     * Guarda los datos de un nuevo producto.
     *
     * @param  Request $request Arreglo que contiene los datos del producto.
     *
     * @return json             Resultado del guardado del producto.
     */
    public function save(Request $request) {

        $errores = $this->validar($request);

        if (count($errores) == 0) {

            try {

                $alias = str_slug($request->nombre_producto);

                if ($request->hasFile('logo')) {
                    $logo = $request->file('logo');
                    $nombre_logo = $logo->getClientOriginalName();
                    $logo->move(public_path().'/images/iconos', $nombre_logo);
                }

                $vigencia_hasta = null;
                if ($request->vigencia_hasta) {
                    $vigencia_hasta = $request->vigencia_hasta;
                }

                $do_monto_maximo = null;
                if ($request->tipo_monto_do == 'definido') {
                    $do_monto_maximo = $request->do_monto_maximo;
                } else {
                    $do_monto_maximo = $request->monto_maximo;
                }

                $simplificado_monto_maximo = null;
                $simplificado_monto_minimo = null;
                if ($request->tipo_monto_simplificado == 'definido') {
                    $simplificado_monto_maximo = $request->simplificado_monto_maximo;
                    $simplificado_monto_minimo = $request->simplificado_monto_minimo;
                }

                $producto = Producto::updateOrCreate(
                    [
                        'nombre_producto' => $request->nombre_producto,
                        'alias' => $alias
                    ],
                    [
                        'tipo'                                      => $request->tipo,
                        'empresa'                                   => $request->empresa,
                        'monto_minimo'                              => $request->monto_minimo,
                        'monto_maximo'                              => $request->monto_maximo,
                        'bc_score'                                  => $request->bc_score,
                        'condicion'                                 => $request->condicion,
                        'micro_score'                               => ($request->micro_score) ? $request->micro_score : null,
                        'edad_minima'                               => $request->edad_minima,
                        'sin_historial'                             => ($request->sin_historial) ? 1 : 0,
                        'sin_cuentas_recientes'                     => ($request->sin_cuentas_recientes) ? 1 : 0,
                        'edad_maxima'                               => $request->edad_maxima,
                        'cat'                                       => $request->cat,
                        'tasa_minima'                               => $request->tasa_minima,
                        'tasa_fija'                                 => ($request->tasa_fija) ? 1 : 0,
                        'tasa_maxima'                               => $request->tasa_maxima,
                        'comision_apertura'                         => $request->comision_apertura,
                        'stored_procedure'                          => $request->stored_procedure,
                        'doble_oferta'                              => ($request->doble_oferta) ? 1 : 0,
                        'tipo_monto_do'                             => $request->tipo_monto_do,
                        'do_monto_maximo'                           => $do_monto_maximo,
                        'proceso_simplificado'                      => ($request->proceso_simplificado) ? 1 : 0,
                        'tipo_monto_simplificado'                   => $request->tipo_monto_simplificado,
                        'simplificado_monto_minimo'                 => $simplificado_monto_minimo,
                        'simplificado_monto_maximo'                 => $simplificado_monto_maximo,
                        'facematch_simplificado'                    => ($request->facematch_simplificado) ? 1 : 0,
                        'carga_identificacion_selfie_simplificado'  => ($request->carga_identificacion_selfie_simplificado) ? 1 : 0,
                        'facematch'                                 => ($request->facematch) ? 1 : 0,
                        'carga_identificacion_selfie'               => ($request->carga_identificacion_selfie) ? 1 : 0,
                        'captura_referencias'                       => ($request->captura_referencias) ? 1 : 0,
                        'sms_referencias'                           => ($request->envio_sms_referencias) ? 1 : 0,
                        'captura_cuenta_clabe'                      => ($request->captura_cuenta_clabe) ? 1 : 0,
                        'carga_comprobante_domicilio'               => ($request->carga_comprobante_domicilio) ? 1 : 0,
                        'carga_comprobante_ingresos'                => ($request->carga_comprobante_ingresos) ? 1 : 0,
                        'carga_certificados_deuda'                  => ($request->carga_certificados_deuda) ? 1 : 0,
                        'logo'                                      => $nombre_logo,
                        'campo_cobertura'                           => $request->campo_cobertura,
                        'consulta_alp'                              => ($request->consulta_alp) ? 1 : 0,
                        'consulta_buro'                             => ($request->consulta_buro) ? 1 : 0,
                        'garantia'                                  => ($request->garantia) ? 1 : 0,
                        'seguro'                                    => ($request->seguro) ? 1 : 0,
                        'vigencia_de'                               => $request->vigencia_de,
                        'vigente'                                   => ($request->vigente) ? 1 : 0,
                        'vigencia_hasta'                            => $vigencia_hasta,
                        'proceso_experian'                          => ($request->proceso_experian) ? 1 : 0,
                        'producto_experian'                         => $request->producto_experian,
                        'redireccion'                               => ($request->redireccion == '') ? null : $request->redireccion,
                        'redireccion_portal'                        => ($request->redireccion_portal == '') ? null : $request->redireccion_portal,
                        'no_aplica_oferta'                          => ($request->no_aplica_oferta) ? 1 : null
                    ]
                );

                if ($request->plazos != 'null' ) {

                    $plazos = explode(',', $request->plazos);
                    if ($request->plazo_default) {
                        $value_default = $request->plazo_default;
                    } else {
                        $value_default = array_first($plazos);
                    }

                    $plazos_data = [];
                    foreach ($plazos as $key => $value) {
                        if ($value_default == $value) {
                            data_fill($plazos_data, $value.'.default', 1);
                        } else {
                            data_fill($plazos_data, $value.'.default', 0);
                        }
                    }
                    $producto->plazos()->sync($plazos_data);

                }

                if ($request->finalidades != 'null' ) {
                    $finalidades = explode(',', $request->finalidades);
                    $producto->finalidades()->sync($finalidades);
                }

                if ($request->ocupaciones != 'null' ) {
                    $ocupaciones = explode(',', $request->ocupaciones);
                    $producto->ocupaciones()->sync($ocupaciones);
                }

                //DB::statement('ALTER TABLE catalogo_sepomex ADD COLUMN '.$request->campo_cobertura.' BOOLEAN NULL DEFAULT 0 AFTER cobertura');

                return response()->json([
                    'success' => true
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }

    }

    /**
     * Muestra la vista para editar un nuevo producto.
     *
     * @param  Integer $id Id del producto a actualizar.
     *
     * @return View        Vista del formulario para editar el producto.
     */
    public function editar($id) {

        $producto = Producto::with('plazos', 'finalidades', 'ocupaciones')->find($id);

        $finalidades = Finalidad::orderBy('id', 'asc')->get();
        $plazos = Plazo::orderBy('id', 'asc')->get();
        $ocupaciones = Ocupacion::orderBy('id', 'asc')->get();

        return view(
            'producto.editar',
            [
                'producto'      => $producto,
                'plazos'        => $plazos,
                'finalidades'   => $finalidades,
                'ocupaciones'   => $ocupaciones,
            ]
        );
    }

    /**
     * Actualiza los valores del producto en la tabla productos
     *
     * @param  Request $request Contiene los datos del producto enviados del
     * formulario. El único dato qe no se actualiza es el de campo_cobertura
     *
     * @return json             Resultado de actulizar el producto
     */
    public function update(Request $request) {

        $producto = Producto::findOrFail($request->producto_id);

        $errores = $this->validar($request, true);

        if (count($errores) == 0) {

            try {

                $nombre_logo = null;

                if (!$request->logo_name) {

                    if ($request->hasFile('logo')) {
                        $logo = $request->file('logo');
                        $nombre_logo = $logo->getClientOriginalName();
                        $logo->move(public_path().'/images/iconos', $nombre_logo);
                    }

                }

                $producto->tipo = $request->tipo;
                $producto->empresa = $request->empresa;
                $producto->monto_minimo = $request->monto_minimo;
                $producto->monto_maximo = $request->monto_maximo;
                $producto->bc_score = $request->bc_score;
                $producto->condicion = $request->condicion;
                $producto->micro_score = ($request->micro_score) ? $request->micro_score : null;
                $producto->sin_historial = ($request->sin_historial) ? 1 : 0;
                $producto->sin_cuentas_recientes = ($request->sin_cuentas_recientes) ? 1 : 0;
                $producto->edad_minima = $request->edad_minima;
                $producto->edad_maxima = $request->edad_maxima;
                $producto->cat = $request->cat;
                $producto->tasa_minima = $request->tasa_minima;
                $producto->tasa_fija = ($request->tasa_fija) ? 1 : 0;
                $producto->tasa_maxima = $request->tasa_maxima;
                $producto->comision_apertura = $request->comision_apertura;
                $producto->stored_procedure = $request->stored_procedure;
                if ($nombre_logo != null) {
                    $producto->logo = $nombre_logo;
                }
                $producto->doble_oferta = ($request->doble_oferta) ? 1 : 0;
                $producto->tipo_monto_do = $request->tipo_monto_do;
                $do_monto_maximo = null;
                if ($request->tipo_monto_do == 'definido') {
                    $producto->do_monto_maximo = $request->do_monto_maximo;
                } else {
                    $do_monto_maximo = $request->monto_maximo;
                }
                $producto->proceso_simplificado = ($request->proceso_simplificado) ? 1 : 0;
                $producto->tipo_monto_simplificado = $request->tipo_monto_simplificado;
                if ($request->tipo_monto_simplificado == 'definido') {
                    $producto->simplificado_monto_maximo = $request->simplificado_monto_maximo;
                    $producto->simplificado_monto_minimo = $request->simplificado_monto_minimo;
                } else {
                    $producto->simplificado_monto_maximo = null;
                    $producto->simplificado_monto_minimo = null;
                }
                $producto->facematch_simplificado = ($request->facematch_simplificado) ? 1 : 0;
                $producto->carga_identificacion_selfie_simplificado = ($request->carga_identificacion_selfie_simplificado) ? 1 : 0;
                $producto->facematch = ($request->facematch) ? 1 : 0;
                $producto->captura_referencias = ($request->captura_referencias) ? 1 : 0;
                $producto->sms_referencias = ($request->envio_sms_referencias) ? 1 : 0;
                $producto->captura_cuenta_clabe = ($request->captura_cuenta_clabe) ? 1 : 0;
                $producto->carga_comprobante_domicilio = ($request->carga_comprobante_domicilio) ? 1 : 0;
                $producto->carga_comprobante_ingresos = ($request->carga_comprobante_ingresos) ? 1 : 0;
                $producto->carga_certificados_deuda = ($request->carga_certificados_deuda) ? 1 : 0;
                $producto->carga_identificacion_selfie = ($request->carga_identificacion_selfie) ? 1 : 0;
                $producto->consulta_alp = ($request->consulta_alp) ? 1 : 0;
                $producto->consulta_buro = ($request->consulta_buro) ? 1 : 0;
                $producto->garantia = ($request->garantia) ? 1 : 0;
                $producto->seguro = ($request->seguro) ? 1 : 0;
                $producto->vigencia_de = $request->vigencia_de;
                $producto->vigente = ($request->vigente) ? 1 : 0;
                $producto->proceso_experian = ($request->proceso_experian) ? 1 : 0;
                $producto->producto_experian = $request->producto_experian;
                $producto->redireccion = ($request->redireccion == '') ? null : $request->redireccion;
                $producto->redireccion_portal = ($request->redireccion_portal == '') ? null : $request->redireccion_portal;
                $producto->no_aplica_oferta = ($request->no_aplica_oferta) ? 1 : null;

                if ($request->vigencia_hasta) {
                    $producto->vigencia_hasta = $request->vigencia_hasta;
                } else {
                    $producto->vigencia_hasta = null;
                }

                $producto->setVersionName('alias');
                $producto->save();

                if ($request->plazos != 'null' ) {

                    $producto->plazos()->detach();
                    $plazos = explode(',', $request->plazos);

                    if ($request->plazo_default) {
                        $value_default = $request->plazo_default;
                    } else {
                        $value_default = array_first($plazos);
                    }

                    $plazos_data = [];
                    foreach ($plazos as $key => $value) {
                        if ($value_default == $value) {
                            data_fill($plazos_data, $value.'.default', 1);
                        } else {
                            data_fill($plazos_data, $value.'.default', 0);
                        }
                    }
                    $producto->plazos()->sync($plazos_data);

                }

                if ($request->finalidades != 'null' ) {
                    $finalidades = explode(',', $request->finalidades);
                    $producto->finalidades()->sync($finalidades);
                }

                if ($request->ocupaciones != 'null' ) {
                    $ocupaciones = explode(',', $request->ocupaciones);
                    $producto->ocupaciones()->sync($ocupaciones);
                }

                return response()->json(['success' => true]);

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }
    }

    /**
     * Guarda un nuevo plazo.
     *
     * @param  Request $request Arreglo con los datos del plazo.
     *
     * @return json            Respuesta del guardado del plazo.
     */
    public function savePlazos(Request $request) {

        $messages = [
            'required'                  => '* El campo es requerido.',
            'numeric'                   => '* El campo debe contener un valor numérico entero válido.',
            'duracion.min'              => '* La duración debe ser mayor a 0',
            'unique_duracion_plazo'     => '* La duración y plazo elegidos ya existen',
        ];

        $validator = Validator::make($request->all(), [
            'plazo'    => 'required',
            'duracion' => 'required|numeric|min:1|unique_duracion_plazo:'. $request->plazo,
        ], $messages);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            try {

                $plazo = Plazo::updateOrCreate([
                    'plazo'     =>  $request->plazo,
                    'duracion'  =>  $request->duracion,
                ]);

                $plazo->clave = $plazo->id;
                $plazo->save();

                return response()->json([
                    'success' => true,
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }

    }

    /**
     * Guarda una nueva finalidad.
     *
     * @param  Request $request Arreglo con los datos de la finalidad.
     *
     * @return json            Respuesta del guardado de la finalidad.
     */
    public function saveFinalidad(Request $request) {

        $messages = [
            'required'  => '* El campo es requerido.',
            'alpha'     => '* El campo solo debe contener caracteres.',
            'unique'    => '* La finalidad ya existe.',
        ];

        $validator = Validator::make($request->all(), [
            'finalidad'    => 'required|alpha_numeric_spaces|unique:finalidades,finalidad',
        ], $messages);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            try {

                $finalidad = Finalidad::updateOrCreate([
                    'finalidad' =>  $request->finalidad,
                ]);

                return response()->json([
                    'success' => true,
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }

    }

    /**
     * Guarda una nueva ocupación.
     *
     * @param  Request $request Arreglo con los datos de la ocupación.
     *
     * @return json            Respuesta del guardado de la ocupación.
     */
    public function saveOcupacion(Request $request) {

        $messages = [
            'required'  => '* El campo es requerido.',
            'alpha'     => '* El campo solo debe contener caracteres.',
            'unique'    => '* La ocupación ya existe.',
        ];

        $validator = Validator::make($request->all(), [
            'ocupacion'    => 'required|alpha_numeric_spaces|unique:ocupaciones,ocupacion',
        ], $messages);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            try {

                $ocupacion = Ocupacion::updateOrCreate([
                    'ocupacion' =>  $request->ocupacion,
                ]);

                return response()->json([
                    'success' => true,
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }

    }

    /**
     * Contiene todas las validaciones para el producto
     *
     * @param  array   $request Arreglo que contiene la información del producto
     * @param  boolean $update  Determina si se esta creando o actualizando el
     * producto
     *
     * @return array            Arreglo que contiene los mensajes de error de la
     * validación
     */
    public function validar($request, $update = false) {

        if ($update == false) {

            $columnas_tabla = DB::select('SHOW COLUMNS FROM catalogo_sepomex');
            $columns = [];
            foreach ($columnas_tabla as $value) {
                $columns[] = $value->Field;
            }

            $validate_nombre = [
                'required',
                Rule::unique('productos'),
                'max:50'
            ];
            $validate_logo = [
                'required',
                'image',
                'dimensions:max_width=240,max_height=70',
            ];
            $validate_cobertura = [
                'required',
                'alpha_dash',
                Rule::notIn($columns),
            ];
        } else {
            $validate_nombre = [
                'required',
                Rule::unique('productos')->ignore($request->producto_id),
                'max:50'
            ];
            $validate_logo = [
                'image',
                'dimensions:max_width=240,max_height=70',
                'required_without:logo_name'
            ];
            $validate_cobertura = [];
        }

        $messages = [
            'required'                                              => '* El campo es requerido.',
            'numeric'                                               => '* El campo debe contener un valor numérico entero válido.',
            'cat.regex'                                             => '* El campo debe contener un valor porcentaje válido.',
            'alpha_dash'                                            => '* El campo solo puede contener letras, números y guiones bajos o medios',
            'image'                                                 => '* El campo debe ser una imagen válida.',
            'dimensions'                                            => '* Las dimensiones de la imagen deben ser menores o iguales a 240x70px.',
            'edad_maxima.different'                                 => '* La edad mínima y máxima no pueden ser iguales.',
            'monto_maximo.different'                                => '* El monto mínimo y máximo no pueden ser iguales.',
            'tasa_maxima.different'                                 => '* La tasa mínima y máxima no pueden ser iguales.',
            'after_or_equal'                                        => '* La fecha no puede ser menor a Hoy.',
            'plazos.not_in'                                         => '* Se debe seleccionar al menos un plazo.',
            'finalidades.not_in'                                    => '* Se debe seleccionar al menos una finalidad.',
            'ocupaciones.not_in'                                    => '* Se debe seleccionar al menos una ocupación.',
            'required_without'                                      => '* El campo es requerido.',
            'nombre_producto.unique'                                => '* El nombre del producto ya existe.',
            'campo_cobertura.not_in'                                => '* El nombre del campo cobertura ya existe.',
            'empresa.required_if'                                   => '* El campo es requerido.',
            'alpha_numeric_spaces'                                  => '* El campo solo puede contener letras, números y espacios',
            'tipo_monto_do.required_if'                             => '* Es necesario seleccionar un tipo de monto',
            'do_monto_maximo.required_if'                           => '* El campo es requerido',
            'tipo_monto_simplificado.required_if'                   => '* Es necesario seleccionar un tipo de monto',
            'simplificado_monto_maximo.required_if'                 => '* El campo es requerido',
            'simplificado_monto_minimo.required_if'                 => '* El campo es requerido',
            'simplificado_monto_maximo.different'                   => '* El monto mínimo y máximo no pueden ser iguales.',
            'carga_identificacion_selfie_simplificado.required_if'  => '* El campo es requerido si se quiere usar FaceMatch',
            'carga_identificacion_selfie.required_if'               => '* El campo es requerido si se quiere usar FaceMatch',
            'producto_experian.required_if'                         => '* El campo es requerido si se quiere activar el Proceso Experian',
            'captura_referencias.required_if'                       => '* El campo es requerido si se quiere activar el envio de SMS a Referencias',
            'consulta_buro.required_if'                             => '* El campo es requerido si se quiere activar el Proceso Experian',
        ];

        $number_double = "";

        $validator = Validator::make($request->all(), [

            'nombre_producto'                           => $validate_nombre,
            'tipo'                                      => 'required|alpha_numeric_spaces',
            'empresa'                                   => 'required_if:tipo,Nómina,tipo,Convenio|alpha_numeric_spaces',
            'cat'                                       => [
                'required',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/' // Double
            ],
            'comision_apertura'                         => [
                'required',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/' // Double
            ],
            'bc_score'                                  => 'required|numeric',
            'stored_procedure'                          => 'required|alpha_dash',
            'consulta_buro'                             => 'required_if:proceso_experian,on',
            'tipo_monto_do'                             => 'required_if:doble_oferta,on',
            'do_monto_maximo'                           => 'required_if:tipo_monto_do,definido|numeric',
            'tipo_monto_simplificado'                   => 'required_if:proceso_simplificado,on',
            'simplificado_monto_maximo'                 => 'required_if:tipo_monto_simplificado,definido|numeric|different:simplificado_monto_minimo',
            'simplificado_monto_minimo'                 => 'required_if:tipo_monto_simplificado,definido|numeric',
            'carga_identificacion_selfie_simplificado'  => 'required_if:facematch_simplificado,on',
            'carga_identificacion_selfie'               => 'required_if:facematch,on',
            'captura_referencias'                       => 'required_if:envio_sms_referencias,on',
            'producto_experian'                         => 'required_if:proceso_experian,on|alpha_numeric_spaces',
            'campo_cobertura'                           => $validate_cobertura,
            'monto_minimo'                              => 'required|numeric',
            'monto_maximo'                              => 'required|numeric|different:monto_minimo',
            'edad_minima'                               => 'required|numeric',
            'edad_maxima'                               => 'required|numeric|different:edad_minima',
            'tasa_minima'                               => [
                'required',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/' // Double
            ],
            'tasa_maxima'                               => [
                'required_without:tasa_fija',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/', // Double
                'different:tasa_minima'
            ],
            'logo'                                      => $validate_logo,
            'vigencia_de'                               => 'required|date_format:Y-m-d',
            'vigente'                                   => 'required_without:vigencia_hasta',
            'vigencia_hasta'                            => 'required_without:vigente|date_format:Y-m-d|different:vigencia_de',
            'plazos'                                    => Rule::notIn(['null']),
            'finalidades'                               => Rule::notIn(['null']),
            'ocupaciones'                               => Rule::notIn(['null']),
        ], $messages);

        return $validator->errors()->messages();

    }

    /**
     * Muestra la pantalla para cargar el csv con el listado de cps de cobertura
     * del producto.
     *
     * @param  Integer $id Id del producto

     * @return view
     */
    public function cobertura($id) {

        $producto = Producto::find($id);

        return view(
            'producto.cobertura',
            [
                'producto' => $producto
            ]
        );

    }

    /**
     * Actualiza la cobertura de códigos postales de un producto
     *
     * @param  Request $request Arreglo de datos enviados en el formulario
     *
     * @return json             Respuesta de actualizar la cobertura.
     */
    public function updateCobertura(Request $request) {

        $validator = Validator::make($request->all(), [
            'csv' => 'required|mimetypes:text/plain',
        ], [
            'required' => 'Se debe seleccionar un archivo csv.',
            'mimetypes'    => 'El archivo debe ser tipo csv.'
        ]);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            try {

                if ($request->hasFile('csv')) {

                    $csv = $request->file('csv');
                    $campo_cobertura = $request->campo_cobertura;
                    $nombre_csv = $csv->getClientOriginalName();
                    $csv->move(storage_path().'/app/public/cps_cobertura/', $nombre_csv);

                    $rows = \Excel::load(storage_path().'/app/public/cps_cobertura/'.$nombre_csv, function($reader) {
                        $reader->toArray();
                    })->get();

                    $lista_cp = [];
                    foreach ($rows as $row) {
                        $cp = $row->cp;
                        if (strlen($cp) == 4) {
                            $cp = '0'.$cp;
                        }
                        $lista_cp[] = $cp;
                    }

                    if ($campo_cobertura == 'cobertura_cambaceo') {
                        $producto = Producto::with('cobertura')->find(4);
                        $producto->cobertura()->detach();
                        $producto->cobertura()->attach($lista_cp);

                        return response()->json([
                            'success' => true,
                        ]);

                    } else {

                        CatalogoSepomex::whereIn('codigo', $lista_cp)->update([$campo_cobertura => 1]);
                        return response()->json([
                            'success' => true,
                        ]);
                    }

                }

            } catch (\Exception $e) {

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);

            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Faltan datos obligatorios.',
                'errores' => $errores,
            ]);

        }

    }



}
