<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App;
use Validator;
use Auth;
use Illuminate\Validation\Rule;
use App\Situacion;
use App\RespuestaCuestionarioDinamico;
use App\RespuestaMaquinaRiesgo;
use App\Solicitud;
use App\ClientesAlta;
use App\Jobs\AltaReferencias;
use App\ReferenciasSolicitud;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use App\Repositories\SolicitudRepository;

class ReferenciasController extends Controller
{
    private $solicitudRepository;

    public function __construct() {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function index() {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id);
        $pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            return view('webapp.referencias', ['pasos_habilitados' => $pasos_habilitados]);
        } else {
            return $siguientePaso;
        }

    }

    public function save(request $request)
    {
        if (Auth::guard('prospecto')->check()) {

            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;

            $validaciones = $this->validaciones($request->all());
            if ($validaciones->fails()) {
                return response()->json([
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ]);
            }

            $nombre_ref1 = explode(' ', mb_strtoupper($request->nombre_ref1), 2);
            $nombre_ref2 = explode(' ', mb_strtoupper($request->nombre_ref2), 2);
            $nombre_ref3 = explode(' ', mb_strtoupper($request->nombre_ref3), 2);

            $referencias = ReferenciasSolicitud::updateOrCreate([
                'solicitud_id'          => $solicitud_id
            ], [
                'primer_nombre_ref1'    => $nombre_ref1[0],
                'segundo_nombre_ref1'   => isset($nombre_ref1[1]) ? $nombre_ref1[1] : '',
                'apellido_paterno_ref1' => mb_strtoupper($request->apellido_paterno_ref1),
                'apellido_materno_ref1' => mb_strtoupper($request->apellido_materno_ref1),
                'tipo_relacion_ref1'    => 13,
                'telefono_ref1'         => $request->telefono_ref1,
                'primer_nombre_ref2'    => $nombre_ref2[0],
                'segundo_nombre_ref2'   => isset($nombre_ref2[1]) ? $nombre_ref2[1] : '',
                'apellido_paterno_ref2' => mb_strtoupper($request->apellido_paterno_ref2),
                'apellido_materno_ref2' => mb_strtoupper($request->apellido_materno_ref2),
                'tipo_relacion_ref2'    => 20,
                'telefono_ref2'         => $request->telefono_ref2,
                'primer_nombre_ref3'    => $nombre_ref3[0],
                'segundo_nombre_ref3'   => isset($nombre_ref3[1]) ? $nombre_ref3[1] : '',
                'apellido_paterno_ref3' => mb_strtoupper($request->apellido_paterno_ref3),
                'apellido_materno_ref3' => mb_strtoupper($request->apellido_materno_ref3),
                'tipo_relacion_ref3'    => 20,
                'telefono_ref3'         => $request->telefono_ref3
            ]);

            $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->orderBy('id', 'desc')
                ->first();

            $job = (new AltaReferencias($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
            dispatch($job);

            CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->update(['referencias_completo' => 1]);

            $log = [
                'success' => true,
                'msj'     => 'Referencias guardadas',
            ];
            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
            $solicitud->save();

            $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, false);

            if ($siguientePaso == null) {

                if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                    $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                } else {
                    $id_plantilla = 89;
                }

                $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                            ->where('plantilla_id', $id_plantilla)
                            ->get()
                            ->toArray();
                $tituloModal = $plantilla[0]['modal_encabezado'];
                $imgModal = $plantilla[0]['modal_img'];
                $cuerpoModal = $plantilla[0]['modal_cuerpo'];

                $producto = null;
                if ($solicitud->producto()->exists()) {
                    $producto = $solicitud->producto[0]['alias'];
                }

                $faltanDocumentos = $this->solicitudRepository->faltanDocumentos($prospecto_id, $solicitud_id, $producto);

                if ($faltanDocumentos != null) {
                    $cuerpoModal = str_replace('{{ lista_documentos }}', $faltanDocumentos, $cuerpoModal);
                } else {
                    $tituloModal = __('messages.titulo_modal_carga_documentos_terminada');
                    $cuerpoModal = __('messages.cuerpo_modal_carga_documentos_terminada');
                }
                $modal = view("modals.modalStatusSolicitudDocumentos", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();

                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Oferta Aceptada - Carga Documentos Completada',
                    1,
                    'Se ha completado la carga de documentos en el Panel Operativo'
                );
                $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                $solicitud->save();

                Auth::guard('prospecto')->logout();

                return response()->json([
                    'success'           => true,
                    'siguiente_paso'    => $siguientePaso,
                    'modal'             => $modal,
                    'message'           => 'Referencias guardadas con éxito'
                ]);

            } else {

                return response()->json([
                    'success'           => true,
                    'siguiente_paso'    => $siguientePaso,
                    'message'           => 'Referencias guardadas con éxito'
                ]);

            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true
            ]);

        }

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    function validaciones($datos) {

        $reglas = [
            'nombre_ref1'           => 'required|alpha_spaces_not_html',
            'apellido_paterno_ref1' => 'required|alpha_spaces_not_html',
            'apellido_materno_ref1' => 'alpha_spaces_not_html',
            'telefono_ref1'         => 'required|digits:10',
            'nombre_ref2'           => 'required|alpha_spaces_not_html',
            'apellido_paterno_ref2' => 'required|alpha_spaces_not_html',
            'apellido_materno_ref2' => 'alpha_spaces_not_html',
            'telefono_ref2'         => 'required|digits:10',
            'nombre_ref3'           => 'required|alpha_spaces_not_html',
            'apellido_paterno_ref3' => 'required|alpha_spaces_not_html',
            'apellido_materno_ref3' => 'alpha_spaces_not_html',
            'telefono_ref3'         => 'required|digits:10'
        ];
        $mensajes = [
            'required'              => 'El campo es obligatorio.',
            'alpha_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'digits'                => 'El campo debe tener :digits números.',
        ];

        $validaciones = Validator::make($datos, $reglas, $mensajes);
        return $validaciones;

    }

    /**
     * Genera el job para procesamiento del alta de las Referencias mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function procesar(Request $request) {

        $clienteAlta = ClientesAlta::find($request->idAltaCliente);
        $job = (new AltaReferencias($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
        dispatch($job);

        return response()->json(['success' => true]);

    }

    public function siguientePaso($prospecto_id, $solicitud_id, $redirect = true) {

        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        if ($siguientePaso != 'referencias' && $redirect == true) {
            return redirect('/webapp/'.$siguientePaso);
        } elseif ($siguientePaso != 'referencias' && $redirect == false) {
            return $siguientePaso;
        } else {
            return null;
        }

    }

}
