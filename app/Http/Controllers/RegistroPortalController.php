<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitud;
use App\Prospecto;
use App\Analytic;
use App\Finalidad;
use App\Plazo;
use App\DomicilioSolicitud;
use App\DatosAdicionales;
use App\Producto;
use App\CatalogoSepomex;
use App\UserConvenio;
use App\DatosConvenio;
use App\MotivoRechazo;
use App\RespuestaCuestionarioDinamico;
use App\Situacion;
use App\CuestionarioDinamico;
use App\TrackingSolicitud;
use App\DatosEmpleo;
use App\ProductoCobertura;
use Carbon\Carbon;
use App\Etiquetas;
use Jenssegers\Agent\Agent;
use App;
use Cookie;
use RFC\RfcBuilder;
use App\CodigosSMS;
use Validator;
use Hash;
use Auth;
use App\Repositories\LDAPRepository;
use App\Repositories\CalixtaRepository;
use App\Repositories\SolicitudRepository;
use App\Repositories\CognitoRepository;
use App\W_USER;
use App\Repositories\CurlCaller;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Validation\Rules\Exists;

class RegistroPortalController extends Controller
{
    private $configuracionProducto;
    private $ocupaciones;
    private $solicitudRepository;
    


    public function __construct(request $request) 
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        $prospecto = Auth::guard('prospecto')->check();
        if ($prospecto == true) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $solicitud = Solicitation::with('producto')
                ->where('id', $prospecto_id)
                ->get()
                ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );
        $this->ocupaciones = collect($this->configuracionProducto['ocupaciones'])->pluck('ocupacion');
        $this->solicitudRepository = new SolicitudRepository;

    }

    public function validaciones(Request $request, $formulario = null) {

        $datos = $request->datosSolicitud;

        $validaciones = $this->validacionesPortal($datos, $formulario);
        if ($validaciones->fails()) {
            return [
                'sucess'    => false,
                'errors'    => $validaciones->errors()
            ];
        } 
    }

    public function registroProspecto(Request $request) {

        $datosSolicitud = $request['datosSolicitud'];
        if(Auth::guard('captura_convenio')->check()) {
            $datosSucursal = [
                'empresa'   => Auth::guard('captura_convenio')->user()->empresa,
                'sucursal' 	=> Auth::guard('captura_convenio')->user()->sucursal,
                'email'		=> Auth::guard('captura_convenio')->user()->email,
                'telefono'	=> Auth::guard('captura_convenio')->user()->telefono,
                'embajador'	=> Auth::guard('captura_convenio')->user()->embajador,
                'email'	    => Auth::guard('captura_convenio')->user()->email
            ];

            $empresa = Auth::guard('captura_convenio')->user()->empresa;

        } else {
            $datosSucursal = [
                'empresa'   => '',
                'sucursal'  => '',
                'email'     => '',
                'telefono'  => '',
                'embajador' => '',
                'email'     => '',
            ];
        }
        if($datosSucursal['empresa'] == 'callcenter') {
            $finalidad_custom = 'Solicitud registrada desde portal Call Center en llamada telefónica';
            $referencia = 'Portal CC';
            $pass_temp = 1;
        } elseif ($datosSucursal['empresa'] == 'sucursal') {
            $finalidad_custom = 'Solitud registrada por ejecutivo en Sucursal '.$datosSucursal['sucursal'];
            $referencia = 'Portal SUC';
            $pass_temp = 0;
        } else {
            $finalidad_custom = 'Solicitud registrada desde el portal de cambaceo.Solicitud registrada desde el portal de cambaceo';
            $referencia = 'Portal CAMB';
            $pass_temp = 0;
        }
        //$finalidad_custom = 'Solicitud registrada desde portal Call Center en llamada telefónica';
        $validaciones = $this->validaciones($request, 'registro');
        
        if ($validaciones != '') {
            return response()->json($validaciones);
        }
        $datos_registro = $request->datosSolicitud;
        $cognito_id = null;
        $password = $datosSolicitud['password'];
        
        $user_exist= Prospecto::where('email',$datosSolicitud['email'])
                ->first();
                
        $responseCognito = $this->registroCognito($request, $password);

        if ($responseCognito['success'] == false) {
            $responseCognito['prospect_id'] = $user_exist->id;
            $responseCognito['nueva_solicitud'] = true;
            return response()->json($responseCognito);
            
        } else {
            $cognito_id = $responseCognito['cognito_id'];
        }

        $prospecto = Prospecto::firstOrCreate([
            'email' => mb_strtolower($datos_registro['email'])
        ]);

        // Si el prospecto fue creado recientemente, se actualizan atributos
        // faltantes y se crea solicitud
        if ($prospecto->wasRecentlyCreated) {

            $prospecto->nombres               = trim(mb_strtoupper($datos_registro['nombres']));
            $prospecto->apellido_paterno      = trim(mb_strtoupper($datos_registro['apellido_paterno']));
            $prospecto->apellido_materno      = trim(mb_strtoupper($datos_registro['apellido_materno']));
            $prospecto->celular               = $datos_registro['celular'];
            $prospecto->password              = Hash::make($password);
            $prospecto->encryptd              = encrypt($password);
            $prospecto->usuario_ldap          = 1;
            $prospecto->sms_verificacion      = 0;
            $prospecto->usuario_confirmado    = 0;
            $prospecto->password_temporal     = $pass_temp;  
            $prospecto->referencia            = $referencia;
            $prospecto->cognito_id            = $cognito_id;
            $prospecto->save();

            $w_user = W_USER::updateOrCreate([
                'ID_USER'       => mb_strtolower($datos_registro['email'])
            ], [
                'PHONE_NUMBER'  => $datos_registro['celular'],
                'CREATE_DATE'   => Carbon::now(),
                'UPDATE_DATE'   => Carbon::now()
            ]);

            // Obteniendo la descripcion de la finalidad
            $finalidad = $datos_registro['finalidad_custom'];
            $finalidad = Finalidad::find($finalidad);
            if (isset($finalidad->finalidad)) {
                $finalidad = mb_strtoupper($finalidad->finalidad);
            }

            // Obteniendo la clave del plazo
            $plazo = $datos_registro['plazo'];
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (isset($plazo->clave)) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }
            $pago_estimado = trim($datos_registro['pago_estimado'],"$");
            $pgo_estimado = str_replace(",", "", $pago_estimado);
            
            $solicitud = Solicitud::create([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $datos_registro['monto_prestamo'],
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $pgo_estimado,
                'finalidad'         => $finalidad,
                'finalidad_custom'  => $finalidad_custom
            ]);

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($datos_registro['email'], ['custom:idProspecto' => 'FMP-'.$prospecto->id]);

            $r_parameter = 'none';
            $campaña = 'none';
            if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                $r_parameter = Cookie::get('r_parameter');
            }
            $campaña = $campaña == 'none' ? null : $campaña;
            $r_parameter = $r_parameter == 'none' ? null : $r_parameter;
            $producto_id = $datosSolicitud['producto_id'];
            //$version_producto = $this->configuracionProducto['version_producto'];
            $version_producto = Producto::find($producto_id)->getCurrentVersionNo();

            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                    'lead'             => $campaña,
                    'lead_id'          => $r_parameter,
                ]
            ]);

            Cookie::queue(Cookie::forget('campaña'));
            Cookie::queue(Cookie::forget('r_parameter'));


            $datosConvenio = DatosConvenio::updateOrCreate([
                'prospecto_id'  => $prospecto->id,
                'solicitud_id'  => $solicitud->id
            ], [
                'empresa'   => $datosSucursal['empresa'],
                'sucursal'  => $datosSucursal['sucursal'],
                'embajador' => $datosSucursal['embajador'],
                'telefono'  => $datosSucursal['telefono'],
                'email'     => $datosSucursal['email']
            ]);

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if ($agent->isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = $agent->device();
            }
            if ($agent->isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = $agent->device();
            }
            if ($agent->isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = $agent->device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $datos_registro['clientId'],
                'email'             => $datos_registro['email'],
                'ip'                => self::getUserIP(),
                'telefono'          => $datos_registro['celular'],
                'sistema_operativo' => $agent->platform(),
                'navegador'         => $agent->browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id
            ];

            $name_string = $datos_registro['nombres'].' '.$datos_registro['apellido_paterno'].' '.$datos_registro['apellido_materno'];
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Usuario y Solicitud creados con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $eventTM[] = ['event'=> 'RegistroUsuario', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $datos_registro['clientId'], 'producto' => strtoupper($datos_registro['alias'])];
            $mnsj_str['eventTM'] = $eventTM;

            return response()->json($mnsj_str, 201);

        } elseif($prospecto->id) {

            if ($prospecto->cognito_id == null) {
                $prospecto->cognito_id = $cognito_id;
                $prospecto->save();
            } else {
                return response()->json([
                    'success'           => false,
                    'msg'               => 'El usuario ya esta registrado',
                    'siguiente_paso'    => 'login',

                ]);
            }

        } else {

            return response()->json([
                'success' => false,
                'msg'     => 'Hubo un error al crear el prospecto y la solicitud',
            ]);

        }

    }

    public function editarProspecto(Request $request) {

        $datosSolicitud = $request['datosSolicitud'];
        if(Auth::guard('captura_convenio')->check()) {
            $datosSucursal = [
                'empresa'   => Auth::guard('captura_convenio')->user()->empresa,
                'sucursal' 	=> Auth::guard('captura_convenio')->user()->sucursal,
                'email'		=> Auth::guard('captura_convenio')->user()->email,
                'telefono'	=> Auth::guard('captura_convenio')->user()->telefono,
                'embajador'	=> Auth::guard('captura_convenio')->user()->embajador,
                'email'	    => Auth::guard('captura_convenio')->user()->email
            ];

            $empresa = Auth::guard('captura_convenio')->user()->empresa;

        } else {
            $datosSucursal = [
                'empresa'   => '',
                'sucursal'  => '',
                'email'     => '',
                'telefono'  => '',
                'embajador' => '',
                'email'     => '',
            ];
        }

        if($datosSucursal['empresa'] == 'callcenter') {
            $finalidad_custom = 'Solicitud registrada desde portal Call Center en llamada telefónica';
            $referencia = 'Portal CC';
            $pass_temp = 1;
        } elseif ($datosSucursal['empresa'] == 'sucursal') {
            $finalidad_custom = 'Solitud registrada por ejecutivo en Sucursal '.$datosSucursal['sucursal'];
            $referencia = 'Portal SUC';
            $pass_temp = 0;
        } else {
            $finalidad_custom = 'Solicitud registrada desde el portal de cambaceo.Solicitud registrada desde el portal de cambaceo';
            $referencia = 'Portal CAMB';
            $pass_temp = 0;
        }
        //$finalidad_custom = 'Solicitud registrada desde portal Call Center en llamada telefónica';
        $validaciones = $this->validaciones($request, 'editar_registro');
        
        if ($validaciones != '') {
            return response()->json($validaciones);
        }
        $datos_registro = $request->datosSolicitud;
        
        // Obteniendo la descripcion de la finalidad
        $finalidad = $datos_registro['finalidad_custom'];
        $finalidad = Finalidad::find($finalidad);
        if (isset($finalidad->finalidad)) {
            $finalidad = mb_strtoupper($finalidad->finalidad);
        }

        // Obteniendo la clave del plazo
        $plazo = $datos_registro['plazo'];
        $duracion = null;
        $clave_plazo = null;
        $plazo = Plazo::find($plazo);

        if (isset($plazo->clave)) {
            $clave_plazo = $plazo->clave;
            $duracion = $plazo->duracion;
            $plazo = mb_strtoupper($plazo->plazo);
        } else {
            $plazo = Plazo::find(1);
            $clave_plazo = $plazo->clave;
            $duracion = $plazo->duracion;
            $plazo = mb_strtoupper($plazo->plazo);
        }
        $pago_estimado = trim($datos_registro['pago_estimado'],"$");
        $pgo_estimado = str_replace(",", "", $pago_estimado);

        $solicitud = Solicitud::where('prospecto_id', $request->prospecto_id,)
            ->orderBy('created_at', 'DESC')
            ->first();

        $prospecto = Prospecto::where('id', $request->prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();
            
        $prospecto = Prospecto::updateOrCreate([
            'id' => $request->prospecto_id
        ],  [
            'nombres'               => trim(mb_strtoupper($datos_registro['nombres'])),
            'apellido_paterno'      => trim(mb_strtoupper($datos_registro['apellido_paterno'])),
            'apellido_materno'      => trim(mb_strtoupper($datos_registro['apellido_materno']))

        ]);
        $solicitud = Solicitud::updateOrCreate([
            'prospecto_id'  => $request->prospecto_id,
            'id'  => $solicitud->id
        ],  [
            //'status'            => 'Registro',
            //'sub_status'        => 'Crear Usuario',
            'prospecto_id'      => $request->prospecto_id,
            'prestamo'          => $datos_registro['monto_prestamo'],
            'plazo'             => $clave_plazo,
            'pago_estimado'     => $pgo_estimado,
            'finalidad'         => $finalidad,
            'finalidad_custom'  => $finalidad_custom
        ]);

        /* $cognito = new CognitoRepository;
        $responseCognito = $cognito->setUserAttributes($datos_registro['email'], ['custom:idProspecto' => 'FMP-'.$prospecto->id]); */

        $r_parameter = 'none';
        $campaña = 'none';
        if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
            $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
            $r_parameter = Cookie::get('r_parameter');
        }
        $campaña = $campaña == 'none' ? null : $campaña;
        $r_parameter = $r_parameter == 'none' ? null : $r_parameter;
        $producto_id = $datosSolicitud['producto_id'];
        //$version_producto = $this->configuracionProducto['version_producto'];
        $version_producto = Producto::find($producto_id)->getCurrentVersionNo();

        $solicitud->producto()->attach([
            $producto_id => [
                'version_producto' => $version_producto,
                'lead'             => $campaña,
                'lead_id'          => $r_parameter,
            ]
        ]);

        Cookie::queue(Cookie::forget('campaña'));
        Cookie::queue(Cookie::forget('r_parameter'));


        $datosConvenio = DatosConvenio::updateOrCreate([
            'prospecto_id'  => $request->prospecto_id,
            'solicitud_id'  => $solicitud->id
        ], [
            'empresa'   => $datosSucursal['empresa'],
            'sucursal'  => $datosSucursal['sucursal'],
            'embajador' => $datosSucursal['embajador'],
            'telefono'  => $datosSucursal['telefono'],
            'email'     => $datosSucursal['email']
        ]);

        $agent = new Agent();
        $tipo_dispositivo = null;
        $marca =  null;

        if ($agent->isDesktop()) {
            $tipo_dispositivo = 'desktop';
            $marca = $agent->device();
        }
        if ($agent->isMobile()) {
            $tipo_dispositivo = 'mobile';
            $marca = $agent->device();
        }
        if ($agent->isTablet()) {
            $tipo_dispositivo = 'tablet';
            $marca = $agent->device();
        }

        $analytic = Analytic::insert([
            'prospecto_id'      =>$request->prospecto_id,
            'solicitud_id'      => $solicitud->id,
            'client_id'         => $datos_registro['clientId'],
            'email'             => $datos_registro['email'],
            'ip'                => self::getUserIP(),
            'telefono'          => $datos_registro['celular'],
            'sistema_operativo' => $agent->platform(),
            'navegador'         => $agent->browser(),
            'tipo_dispositivo'  => $tipo_dispositivo,
            'marca'             => $marca,
            'datos_obtenidos'   => 'Sin datos',
            'fecha_hora'        => date('YmdHi'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        ]);

        $mnsj_str = [
            'success'           => true,
            'login'             => false,
            'nueva_solicitud'   => true,
            'msj'               => 'Prospecto y solicitud creados',
            'siguiente_paso'    => 'sms',
            'prospecto_id'      => $request->prospecto_id,
            'solicitud_id'      => $solicitud->id,
            'sub_status'        => $solicitud->sub_status
        ];

        $name_string = $datos_registro['nombres'].' '.$datos_registro['apellido_paterno'].' '.$datos_registro['apellido_materno'];
/*         $this->solicitudRepository->setUltPuntoReg($solicitud->id,
            $solicitud->status,
            'Crear Usuario',
            1,
            'Solicitud editada con éxito',
            ['prospecto_id' => $request->prospecto_id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
        );

        $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
        $solicitud->save(); */

        $eventTM[] = ['event'=> 'RegistroUsuario', 'userId'=> $request->prospecto_id, 'solicId' => $solicitud->id, 'clientId' => $datos_registro['clientId'], 'producto' => strtoupper($datos_registro['alias'])];
        $mnsj_str['eventTM'] = $eventTM;

        return response()->json($mnsj_str, 201);
    }

    public function registroSolicitud(Request $request) {

        $datosSolicitud = $request['datosSolicitud'];
        if(Auth::guard('captura_convenio')->check()) {
            $datosSucursal = [
                'empresa'   => Auth::guard('captura_convenio')->user()->empresa,
                'sucursal' 	=> Auth::guard('captura_convenio')->user()->sucursal,
                'email'		=> Auth::guard('captura_convenio')->user()->email,
                'telefono'	=> Auth::guard('captura_convenio')->user()->telefono,
                'embajador'	=> Auth::guard('captura_convenio')->user()->embajador,
                'email'	    => Auth::guard('captura_convenio')->user()->email
            ];

            $empresa = Auth::guard('captura_convenio')->user()->empresa;

        } else {
            $datosSucursal = [
                'empresa'   => '',
                'sucursal'  => '',
                'email'     => '',
                'telefono'  => '',
                'embajador' => '',
                'email'     => '',
            ];
        }
        $finalidad_custom = 'Solicitud registrada desde portal Sucursal con un ejecutivo';
        $datos_registro = $request->datosSolicitud;

         // Obteniendo la descripcion de la finalidad
         $finalidad = $datos_registro['finalidad_custom'];
         $finalidad = Finalidad::find($finalidad);
         if (isset($finalidad->finalidad)) {
             $finalidad = mb_strtoupper($finalidad->finalidad);
         } else {
            $finalidad = Finalidad::find(1);
            $finalidad = mb_strtoupper($finalidad->finalidad);
         }
         // Obteniendo la clave del plazo
         $plazo = $datos_registro['plazo'];
         $duracion = null;
         $clave_plazo = null;
         $plazo = Plazo::find($plazo);

         if (isset($plazo->clave)) {
             $clave_plazo = $plazo->clave;
             $duracion = $plazo->duracion;
             $plazo = mb_strtoupper($plazo->plazo);
         } else {
             $plazo = Plazo::find(1);
             $clave_plazo = $plazo->clave;
             $duracion = $plazo->duracion;
             $plazo = mb_strtoupper($plazo->plazo);
         }
         $pago_estimado = trim($datos_registro['pago_estimado'],"$");
         $pgo_estimado = str_replace(",", "", $pago_estimado);

        $solicitud = Solicitud::updateOrCreate([
            'status'            => 'Registro',
            'sub_status'        => 'Crear Usuario',
            'prospecto_id'      => $request->prospecto_id,
            'prestamo'          => $datos_registro['monto_prestamo'],
            'plazo'             => $clave_plazo,
            'pago_estimado'     => $pgo_estimado,
            'finalidad'         => $finalidad,
            'finalidad_custom'  => $finalidad_custom,
        ]);

        $mnsj_str = [
            'success'           => true,
            'login'             => true,
            'nueva_solicitud'   => true,
            'prospecto_id'      => $request->prospecto_id,
            'solicitud_id'      => $solicitud->id,
            'celular'           => $datos_registro['celular'],
        ];

        $prospecto = Prospecto::where('id',$request->prospecto_id)
            ->first();

        if ($solicitud->wasRecentlyCreated) {
            $r_parameter = 'none';
            $campaña = 'none';
            if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                $r_parameter = Cookie::get('r_parameter');
            }
            $campaña = $campaña == 'none' ? null : $campaña;
            $r_parameter = $r_parameter == 'none' ? null : $r_parameter;
            $producto_id = $datosSolicitud['producto_id'];
            $version_producto = Producto::find($producto_id)->getCurrentVersionNo();

            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                    'lead'             => $campaña,
                    'lead_id'          => $r_parameter,
                ]
            ]);

            Cookie::queue(Cookie::forget('campaña'));
            Cookie::queue(Cookie::forget('r_parameter'));
            
            $datosConvenio = DatosConvenio::updateOrCreate([
                'prospecto_id'  => $request->prospecto_id,
                'solicitud_id'  => $solicitud->id
            ], [
                'empresa'   => $datosSucursal['empresa'],
                'sucursal'  => $datosSucursal['sucursal'],
                'embajador' => $datosSucursal['embajador'],
                'telefono'  => $datosSucursal['telefono'],
                'email'     => $datosSucursal['email']
            ]);

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if ($agent->isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = $agent->device();
            }
            if ($agent->isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = $agent->device();
            }
            if ($agent->isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = $agent->device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $datosSolicitud['clientId'],
                'email'             => $prospecto->email,
                'ip'                => self::getUserIP(),
                'telefono'          => $prospecto->celular,
                'sistema_operativo' => $agent->platform(),
                'navegador'         => $agent->browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Nueva solicitud creada con éxito',
                ['prospecto_id' => $request->prospecto_id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

        }

        session()->forget('nueva_solicitud');
        return response()->json($mnsj_str, 201);
        
    }

    public function registroDomicilio(request $request) {

        $prospecto_id = $request->prospecto_id;
        $prospecto = Prospecto::find($prospecto_id);
        $producto_id = $request->producto_id;
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {

            $validaciones = $this->validaciones($request, 'domicilio');
            if ($validaciones != '') {
                return response()->json($validaciones);
            }
            $datos_domicilio = $this->buscarDatosDomicilio($request, $producto_id);
            
            // Guardando los datos de la solicitud desencriptados en la nueva tabla
            $domicilio = DomicilioSolicitud::firstOrNew(
                [
                'prospecto_id'  => $prospecto->id,
                'solicitud_id'  => $solicitud->id
            ]);

            $domicilio->cp              = $request->datosSolicitud['codigo_postal'];
            $domicilio->calle           = mb_strtoupper($request->datosSolicitud['calle']);
            $domicilio->num_exterior    = mb_strtoupper($request->datosSolicitud['no_ext']);
            $domicilio->num_interior    = mb_strtoupper($request->datosSolicitud['no_int']);
            $domicilio->colonia         = mb_strtoupper($request->datosSolicitud['colonia']);
            $domicilio->id_colonia      = $request->datosSolicitud['id_colonia'];
            $domicilio->delegacion      = mb_strtoupper($request->datosSolicitud['delegacion_municipio']);
            $domicilio->id_delegacion   = $request->datosSolicitud['id_delegacion_municipio'];
            $domicilio->ciudad          = mb_strtoupper($request->datosSolicitud['ciudad']);
            $domicilio->id_ciudad       = $request->datosSolicitud['id_ciudad'];
            $domicilio->estado          = mb_strtoupper($request->datosSolicitud['estado']);
            $domicilio->id_estado       = $request->datosSolicitud['id_estado'];
            $domicilio->codigo_estado   = $request->datosSolicitud['codigo_estado'];
            if (count($datos_domicilio) > 1) {
                $domicilio->cp            = $datos_domicilio['codigopostal'];
                $domicilio->id_estado     = $datos_domicilio['id_estado'];
                $domicilio->codigo_estado = $datos_domicilio['codigo_estado'];
                $domicilio->id_delegacion = $datos_domicilio['id_delegacion'];
                $domicilio->id_ciudad     = $datos_domicilio['id_ciudad'];
                $domicilio->id_colonia    = $datos_domicilio['id_colonia'];
                $domicilio->cobertura     = $datos_domicilio['cobertura'];
                if ($datos_domicilio['ciudad'] == '' && $datos_domicilio['ciudad'] != '') {
                    $domicilio->ciudad = $datos_domicilio['ciudad'];
                }
            }
            $domicilio->save();

            // Se actualiza la solicitud con los datos del domicilio
            $dom_string = $request->datosSolicitud['calle'].' '
                .$request->datosSolicitud['no_ext'].' '
                .$request->datosSolicitud['no_int'].', '
                .$request->datosSolicitud['colonia'].', '
                .$request->datosSolicitud['delegacion_municipio'].', '
                .$request->datosSolicitud['codigo_postal'].', '
                .$request->datosSolicitud['ciudad'].', '
                .$request->datosSolicitud['estado'];

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Domicilio',
                1,
                'Los datos de domicilio se han guardado con éxito',
                ['domicilio' => $dom_string]
            );

            $mnsj_str = [
                'success'       => true,
                'domicilio'     => $dom_string
            ];

            // Guardando el mensaje en el campo ult_mensj_a_usuario
            $this->solicitudRepository->setUltMnsjUsr(
                $solicitud,
                json_encode($mnsj_str)
            );
            $solicitud->sub_status = 'Domicilio';
            $solicitud->save();

            return response()->json($mnsj_str);
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La solicitud no existe'
            ]);
        }
    }

    public function datosPersonales(request $request)
    {
        $prospecto_id = $request->prospecto_id;
        $conf_code = $request->input('conf_code');

        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {

            $validaciones = $this->validaciones($request, 'datos_personales');
            if ($validaciones != '') {
                return response()->json($validaciones);
            }

            $fecha_nacimiento = Carbon::createFromFormat('Y-m-d', $request->datosSolicitud['fecha_nacimiento']);

            $rfc = $request->datosSolicitud['rfc'];
            //$error_rfc = $request->datosSolicitud['error_rfc'];

            if ($rfc == '' ) {

                try {

                    $builder = new RfcBuilder();
                    $dia = $fecha_nacimiento->format('d');
                    $mes = $fecha_nacimiento->format('m');
                    $año = $fecha_nacimiento->format('Y');
                    $nombre = ($solicitud->prospecto->nombres);
                    $apellido_paterno = $solicitud->prospecto->apellido_paterno;
                    $apellido_materno = $solicitud->prospecto->apellido_materno;
                    if ($apellido_materno == 'XXX') {
                        $apellido_materno = '';
                    }

                    $rfc = $builder->name($nombre)
                        ->firstLastName($apellido_paterno)
                        ->secondLastName($apellido_materno)
                        ->birthday($dia, $mes, $año)
                        ->build()
                        ->toString();

                    $rfc = substr($rfc, 0, 10);

                    $mnsj_str = [
                        'success'   => true,
                        'message'   => 'RFC calculado: '.$rfc
                    ];

                    // Guardando el mensaje en el campo ult_mensj_a_usuario
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );

                } catch (\Exception $e) {

                    $mnsj_str = [
                        'success'   => false,
                        'message'   => 'Error al calcular RFC: '. $e->getMessage(),
                    ];

                    // Guardando el mensaje en el campo ult_mensj_a_usuario
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    $rfc = '';
                }

            } else {

                $mnsj_str = [
                    'success'   => true,
                    'message'   => 'RFC Capturado.',
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->save();
            }

            // Actualiza los datos personales de la solicitud
            $solicitud->sexo                    = $request->datosSolicitud['sexo'];
            $solicitud->fecha_nacimiento        = $fecha_nacimiento;
            $solicitud->lugar_nacimiento_estado = mb_strtoupper($request->datosSolicitud['estado_nacimiento']);
            $solicitud->lugar_nacimiento_ciudad = mb_strtoupper($request->datosSolicitud['ciudad_nacimiento']);
            $solicitud->estado_civil            = $request->datosSolicitud['estado_civil'];
            $solicitud->nivel_estudios          = $request->datosSolicitud['nivel_estudios'];
            $solicitud->rfc                     = trim(mb_strtoupper($rfc));
            //$solicitud->curp                    = mb_strtoupper($request->input('curp'));
            $solicitud->telefono_casa           = $request->datosSolicitud['telefono_casa'];

            $mnsj_str = [
                'success'   => true,
                'message'   => 'Los datos personales se han guardado con éxito.',
            ];

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Datos Personales',
                1,
                'Los datos personales se han guardado con éxito.');

            // Guardando el mensaje en el campo ult_mensj_a_usuario
            $this->solicitudRepository->setUltMnsjUsr(
                $solicitud,
                json_encode($mnsj_str)
            );

            $solicitud->sub_status = 'Datos Personales';
            $solicitud->save();

            return response()->json($mnsj_str);
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La solicitud no existe'
            ]);
        }
    }

    public function datosAdicionales(request $request) {

        $prospecto_id = $request->prospecto_id;
        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {

            $validaciones = $this->validaciones($request, 'datos_adicionales');
            if ($validaciones != '') {
                return response()->json($validaciones);
            }

            $solicitud->tipo_residencia       = mb_strtoupper($request->datosSolicitud['residencia']);
            $solicitud->ingreso_mensual       = $request->datosSolicitud['ingreso_mensual'];
            $solicitud->antiguedad_domicilio  = $request->datosSolicitud['antiguedad_domicilio'];
            $solicitud->gastos_familiares     = $request->datosSolicitud["gastos_mensuales"];
            $solicitud->numero_dependientes   = $request->datosSolicitud["dependientes_economicos"];
            $solicitud->ocupacion             = mb_strtoupper($request->datosSolicitud["ocupacion"]);
            $solicitud->fuente_ingresos       = mb_strtoupper($request->datosSolicitud["fuente_ingresos"]);
            $solicitud->save();

            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                "Datos Ingresos-Egresos",
                1,
                "Los datos de ingresos/egresos se han guardado con éxito."
            );

            $solicitud->sub_status = 'Datos Ingresos-Egresos';
            $solicitud->save();

            $eventTM = ['event'=> 'Finalizan4', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $siguiente_paso = '';
            if ($request->input("ocupacion") != 'Jubilado') {
                $siguiente_paso = 'datos_empleo';
            } else {
                $siguiente_paso = 'alp';
            }


            return response()->json([
                'stat'              => 'Datos Adicionales OK',
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'siguiente_paso'    => $siguiente_paso,
                'eventTM'           => $eventTM
            ]);

        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La solicitud no existe',
            ]);
        }

    }

    public function datosEmpleo(request $request) {

        $prospecto_id = $request->prospecto_id;
        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {

            $validaciones = $this->validaciones($request, 'datos_empleo');
            if ($validaciones != '') {
                return response()->json($validaciones);
            }

            $solicitud->antiguedad_empleo = $request->datosSolicitud['antiguedad_empleo'];
            $solicitud->telefono_empleo   = $request->datosSolicitud["telefono_empleo"];
            $solicitud->save();

            $datos_adicionales = DatosAdicionales::updateOrCreate([
                'prospecto_id'  => $prospecto->id,
                'solicitud_id'  => $solicitud->id
            ], [
                'nombre_empresa' => mb_strtoupper($request->datosSolicitud['nombre_empresa'])
            ]);

            $datos_empleo = DatosEmpleo::updateOrCreate([
                'prospecto_id'          => $prospecto->id,
                'solicitud_id'          => $solicitud->id
            ], [
                'nombre_empresa'        => mb_strtoupper($request->datosSolicitud['nombre_empresa']),
                'antiguedad_empleo'     => $request->datosSolicitud['antiguedad_empleo'],
                'telefono_empleo'       => $request->datosSolicitud['telefono_empleo'],
                'fecha_ingreso'         => $fecha_nacimiento = Carbon::createFromFormat('Y-m-d', $request->datosSolicitud['fecha_ingreso']),
                'calle'                 => mb_strtoupper($request->datosSolicitud['calle_empleo']),
                'num_exterior'          => mb_strtoupper($request->datosSolicitud['no_ext_empleo']),
                'num_interior'          => mb_strtoupper($request->datosSolicitud['no_int_empleo']),
                'id_colonia'            => $request->datosSolicitud['id_colonia_empleo'],
                'id_delegacion'         => $request->datosSolicitud['id_delegacion_municipio_empleo'],
                'id_ciudad'            => $request->datosSolicitud['id_ciudad_empleo'],
                'id_estado'            => $request->datosSolicitud['id_estado_empleo'],
                'codigo_postal'         => $request->datosSolicitud['codigo_postal_empleo'],
                'colonia'               => mb_strtoupper($request->datosSolicitud['colonia_empleo']),
                'delegacion_municipio'  => mb_strtoupper($request->datosSolicitud['delegacion_municipio_empleo']),
                'ciudad'                => mb_strtoupper($request->datosSolicitud['ciudad_empleo']),
                'estado'                => mb_strtoupper($request->datosSolicitud['estado_empleo']),
            ]);

            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                "Datos Empleo",
                1,
                "Los datos del empleo se han guardado con éxito."
            );
            $solicitud->sub_status = 'Datos Empleo';
            $solicitud->save();

            if ($solicitud->status == 'Hit BC') {
                $siguiente_paso = 'segunda_llamada';
            } else {
                $siguiente_paso = 'alp';
            }

            $eventTM = ['event'=> 'Finalizan5', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            return response()->json([
                'success'           => true,
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'eventTM'           => $eventTM,
                'siguiente_paso'    => $siguiente_paso,
            ]);

        } else {

            return response()->json([
                'success'   => false,
                'message'   => 'La solicitud no existe',
            ]);

        }

    }

    public function registroCognito(Request $request, $password) {

        $cognito = new CognitoRepository;

        if ($request->datosSolicitud['apellido_materno'] != '')  {
            $apellidos = mb_strtoupper("{$request->datosSolicitud['apellido_paterno']} {$request->datosSolicitud['apellido_materno']}");
        } else {
            $apellidos = mb_strtoupper($request->datosSolicitud['apellido_paterno']);
        }
        $fecha = Carbon::now('UTC');

        $datos_usuario = [
            'name'                  => mb_strtoupper($request->datosSolicitud['nombres']),
            'middle_name'           => $apellidos,
            'phone_number'          => '+52'.$request->datosSolicitud['celular'],
        ];

        $cognito = $cognito->createUserCognito(mb_strtolower($request->datosSolicitud['email']), $password, $datos_usuario);

        return $cognito;

    }

    public function envioSMS(Request $request, CalixtaRepository $calixta) {

        $prospecto_id = $request->prospecto_id;
        $celular = $request->datosSolicitud['celular'];
        $codigo = mt_rand(10000, 99999);
        $msj = "Código de Verificación: ". $codigo;
        $responsecalixta = $calixta->sendSMS($celular, $msj);
        $responsecalixta = json_decode($responsecalixta);

        if ($responsecalixta->success == true) {

            $prospecto = Prospecto::findOrFail($prospecto_id);

            $codigo_sms = CodigosSMS::create([
                'prospecto_id'  => $prospecto_id,
                'codigo'        => $codigo,
                'celular'       => $celular,
                'nombre'        => $prospecto->nombres.' '.$prospecto->apellido_paterno,
                'email'         => $request->email,
                'enviado'       => $responsecalixta->success,
                'descripcion'   => $responsecalixta->message,
                'verificado'    => false,
            ]);

            $prospecto->sms_verificacion = true;
            $prospecto->save();

            return response()->json([
                'success'           => true,
                'message'           => 'Código de verificacón enviado con éxito.',
                'celular'           => $celular
            ]);

        } else {

            if ($responsecalixta->response_code == '6' || $responsecalixta->response_code == '10') {
                return response()->json([
                    'success'               => true,
                    'message'               => $responsecalixta->message,
                    'celular'               => $celular,
                    //'actualizar_celular'    => true,
                ]);
            } else {
                return response()->json([
                    'success'               => false,
                    'message'               => $responsecalixta->message,
                ]);
            }

        }

    }

    public function actualizarContraseña(request $request, CalixtaRepository $calixta) {

        if (Auth::guard('prospecto')->check()) {

            $messages = [
                'required'                      => 'El campo es obligatorio.',
                'digits'                        => 'El campo debe tener :digits números.',
                'password.regex'                => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
                'confirma_password.same'        => 'El campo contraseña y confirma contraseña deben ser iguales.',

            ];

            $validator = Validator::make($request->all(), [
                'codigo_verificacion'   => 'required|digits:6',
                'new_password'          => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                'confirm_password'      => 'required|same:password', 
            ], $messages);

            $errores = $validator->errors()->messages();

            if ($errores == null) {

                $prospecto_id = Auth::guard('prospecto')->user()->id;
                $conf_code = $request->input('conf_code');
                $is_login = $request->input('is_login');

                $prospecto = Prospecto::find($prospecto_id);
                $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                    ->orderBy('created_at', 'DESC')
                    ->first();

                $prospecto->password_temporal = 0;
                $prospecto->save();

                $mnsj_str = [
                    'success'           => true,
                    'message'           => 'Contraseña actualizada con éxito.',
                    'oculta'            => 'restaurar_contraseña'
                ];

                $siguiente_paso = $this->solicitudRepository->siguientePaso(null, $prospecto);
                $mnsj_str['siguiente_paso'] = $siguiente_paso;

                return response()->json($mnsj_str);

            } else {

                return json_encode([
                    'success' => false,
                    'errores' => $errores
                ]);
            }

        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    public function correccionBC(request $request)
    {
      
        $prospecto_id = $request->prospecto_id;

        $fecha_nac = Carbon::parse($request->datos['fecha_nacimiento'])->format('d-m-Y');

        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {

            $fecha_nacimiento = Carbon::createFromFormat('d-m-Y', $fecha_nac);
            $rfc = $request->datos['rfc'];
            
            $message = '';

            if ($rfc == '') {

                try {
                    $builder = new RfcBuilder();
                    $dia = $fecha_nacimiento->format('d');
                    $mes = $fecha_nacimiento->format('m');
                    $año = $fecha_nacimiento->format('Y');
                    $nombre = ($solicitud->prospecto->nombres);
                    $apellido_paterno = $solicitud->prospecto->apellido_paterno;
                    $apellido_materno = $solicitud->prospecto->apellido_materno;
                    if ($apellido_materno == 'XXX') {
                        $apellido_materno = '';
                    }

                    $rfc = $builder->name($nombre)
                        ->firstLastName($apellido_paterno)
                        ->secondLastName($apellido_materno)
                        ->birthday($dia, $mes, $año)
                        ->build()
                        ->toString();

                    $rfc = substr($rfc, 0, 10);
                    $message = ' RFC calculado: '.$rfc;

                } catch (\Exception $e) {

                    $mnsj_str = [
                        'success'   => false,
                        'message'   => 'Error al calcular RFC: '. $e->getMessage(),
                    ];

                    // Guardando el mensaje en el campo ult_mensj_a_usuario
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    $rfc = '';
                }
            } else {
                $rfc = $request->datos['rfc'];
                if (isset($request->datos['homoclave'])) {
                    $rfc .= $request->datos['homoclave'];
                }
            }

            $solicitud->fecha_nacimiento = $fecha_nacimiento;
            $solicitud->rfc = $rfc;
            $solicitud->save();

            $mnsj_str = [
                'success'           => true,
                'message'           => 'Se actualiza el RFC/Fecha de nacimiento.'.$message,
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'siguiente_paso'    => 'primera_llamada_bc',
            ];

            // Guardando el mensaje en el campo ult_mensj_a_usuario field
            $this->solicitudRepository->setUltMnsjUsr(
                $solicitud,
                json_encode($mnsj_str)
            );

            return response()->json($mnsj_str);

        } else {

            $mnsj_str = [
                'success'           => false,
                'message'           => 'La solicitud no existe',
            ];
            return response()->json($mnsj_str);

        }
        

    }

    public function cuentasDeCredito(request $request)
    {

        $prospecto_id = $request->prospecto_id;
        $prospecto = Prospecto::find($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($solicitud) {
            // Actualizando los campos de cuentas de crédito para esta solicitud
            $solicitud->credito_hipotecario = ($request->credito_hipotecario == 'on') ? 1 : 0;
            $solicitud->credito_automotriz  = ($request->credito_automotriz == 'on') ? 1 : 0;
            $solicitud->credito_bancario    = ($request->credito_tdc == 'on') ? 1 : 0;
            $solicitud->ultimos_4_digitos   = ($request->num_tdc);
            $solicitud->autorizado          = $request->autorizado;

            if (count($solicitud->producto) > 0) {
                $lead = $solicitud->producto[0]['pivot']['lead'];
                $lead_id = $solicitud->producto[0]['pivot']['lead_id'];
            } else {
                $lead = null;
                $lead_id = null;
            }

            if ($lead == null && $lead_id == null) {
                $lead = 'none';
                $lead_id = 'none';
            }

            $mnsj_str = [
                'success'           => true,
                'message'           => 'Los datos de cuentas de crédito se han guardado con éxito',
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'lead'              => $lead,
                'lead_id'           => $lead_id,
                'siguiente_paso'    => 'primera_llamada_bc',
            ];

            // Actualizando el ultimo punto de registro de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Cuentas de Crédito',
                1,
                'Los datos de cuentas de crédito se han guardado con éxito'
            );

            // Guardando el mensaje en el campo ult_mensj_a_usuario field
            $this->solicitudRepository->setUltMnsjUsr(
                $solicitud,
                json_encode($mnsj_str)
            );

            $solicitud->sub_status = 'Cuentas de Crédito';
            $solicitud->save();

            $eventTM = ['event' => 'AutorizaCB', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->alias)];
            $mnsj_str['eventTM'] = $eventTM;
            return response()->json($mnsj_str);

        } else {

            $mnsj_str = [
                'success'           => false,
                'message'           => 'La solicitud no existe',
            ];
            return response()->json($mnsj_str);

        }

    }

    public function buscarDatosDomicilio($datos_usuario, $id_producto) {

        $cp = $datos_usuario->datosSolicitud['codigo_postal'];
        $colonia = $datos_usuario->datosSolicitud['colonia'];
        $msj_colonia = '';
        $colonia_correcta = 0;

        $campo_cobertura = false;

        $producto = Producto::with('cobertura')->find($id_producto);
        $existe = $producto->cobertura->where('codigo', $cp);

        $campo_cobertura = $existe->first() ? true : false;
        
        if (strlen($cp) == 4) {
            $cp = '0'.$cp;
        }

        $direccion = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "%'.$colonia.'%"')
            ->get()
            ->toArray();

        if ( count($direccion) == 0 ) {

            $direccion = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'"')
                ->get()
                ->toArray();

            if ( count($direccion) >= 1 ) {

                $id_edo = $direccion[0]['id_estado'];
                $codigo_edo = $direccion[0]['codigo_estado'];

                if ($id_edo == '09') {
                    $id_municipio = '0'.$direccion[0]['id_ciudad'];
                    $id_ciudad = '01';
                } else {
                    $id_municipio = $direccion[0]['id_municipio'];
                    $id_ciudad = $direccion[0]['id_ciudad'];
                }

                $id_colonia = intval($direccion[0]['id_colonia']);

                $ciudad = '';
                if ($id_ciudad != '') {
                    $id_ciudad = $id_ciudad;
                    $ciudad = $direccion[0]['ciudad'];
                }


                return [
                    'codigopostal'  => $cp,
                    'id_estado'     => $id_edo,
                    'codigo_estado' => $codigo_edo,
                    'id_delegacion' => $id_municipio,
                    'id_ciudad'     => $id_ciudad,
                    'ciudad'        => $ciudad,
                    'id_colonia'    => '',
                    'cobertura'     => $campo_cobertura,
                ];

            } else {
                return [];
            }

        } elseif (count($direccion) > 1) {

            $direccion_exacta = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "'.$colonia.'"')
                ->get()
                ->toArray();

            if ( count($direccion_exacta) == 1 ) {

                $direccion = $direccion_exacta;
                $colonia_correcta = 1;

            }

        } elseif ( count($direccion) == 1 ) {

            $colonia_correcta = 1;

        } else {

            $colonia_correcta = 0;

        }

        $id_edo = $direccion[0]['id_estado'];
        $codigo_edo = $direccion[0]['codigo_estado'];

        if ($id_edo == '09') {
            $id_municipio = '0'.$direccion[0]['id_ciudad'];
            $id_ciudad = '01';
        } else {
            $id_municipio = $direccion[0]['id_municipio'];
            $id_ciudad = $direccion[0]['id_ciudad'];
        }

        $id_colonia = '';
        if ($colonia_correcta == 1) {
            $id_colonia = intval($direccion[0]['id_colonia']);
            $id_colonia = $id_edo.$id_municipio.$cp.$id_colonia;
        }

        $ciudad = '';
        if ($id_ciudad != '') {
            $id_ciudad = $id_ciudad;
            $ciudad = $direccion[0]['ciudad'];
        }

        return [
            'codigopostal'  => $cp,
            'id_estado'     => $id_edo,
            'codigo_estado' => $codigo_edo,
            'id_delegacion' => $id_municipio,
            'id_ciudad'     => $id_ciudad,
            'ciudad'        => $ciudad,
            'id_colonia'    => $id_colonia,
            'cobertura'     => $campo_cobertura,
        ];
    }

    public function makePassword($datos_usuario, $sitio) {

        $caracter = substr(str_shuffle(".+*#$-"), 0, 1);

        $password = mb_strtoupper(substr($datos_usuario['nombres'], 0, 1))
                    .mb_strtolower(substr($datos_usuario['apellido_paterno'], 0, 1))
                    .mb_strtolower(substr($datos_usuario['apellido_materno'], 0, 1))
                    .substr($datos_usuario['celular'], -4)
                    .$caracter.$sitio;

        return $password;

    }

    function validacionesPortal($datosSolicitud, $formulario) {

        $reglas = null;
        $mensajes = [
            'required'                      => 'El campo es obligatorio.',
            'calle.required'                => 'El campo Calle es obligatorio.',
            'no_ext.required'               => 'El campo Número exterior es obligatorio.',
            'sms.required'               => 'El campo SMS es obligatorio.',
            'calle_empleo.required'         => 'El campo Calle es obligatorio.',
            'no_ext_empleo.required'        => 'El campo Número exterior es obligatorio.',
            'password.regex'                => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
            'confirma_password.same'        => 'El campo contraseña y confirma contraseña deben ser iguales.',
            'numeric'                       => 'El campo debe contener solo números.',
            'digits_between'                => 'El campo debe ser mayor a cero',
            'email'                         => 'El email no es válido.',
            'digits'                        => 'El campo debe tener :digits números.',
            'ingreso_mensual.min'           => 'El ingreso mensual debe tener al menos 4 dígitos',
            'alpha_numeric_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z][0-9] o espacios',
            'validacion_combos'             => 'El campo solo puede contener caracteres válidos: [A-Z][0-9], -, parentesis o espacios',
            'alpha_spaces_not_html'         => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'monto_prestamo.max'            => 'El monto no puede ser mayor a $'.number_format($datosSolicitud['monto_prestamo_maximo'], 0),
            'monto_prestamo.min'            => 'El monto no puede ser menor a $'.number_format($datosSolicitud['monto_prestamo_minimo'], 0),
            'rfc'                           => 'El RFC esta formado por 4 letras y 6 números (2 del año, 2 del mes y 2 del día de la fecha de nacimiento)'
        ];

        switch ($formulario) {
            case 'simulador':
                $reglas = [
                    'monto_prestamo'    => 'required|numeric|min:'.$datosSolicitud['monto_minimo'].'|max:'.$datosSolicitud['monto_maximo'],
                    'plazo'             => 'required',
                    'finalidad'         => 'required',
                    'finalidad_custom'  => 'required|alpha_numeric_spaces_not_html'
                ];
                $mensajesSimulador = [
                    'monto_prestamo.max' => 'El monto no puede ser mayor a $'.number_format($datosSolicitud['monto_maximo'], 0),
                    'monto_prestamo.min' => 'El monto no puede ser menor a $'.number_format($datosSolicitud['monto_minimo'], 0),
                ];
                $mensajes = array_merge($mensajes, $mensajesSimulador);
            break;
            case 'registro':
                $reglas = [
                    'monto_prestamo'            => 'required|numeric|digits_between:4,6|min:'.$datosSolicitud['monto_prestamo_minimo'].'|max:'.$datosSolicitud['monto_prestamo_maximo'],
                    'plazo'                     => 'required',
                    'finalidad_custom'                 => 'required',
                    //'finalidad_custom'          => 'required|alpha_numeric_spaces_not_html',
                    'nombres'                   => 'required|alpha_spaces_not_html',
                    'apellido_materno'          => 'required|alpha_spaces_not_html',
                    'apellido_paterno'          => 'required|alpha_spaces_not_html',
                    'celular'                   => 'required|numeric|digits:10',
                    'email'                     => 'required|email',
                    'password'                  => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                    'confirma_password'         => 'required|same:password',
                ];
            break;
            case 'editar_registro':
                $reglas = [
                    'monto_prestamo'    => 'required|numeric|digits_between:4,6|min:'.$datosSolicitud['monto_prestamo_minimo'].'|max:'.$datosSolicitud['monto_prestamo_maximo'],
                    'plazo'             => 'required',
                    'finalidad_custom'  => 'required|alpha_numeric_spaces_not_html'
                ];
            break;
            case 'conf_sms':
                $reglas = [
                'sms'       =>  'required|numeric',
                ];
            break;
            case 'domicilio':
                $reglas = [
                    'sms'       =>  'required|numeric',
                    'calle'         => 'required|alpha_numeric_spaces_not_html|max:100',
                    'no_ext'   => 'required|alpha_numeric_spaces_not_html|max:5',
                    'no_int'   => 'alpha_numeric_spaces_not_html|max:5',
                    'codigo_postal' => 'required|digits:5',
                    'colonia'       => 'required|validacion_combos',
                    'delegacion_municipio'    => 'required|validacion_combos',
                    'ciudad'        => 'validacion_combos',
                    'estado'        => 'required|validacion_combos',
                ];
            break;
            case 'datos_personales':
                $reglas = [
                    'sms'       =>  'required|numeric',
                    'sexo'              => 'required|alpha_numeric_spaces_not_html',
                    'fecha_nacimiento'  => 'required|date',
                    'ciudad_nacimiento' => 'required|alpha_numeric_spaces_not_html',
                    'estado_nacimiento' => 'required|alpha_numeric_spaces_not_html',
                    'telefono_casa'     => 'required|digits:10',
                    'estado_civil'      => 'required|alpha_numeric_spaces_not_html',
                    'nivel_estudios'    => 'required|alpha_numeric_spaces_not_html',
                    //'rfc'               => 'required|alpha_numeric_spaces_not_html',
                ];
            break;
            case 'datos_adicionales':
                $reglas = [
                    'sms'           =>  'required|numeric',
                    'residencia'    => 'required|validacion-combos',
                    'ocupacion'     => 'required|validacion-combos',
                    'fuente_ingresos'   => 'required|validacion-combos',
                    'ingreso_mensual'   => 'required|numeric|digits_between:4,6',
                    'antiguedad_domicilio' => 'required|validacion-combos',
                    'dependientes_economicos'   => 'required|validacion-combos',
                    'gastos_mensuales'  => 'required|numeric|digits_between:4,6',

                ];
            break;
            case 'datos_empleo':
                $reglas = [
                    'sms'       =>  'required|numeric',
                    'nombre_empresa'               => 'required|alpha_numeric_spaces_not_html',
                    'antiguedad_empleo'     => 'required|alpha_numeric_spaces_not_html',
                    'telefono_empleo'       => 'required|digits:10',
                    'fecha_ingreso'         => 'required|date',
                    'calle_empleo'          => 'required|alpha_numeric_spaces_not_html|max:100',
                    'no_ext_empleo'    => 'required|alpha_numeric_spaces_not_html|max:5',
                    'no_int_empleo'    => 'alpha_numeric_spaces_not_html|max:5',
                    'codigo_postal_empleo'  => 'required|digits:5',
                    'colonia_empleo'        => 'required|validacion-combos',
                    'delegacion_municipio_empleo'     => 'required|validacion_combos',
                    'ciudad_empleo'         => 'validacion_combos',
                    'estado_empleo'         => 'required|validacion_combos',
                ];
            break;
            case 'correccion_bc':
                $reglas = [
                    'fecha_nacimientoBC'    => 'required|date',
                ];
            break;
            case 'actualiza_password':
                $reglas = [
                    'codigo_verificacion'   => 'required|digits:6',
                    'new_password'          => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                    'confirm_password'      => 'required|same:password',
                ];
            break;
        }



        $validaciones = Validator::make($datosSolicitud, $reglas, $mensajes);
        return $validaciones;
    }

    public function getTrackingSolicitud(Request $request) {
        $solicitud = $request->solicitud_id;

        $tracking = TrackingSolicitud::where('solicitud_id',$solicitud)
                    ->get();
        $datosTracking = [];
        foreach ($tracking as $track) {
            $datosTracking[] = [
                'status' => $track->status,
                'sub_status' => $track->sub_status
            ];
            /* foreach($datosTracking as $key=> $val){
                if($datosTracking[$key])
            } */
        }
        //dd($datosTracking);
        if ($tracking){
            return response()
                ->json($datosTracking)
                ->header('Content-Type', 'json');
        }
    }

    public function getDetalleSolicitud($encrypt) {

        $solicitud_id= base64_decode($encrypt);

        $solicitud = Solicitud::with(
                'prospecto', 'domicilio', 'datos_adicionales', 'producto',
                'respuesta_maquina_riesgos', 'consultas_buro', 'datos_empleo',
                'alp', 'convenio', 'tracking', 'log')
            ->select('id', 'prospecto_id', 'status', 'sub_status', 'sexo', 'fecha_nacimiento',
                'lugar_nacimiento_ciudad', 'lugar_nacimiento_estado', 'rfc',
                'curp', 'telefono_casa', 'estado_civil', 'nivel_estudios', 'autorizado',
                'encontrado', 'user_ip', 'finalidad_custom', 'ocupacion', 'fuente_ingresos',
                'credito_hipotecario', 'credito_automotriz', 'credito_bancario', 'ultimos_4_digitos',
                'tipo_residencia', 'ingreso_mensual', 'antiguedad_empleo', 'telefono_empleo',
                'antiguedad_domicilio', 'gastos_familiares', 'numero_dependientes',
                'prestamo', 'plazo', 'finalidad', 'pago_estimado', 'finalidad_custom',
                'created_at', 'updated_at')
            ->find($solicitud_id);
            
        try {
            $solicitud->prospecto->encryptd = decrypt($solicitud->prospecto->encryptd);
        } catch (DecryptException $e) {
            $solicitud->prospecto->encryptd = '';
        }
        $solicitud->id = $solicitud->id;
        $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
        $solicitud->plazo = $solicitud->plazo;
        $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
        $solicitud->finalidad = self::getFinalidad($solicitud->finalidad);
        $solicitud->encontrado = ($solicitud->encontrado)? 'Si' : 'No';
        $solicitud->sexo = ($solicitud->sexo != '')? ($solicitud->sexo == 'M')? 'M' : 'F' : '';
        $solicitud->estado_civil = $solicitud->estado_civil;
        if ($solicitud->fecha_nacimiento != '') {
            $fecha_nacimiento_dp = Carbon::createFromFormat('Y-m-d', $solicitud->fecha_nacimiento);
            $solicitud->fecha_nacimiento_dp = $fecha_nacimiento_dp->format('Y').'-'.$fecha_nacimiento_dp->format('n').'-'.$fecha_nacimiento_dp->format('d');
        }

        if ($solicitud->lugar_nacimiento_estado != '') {
            $solicitud->ciudades = $this->apiCiudades($solicitud->lugar_nacimiento_estado);
        }

        $solicitud->id_estado_nacimiento = self::getEstadoNac($solicitud->lugar_nacimiento_estado);
        $solicitud->lugar_nacimiento_ciudad = ucwords(mb_strtolower($solicitud->lugar_nacimiento_ciudad));

        if ($solicitud->domicilio !== null) {
            $catalogoSepomex = $this->apiGeoCP($solicitud->domicilio->cp);
            $solicitud->colonias = $catalogoSepomex['colonias'];
            $solicitud->domicilio->id_colonia = self::getColonia($solicitud->domicilio);
        }
        $solicitud->estado_nacimiento = $solicitud->lugar_nacimiento_estado;
        $solicitud->lugar_nacimiento_estado = self::getEstadoNac($solicitud->lugar_nacimiento_estado);
        $solicitud->lugar_nacimiento_ciudad = ucwords(mb_strtolower($solicitud->lugar_nacimiento_ciudad));
        $solicitud->autorizado = self::getYesOrNo($solicitud->autorizado);
        $solicitud->credito_hipotecario = self::getYesOrNo($solicitud->credito_hipotecario);
        $solicitud->credito_automotriz = self::getYesOrNo($solicitud->credito_automotriz);
        $solicitud->credito_bancario = self::getYesOrNo($solicitud->credito_bancario);
        $solicitud->ingreso_mensual = '$'.number_format($solicitud->ingreso_mensual, 0, '.', ',');
        $solicitud->antiguedad_empleo  = (isset($solicitud->tipo_residencia) && $solicitud->tipo_residencia != '')? $solicitud->antiguedad_empleo : '';
        $solicitud->antiguedad_domicilio  = (isset($solicitud->tipo_residencia) && $solicitud->tipo_residencia != '')? $solicitud->antiguedad_domicilio : '';
        $solicitud->gastos_familiares = '$'.number_format($solicitud->gastos_familiares, 0, '.', ',');
        $solicitud->created_at_disp = self::displayMySqlDate($solicitud->created_at->toDateTimeString());

        // Obteniendo datos FICO
        if (isset($solicitud->alp->alp)) {

            $solicitud->fico_tabla = 1;
            $solicitud->fico = 0;

        }

        $etiquetas = Etiquetas::all()->pluck('etiqueta', 'descripcion');
        $solicitud->etiquetas = $etiquetas;

       
        // Obteniendo los cuestionarios dinámico
        if (isset($solicitud->respuesta_maquina_riesgos->respuesta_maquina_riesgos)) {

            if ($solicitud->respuesta_maquina_riesgos[0]->pantallas_extra == 1) {
                if ($solicitud->respuesta_maquina_riesgos[0]->cuestionario_dinamico_guardado == 1) {

                    $respuestas = RespuestaCuestionarioDinamico::where('solicitud_id', $solicitud->id)
                        ->get();
                    $respuestas = $respuestas->groupBy('situacion_id')->toArray();

                    $situaciones_id = array_keys($respuestas);
                    $situaciones_text = explode(',', $solicitud->respuesta_maquina_riesgos[0]->situaciones);

                    $situaciones = Situacion::select('id', 'encabezado')
                        ->whereIN('situacion', $situaciones_text)
                        ->get();
                    $situaciones = $situaciones->keyBy('id')->toArray();

                    $preguntas = CuestionarioDinamico::select('id', 'pregunta')
                        ->whereIN('situacion_id', $situaciones_id)
                        ->get();
                    $preguntas = $preguntas->keyBy('id')->toArray();

                    $cuestionariosDinamicos = [
                        'situaciones' => $situaciones,
                        'preguntas'   => $preguntas,
                        'respuestas'  => $respuestas
                    ];

                    $solicitud->cuestionarios_dinamicos = $cuestionariosDinamicos;
                }
            }
        }
        //dd($solicitud->respuesta_maquina_riesgos);
        if (isset($solicitud->respuesta_maquina_riesgos[0])) {
            //dd($solicitud->respuesta_maquina_riesgos[0]->decision);
            if ($solicitud->respuesta_maquina_riesgos[0]->decision == 'Invitación a Continuar') {
                $plantillaComunicacion = null;
                $modal = 'modalOferta';
                $datosOferta = [];
                foreach ($solicitud->respuesta_maquina_riesgos as $oferta) {
                    
                    $datosOferta[] = [
                        'monto'         => $oferta['monto'],
                        'plazo'         => $oferta['plazo'],
                        'tasa'          => $oferta['tasa'],
                        'pago_estimado' => $oferta['pago'],
                        'tipo_oferta'   => $oferta['tipo_oferta'],
                        'modelo'        => 'Prestanómico',
                        'oferta_id'     => $oferta['id'],
                    ];
                    $tipoPoblacion = $oferta['tipo_poblacion'];
                    $plantillaComunicacion = $oferta['plantilla_comunicacion'];
                }
                if ($tipoPoblacion == 'oferta_doble') {
                    $modal = 'modalDobleOferta';
                }
                $motivos = MotivoRechazo::pluck('motivo');
                $modal = view("modals.{$modal}", ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render();

                $datos_oferta = [
                    'motivos' => $motivos,
                    'modal' => $modal
                ];

                $solicitud->datos_oferta = $datos_oferta;
            }
        }
        if ($solicitud->log->exists()) {
            $solicitud->ultimo_mensaje_usuario = json_decode($solicitud->log['ultimo_mensaje_usuario'], true);
        }

        return response()
                ->json($solicitud)
                ->header('Content-Type', 'json');
    }
    //TODO: cambiar metodo para busqueda de solicitudes, ocupar este metodo

    public function getProspectos(Request $request)
    {
        $term = $request->term;
        $term= base64_decode($term);
        
        $email = self::is_valid_email($term);
        if ($email) {
            $user_exist= Prospecto::where('email',$term)
                ->first();
            if ($user_exist === null) {
                return 0;
            }
            $term = $user_exist->id;
        }

        $prospecto = Prospecto::with('solicitudes.producto')
            ->find($term);
        if ($prospecto) {
            foreach ($prospecto->solicitudesV2 as $solicitud) {
                $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
                $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
                $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
                $solicitud->encontrado = ($solicitud->encontrado) ? 'Si' : 'No';
                $solicitud->created_at_disp = self::displayMySqlDate($solicitud->created_at->toDateTimeString());
                $solicitud->updated_at_disp = self::displayMySqlDate($solicitud->updated_at->toDateTimeString());

                // Obteniendo el score
                if (isset($solicitud->bc_score->bc_score)) {
                    $solicitud->score_bc = $solicitud->bc_score->bc_score;
                    $solicitud->score_micro = $solicitud->bc_score->micro_valor;
                } else {
                    $solicitud->score_bc = 0;
                    $solicitud->score_micro = 0;
                }

                // Obteniendo el producto
                if (count($solicitud->producto) > 0) {
                    $solicitud->nombre_producto = $solicitud->producto[0]->nombre_producto;
                } else {
                    $solicitud->nombre_producto = 'MERCADO ABIERTO';
                }
            }

            return response()
                ->json($prospecto)
                ->header('Content-Type', 'json');
        } else {
            return response()
                ->json(['success' => false]);
        }     

        
    }

    public function is_valid_email($str) {
        return (false !== strpos($str, "@") && false !== strpos($str, "."));
    }

    public function apiGeoCP($cp)
    {

        $cp_data = CatalogoSepomex::where('codigo', $cp)->get()->toArray();

        $colonias = [];
        $ciudad = "";
        $id_ciudad = "";
        foreach ($cp_data as $data) {

            array_push(
                $colonias,
                [
                    'colonia' => addslashes($data['colonia_asentamiento']),
                    'id' => intval($data['id_colonia'])
                ]
            );
            if ($data['ciudad'] != "") {
                $ciudad = $data['ciudad'];
                $id_ciudad = $data['id_ciudad'];
            }
        }
        // Si el id del estado es 09 (Ciudad de México), el Id de municipio
        // es igual al id de la ciudad y el id de la ciudad es 01
        $id_estado = $cp_data[0]['id_estado'];
        if ($id_estado == '09') {
            $id_municipio = '0'.$id_ciudad;
            $id_ciudad = '01';
        } else {
            $id_municipio = $cp_data[0]['id_municipio'];
        }

        $id_colonia = $id_estado.$id_municipio.$cp;

        return [
            'success'        => true,
            'cp'             => $cp_data[0]['codigo'],
            'colonias'       => $colonias,
            'deleg_munic'    => addslashes($cp_data[0]['municipio']),
            'id_deleg_munic' => $id_municipio,
            'ciudad'         => addslashes($ciudad),
            'id_ciudad'      => $id_ciudad,
            'estado'         => addslashes($cp_data[0]['estado']),
            'codigo_estado'  => addslashes($cp_data[0]['codigo_estado']),
            'id_estado'      => $id_estado,
            'id_colonia'     => $id_colonia,
            'cobertura'      => 1,
        ];

    }

    public function apiCiudades($estado)
    {
        // Obteniendo los municipios del estado
        $municipios = CatalogoSepomex::selectRAW('distinct(municipio)')
            ->where('estado', ucwords($estado))
            ->orderBy('municipio')
            ->pluck('municipio');

        $no_municipios = count($municipios);

        return [
            'success'   => true,
            'count'      => $no_municipios,
            'municipios' => $municipios
        ];
    }

}
