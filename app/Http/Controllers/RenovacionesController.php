<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Prospecto;
use App\Solicitud;
use App\Analytic;
use App\Finalidad;
use App\Plazo;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use App\Producto;
use App\OfertaRenovacion;
use App\OfertaRenovacionSitio;
use Log;
use App;
use Auth;
use Jenssegers\Agent\Agent;
use App\Repositories\SolicitudRepository;
use Session;
use Validator;
use Illuminate\Validation\Rule;
use App\Repositories\CognitoRepository;
use Hash;
use App\W_USER;
use Uuid;
use Excel;

class RenovacionesController extends Controller
{
    private $solicitudRepository;

    public function __construct()
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function buscarRFC (Request $request) {

        $validaciones = $this->solicitudRepository->validacionesSolicitud($request->toArray(), 'buscarRFC-renovaciones');
        if ($validaciones->fails()) {
            return [
                'success'   => false,
                'errores'   => $validaciones->errors()
            ];
        }

        $oferta = $this->solicitudRepository->buscarOfertaSitio($request->uuid);

        if ($oferta['success'] == true) {

            $fecha_nacimiento = substr($oferta['ofertas']['rfc'], 4, 6);
            $fecha_nacimiento = Carbon::createFromFormat('ymd', $fecha_nacimiento)->format('d-m-Y');

            $response = [
                'success'           => true,
                'rfc'               => $oferta['ofertas']['rfc'],
                'fecha_nacimiento'  => $fecha_nacimiento
            ];
            return response()->json($response);

        } else {

            $modal = view("modals.modalOfertaRenovacionNoEncontrada")->render();
            $response = [
                'success'           => false,
                'login'             => false,
                'siguiente_paso'    => 'modal',
                'modal'             => $modal,
                'stat'              => 'Oferta no encontrada'
            ];
            return response()->json($response);
        }

    }

    public function obtenerOferta(Request $request) {

        $oferta_sitio = false;
        $validaciones = $this->solicitudRepository->validacionesSolicitud($request->toArray(), 'registro-renovaciones');
        if ($validaciones->fails()) {
            return [
                'success'   => false,
                'errores'   => $validaciones->errors()
            ];
        }

        if ($request->has('uuid')) {
            $oferta = $this->solicitudRepository->buscarOfertaSitio($request->uuid, $request->rfc_renovaciones);
            $oferta_sitio = true;
        } else {
            $oferta = $this->solicitudRepository->buscarOferta($request);
            $oferta_sitio = false;
        }

        if ($oferta['success'] == true) {
            $response = $this->generarProspectoSolicitud('renovaciones', $oferta, $request, $oferta_sitio);
            return response()->json($response, 201);
        } else {
            // $response = $this->generarProspectoSolicitud('mercado_abierto', $oferta, $request);
            $modal = view("modals.modalOfertaRenovacionNoEncontrada")->render();
            $response = [
                'success'           => false,
                'login'             => false,
                'siguiente_paso'    => 'modal',
                'modal'             => $modal,
                'stat'              => 'Oferta no encontrada'
            ];
            return response()->json($response);
        }
    }

    public function generarProspectoSolicitud($producto, $oferta, $request, $oferta_sitio) {

        if ($oferta_sitio == false) {
            $fecha_creacion = Carbon::createFromFormat('d/m/Y', $oferta['ofertas']['datosCampania']['fechaCreacion'])->format('Y-m-d');
            $vigencia = Carbon::createFromFormat('d/m/Y', $oferta['ofertas']['datosCampania']['vigencia'])->format('Y-m-d');
            $datosUnicos = [
                'Id_Oferta'         => $oferta['ofertas']['id_oferta'],
                'Fecha_Creacion'    => $fecha_creacion,
                'Id_Transaccion'    => $oferta['ofertas']['datosCampania']['idTransaccion']
            ];
        } else {
            $fecha_creacion = Carbon::createFromFormat('Y-m-d', $oferta['ofertas']['datosCampania']['fechaCreacion'])->format('Y-m-d');
            $vigencia = null;
            $datosUnicos = [
                'Id_Oferta'         => $oferta['ofertas']['id_oferta'],
                'Fecha_Creacion'    => $fecha_creacion,
            ];
        }

        $oferta_renovacion = OfertaRenovacion::updateOrCreate($datosUnicos);

        if ($oferta_renovacion->wasRecentlyCreated) {

            $oferta_renovacion->Monto_Oferta1   = $oferta['ofertas']['oferta1']['monto'];
            $oferta_renovacion->Tasa_Oferta1    = $oferta['ofertas']['oferta1']['tasa'];
            $oferta_renovacion->Plazo_Oferta1   = $oferta['ofertas']['oferta1']['plazo'];
            $oferta_renovacion->Pago_Oferta1    = $oferta['ofertas']['oferta1']['pago'];
            $oferta_renovacion->Monto_Oferta2   = $oferta['ofertas']['oferta2']['monto'];
            $oferta_renovacion->Tasa_Oferta2    = $oferta['ofertas']['oferta2']['tasa'];
            $oferta_renovacion->Plazo_Oferta2   = $oferta['ofertas']['oferta2']['plazo'];
            $oferta_renovacion->Pago_Oferta2    = $oferta['ofertas']['oferta2']['pago'];
            $oferta_renovacion->Vigencia        = $vigencia;
            $oferta_renovacion->Id_Transaccion  = $oferta['ofertas']['datosCampania']['idTransaccion'];
            $oferta_renovacion->save();

            $prospecto = Prospecto::firstOrCreate([
                'email' => mb_strtolower($request->email)
            ]);

            // Si el prospecto fue creado recientemente, se actualizan atributos
            // faltantes y se crea solicitud

            if ($prospecto->wasRecentlyCreated) {

                $cognito_id = null;
                $responseCognito = $this->registroCognito($request);
                if ($responseCognito['success'] == false) {
                    return response()->json($responseCognito);
                } else {
                    $cognito_id = $responseCognito['cognito_id'];
                }

                $prospecto->nombres               = mb_strtoupper($request->nombres);
                $prospecto->apellido_paterno      = mb_strtoupper($request->apellido_paterno);
                $prospecto->apellido_materno      = mb_strtoupper($request->apellido_materno);
                $prospecto->celular               = $request->celular;
                $prospecto->password              = Hash::make($request->contraseña);
                $prospecto->encryptd              = encrypt($request->contraseña);
                $prospecto->usuario_ldap          = 1;
                $prospecto->sms_verificacion      = 0;
                $prospecto->usuario_confirmado    = 0;
                $prospecto->referencia            = 'SITIO';
                $prospecto->cognito_id            = $cognito_id;
                $prospecto->acepto_avisop         = $request->acepto_avisotyc;
                $prospecto->save();

                $w_user = W_USER::updateOrCreate([
                    'ID_USER'       => mb_strtolower($request->email)
                ], [
                    'PHONE_NUMBER'  => $request->celular,
                    'CREATE_DATE'   => Carbon::now(),
                    'UPDATE_DATE'   => Carbon::now()
                ]);

            }

            $producto = Producto::where('alias', 'renovaciones')->first();

            // Obteniendo la descripcion de la finalidad
            $finalidades = $producto->finalidades;
            $finalidad = $finalidades->where('finalidad', 'Renovación de crédito clientes FMP')->values();

            if (count($finalidad) == 1) {
                $finalidad = mb_strtoupper($finalidad[0]->finalidad);
            } else {
                $finalidad = 12;
            }

            // Obteniendo la clave del plazo
            $plazos = $producto->plazos;
            $plazo = $oferta['ofertas']['oferta1']['plazo'];
            $duracion = null;
            $clave_plazo = null;
            $plazo = $plazos->where('duracion', $plazo)->values();

            if (count($plazo) == 1) {
                $clave_plazo = $plazo[0]->clave;
                $duracion = $plazo[0]->duracion;
                $plazo = mb_strtoupper($plazo[0]->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $fecha_nacimiento = Carbon::createFromFormat('d-m-Y', $request->input('fecha_nacimiento_renovaciones'));

            $solicitud = Solicitud::create([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $oferta['ofertas']['oferta1']['monto'],
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $oferta['ofertas']['oferta1']['pago'],
                'finalidad'         => $finalidad,
                'finalidad_custom'  => 'Solicitud de renovación de crédito para clientes de Financiera Monte de Piedad',
                'rfc'               => mb_strtoupper($oferta['ofertas']['rfc']),
                'fecha_nacimiento'  => $fecha_nacimiento,
            ]);

            if ($oferta_sitio == true) {
                $oferta_renovacion_sitio = OfertaRenovacionSitio::find($oferta['ofertas']['id']);
                $oferta_renovacion_sitio->solicitud_id = $solicitud->id;
                $oferta_renovacion_sitio->save();
            }

            $oferta_renovacion->id_prospect = $prospecto->id;
            $oferta_renovacion->id_solic = $solicitud->id;
            $oferta_renovacion->save();

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($request->email, ['custom:idProspecto' => 'FMP-'.$prospecto->id]);

            // Agregando el producto a la solicitud
            $producto_id = $producto->id;
            $version_producto = Producto::find($producto->id)->getCurrentVersionNo();
            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                ]
            ]);

            Auth::guard('prospecto')->loginUsingId($prospecto->id);

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if ($agent->isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = $agent->device();
            }
            if ($agent->isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = $agent->device();
            }
            if ($agent->isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = $agent->device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $request->input('clientId'),
                'email'             => $request->input('email'),
                'ip'                => self::getUserIP(),
                'telefono'          => $request->input('celular'),
                'sistema_operativo' => $agent->platform(),
                'navegador'         => $agent->browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => [
                    'monto'         => intval($oferta['ofertas']['oferta1']['monto']),
                    'plazo'         => "{$duracion} {$plazo}",
                    'finalidad'     => 'Renovación de crédito clientes FMP'
                ]
            ];

            $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Usuario y Solicitud creados con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $eventTM[] = ['event'=> 'RegistroUsuario', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $mnsj_str['eventTM'] = $eventTM;

            return $mnsj_str;

        } else {

            $prospecto = Prospecto::where('email', mb_strtolower($request->email))
                ->first();

            if ($prospecto) {

                $solicitudRenovacion = $this->solicitudRenovaciones($prospecto);

                if ($solicitudRenovacion == true) {

                    return [
                        'success'           => false,
                        'nueva_solicitud'   => false,
                        'siguiente_paso'    => 'login',
                    ];

                } else {

                    $modal = view("modals.modalSolicitudRenovacionTerminada")->render();
                    return [
                        'success'           => false,
                        'nueva_solicitud'   => false,
                        'siguiente_paso'    => 'modal',
                        'modal'             => $modal,
                        'stat'              => 'Solicitud terminada'
                    ];

                }

            } else {

                $modal = view("modals.modalSolicitudRenovacionIncompletaEmailDiferente")->render();
                return [
                    'success'           => false,
                    'nueva_solicitud'   => false,
                    'siguiente_paso'    => 'modal',
                    'modal'             => $modal,
                    'stat'              => 'Solicitud encontrada'
                ];

            }

        }

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    public function registroCognito(Request $request) {

        $cognito = new CognitoRepository;

        if ($request->apellido_materno != '')  {
            $apellidos = mb_strtoupper("{$request->apellido_paterno} {$request->apellido_materno}");
        } else {
            $apellidos = mb_strtoupper($request->apellido_paterno);
        }
        $fecha = Carbon::now('UTC');

        $datos_usuario = [
            'name'                  => mb_strtoupper($request->nombres),
            'middle_name'           => $apellidos,
            'phone_number'          => '+52'.$request->celular,
        ];

        $cognito = $cognito->createUserCognito(mb_strtolower($request->email), $request->contraseña, $datos_usuario);

        return $cognito;

    }

    public function solicitudRenovaciones($prospecto) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
            ->orderBy('id', 'desc')
            ->first();

        $renovaciones = false;
        if ($solicitud->producto()->exists()) {
            $renovaciones = $solicitud->producto[0]['alias'] == 'renovaciones' ? true : false;
        }

        if ($renovaciones == true) {

            $siguientPaso = $this->solicitudRepository->siguientePaso(null, $prospecto);

            if ($siguientPaso['nueva_solicitud'] == true) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }

    }

    public function cargaLayout(Request $request) {

        $file = $request->file('file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $file = $file->move(storage_path().'/app/public', $filename);

            if ($file->getExtension() == 'xlsx' || $file->getExtension() == 'xls') {
                $location = $file->getPathName();

                Excel::load($location, function ($reader) {
                    $headers = $reader->first()->keys()->toArray();
                    $results = $reader->formatDates(false)->toArray();

                    $clientesAlta = [];

                    foreach ($results as $result) {

                        OfertaRenovacionSitio::updateOrCreate([
                            'id_oferta'         => $result['id'],
                            'rfc'               => $result['rfc'],
                            'fecha_creacion'    => $result['fecha_creacion'],
                        ], [
                            'monto_oferta1'     => $result['monto_oferta1'],
                            'tasa_oferta1'      => $result['tasa_oferta1'],
                            'plazo_oferta1'     => $result['plazo_oferta1'],
                            'pago_oferta1'      => $result['pago_oferta1'],
                            'monto_oferta2'     => $result['monto_oferta2'],
                            'tasa_oferta2'      => $result['tasa_oferta2'],
                            'plazo_oferta2'     => $result['plazo_oferta2'],
                            'pago_oferta2'      => $result['pago_oferta2'],
                            'vigencia'          => $result['vigencia'],
                            'id_transaccion'    => $result['id_transaccion'],
                        ]);

                    }

                });

                Storage::delete([storage_path().'/app/public', $filename]);
                return response()->json([
                    'success' => 'true'
                ]);

            } else {
                return response()->json([
                    'success' => 'false',
                    'msg' => 'Solo se permiten archivos con extensión .xls o .xlsx'
                ]);
            }
        } else {
            return response()->json([
                'success' => 'false',
                'msg' => 'No se selecciono ningún archivo'
            ]);
        }

    }

}
