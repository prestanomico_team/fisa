<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ReporteEficiencia;
use Excel;
use Illuminate\Support\Facades\Storage;

class ReporteDiarioController extends Controller
{

    public function __construct()
    {

    }


    public function datosReporteEficiencia(request $request)
    {
        if (App::environment(['produccion'])) {

            if ($request->has('fecha')) {
                $fecha = $request->fecha;
            } else {
                $fecha = Carbon::yesterday()->format('Y-m-d');
            }

            $respuestasMR = DB::connection('mysql')
                ->table('respuestas_maquina_riesgos')
                ->leftJoin('fmp_maquina_riesgos.Tb1_ResFinal_MR', 'respuestas_maquina_riesgos.solicitud_id', '=', 'Tb1_ResFinal_MR.id_solic')
                ->leftJoin('fmp_maquina_riesgos.detalle_solicitudes_mr', 'respuestas_maquina_riesgos.solicitud_id', '=', 'detalle_solicitudes_mr.ID_SOLIC')
                ->leftJoin('fmp.producto_solicitud', 'respuestas_maquina_riesgos.solicitud_id', '=', 'producto_solicitud.solicitud_id')
                ->leftJoin('fmp.productos', 'productos.id', '=', 'producto_solicitud.producto_id')
                ->where('respuestas_maquina_riesgos.updated_at', '>=', "{$fecha} 00:00:00")
                ->where('respuestas_maquina_riesgos.updated_at', '<=', "{$fecha} 23:59:59")
                ->where('ejecucion_sp', 1)
                ->select('respuestas_maquina_riesgos.*',
                    'Tb1_ResFinal_MR.TipoSolicitud',
                    'Tb1_ResFinal_MR.Monto_Mod',
                    'detalle_solicitudes_mr.BCSCORE', 'detalle_solicitudes_mr.MICROSCORE',
                    'detalle_solicitudes_mr.Origen', 'detalle_solicitudes_mr.Prestamo as MtoSolic',
                    'productos.nombre_producto')
                ->selectRaw('Substr(Cast(respuestas_maquina_riesgos.updated_at As Char),1,10) As Fec_Actualizacion,
                    Substr(Cast(respuestas_maquina_riesgos.updated_at As Char),1,4) As Anio,
                    Substr(Cast(respuestas_maquina_riesgos.updated_at As Char),6,2) As Mes,
                    Substr(Cast(respuestas_maquina_riesgos.updated_at As Char),9,2) As Dia')
                ->get();

                $casosDetallados = $respuestasMR->filter(function ($dato, $key) use ($respuestasMR) {

                if ($dato->decision == 'Rechazado') {
                    $dato->TipoCasosDetallado = 'Rechazado';
                    $dato->TipoCasosRepEC = 'Rechazado';
                } elseif ($dato->decision == 'Oferta Diferida') {
                    $dato->TipoCasosDetallado = 'Oferta Diferida';
                    $dato->TipoCasosRepEC = 'Oferta Diferida';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra <> 1
                    && $dato->status_oferta == 'Oferta Aceptada') {
                        $dato->TipoCasosDetallado = 'Continua-OfertaAceptada';
                        $dato->TipoCasosRepEC = 'Continua';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra == 1
                    && $dato->cuestionario_dinamico_guardado == 1
                    && $dato->status_oferta == 'Oferta Aceptada') {
                        $dato->TipoCasosDetallado = 'Continua-OfertaAceptada';
                        $dato->TipoCasosRepEC = 'Continua';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra <> 1
                    && $dato->status_oferta === null) {
                        $dato->TipoCasosDetallado = 'Continua-NoRespuestaOferta';
                        $dato->TipoCasosRepEC = 'Continua';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra == 1
                    && $dato->cuestionario_dinamico_guardado == 1
                    && $dato->status_oferta === null) {
                        $dato->TipoCasosDetallado = 'Continua-NoRespuestaOferta';
                        $dato->TipoCasosRepEC = 'Continua';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra <> 1
                    && $dato->status_oferta == 'Oferta Rechazada') {
                        $dato->TipoCasosDetallado = 'Continua-OfertaNoAceptada';
                        $dato->TipoCasosRepEC = 'Continua';
                }  elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra == 1
                    && $dato->cuestionario_dinamico_guardado == 1
                    && $dato->status_oferta == 'Oferta Rechazada') {
                        $dato->TipoCasosDetallado = 'Continua-OfertaNoAceptada';
                        $dato->TipoCasosRepEC = 'Continua';
                } elseif ($dato->decision == 'Invitación a Continuar'
                    && $dato->pantallas_extra == 1
                    && $dato->cuestionario_dinamico_guardado === null) {
                        $dato->TipoCasosDetallado = 'Continua-Cuestionario Incompleto';
                        $dato->TipoCasosRepEC = 'Continua';
                } else {
                    $dato->TipoCasosDetallado = '-';
                }

                if ($dato->decision === 'Rechazado'
                    || $dato->decision === 'Oferta Diferida'
                    || $dato->decision === 'Invitación a Continuar' && ($dato->tipo_poblacion === 'oferta_normal'
                    || $dato->tipo_poblacion === 'oferta_incrementada'
                    || $dato->tipo_poblacion === null)) {
                    $dato->Cuenta = true;
                } elseif ($dato->decision === 'Invitación a Continuar' && $dato->tipo_poblacion == 'oferta_doble') {

                    // Si es una invitacion a continuar y no ha aceptado/rechazado la oferta
                    // debe de contar la oferta menor
                    if (($dato->TipoCasosDetallado === 'Continua-Cuestionario Incompleto'
                        || $dato->TipoCasosDetallado === 'Continua-OfertaNoAceptada'
                        || $dato->TipoCasosDetallado === 'Continua-NoRespuestaOferta')
                        && $dato->tipo_oferta === 'oferta_normal' && !(isset($dato->Cuenta))) {
                        $dato->Cuenta = true;
                    } elseif (($dato->TipoCasosDetallado === 'Continua-Cuestionario Incompleto'
                        || $dato->TipoCasosDetallado === 'Continua-OfertaNoAceptada'
                        || $dato->TipoCasosDetallado === 'Continua-NoRespuestaOferta')
                        && $dato->tipo_oferta === 'oferta_mayor' && !(isset($dato->Cuenta))) {
                        $dato->Cuenta = false;
                    }

                    // Si es una invitacion a continuar y ya acepto la oferta, solo debe de
                    // contar la oferta aceptada
                    if ($dato->TipoCasosDetallado === 'Continua-OfertaAceptada' &&
                        $dato->status_oferta == 'Oferta Aceptada') {
                        $dato->Cuenta = true;

                        $respuestasID = $respuestasMR->where('solicitud_id', $dato->solicitud_id)
                            ->where('status_oferta', null);

                        foreach ($respuestasID as $key => $value) {
                            $respuestasMR[$key]->Cuenta = false;
                        }
                    }

                } else {
                    $dato->Cuenta = '--';
                }

                $dato->MontoF2_Mod = ($dato->Monto_Mod == -1) ? $dato->MtoSolic : $dato->Monto_Mod;

                if ($dato->BCSCORE <= 560) {
                    $dato->RangoBCScore = '(--- -560]';
                } elseif ($dato->BCSCORE >= 560 && $dato->BCSCORE < 580) {
                    $dato->RangoBCScore = '[560-580)';
                } elseif ($dato->BCSCORE >= 580 && $dato->BCSCORE < 600) {
                    $dato->RangoBCScore = '[580-600)';
                } elseif ($dato->BCSCORE >= 600) {
                    $dato->RangoBCScore = '[600-+++)';
                } else {
                    $dato->RangoBCScore = '';
                }

                if ($dato->MICROSCORE >= 600) {
                    $dato->RangoMicroScore = '[600,+++)';
                } elseif ($dato->MICROSCORE < 600) {
                    $dato->RangoMicroScore = '[--- ,600)';
                } else {
                    $dato->RangoMicroScore = '';
                }

                if ($dato->BCSCORE >= 600 && $dato->MICROSCORE >= 600) {
                    $dato->RangoBCMicro = 'BC>=600,MS>=600';
                } elseif ($dato->BCSCORE >= 600 && $dato->MICROSCORE < 600) {
                    $dato->RangoBCMicro = 'BC>=600,MS<600';
                } elseif ($dato->BCSCORE < 600 && $dato->MICROSCORE >= 600) {
                    $dato->RangoBCMicro = 'BC<600,MS>=600';
                } elseif ($dato->BCSCORE < 600 && $dato->MICROSCORE < 600) {
                    $dato->RangoBCMicro = 'BC<600,MS<600';
                } else {
                    $dato->RangoBCMicro = '';
                }

                if ($dato->BCSCORE >= 560 && $dato->MICROSCORE >= 600) {
                    $dato->RangoBCMicro_V2 = 'BC>=560,MS>=600';
                } elseif ($dato->BCSCORE >= 560 && $dato->MICROSCORE < 600) {
                    $dato->RangoBCMicro_V2 = 'BC>=560,MS<600';
                } elseif ($dato->BCSCORE < 560 && $dato->MICROSCORE >= 600) {
                    $dato->RangoBCMicro_V2 = 'BC<560,MS>=600';
                } elseif ($dato->BCSCORE < 560 && $dato->MICROSCORE < 600) {
                    $dato->RangoBCMicro_V2 = 'BC<600,MS<600';
                } else {
                    $dato->RangoBCMicro_V2 = '';
                }


                if ($dato->BCSCORE >= 560 && $dato->MICROSCORE >= 600) {
                    $dato->RangoBCMicro_V3 = 'BC>=560,MS>=600';
                } elseif ($dato->BCSCORE >= 560) {
                    $dato->RangoBCMicro_V3 = 'BC>=560';
                } elseif ($dato->BCSCORE == -8) {
                    $dato->RangoBCMicro_V3 = 'BC=-8';
                } else {
                    $dato->RangoBCMicro_V3 = '';
                }

                return $dato;
            });

            $casosFiltrados = $casosDetallados->whereNotIn('prospecto_id', [4, 5, 975]);
            $casosFiltrados = $casosFiltrados->where('Cuenta', 1);

            foreach ($casosFiltrados as $key => $caso) {
                $datoReporte = ReporteEficiencia::updateOrCreate([
                    'prospecto_id'          => $caso->prospecto_id,
                    'solicitud_id'          => $caso->solicitud_id,
                ], [
                    'Producto'              => mb_strtoupper($caso->nombre_producto),
                    'Origen'                => $caso->Origen,
                    'Anio'                  => $caso->Anio,
                    'Mes'                   => $caso->Mes,
                    'Dia'                   => $caso->Dia,
                    'TipoSolicitud'         => $caso->TipoSolicitud,
                    'RangoBCScore'          => $caso->RangoBCScore,
                    'RangoBCMicro'          => $caso->RangoBCMicro,
                    'RangoBCMicro_V2'       => $caso->RangoBCMicro_V2,
                    'RangoBCMicro_V3'       => $caso->RangoBCMicro_V3,
                    'TipoCasosDetallado'    => $caso->TipoCasosDetallado,
                    'TipoCasosRepEC'        => $caso->TipoCasosRepEC,
                    'MontoF2_Mod'           => $caso->MontoF2_Mod,
                    'Fec_Actualizacion'     => $caso->Fec_Actualizacion,
                    'Creado'                => $caso->created_at,
                    'Actualizado'           => $caso->updated_at,
                ]);

            }

            $resultado1 = ReporteEficiencia::selectRaw('TipoCasosRepEC, Count(*) As Poblacion, Origen, Producto')
                ->where('Fec_Actualizacion', $fecha)
                ->groupBy('TipoCasosRepEC', 'Origen', 'Producto')
                ->get()
                ->toArray();

            $reporte1 = Excel::create('Resultado1_'.$fecha, function($excel) use($resultado1) {
                $excel->sheet('Sheetname', function($sheet) use($resultado1) {
                    $sheet->fromArray($resultado1);
                });
            })->store('csv', false, true);

            $mesFecha= Carbon::createFromFormat('Y-m-d', $fecha)->format('m');
            $añoFecha= Carbon::createFromFormat('Y-m-d', $fecha)->format('Y');
            if ($mesFecha >= 1 && $mesFecha <= 6) {
                $inicioFiltro = "{$añoFecha}-01-01";
            } elseif ($mesFecha >= 7 && $mesFecha <= 12) {
                $inicioFiltro = "{$añoFecha}-07-01";
            }

            $resultado2 = ReporteEficiencia::selectRaw('Anio, Mes, Dia, TipoSolicitud, RangoBCScore, RangoBCMicro, RangoBCMicro_V2, RangoBCMicro_V3, Count(*) As Poblacion')
                ->groupBy('Anio', 'Mes', 'Dia', 'TipoSolicitud', 'RangoBCScore', 'RangoBCMicro', 'RangoBCMicro_V2', 'RangoBCMicro_V3')
                ->whereRaw("Fec_Actualizacion >= '{$inicioFiltro}' AND Fec_Actualizacion <= '{$fecha}'")
                ->get()
                ->toArray();

            $reporte2 = Excel::create('Resultado2_'.$fecha, function($excel) use($resultado2) {
                $excel->sheet('Sheetname', function($sheet) use($resultado2) {
                    $sheet->fromArray($resultado2);
                });
            })->store('csv', false, true);

            $resultado3 = ReporteEficiencia::selectRaw('Origen, Anio, Mes, Dia, TipoCasosDetallado, AntesRelease25May, Count(*) As Poblacion, Producto')
                ->groupBy('Origen', 'Anio', 'Mes', 'Dia', 'TipoCasosDetallado', 'AntesRelease25May', 'Producto')
                ->whereRaw("Fec_Actualizacion >= '{$inicioFiltro}' AND Fec_Actualizacion <= '{$fecha}'")
                ->get()
                ->toArray();

            $reporte3 = Excel::create('Resultado3_'.$fecha, function($excel) use($resultado3) {
                $excel->sheet('Sheetname', function($sheet) use($resultado3) {
                    $sheet->fromArray($resultado3);
                });
            })->store('csv', false, true);

            $resultado4 = ReporteEficiencia::selectRaw('Origen, Anio, Mes, Dia, TipoCasosDetallado, Sum(MontoF2_Mod) As SMonto, Count(*) As Poblacion, Producto')
                ->groupBy('Origen', 'Anio', 'Mes', 'Dia', 'TipoCasosDetallado', 'Producto')
                ->whereRaw("Fec_Actualizacion >= '{$inicioFiltro}' AND Fec_Actualizacion <= '{$fecha}'")
                ->get()
                ->toArray();

            $reporte4 = Excel::create('Resultado4_'.$fecha, function($excel) use($resultado4) {
                $excel->sheet('Sheetname', function($sheet) use($resultado4) {
                    $sheet->fromArray($resultado4);
                });
            })->store('csv', false, true);

            $connector = new \Sebbmyr\Teams\TeamsConnector(env('MICROSOFT_TEAMS_WEBHOOK'));
            $card  = new App\Repositories\CardReporte([
                'title'     => 'Reportes Eficiencia Monte '.$fecha,
                'reporte_1' => env('APP_URL').'descargaReporte/'.$reporte1['title'],
                'reporte_2' => env('APP_URL').'descargaReporte/'.$reporte2['title'],
                'reporte_3' => env('APP_URL').'descargaReporte/'.$reporte3['title'],
                'reporte_4' => env('APP_URL').'descargaReporte/'.$reporte4['title'],
            ]);
            $connector->send($card);

        } else {

            echo 'Disponible solo en Producción';

        }

    }

    public function descargaReporte($file)
    {
        $url = Storage::disk('reportes')->path($file.'.csv');
        return response()->download($url);

    }

}
