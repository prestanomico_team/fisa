<?php
namespace App\Http\Controllers;

use App\Analytic;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;
use App\Solicitud;
//use DB;
use Excel;
use Log;
use Carbon\Carbon;
use Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use App\Repositories\CurlCaller;

class ReporteSolicitudesDiariasController extends Controller
{

    public function getSolicitudesDiarias(Request $request)
    {

        ini_set('memory_limit','768M');
        
        $sDate= Carbon::now()->format('Y-m-d');
        $eDate= Carbon::yesterday()->format('Y-m-d');
        
        $solics = Solicitud::with('prospecto', 'domicilio', 'producto', 'bc_score', 'convenio')
            ->whereBetween('updated_at',[$eDate, $sDate])
            ->orderBy('updated_at', 'desc');
            
        $reporte1 = Excel::create('tabla-solicitudes-'.$eDate, function ($excel) use ($solics) {
            //Datos de Solicitud
            $excel->sheet('Datos', function ($sheet) use ($solics) {
                // Set auto size for sheet
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'SOLICITUD ID',
                    'PROSPECTO ID',
                    'FECHA REGISTRO',
                    'PRODUCTO',
                    'STATUS',
                    'ULTIMO PUNTO REGISTRO',
                    'ÚLTIMA ACTUALIZACIÓN',
                    'PRESTAMO',
                    'PLAZO',
                    'FINALIDAD',
                    'NOS GUSTARÍA CONOCERTE',
                    'NOMBRE',
                    'APELLIDO P',
                    'APELLIDO M',
                    'GENERO',
                    'EMAIL',
                    'CELULAR',
                    'EDAD',
                    'CRED HIPOTECARIO',
                    'CRED AUTOMOTRIZ',
                    'CRED BANCARIO (TDC)',
                    '4 ULT TDC',
                    'ORIGEN',
                    '(LOG DE CONSOLA) ENCONTRADO',
                    'RFC',
                    'ESTADO CIVIL',
                    'RESIDENCIA',
                    '# DEPENDIENTES',
                    'NIVEL DE ESTUDIOS',
                    'OCUPACION',
                    'INGRESO',
                    'GASTOS FAMILIARES',
                    'FECHA DE NACIMIENTO',
                    'BC SCORE',
                    'MICRO SCORE',
                    'IP ADDRESS',
                    'DOMICILIO AÑOS',
                    'ANTIGUEDAD EMPLEO',
                    'LUGAR DE NACIMIENTO',
                    'TELEFONO DOMICILIO',
                    'TELEFONO OFICINA',
                    'CURP',
                    'CALLE',
                    'NUM EXT',
                    'NUM INT',
                    'COLONIA',
                    'DELEGACION/MUNICIPIO',
                    'CIUDAD',
                    'DOMICILIO ESTADO',
                    'CODIGO POSTAL',
                    'DISPOSITIVO ENTRADA',
                    'DISPOSITIVO SALIDA',
                    'USUARIO',
                    '# EJECUTIVO'
                ));

                $solics->chunk(1000, function($rows) use ($sheet)
                {
                    foreach ($rows as $sol)
                    {
                        $tipoDispositivo = Solicitud::with('primerDispositivo', 'ultimoDispositivo')->find($sol->id);
                        $referencia = null;
                        $producto = $sol->producto;
                        if (count($producto) != 0) {
                            $nombre_producto = mb_strtoupper($producto[0]->nombre_producto);
                            if ($producto[0]->pivot['lead'] != null) {
                                $referencia = mb_strtoupper($producto[0]->pivot['lead']);
                            }
                        } else {
                            $nombre_producto = 'MERCADO ABIERTO';
                        }

                        $calle = '';
                        $num_exterior = '';
                        $num_interior = '';
                        $colonia = '';
                        $delegacion = '';
                        $ciudad = '';
                        $estado = '';
                        $cp = '';
                        if (isset($sol->domicilio['calle'])) {
                            $calle = $sol->domicilio['calle'];
                            $num_exterior = $sol->domicilio['num_exterior'];
                            $num_interior = $sol->domicilio['num_interior'];
                            $colonia = $sol->domicilio['colonia'];
                            $delegacion = $sol->domicilio['delegacion'];
                            $ciudad = $sol->domicilio['ciudad'];
                            $estado = $sol->domicilio['codigo_estado'];
                            $cp = $sol->domicilio['cp'];
                        }

                        if ($referencia == null) {
                            if (isset($sol->prospecto->referencia)) {
                                $referencia = $sol->prospecto->referencia;
                            }
                        }

                        $sheet->appendRow(array(
                            $sol->id,
                            $sol->prospecto_id,
                            $sol->created_at,
                            $nombre_producto,
                            $sol->status,
                            $sol->sub_status,
                            $sol->updated_at,
                            $sol->prestamo,
                            self::getPlazoLabel($sol->plazo),
                            $sol->finalidad,
                            $sol->finalidad_custom,
                            $sol->prospecto->nombres,
                            $sol->prospecto->apellido_paterno,
                            $sol->prospecto->apellido_materno,
                            $sol->sexo,
                            $sol->prospecto->email,
                            $sol->prospecto->celular,
                            ($sol->fecha_nacimiento != '0000-00-00') ? Carbon::parse($sol->fecha_nacimiento)->age : '',
                            self::getYesOrNo($sol->credito_hipotecario),
                            self::getYesOrNo($sol->credito_automotriz),
                            self::getYesOrNo($sol->credito_bancario),
                            $sol->ultimos_4_digitos,
                            $referencia,
                            self::getYesOrNo($sol->encontrado),
                            $sol->rfc,
                            self::getEstadoCivilDesc($sol->estado_civil),
                            $sol->tipo_residencia,
                            $sol->numero_dependientes,
                            $sol->nivel_estudios,
                            $sol->ocupacion,
                            $sol->ingreso_mensual,
                            $sol->gastos_familiares,
                            ($sol->fecha_nacimiento != '0000-00-00') ? $sol->fecha_nacimiento : '',
                            isset($sol->bc_score) ? $sol->bc_score->bc_score : '',
                            isset($sol->bc_score) ? $sol->bc_score->micro_valor : '',
                            $sol->user_ip,
                            $sol->antiguedad_domicilio,
                            $sol->antiguedad_empleo,
                            (!empty($sol->lugar_nacimiento_ciudad))? $sol->lugar_nacimiento_ciudad.', '.$sol->lugar_nacimiento_estado : '',
                            $sol->telefono_casa,
                            $sol->telefono_empleo,
                            $sol->curp,
                            $calle,
                            $num_exterior,
                            $num_interior,
                            $colonia,
                            $delegacion,
                            $ciudad,
                            $estado,
                            $cp,
                            isset($tipoDispositivo->primerDispositivo->tipo_dispositivo) ? $tipoDispositivo->primerDispositivo->tipo_dispositivo : '',
                            isset($tipoDispositivo->ultimoDispositivo->tipo_dispositivo) ? $tipoDispositivo->ultimoDispositivo->tipo_dispositivo : '',
                            isset($sol->convenio['email']) ? self::getEmbajador($sol->convenio['email']) : '',
                            isset($sol->convenio['email']) ? self::getIdEmbajador($sol->convenio['email']) : ''
                        ));
                    }
                });
            });
        })->store('xls', storage_path('excel/'),true);
        
        $root ='reportes_diarios_fmp';
        Storage::disk('s3')->put("{$root}/{$reporte1['file']}", \File::get($reporte1['full']));

        
        $curl = new CurlCaller();
        
        $bucket = env('AWS_S3_BUCKET');
        $key = 'reportes_diarios_fmp/tabla-solicitudes-'.$eDate.'.xls';
        $path = 'ReportePrestanomico';
        $filename = 'tabla-solicitudes-'.$eDate.'.xls';
        
        $response = $curl->getDatosTitan($bucket, $key, $path, $filename);
        Log::info($response);
        
    }

    public function getConsultasBuroDiarias(Request $request)
    {
        ini_set('memory_limit','768M');
        
        $sDate= Carbon::now()->format('Y-m-d');
        $eDate= Carbon::yesterday()->format('Y-m-d');
        
        $consultas = DB::table('consultas_buro')
                            ->select('prospecto_id','consultas_buro.solicitud_id','cadena_original','folio_consulta','consultas_buro.created_at')
                            ->join('tracking_solicitud','consultas_buro.solicitud_id', '=', 'tracking_solicitud.solicitud_id')
                            ->where('sub_status', 'Reporte Completo')
                            ->where('orden', 'Segunda')
                            ->where('tipo', 'Respuesta')
                            ->whereBetween('consultas_buro.created_at',[$eDate, $sDate])
                            ->get();

        $consultas= json_decode( json_encode($consultas), true);                    
        
        $reporte2 = Excel::create('tabla-consultasBuro-'.$eDate, function ($excel) use ($consultas) {
            $excel->sheet('Sheetname', function($sheet) use($consultas) {
                $sheet->fromArray($consultas);
            });
        })->store('xls', storage_path('excel/'), true);
        
        $root ='reportes_diarios_fmp';
        Storage::disk('s3')->put("{$root}/{$reporte2['file']}", \File::get($reporte2['full']));

        $curl = new CurlCaller();
        
        $bucket = env('AWS_S3_BUCKET');
        $key = 'reportes_diarios_fmp/tabla-consultasBuro-'.$eDate.'.xls';
        $path = 'ReportePrestanomico';
        $filename = 'tabla-consultasBuro-'.$eDate.'.xls';
        
        $response = $curl->getDatosTitan($bucket, $key, $path, $filename);
        Log::info($response);
        
       
    }

    
}