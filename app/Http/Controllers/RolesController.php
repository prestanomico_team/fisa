<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use Auth;
use Validator;
use Illuminate\Validation\Rule;

class RolesController extends Controller
{

    /**
     * Muestra el listado de roles/perfiles con sus permisos asignados
     *
     * @return view Vista que contiene el listao de roles/perfiles.
     */
    public function index() {

        $roles = Role::with('perms')->paginate(10);

        return view(
            'usuarios.perfiles.index',
            [
                'roles'  => $roles,
            ]
        );

    }

    /**
     * Muestra el formulario para editar los datos del rol/perfil.
     *
     * @param  Integer $id Id del rol/perfil
     *
     * @return view        Vista con el formulario para editar el rol/perfil.
     */
    public function editar($id) {

        try {

            $rol = Role::with('perms')->where('id', $id)->get();
            $permisos = Permission::all();

            return view(
                'usuarios.perfiles.editar',
                [
                    'rol'       => $rol,
                    'permisos'  => $permisos,
                ]
            );

        } catch (\Exception $e) {

        }

    }

    /**
     * Actualiza los datos del usuario.
     *
     * @param  Integer $id      Id del rol/perfil a actualizar.
     * @param  Request $request Arreglo con los datos del rol/perfil.
     *
     * @return json             Resultado de actualizar los datos del rol/perfil.
     */
    public function actualizar($id, Request $request) {

        $rol = Role::findOrFail($id);
        $permisos = $request->permisos;

        try {

            if ($permisos != 'null') {
                $permisos = explode(',', $permisos);
                $rol->perms()->sync($permisos);
            }

            return response()->json(['success' => true]);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);

        }

    }

    /**
     * Crea un nuevo rol/perfil.
     *
     * @param  Request $request Arreglo con los datos del rol/perfil.
     *
     * @return json             Resultado de crear el rol/perfil.
     */
    public function save(Request $request) {

        try {

            $messages = [
                'required'                  => '* El campo es requerido.',
                'alpha_numeric_spaces'      => '* El campo solo puede contener caracteres alfanuméricos.',
                'max'                       => '* El campo no puede contener mas de :max caracteres.',
                'unique_rol'                => '* El perfil ya existe',
            ];

            $displayName = $request->display_name;
            $area = $request->area;
            $name = mb_strtolower($displayName).'.'.mb_strtolower($area);
            $description = ucfirst($request->description);
            $displayName = ucfirst($displayName);

            $validator = Validator::make($request->all(), [
                'display_name'  => 'required|unique_rol:'.$name.'|alpha_numeric_spaces|max:50',
                'area'          => 'required',
                'description'   => 'required|alpha_numeric_spaces|max:100',
            ], $messages);

            $errores = $validator->errors()->messages();

            if (count($errores) == 0) {

                $rol = Role::updateOrCreate(
                    [
                        'name' => $name
                    ],
                    [
                        'display_name'  => $displayName,
                        'area'          => $area,
                        'description'   => $description,
                    ]
                );

                return response()->json(['success' => true]);

            } else {

                return response()->json([
                    'success' => false,
                    'message' => 'Faltan datos obligatorios.',
                    'errores' => $errores,
                ]);

            }

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);

        }

    }


}
