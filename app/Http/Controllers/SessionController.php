<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Solicitud;
use App\Prospecto;
use App\Plazo;
use App\User;
use Excel;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Analytic;
use App\RespuestaMaquinaRiesgo;
use App\Situacion;
use App\PlantillaComunicacion;
use Jenssegers\Agent\Agent;
use Cookie;
use App\CodigosSMS;
use App\ClientesAlta;
use App\MB_UPLOADED_DOCUMENT;
use Illuminate\Support\Facades\Storage;
use App\Repositories\SolicitudRepository;
use App\Repositories\LDAPRepository;
use App\Repositories\CalixtaRepository;
use App\Repositories\CognitoRepository;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Lang;

class SessionController extends Controller
{
    use ThrottlesLogins;

    private $solicitudRepository;
    private $ldap;
    private $cognito;
    public $maxAttempts = 3;
    public $decayMinutes = 15;

    public function __construct () {
        $this->solicitudRepository = new SolicitudRepository;
        $this->cognito = new CognitoRepository;
    }

    /**
    * Hace el login de un usuario registrado en LDAP
    *
    * @param  request $request Arreglo con los datos del usuario que se quiere loguear
    *
    * @return json Resultado de realizar el login
    */
    public function loginLDAP(request $request, CognitoRepository $cognito, CalixtaRepository $calixta) {

        $times = $this->incrementLoginAttempts($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $data = $request->toArray();
        $data['email'] = mb_strtolower($data['email']);
        $hostname = self::getCurrentHost();
        $login = $cognito->authenticate($data['email'], $data['password']);

        if ($login['success'] == true) {

            // Si el login fue exitoso en Cognito, buscamos los datos del prospecto en la BD
            $prospecto = Prospecto::where('email', $data['email'])->first();
            if ($prospecto) {

                $this->clearLoginAttempts($request);

                $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
                    ->orderBy('created_at', 'DESC')
                    ->first();

                // Actualizando la contraseña
                $password  = $data['password'];
                Prospecto::where('email', $data['email'])
                    ->update(['encryptd' => encrypt($password)]);

                $agent = new Agent();
                $prospecto_id = $prospecto->id;
                $tipo_dispositivo = null;
                $marca =  null;

                if ($agent->isDesktop()) {
                    $tipo_dispositivo = 'desktop';
                    $marca = $agent->device();
                }
                if ($agent->isMobile()) {
                    $tipo_dispositivo = 'mobile';
                    $marca = $agent->device();
                }
                if ($agent->isTablet()) {
                    $tipo_dispositivo = 'tablet';
                    $marca = $agent->device();
                }

                $analytic = Analytic::insert([
                    'prospecto_id'      => $prospecto_id,
                    'solicitud_id'      => $solicitud->id,
                    'client_id'         => $request->input('clientId'),
                    'telefono'          => $prospecto->celular,
                    'email'             => $data['email'],
                    'ip'                => self::getUserIP(),
                    'sistema_operativo' => $agent->platform(),
                    'navegador'         => $agent->browser(),
                    'navegador_version' => $agent->version($agent->browser()),
                    'tipo_dispositivo'  => $tipo_dispositivo,
                    'marca'             => $marca,
                    'datos_obtenidos'   => 'Sin datos',
                    'fecha_hora'        => date('YmdHi'),
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);

                $prospecto->login = true;
                $prospecto->save();
                $siguientePaso = $this->solicitudRepository->siguientePaso(null, $prospecto, true);

                // La solicitud esta inconclusa y necesita autenticarse para continuar
                if ($siguientePaso['formulario'] == 'verificar_codigo') {

                    $codigo = mt_rand(10000, 99999);
                    $msj = "Código de Verificación: ". $codigo;
                    $responsecalixta = $calixta->sendSMS($prospecto->celular, $msj);
                    $responsecalixta = json_decode($responsecalixta);

                    if ($responsecalixta->success == true) {

                        $codigo_sms = CodigosSMS::create([
                            'prospecto_id'  => $prospecto_id,
                            'codigo'        => $codigo,
                            'celular'       => $prospecto->celular,
                            'nombre'        => $prospecto->nombres.' '.$prospecto->apellido_paterno,
                            'email'         => $prospecto->email,
                            'enviado'       => $responsecalixta->success,
                            'descripcion'   => $responsecalixta->message,
                            'verificado'    => false,
                        ]);

                        $siguientePaso['success'] = true;
                        $plazo = Plazo::find($prospecto->ultima_solicitud->plazo);
                        $siguientePaso['prestamo'] = '$ '.number_format($prospecto->ultima_solicitud->prestamo, 2);
                        $siguientePaso['plazo'] = "{$plazo->duracion} {$plazo->plazo}";
                        $siguientePaso['finalidad'] = $prospecto->ultima_solicitud->finalidad;
                        $siguientePaso['celular'] = $prospecto->celular;
                        Auth::guard('prospecto')->loginUsingId($prospecto_id);

                    } else {

                        $siguientePaso['success'] = false;
                        $siguientePaso['message'] = $responsecalixta->message;

                    }

                // La solicitud esta en un status de terminación exitoso o fallido y
                // debe de empezar una nueva
                } else {
                    $prospecto->login = false;
                    $prospecto->save();
                    $siguientePaso['success'] = true;
                    $siguientePaso['formulario'] = 'simulador';
                    session()->flash('nueva_solicitud', true);
                    Auth::guard('prospecto')->loginUsingId($prospecto_id);
                }

                $eventTM[] = ['userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $siguientePaso['eventTM'] = $eventTM;

                $siguientePaso['nombre_prospecto'] = "{$prospecto->nombres} {$prospecto->apellido_paterno} {$prospecto->apellido_materno}";
                return response()->json($siguientePaso);

            } else {

                $login = (array) $login;
                $login['message'] = 'El usuario o contraseña son incorrectos.';
                $login['success'] = false;
                return response()->json($login);
            }

        } else {

            if ($login['message'] == 'PasswordResetRequiredException') {
                $login['message'] = 'Se debe completar la solicitud de restauración de contraseña.';
            } else if ($login['message'] == 'UserNotFoundException') {
                $login['message'] = 'Usuario y/o contraseña incorrectos.';
            } else if ($login['message'] == 'Incorrect username or password.') {
                $login['message'] = 'Usuario y/o contraseña incorrectos.';
            }

            return response()->json($login);

        }

    }

    public function restaurarContraseña($datos) {
        /*
        $response = \EmailChecker::check('ferry.germanc_e615p@anom.xyz');
        print_r($response);
        die();
        $response = $this->cognito->sendResetLink('deivguerrero@gmail.com');
        print_r($response);
        */
    }

    public function username() { return 'email'; }

    protected function incrementLoginAttempts(Request $request)
    {
        return ($this->limiter()->hit(
            $this->throttleKey($request), $this->decayMinutes()
        ));
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        if ($seconds > 60) {
            $minutes = gmdate('i', $seconds);
            $message = Lang::get('auth.throttle_minutes', ['minutes' => $minutes]);
        } else {
            $message = Lang::get('auth.throttle', ['seconds' => $seconds]);
        }

        $errors = [
            'success' => false,
            'message' => $message
        ];

        if ($request->expectsJson()) {
            return response()->json($errors, 423);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

}
