<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Prospecto;
use App\Solicitud;
use App\Analytic;
use App\Finalidad;
use App\Plazo;
use Cookie;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use App\Producto;
use App\OfertaRenovacion;
use App\OfertaRenovacionSitio;
use Log;
use App;
use Auth;
use Jenssegers\Agent\Agent;
use App\Repositories\SolicitudRepository;
use Session;
use Validator;
use Illuminate\Validation\Rule;
use App\Repositories\CognitoRepository;
use Hash;
use App\W_USER;
use Uuid;
use Excel;

class SindicalizadosController extends Controller
{
    private $solicitudRepository;

    public function __construct()
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function validaciones(Request $request, $formulario = null) {

        if ($request->has('datos') && $request->has('formulario')) {

            $datos = $request->datos;
            $formulario = $request->formulario;

            $validaciones = $this->solicitudRepository->validacionesSolicitud($datos, $formulario);
            if ($validaciones->fails()) {
                return response()->json([
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ]);
            } else {
                return response()->json([
                    'success'   => true,
                    'errores'   => []
                ]);
            }

        } elseif ($formulario != null) {

            $validaciones = $this->solicitudRepository->validacionesSolicitud($request->toArray(), $formulario);
            if ($validaciones->fails()) {
                return [
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ];
            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Operacion no permitida'
            ]);

        }

    }

    /**
     * Registra el prospecto en la base de datos
     *
     * @param  request $request Contiene los datos enviados por el usuario en el
     * formulario
     *
     * @return  json Respuesta de registrar el prospecto
     */
    public function registroProspectoSindicalizado(Request $request) {

        $reintento = true;
        $no_intentos = 1;
        $intentos = 0;

        $validaciones = $this->validaciones($request, 'registro-simulador');
        if ($validaciones != '') {
            return response()->json($validaciones);
        }

        $cognito_id = null;
        $responseCognito = $this->registroCognito($request);
        if ($responseCognito['success'] == false) {
            return response()->json($responseCognito);
        } else {
            $cognito_id = $responseCognito['cognito_id'];
        }

        $prospecto = Prospecto::firstOrCreate([
            'email' => mb_strtolower($request->email)
        ]);

        // Si el prospecto fue creado recientemente, se actualizan atributos
        // faltantes y se crea solicitud
        if ($prospecto->wasRecentlyCreated) {

            $prospecto->nombres               = mb_strtoupper($request->nombres);
            $prospecto->apellido_paterno      = mb_strtoupper($request->apellido_paterno);
            $prospecto->apellido_materno      = mb_strtoupper($request->apellido_materno);
            $prospecto->celular               = $request->celular;
            $prospecto->password              = Hash::make($request->contraseña);
            $prospecto->encryptd              = encrypt($request->contraseña);
            $prospecto->usuario_ldap          = 1;
            $prospecto->sms_verificacion      = 0;
            $prospecto->usuario_confirmado    = 0;
            $prospecto->referencia            = 'SITIO';
            $prospecto->cognito_id            = $cognito_id;
            $prospecto->acepto_avisop         = $request->acepto_avisotyc;
            $prospecto->save();

            $w_user = W_USER::updateOrCreate([
                'ID_USER'       => mb_strtolower($request->email)
            ], [
                'PHONE_NUMBER'  => $request->celular,
                'CREATE_DATE'   => Carbon::now(),
                'UPDATE_DATE'   => Carbon::now()
            ]);

            // Obteniendo la descripcion de la finalidad
            $finalidad = $request->input('finalidad');
            $finalidad = Finalidad::find($finalidad);
            if (isset($finalidad->finalidad)) {
                $finalidad = mb_strtoupper($finalidad->finalidad);
            } else {
                $finalidad = Finalidad::find(1);
                $finalidad = mb_strtoupper($finalidad->finalidad);
            }

            // Obteniendo la clave del plazo
            $plazo = $request->input('plazo');
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (isset($plazo->clave)) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $solicitud = Solicitud::create([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $request->monto_prestamo,
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $request->pago_estimado,
                'finalidad'         => $finalidad,
                'finalidad_custom'  => mb_strtoupper($request->finalidad_custom),
            ]);

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($request->email, ['custom:idProspecto' => 'FMP-'.$prospecto->id]);

            Auth::guard('prospecto')->loginUsingId($prospecto->id);

            $r_parameter = 'none';
            $campaña = 'none';
            if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                $r_parameter = Cookie::get('r_parameter');
            } else if (Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña');
            }

            $campaña = $campaña == 'none' ? null : $campaña;
            $r_parameter = $r_parameter == 'none' ? null : $r_parameter;
            $producto = Producto::where('alias', 'sindicalizados')->first();

            // Agregando el producto a la solicitud
            $producto_id = $producto->id;
            $version_producto = Producto::find($producto->id)->getCurrentVersionNo();
            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                ]
            ]);

            Cookie::queue(Cookie::forget('campaña'));
            Cookie::queue(Cookie::forget('r_parameter'));

            if ($request->has('sucursal')) {
                $sucursal = $request->sucursal;
                $datosSucursal = UserConvenio::find($sucursal);
                if (isset($datosSucursal->sucursal)) {
                    $datosConvenio = DatosConvenio::updateOrCreate([
                        'prospecto_id'  => $prospecto->id,
                        'solicitud_id'  => $solicitud->id
                    ], [
                        'empresa'   => $datosSucursal->empresa,
                        'sucursal'  => $datosSucursal->sucursal,
                        'embajador' => $datosSucursal->embajador,
                        'telefono'  => $datosSucursal->telefono,
                        'email'     => $datosSucursal->email
                    ]);
                }
            }

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if ($agent->isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = $agent->device();
            }
            if ($agent->isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = $agent->device();
            }
            if ($agent->isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = $agent->device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $request->input('clientId'),
                'email'             => $request->input('email'),
                'ip'                => self::getUserIP(),
                'telefono'          => $request->input('celular'),
                'sistema_operativo' => $agent->platform(),
                'navegador'         => $agent->browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id
            ];

            $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Usuario y Solicitud creados con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $evento = $this->solicitudRepository->getEvento($campaña, 'RegistroUsuario');

            $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $mnsj_str['eventTM'] = $eventTM;

            return response()->json($mnsj_str, 201);

        } elseif($prospecto->id) {

            if ($prospecto->cognito_id == null) {
                $prospecto->cognito_id = $cognito_id;
                $prospecto->save();
            } else {

                return response()->json([
                    'success'           => false,
                    'msg'               => 'El usuario ya esta registrado',
                    'siguiente_paso'    => 'login'
                ]);
            }

        } else {

            return response()->json([
                'success' => false,
                'msg'     => 'Hubo un error al crear el prospecto y la solicitud',
            ]);

        }

    }


    public function registroCognito(Request $request) {

        $cognito = new CognitoRepository;

        if ($request->apellido_materno != '')  {
            $apellidos = mb_strtoupper("{$request->apellido_paterno} {$request->apellido_materno}");
        } else {
            $apellidos = mb_strtoupper($request->apellido_paterno);
        }
        $fecha = Carbon::now('UTC');

        $datos_usuario = [
            'name'                  => mb_strtoupper($request->nombres),
            'middle_name'           => $apellidos,
            'phone_number'          => '+52'.$request->celular,
        ];

        $cognito = $cognito->createUserCognito(mb_strtolower($request->email), $request->contraseña, $datos_usuario);

        return $cognito;

    }


}
