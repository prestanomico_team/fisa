<?php
namespace App\Http\Controllers;

use SoapWrapper;

class SoapController extends Controller {
	public function demo()
	{
		// Add a new service to the wrapper
		SoapWrapper::add(function ($service) {
		    $service->name('currency')->wsdl('http://currencyconverter.kowabunga.net/converter.asmx?WSDL');
		});

		$data = [
		    'CurrencyFrom' => 'USD',
		    'CurrencyTo'   => 'MXN',
		    'RateDate'     => '2016-06-05',
		    'Amount'       => 1.00
		];

		SoapWrapper::service('currency',function($service) use ($data) {
		    var_dump($service->call('GetCurrencies',$data));
		    var_dump($service->call('GetConversionAmount',$data)->GetConversionAmountResult);
		});
	}
}