<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Prospecto;
use App\Solicitud;
use App\MockPerson;
use App\FicoUtil;
use App\PlazaCobertura;
use App\SoapHttpClient;
use App\Sepomex;
use App\DomicilioSolicitud;
use App\ConsultaBcTest;
use App\DetalleCuenta;
use App\DetalleConsulta;
use App\DetalleSolicitud;
use App\OfertaPredominante;
use App\RespuestaEvaluacionExperian;
use App\PlantillaComunicacion;
use App\Situacion;
use App\MotivoRechazo;
use App\RespuestaCuestionarioDinamico;
use App\RespuestaMaquinaRiesgo;
use App\CatalogoSepomex;
use App\Plazo;
use Cookie;
use Validator;
use App\Repositories\SolicitudRepository;

use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Encryption\DecryptException;

class SolicitudApiController extends Controller
{
    private $configuracionProducto;
    private $bcScore;
    private $edadMinima;
    private $edadMaxima;
    private $campoCobertura;
    private $alp;
    private $solicitudRepository;

    /**
    * Constructor de la clase
    */
    public function __construct(request $request)
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        if (isset($request->solic_bc_id)) {
            $solicitud_id = $request->solic_bc_id;
        } elseif (isset($request->solic_hawk_id)) {
            $solicitud_id = $request->solic_hawk_id;
        } elseif (isset($request->solic_alp_id)) {
            $solicitud_id = $request->solic_alp_id;
        }

        if ($solicitud_id != null) {

            $solicitud = Solicitation::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );
        $this->bcScore = $this->configuracionProducto['bc_score'];
        $this->condicion = $this->configuracionProducto['condicion'];
        $this->microScore = $this->configuracionProducto['micro_score'];
        $this->edadMinima = $this->configuracionProducto['edad_minima'];
        $this->edadMaxima = $this->configuracionProducto['edad_maxima'];
        $this->campoCobertura = $this->configuracionProducto['campo_cobertura'];
        $this->storedProcedure = $this->configuracionProducto['stored_procedure'];
        $this->alp = $this->configuracionProducto['consulta_alp'];
        $this->ocupaciones = collect($this->configuracionProducto['ocupaciones'])->pluck('ocupacion');
        $this->solicitudRepository = new SolicitudRepository;
    }

    /**
    * Determina si la solicitud es Elegible o No
    *
    * @param  request $request Arreglo que contiene los datos capturados
    *
    * @return json             Resultado del guardado de datos
    */
    public function solicitudElegible(request $request)
    {
        $prospecto_id = $request->prospecto_id;
        $solicitud_id = $request->solicitud_id;
        // Obtiene la solicitud por id
        $solic = Solicitation::where('id', $solicitud_id)
        ->where('prospect_id', $prospecto_id)
        ->first();

        if ($solic) {

            //update the solics ult_punto_reg
            $solic->ult_punto_reg = self::setUltPuntoReg(
                $solic->ult_punto_reg,
                "Datos Elegible",
                1,
                "Los datos adicionales se han guardado con éxito"
            );
            //have not decrypted so its ok to save
            $solic->save();
            //decrypt the newly added solicitud values to keep working on it
            $solic = self::decryptSolicitud($solic);
            //now we will check the user's ocoupación
            $ocupacion = $solic->ocupacion;
            $ocupacionElegible = $this->ocupaciones->filter(function ($value, $key) use($ocupacion) {
                return mb_strtoupper($value) == mb_strtoupper($ocupacion);
            });

            // in_array($solic->ocupacion, ['Empleado Sector Público',mb_strtoupper('Empleado Sector Público'),'Empleado Sector Privado',mb_strtoupper('Empleado Sector Privado'),'Pensionado',mb_strtoupper('Pensionado'),'Jubilado',mb_strtoupper('Jubilado')])
            if (count($ocupacionElegible) == 1) {
                //is elegible
                $mnsj_str = [
                    'stat'      => 'Elegible',
                    'ocupacion' => mb_strtoupper($solic->ocupacion),
                    'message'   => 'El usuario es Elegible. La ocupación es: '. implode(' o ', $this->ocupaciones->toArray()),
                    'elegible'  => 1,
                ];

                $solic->ult_punto_reg = self::setUltPuntoReg(
                    $solic->ult_punto_reg,
                    "Check Ocupación",
                    1,
                    "El usuario es Elegible. La ocupación es: ". implode(' o ', $this->ocupaciones->toArray())
                );
                //update the solicitud status to Elegible
                $solic->type = "Elegible";

            } else {
                //is not elegible

                $mnsj_str = [
                    'stat'      => 'No Elegible',
                    'ocupacion' => mb_strtoupper($solic->ocupacion),
                    'message'   => 'El usuario NO es Elegible. La ocupación no es: '. implode(' o ', $this->ocupaciones->toArray()),
                    'elegible'  => 0,
                ];
                //update the solicitud status to No Elegible
                $solic->type = "No Elegible";
            }

            $solic->ult_punto_reg = self::setUltPuntoReg($solic->ult_punto_reg, "Check Ocupación", 0, "El usuario NO es Elegible. La ocupación no es: ". implode(' o ', $this->ocupaciones->toArray()));
            //save the message to the ult_mensj_a_usuario field
            $solic->ult_mensj_a_usuario = self::setUltMnsjUsr($solic->ult_mensj_a_usuario, json_encode($mnsj_str));
            //make sure your recrypt all solic values before saving
            $solic = self::recryptSolicitud($solic);
            $solic->save();

            return  response()->json($mnsj_str);
            // echo $mnsj_str;

        } else {
            echo '{
                "response" :  "error",
                "message" : "La solicitud no existe"
            }';
        }
    }

    /**
    * Obtiene el cuestionarioDinamico
    *
    * @param  Request $request Datos para armar el cuestionario dinámico
    *
    * @return json             Cuestionario dinámico
    */
    public function cuestionarioDinamico(Request $request) {

        $prospecto_id = $request->prospecto_id;
        $solicitud_id = $request->solicitud_id;

        // Obteniendo la respuesta de la máquina de riesgos
        $respuesta = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
        ->where('solicitud_id', $solicitud_id)
        ->get();

        // Obteniendo la plantilla de comunicación
        $plantilla = PlantillaComunicacion::select('modal_encabezado', 'modal_cuerpo', 'modal_img')
        ->where('plantilla_id', $respuesta[0]->plantilla_comunicacion)
        ->get()
        ->toArray();

        $plantilla = $plantilla[0];

        // Obteniendo las preguntas del cuestionario dinámico
        $pantallas = $respuesta[0]->situaciones;
        $pantallas = explode(',', $pantallas);
        $pantallas = array_filter($pantallas, 'strlen');

        $secciones = Situacion::whereIN('situacion', $pantallas)
        ->with('cuestionario')
        ->get()
        ->toArray();

        $datosSeccion = null;
        $model = null;
        foreach ($secciones as $seccion) {
            $preguntas = null;
            foreach ($seccion['cuestionario'] as $pregunta) {

                $visible = '';
                $model_id = $seccion['situacion'].'_'.$pregunta['id'];
                if ($pregunta['oculto'] == 1) {
                    $visible .= ' field_oculto';
                }

                if ($pregunta['tipo'] == 'text') {

                    $preguntas[] = [
                        'type'          => 'input',
                        'inputType'     => 'text',
                        'label'         => $pregunta['pregunta'],
                        'styleClasses'  => $model_id.' col-md-6 display-field'.$visible,
                        'model'         => $model_id,
                        'id'            => $model_id,
                    ];


                } else if ($pregunta['tipo'] == 'textarea') {

                    $preguntas[] = [
                        'type'          => 'textArea',
                        'label'         => $pregunta['pregunta'],
                        'styleClasses'  => $model_id.' col-md-6 display-field'.$visible,
                        'model'         => $model_id,
                        'id'            => $model_id,
                    ];


                } else if ($pregunta['tipo'] == 'select') {

                    $opciones = explode('|', $pregunta['opciones']);
                    $function = '';
                    if ($pregunta['depende'] != null) {

                        $pregunta_depende = explode('|', $pregunta['depende']);
                        $respuesta_depende = explode('|', $pregunta['respuesta_depende']);
                        foreach ($pregunta_depende as $key => $value) {
                            $condicion_muestra = "if (model.".$seccion['situacion'].'_'.$pregunta['id']." == '".$respuesta_depende[$key]."') {";
                            $muestra = "$('.".$value."').removeClass('field_oculto')}";
                            $condicion_oculta = "else{";
                                $oculta = "$('.".$value."').addClass('field_oculto'); model.".$value." = '' }";
                                $function .= $condicion_muestra.$muestra.$condicion_oculta.$oculta;
                        }
                    }

                    $preguntas[] = [
                        'type'                  => 'select',
                        'label'                 => $pregunta['pregunta'],
                        'styleClasses'          => $model_id.' col-md-6 display-field'.$visible,
                        'model'                 => $model_id,
                        'values'                => $opciones,
                        'id'                    => $model_id,
                        'onChanged'             => $function,
                        'selectOptions'         => [
                            'hideNoneSelectedText'  => true,
                        ]

                    ];

                }

                $model[$seccion['situacion'].'_'.$pregunta['id']] = '';

            }

                $datosSeccion[] = [
                    'legend' => $seccion['encabezado'].'. '.$seccion['introduccion'],
                    'styleClasses' => 'col-md-12',
                    'fields' => $preguntas
                ];

        }

        $formulario = [
            'formulario'  => $datosSeccion,
            'model'       => $model,
            'situaciones' => implode('|', $pantallas),
        ];

        return response()->json($formulario);

    }

    /**
    * Guarda el status de la oferta (Aceptada/Rechazada), si la oferta es Rechazada
    * tambien guarda el motivo de rechazo.
    *
    * @param  Request $request Datos capturados en el modal oferta
    *
    * @return json             Resultado del guardado del status
    */
    public function statusOferta(Request $request) {

        $status_oferta = $request->status_oferta;
        $eventTM = [];
        $prospecto = Prospecto::find($request->prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $request->prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        $ofertaPredominante = OfertaPredominante::where('prospecto_id', $request->prospecto_id)
            ->where('solicitud_id', $solicitud->id)
            ->first(); 

        $modeloEvaluacion = $request->modelo;
        $ofertaId = $request->oferta_id;

        Cookie::queue(Cookie::forget('producto'));
        Cookie::queue(Cookie::forget('logo'));
        Cookie::queue(Cookie::forget('checa_calificas'));

        $solicitud->status = 'Invitación a Continuar';
        $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos';
        $solicitud->save();

        // Actualizando el campo ult_punto_reg de la solicitud
        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            $request->status_oferta,
            1,
            'Se ha guardado el status de la solicitud con éxito',
            null
        );

        if ($status_oferta == 'Oferta Aceptada') {

            $resultado = $this->solicitudRepository->ofertaAceptada($prospecto, $solicitud, $ofertaPredominante, $modeloEvaluacion, $ofertaId);
            if ($resultado['success'] == true) {
                $eventTM[] = ['event'=> 'OfertaAceptada', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                if ($resultado['oferta_minima']) {
                    $eventTM[] = ['event'=> 'OfertaMinima', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                }
                $eventTM[] = ['event'=> 'OfertaRegular', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                $resultado['eventTM'] = $eventTM;
            }

            if ($resultado['carga_documentos'] == true) {
                $request->session()->put('isRedirect', true);
            }

        } else {

            $msj = 'Se ha guardado el motivo de recahzo de la solicitud con éxito';
            $motivo = $request->motivo_rechazo;
            $descripcion_otro = $request->descripcion_otro;
            $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                'prospecto_id' => $request->prospecto_id,
                'solicitud_id' => $request->solicitud_id
            ], [
                'status_oferta'     => $status_oferta,
                'elegida'           => $elegida,
                'motivo_rechazo'    => mb_strtoupper($motivo),
                'descripcion_otro'  => mb_strtoupper($descripcion_otro)
            ]);
        }

        return response()->json($resultado);

    }

    public function statusOfertaDoble(Request $request) {

        $prospecto = Prospecto::find($request->prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $request->prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        $status_oferta = $request->status_oferta;
        $eventTM = [];

        $modeloEvaluacion = $request->modelo;
        $ofertaId = $request->oferta_id;
        $tipoOferta = $request->tipo_oferta;

        $ofertaPredominante = OfertaPredominante::where('prospecto_id', $request->prospecto_id)
            ->where('solicitud_id', $solicitud->id)
            ->where('tipo_oferta', $request->tipo_oferta)
            ->first();

        Cookie::queue(Cookie::forget('producto'));
        Cookie::queue(Cookie::forget('logo'));
        Cookie::queue(Cookie::forget('checa_calificas'));

        $solicitud->status = 'Invitación a Continuar';
        $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos';
        $solicitud->save();

        // Actualizando el campo ult_punto_reg de la solicitud
        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            $request->status_oferta,
            1,
            'Se ha guardado el status de la solicitud con éxito',
            null
        );

        if ($status_oferta == 'Oferta Aceptada') {

            $resultado = $this->solicitudRepository->ofertaAceptada($prospecto, $solicitud, $ofertaPredominante, $modeloEvaluacion, $ofertaId, $tipoOferta);
            if ($resultado['success'] == true) {
                $eventTM[] = ['event'=> 'OfertaAceptada', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                if ($resultado['oferta_minima']) {
                    $eventTM[] = ['event'=> 'OfertaMinima', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                }
                $eventTM[] = ['event'=> 'OfertaRegular', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId, 'producto' => strtoupper($request->producto)];
                $resultado['eventTM'] = $eventTM;
            }
        }

        return response()->json($resultado);

    }

    public function rechazarOferta(Request $request) {

       
        $prospecto = Prospecto::find($request->prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $request->prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        $status_oferta = $request->status_oferta;

        $msj = 'Se ha guardado el motivo de recahzo de la solicitud con éxito';
        $motivo = $request->motivo_rechazo;
        $descripcion_otro = $request->descripcion_otro;
        $modelo = $request->modelo;

        OfertaPredominante::where('prospecto_id', $prospecto->id)
            ->where('solicitud_id', $solicitud->id)
            ->update([
                'status_oferta'     => $status_oferta,
                'elegida'           => 0,
                'motivo_rechazo'    => mb_strtoupper($motivo),
                'descripcion_otro'  => mb_strtoupper($descripcion_otro)
            ]);

        if ($modelo == 'Experian') {

            $resultado = RespuestaEvaluacionExperian::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->update([
                    'status_oferta'     => $status_oferta,
                    'elegida'           => 0,
                    'motivo_rechazo'    => mb_strtoupper($motivo),
                    'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                ]);

        } else {

            $resultado = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->update([
                    'status_oferta'     => $status_oferta,
                    'elegida'           => 0,
                    'motivo_rechazo'    => mb_strtoupper($motivo),
                    'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                ]);

        }

        Cookie::queue(Cookie::forget('producto'));
        Cookie::queue(Cookie::forget('logo'));
        Cookie::queue(Cookie::forget('checa_calificas'));

        $solicitud->status = 'Invitación a Continuar';
        $solicitud->sub_status = $request->status_oferta;
        $solicitud->save();

        // Actualizando el campo ult_punto_reg de la solicitud
        $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            $request->status_oferta,
            1,
            'Se ha guardado el status de la solicitud con éxito',
            null
        );

        return response()->json([
            'success' => true,
            'message' => 'Oferta rechazada'
        ]);
    }

    /**
    * Obtiene los datos de la oferta
    *
    * @param  Request $request Datos para obtener la oferta
    *
    * @return json             Obtiene la oferta
    */
    public function getDatosOferta(Request $request) {
        
        $prospecto_id = $request['prospecto_id'];
        $solicitud_id = $request['solicitud_id'];

        $resultadosMR = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->get();

        RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->update([
                'cuestionario_dinamico_guardado' => 1,
                'status_guardado'                => 'Cuestionario Dinámico Guardado'
            ]);

        // Si el guardado y validación fue exitoso se cambia actualiza el status
        // final de la solicitud a Invitación a Continuar y el ultimo punto
        // de registro a Cuestionario Completo
        $solicitud = Solicitud::find($solicitud_id);
        $solicitud->status = 'Invitación a Continuar';
        $solicitud->sub_status = 'Invitacion a Continuar';

        // Actualizando el utlimo punto de registro de la solicitud
        /* $this->solicitudRepository->setUltPuntoReg(
            $solicitud->id,
            $solicitud->status,
            'Cuestionario Completo',
            1,
            'El cuestionario dinámico se guardo con éxito'
        ); */
        $solicitud->save();

        $motivos = MotivoRechazo::pluck('motivo');

        foreach ($resultadosMR as $key => $resultado) {

            $tipo_oferta = ($resultado->tipo_oferta == null) ? 'oferta_normal' : $resultado->tipo_oferta;
            $tipo_poblacion = $resultado->tipo_poblacion;

            $datosOferta[$key] = [
                'monto'         => $resultado->monto,
                'plazo'         => $resultado->plazo,
                'tasa'          => $resultado->tasa,
                'pago_estimado' => $resultado->pago,
                'tipo_oferta'   => $resultado->tipo_oferta,
                'modelo'        => 'Prestanómico',
                'oferta_id'     => $resultado->id,
            ];
        }


        if ($tipo_poblacion == 'oferta_doble') {

            return response()->json([
                'success'           => true,
                'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                'prospecto_id'      => $prospecto_id,
                'solicitud_id'      => $solicitud_id,
                'tipo_poblacion'    => $tipo_poblacion,
                'modal'             => view('modals.modalDobleOferta', ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
            ]);

        } else if($tipo_poblacion == 'oferta_normal' || $tipo_poblacion == 'oferta_incrementada'){

            return response()->json([
                'success'           => true,
                'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                'prospecto_id'      => $prospecto_id,
                'solicitud_id'      => $solicitud_id,
                'tipo_poblacion'    => $tipo_poblacion,
                'modal'             => view('modals.modalOferta', ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
            ]);

        }
    }

    function datosSolicitud(Request $request) {

        return response()->json([
            'id_solicitud'  =>  $request->id_solicitud,
            'nombre'        => 'Prueba'
        ]);

    }

    /**
     * Guarda las respuestas del cuestionario dinámico
     *
     * @param  request $request Datos capturados en el cuestionario dinámico
     *
     * @return json             Resultado del guardado del cuestionario dinámico
     */
    public function saveCuestionario(request $request)
    {
        $prospecto = Prospecto::find($request->prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $request->prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        $solicitud_id = $solicitud->id;

        $situaciones = null;
        // Obteniendo las situaciones que incluye el cuestionario dinámico
        if ($request->situaciones != '') {
            $situaciones = $request->situaciones;
            $situaciones = explode('|', $situaciones);
            $situaciones = array_filter($situaciones, 'strlen');
        }

        // Realizando la validación de los datos
        $reglas = $this->reglasValidacion($situaciones);
        $mensajes = $this->mensajesValidacion();
        $respuestas = $request->except(['situaciones', 'prospecto_id', 'solicitud_id', 'tipo_poblacion']);

        $validacion = Validator::make($respuestas, $reglas, $mensajes)
            ->validate();

        // Guardado de respuestas
        $situaciones = Situacion::pluck('id','situacion')->toArray();
        $guardaRespuestas = $this->guardaRespuestas($respuestas, $situaciones, $request->prospecto_id, $solicitud_id);

        $modelo = $request->modelo;

        if ($guardaRespuestas['success'] == true) {

            $ofertaPredominante = OfertaPredominante::where('prospecto_id', $request->prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->get();

            OfertaPredominante::where('prospecto_id', $request->prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->update([
                'cuestionario_dinamico_guardado' => 1,
                'status_guardado'                => 'Cuestionario Dinámico Guardado'
            ]);
        
            if ($ofertaPredominante[0]['decision'] == 'Aprobado Ambos Modelos') {

                RespuestaMaquinaRiesgo::where('prospecto_id', $request->prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'cuestionario_dinamico_guardado' => 1,
                        'status_guardado'                => 'Cuestionario Dinámico Guardado'
                    ]);

                RespuestaEvaluacionExperian::where('prospecto_id', $request->prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'cuestionario_dinamico_guardado' => 1,
                        'status_guardado'                => 'Cuestionario Dinámico Guardado'
                    ]);

            } else {

                if ($modelo == 'Prestanómico') {

                    RespuestaMaquinaRiesgo::where('prospecto_id', $request->prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->update([
                            'cuestionario_dinamico_guardado' => 1,
                            'status_guardado'                => 'Cuestionario Dinámico Guardado'
                        ]);

                } else {

                    RespuestaEvaluacionExperian::where('prospecto_id', $request->prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->update([
                            'cuestionario_dinamico_guardado' => 1,
                            'status_guardado'                => 'Cuestionario Dinámico Guardado'
                        ]);

                }

            }

            // Si el guardado y validación fue exitoso se cambia actualiza el status
            // final de la solicitud a Invitación a Continuar y el ultimo punto
            // de registro a Cuestionario Completo
            $solicitud = Solicitud::find($solicitud_id);
            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = 'Cuestionario Completo';

            // Actualizando el utlimo punto de registro de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Cuestionario Completo',
                1,
                'El cuestionario dinámico se guardo con éxito'
            );
            $solicitud->save();

            $motivos = MotivoRechazo::pluck('motivo');

            foreach ($ofertaPredominante as $key => $resultado) {

                $tipo_oferta = ($resultado->tipo_oferta == null) ? 'oferta_normal' : $resultado->tipo_oferta;
                $tipo_poblacion = $resultado->tipo_poblacion;

                $datosOferta[$key] = [
                    'monto'         => $resultado->monto,
                    'plazo'         => $resultado->plazo,
                    'tasa'          => $resultado->tasa,
                    'pago_estimado' => $resultado->pago,
                    'tipo_oferta'   => $resultado->tipo_oferta,
                    'modelo'        => $resultado->modeloEvaluacion,
                    'oferta_id'     => $resultado->oferta_id
                ];
            }


            if ($tipo_poblacion == 'oferta_doble') {

                return response()->json([
                    'success'           => true,
                    'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                    'prospecto_id'      => $request->prospecto_id,
                    'solicitud_id'      => $solicitud_id,
                    'tipo_poblacion'    => $tipo_poblacion,
                    'modal'             => view('modals.modalDobleOferta', ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
                ]);

            } else if($tipo_poblacion == 'oferta_normal' || $tipo_poblacion == 'oferta_incrementada'){

                return response()->json([
                    'success'           => true,
                    'message'           => 'Las respuestas del cuestionario dinámico se guardaron con éxito',
                    'prospecto_id'      => $request->prospecto_id,
                    'solicitud_id'      => $solicitud_id,
                    'tipo_poblacion'    => $tipo_poblacion,
                    'modal'             => view('modals.modalOferta', ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render()
                ]);

            }

        } else {

            $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                'prospecto_id'  => $request->prospecto_id,
                'solicitud_id'  => $solicitud_id
            ], [
                'cuestionario_dinamico_guardado'    => 1,
                'status_guardado'                   => $guardaRespuestas['message'],
            ]);

            return response()->json([
                'success' => false,
                'message' => $guardaRespuestas['message'],
            ]);
        }

    }

    public function reglasValidacion($situaciones)
    {
        $reglas = null;
        // Sección: ATRASO EN CUENTA REPORTADO EN BURO
        if (in_array('A', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿qué fue lo que pasó con
            //  esa(s) cuenta(s)?
            $reglas ['A_1']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Llegaste a un acuerdo con esa
            // institución?
            $reglas ['A_2']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Tienes algún documento que demuestre
            // la liquidación de ese adeudo o bien el acuerdo con la institución?
            $reglas ['A_3']  = [
                'required_if:A_2,SI'
            ];
            // Validacion para la pregunta: ¿Cuál documento?
            $reglas ['A_4']  = [
                'required_if:A_3,SI',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
        }
        // Sección: ACLARACION SOBRE INGRESO Y/O GASTO FAMILIAR
        if (in_array('B', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['B_5']  = [
                'required',
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['B_6']  = [
                'required_if:B_5,SI',
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['B_7']  = [
                'required_if:B_5,SI',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: Del total de gastos familiares que
            // nos indicas en tu solicitud, ¿cuánto es el gasto que te corresponde
            // cubrir a ti?
            $reglas ['B_8']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:50'
            ];
        }

        // Sección: ACLARACION SOBRE DEUDA RECIENTE
        if (in_array('C', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿cuál ha sido el propósito
            // de los créditos que has adquirido recientemente?
            $reglas ['C_9']  = [
                'required',
                'min:10',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Has tenido algún cambio importante
            // en tu nivel o fuente de ingreso?
            $reglas ['C_10']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
            // Validacion para la pregunta: ¿Has tenido algún cambio importante
            // en tu nivel de gastos?
            $reglas ['C_11']  = [
                'required',
                'alpha_numeric_spaces_not_html',
                'max:255'
            ];
        }

        // Sección: PAGOS A PRESTAMOS ACTUALES
        if (in_array('D', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['D_12']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['D_13']  = [
                'required_if:D_12,SI',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['D_14']  = [
                'required_if:D_12,SI',
                'alpha_numeric_spaces_not_html',
                'max:50'
            ];
            // Validacion para la pregunta: ¿Alguno de los préstamos a tu nombre
            // es pagado parcial o totalmente por otra persona?
            $reglas ['D_15']  = [
                'required',
            ];
            // Validacion para la pregunta: Cuéntanos más sobre ese préstamo y
            // quién lo paga
            $reglas ['D_16']  = [
                'required_if:D_15,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: FUENTE ACTUAL DE INGRESOS
        if (in_array('E', $situaciones)) {
            // Validacion para la pregunta: Coméntanos por favor algo sobre tu
            // ocupación y cuál es tu fuente de ingresos
            $reglas ['E_17']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];

        }

        // Sección: PROPOSITO DEL PRESTAMO
        if (in_array('F', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿para qué vas a usar el
            // dinero si tu préstamo es aprobado?
            $reglas ['F_18']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: NOS GUSTARIA ENTENDER TU NEGOCIO
        if (in_array('G', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿cuál es tu negocio?
            $reglas ['G_19']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Desde cuándo lo tienes? ¿O cuándo
            // lo vas a iniciar?
            $reglas ['G_20']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Tienes un local fijo?
            $reglas ['G_21']  = [
                'required',
                'min:2',
                'max:25',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta:¿Para qué vas a utilizar los recursos
            // del préstamo?
            $reglas ['G_22']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: TELEFONO FIJO
        if (in_array('H', $situaciones)) {
            // Validacion para la pregunta: Por favor indícanos un teléfono fijo
            // en donde podamos localizarte si es necesario (puede ser de tu
            // trabajo o un familiar cercano)
            $reglas ['H_23']  = [
                'required',
                'digits:10',
            ];
        }

        // Sección:
        if (in_array('BD', $situaciones)) {
            // Validacion para la pregunta: Cuéntanos, ¿tienes algún ingreso
            // personal o familiar mayor que el que indicaste en tu solicitud?
            $reglas ['BD_24']  = [
                'required'
            ];
            // Validacion para la pregunta: ¿Cuál es la fuente de ese ingreso?
            $reglas ['BD_25']  = [
                'required_if:BD_24,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿De cuánto es ese ingreso?
            $reglas ['BD_26']  = [
                'required_if:BD_24,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: Del total de gastos familiares que
            // nos indicas en tu solicitud, ¿cuánto es el gasto que te
            // corresponde cubrir a ti?
            $reglas ['BD_27']  = [
                'required',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
            // Validacion para la pregunta: ¿Alguno de los préstamos a tu nombre
            // es pagado parcial o totalmente por otra persona?
            $reglas ['BD_28']  = [
                'required',
            ];
            // Validacion para la pregunta: Cuéntanos más sobre ese préstamo y
            // quién lo paga
            $reglas ['BD_29']  = [
                'required_if:BD_28,SI',
                'min:10',
                'max:255',
                'alpha_numeric_spaces_not_html',
            ];
        }

        // Sección: REFERENCIAS LABORALES
        if (in_array('I', $situaciones)) {

            $reglas ['I_30']  = [
                'alpha_numeric_spaces_not_html',
                'min:10',
                'max:255'
            ];
            $reglas ['I_31']  = [
                'digits:10',
            ];
        }

        return $reglas;
    }

    /**
     * Establece los mensajes para cada situación de las validaciones
     *
     * @return array Arreglo con los mensajes
     */
    public function mensajesValidacion()
    {
        return [
            'required'                       => '* El campo es requerido',
            'required_if'                    => '* El campo es requerido',
            'min'                            => '* Al menos :min caracteres',
            'max'                            => '* Menos de :max caracteres',
            'digits'                         => '* Deben ser 10 dígitos',
            'alpha_numeric_spaces_not_html'  => '* El campo solo permite letras, números y espacios'
        ];
    }

    public function guardaRespuestas($respuestas, $situaciones, $prospecto_id, $solicitud_id)
    {
        try {

            foreach ($respuestas as $key => $respuesta) {

                $ids = explode('_', $key);

                if (count($ids) == 2) {

                    $situacion = $ids[0];
                    $pregunta_id = $ids[1];
                    $situdacion_id = $situaciones[$situacion];

                    $res = RespuestaCuestionarioDinamico::updateorCreate([
                        'prospecto_id'  => $prospecto_id,
                        'solicitud_id'  => $solicitud_id,
                        'situacion_id'  => $situdacion_id,
                        'pregunta_id'   => $pregunta_id
                    ], [
                        'respuesta'     => mb_strtoupper($respuesta)
                    ]);

                    if ($situdacion_id == 8) {

                        $solicitud = Solicitud::find($solicitud_id);

                        if ($solicitud->exists()) {
                            $solicitud->telefono_casa = $respuesta;
                            $solicitud->save();
                        }


                    }

                }

            }

            return [
                'success' => true,
            ];

        } catch (\Exception $e) {

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];

        }
    }
}
