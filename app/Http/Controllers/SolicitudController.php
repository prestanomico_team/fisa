<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Prospecto;
use App\Solicitud;
use App\MockPerson;
use App\FicoUtil;
use App\PlazaCobertura;
use App\SoapHttpClient;
use App\Sepomex;
use App\DomicilioSolicitud;
use App\ConsultaBcTest;
use App\DetalleCuenta;
use App\DetalleConsulta;
use App\DetalleSolicitud;
use App\PlantillaComunicacion;
use App\Situacion;
use App\RespuestaMaquinaRiesgo;
use App\RespuestaEvaluacionExperian;
use App\OfertaPredominante;
use App\CatalogoSepomex;
use App\Plazo;
use Cookie;
use App\Repositories\SolicitudRepository;

use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Encryption\DecryptException;

class SolicitudController extends Controller
{
    private $configuracionProducto;
    private $bcScore;
    private $edadMinima;
    private $edadMaxima;
    private $campoCobertura;
    private $alp;
    private $microScore;
    private $solicitudRepository;

    /**
     * Constructor de la clase
     */
    public function __construct(request $request)
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        if (isset($request->solic_bc_id)) {
            $solicitud_id = $request->solic_bc_id;
        } elseif (isset($request->solic_hawk_id)) {
            $solicitud_id = $request->solic_hawk_id;
        } elseif (isset($request->solic_alp_id)) {
            $solicitud_id = $request->solic_alp_id;
        }

        if ($solicitud_id != null) {

            $solicitud = Solicitud::with('producto')
                ->where('id', $solicitud_id)
                ->get()
                ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );
        $this->bcScore = $this->configuracionProducto['bc_score'];
        $this->condicion = $this->configuracionProducto['condicion'];
        $this->microScore = $this->configuracionProducto['micro_score'];
        $this->edadMinima = $this->configuracionProducto['edad_minima'];
        $this->edadMaxima = $this->configuracionProducto['edad_maxima'];
        $this->campoCobertura = $this->configuracionProducto['campo_cobertura'];
        $this->storedProcedure = $this->configuracionProducto['stored_procedure'];
        $this->alp = $this->configuracionProducto['consulta_alp'];
        $this->solicitudRepository = new SolicitudRepository;
    }

    /**
     * Guarda el status de la oferta (Aceptada/Rechazada), si la oferta es Rechazada
     * tambien guarda el motivo de rechazo.
     *
     * @param  Request $request Datos capturados en el modal oferta
     *
     * @return json             Resultado del guardado del status
     */
     public function statusOferta(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            $status_oferta = $request->status_oferta;
            $eventTM = [];

            $ofertaPredominante = OfertaPredominante::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->first();

            Cookie::queue(Cookie::forget('producto'));
            Cookie::queue(Cookie::forget('logo'));
            Cookie::queue(Cookie::forget('checa_calificas'));

            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = $request->status_oferta;
            $solicitud->save();

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                $request->status_oferta,
                1,
                'Se ha guardado el status de la solicitud con éxito',
                null
            );

            $modeloEvaluacion = $request->modelo;
            $ofertaId = $request->oferta_id;

            if ($status_oferta == 'Oferta Aceptada') {

                $evento = 'OfertaAceptada';
                if ($solicitud->producto()->exists()) {
                    $campaña = $solicitud->producto[0]['pivot']['lead'];
                    $evento = $this->solicitudRepository->getEvento($campaña, 'OfertaAceptada');
                }

                $resultado = $this->solicitudRepository->ofertaAceptada($prospecto, $solicitud, $ofertaPredominante, $modeloEvaluacion, $ofertaId);
                if ($resultado['success'] == true) {
                    $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    if ($resultado['oferta_minima']) {
                        $eventTM[] = ['event'=> 'OfertaMinima', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    }
                    $eventTM[] = ['event'=> 'OfertaRegular', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    $resultado['eventTM'] = $eventTM;
                }

                if ($resultado['carga_documentos'] == true) {
                    $request->session()->put('isRedirected', true);
                }

            }

            return response()->json($resultado);

        } else {

            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);

        }

    }

    /**
     * Guarda el status de la oferta doble (Aceptada/Rechazada), si la oferta es Rechazada
     * tambien guarda el motivo de rechazo.
     *
     * @param  Request $request Datos capturados en el modal oferta
     *
     * @return json             Resultado del guardado del status
     */
     public function statusOfertaDoble(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            $status_oferta = $request->status_oferta;
            $eventTM = [];

            $modeloEvaluacion = $request->modelo;
            $ofertaId = $request->oferta_id;
            $tipoOferta = $request->tipo_oferta;

            $ofertaPredominante = OfertaPredominante::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->where('tipo_oferta', $tipoOferta)
                ->where('oferta_id', $ofertaId)
                ->first();

            Cookie::queue(Cookie::forget('producto'));
            Cookie::queue(Cookie::forget('logo'));
            Cookie::queue(Cookie::forget('checa_calificas'));

            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = $request->status_oferta;
            $solicitud->save();

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                $request->status_oferta,
                1,
                'Se ha guardado el status de la solicitud con éxito',
                null
            );

            if ($status_oferta == 'Oferta Aceptada') {

                $resultado = $this->solicitudRepository->ofertaAceptada($prospecto, $solicitud, $ofertaPredominante, $modeloEvaluacion, $ofertaId, $tipoOferta);

                if ($resultado['success'] == true) {

                    $evento = 'OfertaAceptada';
                    if ($solicitud->producto()->exists()) {
                        $campaña = $solicitud->producto[0]['pivot']['lead'];
                        $evento = $this->solicitudRepository->getEvento($campaña, 'OfertaAceptada');
                    }

                    $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    if ($resultado['oferta_minima']) {
                        $eventTM[] = ['event'=> 'OfertaMinima', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    }
                    $eventTM[] = ['event'=> 'OfertaRegular', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    $resultado['eventTM'] = $eventTM;
                }

                if ($ofertaPredominante == true) {
                    $request->session()->put('isRedirected', true);
                }

            }

            return response()->json($resultado);

        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);
        }

    }

     public function rechazarOferta(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $status_oferta = 'Oferta Rechazada';

            $msj = 'Se ha guardado el motivo de recahzo de la solicitud con éxito';
            $motivo = $request->motivo_rechazo;
            $descripcion_otro = $request->descripcion_otro;
            $modelo = $request->modelo;

            OfertaPredominante::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->update([
                    'status_oferta'     => $status_oferta,
                    'elegida'           => 0,
                    'motivo_rechazo'    => mb_strtoupper($motivo),
                    'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                ]);

            if ($modelo == 'Experian') {

                $resultado = RespuestaEvaluacionExperian::where('prospecto_id', $prospecto->id)
                    ->where('solicitud_id', $solicitud->id)
                    ->update([
                        'status_oferta'     => $status_oferta,
                        'elegida'           => 0,
                        'motivo_rechazo'    => mb_strtoupper($motivo),
                        'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                    ]);

            } else {

                $resultado = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                    ->where('solicitud_id', $solicitud->id)
                    ->update([
                        'status_oferta'     => $status_oferta,
                        'elegida'           => 0,
                        'motivo_rechazo'    => mb_strtoupper($motivo),
                        'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                    ]);

            }

            Cookie::queue(Cookie::forget('producto'));
            Cookie::queue(Cookie::forget('logo'));
            Cookie::queue(Cookie::forget('checa_calificas'));

            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = $request->status_oferta;
            $solicitud->save();

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                $request->status_oferta,
                1,
                'Se ha guardado el status de la solicitud con éxito',
                null
            );

            return response()->json([
                'success' => true,
                'message' => 'Oferta rechazada'
            ]);

        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);
        }

    }

    /**
     * Busca el código postal enviado y regresa la informacion de colonias
     * estado y municipio
     *
     * @param  request $request Parametros enviados
     * @param  string  $cp      Código postal
     *
     * @return json             Informacion de colonias, estado y municipio
     */
    public function apiGeoCP(request $request, $cp)
    {
        if (!is_numeric($cp)) {
            return response()->json([
                'response' => 'error',
                'message'  => 'CP debe ser numérico'
            ]);
        }
        if (strlen($cp) != 5) {
            return response()->json([
                'response' => 'error',
                'message'  => 'CP debe ser 5 números'
            ]);
        }

        $cp_data = CatalogoSepomex::where('codigo', $cp)->get()->toArray();

        if (count($cp_data) > 0) {
            $colonias = [];
            $ciudad = "";
            $id_ciudad = "";
            foreach ($cp_data as $data) {

                array_push(
                    $colonias,
                    [
                        'colonia' => addslashes($data['colonia_asentamiento']),
                        'id' => intval($data['id_colonia'])
                    ]
                );
                if ($data['ciudad'] != "") {
                    $ciudad = $data['ciudad'];
                    $id_ciudad = $data['id_ciudad'];
                }
            }
            // Si el id del estado es 09 (Ciudad de México), el Id de municipio
            // es igual al id de la ciudad y el id de la ciudad es 01
            $id_estado = $cp_data[0]['id_estado'];
            if ($id_estado == '09') {
                $id_municipio = '0'.$id_ciudad;
                $id_ciudad = '01';
            } else {
                $id_municipio = $cp_data[0]['id_municipio'];
            }

            $id_colonia = $id_estado.$id_municipio.$cp;

            return response()->json([
                'success'        => true,
                'cp'             => $cp_data[0]['codigo'],
                'colonias'       => $colonias,
                'deleg_munic'    => addslashes($cp_data[0]['municipio']),
                'id_deleg_munic' => $id_municipio,
                'ciudad'         => addslashes($ciudad),
                'id_ciudad'      => $id_ciudad,
                'estado'         => addslashes($cp_data[0]['estado']),
                'codigo_estado'  => addslashes($cp_data[0]['codigo_estado']),
                'id_estado'      => $id_estado,
                'id_colonia'     => $id_colonia,
                'cobertura'      => $cp_data[0][$this->campoCobertura],
            ]);
        } else {
            return response()->json([
                'response' => 'Not Found',
                'cp'       => $cp
            ]);
        }
    }

    /**
     * Obtiene la edad del prospecto en base a su fecha de nacimiento
     *
     * @param  string $año Año de la fecha de nacimiento
     * @param  string $mes Mes de la fecha de nacimiento
     * @param  string $dia Día de la fecha de nacimiento
     *
     * @return integer     Edad del prospecto
     */
    private function apiGetAge($año, $mes, $dia)
    {
        $age = Carbon::createFromDate($año, $mes, $dia)->age;
        return $age;
    }

    /**
     * Obtiene la lista de municipios por estado
     *
     * @param  request $request Arreglo que contiene los parametros que se envian
     * @param  string  $estado  Código del estado del que se quieren obtener los municipios
     *
     * @return json             Lista de municipio
     */
    public function apiCiudades(request $request, $estado)
    {
        // Obteniendo los municipios del estado

        $municipios = Sepomex::selectRAW('distinct(municipio)')
            ->where('estado', $estado)
            ->orderBy('municipio')
            ->pluck('municipio');

        $no_municipios = count($municipios);

        if ($no_municipios > 0) {
            return response()->json([
                'success'   => true,
                'count'      => $no_municipios,
                'municipios' => $municipios
            ]);
        } else {
            return response()->json([
                'success'   => false,
                'message'    => 'Estado no encontrado'
            ]);
        }
    }

    private function ageAndStateCheck($age, $BCresponse_arr, $domicilio)
    {
        $check_res = false;//default
        $failed_reason = '';//default
        $estado_matched = false;
        $ciudad_matched = false;
        // get all plaza_cobertura rows
        $plazas = PlazaCobertura::all();
        //check if age is in accepted range
        if (($age >= $this->edadMinima && $age <= $this->edadMaxima) && $domicilio->cobertura == 1) {
            $check_res = true;
        } elseif ($age < $this->edadMinima || $age > $this->edadMaxima) {
            $check_res = false;
            $failed_reason = "Edad";
        } elseif ($domicilio->cobertura == 0) {
            $check_res = false;
            $failed_reason = "Plaza";
        }
        return [$check_res, $failed_reason];
    }
}
