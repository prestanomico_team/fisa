<?php
/**
* T24Controller.php
*
* PHP version 7
*
* @category Controllers
* @package  App\Http\Controllers
* @author   Héctor Urbano Maza <hector.urbano@prestanomico.com>
* @license  https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio/licence.txt Licence
* @link     https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio
*/
namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\SoapWrapper;
use stdClass;
use Illuminate\Http\Request;
use Excel;
use App\ClientesAlta;
use App\Prospect;
use App\Solicitation;
use App\Jobs\AltaCliente;
use App\Jobs\AltaSolicitud;
use App\Jobs\LigueUsuario;
use DB;
use App\StatusT24;
use Carbon\Carbon;
use App\Holiday;
use App;

/**
* T24Controller Se encarga de realizar los proceso para el alta de cliente y solicitud
* en T24 asi como el ligue de usuarios en LDAP
*
* @category Controllers
* @package  App\Http\Controllers
* @author   Héctor Urbano Maza <hector.urbano@prestanomico.com>
* @license  https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio/licence.txt Licence
* @link     https://bitbucket.org/sistemas_prestanomico/prestanomico-sitio
*/
class T24Controller extends Controller
{
    protected $soapWrapper;

    // Contiene la relación del nombre de los campos en la tabla mySQL con el del
    // CSV
    private $_columnasCustomerLoan = [
        'prospecto_id'                      => 'prospecto_id',
        'solicitud_id'                      => 'solicitud_id',
        'fecha_de_apertura'                 => 'fecha_alta',
        'simplificado'                      => 'simplificado',
        'apellido_paterno'                  => 'SHORTNAME',
        'apellido_materno'                  => 'NAME1',
        'primer_nombre'                     => 'NAME2',
        'fecha_de_nacimiento'               => 'FECNACIMIENTO',
        'sexo'                              => 'GENDERNC',
        'estado_civil'                      => 'MARITALSTSNC',
        'calle'                             => 'STREET',
        'numero_exterior'                   => 'DIRNUMEXT',
        'estado'                            => 'DIRCDEDO',
        'delegacionmunicipio'               => 'DIRDELMUNI',
        'colonia'                           => 'DIRCOLONIA',
        'pais'                              => 'DIRPAIS',
        'telefono_domicilio'                => 'TELDOM',
        'telefono_oficina'                  => 'TELOFI',
        'telefono_celular'                  => 'TELCEL',
        'correo_electronico'                => 'EMAIL',
        'rfchomoclave'                      => [
                0 => 'MNEMONIC',
                1 => 'RFCCTE'
        ],
        'segundo_nombre'                    => 'FORMERNAME',
        'lugar_de_nacimiento'               => 'LUGNAC',
        'curp'                              => 'VALCURP',
        'nacionalidad'                      => 'NATIONALITY',
        'pais_nacimiento'                   => 'RESIDENCE',
        'numero_interior'                   => 'DIRNUMINT',
        'ciudad'                            => 'DIRCIUDAD',
        'codigo_postal'                     => 'DIRCODPOS',
        'estudios'                          => 'ESTUDIOS',
        'ocupacion'                         => 'OCUPACION',
        'ingreso_actual'                    => 'MAININCOME',
        'domicilio_anos'                    => 'DOMANOS',
        'egresos_mensuales'                 => 'EGRORDMEN',
        'tipo_de_residencia'                => 'TIPODOM',
        'numero_de_dependientes_economicos' => 'NOOFDEPEND',
        'monto'                             => 'LOANAMOUNT',
        'plazo'                             => 'TERM',
        'objetivo_del_prestamo'             => 'LOANPURPOSE',
        'antiguedad_empleo'                 => 'CUREMPMTEXPNCYR',
        'credito_hipotecario_si_no'         => 'HLDMORTGAGE',
        'credito_auto_si_no'                => 'HLDAUTOLOAN',
        'tarjeta_de_credito_si_no'          => 'HLDTDC',
        'cuatro_ultimos_digitos_de_tdc'     => 'TDCCODE',
        'origen'                            => 'INCSNDSOURCE',
        'tipo_tasa'                         => 'TIPOTASA',
    ];

    // Contiene la relación del nombre de los campos en la tabla mySQL con el del
    // arreglo con los datos para el alta automática
    public $_columnasCustomerLoanAlta = [
        'prospecto_id'                      => 'prospecto_id',
        'id'                                => 'solicitud_id',
        'fecha_alta'                        => 'fecha_alta',
        'simplificado'                      => 'simplificado',
        'apellido_paterno'                  => 'SHORTNAME',
        'apellido_materno'                  => 'NAME1',
        'nombres'                           => 'NAME2',
        'fecha_nacimiento'                  => 'FECNACIMIENTO',
        'sexo'                              => 'GENDERNC',
        'estado_civil'                      => 'MARITALSTSNC',
        'domicilio.calle'                   => 'STREET',
        'domicilio.num_exterior'            => 'DIRNUMEXT',
        'domicilio.num_interior'            => 'DIRNUMINT',
        'domicilio.id_estado'               => 'DIRCDEDO',
        'domicilio.id_delegacion'           => 'DIRDELMUNI',
        'domicilio.id_colonia'              => 'DIRCOLONIA',
        'domicilio.id_ciudad'               => 'DIRCIUDAD',
        'domicilio.cp'                      => 'DIRCODPOS',
        'pais'                              => 'DIRPAIS',
        'telefono_casa'                     => 'TELDOM',
        'telefono_empleo'                   => 'TELOFI',
        'celular'                           => 'TELCEL',
        'email'                             => 'EMAIL',
        'rfc'                               => [
                0 => 'MNEMONIC',
                1 => 'RFCCTE'
        ],
        'segundo_nombre'                    => 'FORMERNAME',
        'lugar_nacimiento_estado'           => 'LUGNAC',
        'curp'                              => 'VALCURP',
        'nacionalidad'                      => 'NATIONALITY',
        'pais_nacimiento'                   => 'RESIDENCE',
        'nivel_estudios'                    => 'ESTUDIOS',
        'ocupacion'                         => 'OCUPACION',
        'ingreso_mensual'                   => 'MAININCOME',
        'antiguedad_domicilio'              => 'DOMANOS',
        'gastos_familiares'                 => 'EGRORDMEN',
        'tipo_residencia'                   => 'TIPODOM',
        'numero_dependientes'               => 'NOOFDEPEND',
        'prestamo'                          => 'LOANAMOUNT',
        'plazo'                             => 'TERM',
        'finalidad'                         => 'LOANPURPOSE',
        'antiguedad_empleo'                 => 'CUREMPMTEXPNCYR',
        'credito_hipotecario'               => 'HLDMORTGAGE',
        'credito_automotriz'                => 'HLDAUTOLOAN',
        'credito_bancario'                  => 'HLDTDC',
        'ultimos_4_digitos'                 => 'TDCCODE',
        'referencia'                        => 'INCSNDSOURCE',
        'nombre_producto'                   => 'IDTYPE',
        'tipo_tasa'                         => 'TIPOTASA',
    ];

    // Contiene las reglas para procesar los datos al formato de T24
    private $_reglasCustomerLoan = [
        'LUGNAC' => [
            'funcion' => 'partir_buscar',
            'parametros' => ', |1' // Caracter por el que se hara explode | Valor en el arreglo que se regresara.
        ],
        'NAME2' => [
            'funcion' => 'partir_nombre',
            'parametros' => 'nombres'
        ],
        'GENDERNC' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'MARITALSTSNC' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'TIPODOM' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'OCUPACION' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'DIRCDEDO' => [
            'funcion' => 'buscar',
            'parametros' => 'direccion',
        ],
        'MNEMONIC' => [
            'funcion' => 'truncar',
            'parametros' => '10' // Número máximo de caracteres.
        ],
        'RFCCTE' => [
            'funcion' => 'borrar',
            'parametros' => '13' // Si la longitud del campo es igual al valor, se queda el valor.
        ],
        'LOANPURPOSE' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'HLDMORTGAGE' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'HLDAUTOLOAN' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'HLDTDC' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'TERM' => [
            'funcion' => 'tiempo_meses',
            'parametros' => null,
        ],
    ];

    private $_reglasCustomerLoanAlta = [
        'LUGNAC' => [
            'funcion' => 'buscar_estado',
            'parametros' => null
        ],
        'NAME2' => [
            'funcion' => 'partir_nombre',
            'parametros' => 'nombre'
        ],
        'GENDERNC' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'MARITALSTSNC' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'TIPODOM' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'OCUPACION' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'DIRCDEDO' => [
            'funcion' => 'ids_domicilio',
            'parametros' => null,
        ],
        'MNEMONIC' => [
            'funcion' => 'truncar',
            'parametros' => '10' // Número máximo de caracteres.
        ],
        'RFCCTE' => [
            'funcion' => 'borrar',
            'parametros' => '13' // Si la longitud del campo es igual al valor, se queda el valor.
        ],
        'LOANPURPOSE' => [
            'funcion' => 'buscar',
            'parametros' => 'catalogos_t24',
        ],
        'HLDMORTGAGE' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'HLDAUTOLOAN' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'HLDTDC' => [
            'funcion' => 'primera_mayuscula',
            'parametros' => null,
        ],
        'TERM' => [
            'funcion' => 'tiempo_meses',
            'parametros' => null,
        ],
    ];

    // Contiene la consulta que se ejecuta para la obtencion de datos de la
    // tabla clientes_alta
    private $sql_alta_cliente = "*,
        IF(aplica_cliente is null AND alta_cliente = 0 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is null, 'Alta de cliente en proceso',
        IF(aplica_cliente is null AND alta_cliente = 0 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is not null, 'Alta de cliente erronea',
        IF(aplica_cliente is null AND alta_cliente = 1, 'Alta de cliente exitosa',
        IF(aplica_cliente = 1 AND alta_cliente = 0 AND error is null, 'Alta de cliente en proceso',
        IF(aplica_cliente = 1 AND alta_cliente = 0 AND error is not null, 'Alta de cliente erronea',
        IF(aplica_cliente = 1 AND alta_cliente = 1 , 'Alta de cliente exitosa', 'NA'))))))
        as 'status_alta',
        IF(aplica_solicitud is null AND alta_cliente = 1 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is null, 'Alta de solicitud en proceso',
        IF(aplica_solicitud is null AND alta_cliente = 1 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is not null, 'Alta de solicitud erronea',
        IF(aplica_solicitud is null AND alta_solicitud = 1, 'Alta de solicitud exitosa',
        IF((aplica_cliente = 1 AND alta_cliente = 0) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud por procesar',
        IF((aplica_cliente = 1 AND alta_cliente = 1) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud en proceso',
        IF((aplica_cliente = 1 AND alta_cliente = 1) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is not null, 'Alta de solicitud erronea',
        IF((aplica_cliente = 1 AND alta_cliente = 0) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is not null, 'Alta de solicitud no procesada',
        IF((aplica_cliente = 1 AND alta_cliente = 1) AND aplica_solicitud = 1 AND alta_solicitud = 1, 'Alta de solicitud exitosa',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud en proceso',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Alta de solicitud erronea',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND (error is not null AND error = 'El cliente se registro con un correo diferente'), 'Alta de solicitud en proceso',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 1, 'Alta de solicitud exitosa', 'NA'))))))))))))
        as 'status_solicitud',
        IF(aplica_ligue is null AND alta_cliente = 1 AND alta_solicitud = 1 AND usuario_ligado = 0 AND error is null, 'Ligue de usuario en proceso',
        IF(aplica_ligue is null AND alta_cliente = 1 AND alta_solicitud = 1 AND usuario_ligado = 0 AND error is not null, 'Ligue de usuario erroneo',
        IF(aplica_ligue is null AND usuario_ligado = 1, 'Ligue de usuario exitoso',
        IF(aplica_ligue = 0, 'NA',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 0) AND usuario_ligado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Ligue de usuario por procesar',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Ligue de usuario en proceso',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Ligue de usuario erroneo',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 0) AND usuario_ligado = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Ligue de usuario no procesado',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 1, 'Ligue de usuario exitoso',
        IF(usuario_ligado = 1, 'Ligue de usuario exitoso', 'NA'))))))))))
        as 'status_ligue',
        IF(aplica_facematch = 0 AND facematch is null AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND documentos_facematch is null AND registro_facematch is null, 'Identificación/Selfie pendiente',
        IF(aplica_facematch = 0 AND facematch = 1 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND documentos_facematch = 1 AND registro_facematch = 1, 'Identificación/Selfie exitoso',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 0 OR registro_facematch = 0) AND facematch_error is not null, 'Identificación/Selfie erroneo',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 1 AND registro_facematch = 0) AND facematch_error is not null, 'Identificación/Selfie-Documentos exitoso',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 0 AND registro_facematch = 1) AND facematch_error is not null, 'Identificación/Selfie-Registro exitoso',
        IF(aplica_facematch = 1 AND facematch is null AND facematch_error is null AND alta_solicitud = 1, 'Facematch pendiente',
        IF(aplica_facematch = 1 AND facematch = 0 AND facematch_error is null, 'Facematch no procesado',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch is null AND registro_facematch is null AND facematch_error is not null, 'Facematch erroneo',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 1 AND registro_facematch = 1 AND facematch_error is not null, 'Facematch erroneo-Procesos exitos',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 0 AND registro_facematch = 1 AND facematch_error is not null, 'Facematch erroneo-Registro exitoso',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 1 AND registro_facematch = 0 AND facematch_error is not null, 'Facematch erroneo-Documentos exitoso',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 1 AND registro_facematch = 1, 'Facematch exitoso',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 1 AND registro_facematch = 0, 'Facematch exitoso-Error Registro',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 0 AND registro_facematch = 1, 'Facematch exitoso-Error Documentos', 'NA'))))))))))))))
        as 'fachematch',
        IF((aplica_ligue = 1 AND usuario_ligado = 0) AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email por procesar',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email en proceso',
        IF((aplica_ligue = 1 AND usuario_ligado = 0) AND aplica_email = 1 AND email_enviado is null AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Envio de email no procesado',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email erroneo',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 1 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email exitoso',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 1 AND (error is not null AND id_email is not null), 'Envio de email exitoso',
        IF(aplica_ligue = 0 AND alta_solicitud = 0 AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email por procesar',
        IF(aplica_ligue = 0 AND alta_solicitud = 1 AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email en proceso',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado is null AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Envio de email no procesado',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado = 0 AND error is not null, 'Envio de email erroneo',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado = 1 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email exitoso',
        IF(email_enviado = 1, 'Envio de email exitoso', 'NA'))))))))))))
        as 'status_email',
        IF ((aplica_referencias = 0 OR aplica_referencias is null) AND alta_referencias is null, 'NA',
        IF (aplica_referencias = 1 AND alta_referencias is null and referencias_error is null, 'Alta referencias pendiente',
        IF (aplica_referencias = 1 AND alta_referencias = 0 and referencias_error is not null, 'Alta referencias erronea',
        IF (aplica_referencias = 1 AND alta_referencias = 1, 'Alta referencias exitosa', 'NA'))))
        as 'status_referencias',
        IF ((aplica_cuenta_clabe = 0 OR aplica_cuenta_clabe is null) AND alta_cuenta_clabe is null, 'NA',
        IF (aplica_cuenta_clabe = 1 AND alta_cuenta_clabe is null and cuenta_clabe_error is null, 'Alta cuenta clabe pendiente',
        IF (aplica_cuenta_clabe = 1 AND alta_cuenta_clabe = 0 and cuenta_clabe_error is not null, 'Alta cuenta clabe erronea',
        IF (aplica_cuenta_clabe = 1 AND alta_cuenta_clabe = 1, 'Alta cuenta clabe exitosa', 'NA'))))
        as 'status_cuenta_clabe',
        IF ((aplica_comprobante_domicilio = 0 OR aplica_comprobante_domicilio is null) AND alta_comprobante_domicilio is null, 'NA',
        IF (aplica_comprobante_domicilio = 1 AND alta_comprobante_domicilio is null and comprobante_domicilio_error is null, 'Alta comprobante domicilio pendiente',
        IF (aplica_comprobante_domicilio = 1 AND alta_comprobante_domicilio = 0 and comprobante_domicilio_error is not null, 'Alta comprobante domicilio erronea',
        IF (aplica_comprobante_domicilio = 1 AND alta_comprobante_domicilio = 1, 'Alta comprobante domicilio exitosa', 'NA'))))
        as 'status_comprobante_domicilio',
        IF ((aplica_comprobante_ingresos = 0 OR aplica_comprobante_ingresos is null) AND alta_comprobante_ingresos is null, 'NA',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos is null and comprobante_ingresos_error is null, 'Alta comprobante ingresos pendiente',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos = 0 and comprobante_ingresos_error is not null, 'Alta comprobante ingresos erronea',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos = 1, 'Alta comprobante ingresos exitosa', 'NA'))))
        as 'status_comprobante_ingresos',
        IF ((aplica_certificados_deuda = 0 OR aplica_certificados_deuda is null) AND alta_certificados_deuda is null, 'NA',
        IF (aplica_certificados_deuda = 1 AND alta_certificados_deuda is null and certificados_deuda_error is null, 'Alta certificados deuda pendiente',
        IF (aplica_certificados_deuda = 1 AND alta_certificados_deuda = 0 and certificados_deuda_error is not null, 'Alta certificados deuda erronea',
        IF (aplica_certificados_deuda = 1 AND alta_certificados_deuda = 1, 'Alta certificados deuda exitosa', 'NA'))))
        as 'status_certificados_deuda'";

    /**
     * Constructor de la clase
     */
    public function __construct()
    {
        date_default_timezone_set('America/Mexico_City');
     }

    /**
     * Muestra la vista para el alta de clientes
     *
     * @param  Request $request Arreglo que contiene los datos que se envian a
     * la vista
     *
     * @return View            Vista para el alta de clientes
     */
    public function altaClientes(Request $request)
    {
        if (!isset($request['search'])) {
            $altas = ClientesAlta::selectRaw($this->sql_alta_cliente)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            return view('crm.alta_clientes')->with('altas', $altas);
        } else {
            $term = $request['search'];
            $altas = ClientesAlta::selectRaw($this->sql_alta_cliente)
                ->orHavingRaw('prospecto_id LIKE "%'.$term.'%"')
                ->orHavingRaw('solicitud_id LIKE "%'.$term.'%"')
                ->orHavingRaw('status_alta LIKE "%'.$term.'%"')
                ->orHavingRaw('status_solicitud LIKE "%'.$term.'%"')
                ->orHavingRaw('status_ligue LIKE "%'.$term.'%"')
                ->orHavingRaw('CONCAT(SHORTNAME," ",NAME1," ",NAME2) LIKE "%'.$term.'%"')
                ->orHavingRaw('EMAIL LIKE "%'.$term.'%"')
                ->orderBy('created_at', 'desc')
                ->simplePaginate(10);
            $altas->withPath('alta-clientes?search='.$term);

            return view('crm.alta_clientes')->with([
                'altas'=>$altas,
                'search'=>$term
            ]);
        }
    }

    /**
     * Guarda los datos que se mandan en el archivo CSV en la tabla alta clientes
     *
     * @param  Request $request Arreglo que contiene el archivo CSV enviado
     *
     * @return Json             Resultado de la carga del CSV
     */
    public function cargaLayout(Request $request)
    {
        $file = $request->file('file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $file = $file->move(storage_path().'/app/public', $filename);

            if ($file->getExtension() == 'csv') {
                $location = $file->getPathName();

                Excel::load($location, function ($reader) {
                    $headers = $reader->first()->keys()->toArray();
                    $results = $reader->formatDates(false)->toArray();

                    $clientesAlta = [];

                    foreach ($results as $result) {
                        $customerLoan = [];
                        foreach ($result as $key => $value) {

                            // Construyendo el arreglo CustomerInput (Cliente) y LoanApplication (Solicitud)
                            if (array_key_exists($key, $this->_columnasCustomerLoan) == true) {
                                $campo = $this->_columnasCustomerLoan[$key];

                                if (is_array($campo) == false) {
                                    $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                                } else {
                                    foreach ($campo as $nombre) {
                                        $customerLoan[$nombre] = mb_strtoupper(Self::limpiar_caracteres($value));
                                    }
                                }
                            }
                        }

                        $clientesAlta = $this->procesaDatos($customerLoan);

                        $existe = ClientesAlta::where('MNEMONIC', $clientesAlta['MNEMONIC'])
                            ->where('alta_cliente', 1)
                            ->get()
                            ->count();

                        if ($existe == 0) {
                            $clientesAlta = ClientesAlta::create($clientesAlta);
                            $job = (new AltaCliente($clientesAlta))->delay(30);
                            dispatch($job);
                        } else {
                            $clientesAlta['error'] = 'RFC del prospecto ya procesado';
                            $clientesAlta = ClientesAlta::create($clientesAlta);
                        }
                    }
                });

                return response()->json([
                    'success' => 'true'
                ]);
            } else {
                return response()->json([
                    'success' => 'false',
                    'msg' => 'Solo se permiten archivos con extensión .csv'
                ]);
            }
        } else {
            return response()->json([
                'success' => 'false',
                'msg' => 'No se selecciono ningún archivo'
            ]);
        }
    }

    public function arregloDatosAlta($datos) {
        $customerLoan = [];

        foreach ($datos as $key => $value) {
            // Construyendo el arreglo CustomerInput (Cliente) y LoanApplication (Solicitud)
            if (array_key_exists($key, $this->_columnasCustomerLoanAlta) == true) {
                $campo = $this->_columnasCustomerLoanAlta[$key];

                if (is_array($campo) == false) {

                    if ($campo == 'FECNACIMIENTO') {
                        $value = Carbon::createFromFormat('Y-m-d', $datos['fecha_nacimiento'])->format('Ymd');
                        $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                    } else {
                        $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                    }

                } else {

                    foreach ($campo as $nombre) {
                        $customerLoan[$nombre] = mb_strtoupper(Self::limpiar_caracteres($value));
                    }

                }
            }
        }

        $customerLoan['DIRPAIS'] = 'MX';

        return $customerLoan;
    }

    /**
     * Convierte los datos enviados en el arreglo de alta al formato necesario
     * por T24
     *
     * @param  array $customerLoan Contiene los datos que se envian en el arreglo
     *
     * @return array               Contiene el array con los datos en el formato
     * necesario para T24
     */
    public function procesaDatosAlta($customerLoan)
    {
        foreach ($customerLoan as $key => $valor) {
            if (array_key_exists($key, $this->_reglasCustomerLoanAlta)) {
                $reglas = $this->_reglasCustomerLoanAlta[$key];
                if ($reglas['parametros'] == 'nombre') {
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['NAME2'] = $valores['NAME2'];
                    $customerLoan['FORMERNAME'] = $valores['FORMERNAME'];
                } elseif ($reglas['funcion'] == 'ids_domicilio') {
                    $valor = $customerLoan;
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['DIRDELMUNI'] = $valores['DIRDELMUNI'];
                    $customerLoan['DIRCIUDAD'] = $valores['DIRCIUDAD'];
                }   else {
                    $nuevo_valor = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan[$key] = $nuevo_valor;
                }
            }
        }

        return $customerLoan;
    }


    /**
     * Convierte los datos enviados en el CSV al formato necesario por T24
     *
     * @param  array $customerLoan Contiene los datos que se envian en el CSV
     *
     * @return array               Contiene el array con los datos en el formato
     * necesario para T24
     */
    public function procesaDatos($customerLoan)
    {
        foreach ($customerLoan as $key => $valor) {
            if (array_key_exists($key, $this->_reglasCustomerLoan)) {
                $reglas = $this->_reglasCustomerLoan[$key];
                if ($reglas['parametros'] == 'direccion') {
                    $valor = $customerLoan;
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['DIRCODPOS'] = $valores['DIRCODPOS'];
                    $customerLoan['DIRCDEDO'] = $valores['DIRCDEDO'];
                    $customerLoan['DIRDELMUNI'] = $valores['DIRDELMUNI'];
                    $customerLoan['DIRCIUDAD'] = $valores['DIRCIUDAD'];
                    $customerLoan['DIRCOLONIA'] = $valores['DIRCOLONIA'];
                    //if ($valores['COLONIACORRECTA'] == 0) {
                    $customerLoan['error'] = $valores['MSGCOLONIA'];
                    //}
                } elseif ($reglas['parametros'] == 'nombre') {
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['NAME2'] = $valores['NAME2'];
                    $customerLoan['FORMERNAME'] = $valores['FORMERNAME'];
                } else {
                    $nuevo_valor = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan[$key] = $nuevo_valor;
                }
            }
        }

        return $customerLoan;
    }

    /**
     * Realiza el filtrado de los datos que se muestran en la lista de altas
     * cuando se realiza una búsqueda
     *
     * @param  Request $request Arreglo que contiene los parametros que se envían
     * para la búsqueda
     *
     * @return json             Resultado del filtrado
     */
    public function autoComplete(Request $request)
    {
        $term = $request->term;

        $results = array();

        $altas = ClientesAlta::selectRaw($this->sql_alta_cliente)
            ->orHavingRaw('prospecto_id LIKE "%'.$term.'%"')
            ->orHavingRaw('solicitud_id LIKE "%'.$term.'%"')
            ->orHavingRaw('status_alta LIKE "%'.$term.'%"')
            ->orHavingRaw('status_solicitud LIKE "%'.$term.'%"')
            ->orHavingRaw('status_ligue LIKE "%'.$term.'%"')
            ->orHavingRaw('CONCAT(SHORTNAME," ",NAME1," ",NAME2) LIKE "%'.$term.'%"')
            ->orHavingRaw('EMAIL LIKE "%'.$term.'%"')
            ->orderBy('created_at', 'desc');

        $altas_paginadas = $altas->simplePaginate(10);
        $altas = $altas->take(0)->get();

        $altas_paginadas->withPath('alta-clientes?search='.$term);

        foreach ($altas as $alta) {
            $results['content'][] = [
                'id' => $alta->id,
                'value' => $alta->SHORTNAME.' '.$alta->NAME1
            ];
        }

        $results['view'] =  view('crm.lista_clientes')
            ->with('altas', $altas_paginadas)
            ->render();

        return response()->json($results);
    }

    /**
     * Actualiza la lista de altas de clientes
     *
     * @return json Resultado de actualizar la lista
     */
    public function updateLista()
    {
        $results = array();
        $altas = ClientesAlta::selectRaw($this->sql_alta_cliente)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $altas->withPath('alta-clientes');

        $results['view'] =  view('crm.lista_clientes')
            ->with('altas', $altas)
            ->render();
        return response()->json($results);
    }

    /**
     * Establece la fecha a la que se encunetra T24
     */
    public function setDatesT24() {

        if (App::environment(['produccion'])) {

            $holidays = Holiday::select('fecha')->get()->toArray();
            $status_t24 = StatusT24::first();

            $holidays_dates = [];
            foreach ($holidays as $holiday) {
                $holidays_dates[] = strtotime($holiday['fecha']);
            }

            $dia = Carbon::today();

            if (in_array($dia->timestamp, $holidays_dates) || $dia->isWeekend() == true) {
                $status_t24->fecha_alta = 'fecha_t24';
                $status_t24->fecha_actual = date('Ymd');
                $status_t24->save();
            } else {
                $status_t24->fecha_alta = null;
                $status_t24->fecha_actual = date('Ymd');
                $status_t24->save();
            }

        }  else {

            echo 'Disponible solo en Producción';

        }

    }

    /**
     * Genera el job para procesamiento del alta de cliente en T24 mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function altaCliente_T24(Request $request) {

        self::altaClienteT24(null, null, $request->idAltaCliente, null, null);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del alta de solicitud en T24 mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function altaSolicitud_T24(Request $request) {

        self::altaSolicitudT24(null, null, $request->idAltaCliente, null, null);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del ligue de usuario en LDAP mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function ligarUsuarioLDAP(Request $request) {

        self::ligueUsuarioLDAP($request->idAltaCliente);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del envío del email y sms mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function enviarEmail(Request $request) {

        self::EnvioEmail($request->idAltaCliente);

        return response()->json(['success' => true]);

    }

}
