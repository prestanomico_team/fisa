<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;
use Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    /**
     * Muestra el listado de usuarios con sus roles asignados
     *
     * @return view Vista que contiene el listao de usaurios.
     */
    public function index() {

        $users = User::with('roles')->get();
        $is_admin = Auth::user()->roles()->where('name', 'administrador')->count();

        return view(
            'usuarios.index',
            [
                'usuarios'  => $users,
                'is_admin'  => $is_admin,
            ]
        );

    }

    /**
     * Muestra el formulario para editar los datos del usuario.
     *
     * @param  Integer $id Id del usuario
     *
     * @return view        Vista con el formulario para editar al usuario.
     */
    public function editar($id) {

        $usuario = User::with('roles')->where('id', $id)->get();

        try {

            if (Auth::user()->roles()->where('name', 'administrador')->count() == 1) {
                $roles = Role::all();
            } else {
                $roles = Role::where('area', $usuario[0]->area)->where('area', '!=', '')->get();
            }

            return view(
                'usuarios.editar',
                [
                    'usuario'  => $usuario,
                    'roles'    => $roles,
                ]
            );

        } catch (\Exception $e) {

        }

    }

    /**
     * Actualiza los datos del usuario.
     *
     * @param  Integer $id      Id del usuario a actualizar.
     * @param  Request $request Arreglo con los datos del usuario.
     *
     * @return json             Resultado de actualizar los datos del usuario.
     */
    public function actualizar($id, Request $request) {

        $usuario = User::findOrFail($id);
        $roles = $request->roles;

        try {

            if ($roles != 'null') {
                $roles = explode(',', $roles);
                $usuario->roles()->sync($roles);
            }

            return response()->json(['success' => true]);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);

        }

    }

    /**
     * Crea un nuevo usuario.
     *
     * @param  Request $request Arreglo con los datos del usuario.
     *
     * @return json             Resultado de crear el usuario.
     */
    public function save(Request $request) {

        try {

            $messages = [
                'required'              => '* El campo es requerido.',
                'alpha_numeric_spaces'  => '* El campo solo puede contener caracteres alfanuméricos.',
                'max'                   => '* El campo no puede contener mas de :max caracteres.',
                'min'                   => '* El campo debe contener mínimo :min caracteres.',
                'unique'                => '* El usuario ya existe',
                'email'                 => '* El campo debe contener un email válido.',
                'same'                  => '* El campo contraseña y confirmar contraseña deben ser iguales.'
            ];

            $name = ucwords($request->name);
            $email = $request->email;
            $password = bcrypt($request->password);
            $area = $request->area;
            $puesto = ucfirst($request->puesto);

            $validator = Validator::make($request->all(), [
                'email'             => 'required|unique:users,email|email',
                'name'              => 'required|alpha_numeric_spaces|max:100',
                'password'          => 'required|min:8',
                'confirm_password'  => 'required|same:password',
                'area'              => 'required',
                'puesto'            => 'required|alpha_numeric_spaces|max:100',
            ], $messages);

            $errores = $validator->errors()->messages();

            if (count($errores) == 0) {

                $usuario = User::updateOrCreate(
                    [
                        'email' => $email
                    ],
                    [
                        'name'      => $name,
                        'password'  => $password,
                        'area'      => $area,
                        'puesto'    => $puesto,
                    ]
                );

                return response()->json(['success' => true]);

            } else {

                return response()->json([
                    'success' => false,
                    'message' => 'Faltan datos obligatorios.',
                    'errores' => $errores,
                ]);

            }

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);

        }

    }


}
