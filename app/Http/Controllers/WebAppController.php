<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Jobs\ProcesaFaceMatch;
use App\Jobs\ProcesaCargaIdentificacionSelfie;
use App\Solicitud;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\MB_UPLOADED_DOCUMENT;
use App\PlantillaComunicacion;
use App\CargaDocumentos;
use Log;
use Image;
use App;
use Auth;
use Jenssegers\Agent\Agent;
use App\Repositories\SolicitudRepository;
use Session;
use Validator;
use Illuminate\Validation\Rule;

class WebAppController extends Controller
{
    private $solicitudRepository;

    public function __construct(DocumentosRepository $documentos)
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function index(Request $request) {

        if ($request->has('idPanel')) {

            $idPanel = $request->get('idPanel');
            $clienteAlta = ClientesAlta::where('panel_operativo_id', $idPanel)
                ->firstOrFail();

            Auth::guard('prospecto')->loginUsingId($clienteAlta->prospecto_id);

            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;

            // Verificando que estamos en el paso correcto
            $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);
            if ($siguientePaso != null) {
                return redirect("/webapp/{$siguientePaso}");
            } else {
                Auth::guard('prospecto')->logout();
                return view('webapp.documentos_completados');
            }

        } else {

            if (Auth::guard('prospecto')->check()) {

                $prospecto = Auth::guard('prospecto')->user();
                $prospecto_id = $prospecto->id;
                $solicitud = $this->solicitudActual($prospecto->id);
                $solicitud_id = $solicitud->id;

                // Verificando que estamos en el paso correcto
                $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);
                if ($siguientePaso != null) {
                    return redirect("/webapp/{$siguientePaso}");
                } else {
                    Auth::guard('prospecto')->logout();
                    return view('webapp.documentos_completados');
                }

            } else {
                return view('webapp.index');
            }
        }

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    function repetirProceso(Request $request) {
        // Si se requiere repetir proceso Facemath
        // poner el tabla clientes_alta facematch = 0
    }

}
