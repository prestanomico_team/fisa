<?php
namespace App\Http\Controllers;

use App\CatalogoSepomex;
use App\Plazo;
use App\UserConvenio;

trait cleanStrMethods{

    //function to clean general user data from registration and solic forms
    public function cleanStr($str){
		$chars_bad  = ['-','á','é','í','ó','ú','ñ','Á','É','Í','Ó','Ú','Ñ','/','_','Ü','ü','Ń'];
	    $chars_good = [' ','A','E','I','O','U','N','A','E','I','O','U','N', '', '','U','u','N'];
	    //replace unwanted characters and convert to uppercase
	    $str = strtoupper( str_replace($chars_bad, $chars_good, $str) );
	    return utf8_encode($str);
    }

    public function cleanStrBC($str){
		$chars_bad  = ['(', ')', '.', ',', '[', ']', ';', ':', '-', '$', '#', '&', '¡', '!', '¿', '?', '@', '°'];
	    $chars_good = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
	    //replace unwanted characters and convert to uppercase
	    $str = strtoupper( str_replace($chars_bad, $chars_good, $str) );
        $str = preg_replace('/\s+/', ' ', $str);
        $str = trim($str);
	    return utf8_encode($str);
    }

    //function to clean reported amounts in HAWK response for calculation
    public function usoLineaClean($val){
    	//Si el saldo tiene un signo de - lo tomamos como 0 (cero)
    	if($val < 0){
    		$val = 0;
    	}
			$val = str_replace(['+'], [''], $val );
			return $val;
    }

    //function to clean monetary input from user forms
    public function dineroClean($val,$round=false){
    	//remove commas and dollar signs and rounds up
			$val = str_replace(['$',','], ['',''], $val );
			if($round){
				$val = round($val);
			}
			return $val;
    }

    //function to get the Buró de Crédito tag asociated with a report index (key)
    public function getBCTagName($key,$return_array = false){
        $tags_arr = [
            "apellido_p"                => "PN",
            "apellido_m"                => "00",
            "nombre"                    => "02",
            "segundo_nombre"            => "03",
            "fecha_nac_dd"              => "04",
            "fecha_nac_mm"              => "04",
            "fecha_nac_yyyy"            => "04",
            "rfc"                       => "05",
            "prefijo_personal"          => "06",
            "sufijo_personal"           => "07",
            "nacionalidad"              => "08",
            "tipo_residencia"           => "09",
            "lic_conducir"              => "10",
            "estado_civil"              => "11",
            "sexo"                      => "12",
            "cedula_profesional"        => "13",
            "ife"                       => "14",
            "curp"                      => "15",
            "clave_pais"                => "16",
            "num_dependientes"          => "17",
            "edades_dependientes"       => "18",
            "fecha_recep_dependientes"  => "19",
            "fecha_defuncion"           => "20",

            "dom_calle"                 => "PA",
            "dom_calle_segunda_linea"   => "00",
            "dom_colonia"               => "01",
            "dom_deleg"                 => "02",
            "dom_ciudad"                => "03",
            "dom_estado"                => "04",
            "dom_cp"                    => "05",
            "dom_fecha"                 => "06",
            "dom_tel_casa"              => "07",
            "dom_tel_casa_ext"          => "08",
            "dom_fax"                   => "09",
            "dom_tipo"                  => "10",
            "dom_indicador_espec"       => "11",
            "dom_fecha_reporte"         => "12",
            "dom_origen"                => "13",

            "emp_razon_social"          => "PE",
            "emp_dir1"                  => "00",
            "emp_dir2"                  => "01",
            "emp_colonia"               => "02",
            "emp_deleg"                 => "03",
            "emp_ciudad"                => "04",
            "emp_estado"                => "05",
            "emp_cp"                    => "06",
            "emp_num_tel"               => "07",
            "emp_tel_ext"               => "08",
            "emp_fax"                   => "09",
            "emp_cargo"                 => "10",
            "emp_fecha_contrat"         => "11",
            "emp_clave_moneda"          => "12",
            "emp_monto_sueldo"          => "13",
            "emp_periodo_pago"          => "14",
            "emp_num_empleado"          => "15",
            "emp_fecha_ult_dia"         => "16",
            "emp_fecha_reporte"         => "17",
            "emp_fecha_verificacion"    => "18",
            "emp_modo_verificacion"     => "19",
            "emp_origen_razon_social"   => "20",

            "cuenta_fecha_actualizacion"            => "TL",
            "cuenta_impugnado"                      => "00",
            "cuenta_clave_member_code"              => "01",
            "cuenta_nombre_usuario"                 => "02",
            "cuenta_num_tel"                        => "03",
            "cuenta_num_cuenta"                     => "04",
            "cuenta_responsabilidad"                => "05",
            "cuenta_tipo"                           => "06",
            "cuenta_contrato_producto"              => "07",
            "cuenta_moneda"                         => "08",
            "cuenta_importe_evaluo"                 => "09",
            "cuenta_num_pagos"                      => "10",
            "cuenta_freguencia_pagos"               => "11",
            "cuenta_monto_pagar"                    => "12",
            "cuenta_fecha_apertura"                 => "13",
            "cuenta_fecha_ult_pago"                 => "14",
            "cuenta_fecha_ult_compra"               => "15",
            "cuenta_fecha_cierre"                   => "16",
            "cuenta_fecha_reporte"                  => "17",
            "cuenta_modo_reporte"                   => "18",
            "cuenta_ult_fecha_cero"                 => "19",
            "cuenta_garantia"                       => "20",
            "cuenta_cred_max_aut"                   => "21",
            "cuenta_saldo_actual"                   => "22",
            "cuenta_limite_credito"                 => "23",
            "cuenta_saldo_vencido"                  => "24",
            "cuenta_num_pagos_vencidos"             => "25",
            "cuenta_mop"                            => "26",
            "cuenta_hist_pagos"                     => "27",
            "cuenta_hist_pagos_fecha_reciente"      => "28",
            "cuenta_hist_pagos_fecha_antigua"       => "29",
            "cuenta_clave_observacion"              => "30",
            "cuenta_total_pagos"                    => "31",
            "cuenta_total_pagos_mop2"               => "32",
            "cuenta_total_pagos_mop3"               => "33",
            "cuenta_total_pagos_mop4"               => "34",
            "cuenta_total_pagos_mop5_plus"          => "35",
            "cuenta_saldo_morosidad_mas_alta"       => "36",
            "cuenta_fecha_morosidad_mas_alta"       => "37",
            "cuenta_clasif_puntualidad_de_pago"     => "38",
            "cuenta_fecha_inicio_reestructura"      => "42",
            "cuenta_monto_ultimo_pago"              => "45",

            "consulta_fecha"                => "IQ",
            "consulta_reservado1"           => "00",
            "consulta_clave_member_code"    => "01",
            "consulta_nombre_usuario"       => "02",
            "consulta_num_tel"              => "03",
            "consulta_contrato_producto"    => "04",
            "consulta_moneda"               => "05",
            "consulta_importe"              => "06",
            "consulta_responsabilidad"      => "07",
            "consulta_indicador_cliente"    => "08",
            "consulta_resevado2"            => "09",

            "resumen_fecha_integracion"                         => "RS",
            "resumen_cuentas_mop7"                              => "00",
            "resumen_cuentas_mop6"                              => "01",
            "resumen_cuentas_mop5"                              => "02",
            "resumen_cuentas_mop4"                              => "03",
            "resumen_cuentas_mop3"                              => "04",
            "resumen_cuentas_mop2"                              => "05",
            "resumen_cuentas_mop1"                              => "06",
            "resumen_cuentas_mop0"                              => "07",
            "resumen_cuentas_mop_ur"                            => "08",
            "resumen_numero_de_cuentas"                         => "09",
            "resumen_cuentas_pagos_fijos_hipo"                  => "10",
            "resumen_cuentas_revolventes_sin_limite"            => "11",
            "resumen_cuentas_cerradas"                          => "12",
            "resumen_cuentas_con_morosidad_actual"              => "13",
            "resumen_cuentas_con_historial_de_morosidad"        => "14",
            "resumen_cuentas_en_aclaracion"                     => "15",
            "resumen_solicitudes_de_consulta"                   => "16",
            "resumen_nueva_direccion_ult_60_dias"               => "17",
            "resumen_mensaje_de_alerta"                         => "18",
            "resumen_declarativa"                               => "19",
            "resumen_moneda_del_credito"                        => "20",
            "resumen_total_creditos_max_rev_sinlim"             => "21",
            "resumen_total_limites_de_credito_rev_sinlim"       => "22",
            "resumen_total_saldos_actuales_rev_sinlim"          => "23",
            "resumen_total_saldos_vencidos_rev_sinlim"          => "24",
            "resumen_total_importe_de_pago_rev_sinlim"          => "25",
            "resumen_porcentaje_limite_de_credito_utilizado_rev_sinlim"    => "26",
            "resumen_total_creditos_max_fijos_hipo"             => "27",
            "resumen_total_saldos_actuales_fijos_hipo"          => "28",
            "resumen_total_saldos_vencidos_fijos_hipo"          => "29",
            "resumen_total_importe_de_pago_fijos_hipo"          => "31",
            "resumen_cuentas_mop96"                             => "32",
            "resumen_cuentas_mop97"                             => "33",
            "resumen_cuentas_mop99"                             => "34",
            "resumen_fecha_apertura_cuenta_antigua"             => "35",
            "resumen_fecha_apertura_cuenta_reciente"            => "36",
            "resumen_num_solicitudes_informe_buro"              => "37",
            "resumen_fecha_consulta_reciente"                   => "38",
            "resumen_num_cuentas_despacho_cobranza"             => "39",
            "resumen_fecha_apertura_cuenta_en_despacho_reciente" => "40",
            "resumen_num_solicitudes_informe_por_despachos"      => "41",
            "resumen_fecha_consulta_por_despacho_reciente"       => "42",

            "hawk_fecha_reporte"        => "HI",
            "hawk_codigo_prevencion"    => "00",
            "hawk_tipo_usuario"         => "01",
            "hawk_mensaje"              => "02",

            "hawk_fecha_reporte"        => "HR",
            "hawk_codigo_prevencion"    => "00",
            "hawk_tipo_usuario"         => "01",
            "hawk_mensaje"              => "02",

            "declarativa_longitud" => "CR",
            "declarativa_texto" => "00",

            "bc_nombre"         => "SC",
            "bc_codigo_prod"    => "01",
            "bc_valor"          => "02",
            "bc_razon1"         => "03",
            "bc_razon2"         => "04",
            "bc_razon3"         => "05",
            "bc_codigo_error"   => "06",

            "micro_nombre"         => "SC",
            "micro_codigo_prod"    => "01",
            "micro_valor"          => "02",
            "micro_razon1"         => "03",
            "micro_razon2"         => "04",
            "micro_razon3"         => "05",
            "micro_codigo_error"   => "06",
        ];
        if($return_array){
            return $tags_arr;
        }else{
            if(isset($tags_arr[$key])){
                return $tags_arr[$key];
            }else{
                return '';
            }
        }
    }

    //function to get the Buró de Crédito tag asociated with a report index (key)
    public function getCodigoActvDesc($key){
        $codigos = [
            "AD"     => "Cuenta o monto en aclaración directamente con el Usuario",
            "CA"     => "Cuenta al corriente vendida o cedida a un Usuario de una Sociedad de Información Crediticia.",
            "CC"     => "Cuenta cancelada o cerrada",
            "CD"     => "Disminución del monto a pagar debido a un acuerdo con la Institución y ajuste al plan de pagos",
            "CL"     => "Cuenta en cobranza pagada totalmente, sin causar quebranto",
            "CO"     => "Crédito en controversia",
            "CV"     => "Cuenta que no está al corriente vendida o cedida a un Usuario de Buró de Crédito",
            "EL"     => "Eliminación de Clave de Observación",
            "FD"     => "Cuenta con fraude atribuible al Cliente",
            "FN"     => "Fraude NO atribuible al Cliente",
            "FP"     => "Fianza pagada",
            "FR"     => "Adjudicación y/o aplicación de garantía",
            "GP"     => "Ejecución de Garantía Prendaria o Fiduciaria en Pago por Crédito.",
            "IA"     => "Cuenta Inactiva",
            "IM"     => "Integrante Causante de Mora",
            "IS"     => "Integrante que fue subsidiado para evitar mora.",
            "LC"     => "Convenio de finiquito con pago menor a la deuda, acordado con el Cliente (Quita)",
            "LG"     => "Pago menor de la deuda por programa institucional o de gobierno, incluyendo los apoyos a damnificados por catástrofes naturales (Quita)",
            "LO"     => "En Localización",
            "LS"     => "Tarjeta de Crédito robada o extraviada",
            "NA"     => "Cuenta al corriente vendida o cedida a un No Usuario de Buró de Crédito.",
            "NV"     => "Cuenta vencida vendida a un No Usuario de Buró de la Sociedad de Información Crediticia.",
            "PC"     => "Cuenta en Cobranza",
            "PD"     => "Prórroga otorgada debido a un desastre natural.",
            "PE"     => "Prórroga otorgada al Acreditado por situaciones especiales",
            "PI"     => "Prórroga otorgada al Acreditado por invalidez, defunción.",
            "PR"     => "Prórroga otorgada debido a una pérdida de relación laboral.",
            "RA"     => "Cuenta reestructurada sin pago menor, por programa institucional o gubernamental, incluyendo los apoyos a damnificados por catástrofes naturales",
            "RI"     => "Robo de identidad",
            "RF"     => "Resolución judicial favorable al Cliente",
            "RN"     => "Cuenta reestructurada debido a un proceso judicial",
            "RV"     => "Cuenta reestructurada sin pago menor por modificación de la situación del cliente, a petición de éste.",
            "SG"     => "Demanda por el Usuario",
            "UP"     => "Cuenta que causa castigo y/o quebranto",
            "VR"     => "Dación en pago o Renta"
        ];
        if(isset($codigos[$key])){
            return $codigos[$key];
        }else{
            return '';
        }
    }

    //function to get the Score Micro description for a given code
    public function getMicroDesc($code){
        $codigos = [
            "10" => "Poca experiencia crediticia",
            "11" => "Muestra una utilización elevada en sus cuentas",
            "12" => "Muestra límites de crédito elevado",
            "13" => "Consultas recientes",
            "14" => "Presenta cuentas recién abiertas y morosas",
            "15" => "Presenta varias cuentas activas con morosidad",
            "16" => "Muestra cuentas recién aperturadas que alcanzan morosidad",
            "17" => "Varias cuentas con saldo vencido alto",
            "18" => "Poca antigüedad en cuentas abiertas",
            "19" => "Morosidades bajas en el histórico de pagos",
            "20" => "Morosidades moderadas y recurrentes en el histórico de pago",
            "21" => "Morosidades altas en el histórico de pagos",
            "22" => "El cliente presenta alto índice de morosidad",
            "23" => "Varias cuentas en pagos vencidos",
            "24" => "Monto de crédito alto con índice de morosidad elevada",
            "25" => "Mucha actividad en líneas de crédito altas",
            "26" => "Poca experiencia en créditos revolventes",
            "27" => "Varias cuentas que presentan sobregiro",
            "28" => "Al menos una cuenta (abierta o cerrada) con morosidad alta",
            "29" => "Al menos una cuenta abierta con morosidad alta",
            "31" => "Buen manejo de sus cuentas de crédito",
            "32" => "Presentó algunos pagos vencidos que afectan la calificación de manera moderada",
            "33" => "Actualmente alguna cuenta en mora con saldo bajo",
            "34" => "Actualmente alguna cuenta en mora con saldo considerable",
            "35" => "En mora moderada sin financiamiento alterno"
        ];
        if(isset($codigos[$code])){
            return $codigos[$code];
        }else{
            return '';
        }
    }

    public function getPlazoLabel($clave){

        $plazo = Plazo::where('clave', $clave)->get();

        if (count($plazo) == 1) {
            $duracion = $plazo[0]->duracion;
            $plazo = $plazo[0]->duracion.' '.mb_strtoupper($plazo[0]->plazo);
        } else {
            $plazo = '';
        }

        return $plazo;
    }

    public function getEstadoNac($val){
        if($val){
            $estado =  CatalogoSepomex::select('id_estado')
            ->where('estado', $val)
            ->first();

            return $estado->id_estado;
        } else{
            return '';
        }

    }

    public function getCiudadNac($val){
        $ciudad = CatalogoSepomex::select('id_municipio')
           ->where('municipio', $val)
           ->first();

           return $ciudad->id_municipio;
    }

    public function getCuentaFrequenciaLabel($code){
        $labels = [
            'B' => 'Bimestral',
            'D' => 'Diario',
            'H' => 'Semestral',
            'K' => 'Catorcenal',
            'M' => 'Mensual',
            'P' => 'Salario',
            'Q' => 'Trimestral',
            'S' => 'Quincenal',
            'V' => 'Variable',
            'W' => 'Semanal',
            'Y' => 'Anual',
            'Z' => 'Pago Minimo',
        ];
        if(isset($labels[$code])){
            return $labels[$code];
        }else{
            return '';
        }
    }

    public function displayMySqlDate($string){
        $date_time_arr = explode(' ', $string);
        //check if there is a time
        $time = '';
        if(isset($date_time_arr[1])){
            $time = $date_time_arr[1];
        }
        $date = '';
        if(isset($date_time_arr[0])){
            $date_arr = explode('-', $date_time_arr[0]);
            $y = $date_arr[0];
            $m = $date_arr[1];
            $d = $date_arr[2];
            $date = $d.'/'.$m.'/'.$y;
        }
        if($date != ''){
            return $date.' '.$time;
        }else{
            return '';
        }
    }

    public function getEstadoCivilDesc($val){
        $estados_civiles = [
            "D" => "Divorciado",
            "F" => "Unión Libre",
            "M" => "Casado",
            "S" => "Soltero",
            "W" => "Viudo",
        ];
        if(isset($estados_civiles[$val])){
            return $estados_civiles[$val];
        }else{
            return '';
        }
    }

    public function getFinalidad($val) {
        $finalidades = [
            "BIENES PARA EL HOGAR" =>   "1",
            "BIENES PERSONALES"    =>   "2",
            "PAGO TARJETA"         =>   "3",
            "PAGO PRÉSTAMO PERSONAL" =>  "4",
            "GASTOS ESCOLARES"      =>  "5",
            "VACACIONES Y VIAJES"   =>  "6",
            "SALUD Y GASTOS MÉDICOS" => "7",
            "INVERSIÓN NEGOCIO"     =>  "8",
            "GASTOS DE TU NEGOCIO"  =>  "9",
            "REPARACIÓN EN CASA"    =>  "10",
            "EMERGENCIA PERSONAL"   =>  "11",
            "OTRA"                  =>  "12",
        ];
        if(isset($finalidades[$val])){
            return $finalidades[$val];
        }else{
            return '';
        }
    }

    public function getYesOrNo($val){
        if($val == 1){
            return 'Si';
        }
        elseif($val == 0){
            return 'No';
        }else{
            return '';
        }

    }

    public function getAntiguedadLabel($val){
        $antigs = [
            "Menos de 1 año",
            "1 año",
            "2 años",
            "3 años",
            "4 años",
            "5 años",
            "6 años",
            "7 años",
            "8 años",
            "9 años",
            "10 años",
            "Más de 10 años",
        ];
        if(isset($antigs[$val])){
            return $antigs[$val];
        }else{
            return '';
        }
    }

    public function getIDEmbajador($embajador) {
        $id_embajador = UserConvenio::select('id_fmp')
        ->where('email', $embajador)
        ->first();

        return $id_embajador['id_fmp'];
    }

    public function getEmbajador($embajador) {
        $username = explode("@",$embajador);

        return $username[0];
    }

    public function getColonia($domicilio) {

        $id_estado = $domicilio->id_estado;
        $id_delegacion = $domicilio->id_delegacion;
        $cp = $domicilio->cp;

        $id = $id_estado.$id_delegacion.$cp;
        $id_colonia = str_replace($id, '', $domicilio->id_colonia);

        return $id_colonia;

    }

    public function displayBuroDate($string){
        if(strlen($string) == 8){
            $dd = substr($string,0,2);
            $mm = substr($string,2,2);
            $yy = substr($string,4,4);
            return $dd.'/'.$mm.'/'.$yy;
        }else{
            return $string;
        }
    }

    public function setUltPuntoReg($current_upr, $punto, $success, $result, $extra = NULL){
        //check if this is the first ult_punto_reg reported
        if($current_upr == ''){
            $current_upr = '[]';
        }
        $ult_punto_data = json_decode($current_upr);
        //create the new reqport object
        $now = date('d/m/Y H:i:s', time());
        $new_ult_punto = [
                            "punto" => $punto,
                            "success" => $success,
                            "report" => [
                              "datetime" => $now,
                              "result" => $result
                            ]
                          ];
        //add any extra data to the report if any
        if(is_array($extra)){
            foreach($extra as $key => $val){
                $new_ult_punto["report"][$key] = $val;
            }
        }
        //add the new report to the ult_punto_data array
        array_push($ult_punto_data, $new_ult_punto);
        //return the updated json
        return json_encode($ult_punto_data);
    }

    public function setUltMnsjUsr($current,$new){

        //check if this is the first ult_mensj_a_usuario reported
        if($current == ''){
            $current = '[]';
        }
        $ult_mensj_data = json_decode($current);
        //create the new reqport object
        $now = date('d/m/Y H:i:s', time());
        $new_ult_mensj = [
                            "datetime" => $now,
                            "console_log" => $new,
                         ];
        //add the new report to the ult_mensj_data array

        array_push($ult_mensj_data, $new_ult_mensj);
        //return the updated json
        return json_encode($ult_mensj_data);
    }
}
?>
