<?php
namespace App\Http\Controllers;
use App\ClienteT24;
use App\Solicitud;
use App\Prospecto;

trait serverMethods
{
    /**
     * Busca el prospecto en la tabla de clientes_t24 para verificar si ya se
     * dio de alta con anterioridad o no.
     *
     * @param  integer $id_prospecto Id del prospecto
     * @param  integer $id_solicitud Id de la solicitud del prospecto
     *
     * @return array                 Resultado de la búsqueda
     */
    public function buscarClienteT24 ($id_prospecto, $id_solicitud) {

        $prospecto = Prospecto::select('id', 'apellido_paterno', 'apellido_materno', 'nombres', 'email')->find($id_prospecto);
        $solicitud = Solicitud::select('id', 'sexo', 'rfc', 'curp')->find($id_solicitud);

        $rfc = $solicitud->rfc;

        $clienteT24_consulta = null;

        // Verificando si el RFC del prospecto esta a 13 posiciones
        if (strlen($rfc) == 13) {

            // La búsqueda se realiza por rfc con homoclave en la tabla clientes_t24
            $clienteT24_consulta = $this->consultaRFC($rfc, 'rfc_homoclave');

            // Si arroja una coincidencia entonces se trata del mismo prospecto
            if (count($clienteT24_consulta) == 1) {

                // Verificando si el id del prospecto es el mismo de la tabla
                // clientes_t24
                $id_panel = '';
                if ($clienteT24_consulta[0]->id_panel == $id_prospecto) {
                    $id_panel = 'Id panel correcto';
                } else {
                    $id_panel = 'Id panel incorrecto';
                }

                // Verificando si el correo del prospecto es el mismo de la tabla
                // clientes_t24
                $email_distinto = 0;
                if (mb_strtoupper($clienteT24_consulta[0]->email) != mb_strtoupper($prospecto->email)) {
                    $email_distinto = 1;
                }

                return [
                    'cliente_t24'    => true,
                    'datos'          => $clienteT24_consulta->toArray(),
                    'error'          => false,
                    'msj'            => 'Coincidencia búsqueda RFC con homoclave',
                    'id_panel'       => $id_panel,
                    'email_distinto' => $email_distinto
                ];

            } else {

                // Si la búsqueda de rfc con homoclave arroja 0 o más resultados
                // hay que verificar la búsqeda con rfc sin homoclave
                $rfc = substr($rfc, 0, 10);
                $clienteT24_consulta = $this->consultaRFC($rfc, 'rfc');
                $verificaDatos = $this->verificaDatos($prospecto, $solicitud, $clienteT24_consulta);

                return $verificaDatos;

            }

        } else {

            // Si el prospecto no captura el rfc con homoclave
            // hay que verificar la búsqeda con rfc sin homoclave
            //
            $rfc = substr($rfc, 0, 10);
            $clienteT24_consulta = $this->consultaRFC($rfc, 'rfc');
            $verificaDatos = $this->verificaDatos($prospecto, $solicitud, $clienteT24_consulta);

            return $verificaDatos;

        }


    }

    /**
     * Busca el cliente por RFC con o sin homoclave en tabla clientes_t24
     *
     * @param  string $rfc  RFC del prospecto a buscar en tabla clientes_T24
     * @param  string $tipo Tipo de RFC por el que se hara la búsqueda
     *
     * @return array        Resultado de la búsqueda en tabla clientes_T24
     */
    public function consultaRFC($rfc, $tipo) {

        return $clienteT24_consulta = ClienteT24::where($tipo, $rfc)
            ->get();

    }

    /**
     * Busca el cliente por email en tabla clientes_t24
     *
     * @param  string $email  Email del prospecto a buscar en tabla clientes_T24
     *
     * @return array          Resultado de la búsqueda en tabla clientes_T24
     */
    public function consultaEmail($email) {

        return $clienteT24_consulta = ClienteT24::where('email', $email)
            ->get();

    }

    /**
     * Veririca la coincididencia del nombre o curp del prospecto con el resultado
     * de la búsqueda en la tabla clientes_t24 cuando esta es solo por rfc sin homoclave
     *
     * @param  object $prospecto           Datos del prospecto
     * @param  object $solicitud           Datos de la solicitud del prospecto
     * @param  object $clienteT24_consulta Datos del resultado de búsqueda
     *
     * @return array                       Resultado de la coincidencia
     */
    public function VerificaDatos($prospecto, $solicitud, $clienteT24_consulta) {

        // Si la búsqueda por rfc no arrojo resultados se verfica si el email
        // capturado por el prospecto no esta dado de alta en la tabla de clientes_T24
        if (count($clienteT24_consulta) == 0) {

            $clienteT24_consulta = $this->consultaEmail($prospecto->email);
            // Si se encuentra uno o más resultados el prospecto esta usando un
            // email de otro cliente
            if (count($clienteT24_consulta) > 1) {

                return [
                    'cliente_t24'    => false,
                    'datos'          => $clienteT24_consulta->toArray(),
                    'error'          => false,
                    'msj'            => 'La búsqueda del cliente en T24 por e-mail arrojo más de un resultado',
                ];

            // Si el email no se encuentra en la tabla de de clientes_T24 el
            // email no esta registrado y el cliente es Nuevo (Happy Path)
            } else {

                return [
                    'cliente_t24'   => false,
                    'error'         => false,
                    'msj'           => 'No existe el prospecto en T24'
                ];

            }

        // Si la búsqueda por rfc arroja un resultado, se procede a descartar
        // duplicidades primero por CURP en caso de que el prospecto la haya
        // si no por coincidencia de nombre
        } elseif (count($clienteT24_consulta) == 1) {

            $curp = 0;
            $nombre = 0;

            // Si el CURP capturado es el mismo que el CURP en el resultado de
            // la búsqueda entonces el prospecto es el mismo.
            if ($prospecto->curp != '') {

                if ($clienteT24_consulta[0]->curp == $solicitud->curp) {
                    $curp = 1;
                }

            } else {
                // Si el porcentaje de coincidencia del nombre capturado al de la
                // búsqueda mayor a igual del 90% entonces el prospecto es el mismo.
                $coincidencia = $this->coincidenciaNombres($prospecto, $clienteT24_consulta);
                if ($coincidencia >= 90) {
                    $nombre = 1;
                }

            }

            // Si nombre o curp coinciden, el prospecto es el mismo que el
            // encontrado en el resultado de la búsqueda
            if ($nombre = 1 || $curp = 1) {

                $coincidencia = $nombre = 1 ? 'Nombre' : 'Curp';

                $email_distinto = 0;
                if (mb_strtoupper($clienteT24_consulta[0]->email) != mb_strtoupper($prospecto->email)) {
                    $email_distinto = 1;
                }

                $id_panel = '';
                if ($clienteT24_consulta[0]->id_panel == $prospecto->id) {
                    $id_panel = 'Id panel correcto';
                } else {
                    $id_panel = 'Id panel incorrecto';
                }

                return [
                    'cliente_t24'    => true,
                    'datos'          => $clienteT24_consulta->toArray(),
                    'error'          => false,
                    'msj'            => 'Coincidencia búsqueda por '.$coincidencia,
                    'id_panel'       => $id_panel,
                    'email_distinto' => $email_distinto
                ];

            } else {
                // Si no se puede realizar la identificación del cliente por Curp
                // o nombre entonces entonces se tiene que regresar un error.
                return [
                    'cliente_t24'    => false,
                    'datos'          => $clienteT24_consulta->toArray(),
                    'error'          => true,
                    'msj'            => 'Posible homonimia o Cliente registrado con otros datos',
                ];

            }

        } else {
            // Si la búsqueda por RFC sin homoclave arrojo más de un resultado
            // entonces se tiene que regresar un error.
            return [
                'cliente_t24'    => false,
                'datos'          => $clienteT24_consulta->toArray(),
                'error'          => true,
                'msj'            => 'La búsqueda del cliente en T24 por RFC sin homoclave arrojo más de un resultado',
            ];

        }


    }

    /**
     * Veirifica el porcentaje de coincidencia del nombre del prospcecto con el
     * arrojado en el resultado de búsqueda en tabla clientes_t24
     *
     * @param  object $prospecto           Datos del prospecto
     * @param  object $clienteT24_consulta Datos del resultado de búsqueda
     *
     * @return double                      Porcentaje de coincidencia
     */
    public function coincidenciaNombres($prospecto, $clienteT24_consulta) {

        // Concatenando los nombres y apellidos del resultado de la búsqueda y
        // el capturado por el prospecto
        $apellidos_solicitud = $prospecto->apellido_p. ' ' . $prospecto->apellido_m;
        $apellidos_t24 = $clienteT24_consulta[0]->apellido_paterno. ' ' . $clienteT24_consulta[0]->apellido_materno;

        $nombre_solicitud = $prospecto->nombre;
        $nombre_solicitud = explode(' ', $prospecto->nombre, 2);
        $nombre_solicitud = $nombre_solicitud[0];

        $nombre_t24 = $clienteT24_consulta[0]->nombre;

        // Obteniendo el porcentaje de coincidencia de los apellidos
        $apellidos = similar_text(
            self::limpiar_caracteres($apellidos_solicitud),
            self::limpiar_caracteres($apellidos_t24),
            $porcentaje_apellidos
        );

        // Obteniendo el porcentaje de coincidencia de los nombres
        $nombres = similar_text(
            $nombre_solicitud,
            $nombre_t24,
            $porcentaje_nombre
        );

        // Obteniendo el porcentaje de coincidencia global
        $coincidencia = ($porcentaje_nombre + $porcentaje_apellidos)/2;

        return $coincidencia;

    }

    public function createFicoXML($solic, $calculos)
    {
        $Lender="prestanomico";
        $CUSTOMER_NO="prestano";
        $APPLICATION_NO=$solic->id;//"B78CA74C28BEE4B0";
        $APPLICATION_DATE=$solic->created_at->toDateString();
        $APPLICATION_TYPE="L";
        $CUSTOMER_NO="prestano";
        //TRANSLATE PLACE_BIRTH_STATE TO FICO VALUES
        $group_1 = [
        "chiapas",
        "guerrero",
        "oaxaca",
        "durango",
        "guanajuato",
        "michoacán de ocampo",
        "tlaxcala",
      ];
        $group_2 = [
        'campeche',
        'hidalgo',
        'puebla',
        'san luis potosí',
        'tabasco',
        'veracruz',
        'veracruz llave',
        'veracruz de ignacio de la llave',
        'distrito federal',
      ];
        $group_3 = [
        'colima',
        'méxico',
        'morelos',
        'nayarit',
        'querétaro',
        'queretaro arteaga',
        'queretaro de arteaga',
        'quintana roo',
        'sinaloa',
        'yucatan',
        'baja california',
        'baja california sur',
        'chihuahua',
        'sonora',
        'tamaulipas',
      ];
        $group_4 = [
        'aguascalientes',
        'coahuila',
        'jalisco',
        'nuevo león',
      ];
        $PLACE_BIRTH_STATE="0";//DEFAULT
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_1)) {
            $PLACE_BIRTH_STATE="1";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_2)) {
            $PLACE_BIRTH_STATE="2";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_3)) {
            $PLACE_BIRTH_STATE="3";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_4)) {
            $PLACE_BIRTH_STATE="4";
        }
        //TRANSLATE GENDER TO FICO VALUES
        $GENDER= ($solic->sexo == 'F')? "1" : "2";
        //GET AGE FROM DOB
        $fn_mm = str_pad($solic->fecha_nac_mm, 2, '0', STR_PAD_LEFT);
        $fn_dd = str_pad($solic->fecha_nac_dd, 2, '0', STR_PAD_LEFT);
        $bdate = $solic->fecha_nac_yyyy.'-'.$fn_mm.'-'.$fn_dd;
        $age_now = date_diff(date_create($bdate), date_create('today'))->y;
        $AGE= (string) $age_now;
        //TRANSLATE MARITAL STATUS TO FICO VALUES
      $MARITAL_STATUS="0";//default
      $mar_stat_arr = [
        "M" => "1", //Married
        "W" => "2", //Widow
        "F" => "3", //Co-habitating
        "S" => "4", //Single
        "D" => "5", //Separated / Divorced
      ];
        if (in_array($solic->estado_civil, ["M","W","F","S","D"])) {
            $MARITAL_STATUS = $mar_stat_arr[$solic->estado_civil];
        }
        //TRANSLATE OCCUPACION TO FICO VALUES
      $OCCUPATION="0"; //default
      $group_1 = [//Semi-Professional (SP)
        "profesional independiente"
      ];
        $group_3 = [//Driver (DR), Construction (CT), Guard (Civil or Private)(GD), Military (ML), Factory Worker (FW), Services (SV), Unskilled Worker (UW)
        "empleado sector público",
        "empleado sector privado"
      ];
        $group_4 = [// Administratiors (MG), Independent / Business Owner (OB)
        "negocio propio"
      ];
        $group_6 = [// Retired / Pensioned (RT)
        "pensionado",
        "jubilado"
      ];
        $group_10 = [//Other Occupations (OT)
        "arrendador",
        "otro"
      ];
        if (in_array(mb_strtolower($solic->ocupacion), $group_1)) {
            $OCCUPATION="1";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_3)) {
            $OCCUPATION="3";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_4)) {
            $OCCUPATION="4";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_6)) {
            $OCCUPATION="6";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_10)) {
            $OCCUPATION="10";
        }
        //TRANSLATE HOUSING TYPE TO FICO VALUES
        $housing_arr = [
        "propia" => "1", //Own
        "con familiares" => "3", //Parents / With family
        "renta" => "4", //Rent
      ];
        $TYPE_HOUSING=$housing_arr[mb_strtolower($solic->tipo_residencia)];
        $TIME_IN_JOB="0";//default
      //TRANSLATE TIME IN JOB TO FICO VALUES
      $group_1 = [0]; //Less than 12 months
      $group_2 = [1]; //12 - 23 months
      $group_3 = [2]; //24 - 35 months
      $group_4 = [3]; //36 - 47 months
      $group_5 = [4]; //48 - 59 months
      $group_6 = [5,6,7,8,9]; //60 - 119 months
      $group_7 = [10,11]; //120 months or more
      if (in_array($solic->antiguedad_empleo, $group_1)) {
          $TIME_IN_JOB = "1";
      }
        if (in_array($solic->antiguedad_empleo, $group_2)) {
            $TIME_IN_JOB = "2";
        }
        if (in_array($solic->antiguedad_empleo, $group_3)) {
            $TIME_IN_JOB = "3";
        }
        if (in_array($solic->antiguedad_empleo, $group_4)) {
            $TIME_IN_JOB = "4";
        }
        if (in_array($solic->antiguedad_empleo, $group_5)) {
            $TIME_IN_JOB = "5";
        }
        if (in_array($solic->antiguedad_empleo, $group_6)) {
            $TIME_IN_JOB = "6";
        }
        if (in_array($solic->antiguedad_empleo, $group_7)) {
            $TIME_IN_JOB = "7";
        }
        //TRANSLATE TIME IN CURRENT RESIDENCE TO FICO VALUES
      $TIME_IN_CURR_RESID="0";//default
      $group_1 = [0,1];//Less than 24 months
      $group_2 = [2];//24 - 35 months
      $group_3 = [3,4];//36 - 59 months
      $group_4 = [5,6];//60 - 83 months
      $group_5 = [7,8,9,10,11];//84 - 179 months
      if (in_array($solic->antiguedad_domicilio, $group_1)) {
          $TIME_IN_CURR_RESID = "1";
      }
        if (in_array($solic->antiguedad_domicilio, $group_2)) {
            $TIME_IN_CURR_RESID = "2";
        }
        if (in_array($solic->antiguedad_domicilio, $group_3)) {
            $TIME_IN_CURR_RESID = "3";
        }
        if (in_array($solic->antiguedad_domicilio, $group_4)) {
            $TIME_IN_CURR_RESID = "4";
        }
        if (in_array($solic->antiguedad_domicilio, $group_5)) {
            $TIME_IN_CURR_RESID = "5";
        }
        $TELEPHONE_REF="1";//Home + Work + Cell
      //TRANSLATE NUMERO DE DEPENDIENTES TO FICO VALUES
      $NUM_DEPENDENT="1";//default
      $group_1 = [0]; //0
      $group_2 = [1]; //1
      $group_3 = [2]; //2
      $group_4 = [3,4,5,6,7,8,9,10,11,12,13,14,15]; //3 or more
      if (in_array($solic->numero_dependientes, $group_1)) {
          $NUM_DEPENDENT = "1";
      }
        if (in_array($solic->numero_dependientes, $group_2)) {
            $NUM_DEPENDENT = "2";
        }
        if (in_array($solic->numero_dependientes, $group_3)) {
            $NUM_DEPENDENT = "3";
        }
        if (in_array($solic->numero_dependientes, $group_4)) {
            $NUM_DEPENDENT = "4";
        }
        $DEBT_TO_INCOME=round($solic->calc_deuda_ingr);
        $DEBT_TO_INCOME_UNSECURED=round($solic->calc_deuda_ingr_cons);
        $ABILITY_TO_PAY=($solic->calc_atp > 0)? round($solic->calc_atp) : 0;
        //TRANSLATE CONSULTAS TO FICO VALUES
        $inq_vals = [
        0 => 2,
        1 => 3,
        2 => 4,
      ];
        if ($solic->calc_consultas < 3) {
            $NUM_OF_INQ=$inq_vals[$solic->calc_consultas];
        } else {
            $NUM_OF_INQ="5";
        }
        //Number of months since last inquiry
      $MON_SIN_LAST_INQ="0"; //default
      $mon_val = round($solic->meses_desde_consulta_mas_reciente);
        if ($mon_val <= 2) {
            $MON_SIN_LAST_INQ="5";
        } elseif ($mon_val <= 5) {
            $MON_SIN_LAST_INQ="6";
        } elseif ($mon_val <= 11) {
            $MON_SIN_LAST_INQ="7";
        } elseif ($mon_val <= 23) {
            $MON_SIN_LAST_INQ="8";
        } elseif ($mon_val <= 47) {
            $MON_SIN_LAST_INQ="9";
        } else {
            $MON_SIN_LAST_INQ="10";
        }
        $BC_SCORE=$solic->bc_score;
        $SCORE_MICRO=round($solic->micro_valor);
        $ICC_SCORE=round($solic->icc_score);

        //GET HAWK MESSAGE FROM FULL BC REPORT
      $HAWK_MESSG='00';//DEFAULT
      $codes_found = [];//to hold all codes regardless of value
      $fraud_found = [];//to hold any valid (00) HAWK_CODIGO_PREVENCION found in hawk report
      //collect any HAWK fraud codes from the full report response
      if ($solic->report_response != '') {
          $report_data = json_decode($solic->report_response);
          foreach ($report_data as $key => $val) {
              if ($key == 'hawk') {
                  if (is_array($val)) {//is array of objects
              foreach ($val as $obj) {//each object
                foreach ($obj as $k => $v) {//each attribute and val
                  /* EXAMPLE HAWK OBJECT FOR REFERENCE
                         $k                       $v
                    "hawk_tipo":              "solicitud",
                    "hawk_fecha_reporte":     "14092016",
                    "hawk_codigo_prevencion": "002",
                    "hawk_tipo_usuario":      "BURO DE CREDITO ",
                    "hawk_mensaje":           "NO EXISTE INFORMACION"
                   */
                    if ($k == 'hawk_codigo_prevencion') {
                        //check the first two digits only
                        if (strlen($v) == 3) {
                            $v = substr($v, 0, -1);
                        }
                        //add the value to the codes found array (all codes)
                        array_push($codes_found, $v);
                        $fraudKeys = ['01','02','03','04','05','06','07','08','09'];
                        if (in_array($v, $fraudKeys)) {
                            //also add to the fraud_found array (fraud only)
                            array_push($fraud_found, $v);
                        }
                    }
                }
              }
                  }
              }
          }
      }
        if (count($codes_found) > 0 && count($fraud_found) == 0) {
            //Means application is ok regarding Hawk messages.
            //include the first two digits of the first instance listed in this section.
            $HAWK_MESSG=$codes_found[0];
        }
        if (count($codes_found) > 0 && count($fraud_found) >= 1) {
            //Means: application includes one or more references of potential fraud.
            //include the first two digits showed on that instance or on the first of those instances.
            $HAWK_MESSG=$fraud_found[0];
        }
        $UTIL_TRALINE=round($solic->calc_uso_lineas);
        $PAY_HIST_MOP_02=$solic->calc_mop2;
        $PAY_HIST_MOP_03=$solic->calc_mop3;
        $PAY_HIST_MOP_01=round($calculos['porcentaje_mop1_a_meses']);
        $FILE_THICKNESS=($calculos['is_hit'])? '1' : '0';

        //==========================================================================
        // EXAMPLE WITH ALL PARAMS
        // DO NOT DELETE
        // $xml = '
        // <Application Lender="prestanomico" CUSTOMER_NO="prestano" APPLICATION_NO="B78CA74C28BEE4B0" APPLICATION_DATE="2016-10-10" APPLICATION_TYPE="L">
        //   <LoanApplication CUSTOMER_NO="prestano" APPLICATION_NO="B78CA74C28BEE4B0" PLACE_BIRTH_STATE="1" GENDER="1" AGE="59" MARITAL_STATUS="1" OCCUPATION="1" TYPE_HOUSING="1" TIME_IN_JOB="7" TIME_IN_CURR_RESID="7" TELEPHONE_REF="1" UNSER_LOCATION="10" APPLICANT_PRE_DECLINED="10" NUM_DEPENDENT="1" ICC="20" DEBT_TO_INCOME="6" DEBT_TO_INCOME_UNSECURED="3" NET_INCOME="10000" AVAILABLE_INCOME="200" ABILITY_TO_PAY="100" NUM_OF_INQ="2" NUM_TRALIN_90EVER="5" MAX_DLQ_EVER="12"  NUM_REV_TRALIN_CURR="9" MON_SIN_LAST_INQ="10" NUM_SAT_RAT="8" BC_SCORE="631" UTIL_TRALINE="1" PAY_HIST_MOP_02="2" PAY_HIST_MOP_03="0" SCORE_MICRO="744" HAWK_MESSG="20" NET_FRA_REV_BUR="2" TOT_AMT_NOW_PAS_DUE="2" MON_SIN_MOS_REC_DATE_OPEN="3" PER_TRADLINE_WTH_BAL="4" PER_TRADLINE_NEV_DEL="5" TIME_AS_CLIENT="6" NUM_TRALIN_30_PAST_DUE="2" NUM_TRALIN_60_PAST_DUE="5" PAY_HIST_MOP_01="76" />
        // </Application>
        // ';
        //===========================================================================

        //build the xml string with param values
        $xml = '
        <Application Lender="'.$Lender.'" CUSTOMER_NO="'.$CUSTOMER_NO.'" APPLICATION_NO="'.$APPLICATION_NO.'" APPLICATION_DATE="'.$APPLICATION_DATE.'" APPLICATION_TYPE="'.$APPLICATION_TYPE.'">
          <LoanApplication CUSTOMER_NO="'.$CUSTOMER_NO.'" APPLICATION_NO="'.$APPLICATION_NO.'" PLACE_BIRTH_STATE="'.$PLACE_BIRTH_STATE.'" GENDER="'.$GENDER.'" AGE="'.$AGE.'" MARITAL_STATUS="'.$MARITAL_STATUS.'" OCCUPATION="'.$OCCUPATION.'" TYPE_HOUSING="'.$TYPE_HOUSING.'" TIME_IN_JOB="'.$TIME_IN_JOB.'" TIME_IN_CURR_RESID="'.$TIME_IN_CURR_RESID.'" TELEPHONE_REF="'.$TELEPHONE_REF.'" NUM_DEPENDENT="'.$NUM_DEPENDENT.'" DEBT_TO_INCOME="'.$DEBT_TO_INCOME.'" DEBT_TO_INCOME_UNSECURED="'.$DEBT_TO_INCOME_UNSECURED.'" ABILITY_TO_PAY="'.$ABILITY_TO_PAY.'" NUM_OF_INQ="'.$NUM_OF_INQ.'" MON_SIN_LAST_INQ="'.$MON_SIN_LAST_INQ.'" BC_SCORE="'.$BC_SCORE.'" SCORE_MICRO="'.$SCORE_MICRO.'" ICC="'.$ICC_SCORE.'" HAWK_MESSG="'.$HAWK_MESSG.'" UTIL_TRALINE="'.$UTIL_TRALINE.'" PAY_HIST_MOP_02="'.$PAY_HIST_MOP_02.'" PAY_HIST_MOP_03="'.$PAY_HIST_MOP_03.'" PAY_HIST_MOP_01="'.$PAY_HIST_MOP_01.'" FILE_THICKNESS="'.$FILE_THICKNESS.'"/>
        </Application>
      ';
        //echo htmlentities($xml);exit();
        return $xml;
    }


    public function createFicoApplication($solic, $calculos)
    {
        $Lender="prestanomico";
        $CUSTOMER_NO="prestano";
        $APPLICATION_NO=$solic->id;//"B78CA74C28BEE4B0";
        $APPLICATION_DATE=$solic->created_at->toDateString();
        $APPLICATION_TYPE="L";
        $CUSTOMER_NO="prestano";
        //TRANSLATE PLACE_BIRTH_STATE TO FICO VALUES
        $group_1 = [
        "chiapas",
        "guerrero",
        "oaxaca",
        "durango",
        "guanajuato",
        "michoacán de ocampo",
        "tlaxcala",
      ];
        $group_2 = [
        'campeche',
        'hidalgo',
        'puebla',
        'san luis potosí',
        'tabasco',
        'veracruz',
        'veracruz llave',
        'veracruz de ignacio de la llave',
        'distrito federal',
      ];
        $group_3 = [
        'colima',
        'méxico',
        'morelos',
        'nayarit',
        'querétaro',
        'queretaro arteaga',
        'queretaro de arteaga',
        'quintana roo',
        'sinaloa',
        'yucatan',
        'baja california',
        'baja california sur',
        'chihuahua',
        'sonora',
        'tamaulipas',
      ];
        $group_4 = [
        'aguascalientes',
        'coahuila',
        'jalisco',
        'nuevo león',
      ];
        $PLACE_BIRTH_STATE="0";//DEFAULT
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_1)) {
            $PLACE_BIRTH_STATE="1";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_2)) {
            $PLACE_BIRTH_STATE="2";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_3)) {
            $PLACE_BIRTH_STATE="3";
        }
        if (in_array(mb_strtolower($solic->lugar_nac_estado), $group_4)) {
            $PLACE_BIRTH_STATE="4";
        }
        //TRANSLATE GENDER TO FICO VALUES
        $GENDER= ($solic->sexo == 'F')? "1" : "2";
        //GET AGE FROM DOB
        $fn_mm = str_pad($solic->fecha_nac_mm, 2, '0', STR_PAD_LEFT);
        $fn_dd = str_pad($solic->fecha_nac_dd, 2, '0', STR_PAD_LEFT);
        $bdate = $solic->fecha_nac_yyyy.'-'.$fn_mm.'-'.$fn_dd;
        $age_now = date_diff(date_create($bdate), date_create('today'))->y;
        $AGE= (string) $age_now;
        //TRANSLATE MARITAL STATUS TO FICO VALUES
      $MARITAL_STATUS="0";//default
      $mar_stat_arr = [
        "M" => "1", //Married
        "W" => "2", //Widow
        "F" => "3", //Co-habitating
        "S" => "4", //Single
        "D" => "5", //Separated / Divorced
      ];
        if (in_array($solic->estado_civil, ["M","W","F","S","D"])) {
            $MARITAL_STATUS = $mar_stat_arr[$solic->estado_civil];
        }
        //TRANSLATE OCCUPACION TO FICO VALUES
      $OCCUPATION="0"; //default
      $group_1 = [//Semi-Professional (SP)
        "profesional independiente"
      ];
        $group_3 = [//Driver (DR), Construction (CT), Guard (Civil or Private)(GD), Military (ML), Factory Worker (FW), Services (SV), Unskilled Worker (UW)
        "empleado sector público",
        "empleado sector privado"
      ];
        $group_4 = [// Administratiors (MG), Independent / Business Owner (OB)
        "negocio propio"
      ];
        $group_6 = [// Retired / Pensioned (RT)
        "pensionado",
        "jubilado"
      ];
        $group_10 = [//Other Occupations (OT)
        "arrendador",
        "otro"
      ];
        if (in_array(mb_strtolower($solic->ocupacion), $group_1)) {
            $OCCUPATION="1";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_3)) {
            $OCCUPATION="3";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_4)) {
            $OCCUPATION="4";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_6)) {
            $OCCUPATION="6";
        }
        if (in_array(mb_strtolower($solic->ocupacion), $group_10)) {
            $OCCUPATION="10";
        }
        //TRANSLATE HOUSING TYPE TO FICO VALUES
        $housing_arr = [
        "propia" => "1", //Own
        "con familiares" => "3", //Parents / With family
        "renta" => "4", //Rent
      ];
        $TYPE_HOUSING=$housing_arr[mb_strtolower($solic->tipo_residencia)];
        $TIME_IN_JOB="0";//default
      //TRANSLATE TIME IN JOB TO FICO VALUES
      $group_1 = [0]; //Less than 12 months
      $group_2 = [1]; //12 - 23 months
      $group_3 = [2]; //24 - 35 months
      $group_4 = [3]; //36 - 47 months
      $group_5 = [4]; //48 - 59 months
      $group_6 = [5,6,7,8,9]; //60 - 119 months
      $group_7 = [10,11]; //120 months or more
      if (in_array($solic->antiguedad_empleo, $group_1)) {
          $TIME_IN_JOB = "1";
      }
        if (in_array($solic->antiguedad_empleo, $group_2)) {
            $TIME_IN_JOB = "2";
        }
        if (in_array($solic->antiguedad_empleo, $group_3)) {
            $TIME_IN_JOB = "3";
        }
        if (in_array($solic->antiguedad_empleo, $group_4)) {
            $TIME_IN_JOB = "4";
        }
        if (in_array($solic->antiguedad_empleo, $group_5)) {
            $TIME_IN_JOB = "5";
        }
        if (in_array($solic->antiguedad_empleo, $group_6)) {
            $TIME_IN_JOB = "6";
        }
        if (in_array($solic->antiguedad_empleo, $group_7)) {
            $TIME_IN_JOB = "7";
        }
        //TRANSLATE TIME IN CURRENT RESIDENCE TO FICO VALUES
      $TIME_IN_CURR_RESID="0";//default
      $group_1 = [0,1];//Less than 24 months
      $group_2 = [2];//24 - 35 months
      $group_3 = [3,4];//36 - 59 months
      $group_4 = [5,6];//60 - 83 months
      $group_5 = [7,8,9,10,11];//84 - 179 months
      if (in_array($solic->antiguedad_domicilio, $group_1)) {
          $TIME_IN_CURR_RESID = "1";
      }
        if (in_array($solic->antiguedad_domicilio, $group_2)) {
            $TIME_IN_CURR_RESID = "2";
        }
        if (in_array($solic->antiguedad_domicilio, $group_3)) {
            $TIME_IN_CURR_RESID = "3";
        }
        if (in_array($solic->antiguedad_domicilio, $group_4)) {
            $TIME_IN_CURR_RESID = "4";
        }
        if (in_array($solic->antiguedad_domicilio, $group_5)) {
            $TIME_IN_CURR_RESID = "5";
        }
        $TELEPHONE_REF="1";//Home + Work + Cell
      //TRANSLATE NUMERO DE DEPENDIENTES TO FICO VALUES
      $NUM_DEPENDENT="1";//default
      $group_1 = [0]; //0
      $group_2 = [1]; //1
      $group_3 = [2]; //2
      $group_4 = [3,4,5,6,7,8,9,10,11,12,13,14,15]; //3 or more
      if (in_array($solic->numero_dependientes, $group_1)) {
          $NUM_DEPENDENT = "1";
      }
        if (in_array($solic->numero_dependientes, $group_2)) {
            $NUM_DEPENDENT = "2";
        }
        if (in_array($solic->numero_dependientes, $group_3)) {
            $NUM_DEPENDENT = "3";
        }
        if (in_array($solic->numero_dependientes, $group_4)) {
            $NUM_DEPENDENT = "4";
        }
        $DEBT_TO_INCOME=round($solic->calc_deuda_ingr);
        $DEBT_TO_INCOME_UNSECURED=round($solic->calc_deuda_ingr_cons);
        $ABILITY_TO_PAY=($solic->calc_atp > 0)? round($solic->calc_atp) : 0;
        //TRANSLATE CONSULTAS TO FICO VALUES
        $inq_vals = [
        0 => 2,
        1 => 3,
        2 => 4,
      ];
        if ($solic->calc_consultas < 3) {
            $NUM_OF_INQ=$inq_vals[$solic->calc_consultas];
        } else {
            $NUM_OF_INQ="5";
        }
        //Number of months since last inquiry
      $MON_SIN_LAST_INQ="0"; //default
      $mon_val = round($solic->meses_desde_consulta_mas_reciente);
        if ($mon_val <= 2) {
            $MON_SIN_LAST_INQ="5";
        } elseif ($mon_val <= 5) {
            $MON_SIN_LAST_INQ="6";
        } elseif ($mon_val <= 11) {
            $MON_SIN_LAST_INQ="7";
        } elseif ($mon_val <= 23) {
            $MON_SIN_LAST_INQ="8";
        } elseif ($mon_val <= 47) {
            $MON_SIN_LAST_INQ="9";
        } else {
            $MON_SIN_LAST_INQ="10";
        }
        $BC_SCORE=$solic->bc_score;
        $SCORE_MICRO=round($solic->micro_valor);
        $ICC_SCORE=round($solic->icc_score);

        //GET HAWK MESSAGE FROM FULL BC REPORT
      $HAWK_MESSG='00';//DEFAULT
      $codes_found = [];//to hold all codes regardless of value
      $fraud_found = [];//to hold any valid (00) HAWK_CODIGO_PREVENCION found in hawk report
      //collect any HAWK fraud codes from the full report response
      if ($solic->report_response != '') {
          $report_data = json_decode($solic->report_response);
          foreach ($report_data as $key => $val) {
              if ($key == 'hawk') {
                  if (is_array($val)) {//is array of objects
              foreach ($val as $obj) {//each object
                foreach ($obj as $k => $v) {//each attribute and val
                  /* EXAMPLE HAWK OBJECT FOR REFERENCE
                         $k                       $v
                    "hawk_tipo":              "solicitud",
                    "hawk_fecha_reporte":     "14092016",
                    "hawk_codigo_prevencion": "002",
                    "hawk_tipo_usuario":      "BURO DE CREDITO ",
                    "hawk_mensaje":           "NO EXISTE INFORMACION"
                   */
                    if ($k == 'hawk_codigo_prevencion') {
                        //check the first two digits only
                        if (strlen($v) == 3) {
                            $v = substr($v, 0, -1);
                        }
                        //add the value to the codes found array (all codes)
                        array_push($codes_found, $v);
                        $fraudKeys = ['01','02','03','04','05','06','07','08','09'];
                        if (in_array($v, $fraudKeys)) {
                            //also add to the fraud_found array (fraud only)
                            array_push($fraud_found, $v);
                        }
                    }
                }
              }
                  }
              }
          }
      }
        if (count($codes_found) > 0 && count($fraud_found) == 0) {
            //Means application is ok regarding Hawk messages.
            //include the first two digits of the first instance listed in this section.
            $HAWK_MESSG=$codes_found[0];
        }
        if (count($codes_found) > 0 && count($fraud_found) >= 1) {
            //Means: application includes one or more references of potential fraud.
            //include the first two digits showed on that instance or on the first of those instances.
            $HAWK_MESSG=$fraud_found[0];
        }
        $UTIL_TRALINE=round($solic->calc_uso_lineas);
        $PAY_HIST_MOP_02=$solic->calc_mop2;
        $PAY_HIST_MOP_03=$solic->calc_mop3;
        $PAY_HIST_MOP_01=round($calculos['porcentaje_mop1_a_meses']);
        $FILE_THICKNESS=($calculos['is_hit'])? '1' : '0';

        //build the xml string with param values
        $application = (object) [
        "LENDER"=>$Lender,
        "CUSTOMER_NO"=>$CUSTOMER_NO,
        "APPLICATION_NO"=>$APPLICATION_NO,
        "APPLICATION_DATE"=>$APPLICATION_DATE,
        "APPLICATION_TYPE"=>$APPLICATION_TYPE,
        "CUSTOMER_NO"=>$CUSTOMER_NO,
        "APPLICATION_NO"=>$APPLICATION_NO,
        "PLACE_BIRTH_STATE"=>$PLACE_BIRTH_STATE,
        "GENDER"=>$GENDER,
        "AGE"=>$AGE,
        "MARITAL_STATUS"=>$MARITAL_STATUS,
        "OCCUPATION"=>$OCCUPATION,
        "TYPE_HOUSING"=>$TYPE_HOUSING,
        "TIME_IN_JOB"=>$TIME_IN_JOB,
        "TIME_IN_CURR_RESID"=>$TIME_IN_CURR_RESID,
        "TELEPHONE_REF"=>$TELEPHONE_REF,
        "NUM_DEPENDENT"=>$NUM_DEPENDENT,
        "DEBT_TO_INCOME"=>$DEBT_TO_INCOME,
        "DEBT_TO_INCOME_UNSECURED"=>$DEBT_TO_INCOME_UNSECURED,
        "ABILITY_TO_PAY"=>$ABILITY_TO_PAY,
        "NUM_OF_INQ"=>$NUM_OF_INQ,
        "MON_SIN_LAST_INQ"=>$MON_SIN_LAST_INQ,
        "BC_SCORE"=>$BC_SCORE,
        "SCORE_MICRO"=>$SCORE_MICRO,
        "ICC"=>$ICC_SCORE,
        "HAWK_MESSG"=>$HAWK_MESSG,
        "UTIL_TRALINE"=>$UTIL_TRALINE,
        "PAY_HIST_MOP_02"=>$PAY_HIST_MOP_02,
        "PAY_HIST_MOP_03"=>$PAY_HIST_MOP_03,
        "PAY_HIST_MOP_01"=>$PAY_HIST_MOP_01,
        "FILE_THICKNESS"=>$FILE_THICKNESS
      ];
        return $application;
    }


    public function getCurrentHost()
    {
        $host = env('APP_URL');
        return $host;
    }

    public function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }

}
