<?php
namespace App\Http\Controllers;
use App\Plazo;

trait specialCalcs
{

  //function to calculate monthly payment given prestamo, tasa, and plazo
    public function sCalcPagoMensual($prestamo, $tasa, $plazo, $duracion)
    {
        $tempo          = 0;
        $pagoFinal      = 0;
        $prestamo       = $prestamo;
        $plazo          = $plazo;
        $interes        = $tasa;
        $pago_estimado  = 0.00;

        $quincena = ($plazo == 'QUINCENAS') ? true : false;
        $interes = ($quincena) ? $interes / 24 : $interes / 12; // paso de interes de anual a mensual
        $tempo = 1 + $interes ;
        $tempo = pow($tempo, - $duracion);
        $tempo = 1 - $tempo;
        $tempo = $interes / $tempo;
        $tempo = $prestamo * $tempo;

        //Agregar comision más IVA a total
        $tPagoComi = $tempo * 0.028;
        $tPagoIva = $tPagoComi * 1.16;
        $tPagos = $tempo + $tPagoIva;
        return round($tPagos);
    }

    //======================= fecha_integracion =============================================
    public function sCalcGetFechaIntegracion($HawkResponse_arr)
    {
        $fecha_integracion = '';//default
        if (isset($HawkResponse_arr['resumen_informe'])) {
            if (isset($HawkResponse_arr['resumen_informe'][0])) {
                if (isset($HawkResponse_arr['resumen_informe'][0]['resumen_fecha_integracion'])) {
                    $fecha_integracion = $HawkResponse_arr['resumen_informe'][0]['resumen_fecha_integracion'];
                }
            }
        }
        return $fecha_integracion;
    }

    //======================= fecha_apertura_linea_mas_antigua ==============================
    public function sCalcGetFechaAperturaAntig($HawkResponse_arr)
    {
        $fecha_apertura_linea_mas_antigua = '';
        if (isset($HawkResponse_arr['resumen_informe'])) {
            if (isset($HawkResponse_arr['resumen_informe'][0])) {
                if (isset($HawkResponse_arr['resumen_informe'][0]['resumen_fecha_apertura_cuenta_antigua'])) {
                    $fecha_apertura_linea_mas_antigua = $HawkResponse_arr['resumen_informe'][0]['resumen_fecha_apertura_cuenta_antigua'];
                }
            }
        }
        return $fecha_apertura_linea_mas_antigua;
    }

    //=========================== claves_de_actividad ==================================
    public function sCalcGetClavesActividad($cuentas)
    {
        $claves_de_actividad = [];
        $claves_de_fraude = [];
        $claves_de_extravio = [];
        foreach ($cuentas as $cn) {
            //convert any objects to arrays
            $cn = (array)$cn;
            if (isset($cn['cuenta_clave_observacion'])) {
                if (in_array($cn['cuenta_clave_observacion'], ['FD','FN','RI','LS'])) {
                    //add to the general array which contains all
                    array_push($claves_de_actividad, $cn['cuenta_clave_observacion']);
                }
                if (in_array($cn['cuenta_clave_observacion'], ['FD','FN','RI'])) {
                    //add to the claves_fraude_reportadas array
                    array_push($claves_de_fraude, $cn['cuenta_clave_observacion']);
                }
                if (in_array($cn['cuenta_clave_observacion'], ['LS'])) {
                    //add to the claves_fraude_reportadas array
                    array_push($claves_de_extravio, $cn['cuenta_clave_observacion']);
                }
            }
        }
        return [$claves_de_actividad, $claves_de_fraude, $claves_de_extravio];
    }

    //========================== cuentas por producto =========================
    public function sCalcFilterCuentasProductos($cuentas)
    {
        $cuentas_hipo_arr = [];
        $cuentas_auto_arr = [];
        $cuentas_consumo = [];
        foreach ($cuentas as $cn) {
            //convert any objects to arrays
            $cn = (array)$cn;
            //check for the 07 segment of the TL
            if (isset($cn['cuenta_contrato_producto'])) {
                //check for hipotecarios
                if (in_array($cn['cuenta_contrato_producto'], ['HE','RE','SM'])) {
                    array_push($cuentas_hipo_arr, $cn);
                }
                //check for automotriz
                elseif (in_array($cn['cuenta_contrato_producto'], ['AL','AU'])) {
                    array_push($cuentas_auto_arr, $cn);
                }
                //default to consumo array
                else {
                    array_push($cuentas_consumo, $cn);
                }
            }
        }
        return [$cuentas_hipo_arr, $cuentas_auto_arr, $cuentas_consumo];
    }

    //======================= cuentas por duración ======================
    public function sCalcFilterCuentasDuration($cuentas)
    {
        $cuentas_con_duracion = [];//default
    $cuentas_cerradas = 0;//default
    $cuentas_abiertas = 0;//default
    foreach ($cuentas as $cn) {
        //convert any objects to arrays
        $cn = (array)$cn;
        //================== DURATION OF 6 MONTHS OR MORE =======================
        //determine start_timestamp from cuenta_fecha_apertura
        $dd = substr($cn['cuenta_fecha_apertura'], 0, 2);
        $mm = substr($cn['cuenta_fecha_apertura'], 2, 2);
        $yyyy = substr($cn['cuenta_fecha_apertura'], 4, 4);
        $start_timestamp = strtotime($mm.'/'.$dd.'/'.$yyyy);

        //Determine the end_timestamp val from close date or todays date if account is still open
        if (isset($cn['cuenta_fecha_cierre'])) {
            //increment $cuentas_cerradas val
            $cuentas_cerradas++;
            //set the end_timestamp for this account
            $dd = substr($cn['cuenta_fecha_cierre'], 0, 2);
            $mm = substr($cn['cuenta_fecha_cierre'], 2, 2);
            $yyyy = substr($cn['cuenta_fecha_cierre'], 4, 4);
            $end_timestamp = strtotime($mm.'/'.$dd.'/'.$yyyy);
        } else {
            //this account is still open, so increment the $cuentas_abiertas
            $cuentas_abiertas++;
            //use todays date as end_timestamp to determine duration of account
            $end_timestamp = strtotime("now");
        }

        //check if duration is 6 months or greater and add to cuentas con duracion if so
        $tl_duration = $end_timestamp - $start_timestamp;
        if ($tl_duration >= (60*60*24*30*6)) {
            array_push($cuentas_con_duracion, $cn);
        }
    }
        //return report
        return [$cuentas_con_duracion, $cuentas_cerradas, $cuentas_abiertas];
    }

    //======================= worst_mop_date ===================================
    public function sCalcGetPeorMopDate($cuentas)
    {
        $all_mops = [];//default (useful for debug)
    $peor_mop = 0;//default
    $fecha_peor_mop = '';//default
    $num_meses_desde_peor_mop = 0;
        foreach ($cuentas as $cn) {
            //convert any objects to arrays
            $cn = (array)$cn;
            if (isset($cn['cuenta_hist_pagos'])) {
                //split the string into array
                $months = str_split($cn['cuenta_hist_pagos']);
                //set max index for mop1 loop (does not filter for numeric vals)
                $max_index = count($months);
                //check up to the max (up to past 2 years)
                for ($i=0; $i <= $max_index; $i++) {
                    //check if there is a value for this month
                    if (isset($months[$i])) {
                        //check for worst mop and worst mop date
                        if (is_numeric($months[$i])) {
                            $worst_mop_date = '';
                            //check if the fecha of highest mop for this TL exists
                            if (isset($cn['cuenta_fecha_morosidad_mas_alta'])) {
                                $worst_mop_date = $cn['cuenta_fecha_morosidad_mas_alta'];
                            }
                            array_push($all_mops, [
                'mop' => $months[$i],
                'fecha_peor' => $worst_mop_date
              ]);
                        }
                    }
                }//end historial loop
            }// end if historial
        }// end cuentas loop
    //find the peor mop in all account histories
    foreach ($all_mops as $cmop) {
        if ($cmop['mop'] > $peor_mop) {
            //found a greater mop than before
            $peor_mop = $cmop['mop'];
            //UPDATE FECHA PEOR MOP TO THE cuenta_fecha_morosidad_mas_alta FOR THIS ACCOUNT
            $fecha_peor_mop = $cmop['fecha_peor'];
        }
    }
        //get num_meses_desde_peor_mop
        if (is_string($fecha_peor_mop) && strlen($fecha_peor_mop) == 8) {
            //get difference in time since worst mop date
            $fpm_dd = substr($fecha_peor_mop, 0, 2);
            $fpm_mm = substr($fecha_peor_mop, 2, 2);
            $fpm_yy = substr($fecha_peor_mop, 4, 4);
            $worst_timestamp = strtotime($fpm_dd.'-'.$fpm_mm.'-'.$fpm_yy);
            $now_timestamp = strtotime('now');
            $diff_time = $now_timestamp - $worst_timestamp;
            $month_time = 60*60*24*30;
            $num_meses_desde_peor_mop = number_format(($diff_time/$month_time), 2);
        }
        return [$peor_mop, $fecha_peor_mop, $num_meses_desde_peor_mop];
    }

    //======================== GET MONTHS HIST TO COUNT ========================
    //takes month and year of pago mas reciente and month and year of solicitud
    //returns the number of months of historial that should be counted
    public function sCalcGetMonthsHistToCount($reciente, $solicitud, $max)
    {
        $rec_arr = explode('-', $reciente);
        $rec_mm = $rec_arr[0];
        $rec_yy = $rec_arr[1];

        $sol_arr = explode('-', $solicitud);
        $sol_mm = $sol_arr[0];
        $sol_yy = $sol_arr[1];

        //get months from reciente date up to solicitud date
        $months_diff = 0;
        $rec_str = $rec_mm.'/'.$rec_yy;
        $sol_str = $sol_mm.'/'.$sol_yy;
        while ($rec_str != $sol_str) {
            $rec_mm++;//increment reciente month
      if ($rec_mm == 13) {//reached end of year, so increase the year and reset the month
        $rec_mm = 1;
          $rec_yy++;
      }
            //echo str_pad($rec_mm, 2, '0', STR_PAD_LEFT).'/'.$rec_yy.' | '.$sol_mm.'/'.$sol_yy.'<br>';
      $rec_str = str_pad($rec_mm, 2, '0', STR_PAD_LEFT).'/'.$rec_yy;//update the rec_str for next loop check
      $months_diff++;//increment the months count
        }
        //we don't count the first month (from solic to pago más reciente) so we need to subtract 1 from the diff
        $months_diff -= 1;
        //if the $months_diff is less than the max, we subtract
        if ($months_diff <= $max) {
            $months_to_count = $max-$months_diff;
        } else {
            //months diff is greater than the max so we will count 0
            $months_to_count = 0;
        }
        // echo 'counted '.$months_diff.' months between '.$reciente.' and '.$solicitud.' <br>
        // <strong>you should count '.$months_to_count.' months of the TLs monthsory</strong>';
        return [$months_diff,$months_to_count];
    }

    //======================== MOP 1 ===========================================
    public function sCalcGetMop1Data($cuentas)
    {
        $mop1_total_all_cuentas = 0;//counts all occurrances of mop1 for all months
    $meses_total = 0;//counts all months (no filter for numeric) for percentage calc
    $porcentaje_mop1_a_meses = 0;//default
    //=========================== CUENTAS LOOP ==================================
    foreach ($cuentas as $cn) {
        //convert any objects to arrays
        $cn = (array)$cn;
        if (isset($cn['cuenta_hist_pagos'])) {
            //split the string into array
            $months = str_split($cn['cuenta_hist_pagos']);
            //set max index for mop1 loop (does not filter for numeric vals)
            $max_index = count($months);
            //counter for all months in the historial for this TL (filters for val of 1,U or X)
            $this_cuenta_months_count = 0;

            //======================= HISTORY LOOP ===================================
            //check up to the max (up to past 2 years) for mop1
            for ($i=0; $i <= $max_index; $i++) {
                //check if there is a value for this month
                if (isset($months[$i])) {
                    //increment the total months count for This TL (numeric or U or X)
                    if (is_numeric($months[$i]) || in_array($months[$i], ['U','X'])) {
                        $this_cuenta_months_count++;
                    }
                    //increment the mop1_total_all_cuentas if val for this month is 1
                    if ($months[$i] == 1) {
                        $mop1_total_all_cuentas++;
                    }
                }
            }//======================= END HISTORY LOOP ==============================
            //add any found numeric or U,X months to the total for mop1 percentage calc
            $meses_total += $this_cuenta_months_count;
        }
    }//======================== END CUENTAS LOOP =================================
    //calculate percentage
    if ($meses_total > 0) {//avoid division by zero
      $porcentaje_mop1_a_meses = ($mop1_total_all_cuentas * 100) / $meses_total;
    }
        $porcentaje_mop1_pasa = ($porcentaje_mop1_a_meses >= 75)? true : false;
        $pasa_result = ($porcentaje_mop1_pasa)? 1 : 0;
        $mop1_report = '{
      "mop1_total_all_cuentas" : '.$mop1_total_all_cuentas.',
      "meses_total" : '.$meses_total.',
      "porcentaje_mop1_a_meses" : '.$porcentaje_mop1_a_meses.',
      "porcentaje_mop1_pasa" : '.$pasa_result.',
      "minimo_para_pasar": 75
    }';
        return [$porcentaje_mop1_a_meses, $porcentaje_mop1_pasa, $mop1_report];
    }

    //========================= MOP 2 ==========================================
    public function sCalcGetMop2Count($cuentas)
    {
        $cuentas_con_mop2_plus = 0;//en los ultimos 12 meses
        $debug_arr = [];
        foreach ($cuentas as $cn) {
            //convert any objects to arrays
            $cn = (array)$cn;
            //count mops
      $tl_mops2 = 0;//counts only months with val 2 or greater for this cuenta
      if (isset($cn['cuenta_hist_pagos'])) {
          //split the string into array
          $months = str_split($cn['cuenta_hist_pagos']);
          //======================= MOP 2 LOOP ===================================
          //for mop2 we check up to the last 12 months, but the history for each TL does not always start
          //from this month so we will use label 28 (cuenta_hist_pagos_fecha_reciente) to get the
          //most recent date (when the history for this TL Begins)
          $fd_dd = substr($cn['cuenta_hist_pagos_fecha_reciente'], 0, 2);
          $fd_mm = substr($cn['cuenta_hist_pagos_fecha_reciente'], 2, 2);
          $fd_yy = substr($cn['cuenta_hist_pagos_fecha_reciente'], 4, 4);
          $hist_from_string = $fd_mm.'-'.$fd_yy;
          $now = time();
          $now_date_str = date("m-Y", $now);
          $res = self::sCalcGetMonthsHistToCount($hist_from_string, $now_date_str, 12);
          $months_diff = $res[0];
          $months_to_count = $res[1];
          $max_index = $months_to_count -1;//for the loop
        $tl_months_counted = 0;//for debug
        //do the loop
        for ($i=0; $i <= $max_index; $i++) {
            //check if there is a value for this month
            if (isset($months[$i])) {
                $tl_months_counted++;
                if (is_numeric($months[$i])) {
                    if ($months[$i] >= 2) {
                        $tl_mops2++;
                    }
                }
            }
        }
          array_push($debug_arr, [$cn['cuenta_hist_pagos'], $hist_from_string, $now_date_str, $months_diff, $months_to_count, $tl_months_counted, $tl_mops2]);
          //increment the counter for TLs with more than 2 occurrences of val 2 or greater
          if ($tl_mops2 >= 2) {
              $cuentas_con_mop2_plus ++;
          }
      }
        }
        //menos de 2 cuentas con 2 o más Mop2 o mayor (mop3 mop4 mop5 mop6 mop7 mop96 mop97 mop99) en los ultimos 12 meses
        $mop2_pasa = ($cuentas_con_mop2_plus > 1)? false : true;
        return [$cuentas_con_mop2_plus, json_encode($debug_arr),$mop2_pasa];
    }

    //========================= MOP 3 ==========================================
    public function sCalcGetMop3Count($cuentas)
    {
        $cuentas_con_mop3_plus = 0;//en los ultimos 18 meses
        $debug_arr = [];
        foreach ($cuentas as $cn) {
            //convert any objects to arrays
            $cn = (array)$cn;
            //count mops
      $tl_mops3 = 0;//counts only months with val 3 or greater for this TL
      if (isset($cn['cuenta_hist_pagos'])) {
          //split the string into array
          $months = str_split($cn['cuenta_hist_pagos']);
          //======================= MOP 3 LOOP ===================================
          //for mop3 we check up to the last 18 months, but the history for each TL does not always start
          //from this month so we will use label 28 (cuenta_hist_pagos_fecha_reciente) to get the
          //most recent date which (when the history for this TL Begins)
          $fd_dd = substr($cn['cuenta_hist_pagos_fecha_reciente'], 0, 2);
          $fd_mm = substr($cn['cuenta_hist_pagos_fecha_reciente'], 2, 2);
          $fd_yy = substr($cn['cuenta_hist_pagos_fecha_reciente'], 4, 4);
          $hist_from_string = $fd_mm.'-'.$fd_yy;
          $now = time();
          $now_date_str = date("m-Y", $now);
          $res = self::sCalcGetMonthsHistToCount($hist_from_string, $now_date_str, 18);
          $months_diff = $res[0];
          $months_to_count = $res[1];
          $max_index = $months_to_count -1;//for the loop
        $tl_months_counted = 0;//for debug
        //do the loop
        for ($i=0; $i <= $max_index; $i++) {
            //check if there is a value for this month
            if (isset($months[$i])) {
                $tl_months_counted++;
                if (is_numeric($months[$i])) {
                    if ($months[$i] >= 3) {
                        $tl_mops3++;
                    }
                }
            }
        }
          array_push($debug_arr, [$cn['cuenta_hist_pagos'], $hist_from_string, $now_date_str, $months_diff, $months_to_count, $tl_months_counted, $tl_mops3]);
          //increment the counter for TLs with more than 1 occurrences of val 3 or greater
          if ($tl_mops3 >= 1) {
              $cuentas_con_mop3_plus ++;
          }
      }
        }
        //No más de una cuenta con 1 o más mop3 o mayor en los ultimos 18 meses
        $mop3_pasa = ($cuentas_con_mop3_plus > 1)? false : true;
        return [$cuentas_con_mop3_plus, json_encode($debug_arr), $mop3_pasa];
    }

    //========================= IS HIT =========================================
    public function sCalcGetNotComServs($cuentas_con_duracion)
    {
        //if any of the accounts (open or closed) lasted at least 6 months and is not of type Services or Comunications
    $not_com_servs = 0;//default
    foreach ($cuentas_con_duracion as $cn) {
        //check if type of account is NOT services or communications
        if (!in_array($cn['cuenta_nombre_usuario'], ['SERVICIOS','COMUNICACIONES'])) {
            $not_com_servs++;
        }
    }
        return $not_com_servs;
    }

    //================= ADJUST PAGO TO MENSUAL =====================================
    public function sCalcAdjustFrequencia($monto, $frequencia)
    {
        $monto_mensual_ajustado = $monto;
        switch ($frequencia) {
      case 'B': //Bimestral (cada 2 Meses)
          $monto_mensual_ajustado = $monto / 2;
        break;
      case 'D': //Diario
          $monto_mensual_ajustado = $monto * 30;
        break;
      case 'H': //Semestral (Cada 6 Meses)
          $monto_mensual_ajustado = $monto / 6;
        break;
      case 'K': //Catorcenal (cada 2 Semanas)
          $monto_mensual_ajustado = ($monto / 14) * 30;
        break;
      case 'M': //Mensual
          $monto_mensual_ajustado = $monto ;
        break;
      case 'Z': //Mensual
          $monto_mensual_ajustado = $monto ;
        break;
      case 'Q': //Trimestral (Cada 3 meses)
          $monto_mensual_ajustado = $monto / 3;
        break;
      case 'S': //Quincenal (dos veces al mes)
          $monto_mensual_ajustado = $monto * 2;
        break;
      case 'W': //Semanal
          $monto_mensual_ajustado = ($monto / 7) * 30;
        break;
      case 'Y': //Anual
          $monto_mensual_ajustado = $monto / 12;
        break;
      case 'P': //Deducción del Salario
          $monto_mensual_ajustado = $monto * 2;
        break;
      case 'V': //Variable
          $monto_mensual_ajustado = $monto;
        break;
    }
        return $monto_mensual_ajustado;
    }



    //================= PAGO MENSUAL TOTAL DE DEUDA BC =====================================
    public function sCalcGetPagoMensualTotalDeuda($cuentas)
    {
        $pago_mensual_total_de_deuda = 0.00;
        $debug_arr = [];
        $counter = 0;
        foreach ($cuentas as $cuenta) {
            $counter++;
            //convert any objects to arrays
            $cuenta = (array)$cuenta;

            $monto_pagar_default = (isset($cuenta['cuenta_monto_pagar']))? $cuenta['cuenta_monto_pagar'] : 0;
            //create the debug arr for this TL with default vals
            $tl_debug = [
        "num" => $counter,//0
        "fecha_cierre" => '',//1
        "saldo_actual" => '',//2
        "saldo_status" => '',//3
        "est_min" => '',//4
        "monto_pagar" => $monto_pagar_default,//5
        "saldo_vencido" => 'NO',//6
        "contrato_producto" => $cuenta['cuenta_contrato_producto'],//7
        "tipo" => $cuenta['cuenta_tipo'],//8
        "frequencia" => (isset($cuenta['cuenta_freguencia_pagos']))? self::getCuentaFrequenciaLabel($cuenta['cuenta_freguencia_pagos']) : '',//9
        "monto_ajustado" => 0,//10
      ];

            //solo cuentas abiertas
            if (!isset($cuenta['cuenta_fecha_cierre'])) {
                //get estimated minimum payment amount for this account (6% of saldo actual)
        $saldo_actual_status = '+';//default
        $saldo_actual = 0.00;//default
        $est_min = 0;//default
        if (isset($cuenta['cuenta_saldo_actual'])) {
            $saldo_actual_status = substr($cuenta['cuenta_saldo_actual'], -1);
            $saldo_actual = substr($cuenta['cuenta_saldo_actual'], 0, (strlen($cuenta['cuenta_saldo_actual']) -1));
            $est_min = $saldo_actual * 0.06;//6% of saldo_actual
        }
                $tl_debug['saldo_status'] = $saldo_actual_status;
                $tl_debug['saldo_actual'] = $saldo_actual;
                $tl_debug['est_min'] = $est_min;


                //check if there is a saldo vencido or Saldo Actual for this account and if it is equal to the monto a pagar
        $pago_is_vencido = false;//default
        $pago_is_actual = false;//default
        if (isset($cuenta['cuenta_monto_pagar'])) {
            if (isset($cuenta['cuenta_saldo_vencido']) && $cuenta['cuenta_saldo_vencido'] > 0) {
                if ($cuenta['cuenta_saldo_vencido'] == $cuenta['cuenta_monto_pagar']) {
                    $pago_is_vencido = true;
                    $tl_debug['saldo_vencido'] = $cuenta['cuenta_saldo_vencido'];
                }
            }
            //Or Saldo Actual
            if (isset($cuenta['cuenta_saldo_actual']) && $cuenta['cuenta_saldo_actual'] > 0) {
                if ($cuenta['cuenta_saldo_actual'] == $cuenta['cuenta_monto_pagar']) {
                    $pago_is_actual = true;
                    $tl_debug['saldo_vencido'] = $cuenta['cuenta_saldo_actual'];
                }
            }
        }

                $monto_a_pagar_por_la_cuenta = 0;//default
                //si no tiene saldo a favor (-)
                if ($saldo_actual_status == '+') {
                    // Cuenta contrato producto es uno de 'CC', 'CL', 'LR', 'SC', 'TE'
                    if (in_array($cuenta['cuenta_contrato_producto'], ['CC', 'CL', 'LR', 'SC', 'TE'])) {
                        // Frecuencia es mensual (si) ‘M’, “Z”, “V”
                        if (isset($cuenta['cuenta_freguencia_pagos']) && in_array($cuenta['cuenta_freguencia_pagos'], ['M', 'Z', 'V'])) {
                            //cuenta monto a pagar >= %6 de saldo actual (SI)
                            if (isset($cuenta['cuenta_monto_pagar']) && $cuenta['cuenta_monto_pagar'] >= $est_min) {
                                $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_monto_pagar'];
                            } else {//cuenta monto a pagar >= 6% de saldo actual (NO) O Carece de etiqueta TL12
                                $monto_a_pagar_por_la_cuenta = $est_min;
                            }
                        } else {//Frecuencia NO es mensual
                            //El monto a pagar NO es igual a el Saldo Vencido o Saldo Actual
                            if (!$pago_is_vencido && !$pago_is_actual) {
                                //Ajustar por frecuencia de pagos
                                $monto_ajustado = self::sCalcAdjustFrequencia($cuenta['cuenta_monto_pagar'], $cuenta['cuenta_freguencia_pagos']);
                                $monto_a_pagar_por_la_cuenta = $monto_ajustado;
                            } else { //El monto a pagar SI es igual a el Saldo Vencido o Saldo Actual
                                if ($pago_is_vencido) {
                                    $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_saldo_vencido'];
                                } else {
                                    $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_saldo_actual'];
                                }
                            }
                        }
                    } else {// Cualquier otro producto que no sea los de ['CC', 'CL', 'LR', 'SC', 'TE']
                        //Frecuencia es mensual (si) ‘M’, “Z”, “V”
                        if (isset($cuenta['cuenta_freguencia_pagos']) && in_array($cuenta['cuenta_freguencia_pagos'], ['M', 'Z', 'V'])) {
                            //Monto mensual a pagar por la cuenta = cuenta monto a pagar (TL12)
                            if (isset($cuenta['cuenta_monto_pagar'])) {
                                $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_monto_pagar'];
                            }
                        } else {//Frecuencia NO es mensual
                            //El monto a pagar NO es igual a el Saldo Vencido
                            if (!$pago_is_vencido && !$pago_is_actual) {
                                //Ajustar por frecuencia de pagos
                                if (isset($cuenta['cuenta_freguencia_pagos'])) {
                                    $monto_ajustado = self::sCalcAdjustFrequencia($cuenta['cuenta_monto_pagar'], $cuenta['cuenta_freguencia_pagos']);
                                    $monto_a_pagar_por_la_cuenta = $monto_ajustado;
                                } else {
                                    $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_monto_pagar'];
                                }
                            } else { //El monto a pagar SI es igual a el Saldo Vencido
                                if ($pago_is_vencido) {
                                    $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_saldo_vencido'];
                                } else {
                                    $monto_a_pagar_por_la_cuenta = $cuenta['cuenta_saldo_actual'];
                                }
                            }
                        }
                    }
                }

                //add the final amount for this account to the total
                $pago_mensual_total_de_deuda += $monto_a_pagar_por_la_cuenta;
                //set debug val for this TL
                $tl_debug['monto_ajustado'] = $monto_a_pagar_por_la_cuenta;
            } else {
                //this account is ignored because of closed date
                $tl_debug['fecha_cierre'] = self::displayBuroDate($cuenta['cuenta_fecha_cierre']);
            }
            //add the TL's debug data to the debug_arr
            array_push($debug_arr, $tl_debug);
        }//end loop
        return [$pago_mensual_total_de_deuda, json_encode($debug_arr)];
    }

    //=====================PAGO MENSUAL TOTAL HIPOTECARIO Y AUTOMOTRIZ ===================
    public function sCalcGetPagoMensualHipoAuto($cuentas_hipo_arr, $cuentas_auto_arr)
    {
        $pago_mensual_total_hipotec_automotriz = 0.00;
        foreach ($cuentas_hipo_arr as $cuenta) {
            if (isset($cuenta['cuenta_monto_pagar'])) {
                $pago_mensual_total_hipotec_automotriz += $cuenta['cuenta_monto_pagar'];
            }
        }
        foreach ($cuentas_auto_arr as $cuenta) {
            if (isset($cuenta['cuenta_monto_pagar'])) {
                $pago_mensual_total_hipotec_automotriz += $cuenta['cuenta_monto_pagar'];
            }
        }
        return $pago_mensual_total_hipotec_automotriz;
    }

    //========================== USO DE LINEAS ====================================
    public function sCalcGetUsoLineas($cuentas, $ingreso_neto_mensual_total)
    {
        $uso_de_lineas = 0.00;//default
    $limite_de_credito = 0.00;//default
    $saldo_actual = 0.00;//default
    $ingreso_neto_cuatro = $ingreso_neto_mensual_total * 4;
        $uso_de_lineas_razon = '';
        $uso_de_lineas_pasa = false;//default
    $cuentas_uso_saldo = [];//for debug
    $cuentas_uso_limite = [];//for debug
    foreach ($cuentas as $cuenta) {
        //convert any objects to arrays
        $cuenta = (array)$cuenta;
        if (!isset($cuenta['cuenta_fecha_cierre'])) {
            if (isset($cuenta['cuenta_limite_credito'])) {
                if (isset($cuenta['cuenta_contrato_producto']) && in_array($cuenta['cuenta_contrato_producto'], array('CC','CL','LR','SC','TE'))) {
                    if ($cuenta['cuenta_contrato_producto'] == 'CL' && in_array($cuenta['cuenta_nombre_usuario'], array('SERVICIOS','COMUNICACIONES','COBRANZA'))) {
                        //DO NOTHING FOR THESE
                    } else {
                        if ($cuenta['cuenta_limite_credito'] > 1) {
                            $limite_de_credito += self::usoLineaClean($cuenta['cuenta_limite_credito']);
                            array_push($cuentas_uso_limite, $cuenta['cuenta_limite_credito']);
                            //Si tenemos saldo actual y NO saldo vencido Tomamos el saldo actual
                            //Si tenemos saldo actual Y saldo vencido Tomamos el saldo actual
                            if (isset($cuenta['cuenta_saldo_actual']) && $cuenta['cuenta_saldo_actual'] > 0) {
                                $saldo_actual+= self::usoLineaClean($cuenta['cuenta_saldo_actual']);
                                array_push($cuentas_uso_saldo, 's:'.$cuenta['cuenta_saldo_actual']);
                            } else {
                                //Si NO tenemos saldo actual y si tenemos saldo vencido Tomamos el saldo vencido
                                if (isset($cuenta['cuenta_saldo_vencido'])) {
                                    $saldo_actual+= self::usoLineaClean($cuenta['cuenta_saldo_vencido']);
                                    array_push($cuentas_uso_saldo, 'v:'.$cuenta['cuenta_saldo_vencido']);
                                }
                            }
                        }
                    }
                } else {
                    //Not the correct product type
                    array_push($cuentas_uso_saldo, 'N');
                    array_push($cuentas_uso_limite, 'N');
                }
            } else {
                //does not have limite credito
                array_push($cuentas_uso_saldo, 'L');
                array_push($cuentas_uso_limite, 'L');
            }
        } else {
            //has fecha de cierre
            array_push($cuentas_uso_saldo, 'X');
            array_push($cuentas_uso_limite, 'X');
        }
    }

        if ($limite_de_credito < $ingreso_neto_cuatro) {
            $uso_de_lineas_pasa = true;
            $uso_de_lineas_razon = 'Ingreso';
        } else {
            if ($limite_de_credito > 0) {
                $uso_de_lineas = ($saldo_actual / $limite_de_credito) * 100;
            }
            if ($uso_de_lineas <= 50) {
                $uso_de_lineas_pasa = true;
                $uso_de_lineas_razon = 'Uso de lineas';
            }
        }
        return [$uso_de_lineas, $limite_de_credito, $saldo_actual, $ingreso_neto_cuatro, $uso_de_lineas_razon, $uso_de_lineas_pasa];
    }

    //======================= ABILITY TO PAY (ATP) ===============================
    public function sCalcGetAtpData($monto_pago_prestanomico, $pago_mensual_total_de_deuda, $gasto_familiar_estimado, $ingreso_neto_mensual_total)
    {
        $gasto_familiar_total_de_deuda = $pago_mensual_total_de_deuda * 1.25;
        $mayor_gastos_familiar = ($gasto_familiar_total_de_deuda > $gasto_familiar_estimado)? $gasto_familiar_total_de_deuda : $gasto_familiar_estimado;
        $ability_to_pay = (($ingreso_neto_mensual_total - $mayor_gastos_familiar - $monto_pago_prestanomico) / $ingreso_neto_mensual_total) * 100;
        $ability_to_pay_pasa = ($ability_to_pay >= 20)? true : false;
        return [$gasto_familiar_total_de_deuda, $mayor_gastos_familiar, $ability_to_pay, $ability_to_pay_pasa];
    }

    //=========================== CONSULTAS =============================================
    public function sCalcGetConsultasData($consultas)
    {
        $consultas_count = 0;
        //count the consultas within the last 3 months and collect their dates
        $consultas_cut_timestamp = strtotime("-3 months");
        $consultas_dates = [];
        foreach ($consultas as $consulta) {
            //convert any objects to arrays
            $consulta = (array)$consulta;
            $cd = str_split($consulta['consulta_fecha'], 2);
            $consulta_date = $cd[2].$cd[3].'-'.$cd[1].'-'.$cd[0];
            $consulta_timestamp = strtotime($consulta_date);
            //ignore any consultas by PRESTANOMICO or BC
            if (!in_array($consulta['consulta_nombre_usuario'], ['PRESTANOMICO','BURO DE CREDITO','CONSUMIDOR FINAL','Prestanómico'])) {
                if ($consulta_timestamp > $consultas_cut_timestamp) {
                    $consultas_count++;//consulta is within the last 3 months
          array_push($consultas_dates, $consulta_timestamp);//for finding latest timestamp below
                }
            }
        }
        $consultas_count_pasa = ($consultas_count <= 8)? true : false;

        //find the latest timestamp for consultas
        $highest_consulta_timestamp = 0;
        foreach ($consultas_dates as $tms) {
            if ($tms > $highest_consulta_timestamp) {
                $highest_consulta_timestamp = $tms;
            }
        }
        $fecha_consulta_mas_reciente = date('d/m/Y', $highest_consulta_timestamp);

        //get number of months since last consulta
        $now_timestamp = strtotime('now');
        $diff_time = $now_timestamp - $highest_consulta_timestamp;
        $month_time = 60*60*24*30;
        $meses_desde_consulta_mas_reciente = number_format(($diff_time/$month_time), 2);

        return [$consultas_count, $consultas_count_pasa, $fecha_consulta_mas_reciente, $meses_desde_consulta_mas_reciente];
    }

    public function sCalcRunCalcs1($solic, $HawkResponse_arr)
    {
        $calculos = [];
        //=================== PEOR CALIFICACION MOP ========================
        $peor_mop_data = self::sCalcGetPeorMopDate($HawkResponse_arr['cuentas']);
        $calculos['peor_mop'] = $peor_mop_data[0];
        $calculos['fecha_peor_mop'] = $peor_mop_data[1];
        $calculos['num_meses_desde_peor_mop'] = $peor_mop_data[2];

        //========================== MOP 1 =======================================
        $mop1_data = self::sCalcGetMop1Data($HawkResponse_arr['cuentas']);//get percentage of mop1 to all months for all accounts
        $calculos["porcentaje_mop1_a_meses"] = $mop1_data[0];//percentage of mop1 in all months
        $calculos["porcentaje_mop1_pasa"] = $mop1_data[1];//true or false for pass
        $calculos["mop1_report"] = $mop1_data[2];//to show in crm (how result was obtained)

        //========================== MOP 2 =======================================
        $cuentas_mop2_data = self::sCalcGetMop2Count($HawkResponse_arr['cuentas']);
        $calculos["mop2"] = $cuentas_mop2_data[0];
        $calculos["cuentas_con_mop2_debug"] = $cuentas_mop2_data[1];
        $calculos["mop2_pasa"] = $cuentas_mop2_data[2];

        //========================== MOP 3 =======================================
        $cuentas_mop3_data = self::sCalcGetMop3Count($HawkResponse_arr['cuentas']);
        $calculos['mop3'] = $cuentas_mop3_data[0];
        $calculos["cuentas_con_mop3_debug"] = $cuentas_mop3_data[1];
        $calculos["mop3_pasa"] = $cuentas_mop3_data[2];

        //========= Fechas de Integracion y Apertura Linea Más Antigua ===========
        $calculos["fecha_integracion"] = self::sCalcGetFechaIntegracion($HawkResponse_arr);
        $calculos["fecha_apertura_linea_mas_antigua"] = self::sCalcGetFechaAperturaAntig($HawkResponse_arr);

        //========== Claves de Observacion/Actividad =============================
        $all_claves_arr = self::sCalcGetClavesActividad($HawkResponse_arr['cuentas']);
        $calculos["claves_de_actividad"] = $all_claves_arr[0];
        $calculos["claves_de_fraude"] = $all_claves_arr[1];
        $calculos["claves_de_extravio"] = $all_claves_arr[2];

        //=========== Cuentas Hipo, Auto, and Consumo ============================
        $cuentas_filtered_product = self::sCalcFilterCuentasProductos($HawkResponse_arr['cuentas']);
        $calculos['calc_total_cuentas'] = count($HawkResponse_arr['cuentas']);
        $calculos['num_cuentas_hipoteca'] = count($cuentas_filtered_product[0]);
        $calculos['num_cuentas_automotriz'] = count($cuentas_filtered_product[1]);
        $calculos['num_cuentas_consumo'] = count($cuentas_filtered_product[2]);

        //=========== Cuentas by duration and open and closed accounts ============
        $cuentas_filtered_duration = self::sCalcFilterCuentasDuration($HawkResponse_arr['cuentas']);
        $calculos["cuentas_con_duracion"] = count($cuentas_filtered_duration[0]);//array of accounts with duration of more than 6 months
        $calculos["cuentas_cerradas"] = $cuentas_filtered_duration[1];//count of closed accounts
        $calculos["cuentas_abiertas"] = $cuentas_filtered_duration[2];//count of open accounts

        //========================== IS HIT (Thin or Thick File) =====================
        //HIT significa que tiene al menos una cuenta abierta o cerrada con duración de mínimo 6 meses que no sea de comunicaciones ni servicios
        $not_com_servs = self::sCalcGetNotComServs($cuentas_filtered_duration[0]);
        $calculos['is_hit'] = ($not_com_servs)? true : false;//if any of the accounts lasted at least 6 months and is not of type Services or Comunications

        //================= PAGO MENSUAL TOTAL DE DEUDA BC =====================================
        $pago_mensual_data = self::sCalcGetPagoMensualTotalDeuda($HawkResponse_arr['cuentas']);
        $calculos["pago_mensual_total_de_deuda"]= $pago_mensual_data[0];
        $calculos["pago_mensual_debug"] = $pago_mensual_data[1];

        //=====================PAGO MENSUAL TOTAL HIPOTECARIO Y AUTOMOTRIZ ===================
        $calculos["pago_mensual_total_hipotec_automotriz"] = self::sCalcGetPagoMensualHipoAuto($cuentas_filtered_product[0], $cuentas_filtered_product[1]);

        //=============== PAGO MENSUAL TOTAL DEUDA LINEAS DE CONSUMO ================
        $calculos["pago_mensual_total_deuda_lineas_de_consumo"] = $calculos["pago_mensual_total_de_deuda"] - $calculos["pago_mensual_total_hipotec_automotriz"];

        //======================= CONSULTAS =======================================
        $consultas_data = self::sCalcGetConsultasData($HawkResponse_arr['consultas']);
        $calculos['consultas_count'] = $consultas_data[0];
        $calculos['consultas_count_pasa'] = $consultas_data[1];
        $calculos['fecha_consulta_mas_reciente'] = $consultas_data[2];
        $calculos['meses_desde_consulta_mas_reciente'] = number_format($consultas_data[3], 2);

        //save the calclulos values to their respective solic columns
        $solic->calc_consultas                    = $calculos["consultas_count"];
        $solic->peor_mop                          = $calculos["peor_mop"];
        $solic->fecha_peor_mop                    = $calculos["fecha_peor_mop"];
        $solic->num_meses_desde_peor_mop          = $calculos["num_meses_desde_peor_mop"];
        $solic->calc_mop2                         = $calculos["mop2"];
        $solic->calc_mop3                         = $calculos["mop3"];
        $solic->fecha_integracion                 = $calculos["fecha_integracion"];
        $solic->fecha_apertura_linea_mas_antigua  = $calculos["fecha_apertura_linea_mas_antigua"];
        $solic->calc_is_hit                       = $calculos["is_hit"];
        $solic->calc_total_cuentas                = $calculos['calc_total_cuentas'];
        $solic->calc_cuentas_hipo                 = $calculos['num_cuentas_hipoteca'];
        $solic->calc_cuentas_auto                 = $calculos['num_cuentas_automotriz'];
        $solic->calc_cuentas_consumo              = $calculos['num_cuentas_consumo'];
        $solic->calc_cuentas_4meses_plus          = $calculos["cuentas_con_duracion"];
        $solic->calc_cuentas_cerradas             = $calculos["cuentas_cerradas"];
        $solic->calc_cuentas_abiertas             = $calculos["cuentas_abiertas"];
        $solic->fecha_consulta_mas_reciente       = $calculos["fecha_consulta_mas_reciente"];
        $solic->meses_desde_consulta_mas_reciente = $calculos["meses_desde_consulta_mas_reciente"];
        $solic->claves_actividad_reportadas       = implode(',', $calculos["claves_de_actividad"]);
        $solic->claves_fraude_reportadas          = count($calculos["claves_de_fraude"]);
        $solic->claves_extravio_reportadas        = count($calculos["claves_de_extravio"]);

        $mnsj_str = '{
      "stat" : "Calculos 1 OK",
      "prospect_id" : '.$solic->prospect_id.',
      "solic_id" : '.$solic->id.',
      "message" : "Los primeros cálculos se ejecutaron con éxito. Solicitar datos adicionales.",
      "calculos" : {
        "peor_mop"                     : "'.$calculos['peor_mop'].'",
        "fecha_peor_mop"               : "'.$calculos['fecha_peor_mop'].'",
        "num_meses_desde_peor_mop"     : "'.$calculos['num_meses_desde_peor_mop'].'",
        "porcentaje_mop1_a_meses"      : "'.$calculos["porcentaje_mop1_a_meses"].'",
        "porcentaje_mop1_pasa"         : "'.$calculos["porcentaje_mop1_pasa"].'",
        "mop2"                         : "'.$calculos["mop2"].'",
        "mop2_pasa"                    : "'.$calculos["mop2_pasa"].'",
        "mop3"                         : "'.$calculos['mop3'].'",
        "mop3_pasa"                    : "'.$calculos["mop3_pasa"].'",
        "fecha_integracion"            : "'.$calculos["fecha_integracion"].'",
        "fecha_apertura_linea_mas_antigua"  : "'.$calculos["fecha_apertura_linea_mas_antigua"].'",
        "claves_de_actividad"          : "'.implode(",", $calculos["claves_de_actividad"]).'",
        "claves_de_fraude"             : "'.implode(",", $calculos["claves_de_fraude"]).'",
        "claves_de_extravio"           : "'.implode(",", $calculos["claves_de_extravio"]).'",
        "calc_total_cuentas"           : "'.$calculos['calc_total_cuentas'].'",
        "num_cuentas_hipoteca"         : "'.$calculos['num_cuentas_hipoteca'].'",
        "num_cuentas_automotriz"       : "'.$calculos['num_cuentas_automotriz'].'",
        "num_cuentas_consumo"          : "'.$calculos['num_cuentas_consumo'].'",
        "cuentas_con_duracion"         : "'.$calculos["cuentas_con_duracion"].'",
        "cuentas_cerradas"             : "'.$calculos["cuentas_cerradas"].'",
        "cuentas_abiertas"             : "'.$calculos["cuentas_abiertas"].'",
        "is_hit"                       : "'.$calculos['is_hit'].'",
        "pago_mensual_total_de_deuda"  : "'.$calculos["pago_mensual_total_de_deuda"].'",
        "pago_mensual_total_hipotec_automotriz"       : "'.$calculos["pago_mensual_total_hipotec_automotriz"].'",
        "pago_mensual_total_deuda_lineas_de_consumo"  : "'.$calculos["pago_mensual_total_deuda_lineas_de_consumo"].'",
        "consultas_count"                             : "'.$calculos['consultas_count'].'",
        "consultas_count_pasa"                        : "'.$calculos['consultas_count_pasa'].'",
        "fecha_consulta_mas_reciente"                 : "'.$calculos['fecha_consulta_mas_reciente'].'",
        "meses_desde_consulta_mas_reciente"           : "'.$calculos['meses_desde_consulta_mas_reciente'].'"
      }
    }';
        return [$solic, $calculos, $mnsj_str];
    }


    public function sCalcRunCalcs2($solic)
    {
        $report_resp_object = json_decode($solic->report_response);
        $cuentas_arr = (array) $report_resp_object->cuentas;
        $calculos = (array) $report_resp_object->calculos;

        //clean values for solic_ingreso_mensual and solic_gastos_mensuales (remove $ sign)
        $calculos["solic_ingreso_mensual"]      =   self::dineroClean($solic->ingreso_mensual, true);
        $calculos["solic_gastos_mensuales"]     =   self::dineroClean($solic->gastos_familiares, true);

        //get values for new calcs from old results (to make life easier below)
        $solic_ingreso_mensual  = $calculos["solic_ingreso_mensual"];
        $solic_gastos_mensuales = $calculos["solic_gastos_mensuales"];
        $pago_mensual_total_de_deuda = $calculos["pago_mensual_total_de_deuda"];
        $pago_mensual_total_deuda_lineas_de_consumo = $calculos["pago_mensual_total_deuda_lineas_de_consumo"];

        //======================== DEUDA A INGRESO ==================================
        $calculos["deuda_a_ingreso"] = ($pago_mensual_total_de_deuda / $solic_ingreso_mensual) * 100;
        $calculos["deuda_a_ingreso_pasa"] = ($calculos["deuda_a_ingreso"] <= 50)? true : false;

        //======================== DEUDA A INGRESO SOLO CONSUMO ======================
        $calculos["deuda_a_ingreso_solo_consumo"] = ($pago_mensual_total_deuda_lineas_de_consumo / $solic_ingreso_mensual) * 100;
        $calculos["deuda_a_ingreso_solo_consumo_pasa"] = ($calculos["deuda_a_ingreso_solo_consumo"] <= 35)? true : false;

        //======================== INGRESO DISPONIBLE ================================
        $calculos["ingreso_disponible"] = ((($solic_ingreso_mensual - $pago_mensual_total_de_deuda) - $solic_gastos_mensuales) / $solic_ingreso_mensual) * 100;

        //========================== USO DE LINEAS ====================================
        $uso_lineas_data = self::sCalcGetUsoLineas($cuentas_arr, $solic_ingreso_mensual);
        $calculos["uso_de_lineas"] = $uso_lineas_data[0];
        $calculos["limite_de_credito"] = $uso_lineas_data[1];
        $calculos["saldo_actual"] = $uso_lineas_data[2];
        $calculos["ingreso_neto_cuatro"] = $uso_lineas_data[3];
        $calculos["uso_de_lineas_razon"] = $uso_lineas_data[4];
        $calculos["uso_de_lineas_pasa"] = $uso_lineas_data[5];

        //======================= ABILITY TO PAY (ATP) ===============================
        $calculos["monto_pago_prestanomico"] = $solic->pago_estimado;
        //check if plazo is quincenal and adjust monto_pago_prestanomico if so
        if (in_array($solic->plazo, [3,4,5])) {
            $calculos["monto_pago_prestanomico"] = $solic->pago_estimado * 2;
        }
        $atp_data = self::sCalcGetAtpData($calculos["monto_pago_prestanomico"], $pago_mensual_total_de_deuda, $solic_gastos_mensuales, $solic_ingreso_mensual);
        $calculos["gasto_familiar_total_de_deuda"] = $atp_data[0];
        $calculos["mayor_gastos_familiar"] = $atp_data[1];
        $calculos["ability_to_pay"] = $atp_data[2];
        $calculos["ability_to_pay_pasa"] = $atp_data[3];

        //update the solics report_response
        $report_resp_object->calculos = $calculos;
        $solic->report_response = json_encode($report_resp_object);

        //save the values to their respective columns in the solic
        $solic->calc_deuda_ingr = $calculos["deuda_a_ingreso"];
        $solic->calc_deuda_ingr_cons = $calculos["deuda_a_ingreso_solo_consumo"];
        $solic->calc_ingr_disp = $calculos["ingreso_disponible"];
        $solic->calc_uso_lineas = $calculos["uso_de_lineas"];
        $solic->calc_atp = $calculos["ability_to_pay"];
        $solic->calc_ingr_neto_mens = $calculos["solic_ingreso_mensual"];
        $solic->calc_pago_mens_deuda = $calculos["pago_mensual_total_de_deuda"];
        $solic->calc_pago_mens_deuda_cons = $calculos["pago_mensual_total_deuda_lineas_de_consumo"];
        $solic->calc_gasto_fam_est = $calculos["solic_gastos_mensuales"];

        $mnsj_str = '{
      "stat" : "Calculos 2 OK",
      "prospect_id" : '.$solic->prospect_id.',
      "solic_id" : '.$solic->id.',
      "message" : "Los segundos cálculos se ejecutaron con éxito. (Iniciando llamada ALP...)",
      "calculos" : {
        "solic_ingreso_mensual"             : "'.$calculos["solic_ingreso_mensual"].'",
        "solic_gastos_mensuales"            : "'.$calculos["solic_gastos_mensuales"].'",
        "deuda_a_ingreso"                   : "'.$calculos["deuda_a_ingreso"].'",
        "deuda_a_ingreso_pasa"              : "'.$calculos["deuda_a_ingreso_pasa"].'",
        "deuda_a_ingreso_solo_consumo"      : "'.$calculos["deuda_a_ingreso_solo_consumo"].'",
        "deuda_a_ingreso_solo_consumo_pasa" : "'.$calculos["deuda_a_ingreso_solo_consumo_pasa"].'",
        "ingreso_disponible"                : "'.$calculos["ingreso_disponible"].'",
        "uso_de_lineas"                     : "'.$calculos["uso_de_lineas"].'",
        "limite_de_credito"                 : "'.$calculos["limite_de_credito"].'",
        "saldo_actual"                      : "'.$calculos["saldo_actual"].'",
        "ingreso_neto_cuatro"               : "'.$calculos["ingreso_neto_cuatro"].'",
        "uso_de_lineas_razon"               : "'.$calculos["uso_de_lineas_razon"].'",
        "uso_de_lineas_pasa"                : "'.$calculos["uso_de_lineas_pasa"].'",
        "monto_pago_prestanomico"           : "'.$calculos["monto_pago_prestanomico"].'",
        "gasto_familiar_total_de_deuda"     : "'.$calculos["gasto_familiar_total_de_deuda"].'",
        "mayor_gastos_familiar"             : "'.$calculos["mayor_gastos_familiar"].'",
        "ability_to_pay"                    : "'.$calculos["ability_to_pay"].'",
        "ability_to_pay_pasa"               : "'.$calculos["ability_to_pay_pasa"].'"
      }
    }';

        return [$solic, $calculos, $mnsj_str];
    }
}
?>
