<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\Jobs\AltaSolicitud;
use App\Notifications\AltaClienteNotification;

class AltaCliente implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Alta de Cliente T24");
        Log::info(env('WSDL_T24'));
        $datosCliente = $this->clienteAlta->toArray();
        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout' => 10,
                    'default_socket_timeout' => 10
                ]);
        });

        $response = $soapWrapper->call('T24.CustomerInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company' => env('COMPANY_T24'),
                    'password' => env('PASSWORD_T24'),
                    'userName' => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'CUSTOMERPRSTINPUTWSType' => [
                    'MNEMONIC' => $datosCliente['MNEMONIC'],
                    'gSHORTNAME' => [
                        'SHORTNAME' => $datosCliente['SHORTNAME']
                    ],
                    'gNAME1' => [
                        'NAME1' => $datosCliente['NAME1']
                    ],
                    'gNAME2' => [
                        'NAME2' => $datosCliente['NAME2']
                    ],
                    'gSTREET' => [
                        'STREET' => $datosCliente['STREET']
                    ],
                    'BIRTHINCORPDATE' => $datosCliente['fecha_alta'],
                    'CUSTOMERTYPE' => 'ACTIVE',
                    'NOOFDEPEND' => $datosCliente['NOOFDEPEND'],
                    'FORMERNAME' => $datosCliente['FORMERNAME'],
                    'MARITALSTSNC' => $datosCliente['MARITALSTSNC'],
                    'GENDERNC' => $datosCliente['GENDERNC'],
                    'MAININCOME' => $datosCliente['MAININCOME'],
                    'FECNACIMIENTO' => $datosCliente['FECNACIMIENTO'],
                    'LUGNAC' => $datosCliente['LUGNAC'],
                    'RFCCTE' => $datosCliente['RFCCTE'],
                    'DIRNUMEXT' => $datosCliente['DIRNUMEXT'],
                    'DIRNUMINT' => $datosCliente['DIRNUMINT'],
                    'DIRCOLONIA' => $datosCliente['DIRCOLONIA'],
                    'DIRDELMUNI' => $datosCliente['DIRDELMUNI'],
                    'DIRCODPOS' => $datosCliente['DIRCODPOS'],
                    'DIRCDEDO' => $datosCliente['DIRCDEDO'],
                    'DIRPAIS' => 'MX',
                    'TIPODOM' => $datosCliente['TIPODOM'],
                    'DOMANOS' => $datosCliente['DOMANOS'],
                    'gTEL.DOM' => [
                        'TELDOM' => $datosCliente['TELDOM']
                    ],
                    'gTEL.OFI' => [
                        'TELOFI' => $datosCliente['TELOFI']
                    ],
                    'gTEL.CEL' => [
                        'TELCEL' => $datosCliente['TELCEL']
                    ],
                    'EMAIL' => $datosCliente['EMAIL'],
                    'ESTUDIOS' => $datosCliente['ESTUDIOS'],
                    'OCUPACION' => $datosCliente['OCUPACION'],
                    'VALCURP' => $datosCliente['VALCURP'],
                    'DIRCIUDAD' => $datosCliente['DIRCIUDAD'],
                    'EGRORDMEN' => $datosCliente['EGRORDMEN'],
                    'TOTING' => $datosCliente['prospect_id']
                ]
            ]
        ]);

        $idAlta = $datosCliente['id'];
        Log::info('Status alta:'. $response->Status->successIndicator);
        if ($response) {

            if (isset($response->Status)) {
                if ($response->Status->successIndicator == 'Success') {

                    $idT24 = $response->CUSTOMERType->id;
                    Log::info('Alta de cliente exitosa: '.$idT24);
                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_cliente' => 1,
                        'no_cliente_t24' => $idT24
                    ]);

                    $this->clienteAlta = ClientesAlta::find($idAlta);
                    $job = (new AltaSolicitud($this->clienteAlta))->delay(30);
                    dispatch($job);

                } else {

                    $errores = $response->Status->messages;
                    $msg = '';

                    if (is_array($errores)) {
                        $msg = implode(", ", $errores);
                    } else {
                        $msg = $errores;
                    }

                    $error = $datosCliente['error'];
                    if ($error != '') {
                        $msg = $error.chr(13).'<br/>'.$msg;
                    }

                    $clientesAlta = ClientesAlta::where('id', $idAlta)->update([
                        'alta_cliente' => 0,
                        'error' => $msg
                    ]);

                    $notificacion = new \stdClass;
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospect_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitation_id;
                    $notificacion->content = $msg;
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

                }
            }
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

        $datosCliente = $this->clienteAlta->toArray();

        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];
        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        ClientesAlta::where('id', $idAlta)->update([
            'alta_cliente' => 0,
            'error' => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospect_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitation_id;
        $notificacion->content = $exception->getMessage();

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
