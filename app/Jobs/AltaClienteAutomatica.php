<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\ClienteT24;
use App\StatusT24;
use App\DomicilioSolicitud;
use App\W_USER;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\Notifications\AltaClienteNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use JWTFactory;
use JWTAuth;

class AltaClienteAutomatica implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;
    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Alta de Cliente T24");

        $status_t24 = StatusT24::first();
        $cambio_fecha = false;

        if (date('Y-m-d H:i:s') >= date('Y-m-d 03:00:00') && $status_t24->fecha_alta == null) {
            $cambio_fecha = true;
            $fecha_alta = date('Ymd');
        } else {
            $status = $status_t24->toArray();
            $fecha = $status_t24['fecha_alta'];
            $fecha_alta = $status[$fecha];
        }

        $datosCliente = $this->clienteAlta->toArray();
        $datosCliente['fecha_alta'] = $fecha_alta;

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        $curl = curl_init(env('ALTA_CLIENTE_T24'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datosCliente));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Bearer '.$token
            )
        );

        $response = curl_exec($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $response = json_decode($response);
        curl_close($curl);

        $idAlta = $datosCliente['id'];

        if ($response) {

            if (isset($response->Status)) {

                if ($response->Status->successIndicator == 'Success') {

                    $idT24 = $response->CUSTOMERType->id;

                    Log::info('Alta de cliente exitosa: '.$idT24);

                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_cliente'      => 1,
                        'alta_cliente_at'   => date('Y-m-d H:i:s'),
                        'fecha_alta'        => $fecha_alta,
                        'no_cliente_t24'    => $idT24
                    ]);

                    W_USER::where('ID_USER', mb_strtolower($datosCliente['EMAIL']))
                        ->update(['USER_T24' => $idT24]);

                    ClienteT24::updateOrCreate(
                        [
                            'rfc' => $datosCliente['MNEMONIC'],
                        ],
                        [
                            'id_cliente_t24'    => $idT24,
                            'prefijo'           => 'FMP',
                            'apellido_paterno'  => $datosCliente['SHORTNAME'],
                            'apellido_materno'  => $datosCliente['NAME1'],
                            'nombre'            => $datosCliente['NAME2'],
                            'segundo_nombre'    => $datosCliente['FORMERNAME'],
                            'email'             => $datosCliente['EMAIL'],
                            'rfc_homoclave'     => $datosCliente['RFCCTE'],
                            'curp'              => $datosCliente['VALCURP'],
                            'fecha_nacimiento'  => $datosCliente['FECNACIMIENTO'],
                            'id_panel'          => $datosCliente['prospecto_id']
                        ]
                    );

                    if ($cambio_fecha == true) {
                        $status_t24->fecha_alta = 'fecha_actual';
                        $status_t24->fecha_t24 = date('Ymd');
                        $status_t24->save();
                    }

                    $notificacion = new \stdClass;
                    $notificacion->type = 'success';
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                    $notificacion->content = 'Alta de cliente exitosa';
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

                } else {

                    $errores = $response->Status->messages;
                    $msg = '';

                    if (is_array($errores)) {
                        $msg = implode(", ", $errores);
                    } else {
                        $msg = $errores;
                    }

                    if (strpos($msg, 'DIR.COLONIA:1:1=ID IN FILE MISSING') !== false || strpos($msg, 'DIR.COLONIA:1:1=ST.RTN.ID.FILE.MISS, DIR.COLONIA:1:1=ST.RTN.ID.FILE.MISS') !== false) {
                        $colonia = DomicilioSolicitud::select('colonia')
                            ->where('solicitud_id', $this->clienteAlta->solicitud_id)
                            ->first();
                        $msg = $msg.chr(13).'<br>'.'Colonia faltante: '.$this->clienteAlta->DIRCOLONIA.' - '. $colonia->colonia;
                    }

                    throw new Exception($msg);

                }

            } else {

                if (isset($response->status)) {
                    throw new Exception(chr(13).'<br>'.$response->status);
                } else {
                    throw new Exception(chr(13).'<br>'.'El servicio de alta en T24 respondio con un código de error:'. $status_code);
                }

            }

        } else {

            throw new Exception(chr(13).'<br>'.'No hubo respuesta por parte del servicio de alta en T24');

        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta  de cliente fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        if (strpos($exception->getMessage(), 'BIRTH.INCORP.DATE:1:1=DATE MUST BE <= TODAY') !== false) {
            $job = (new AltaClienteAutomatica($this->clienteAlta))->delay(Carbon::now()->addMinutes(30));
            dispatch($job);
        }

        ClientesAlta::where('id', $idAlta)->update([
            'alta_cliente'      => 0,
            'alta_cliente_at'   => date('Y-m-d H:i:s'),
            'error'             => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Alta de cliente erronea: '.chr(13).$exception->getMessage());

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
