<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\ClientesAlta;
use App\CuentaClabeSolicitud;
use App\W_LOAN_USER_ACCOUNT;
use App\Solicitud;
use App\Notifications\AltaClienteNotification;
use Carbon\Carbon;
use App\Repositories\CurlCaller;
use App\Notifications\CardReporte;

class AltaCuentaClabe implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $curl;
    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Cuenta Clabe");

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $no_solicitud_t24 = $datosCliente['no_solicitud_t24'];
        $nombreCliente = $datosCliente['NAME2'];
        $solicitud_id = $datosCliente['solicitud_id'];

        if ($datosCliente['FORMERNAME'] != '') {
            $nombreCliente = $nombreCliente.' '.$datosCliente['FORMERNAME'];
        }
        $nombreCompletoCliente = $nombreCliente.' '.$datosCliente['SHORTNAME'].' '.$datosCliente['NAME1'];

        $datosCuenta = CuentaClabeSolicitud::where('solicitud_id', $datosCliente['solicitud_id'])
            ->first()
            ->toArray();

        if ($no_solicitud_t24 !== null) {

            $loanUserAccount = W_LOAN_USER_ACCOUNT::updateOrCreate([
                'ID_LOAN_APP'           => $no_solicitud_t24,
                'ID_ACCOUNT_TYPE'       => 1
            ], [
                'ID_BANK_ACCOUNT_TYPE'  => 1,
                'NAME'                  => $nombreCompletoCliente,
                'ACCOUNT'               => $datosCuenta['clabe_interbancaria'],
                'BANK_NAME'             => $datosCuenta['banco'],
                'BANK_KEY'              => $datosCuenta['banco_id'],
                'CREATE_DATE'           => Carbon::now(),
                'UPDATE_DATE'           => Carbon::now(),
            ]);

            ClientesAlta::where('id', $idAlta)->update([
                'alta_cuenta_clabe'     => 1,
                'alta_cuenta_clabe_at'  => date('Y-m-d H:i:s'),
                'cuenta_clabe_error'    => null
            ]);

            // Mandamos la actualización al panel operativo
            $datos = [
                'd' => 'cta',
                'i' => $no_solicitud_t24,
            ];
            $this->curl->syncWebapp($datos);

            $notificacion_slack = env('NOTIFICACION_SLACK', false);
            $notificacion_teams = env('NOTIFICACION_TEAMS', false);

            if ($notificacion_slack == true) {
                $notificacion = new \stdClass;
                $notificacion->type = 'success_clabe';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Alta Cuenta CLABE exitosa';
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
            }

            if ($notificacion_teams == true) {

                $solicitud = Solicitud::with('producto')
                    ->where('id', $solicitud_id)
                    ->get()
                    ->toArray();

                $nombreProducto = '';
                if (count($solicitud[0]['producto']) == 1) {
                    $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
                } else {
                    $nombreProducto = 'Mercado Abierto';
                }

                $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_CUENTA_CLABE_WEBHOOK'));
                $card = new CardReporte([
                    'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                    'subtitle'  => 'Cuenta CLABE',
                    'text'      => 'Alta Cuenta CLABE exitosa',
                    'producto'  => $nombreProducto,
                ]);
                $connector->send($card);
            }

        } else {
            throw new Exception("La solicitud en T24 aún no existe");
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta Cuenta CLABE fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['cuenta_clabe_error'];

        ClientesAlta::where('id', $idAlta)->update([
            'alta_cuenta_clabe'     => 0,
            'alta_cuenta_clabe_at'  => date('Y-m-d H:i:s'),
            'cuenta_clabe_error'    => $error.PHP_EOL.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Alta Cuenta CLABE erronea: '.PHP_EOL.$exception->getMessage());
        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }

}
