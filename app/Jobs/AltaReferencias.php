<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\ClientesAlta;
use App\ReferenciasSolicitud;
use App\W_REFERENCE;
use App\Notifications\AltaClienteNotification;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use App\Solicitud;
use App;
use App\Notifications\CardReporte;

class AltaReferencias implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $curl;
    protected $configuracionProducto;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Alta de Referencias");

        $datosCliente = $this->clienteAlta->toArray();
        $no_solicitud_t24 = $datosCliente['no_solicitud_t24'];
        $no_cliente_t24 = $datosCliente['no_cliente_t24'];
        $idAlta = $datosCliente['id'];

        $nombreProspecto = $datosCliente['NAME2'].' '.$datosCliente['SHORTNAME'];
        $solicitud_id = $datosCliente['solicitud_id'];
        $prospecto_id = $datosCliente['prospecto_id'];

        $producto = null;
        $version = null;

        $solicitud = Solicitud::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

        $nombreProducto = '';
        if (count($solicitud[0]['producto']) == 1) {
            $producto = $solicitud[0]['producto'][0]['id'];
            $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
        } else {
            $nombreProducto = 'Mercado Abierto';
        }

        $configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );

        $datosReferencia = ReferenciasSolicitud::where('solicitud_id', $datosCliente['solicitud_id'])
            ->first();

        if ($no_solicitud_t24 !== null) {

            $nombreReferencia1 = $datosReferencia['primer_nombre_ref1'];
            if ($datosReferencia['segundo_nombre_ref1'] != '') {
                $nombreReferencia1 .= ' '.$datosReferencia['segundo_nombre_ref1'];
            }
            $apellidosReferencia1 = $datosReferencia['apellido_paterno_ref1'];

            $nombreReferencia2 = $datosReferencia['primer_nombre_ref2'];
            if ($datosReferencia['segundo_nombre_ref2'] != '') {
                $nombreReferencia2 .= ' '.$datosReferencia['segundo_nombre_ref2'];
            }
            $apellidosReferencia2 = $datosReferencia['apellido_paterno_ref2'];

            $nombreReferencia3 = $datosReferencia['primer_nombre_ref3'];
            if ($datosReferencia['segundo_nombre_ref3'] != '') {
                $nombreReferencia3 .= ' '.$datosReferencia['segundo_nombre_ref3'];
            }
            $apellidosReferencia3 = $datosReferencia['apellido_paterno_ref3'];

            $reference1 = W_REFERENCE::updateOrCreate([
                'CONSECUTIVE'               => 1,
                'ID_LOAN_APP'               => $no_solicitud_t24,
            ], [
                'ID_CUSTOMER_RELATION_TYPE' => $datosReferencia['tipo_relacion_ref1'],
                'NAME'                      => $nombreReferencia1,
                'LAST_NAME'                 => $apellidosReferencia1,
                'PHONE_NUMBER'              => $datosReferencia['telefono_ref1'],
                'ID_T24'                    => 1,
                'CREATE_DATE'               => Carbon::now(),
                'UPDATE_DATE'               => Carbon::now(),
            ]);

            $reference2 = W_REFERENCE::updateOrCreate([
                'CONSECUTIVE'               => 2,
                'ID_LOAN_APP'               => $no_solicitud_t24,
            ], [
                'ID_CUSTOMER_RELATION_TYPE' => $datosReferencia['tipo_relacion_ref2'],
                'NAME'                      => $nombreReferencia2,
                'LAST_NAME'                 => $apellidosReferencia2,
                'PHONE_NUMBER'              => $datosReferencia['telefono_ref2'],
                'ID_T24'                    => 2,
                'CREATE_DATE'               => Carbon::now(),
                'UPDATE_DATE'               => Carbon::now(),
            ]);

            $reference3 = W_REFERENCE::updateOrCreate([
                'CONSECUTIVE'               => 3,
                'ID_LOAN_APP'               => $no_solicitud_t24,
            ], [
                'ID_CUSTOMER_RELATION_TYPE' => $datosReferencia['tipo_relacion_ref3'],
                'NAME'                      => $nombreReferencia3,
                'LAST_NAME'                 => $apellidosReferencia3,
                'PHONE_NUMBER'              => $datosReferencia['telefono_ref3'],
                'ID_T24'                    => 3,
                'CREATE_DATE'               => Carbon::now(),
                'UPDATE_DATE'               => Carbon::now(),
            ]);

            $envioReferencia = null;

            if ($configuracionProducto['sms_referencias'] == true && App::environment(['local']) == false) {
                $nombreReferencia = $nombreReferencia1.' '.$apellidosReferencia1;
                $envioReferencia = 'Envio SMS Referencia 1: '.$this->envioSMS($solicitud_id, $nombreReferencia, $datosReferencia['telefono_ref1'], $nombreProspecto);
                $nombreReferencia = $nombreReferencia2.' '.$apellidosReferencia2;
                $envioReferencia .= PHP_EOL.'Envio SMS Referencia 2: '.$this->envioSMS($solicitud_id, $nombreReferencia, $datosReferencia['telefono_ref2'], $nombreProspecto);
                $nombreReferencia = $nombreReferencia3.' '.$apellidosReferencia3;
                $envioReferencia .= PHP_EOL.'Envio SMS Referencia 3: '.$this->envioSMS($solicitud_id, $nombreReferencia, $datosReferencia['telefono_ref3'], $nombreProspecto);
            }

            /*
            $soapWrapper = new SoapWrapper();
            $soapWrapper->add('T24', function ($service) {
                $service->wsdl(env('WSDL_T24'))
                    ->trace(true)
                    ->options([
                        'connection_timeout'        => 10,
                        'default_socket_timeout'    => 10
                    ]);
            });

            $response = $soapWrapper->call('T24.CustomerInput', [
                'body' => [
                    'WebRequestCommon' => [
                        'company'   => env('COMPANY_T24'),
                        'password'  => env('PASSWORD_T24'),
                        'userName'  => env('USERNAME_T24')
                    ],
                    'OfsFunction' => [

                    ],
                    'CUSTOMERPRSTINPUTWSType' => [
                        'id'                => $no_cliente_t24,
                        'gREF.P.APE.P'      => [
                            'mREF.P.APE.P'  => [
                                [
                                    'm'             => 1,
                                    'REFPAPEP'      => $datosReferencia['apellido_paterno_ref1'],
                                    'REFPAPEM'      => $datosReferencia['apellido_materno_ref1'],
                                    'REFPNOMBRE1'   => $nombreReferencia1,
                                    'REFPTEL'       => $datosReferencia['telefono_ref1'],
                                    'PAISREFPER'    => 'MX'
                                ],
                                [
                                    'm'             => 2,
                                    'REFPAPEP'      => $datosReferencia['apellido_paterno_ref2'],
                                    'REFPAPEM'      => $datosReferencia['apellido_materno_ref2'],
                                    'REFPNOMBRE1'   => $nombreReferencia2,
                                    'REFPTEL'       => $datosReferencia['telefono_ref2'],
                                    'PAISREFPER'    => 'MX'
                                ],
                                [
                                    'm'             => 3,
                                    'REFPAPEP'      => $datosReferencia['apellido_paterno_ref3'],
                                    'REFPAPEM'      => $datosReferencia['apellido_materno_ref3'],
                                    'REFPNOMBRE1'   => $nombreReferencia3,
                                    'REFPTEL'       => $datosReferencia['telefono_ref3'],
                                    'PAISREFPER'    => 'MX'
                                ]
                            ]
                        ],
                        'gREL.NOMBRE'       => [
                            'mREL.NOMBRE'   => [
                                [
                                    'm'             => 1,
                                    'RELNOMBRE'     => 'NA',
                                    'RELTIPOREL'    => $datosReferencia['tipo_relacion_ref1']
                                ],
                                [
                                    'm'             => 2,
                                    'RELNOMBRE'     => 'NA',
                                    'RELTIPOREL'    => $datosReferencia['tipo_relacion_ref2']
                                ],
                                [
                                    'm'             => 3,
                                    'RELNOMBRE'     => 'NA',
                                    'RELTIPOREL'    => $datosReferencia['tipo_relacion_ref3']
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
            */

            // Mandamos la actualización al panel operativo
            $datos = [
                'd' => 'refs',
                'i' => $no_solicitud_t24,
            ];
            $this->curl->syncWebapp($datos);

            ClientesAlta::where('id', $idAlta)->update([
                'alta_referencias'      => 1,
                'referencias_error'     => $envioReferencia,
                'alta_referencias_at'   => date('Y-m-d H:i:s'),
            ]);

            $notificacion_slack = env('NOTIFICACION_SLACK', false);
            $notificacion_teams = env('NOTIFICACION_TEAMS', false);

            if ($notificacion_slack == true) {
                $notificacion = new \stdClass;
                $notificacion->type = 'success';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Alta Referencias exitosa';
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
            }

            if ($notificacion_teams == true) {

                $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_REFERENCIAS_WEBHOOK'));
                $card  = new CardReporte([
                    'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                    'subtitle'  => 'Referencias',
                    'text'      => 'Alta Referencias exitosa',
                    'producto'  => $nombreProducto,
                ]);
                $connector->send($card);
            }

        } else {
            throw new Exception("La solicitud en T24 aún no existe");
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta Referencias fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['referencias_error'];

        ClientesAlta::where('id', $idAlta)->update([
            'alta_referencias'      => 0,
            'alta_referencias_at'   => date('Y-m-d H:i:s'),
            'referencias_error'     => $error.PHP_EOL.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Alta Referencias erronea: '.PHP_EOL.$exception->getMessage());
        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }

    private function envioSMS($solicitud_id, $nombreReferencia, $telReferencia, $nombreProspecto) {

        $data = [
            'celular'    => $telReferencia,
            'referencia' => $nombreReferencia,
            'prospecto'  => $nombreProspecto,
            'idivr'      => env('IDIVR_REFERENCIAS'),
            'auxiliar'   => 'MO-'.$solicitud_id.'-1'
        ];
        $url = env('SMS_REFERENCIAS_URL');
        $response = $this->curl->callCurlSMSReferencias($url, $data);
        $response = json_decode($response);
        if (isset($response->success)) {
            if ($response->success == true) {
                return $response->response_code.' - '.$response->response_descripcion;
            } else {
                return $response->message;
            }
        } else {
            return 'No se obtuvo respuesta del envio de SMS';
        }

    }

}
