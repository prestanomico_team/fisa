<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;

class AltaSolicitud implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {

        Log::info('Inicio de Alta de Solicitud T24');

        $datosCliente = $this->clienteAlta->toArray();;

        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout' => 10,
                    'default_socket_timeout' => 10
                ]);
        });

        $response = $soapWrapper->call('T24.LoanApplicationInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company' => env('COMPANY_T24'),
                    'password' => env('PASSWORD_T24'),
                    'userName' => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'GICLOANAPPLICATIONPRSTINPUTWSType' => [
                    'CLIENTID'        => $datosCliente['no_cliente_t24'],
                    'APPLICATIONDATE' => $datosCliente['fecha_alta'],
                    'LOANAMOUNT'      => $datosCliente['LOANAMOUNT'],
                    'TERM'            => $datosCliente['TERM'],
                    'LOANPURPOSE'     => $datosCliente['LOANPURPOSE'],
                    'CUREMPMTEXPNCYR' => $datosCliente['CUREMPMTEXPNCYR'],
                    'HLDMORTGAGE'     => $datosCliente['HLDMORTGAGE'],
                    'HLDAUTOLOAN'     => $datosCliente['HLDAUTOLOAN'],
                    'HLDTDC'          => $datosCliente['HLDTDC'],
                    'TDCCODE'         => $datosCliente['TDCCODE'],
                    'INCSNDSOURCE'    => $datosCliente['INCSNDSOURCE'],
                    'CURRENTSTATUS'   => 2,
                    'IDNUM'           => $datosCliente['solicitation_id']
                ]
            ]
        ]);

        $idAlta = $datosCliente['id'];

        if ($response) {

            if (isset($response->Status)) {
                if ($response->Status->successIndicator == 'Success') {

                    $idT24 = $response->GICLOANAPPLICATIONType->id;
                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_solicitud' => 1,
                        'no_solicitud_t24' => $idT24
                    ]);

                    Log::info('Alta de Solicitud exitosa: '.$idT24);

                    $this->clienteAlta = ClientesAlta::find($idAlta);
                    $job = (new LigueUsuario($this->clienteAlta))->delay(30);
                    dispatch($job);

                } else {

                    $errores = $response->Status->messages;
                    $msg = '';
                    if (is_array($errores)) {
                        $msg = implode(',', $errores);
                    } else {
                        $msg = $errores;
                    }

                    $error = $datosCliente['error'];
                    if ($error != '') {
                        $msg = $msg.$error.chr(13).'<br/>';
                    }

                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_solicitud' => 0,
                        'error' => $msg
                    ]);

                    Log::info('Alta de Solicitud fallida: '.$error);

                }
            }
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta de Solicitud fallida: '.$exception);
        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];
        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }
        ClientesAlta::where('id', $idAlta)->update([
            'alta_solicitud' => 0,
            'error' => $error.$exception->getMessage()
        ]);
    }
}
