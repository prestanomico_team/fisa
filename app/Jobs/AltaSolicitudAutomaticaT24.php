<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\StatusT24;
use App\Solicitud;
use Log;
use App;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\Notifications\AltaClienteNotification;
use Carbon\Carbon;
use App\LOAN_APPLICATION_SITIO;
use App\Repositories\PanelOperativoRepository;
use App\Repositories\CurlCaller;

class AltaSolicitudAutomaticaT24 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;
    protected $curl;
    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        if ($this->clienteAlta->aplica_facematch == 1) {
            $this->curl = new CurlCaller;
        }
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Inicio de Alta de Solicitud T24');

        $status_t24 = StatusT24::first();
        $cambio_fecha = false;

        if (date('Y-m-d H:i:s') >= date('Y-m-d 03:00:00') && $status_t24->fecha_alta == null) {
            $cambio_fecha = true;
            $fecha_alta = date('Ymd');
        } else {
            $status = $status_t24->toArray();
            $fecha = $status_t24['fecha_alta'];
            $fecha_alta = $status[$fecha];
        }

        $datosCliente = $this->clienteAlta->toArray();

        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout'        => 10,
                    'default_socket_timeout'    => 10
                ]);
        });

        $response = $soapWrapper->call('T24.LoanApplicationInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company'   => env('COMPANY_T24'),
                    'password'  => env('PASSWORD_T24'),
                    'userName'  => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'GICLOANAPPLICATIONPRSTINPUTWSType' => [
                    'CLIENTID'        => $datosCliente['no_cliente_t24'],
                    'APPLICATIONDATE' => $fecha_alta,
                    'LOANAMOUNT'      => $datosCliente['LOANAMOUNT'],
                    'TERM'            => $datosCliente['TERM'],
                    'LOANPURPOSE'     => $datosCliente['LOANPURPOSE'],
                    'CUREMPMTEXPNCYR' => $datosCliente['CUREMPMTEXPNCYR'],
                    'HLDMORTGAGE'     => $datosCliente['HLDMORTGAGE'],
                    'HLDAUTOLOAN'     => $datosCliente['HLDAUTOLOAN'],
                    'HLDTDC'          => $datosCliente['HLDTDC'],
                    'TDCCODE'         => $datosCliente['TDCCODE'],
                    'INCSNDSOURCE'    => $datosCliente['INCSNDSOURCE'],
                    'CURRENTSTATUS'   => 2,
                    'IDNUM'           => $datosCliente['solicitud_id'],
                    'IDTYPE'          => $datosCliente['IDTYPE'],
                ]
            ]
        ]);

        $idAlta = $datosCliente['id'];

        if ($response) {

            if (isset($response->Status)) {
                if ($response->Status->successIndicator == 'Success') {

                    $idT24 = $response->GICLOANAPPLICATIONType->id;
                    $producto = $datosCliente['IDTYPE'];
                    $nombreCliente = $datosCliente['NAME2'];
                    if ($datosCliente['FORMERNAME'] != '') {
                        $nombreCliente = $nombreCliente.' '.$datosCliente['FORMERNAME'];
                    }

                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_solicitud'    => 1,
                        'alta_solicitud_at' => date('Y-m-d H:i:s'),
                        'fecha_alta'        => $fecha_alta,
                        'no_solicitud_t24'  => $idT24
                    ]);

                    if ($cambio_fecha == true) {
                        $status_t24->fecha_alta = 'fecha_actual';
                        $status_t24->fecha_t24 = date('Ymd');
                        $status_t24->save();
                    }

                    LOAN_APPLICATION_SITIO::updateOrCreate([
                        'prospecto_id'  => $datosCliente['prospecto_id'],
                        'solicitud_id'  => $datosCliente['solicitud_id'],
                        'empresa'       => env('EMPRESASOLICITUD'),
                    ],[
                        'loan_id'       => $idT24
                    ]);

                    $solicitud = Solicitud::with('producto')
                        ->find($datosCliente['solicitud_id'])
                        ->toArray();
                    if (isset($solicitud['producto'][0]['pivot']['lead'])) {
                        if ($solicitud['producto'][0]['pivot']['lead'] == 'Cliente estrella') {
                            ClientesAlta::where('id', $idAlta)->update([
                                'aplica_email' => 0
                            ]);
                            $producto = 'CLIENTE ESTRELLA';
                        }
                    }
                    Log::info('Alta de Solicitud exitosa: '.$idT24);

                    try {
                        $notificacion = new \stdClass;
                        $notificacion->type = 'success';
                        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                        $notificacion->content = 'Alta de solicitud exitosa';
                        $notificacion->solicitud = true;
                        $notificacion->simplificado = $datosCliente['simplificado'];
                        $notificacion->celular = $datosCliente['TELCEL'];
                        $notificacion->nombre = $nombreCliente;
                        $notificacion->apellido_paterno = $datosCliente['SHORTNAME'];
                        $notificacion->apellido_materno = $datosCliente['NAME1'];
                        $notificacion->producto = $producto;
                        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
                    } catch (\Exception $e) {
                        ClientesAlta::where('id', $idAlta)->update([
                            'error' => $this->clienteAlta->error.PHP_EOL.$exception->getMessage()
                        ]);
                    }

                    if ($this->clienteAlta->panel_operativo_id === null) {
                        $panelOperativoRepository = new PanelOperativoRepository($this->curl);
                        print_r($panelOperativoRepository);
                        $id_clienteAlta = [
                            'idp' => $this->clienteAlta->id,
                        ];
                        $response = $panelOperativoRepository->forzarAlta($id_clienteAlta);

                        if ($response['success'] == true) {
                            if (isset($response['response']->idp[0])) {
                                $this->altaPanel = true;
                                ClientesAlta::where('id', $this->clienteAlta->id)->update([
                                    'panel_operativo_id' => $response['response']->idp[0],
                                ]);
                            }
                        } else {
                            $this->generaProcesos = false;
                            throw new Exception("No se pudo dar de alta en el Panel Operativo");
                        }
                    }

                } else {

                    $errores = $response->Status->messages;
                    $msg = '';
                    if (is_array($errores)) {
                        $msg = implode(',', $errores);
                    } else {
                        $msg = $errores;
                    }

                    throw new Exception($msg);

                }
            }
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta de Solicitud fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if (strpos($exception->getMessage(), 'APPLICATION.DATE:1:1=APPLICATION DATE CANNOT BE GREATER THAN TODAY') !== false
            || strpos($exception->getMessage(), 'INVALID FUNCTION FOR END.OF.DAY') !== false) {

            $job = (new AltaSolicitudAutomatica($this->clienteAlta))->delay(Carbon::now()->addMinutes(30));
            dispatch($job);

        } else {

            LOAN_APPLICATION_SITIO::updateOrCreate([
                'prospecto_id'  => $datosCliente['prospecto_id'],
                'solicitud_id'  => $datosCliente['solicitud_id'],
                'empresa'       => env('EMPRESASOLICITUD'),
            ],[
                'loan_id'       => $datosCliente['no_solicitud_t24']
            ]);

        }

        ClientesAlta::where('id', $idAlta)->update([
            'alta_solicitud'    => 0,
            'alta_solicitud_at' => date('Y-m-d H:i:s'),
            'error'             => $error.PHP_EOL.$exception->getMessage()
        ]);

        try {
            $notificacion = new \stdClass;
            $notificacion->type = 'error';
            $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
            $notificacion->content = strip_tags('Alta de solicitud erronea: '.chr(13).$exception->getMessage());
            $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
        } catch (\Exception $e) {
            ClientesAlta::where('id', $idAlta)->update([
                'error' => $error.PHP_EOL.$exception->getMessage()
            ]);
        }

    }
}
