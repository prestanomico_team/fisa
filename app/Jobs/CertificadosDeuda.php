<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\ClientesAlta;
use App\Notifications\AltaClienteNotification;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use App\Solicitud;
use App\CargaDocumentos;
use App;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use App\Notifications\CardReporte;
use App\Jobs\EnvioEmail_DocumentosCompletos;

class CertificadosDeuda implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $curl;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Certificados de deuda");

        $datosCliente = $this->clienteAlta->toArray();
        $no_solicitud_t24 = $datosCliente['no_solicitud_t24'];
        $no_cliente_t24 = $datosCliente['no_cliente_t24'];
        $idAlta = $datosCliente['id'];

        $email = $datosCliente['EMAIL'];
        $solicitud_id = $datosCliente['solicitud_id'];
        $prospecto_id = $datosCliente['prospecto_id'];

        $producto = null;
        $version = null;

        $solicitud = Solicitud::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

        $nombreProducto = '';
        if (count($solicitud[0]['producto']) == 1) {
            $producto = $solicitud[0]['producto'][0]['id'];
            $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
        } else {
            $nombreProducto = 'Mercado Abierto';
        }

        if ($no_solicitud_t24 !== null) {

            $certificados = CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->first();

            if (isset($certificados->numero_certificados_deuda)) {
                Log::info($certificados->numero_certificados_deuda);
                $numero_certificados = $certificados->numero_certificados_deuda;
                
                $tipo_documento = 'anexo-cd';
                $root = 'proceso_simplificado';
                $documentos = [];

                for ($i=1; $i <= $numero_certificados; $i++) {
                    
                    $id = str_pad($i, 2, "0", STR_PAD_LEFT);
                    if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}-{$id}.jpg")) {
                        $extension = '.jpg';
                    } elseif (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}-{$id}.pdf")) {
                        $extension = '.pdf';
                    }

                    $documentos[] = [
                        'nombre_documento'      => $tipo_documento.'-'.$id,
                        'formato_documento'     => $extension
                    ];

                }

                $tipo_documento = 'anexo-solcred';
                
                if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}.jpg")) {
                    $extension = '.jpg';
                } elseif (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}.pdf")) {
                    $extension = '.pdf';
                }

                $documentos[] = [
                    'nombre_documento'      => $tipo_documento,
                    'formato_documento'     => $extension
                ];

                $datosDocumentos = [
                    'no_solcitud_t24'   => $no_solicitud_t24,
                    'email'             => $email,
                    'ruta_local'        => "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}",
                    'documentos'        => $documentos
                ];

                $documentosRepository = new DocumentosRepository($this->curl);
                $documentosRepository->subirAnexosS3($datosDocumentos);

                ClientesAlta::where('id', $idAlta)->update([
                    'alta_certificados_deuda'     => 1,
                    'alta_certificados_deuda_at'  => date('Y-m-d H:i:s'),
                    'certificados_deuda_error'    => null
                ]);

                $job = (new EnvioEmail_DocumentosCompletos($this->clienteAlta))->onQueue(env('QUEUE_NAME'));
                dispatch($job);

                $notificacion_slack = env('NOTIFICACION_SLACK', false);
                $notificacion_teams = env('NOTIFICACION_TEAMS', false);

                if ($notificacion_slack == true) {
                    $notificacion = new \stdClass;
                    $notificacion->type = 'success';
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                    $notificacion->content = 'Carga de certificados de deuda exitosa';
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
                }

                if ($notificacion_teams == true) {
                    $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_COMPROBANTES_INGRESO_WEBHOOK'));
                    $card  = new CardReporte([
                        'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                        'subtitle'  => 'Carga de certificados de deuda',
                        'text'      => 'Carga de certificados de deuda exitosa',
                        'producto'  => $nombreProducto,
                    ]);
                    $connector->send($card);
                }

            } else {
                throw new Exception("No existen los archivos");
            }


        } else {
            throw new Exception("La solicitud en T24 aún no existe");
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Carga de certificados de deuda fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['certificados_deuda_error'];

        ClientesAlta::where('id', $idAlta)->update([
            'alta_certificados_deuda'     => 0,
            'alta_certificados_deuda_at'  => date('Y-m-d H:i:s'),
            'certificados_deuda_error'    => $error.PHP_EOL.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Carga de certificados de deuda fallida: '.PHP_EOL.$exception->getMessage());
        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }

}
