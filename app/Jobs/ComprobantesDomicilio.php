<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\ClientesAlta;
use App\Notifications\AltaClienteNotification;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use App\Solicitud;
use App\CargaDocumentos;
use App\DomicilioSolicitud;
use App;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use App\W_LOCATION_VALIDATION;
use App\Notifications\CardReporte;

class ComprobantesDomicilio implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $curl;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Comprobantes Domicilio");

        $datosCliente = $this->clienteAlta->toArray();
        $no_solicitud_t24 = $datosCliente['no_solicitud_t24'];
        $no_cliente_t24 = $datosCliente['no_cliente_t24'];
        $idAlta = $datosCliente['id'];

        $email = $datosCliente['EMAIL'];
        $solicitud_id = $datosCliente['solicitud_id'];
        $prospecto_id = $datosCliente['prospecto_id'];

        $producto = null;
        $version = null;

        $solicitud = Solicitud::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

        $nombreProducto = '';
        if (count($solicitud[0]['producto']) == 1) {
            $producto = $solicitud[0]['producto'][0]['id'];
            $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
        } else {
            $nombreProducto = 'Mercado Abierto';
        }

        if ($no_solicitud_t24 !== null) {

            $comprobantes = CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->first();

            $frecuencia = $comprobantes->tipo_comprobante;
            $tipo_documento = 'comprobante_domicilio';
            $root = 'proceso_simplificado';
            $documentos = [];

            // Verificando que existe el comprobante front
            $detalle_documento = 'front';
            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg")) {
                $documentos[] = [
                    'tipo_documento'    => $tipo_documento,
                    'detalle_documento' => "{$detalle_documento}",
                    'formato_documento' => '.jpg'
                ];
            } elseif (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.pdf")) {
                $documentos[] = [
                    'tipo_documento'    => $tipo_documento,
                    'detalle_documento' => "{$detalle_documento}",
                    'formato_documento' => '.pdf'
                ];
            }

            // Verificando que existe el comprobante back
            $detalle_documento = 'back';
            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg")) {
                $documentos[] = [
                    'tipo_documento'    => $tipo_documento,
                    'detalle_documento' => "{$detalle_documento}",
                    'formato_documento' => '.jpg'
                ];
            } elseif (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.pdf")) {
                $documentos[] = [
                    'tipo_documento'    => $tipo_documento,
                    'detalle_documento' => "{$detalle_documento}",
                    'formato_documento' => '.pdf'
                ];
            }

            $datosDocumentos = [
                'no_solcitud_t24'   => $no_solicitud_t24,
                'email'             => $email,
                'ruta_local'        => "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}",
                'documentos'        => $documentos
            ];

            $documentosRepository = new DocumentosRepository($this->curl);
            $documentosRepository->subirComprobantesS3($datosDocumentos);

            $message = '';
            $geo_response = 0;
            if ($comprobantes->distancia == 0 && ($comprobantes->location_error != '' || $comprobantes->reverse_error != '')) {
                if ($comprobantes->location_error != '') {
                    $message .= $comprobantes->location_error;
                }
                if ($message != '') {
                    $message .= PHP_EOL;
                }
                if ($comprobantes->reverse_error != '') {
                    $message .= $comprobantes->reverse_error;
                }
            } else if ($comprobantes->distancia > env('DISTANCIA_MAXIMA')) {
                $message = "La distancia entre el usuario y su domicilio excede los limites establecidos. Distancia Calculada: {$comprobantes->distancia} - Distancia Permitida: ".env('DISTANCIA_MAXIMA');
            } else if ($comprobantes->distancia < env('DISTANCIA_MAXIMA')) {
                $message = "Operacion Exitosa. Distancia Calculada: {$comprobantes->distancia} - Distancia Permitida: ".env('DISTANCIA_MAXIMA');
                $geo_response = 1;
            }

            $domicilio = DomicilioSolicitud::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->first();

            $user_address = '';
            if ($domicilio->num_interior) {
                $user_address = "{$domicilio->calle} {$domicilio->num_exterior} Int. {$domicilio->num_interior}, {$domicilio->colonia}, {$domicilio->delegacion}, {$domicilio->ciudad}, {$domicilio->estado}";
            } else {
                $user_address = "{$domicilio->calle} {$domicilio->num_exterior}, {$domicilio->colonia}, {$domicilio->delegacion}, {$domicilio->ciudad}, {$domicilio->estado}";
            }

            W_LOCATION_VALIDATION::updateOrCreate([
                'ID_LOAN_APP'           => $no_solicitud_t24,
            ],[
                'GEO_RESPONSE'          => $geo_response,
                'MESSAGE'               => $message,
                'USER_ADDRESS'          => $user_address,
                'LATITUDE'              => $comprobantes->latitud,
                'LONGITUDE'             => $comprobantes->longitud,
                'EXECUTION_DATE'        => Carbon::now(),
                'REVERSE_USER_ADDRESS'  => $comprobantes->reverse_address,
                'LATITUDE_REVERSE'      => $comprobantes->latitud_reverse,
                'LONGITUDE_REVERSE'     => $comprobantes->longitud_reverse,
                'DEVICE'                => $comprobantes->device,
                'BROWSER'               => $comprobantes->browser,
                'PLATFORM'              => $comprobantes->platform,
                'CREATE_DATE'           => Carbon::now(),
                'UPDATE_DATE'           => Carbon::now(),
            ]);

            ClientesAlta::where('id', $idAlta)->update([
                'alta_comprobante_domicilio'     => 1,
                'alta_comprobante_domicilio_at'  => date('Y-m-d H:i:s'),
                'comprobante_domicilio_error'    => null
            ]);

            // Mandamos la actualización al panel operativo
            $datos = [
                'd' => 'gloc',
                'i' => $no_solicitud_t24,
            ];
            $this->curl->syncWebapp($datos);

            $notificacion_slack = env('NOTIFICACION_SLACK', false);
            $notificacion_teams = env('NOTIFICACION_TEAMS', false);

            if ($notificacion_slack == true) {
                $notificacion = new \stdClass;
                $notificacion->type = 'success';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Carga de comprobantes de domicilio exitosa';
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
            }

            if ($notificacion_teams == true) {
                $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_COMPROBANTES_DOMICILIO_WEBHOOK'));
                $card  = new CardReporte([
                    'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                    'subtitle'  => 'Comprobantes de domicilio',
                    'text'      => 'Carga de comprobantes de domicilio exitosa',
                    'producto'  => $nombreProducto,
                ]);
                $connector->send($card);
            }

            if ($nombreProducto == 'Renovaciones') {
                $job = (new EnvioEmail_DocumentosCompletos($this->clienteAlta))->onQueue(env('QUEUE_NAME'));
                dispatch($job);
            }


        } else {
            throw new Exception("La solicitud en T24 aún no existe");
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Carga de comprobantes de domicilio fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['comprobante_domicilio_error'];

        ClientesAlta::where('id', $idAlta)->update([
            'alta_comprobante_domicilio'     => 0,
            'alta_comprobante_domicilio_at'  => date('Y-m-d H:i:s'),
            'comprobante_domicilio_error'    => $error.PHP_EOL.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Carga de comprobantes de domicilio fallida: '.PHP_EOL.$exception->getMessage());
        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }

}
