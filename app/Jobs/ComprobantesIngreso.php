<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\ClientesAlta;
use App\Notifications\AltaClienteNotification;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use App\Solicitud;
use App\CargaDocumentos;
use App;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use App\Notifications\CardReporte;
use App\Jobs\EnvioEmail_DocumentosCompletos;

class ComprobantesIngreso implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $curl;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Comprobantes Ingreso");

        $datosCliente = $this->clienteAlta->toArray();
        $no_solicitud_t24 = $datosCliente['no_solicitud_t24'];
        $no_cliente_t24 = $datosCliente['no_cliente_t24'];
        $idAlta = $datosCliente['id'];

        $email = $datosCliente['EMAIL'];
        $solicitud_id = $datosCliente['solicitud_id'];
        $prospecto_id = $datosCliente['prospecto_id'];

        $producto = null;
        $version = null;

        $solicitud = Solicitud::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

        $nombreProducto = '';
        if (count($solicitud[0]['producto']) == 1) {
            $producto = $solicitud[0]['producto'][0]['id'];
            $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
        } else {
            $nombreProducto = 'Mercado Abierto';
        }

        if ($no_solicitud_t24 !== null) {

            $comprobantes = CargaDocumentos::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->first();

            if (isset($comprobantes->numero_comprobantes)) {

                $numero_comprobantes = $comprobantes->numero_comprobantes;
                $detalle_documento = $comprobantes->detalle_documento;
                $tipo_documento = 'comprobante_ingresos';
                $root = 'proceso_simplificado';
                $documentos = [];

                for ($i=1; $i <= $numero_comprobantes; $i++) {

                    if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$i}.jpg")) {
                        $extension = '.jpg';
                    } elseif (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$i}.pdf")) {
                        $extension = '.pdf';
                    }

                    $documentos[] = [
                        'tipo_documento'    => $tipo_documento,
                        'detalle_documento' => "{$detalle_documento}_{$i}",
                        'formato_documento' => $extension
                    ];

                }

                $datosDocumentos = [
                    'no_solcitud_t24'   => $no_solicitud_t24,
                    'email'             => $email,
                    'ruta_local'        => "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}",
                    'documentos'        => $documentos
                ];

                $documentosRepository = new DocumentosRepository($this->curl);
                $documentosRepository->subirComprobantesS3($datosDocumentos);

                ClientesAlta::where('id', $idAlta)->update([
                    'alta_comprobante_ingresos'     => 1,
                    'alta_comprobante_ingresos_at'  => date('Y-m-d H:i:s'),
                    'comprobante_ingresos_error'    => null
                ]);
                
                if ($datosCliente['aplica_certificados_deuda'] != 1) {
                    $job = (new EnvioEmail_DocumentosCompletos($this->clienteAlta))->onQueue(env('QUEUE_NAME'));
                    dispatch($job);
                }
                $notificacion_slack = env('NOTIFICACION_SLACK', false);
                $notificacion_teams = env('NOTIFICACION_TEAMS', false);

                if ($notificacion_slack == true) {
                    $notificacion = new \stdClass;
                    $notificacion->type = 'success';
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                    $notificacion->content = 'Carga de comprobantes de ingreso exitosa';
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
                }

                if ($notificacion_teams == true) {
                    $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_COMPROBANTES_INGRESO_WEBHOOK'));
                    $card  = new CardReporte([
                        'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                        'subtitle'  => 'Carga de comprobantes de ingreso',
                        'text'      => 'Carga de comprobantes de ingreso exitosa',
                        'producto'  => $nombreProducto,
                    ]);
                    $connector->send($card);
                }

            } else {
                throw new Exception("No existen los archivos");
            }


        } else {
            throw new Exception("La solicitud en T24 aún no existe");
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Carga de comprobantes de ingresos fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['comprobante_ingresos_error'];

        ClientesAlta::where('id', $idAlta)->update([
            'alta_comprobante_ingresos'     => 0,
            'alta_comprobante_ingresos_at'  => date('Y-m-d H:i:s'),
            'comprobante_ingresos_error'    => $error.PHP_EOL.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Carga de comprobantes de ingresos fallida: '.PHP_EOL.$exception->getMessage());
        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }

}
