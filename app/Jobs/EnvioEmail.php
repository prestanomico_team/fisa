<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\Prospecto;
use App\Solicitud;
use App\RespuestaMaquinaRiesgo;
use Exception;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Notifications\AltaClienteNotification;

class EnvioEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;

    private $codigos_error_email = [
        '-10'   => 'Los correos de TO o FROM son inválidos',
        '-19'   => 'Lista negra',
        '-100'  => 'Usuario no válido',
        '-141'  => 'No se tiene saldo suficiente',
        '-200'  => 'No se pudo obtener el id de Nodejs',
        '-300'  => 'No se pudo insertar el ID SMPP',
        '-424'  => 'Timeout',
    ];

    private $codigos_success_sms = [
        '3'     => 'Enviado',
    ];

    private $codigos_error_sms = [
        '4'     => 'Cancelado',
        '5'     => 'Error',
        '6'     => 'No móvil',
        '10'    => 'Número inválido',
        '16'    => 'No se encunetra la plantilla',
        '19'    => 'En lista negra',
        '22'    => 'Lista negra no disponible',
        '49'    => 'No registrado',
        '50'    => 'En registro',
        '51'    => 'Eliminado',
        '52'    => 'Error en registro',
        '101'   => 'Falta de saldo en el servicio',
        '199'   => 'Falta de saldo en el servicio',
        '-1'    => 'Error general',
        '-3'    => 'Lista negra',
        '-200'  => 'Usuario no válido',
        '-201'  => 'No se tiene saldo suficiente',
        '-202'  => 'No se pudo obtener el id de Nodejs',
        '-203'  => 'No se pudo insertar el ID SMPP',
        '-204'  => 'Timeout',
    ];

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
            Log::info("Iniciando envío de email");
            $datosCliente = $this->clienteAlta->toArray();

            $idAlta = $datosCliente['id'];
            $prospecto = $datosCliente['prospecto_id'];
            $solicitud = $datosCliente['solicitud_id'];
            $finalidad = Solicitud::select('finalidad')->where('id', $solicitud)->get()->toArray();
            $email = $datosCliente['EMAIL'];
            $celular = $datosCliente['TELCEL'];

            $respuesta_maquina_riesgos = RespuestaMaquinaRiesgo::with('plantilla_cominicacion')
                ->where('prospecto_id', $prospecto)
                ->where('solicitud_id', $solicitud)
                ->get();

            $vista_email = null;
            $asunto = null;
            $sms = null;

            if (count($respuesta_maquina_riesgos) >= 1) {
                $vista_email = $respuesta_maquina_riesgos[0]->plantilla_cominicacion['email_cuerpo'];
                $asunto = $respuesta_maquina_riesgos[0]->plantilla_cominicacion['email_asunto'];
                $sms = $respuesta_maquina_riesgos[0]->plantilla_cominicacion['sms'];
                $img_header = $respuesta_maquina_riesgos[0]->plantilla_cominicacion['img_email_header'];
            } else {
                $vista_email = 'prospecto_bienvenida';
                $asunto = 'Tu crédito en FINANCIERA MONTE DE PIEDAD está cada vez más cerca, continúa con tu solicitud.';
                $img_header = 'cc747e28-278f-4f39-b3b0-b9f2eec5a19a.png';
            }

            $view = view('emails.'.$vista_email)->with(
                [
                    'nombre'        =>  $datosCliente['NAME2'].' '.$datosCliente['SHORTNAME'],
                    'prospecto_id'  =>  $datosCliente['prospecto_id'],
                    'finalidad'     =>  mb_strtoupper($finalidad[0]['finalidad']),
                    'img_header'    =>  $img_header,
                ]
            );

            $this->soapWrapper = new SoapWrapper();

            $this->soapWrapper->add('Calixta', function ($service) {
                $service->wsdl(env('CALIXTA_EMAIL_WSDL'))
                ->trace(true)
                ->options([
                    'connection_timeout'        => 10,
                    'default_socket_timeout'    => 10
                ]);
            });

            $response = $this->soapWrapper->call('Calixta.EnviaEmail', [
                'cte'                   => env('CALIXTA_EMAIL_CTE'),
                'email'                 => env('CALIXTA_EMAIL_USUARIO'),
                'password'              => env('CALIXTA_EMAIL_PASSWORD'),
                'nombreCamp'            => 'Email Invitación a Continuar',
                'to'                    => $email,
                'from'                  => 'documentaciondigital@financieramontedepiedad.com.mx',
                'fromName'              => 'Equipo Financiera Monte de Piedad',
                'replyTo'               => 'documentaciondigital@financieramontedepiedad.com.mx',
                'subject'               => $asunto,
                'incrustrarImagen'      => 0,
                'textEmail'             => $vista_email,
                'htmlEmail'             => $view,
                'seleccionaAdjuntos'    => 0,
                'envioSinArchivo'       => 1,
                'fechaInicio'           => date('d/m/Y'),
            ]);

            $lista_codigos = array_keys($this->codigos_error_email);
            if (!in_array($response, $lista_codigos)) {

                ClientesAlta::where('id', $idAlta)->update([
                    'email_enviado'             => 1,
                    'id_email'                  => $response,
                    'envio_notificaciones_at'   => date('Y-m-d H:i:s'),
                ]);

                Log::info("El envío de correo fue exitoso");

                $notificacion = new \stdClass;
                $notificacion->type = 'success';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Correo enviado exitosamente con Id '.$response;
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

            }  else {

                Log::info("El envío de correo fue erroneo");
                $msg = $this->codigos_error_email[$response];
                throw new Exception("El envío de correo fue erroneo. ".$msg);
            }

            if ($sms != null) {
                Log::info('Iniciando envío de SMS');

                $response = $this->soapWrapper->call('Calixta.EnviaMensajeOL', [
                    'idCliente'             => env('CALIXTA_EMAIL_CTE'),
                    'email'                 => env('CALIXTA_EMAIL_USUARIO'),
                    'password'              => env('CALIXTA_EMAIL_PASSWORD'),
                    'tipo'                  => env('MTIPO'),
                    'telefono'              => $celular,
                    'mensaje'               => $sms,
                    'idIvr'                 => env('IDIVR'),
                    'fechaInicio'           => date('d/m/Y'),
                    'campoAux'              => env('AUXILIAR'),
                    'Asunto'                => 'Tu crédito PRESTANOMICO está cada vez más cerca',
                ]);

                $lista_codigos_success = array_keys($this->codigos_success_sms);
                $lista_codigos_error = array_keys($this->codigos_error_sms);

                if (in_array($response, $lista_codigos_success)) {

                    Log::info('El SMS fue enviado con éxito');

                    ClientesAlta::where('id', $idAlta)->update([
                        'status_sms'                => $response,
                        'envio_notificaciones_at'   => date('Y-m-d H:i:s'),
                    ]);

                } elseif ($response >= 101 && $response <= 199) {

                    Log::info('El SMS no pudo ser enviado. Falta de saldo en el servicio');
                    throw new Exception('El SMS no pudo ser enviado. Falta de saldo en el servicio');

                } elseif (in_array($response, $lista_codigos_success)) {

                    $msg = $this->codigos_error_sms[$response];
                    throw new Exception($msg);

                } else {

                    throw new Exception($response);

                }
            }
    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('El envío de correo fue erroneo: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        ClientesAlta::where('id', $idAlta)->update([
            'email_enviado' => 0,
            'error' => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Envío de Email/SMS erroneo: '.chr(13).$exception->getMessage());

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
