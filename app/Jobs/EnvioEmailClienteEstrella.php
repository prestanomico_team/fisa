<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\ClienteEstrella;

class EnvioEmailClienteEstrella implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $datosEmail;
    protected $idCliente;

    private $codigos_error_email = [
        '-10'   => 'Los correos de TO o FROM son inválidos',
        '-19'   => 'Lista negra',
        '-100'  => 'Usuario no válido',
        '-141'  => 'No se tiene saldo suficiente',
        '-200'  => 'No se pudo obtener el id de Nodejs',
        '-300'  => 'No se pudo insertar el ID SMPP',
        '-424'  => 'Timeout',
    ];

    private $codigos_success_sms = [
        '3'     => 'Enviado',
    ];

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct($datosEmail, $idCliente)
    {
        $this->datosEmail = $datosEmail;
        $this->idCliente = $idCliente;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        if (env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos') {
            $email = 'emails.Email_Cliente_Estrella_DEV';
        } else {
            $email = 'emails.Email_Cliente_Estrella_PROD';
        }

        $view = view($email)->with(
            [
                'nombre'            => $this->datosEmail['nombre'],
                'nombre_completo'   => $this->datosEmail['nombre_completo'],
                'monto'             => $this->datosEmail['monto'],
                'tasa'              => $this->datosEmail['tasa'],
                'plazo'             => $this->datosEmail['plazo'],
                'pago_estimado'     => $this->datosEmail['pago_estimado'],
                'vigencia'          => $this->datosEmail['vigencia'],
                'uuid'              => $this->datosEmail['uuid']
            ]
        );

        $this->soapWrapper = new SoapWrapper();

        $this->soapWrapper->add('Calixta', function ($service) {
            $service->wsdl(env('CALIXTA_EMAIL_WSDL'))
            ->trace(true)
            ->options([
                'connection_timeout'        => 10,
                'default_socket_timeout'    => 10
            ]);
        });

        $response = $this->soapWrapper->call('Calixta.EnviaEmail', [
            'cte'                   => env('CALIXTA_EMAIL_CTE'),
            'email'                 => env('CALIXTA_EMAIL_USUARIO'),
            'password'              => env('CALIXTA_EMAIL_PASSWORD'),
            'nombreCamp'            => 'Cliente Estrella',
            'to'                    => $this->datosEmail['email'],
            'from'                  => 'prestamos@prestanomico.com',
            'fromName'              => 'Equipo Prestanómico',
            'replyTo'               => 'prestamos@prestanomico.com',
            'subject'               => $this->datosEmail['nombre'].', ¡En Prestanómico recompensamos tu compromiso!',
            'incrustrarImagen'      => 0,
            'textEmail'             => 'Cliente estrella',
            'htmlEmail'             => $view,
            'seleccionaAdjuntos'    => 0,
            'envioSinArchivo'       => 1,
            'fechaInicio'           => date('d/m/Y'),
        ]);

        $lista_codigos = array_keys($this->codigos_error_email);
        if (!in_array($response, $lista_codigos)) {

            $clienteEstrella = ClienteEstrella::where('id', $this->idCliente)
                ->update([
                    'email_enviado' => 1,
                    'id_email'      => $response,
                ]);

        }  else {

            $msg = $this->codigos_error_email[$response];
            $clienteEstrella = ClienteEstrella::where('id', $this->idCliente)
                ->update([
                    'email_enviado' => 0,
                    'email_error'   => 'El envío de correo fue erroneo. '.$msg,
                ]);

        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $clienteEstrella = ClienteEstrella::where('id', $this->idCliente)
            ->update([
                'email_enviado' => 1,
                'id_email'      => $exception->getMessage(),
            ]);
    }
}
