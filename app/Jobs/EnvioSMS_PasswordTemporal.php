<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\Prospecto;
use App\Solicitud;
use App\PlantillaComunicacion;
use Exception;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Notifications\AltaClienteNotification;
use Bitly;
use App\Repositories\SolicitudRepository;
use App\Notifications\CardReporte;
use Illuminate\Contracts\Encryption\DecryptException;

class EnvioSMS_PasswordTemporal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $solicitudRepository;

    private $codigos_error_email = [
        '-10'   => 'Los correos de TO o FROM son inválidos',
        '-19'   => 'Lista negra',
        '-100'  => 'Usuario no válido',
        '-141'  => 'No se tiene saldo suficiente',
        '-200'  => 'No se pudo obtener el id de Nodejs',
        '-300'  => 'No se pudo insertar el ID SMPP',
        '-424'  => 'Timeout',
    ];

    private $codigos_success_sms = [
        '3'     => 'Enviado',
    ];

    private $codigos_error_sms = [
        '4'     => 'Cancelado',
        '5'     => 'Error',
        '6'     => 'No móvil',
        '10'    => 'Número inválido',
        '16'    => 'No se encunetra la plantilla',
        '19'    => 'En lista negra',
        '22'    => 'Lista negra no disponible',
        '49'    => 'No registrado',
        '50'    => 'En registro',
        '51'    => 'Eliminado',
        '52'    => 'Error en registro',
        '101'   => 'Falta de saldo en el servicio',
        '199'   => 'Falta de saldo en el servicio',
        '-1'    => 'Error general',
        '-3'    => 'Lista negra',
        '-200'  => 'Usuario no válido',
        '-201'  => 'No se tiene saldo suficiente',
        '-202'  => 'No se pudo obtener el id de Nodejs',
        '-203'  => 'No se pudo insertar el ID SMPP',
        '-204'  => 'Timeout',
    ];

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->solicitudRepository = new SolicitudRepository;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Iniciando envío de SMS documentos incompletos por sucursal");
        $datosCliente = $this->clienteAlta->toArray();

        $idAlta = $datosCliente['id'];
        $prospecto = $datosCliente['prospecto_id'];
        $solicitud = $datosCliente['solicitud_id'];
        $password = Prospecto::where('id', $prospecto)->first();
        try {
            $password->encryptd = decrypt($password->encryptd);
        } catch(DecryptException $e) {
            $password->encryptd = '';
        }
        $email = $datosCliente['EMAIL'];
        $celular = $datosCliente['TELCEL'];


        $idPanel = $datosCliente['panel_operativo_id'];
        $url = env('APP_URL')."webapp?idPanel={$idPanel}";
        try {
            $url = Bitly::getUrl($url);
        } catch (\Exception $e) {
            $url = 'https://bit.ly/2SxMXrE';
        }

        // Buscando la platilla de comunicación 891 para el envio de documentos incompletos
        $plantilla = PlantillaComunicacion::where('plantilla_id', 894)
            ->first();

        $sms = $plantilla->sms;

        if ($sms != null) {

            Log::info('Enviando SMS password temporal');

            $this->soapWrapper = new SoapWrapper();

            $this->soapWrapper->add('Calixta', function ($service) {
                $service->wsdl(env('CALIXTA_EMAIL_WSDL'))
                ->trace(true)
                ->options([
                    'connection_timeout'        => 10,
                    'default_socket_timeout'    => 10
                ]);
            });

            $response = $this->soapWrapper->call('Calixta.EnviaMensajeOL', [
                'idCliente'             => env('CALIXTA_EMAIL_CTE'),
                'email'                 => env('CALIXTA_EMAIL_USUARIO'),
                'password'              => env('CALIXTA_EMAIL_PASSWORD'),
                'tipo'                  => env('MTIPO'),
                'telefono'              => $celular,
                'mensaje'               => $sms.' '.$password->encryptd,
                'idIvr'                 => env('IDIVR'),
                'fechaInicio'           => date('d/m/Y'),
                'campoAux'              => env('AUXILIAR'),
                'Asunto'                => 'Tu crédito PRESTANOMICO está cada vez más cerca',
            ]);

            $lista_codigos_success = array_keys($this->codigos_success_sms);
            $lista_codigos_error = array_keys($this->codigos_error_sms);

            /* if (in_array($response, $lista_codigos_success)) {

                Log::info('El SMS documentos incompletos fue enviado con éxito');

                ClientesAlta::where('id', $idAlta)->update([
                    'status_sms'                => $response,
                    'envio_notificaciones_at'   => date('Y-m-d H:i:s'),
                ]);

                $notificacion_slack = env('NOTIFICACION_SLACK', false);
                $notificacion_teams = env('NOTIFICACION_TEAMS', false);

                if ($notificacion_slack == true) {
                    $notificacion = new \stdClass;
                    $notificacion->type = 'success';
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                    $notificacion->content = 'Envio de correo exitoso';
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
                }

                if ($notificacion_teams == true) {
                    $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_EMAIL_DOCUMENTOS_INCOMPLETOS_WEBHOOK'));
                    $card  = new CardReporte([
                        'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                        'subtitle'  => 'Envio de notificaciones SMS',
                        'text'      => 'Envio de SMS exitoso',
                    ]);
                    $connector->send($card);
                }

            } elseif ($response >= 101 && $response <= 199) {

                Log::info('El SMS no pudo ser enviado. Falta de saldo en el servicio');
                throw new Exception('El SMS no pudo ser enviado. Falta de saldo en el servicio');

            } elseif (in_array($response, $lista_codigos_success)) {

                $msg = $this->codigos_error_sms[$response];
                throw new Exception($msg);

            } else {

                throw new Exception($response);

            }
 */
        } else {
            Log::info('Se cancela el envio de SMS no hay pasword temporal');
        }
        
    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('El envío de correo fue erroneo: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        ClientesAlta::where('id', $idAlta)->update([
            'email_enviado' => 0,
            'error' => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Envío de Email/SMS erroneo: '.chr(13).$exception->getMessage());

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
