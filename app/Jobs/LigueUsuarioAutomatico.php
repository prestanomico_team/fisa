<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\Prospecto;
use Exception;
use Log;
use App\Repositories\CognitoRepository;
use App\Notifications\AltaClienteNotification;
use App\Repositories\CurlCaller;

class LigueUsuarioAutomatico implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;
    protected $cognito;
    protected $curl;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');

        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Ligue de usuario");

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];

        $datosProspecto = Prospecto::where('id', $datosCliente['prospecto_id'])
            ->get()
            ->toArray();

        if (count($datosProspecto) > 0) {

            $no_cliente_t24 = $datosCliente['no_cliente_t24'];
            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($datosProspecto[0]['email'], ['custom:idT24' => "{$no_cliente_t24}"]);

            if ($responseCognito['success'] == false) {

                Log::info("Ligue de usuario fallido en Cognito");
                throw new Exception("Ligue de usuario fallido en Cognito: ".$responseCognito['message']);

            } elseif ($responseCognito['success'] == true) {


                ClientesAlta::where('id', $idAlta)->update([
                    'usuario_ligado'    => 1,
                    'ligue_usuario_at'  => date('Y-m-d H:i:s'),
                ]);

                Log::info("Usuario ligado con éxito");

                $notificacion = new \stdClass;
                $notificacion->type = 'success';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Usuario ligado con éxito';
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

            }

        } else {

            Log::info("Ligue de usuario fallido: No se encontro el prospecto");
            throw new Exception("Ligue de usuario fallido: No se encontro el prospecto");

        }
    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Ligue de usuario fallido: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        ClientesAlta::where('id', $idAlta)->update([
            'usuario_ligado'    => 0,
            'ligue_usuario_at'  => date('Y-m-d H:i:s'),
            'error'             => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Ligue de usuario LDAP erroneo: '.chr(13).$exception->getMessage());

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

    }
}
