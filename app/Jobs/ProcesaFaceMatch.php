<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Exception;
use SoapClient;
use SimpleXMLElement;
use App;
use Carbon\Carbon;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Storage;
use App\Notifications\AltaClienteNotification;
use App\ClientesAlta;
use App\DatosIdentificacionOficial;
use App\DatosIdentificacionOficialR;
use App\W_ICAR_VALIDATION;
use App\W_LOAN_APPLICATION;
use App\Solicitud;
use App\Repositories\FaceMatchRepository;
use App\Repositories\CurlCaller;
use App\Repositories\DocumentosRepository;
use App\Repositories\PanelOperativoRepository;
use Image;
use DB;
use App\Notifications\CardReporte;

class ProcesaFaceMatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;
    public $tries = 1;
    protected $clienteAlta;
    protected $resultadoDocumentos;
    protected $resultadoRegistro;
    protected $curl;
    protected $altaPanel = false;
    protected $idPanelOperativo = false;
    protected $generaProcesos = true;
    protected $soapWrapper;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
        $this->soapWrapper = new SoapWrapper();
        $this->resultadoDocumentos = $this->clienteAlta->documentos_facematch === null ? false : $this->clienteAlta->documentos_facematch;
        $this->resultadoRegistro = $this->clienteAlta->registro_facematch === null ? false : $this->clienteAlta->registro_facematch;

    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle(FaceMatchRepository $facematch)
    {
        try {
            ini_set('default_socket_timeout', 700);
            $solicitudT24 = $this->clienteAlta->no_solicitud_t24;
            $panel_operativo_id = $this->clienteAlta->panel_operativo_id;
            $this->generaProcesos = true;

            if ($solicitudT24 !== null) {
                if ($panel_operativo_id === null) {
                    $panelOperativoRepository = new PanelOperativoRepository($this->curl);
                    $id_clienteAlta = [
                        'idp' => $this->clienteAlta->id,
                    ];
                    $response = $panelOperativoRepository->forzarAlta($id_clienteAlta);

                    if ($response['success'] == true) {
                        if (isset($response['response']->idp[0])) {
                            $this->altaPanel = true;
                            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                                'panel_operativo_id' => $response['response']->idp[0],
                            ]);
                        }
                    } else {
                        $this->generaProcesos = false;
                        throw new Exception("No se pudo dar de alta en el Panel Operativo");
                    }
                }

                $prospecto_id = $this->clienteAlta->prospecto_id;
                $solicitud_id = $this->clienteAlta->solicitud_id;
                $email = $this->clienteAlta->EMAIL;
                $ruta = "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}";

                $imageFront = Storage::disk('s3')->get("{$ruta}_identificacion_oficial_front.jpg");
                $imageBack = Storage::disk('s3')->get("{$ruta}_identificacion_oficial_back.jpg");
                $selfie = base64_encode(Storage::disk('s3')->get("{$ruta}_identificacion_oficial_photo.jpg"));
                $xml1 = '<?xml version="1.0" encoding="utf-8" standalone="no" ?><acquiredImage><device>30</device><resolutionX>300</resolutionX><resolutionY>300</resolutionY><faceImages><faceImage>'.$selfie.'</faceImage></faceImages></acquiredImage>';
                $xml2 = '<?xml version="1.0" encoding="utf-8" standalone="no" ?><acquiredImage><device>30</device><resolutionX>300</resolutionX><resolutionY>300</resolutionY></acquiredImage>';


                $this->soapWrapper->add('ICAR', function ($service) {
                    $service->wsdl(env('ICAR_ENDPOINT'))
                        ->trace(true)
                        ->options([
                            'connection_timeout'        => 600,
                            'default_socket_timeout'    => 600,
                            'keep_alive'                => false,
                            'stream_context'            => stream_context_create([
                                'ssl' => [
                                    'verify_peer'   => false,
                                    'verify_peer_name' => false,
                                ]
                            ]),
                        ])
                        ->cache(WSDL_CACHE_NONE);
                });

                $response = $this->soapWrapper->call('ICAR.AnalyzeDocumentV2Ex', [
                    'body' => [
                        'Company'                   => env('ICAR_COMPANY'),
                        'User'                      => env('ICAR_USER'),
                        'Pwd'                       => env('ICAR_PASSWORD'),
                        'GetDocumentIdForMerging'   => false,
                        'Activity'                  => '',
                        'DocInV2Ex' => [
                            'Reference' => 'WA-'.$prospecto_id.'-'.$solicitud_id,
                            'Image1'    => [
                                'ImageResolution'   => 300,
                                'Image'             => $imageFront,
                                'Filetype'          => 'jpg',
                                'DeviceInfo'        => $xml1
                            ],
                            'Image2'    => [
                                'ImageResolution'   => 300,
                                'Image'             => $imageBack,
                                'Filetype'          => 'jpg',
                                'DeviceInfo'        => $xml2
                            ],
                        ],
                        'DocumentIdToMerge' => ''
                    ]
                ]);


                $this->soapWrapper->client('ICAR', function ($client) {
                    // Escribe en el log la ultima respuesta del servicio en XML
                    // Log::info($client->getLastResponse());
                });


                if (isset($response->AnalyzeDocumentV2ExResult->Fields->Field)) {
                    $fields = $response->AnalyzeDocumentV2ExResult->Fields->Field;
                    $datosFaceMatch = [];
                    foreach ($fields as $key => $field) {

                        $value = str_contains($field->Code, ['TEST_FACE_RECOGNITION_VALUE',
                            'TEST_EXPIRY_DATE', 'TEST_GLOBAL_AUTHENTICITY_RATIO', 'TEST_GLOBAL_AUTHENTICITY_VALUE']);

                        DatosIdentificacionOficial::updateOrCreate([
                            'solicitud_id'  => $solicitud_id,
                            'prospecto_id'  => $prospecto_id,
                            'index'         => $key,
                        ], [
                            'solicitudT24'  => $solicitudT24,
                            'type'          => $field->Type,
                            'code'          => $field->Code,
                            'value'         => $field->Value,
                            'description'   => $field->Description,
                        ]);

                        if ($value === true) {
                            $index = '';
                            switch ($field->Code) {
                                case 'TEST_FACE_RECOGNITION_VALUE':
                                    $index = 'TEST_FACE_REC_VAL';
                                    break;
                                case 'TEST_EXPIRY_DATE':
                                    $index = 'TEST_EXPIRY_DATE';
                                    break;
                                case 'TEST_GLOBAL_AUTHENTICITY_RATIO':
                                    $index = 'TEST_LOBAL_ATUH_RAT';
                                    break;
                                case 'TEST_GLOBAL_AUTHENTICITY_VALUE':
                                    $index = 'TEST_LOBAL_ATUH_VAL';
                                    break;
                            }
                            $datosFaceMatch[$index] = "{$field->Code} : {$field->Value}";
                        }
                    }

                    // Guardando el resultado en una sola fila por resultado
                    if (count($fields) > 0) {
                        $datos = collect($fields)->groupBy('Code')->toArray();

                        $arreglo = [];
                        foreach ($datos as $key => $dt) {
                            foreach ($dt as $keydt => $value) {
                                $key = trim($key);
                                $key = preg_replace("/\s+/", "", $key);
                                if ($keydt == 0) {
                                    $arreglo[$key] = $value->Value;
                                } else {
                                    $k = $keydt+1;
                                    $arreglo[$key.$k] = $value->Value;
                                }
                            }
                        }
                        $columns = collect($arreglo)->keys()->toArray();

                        $model = new DatosIdentificacionOficialR;
                        $columnasTabla = $model->getTableColumns();
                        $columnasTabla = collect($columnasTabla)->filter(function ($value, $key) {
                            return ($value != 'created_at' && $value != 'updated_at');
                        })->toArray();

                        $notExist = array_diff_key(array_flip($columns), array_flip($columnasTabla));
                        $ultimaPosicion = collect($columnasTabla)->last();
                        $ultimaPosicion;

                        foreach ($notExist as $key => $value) {
                            $resultado = \DB::statement("ALTER TABLE datos_id_oficial_r ADD COLUMN `{$key}` MEDIUMTEXT NULL AFTER `{$ultimaPosicion}`");
                            if ($resultado == 1) {
                                $ultimaPosicion = $key;
                            }
                        }

                        DatosIdentificacionOficialR::updateOrCreate([
                                'prospecto_id'  => $prospecto_id,
                                'solicitud_id'  => $solicitud_id,
                                'solicitudT24'  => $solicitudT24,
                            ], $arreglo);
                    }
                    // Fin del Guardado en una sola fila

                    if (array_key_exists('TEST_FACE_REC_VAL', $datosFaceMatch) === false) {
                        $datosFaceMatch['TEST_FACE_REC_VAL'] = 'TEST_FACE_RECOGNITION_VALUE : null';
                    }
                    if (array_key_exists('TEST_EXPIRY_DATE', $datosFaceMatch) === false) {
                        $datosFaceMatch['TEST_EXPIRY_DATE'] = 'TEST_EXPIRY_DATE : null';
                    }
                    if (array_key_exists('TEST_LOBAL_ATUH_VAL', $datosFaceMatch) === false) {
                        $datosFaceMatch['TEST_LOBAL_ATUH_VAL'] = 'TEST_GLOBAL_AUTHENTICITY_VALUE : null';
                    }
                    if (array_key_exists('TEST_LOBAL_ATUH_RAT', $datosFaceMatch) === false) {
                        $datosFaceMatch['TEST_LOBAL_ATUH_RAT'] = 'TEST_GLOBAL_AUTHENTICITY_RATIO : null';
                    }
                    $datosFaceMatch['STATUS_RESPONSE'] = 1;
                    $datosFaceMatch['OBSERVATIONS'] = $datosFaceMatch['TEST_FACE_REC_VAL'];
                    $datosFaceMatch['EXECUTION_DATE'] = Carbon::now();
                    $datosFaceMatch['CREATED_AT'] = Carbon::now();
                    $datosFaceMatch['UPDATED_AT'] = Carbon::now();
                    $this->generaRegistro($datosFaceMatch);

                    if (isset($response->AnalyzeDocumentV2ExResult->Image1cut)) {
                        $image = Image::make($response->AnalyzeDocumentV2ExResult->Image1cut);
                        Storage::disk('s3')->put("{$ruta}_identificacion_oficial_ICAR_front.jpg", $image->encode());
                    }
                    if (isset($response->AnalyzeDocumentV2ExResult->Image2cut)) {
                        $image = Image::make($response->AnalyzeDocumentV2ExResult->Image2cut);
                        Storage::disk('s3')->put("{$ruta}_identificacion_oficial_ICAR_back.jpg", $image->encode());
                    }
                    $this->cargaDocumentos();

                    $comparaDatos = $facematch->comparaDatos($fields, $prospecto_id, $solicitud_id);
                    $resultadoDocumentos = ($this->resultadoDocumentos == 1 ? 'Ok' : 'Error');
                    $resultadoRegistro = ($this->resultadoRegistro == 1 ? 'Ok' : 'Error');

                    ClientesAlta::where('id', $this->clienteAlta->id)->update([
                        'facematch'         => 1,
                        'facematch_at'      => Carbon::now(),
                    ]);

                    $datos = [
                        'd' => 'fcth',
                        'i' => $solicitudT24,
                    ];
                    $this->curl->syncWebapp($datos);

                    $notificacion_slack = env('NOTIFICACION_SLACK', false);
                    $notificacion_teams = env('NOTIFICACION_TEAMS', false);

                    if ($notificacion_slack == true) {
                        $notificacion = new \stdClass;
                        $notificacion->type = 'success_fm';
                        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                        $notificacion->content = 'Facematch procesado con éxito'.
                            PHP_EOL.'Carga de documentos: '.$resultadoDocumentos.
                            PHP_EOL.'Registro en tablas: '.$resultadoRegistro;
                        $notificacion->solicitud = false;
                        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
                    }

                    if ($notificacion_teams == true) {

                        $solicitud = Solicitud::with('producto')
                            ->where('id', $solicitud_id)
                            ->get()
                            ->toArray();

                        $nombreProducto = '';
                        if (count($solicitud[0]['producto']) == 1) {
                            $nombreProducto = $solicitud[0]['producto'][0]['nombre_producto'];
                        } else {
                            $nombreProducto = 'Mercado Abierto';
                        }

                        $connector = new \Sebbmyr\Teams\TeamsConnector(env('TEAMS_FACEMATCH_WEBHOOK'));
                        $card  = new CardReporte([
                            'title'     => 'Id Prospecto: '.$this->clienteAlta->prospecto_id.' | Id Solicitud: '.$this->clienteAlta->solicitud_id,
                            'subtitle'  => 'Facematch',
                            'text'      => 'Facematch procesado con éxito',
                            'producto'  => $nombreProducto,
                        ]);
                        $connector->send($card);
                    }

                } else {
                    if (isset($response->AnalyzeDocumentV2ExResult->Messages)) {
                        $messages = $response->AnalyzeDocumentV2ExResult->Messages;
                        $message = '';
                        foreach ($messages as $msg) {
                            $message = $msg->Code.': '.$msg->Description;
                        }
                        // Error arrojado por ICAR al procesar el Facematch
                        $this->generaRegistro(null, $message);
                        $this->cargaDocumentos();
                        $this->generaProcesos = false;

                        $datos = [
                            'd' => 'fcth',
                            'i' => $solicitudT24,
                        ];
                        $this->curl->syncWebapp($datos);

                        throw new Exception("Facematch fallido: Error iCAR - {$message}");
                    } elseif (isset($response->AnalyzeDocumentV2ExResult->Warning)) {
                        $message = $response->AnalyzeDocumentV2ExResult->Warning;
                        $this->generaRegistro(null, $message);
                        $this->cargaDocumentos();
                        $this->generaProcesos = false;

                        $datos = [
                            'd' => 'fcth',
                            'i' => $solicitudT24,
                        ];
                        $this->curl->syncWebapp($datos);

                        throw new Exception("Facematch fallido: Error iCAR - {$message}");
                    } else {
                        $message = 'Error Desconocido';
                        $this->generaRegistro(null, $message);
                        $this->cargaDocumentos();
                        $this->generaProcesos = false;

                        $datos = [
                            'd' => 'fcth',
                            'i' => $solicitudT24,
                        ];
                        $this->curl->syncWebapp($datos);

                        throw new Exception("Facematch fallido: Error iCAR - {$message}");
                    }
                }

            } else {
                $this->generaProcesos = false;
                throw new Exception("Facematch fallido: La solicitud en T24 aún no existe");
            }

        } catch (\Exception $e) {
            // Error general del proceso de Facematch
            if ($this->generaProcesos === true) {
                $this->generaRegistro(null, $e->getMessage());
                $this->cargaDocumentos();
            }
            throw new Exception($e->getMessage());

        }

    }

    /**
     * Genera el registro en las tablas necesarias del APP y del Sitio para el control
     * del Facematch
     *
     * @param  array    $datosFaceMatch    Arreglo que contiene el resultado del Facematch
     * @param  string   $exceptionMessage  Cadena que contiene el movito por el cual el Facematch
     * no se proceso con éxito
     */
    public function generaRegistro($datosFaceMatch = null, $exceptionMessage = null) {

        try {

            $loanApp = W_LOAN_APPLICATION::updateOrCreate([
                'ID_LOAN_APP'       => $this->clienteAlta->no_solicitud_t24,
                'ID_USER'           => mb_strtolower($this->clienteAlta->EMAIL),
            ], [
                'ID_LOAN_PURPOSE'   => $this->clienteAlta->LOANPURPOSE,
                'ID_LOAN_STATUS'    => 2,
                'LOAN_AMOUNT'       => $this->clienteAlta->LOANAMOUNT,
                'APPLICATION_DATE'  => Carbon::createFromFormat('Ymd', $this->clienteAlta->fecha_alta),
                'CONTRACT_EXPIRY'   => Carbon::createFromFormat('Ymd', $this->clienteAlta->fecha_alta),
                'CREATE_DATE'       => Carbon::now(),
                'UPDATE_DATE'       => Carbon::now(),
            ]);

            if ($datosFaceMatch !== null) {

                $icarValidation = W_ICAR_VALIDATION::updateOrCreate([
                    'ID_LOAN_APP' => $this->clienteAlta->no_solicitud_t24,
                ], $datosFaceMatch);

            } else {
                $icarValidation = W_ICAR_VALIDATION::updateOrCreate([
                    'ID_LOAN_APP' => $this->clienteAlta->no_solicitud_t24,
                ], [
                    'STATUS_RESPONSE'       => 0,
                    'TEST_FACE_REC_VAL'     => 'TEST_FACE_REC_VAL: null',
                    'TEST_EXPIRY_DATE'      => 'TEST_EXPIRY_DATE: null',
                    'TEST_LOBAL_ATUH_RAT'   => 'TEST_LOBAL_ATUH_RAT: null',
                    'TEST_LOBAL_ATUH_VAL'   => 'TEST_LOBAL_ATUH_VAL: null',
                    'OBSERVATIONS'          => $exceptionMessage,
                    'EXECUTION_DATE'        => Carbon::now(),
                    'CREATED_AT'            => Carbon::now(),
                    'UPDATED_AT'            => Carbon::now()
                ]);
            }

            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'registro_facematch'    => 1,
            ]);
            $this->resultadoRegistro = true;

        } catch (\Exception $e) {
            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'registro_facematch'    => 0,
                'facematch_error'       => $this->clienteAlta->error_facematch.
                    PHP_EOL.
                    'Error al realizar el registro: '.$e->getMessage()
            ]);
            $this->resultadoRegistro = false;
        }

    }

    /**
     * Realiza el llamado al método de la clase DocumentosRepository que permite
     * subir los documentos al Panel Operativo/Repositorio de Imagenes
     */
    public function cargaDocumentos() {

        try {
            $documentos = new DocumentosRepository($this->curl);
            $prospecto_id = $this->clienteAlta->prospecto_id;
            $solicitud_id = $this->clienteAlta->solicitud_id;
            $clienteT24 = $this->clienteAlta->no_cliente_t24;
            $solicitudT24 = $this->clienteAlta->no_solicitud_t24;
            $nombre = "{$this->clienteAlta->NAME2} {$this->clienteAlta->FORMERNAME}";
            $nombre = trim(preg_replace('/\s+/',' ', $nombre));
            $apellidos = "{$this->clienteAlta->NAME1} {$this->clienteAlta->SHORTNAME}";
            $nombre_completo = "{$nombre} {$apellidos}";
            $celular = $this->clienteAlta->TELCEL;
            $email = $this->clienteAlta->EMAIL;
            $ruta = "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}";

            $arregloDocumentos = [
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'front',
                    'formato_documento' => '.jpg'
                ],
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'back',
                    'formato_documento' => '.jpg'
                ],
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'photo',
                    'formato_documento' => '.jpg'
                ],
            ];

            if (Storage::disk('s3')->exists("{$ruta}_identificacion_oficial_ICAR_front.jpg")) {
                $arregloDocumentos[] = [
                    'tipo_documento'    => 'identificacion_oficial_ICAR',
                    'detalle_documento' => 'front',
                    'formato_documento' => '.jpg'
                ];
            }
            if (Storage::disk('s3')->exists("{$ruta}_identificacion_oficial_ICAR_back.jpg")) {
                $arregloDocumentos[] = [
                    'tipo_documento'    => 'identificacion_oficial_ICAR',
                    'detalle_documento' => 'back',
                    'formato_documento' => '.jpg'
                ];

            }

            $datosDocumentos = [
                'no_solcitud_t24'   => $solicitudT24,
                'no_cliente_t24'    => $clienteT24,
                'nombre'            => $nombre,
                'apellidos'         => $apellidos,
                'nombre_completo'   => $nombre_completo,
                'celular'           => $celular,
                'email'             => $email,
                'ruta_local'        => $ruta,
                'documentos'        => $arregloDocumentos
            ];

            $documentos->subirDocumentoS3($datosDocumentos);

            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'documentos_facematch'  => 1,
            ]);

            $this->resultadoDocumentos = true;

        } catch (\Exception $e) {
            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'documentos_facematch'  => 0,
                'facematch_error'       => $this->clienteAlta->error_facematch.
                    PHP_EOL.
                    'Error al cargar documentos: '.$e->getMessage()
            ]);

            $this->resultadoDocumentos = false;
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Facematch fallido: '.$exception);
        $message = $exception->getMessage();
        $idAlta = $this->clienteAlta->id;
        $error = $this->clienteAlta->facematch_error;
        if ($error != '') {
            $error = $error.PHP_EOL;
        }
        ClientesAlta::where('id', $idAlta)->update([
            'facematch'         => 0,
            'facematch_error'   => $error.$message,
        ]);

        $resultadoDocumentos = ($this->resultadoDocumentos == 1 ? 'Ok' : 'Error');
        $resultadoRegistro = ($this->resultadoRegistro == 1 ? 'Ok' : 'Error');

        $notificacion = new \stdClass;
        $notificacion->type = 'error_fm';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = 'Error al procesar Facematch: '.PHP_EOL.$exception->getMessage().
            PHP_EOL.'Carga de documentos: '.$resultadoDocumentos.
            PHP_EOL.'Registro en tablas: '.$resultadoRegistro;

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
