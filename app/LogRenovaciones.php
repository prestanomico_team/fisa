<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla log_renovaciones
 */
class LogRenovaciones extends Model
{
    protected $table = 'log_renovaciones';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'celular',
        'rfc',
        'email',
        'renovaciones_request',
        'renovaciones_response'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
