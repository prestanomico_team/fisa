<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSolicitud extends Model
{
    protected $table = 'log_consola';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'solicitud_id',
        'ultimo_mensaje_usuario'
    ];
}
