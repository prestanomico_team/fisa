<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivoRechazo extends Model
{
    protected $table = 'motivos_rechazo';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
