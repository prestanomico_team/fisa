<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ofertaPredominante extends Model
{
    protected $table = 'oferta_predominante';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'ejecucion_modelo',
        'modeloEvaluacion',
        'ofertaEvaluacion',
        'decision',
        'plantilla_comunicacion',
        'status_oferta',
        'motivo_rechazo',
        'descripcion_otro',
        'pantallas_extra',
        'situaciones',
        'cuestionario_dinamico_guardado',
        'status_guardado',
        'oferta_minima',
        'simplificado',
        'facematch' ,
        'carga_identificacion_selfie',
        'subir_documentos',
        'elegida',
        'oferta_id',
        'tipo_poblacion',
        'tipo_oferta',
        'monto',
        'plazo',
        'pago',
        'tasa',
        'tipo_tasa'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Obtiene los datos de la plantilla de comuniaciones
     *
     * @return object Datos de la plantilla de comunicaciones
     */
    public function plantilla_cominicacion() {
        return $this->hasOne('App\PlantillaComunicacion', 'plantilla_id', 'plantilla_comunicacion');
    }

}
