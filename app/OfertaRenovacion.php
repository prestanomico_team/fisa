<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfertaRenovacion extends Model
{
    protected $table = 'Tb1_OfertasRenov_MR';
    protected $connection = 'mysql_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'id_prospect',
        'id_solic',
        'Id_Oferta',
        'Monto_Oferta1',
        'Tasa_Oferta1',
        'Plazo_Oferta1',
        'Pago_Oferta1',
        'Monto_Oferta2',
        'Tasa_Oferta2',
        'Plazo_Oferta2',
        'Pago_Oferta2',
        'Vigencia',
        'Id_Transaccion',
        'Fecha_Creacion'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
