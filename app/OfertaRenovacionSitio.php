<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modelo que se conecta con la tabla ofertas_renovacion
 * Guarda los datos de las ofertas de renovacion de monte de piedad
 */
class OfertaRenovacionSitio extends Model
{
    use SoftDeletes;

    protected $table = 'ofertas_renovacion';
    protected $dates = ['deleted_at'];

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'uuid',
        'solicitud_id',
        'id_oferta',
        'rfc',
        'monto_oferta1',
        'tasa_oferta1',
        'plazo_oferta1',
        'pago_oferta1',
        'monto_oferta2',
        'tasa_oferta2',
        'plazo_oferta2',
        'pago_oferta2',
        'vigencia',
        'id_transaccion',
        'fecha_creacion'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

    /**
     *  Setup model event hooks
     */
    public static function boot() {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

}
