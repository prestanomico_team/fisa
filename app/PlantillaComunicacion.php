<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantillaComunicacion extends Model
{
    protected $table = 'plantillas_comunicaciones';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'id_plantilla',
        'modal_encabezado',
        'modal_img',
        'modal_cuerpo',
        'sms',
        'email_asunto',
        'email_cuerpo'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
