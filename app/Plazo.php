<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo que se conecta con la tabla plazos
 */
class Plazo extends Model
{
    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'plazo',
        'duracion',
    ];
}
