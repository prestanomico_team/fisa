<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class ProductoCobertura extends Model
{
    protected $table = 'catalogo_sepomex';
    protected $primaryKey = 'codigo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}