<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Encryption\DecryptException;

class Prospecto extends Authenticatable
{
    protected $encryptable = ['nombres', 'apellido_paterno', 'apellido_materno'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres',
		'apellido_paterno',
		'apellido_materno',
		'email',
		'celular',
		'password',
        'usuario_ldap',
        'sms_verificacion',
        'acepto_avisop',
        'usuario_confirmado',
        'password_temporal',
        'referencia',
        'encryptd',
        'prospecto_fmp_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Obtiene las solicitudes del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function solicitudes() {
        return $this->hasMany('App\Solicitud', 'prospecto_id', 'id')
            ->select('id', 'prospecto_id', 'status', 'sub_status')
            ->orderBy('id', 'desc')
            ->limit(1);
    }

    /**
     * Obtiene la ultima solicitud modificada del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function ultima_solicitud() {

        return $this->hasOne('App\Solicitud', 'prospecto_id', 'id')
            ->select('id', 'prospecto_id', 'status', 'sub_status', 'prestamo', 'plazo', 'finalidad')
            ->orderBy('updated_at', 'desc');
    }

    /**
     * Obtiene las solicitudes del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function solicitudesV2() {
        return $this->hasMany('App\Solicitud', 'prospecto_id', 'id')
        ->select(
            'id',
            'prospecto_id',
            'status',
            'sub_status',
            'sexo',
            'fecha_nacimiento',
            'lugar_nacimiento_ciudad',
            'lugar_nacimiento_estado',
            'rfc',
            'curp',
            'telefono_casa',
            'estado_civil',
            'nivel_estudios',
            'autorizado',
            'encontrado',
            'user_ip',
            'finalidad_custom',
            'ocupacion',
            'fuente_ingresos',
            'credito_hipotecario',
            'credito_automotriz',
            'credito_bancario',
            'ultimos_4_digitos',
            'tipo_residencia',
            'ingreso_mensual',
            'antiguedad_empleo',
            'telefono_empleo',
            'antiguedad_domicilio',
            'gastos_familiares',
            'numero_dependientes',
            'prestamo',
            'plazo',
            'finalidad',
            'pago_estimado',
            'created_at',
            'updated_at'
        )
            ->orderBy('id', 'desc');
    }


    public function attributesToArray() {
        $attributes = parent::attributesToArray();
        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])){
                try {
                    $attributes[$key] = decrypt($attributes[$key]);
                } catch (DecryptException $e) {
                    $attributes[$key] = $attributes[$key];
                }
            }
        }
        return $attributes;
    }


    public function getAttribute($key) {
        $value = parent::getAttribute($key);
        if (in_array($key, $this->encryptable) && $value != '') {
            try {
                $value = decrypt($value);
            } catch (DecryptException $e) {
                $value = $value;
            }
            return $value;

        }
        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable) && $value != '') {
            $value = encrypt($value);
        }
        return parent::setAttribute($key, $value);
    }

    public function nombreCompleto() {
        if ($this->apellido_materno != '') {
            return "{$this->nombres} {$this->apellido_paterno} {$this->apellido_materno}";
        } else {
            return "{$this->nombres} {$this->apellido_paterno}";
        }
    }

}
