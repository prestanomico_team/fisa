<?php

namespace App\Providers;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Events\JobFailed;
use App\Http\Controllers\FuncionesT24;
use App\Http\Controllers\serverMethods;
use App\Http\Controllers\cleanStrMethods;
use Log;
use Validator;
use Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use App\Jobs\ProcesaFaceMatch;
use App\Jobs\ProcesaCargaIdentificacionSelfie;
use App\Jobs\AltaReferencias;
use App\Jobs\AltaCuentaClabe;
use App\Jobs\ComprobantesDomicilio;
use App\Jobs\ComprobantesIngreso;
use App\ClientesAlta;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    use FuncionesT24, serverMethods, cleanStrMethods;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
                /**
         * Evento que se ejecuta al termino del procesamiento de un Job de la
         * cola.
         */
        Queue::after(function (JobProcessed $event) {

            // Obteniendo el nombre del Job
            $job = $event->job->resolveName();

            if (strpos($job, 'AltaClienteAutomatica') !== false) {

                $payload = $event->job->getRawBody();
                $payload = json_decode($payload);
                $data = $payload->data;
                $command = (array) unserialize($data->command);
                $datosAltaCliente = json_decode($command['' . "\0" . '*' . "\0" . 'clienteAlta']);

                if ($datosAltaCliente->aplica_solicitud == 1) {
                    $altaSolicitud = self::altaSolicitudT24(
                        $datosAltaCliente->prospecto_id,
                        $datosAltaCliente->solicitud_id,
                        $datosAltaCliente->id
                    );
                }

            }

            if (strpos($job, 'AltaSolicitudAutomatica') !== false) {

                $payload = $event->job->getRawBody();
                $payload = json_decode($payload);
                $data = $payload->data;
                $command = (array) unserialize($data->command);
                $datosAltaCliente = json_decode($command['' . "\0" . '*' . "\0" . 'clienteAlta']);

                $altaSolicitud = self::ligueUsuarioLDAP(
                    $datosAltaCliente->id
                );

                if ($datosAltaCliente->aplica_facematch == 1 && $datosAltaCliente->facematch === 0) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ProcesaFaceMatch($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                if ($datosAltaCliente->solo_carga_identificacion_selfie == 1 && $datosAltaCliente->facematch === 0) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                // Verificando si aplica referencias, hubo un errror al darlas de alta y el error es por que la
                // solicitud T24 aún no existe
                if ($datosAltaCliente->aplica_referencias == 1 && $datosAltaCliente->alta_referencias === 0
                    && strpos($datosAltaCliente->referencias_error, 'La solicitud en T24 aún no existe') !== false) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new AltaReferencias($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                // Verificando si aplica cuenta clabe, hubo un errror al darla de alta y el error es por que la
                // solicitud T24 aún no existe
                if ($datosAltaCliente->aplica_cuenta_clabe == 1 && $datosAltaCliente->alta_cuenta_clabe === 0
                    && strpos($datosAltaCliente->cuenta_clabe_error, 'La solicitud en T24 aún no existe') !== false) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new AltaCuentaClabe($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                // Verificando si aplica comprobante domicilio, hubo un errror al darlo de alta y el error es por que la
                // solicitud T24 aún no existe
                if ($datosAltaCliente->aplica_comprobante_domicilio == 1 && $datosAltaCliente->alta_comprobante_domicilio === 0
                    && strpos($datosAltaCliente->comprobante_domicilio_error, 'La solicitud en T24 aún no existe') !== false) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ComprobantesDomicilio($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                // Verificando si aplica comprobante ingresos, hubo un errror al darlo de alta y el error es por que la
                // solicitud T24 aún no existe
                if ($datosAltaCliente->aplica_comprobante_ingresos == 1 && $datosAltaCliente->alta_comprobante_ingresos === 0
                    && strpos($datosAltaCliente->comprobante_ingresos_error, 'La solicitud en T24 aún no existe') !== false) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ComprobantesIngreso($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                if ($datosAltaCliente->aplica_email == 1) {
                    $altaSolicitud = self::EnvioEmail(
                        $datosAltaCliente->id
                    );
                }

            }

        });

        // Validación customizada que permite solo letras y espacios
        Validator::extend('alpha_numeric_spaces', function ($attribute, $value) {

            return preg_match('/[\p{L}\p{N} .-]+$/u', $value);

        });

        Validator::extend('alpha_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'.\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('validacion_combos', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*+=\\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N} ,.()]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N}]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });


        // Validación customizada que verifica si existe el plazo
        Validator::extend('unique_duracion_plazo', function ($attribute, $value, $parameters) {

            $plazo = \DB::select("SELECT count(*) as total
            FROM plazos
            WHERE duracion ='$value' AND plazo='$parameters[0]'");

            if ($plazo[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Validación customizada que verifica si existe el rol/perfil
        Validator::extend('unique_rol', function ($attribute, $value, $parameters) {

            $rol = \DB::select("SELECT count(*) as total
            FROM roles
            WHERE name LIKE '%$parameters[0]%'");

            if ($rol[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Validación de imagen en base 64
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'jpeg', 'svg', 'bmp', 'gif'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                [
                    '', '', '',
                ],
                $explode[0]
            );

            // check file format
            if (!in_array($format, $allow)) {
                return false;
            }

            // check base64 format
            if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                return false;
            }

            return true;
        });

        Validator::extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/[a-f0-9]{8}\-[a-f0-9]{4}\-4[a-f0-9]{3}\-(8|9|a|b)[a-f0-9]{3}\-[a-f0-9]{12}/', $value);
        });

        // Agregando sftp como Storage Driver
        /* Storage::extend('sftp', function ($app, $config) {
            return new Filesystem(new SftpAdapter($config));
        }); */

        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });

        // Agragando ReCaptcha para portales
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
    }
}
