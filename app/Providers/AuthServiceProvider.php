<?php

namespace App\Providers;

//use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Carbon\Carbon;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /* Passport::routes();

        Passport::enableImplicitGrant();

        Passport::tokensExpireIn(Carbon::now()->addDays(365));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(365)); */

        Auth::provider('prospecto', function($app, array $config) {
            return new ProspectoServiceProvider($app['hash'], $config['model']);
        });
    }
}
