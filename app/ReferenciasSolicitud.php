<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenciasSolicitud extends Model
{
    protected $table = 'referencias_solicitud';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'solicitud_id',
        'primer_nombre_ref1',
        'segundo_nombre_ref1',
        'apellido_paterno_ref1',
        'apellido_materno_ref1',
        'tipo_relacion_ref1',
        'telefono_ref1',
        'primer_nombre_ref2',
        'segundo_nombre_ref2',
        'apellido_paterno_ref2',
        'apellido_materno_ref2',
        'tipo_relacion_ref2',
        'telefono_ref2',
        'primer_nombre_ref3',
        'segundo_nombre_ref3',
        'apellido_paterno_ref3',
        'apellido_materno_ref3',
        'tipo_relacion_ref3',
        'telefono_ref3'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
