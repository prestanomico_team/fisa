<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReporteEficiencia extends Model
{
    protected $table = 'datos_reporte_eficiencia';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'Producto',
        'Origen',
        'Anio',
        'Mes',
        'Dia',
        'TipoSolicitud',
        'RangoBCScore',
        'RangoBCMicro',
        'RangoBCMicro_V3',
        'TipoCasosDetallado',
        'TipoCasosRepEC',
        'AntesRelease25May',
        'MontoF2_Mod',
        'Fec_Actualizacion',
        'Creado',
        'Actualizado'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
