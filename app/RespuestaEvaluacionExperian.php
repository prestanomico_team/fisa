<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaEvaluacionExperian extends Model
{
    protected $table = 'respuesta_evaluacion_experian';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'ejecucion_experian',
        'status_ejecucion_experian',
        'modeloEvaluacion',
        'decision',
        'plantilla_comunicacion',
        'pantallas_extra',
        'situaciones',
        'cuestionario_dinamico_guardado',
        'status_guardado',
        'status_oferta',
        'motivo_rechazo',
        'descripcion_otro',
        'oferta_minima',
        'simplificado',
        'facematch' ,
        'carga_identificacion_selfie',
        'subir_documentos',
        'elegida',
        'oferta_id',
        'tipo_poblacion',
        'tipo_oferta',
        'monto',
        'plazo',
        'pago',
        'tasa',
        'tipo_tasa'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Obtiene los datos de la plantilla de comuniaciones
     *
     * @return object Datos de la plantilla de comunicaciones
     */
    public function plantilla_cominicacion() {
        return $this->hasOne('App\PlantillaComunicacion', 'plantilla_id', 'plantilla_comunicacion');
    }

}
