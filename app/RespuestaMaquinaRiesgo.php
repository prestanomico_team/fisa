<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaMaquinaRiesgo extends Model
{
    protected $table = 'respuestas_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'ejecucion_sp',
        'status_ejecucion_sp',
        'decision',
        'stored_procedure',
        'status_oferta',
        'motivo_rechazo',
        'descripcion_otro',
        'plantilla_comunicacion',
        'pantallas_extra',
        'situaciones',
        'tipo_poblacion',
        'tipo_oferta',
        'monto',
        'plazo',
        'pago',
        'tasa',
        'tipo_tasa',
        'cuestionario_dinamico_guardado',
        'status_guardado',
        'motivo_rechazo',
        'descripcion_otro',
        'oferta_minima',
        'simplificado',
        'facematch',
        'carga_identificacion_selfie',
        'subir_documentos',
        'elegida'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Obtiene los datos de la plantilla de comuniaciones
     *
     * @return object Datos de la plantilla de comunicaciones
     */
    public function plantilla_cominicacion() {
        return $this->hasOne('App\PlantillaComunicacion', 'plantilla_id', 'plantilla_comunicacion');
    }

    /**
     * Obtiene los datos del tipo de tasa
     *
     * @return object Datos del tipo de tasa
     */
    public function tipo_tasa() {
        return $this->hasOne('App\TipoTasa', 'solic_id', 'solicitud_id');
    }
}
