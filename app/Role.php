<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

/**
 * Modelo que se conecta con la tabla roles
 */
class Role extends EntrustRole
{

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'name',
        'display_name',
        'area',
        'description',
    ];

    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }
    
    public function users() {
        return $this->belongsToMany(User::class);
    }
}
