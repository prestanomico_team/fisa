<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla catalogos_t24
 */
class Sepomex extends Model
{
    protected $table = 'catalogo_sepomex';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [

    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
