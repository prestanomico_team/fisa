<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;

class Solicitud extends Model
{
    protected $columns = array('id','prospecto_id','status');
    protected $table = 'solicitudes';
    protected $encryptable = ['sexo', 'lugar_nacimiento_estado', 'lugar_nacimiento_ciudad',
    'estado_civil', 'nivel_estudios', 'rfc', 'curp', 'ultimos_4_digitos', 'ocupacion',
    'fuente_ingresos', 'numero_dependientes', 'telefono_empleo', 'telefono_casa'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'sub_status',
        'prospecto_id',
        // datos personales
		'sexo',
		'fecha_nacimiento',
		'lugar_nacimiento_ciudad',
		'lugar_nacimiento_estado',
        'rfc',
        'curp',
        'telefono_casa',
        'estado_civil',
        // datos buro
        'credito_hipotecario',
        'credito_automotriz',
        'credito_bancario',
        'ultimos_4_digitos',
        'autorizado',
        'encontrado',
        // datos ingresos
        'ocupacion',
        'fuente_ingresos',
        'ingreso_mensual',
        'nivel_estudios',
        'tipo_residencia',
        'antiguedad_empleo',
        'antiguedad_domicilio',
        'gastos_familiares',
        'numero_dependientes',
        'telefono_empleo',
        // datos simulador
        'plazo',
        'prestamo',
        'finalidad',
        'pago_estimado',
        'finalidad_custom',
        'user_ip',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Obtiene el prospecto de cada solicitud
     *
     * @return object Datos del prospecto
     */
    public function prospecto() {
        return $this->hasOne('App\Prospecto', 'id', 'prospecto_id');
    }

    /**
     * Obtiene el domicilio de cada solicitud
     *
     * @return object Datos del domicilio
     */
    public function domicilio() {
        return $this->hasOne('App\DomicilioSolicitud', 'solicitud_id', 'id');
    }

    /**
     * Obtiene la respuesta de la máquina de riesgos de cada solicitud
     *
     * @return object Respuesta de la máquina de riesgos
     */
    public function respuesta_maquina_riesgos() {
        return $this->hasMany('App\RespuestaMaquinaRiesgo', 'solicitud_id', 'id');
    }

    /**
     * Obtiene la respuesta del modelo predominante de cada solicitud
     *
     * @return object Respuesta de la máquina de riesgos
     */
    public function respuesta_modelo_predominante() {
        return $this->hasMany('App\OfertaPredominante', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el producto al que pertenece la solicitud
     *
     * @return object Datos del producto
     */
    public function producto() {
        return $this->belongsToMany('App\Producto')->withPivot('version_producto', 'lead', 'lead_id');
    }

    /**
     * Obtiene los datos adicionales de cada solicitud
     *
     * @return object Datos Adicionales
     */
    public function datos_adicionales() {
        return $this->hasOne('App\DatosAdicionales', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos del empleo de cada solicitud
     *
     * @return object Datos Empleo
     */
    public function datos_empleo() {
        return $this->hasOne('App\DatosEmpleo', 'solicitud_id', 'id');
    }

    /**
     * Obtiene las consultas que el prospecto ha realizado a buro de crédito
     *
     * @return object Consultas Buro
     */
    public function consultas_buro() {
        return $this->hasMany('App\ConsultasBuro', 'solicitud_id', 'id');
    }

    /**
     * Obtiene la respuesta de la segunda llamada a buro
     *
     * @return object Consultas Buro
     */
    public function respuesta_segunda_llamadabc() {
        return $this->hasMany('App\ConsultasBuro', 'solicitud_id', 'id')
            ->select('solicitud_id', 'folio_consulta')
            ->where('orden', 'Segunda')
            ->where('tipo', 'Respuesta');
    }

    /**
     * Obtiene la cadena de respuesta a la segunda llamada de BC
     *
     * @return object Consultas Buro
     */
    public function cadena_respuesta_segunda_llamadabc() {
        return $this->hasMany('App\ConsultasBuro', 'solicitud_id', 'id')
            ->select('solicitud_id', 'cadena_original')
            ->where('orden', 'Segunda')
            ->where('tipo', 'Respuesta');
    }

    /**
     * Obtiene las direcciones reportadas a buro de crédito
     *
     * @return object BCDirecciones
     */
    public function bc_direcciones() {
        return $this->hasMany('App\BCDirecciones', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos personales reportadas a buro de crédito
     *
     * @return object BCEmpleos
     */
    public function bc_datos_personales() {
        return $this->hasMany('App\BCDatosPersonales', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los empleos reportadas a buro de crédito
     *
     * @return object BCEmpleos
     */
    public function bc_empleos() {
        return $this->hasMany('App\BCEmpleos', 'solicitud_id', 'id');
    }

    /**
     * Obtiene las consultas de buro de crédito
     *
     * @return object BCConsultas
     */
    public function bc_consultas() {
        return $this->hasMany('App\BCConsultas', 'solicitud_id', 'id');
    }

    /**
     * Obtiene las cuentas de buro de crédito
     *
     * @return object BCCuentas
     */
    public function bc_cuentas() {
        return $this->hasMany('App\BCCuentas', 'solicitud_id', 'id');
    }

    /**
     * Obtiene las alertas hawk de buro de crédito
     *
     * @return object BCHawk
     */
    public function bc_hawk() {
        return $this->hasMany('App\BCHawk', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el resumen de buro de crédito
     *
     * @return object BCResumenBuro
     */
    public function bc_resumen_buro() {
        return $this->hasOne('App\BCResumenBuro', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los scores de buro de crédito
     *
     * @return object BCScores
     */
    public function bc_score() {
        return $this->hasOne('App\BCScores', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos de la consulta ALP
     *
     * @return object ALP
     */
    public function alp() {
        return $this->hasOne('App\Alp', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos del Convenio
     *
     * @return object Datos del convenio
     */
    public function convenio() {
        return $this->hasOne('App\DatosConvenio', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el tracking de la solcitud
     *
     * @return object Datos del tracking
     */
    public function tracking() {
        return $this->hasMany('App\TrackingSolicitud', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los analytics de la solcitud
     *
     * @return object Datos de analytics
     */
    public function analytic() {
        return $this->hasMany('App\Analytic', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el ultimo dispositivo con el que se capturan datos de la solicitud
     *
     * @return object Datos de analytics
     */
    public function ultimoDispositivo() {
        return $this->hasOne('App\Analytic', 'solicitud_id', 'id')
            ->select('solicitud_id', 'id', 'tipo_dispositivo', 'sistema_operativo', 'marca')
            ->latest();
    }

    /**
     * Obtiene el primer dispositivo con el que se capturan datos de la solicitud
     *
     * @return object Datos de analytics
     */
    public function primerDispositivo() {
        return $this->hasOne('App\Analytic', 'solicitud_id', 'id')
            ->select('solicitud_id', 'id', 'tipo_dispositivo', 'sistema_operativo', 'marca')
            ->oldest();
    }

    public function scopeWithLastLogin($query) {

        $subselect = Analytic::where('solicitud_id','id')
            ->latest()
            ->get();
        $query->addSelect([
            'last_login' => $subselect,
        ]);
    }

    /**
     * Obtiene el log de la solcitud
     *
     * @return object Datos del log
     */
    public function log() {
        return $this->hasOne('App\LogSolicitud', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el registro de clientes alta de la solicitud
     *
     * @return object Datos de clientes alta
     */
    public function clientes_alta() {
        return $this->hasOne('App\ClientesAlta', 'solicitud_id', 'id');
    }

    /**
     * Excluye campos de la consulta
     *
     * @param  query  $query  Consulta realizada
     * @param  array  $value  Campos a excluir
     *
     * @return result         Resultado del query sin los campos que se desean excluir
     */
    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff( $this->columns,(array) $value) );
    }

    public function attributesToArray() {
        $attributes = parent::attributesToArray();

        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])){
                try {
                    $attributes[$key] = decrypt($attributes[$key]);
                } catch (DecryptException $e) {
                    $attributes[$key] = $attributes[$key];
                }
            }
        }

        return $attributes;
    }

    public function getAttribute($key) {

        $value = parent::getAttribute($key);
        if (in_array($key, $this->encryptable) && $value != '') {
            try {
                $value = decrypt($value);
            } catch (DecryptException $e) {
                $value = $value;
            }
        }
        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable) && $value != '') {
            $value = encrypt($value);
        }
        return parent::setAttribute($key, $value);
    }

    public function getEncryptable() {
      return $this->encryptable;
    }

}
