<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Modelo que se conecta con la tabla catalogos_t24
 */
class StatusT24 extends Model
{
    use Notifiable;

    protected $table = 'status_t24';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [

    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];


}
