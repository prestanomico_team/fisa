<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla Tb1_CtasModAcep_MR
 * de la máquina de riesgos
 */
class Tb1_CtasModAcep_MR extends Model
{
    protected $connection = 'mysql_maquina_riesgos';
    protected $table = 'Tb1_CtasModAcep_MR';
    public $timestamps = false;

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [
        
    ];

}
