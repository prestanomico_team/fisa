<?php

namespace App;

use Illuminate\Support\Facades\Redis;

class TokenExperian
{
    public function __construct($token, $expires_at, $valido) {
       $this->token         = $token;
       $this->expires_at    = $expires_at;
       $this->valido        = $valido;
    }

    public function store() {
        $key = 'token_'.env('APP_ENV');
        Redis::hmset($key, [
            'token'         => $this->token,
            'expires_at'    => $this->expires_at,
            'valido'        => $this->valido
        ]);
    }

    public static function find() {
        $key = 'token_'.env('APP_ENV');
        $stored = Redis::hgetall($key);
        if (!empty($stored)) {
            return new TokenExperian($stored['token'], $stored['expires_at'], $stored['valido']);
        }
        return false;
    }

}
