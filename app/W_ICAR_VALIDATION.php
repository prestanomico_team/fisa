<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_ICAR_VALIDATION
 * Guarda los datos que se obtuvieron de la validacion del Facematch en la tabla
 * del app
 */
class W_ICAR_VALIDATION extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_ICAR_VALIDATION';
    public $timestamps = false;
    protected $primaryKey = 'ID_ICAR_VALIDATION';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_LOAN_APP',
        'STATUS_RESPONSE',
        'TEST_FACE_REC_VAL',
        'TEST_EXPIRY_DATE',
        'TEST_LOBAL_ATUH_RAT',
        'TEST_LOBAL_ATUH_VAL',
        'OBSERVATIONS',
        'EXECUTION_DATE',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
