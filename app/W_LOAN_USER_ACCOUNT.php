<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_LOAN_USER_ACCOUNT
 * Guarda los datos de la solicitud en la tabla del app
 */
class W_LOAN_USER_ACCOUNT extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_LOAN_USER_ACCOUNT';
    public $timestamps = false;
    protected $primaryKey = 'ID_LOAN_USER_ACCOUNT';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_LOAN_USER_ACCOUNT',
        'ID_BANK_ACCOUNT_TYPE',
        'ID_LOAN_APP',
        'ID_ACCOUNT_TYPE',
        'NAME',
        'ACCOUNT',
        'BANK_NAME',
        'BANK_KEY',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
