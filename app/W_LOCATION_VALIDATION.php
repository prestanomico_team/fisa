<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_LOCATION_VALIDATION
 * Guarda los datos de la validación del domicilio
 * del app
 */
class W_LOCATION_VALIDATION extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_LOCATION_VALIDATION';
    public $timestamps = false;
    protected $primaryKey = 'ID_LOCATION_VALIDATION';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_LOAN_APP',
        'GEO_RESPONSE',
        'MESSAGE',
        'USER_ADDRESS',
        'LATITUDE',
        'LONGITUDE',
        'EXECUTION_DATE',
        'REVERSE_USER_ADDRESS',
        'LATITUDE_REVERSE',
        'LONGITUDE_REVERSE',
        'DEVICE',
        'BROWSER',
        'PLATFORM',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
