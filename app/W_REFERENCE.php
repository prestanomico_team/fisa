<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_REFERENCE
 */
class W_REFERENCE extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_REFERENCE';
    public $timestamps = false;
    protected $primaryKey = 'ID_REFERENCE';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_REFERENCE',
        'ID_CUSTOMER_RELATION_TYPE',
        'ID_LOAN_APP',
        'NAME',
        'LAST_NAME',
        'PHONE_NUMBER',
        'PHONE_NUMBER_HOME',
        'CONFIRMED',
        'ID_T24',
        'CONSECUTIVE',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
