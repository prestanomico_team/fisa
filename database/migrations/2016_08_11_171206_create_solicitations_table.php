<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->string('status', 100);
            $table->string('sub_status', 100);
            $table->longText('sexo');
            $table->date('fecha_nacimiento');
            $table->longText('lugar_nacimiento_ciudad');
            $table->longText('lugar_nacimiento_estado');
            $table->longText('rfc');
            $table->longText('curp');
            $table->longText('telefono_casa');
            $table->longText('estado_civil');
            $table->boolean('credito_hipotecario');
            $table->boolean('credito_automotriz');
            $table->boolean('credito_bancario');
            $table->longText('ultimos_4_digitos');
            $table->boolean('autorizado');
            $table->boolean('encontrado');
            $table->longText('ocupacion', 50);
            $table->longText('fuente_ingresos', 50);
            $table->integer('ingreso_mensual');
            $table->longText('nivel_estudios', 50);
            $table->string('tipo_residencia', 45);
            $table->integer('antiguedad_empleo');
            $table->integer('antiguedad_domicilio');
            $table->integer('gastos_familiares');
            $table->longText('numero_dependientes');
            $table->longText('telefono_empleo', 20);
            $table->integer('prestamo');
            $table->integer('plazo');
            $table->string('finalidad', 150);
            $table->text('finalidad_custom');
            $table->float('pago_estimado');
            $table->string('user_ip',15);
            $table->timestamps();

            $table->index(['prospecto_id'], 'prospecto_id');
            $table->index(['status', 'sub_status'], 'status_substatus');
            $table->index(['prospecto_id', 'status', 'sub_status'], 'prospecto_status_substatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitudes');
    }
}
