<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosT24Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos_t24', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('valor_t24', 10);
            $table->string('valor_sitio', 50);
            $table->string('campo_webservice', 50);
            $table->timestamps();

            $table->index(['valor_sitio'], 'valor_sitio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catalogos_t24');
    }
}
