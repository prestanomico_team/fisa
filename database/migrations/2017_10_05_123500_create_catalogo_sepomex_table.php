<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoSepomexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogo_sepomex', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo', 5);
            $table->string('colonia_asentamiento', 250);
            $table->string('tipo_asentamiento', 50);
            $table->string('municipio', 100);
            $table->string('estado', 100);
            $table->string('ciudad', 100);
            $table->string('id_estado', 2);
            $table->string('id_municipio', 5);
            $table->string('id_colonia', 5);
            $table->string('id_ciudad', 2);
            $table->timestamps();

            $table->index(['codigo'], 'codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catalogo_sepomex');
    }
}
