<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModifyFieldsToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->renameColumn('alta', 'alta_cliente');
            $table->string('no_cliente_t24', 20)->after('alta')->nullable();
            $table->boolean('alta_solicitud')->default(0)->after('no_cliente_t24')->nullable();
            $table->string('no_solicitud_t24', 20)->after('alta_solicitud')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'no_cliente_t24',
                'alta_solicitud',
                'no_solicitud_t24'
            ]);
        });
    }
}
