<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomiciliosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilio_solicitud', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->unsignedBigInteger('prospecto_id');
           $table->unsignedBigInteger('solicitud_id');
           $table->string('cp', 5);
           $table->string('calle', 150);
           $table->string('num_exterior', 30);
           $table->string('num_interior', 30)->nullable();
           $table->string('colonia', 150);
           $table->string('id_colonia', 20);
           $table->string('delegacion', 150);
           $table->string('id_delegacion', 10);
           $table->string('ciudad', 150)->nullable();
           $table->string('id_ciudad', 10)->nullable();
           $table->string('estado', 150);
           $table->string('id_estado', 10);
           $table->timestamps();

           $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
           $table->index(['solicitud_id'], 'solicitud');
       });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domicilio_solicitud');
    }
}
