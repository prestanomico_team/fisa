<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoberturaToCatalogoSepomexTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalogo_sepomex', function (Blueprint $table) {
            $table->boolean('cobertura')->after('id_ciudad')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalogo_sepomex', function (Blueprint $table) {
            $table->dropColumn('cobertura');
        });
    }
}
