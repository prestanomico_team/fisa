<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoberturaToDomicilioSolicitudTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domicilio_solicitud', function (Blueprint $table) {
            $table->boolean('cobertura')->after('codigo_estado')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domicilio_solicitud', function (Blueprint $table) {
            $table->dropColumn('cobertura');
        });
    }
}
