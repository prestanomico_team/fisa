<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleCuentasMr extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_cuentas_mr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ID_PROSPECT');
            $table->unsignedBigInteger('ID_SOLIC');
            $table->integer('NO_CUENTA');
            $table->string('CUENTA_FECHA_ACTUALIZACION', 8)->nullable();
            $table->string('CUENTA_IMPUGNADO', 4)->nullable();
            $table->string('CUENTA_MEMBER_CODE', 10)->nullable();
            $table->string('CUENTA_NOMBRE_USUARIO', 16)->nullable();
            $table->string('CUENTA_NUM_TEL', 11)->nullable();
            $table->string('CUENTA_NUM_CUENTA', 25)->nullable();
            $table->string('CUENTA_RESPONSABILIDAD', 1)->nullable();
            $table->string('CUENTA_TIPO', 1)->nullable();
            $table->string('CUENTA_CONTRATO_PRODUCTO', 2)->nullable();
            $table->string('CUENTA_MONEDA', 2)->nullable();
            $table->integer('CUENTA_IMPORTE_EVALUO')->nullable();
            $table->integer('CUENTA_NUM_PAGOS')->nullable();
            $table->string('CUENTA_FRECUENCIA_PAGOS', 1)->nullable();
            $table->integer('CUENTA_MONTO_PAGAR')->nullable();
            $table->string('CUENTA_FECHA_APERTURA', 8)->nullable();
            $table->string('CUENTA_FECHA_ULT_PAGO', 8)->nullable();
            $table->string('CUENTA_FECHA_ULT_COMPRA', 8)->nullable();
            $table->string('CUENTA_FECHA_CIERRE', 8)->nullable();
            $table->string('CUENTA_FECHA_REPORTE', 8)->nullable();
            $table->string('CUENTA_MODO_REPORTE', 1)->nullable();
            $table->string('CUENTA_ULT_FECHA_CERO', 8)->nullable();
            $table->string('CUENTA_GARANTIA', 40)->nullable();
            $table->integer('CUENTA_CRED_MAX_AUT')->nullable();
            $table->integer('CUENTA_SALDO_ACTUAL')->nullable();
            $table->integer('CUENTA_LIMITE_CREDITO')->nullable();
            $table->integer('CUENTA_SALDO_VENCIDO')->nullable();
            $table->integer('CUENTA_NUM_PAGOS_VENCIDOS')->nullable();
            $table->string('CUENTA_MOP', 2)->nullable();
            $table->string('CUENTA_HIST_PAGOS', 24)->nullable();
            $table->string('CUENTA_HIST_PAGOS_FECHA_RECIENTE', 8)->nullable();
            $table->string('CUENTA_HIST_PAGOS_FECHA_ANTIGUA', 8)->nullable();
            $table->string('CUENTA_CLAVE_OBSERVACION', 2)->nullable();
            $table->integer('CUENTA_TOTAL_PAGOS')->nullable();
            $table->integer('CUENTA_TOTAL_PAGOS_MOP2')->nullable();
            $table->integer('CUENTA_TOTAL_PAGOS_MOP3')->nullable();
            $table->integer('CUENTA_TOTAL_PAGOS_MOP4')->nullable();
            $table->integer('CUENTA_TOTAL_PAGOS_MOP5_PLUS')->nullable();
            $table->integer('CUENTA_SALDO_MOROSIDAD_MAS_ALTA')->nullable();
            $table->string('CUENTA_FECHA_MOROSIDAD_MAS_ALTA', 8)->nullable();
            $table->string('CUENTA_CLASIF_PUNTUALIDAD_DE_PAGO', 2)->nullable();
            $table->string('CUENTA_FECHA_INICIO_REESTRUCTURA', 8)->nullable();
            $table->integer('CUENTA_MONTO_ULTIMO_PAGO')->nullable();
            $table->timestamps();

            $table->index(['ID_PROSPECT', 'ID_SOLIC'], 'prospecto_solicitud');
        });


    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_cuentas_mr');
    }
}
