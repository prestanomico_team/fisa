<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleSolicitudMr extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_solicitudes_mr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ID_PROSPECT');
            $table->unsignedBigInteger('ID_SOLIC');
            $table->datetime('FECHA_REGISTRO')->nullable();
            $table->string('STATUS', 50)->nullable();
            $table->datetime('ULT_ACT')->nullable();
            $table->integer('PRESTAMO')->nullable();
            $table->string('PLAZO', 50)->nullable();
            $table->string('FINALIDAD', 50)->nullable();
            $table->string('NOS_GUSTARIA', 50)->nullable();
            $table->string('NOMBRE', 100)->nullable();
            $table->string('APELLIDO_P', 50)->nullable();
            $table->string('APELLIDO_M', 50)->nullable();
            $table->string('GENERO', 10)->nullable();
            $table->date('FEC_NACIMIENTO')->nullable();
            $table->string('LUGAR_NACIMIENTO', 200)->nullable();
            $table->integer('EDAD')->nullable();
            $table->string('EMAIL', 100)->nullable();
            $table->string('CELULAR', 20)->nullable();
            $table->string('TEL_DOMICILIO', 20)->nullable();
            $table->string('TEL_OFICINA', 20)->nullable();
            $table->string('RFC', 15)->nullable();
            $table->string('CURP', 20)->nullable();
            $table->string('ESTADO_CIVIL', 50)->nullable();
            $table->string('RESIDENCIA', 50)->nullable();
            $table->integer('NUM_DEPENDIENTES')->nullable();
            $table->string('OCUPACION', 50)->nullable();
            $table->integer('INGRESO')->nullable();
            $table->integer('GASTOS_FAMILIARES')->nullable();
            $table->string('CRED_HIPOTECARIO', 2)->nullable();
            $table->string('CRED_AUTOMOTRIZ', 2)->nullable();
            $table->string('CRED_TDCBANCARIO', 2)->nullable();
            $table->string('ULT4_TDC', 4)->nullable();
            $table->string('ORIGEN', 50)->nullable();
            $table->string('DOM_CALLE', 100)->nullable();
            $table->string('NUM_EXT', 25)->nullable();
            $table->string('NUM_INT', 25)->nullable();
            $table->string('DOM_COLONIA', 100)->nullable();
            $table->string('DEL_MUNIC', 100)->nullable();
            $table->string('DOM_CIUDAD', 100)->nullable();
            $table->string('DOM_ESTADO', 50)->nullable();
            $table->string('COD_POSTAL', 5)->nullable();
            $table->integer('BCSCORE')->nullable();
            $table->integer('NUM_CTAS_HIPOTECA')->nullable();
            $table->integer('NUM_CTAS_AUTOMOTRIZ')->nullable();
            $table->integer('NUM_CTAS_CONSUMO')->nullable();
            $table->double('INGRESO_DISPONIBLE')->nullable();
            $table->double('ATP')->nullable();
            $table->integer('USO_LINEAS')->nullable();
            $table->integer('MOP_02')->nullable();
            $table->integer('MOP_03')->nullable();
            $table->integer('NUM_CONSULTASBC_3M')->nullable();
            $table->integer('MICROSCORE')->nullable();
            $table->integer('ICC')->nullable();
            $table->string('IP_ADDRESS', 20)->nullable();
            $table->integer('DOM_ANIOS')->nullable();
            $table->integer('ANTIG_EMPLEO')->nullable();
            $table->integer('PEOR_MOP')->nullable();
            $table->date('FECHA_MASREC_PEOR_MOP')->nullable();
            $table->integer('NUM_M_DE_PEOR_MOP')->nullable();
            $table->integer('NUM_CONSULTA_3M')->nullable();
            $table->date('FECHA_CONSULTA_REC')->nullable();
            $table->date('FECHA_REGISTRO_BURO')->nullable();
            $table->date('FECHA_APERT_L_ANTIGUA')->nullable();
            $table->string('CVE_ACT_REP_BURO', 10)->nullable();
            $table->string('CVE_FRAUDE_REP_BURO', 10)->nullable();
            $table->string('CVE_EXTRAVIO_REP_BURO', 10)->nullable();
            $table->string('ALP', 10)->nullable();
            $table->timestamps();

            $table->index(['ID_PROSPECT', 'ID_SOLIC'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_solicitudes_mr');
    }
}
