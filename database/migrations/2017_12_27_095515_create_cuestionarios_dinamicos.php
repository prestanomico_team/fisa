<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuestionariosDinamicos extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios_dinamicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('situacion_id')->unsigned()->nullable();
            $table->longText('pregunta')->nullable();
            $table->string('tipo', 50)->nullable();
            $table->longText('opciones')->nullable();
            $table->longText('depende')->nullable();
            $table->longText('respuesta_depende')->nullable();
            $table->boolean('oculto')->nullable();
            $table->longText('grid_estilo')->nullable();
            $table->string('ancho_estilo', 10)->nullable();
            $table->timestamps();

            $table->index(['situacion_id'], 'situacion_id');
        });

    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuestionarios_dinamicos');
    }
}
