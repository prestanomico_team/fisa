<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasCuestionarioDinamicoTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_cuestionario_dinamico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->unsignedBigInteger('situacion_id');
            $table->unsignedBigInteger('pregunta_id');
            $table->mediumText('respuesta')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
            $table->index(['situacion_id'], 'situacion');
            $table->index(['pregunta_id'], 'pregunta');
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repuestas_cuestionarios_dinamicos');
    }
}
