<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsRechazoToRespuesasMaquinaRiesgosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->string('motivo_rechazo', 100)->nullable()->after('status_oferta');
            $table->string('descripcion_otro', 255)->nullable()->after('motivo_rechazo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn('motivo_rechazo');
            $table->dropColumn('descripcion_otro');
        });
    }
}
