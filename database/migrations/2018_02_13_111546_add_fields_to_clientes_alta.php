<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClientesAlta extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->boolean('aplica_cliente')->nullable()->after('INCSNDSOURCE');
            $table->datetime('alta_cliente_at')->nullable()->after('alta_cliente');
            $table->boolean('aplica_solicitud')->nullable()->after('no_cliente_t24');
            $table->datetime('alta_solicitud_at')->nullable()->after('alta_solicitud');
            $table->boolean('aplica_ligue')->nullable()->after('no_solicitud_t24');
            $table->datetime('ligue_usuario_at')->nullable()->after('usuario_ligado');
            $table->boolean('aplica_email')->nullable()->after('ligue_usuario_at');
            $table->boolean('email_enviado')->nullable()->after('aplica_email');
            $table->string('id_email', 20)->nullable()->after('email_enviado');
            $table->string('status_email', 20)->nullable()->after('id_email');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'aplica_cliente',
                'alta_cliente_at',
                'aplica_solicitud',
                'alta_solicitud_at',
                'aplica_ligue',
                'ligue_usuario_at',
                'aplica_email',
                'email_enviado',
                'id_email',
                'status_email'
            ]);
        });
    }
}
