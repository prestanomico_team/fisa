<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusT24 extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_t24', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fecha_alta', 20)->nullable();
            $table->string('fecha_actual', 10);
            $table->string('fecha_t24', 10);
            $table->boolean('status_t24');
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_t24');
    }
}
