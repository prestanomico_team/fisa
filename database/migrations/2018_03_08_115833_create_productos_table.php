<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_producto', 50);
            $table->integer('monto_minimo');
            $table->integer('monto_maximo');
            $table->integer('bc_score');
            $table->integer('edad_minima');
            $table->integer('edad_maxima');
            $table->float('cat', 8, 2);
            $table->float('tasa_minima', 8, 2);
            $table->float('tasa_maxima', 8, 2);
            $table->float('comision_apertura', 8, 2);
            $table->string('stored_procedure', 50);
            $table->text('logo');
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
