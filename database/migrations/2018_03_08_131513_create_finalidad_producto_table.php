<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinalidadProductoTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finalidad_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('finalidad_id')->unsigned();
            $table->integer('producto_id')->unsigned();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finalidad_producto');
    }
}
