<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDuracionToPlazos extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plazos', function (Blueprint $table) {
            $table->string('duracion', 20)->nullable()->after('id');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plazos', function (Blueprint $table) {
            $table->dropColumn([
                'duracion'
            ]);
        });
    }
}
