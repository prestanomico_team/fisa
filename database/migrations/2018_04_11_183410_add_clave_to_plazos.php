<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaveToPlazos extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plazos', function (Blueprint $table) {
            $table->integer('clave')->nullable()->after('id');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plazos', function (Blueprint $table) {
            $table->dropColumn([
                'clave'
            ]);
        });
    }
}
