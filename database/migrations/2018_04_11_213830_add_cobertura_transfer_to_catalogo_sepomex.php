<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoberturaTransferToCatalogoSepomex extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalogo_sepomex', function (Blueprint $table) {
            $table->boolean('cobertura_transfer')->nullable()->after('cobertura');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalogo_sepomex', function (Blueprint $table) {
            $table->dropColumn([
                'cobertura_transfer'
            ]);
        });
    }
}
