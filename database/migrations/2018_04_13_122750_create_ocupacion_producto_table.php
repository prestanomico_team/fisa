<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcupacionProductoTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupacion_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ocupacion_id');
            $table->unsignedBigInteger('producto_id');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocupacion_producto');
    }
}
