<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToProductos extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->date('vigencia_de')->nullable()->after('seguro');
            $table->date('vigencia_hasta')->nullable()->after('vigencia_de');
            $table->boolean('default')->nullable()->default(0)->after('vigencia_hasta');
            $table->string('alias', 50)->nullable()->after('nombre_producto');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn([
                'vigencia_de',
                'vigencia_hasta',
                'default',
                'alias'
            ]);
        });
    }
}
