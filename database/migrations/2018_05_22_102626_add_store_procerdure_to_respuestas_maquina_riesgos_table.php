<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreProcerdureToRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->string('stored_procedure', 50)->nullable()->after('decision');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn([
                'stored_procedure'
            ]);
        });
    }
}
