<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImgEmailHeaderToPlantillasComunicacionesTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plantillas_comunicaciones', function (Blueprint $table) {
            $table->string('img_email_header', 100)->nullable()->after('email_cuerpo');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plantillas_comunicaciones', function (Blueprint $table) {
            $table->dropColumn([
                'img_email_header'
            ]);
        });
    }
}
