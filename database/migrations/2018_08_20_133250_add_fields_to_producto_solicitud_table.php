<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProductoSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_solicitud', function (Blueprint $table) {
            $table->string('lead', 50)->nullable()->after('version_producto');
            $table->string('lead_id', 50)->nullable()->after('lead');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_solicitud', function (Blueprint $table) {
            $table->dropColumn([
                'lead',
                'lead_id'
            ]);
        });
    }
}
