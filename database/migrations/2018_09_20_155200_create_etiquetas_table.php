<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtiquetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etiquetas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('etiqueta', 2);
            $table->string('descripcion', 255);
            $table->enum('tipo', array('Envio','Respuesta'));
            $table->enum('segmento', array('AU','INTL', 'PN', 'PA', 'PE', 'PI', 'ES', 'TL', 'IQ', 'RS', 'HI', 'HR', 'CR', 'SC', 'ERRR', 'UR', 'AR'));
            $table->string('formulario', 50)->nullable();
            $table->string('buscar_valor', 25)->nullable();
            $table->timestamps();

            $table->index(['etiqueta'], 'etiqueta');
            $table->index(['tipo', 'segmento'], 'tipo_segmento');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etiquetas');
    }
}
