<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_cuentas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->integer('no_cuenta');
            $table->string('cuenta_fecha_actualizacion', 8)->nullable();
            $table->string('cuenta_impugnado', 4)->nullable();
            $table->string('cuenta_clave_member_code', 10)->nullable();
            $table->string('cuenta_nombre_usuario', 16)->nullable();
            $table->string('cuenta_num_tel', 11)->nullable();
            $table->string('cuenta_num_cuenta', 25)->nullable();
            $table->string('cuenta_responsabilidad', 1)->nullable();
            $table->string('cuenta_tipo', 1)->nullable();
            $table->string('cuenta_contrato_producto', 2)->nullable();
            $table->string('cuenta_moneda', 2)->nullable();
            $table->integer('cuenta_importe_evaluo')->nullable();
            $table->integer('cuenta_num_pagos')->nullable();
            $table->string('cuenta_frecuencia_pagos', 1)->nullable();
            $table->integer('cuenta_monto_pagar')->nullable();
            $table->string('cuenta_fecha_apertura', 8)->nullable();
            $table->string('cuenta_fecha_ult_pago', 8)->nullable();
            $table->string('cuenta_fecha_ult_compra', 8)->nullable();
            $table->string('cuenta_fecha_cierre', 8)->nullable();
            $table->string('cuenta_fecha_reporte', 8)->nullable();
            $table->string('cuenta_modo_reporte', 1)->nullable();
            $table->string('cuenta_ult_fecha_cero', 8)->nullable();
            $table->string('cuenta_garantia', 40)->nullable();
            $table->integer('cuenta_cred_max_aut')->nullable();
            $table->integer('cuenta_saldo_actual')->nullable();
            $table->integer('cuenta_limite_credito')->nullable();
            $table->integer('cuenta_saldo_vencido')->nullable();
            $table->integer('cuenta_num_pagos_vencidos')->nullable();
            $table->string('cuenta_mop', 2)->nullable();
            $table->string('cuenta_hist_pagos', 24)->nullable();
            $table->string('cuenta_hist_pagos_fecha_reciente', 8)->nullable();
            $table->string('cuenta_hist_pagos_fecha_antigua', 8)->nullable();
            $table->string('cuenta_clave_observacion', 2)->nullable();
            $table->integer('cuenta_total_pagos')->nullable();
            $table->integer('cuenta_total_pagos_mop2')->nullable();
            $table->integer('cuenta_total_pagos_mop3')->nullable();
            $table->integer('cuenta_total_pagos_mop4')->nullable();
            $table->integer('cuenta_total_pagos_mop5_plus')->nullable();
            $table->integer('cuenta_saldo_morosidad_mas_alta')->nullable();
            $table->string('cuenta_fecha_morosidad_mas_alta', 8)->nullable();
            $table->string('cuenta_clasif_puntualidad_de_pago', 2)->nullable();
            $table->string('cuenta_fecha_inicio_reestructura', 8)->nullable();
            $table->integer('cuenta_monto_ultimo_pago')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_cuentas');
    }
}
