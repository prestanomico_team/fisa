<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_consultas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->integer('no_consulta');
            $table->string('consulta_fecha', 8)->nullable();
            $table->string('consulta_reservado1', 8)->nullable();
            $table->string('consulta_clave_member_code', 10)->nullable();
            $table->string('consulta_nombre_usuario', 16)->nullable();
            $table->string('consulta_num_tel', 11)->nullable();
            $table->string('consulta_contrato_producto', 2)->nullable();
            $table->string('consulta_moneda', 2)->nullable();
            $table->integer('consulta_importe')->nullable();
            $table->string('consulta_responsabilidad', 1)->nullable();
            $table->string('consulta_indicador_cliente', 1)->nullable();
            $table->integer('consulta_reservado2')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_consultas');
    }
}
