<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcResumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_resumen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('resumen_fecha_integracion', 8)->nullable();
            $table->string('resumen_cuentas_mop7', 2)->nullable();
            $table->string('resumen_cuentas_mop6', 2)->nullable();
            $table->string('resumen_cuentas_mop5', 2)->nullable();
            $table->string('resumen_cuentas_mop4', 2)->nullable();
            $table->string('resumen_cuentas_mop3', 2)->nullable();
            $table->string('resumen_cuentas_mop2', 2)->nullable();
            $table->string('resumen_cuentas_mop1', 2)->nullable();
            $table->string('resumen_cuentas_mop0', 2)->nullable();
            $table->string('resumen_cuentas_mop_ur', 2)->nullable();
            $table->string('resumen_numero_de_cuentas', 4)->nullable();
            $table->string('resumen_cuentas_pagos_fijos_hipo', 4)->nullable();
            $table->string('resumen_cuentas_revolventes_sin_limite', 4)->nullable();
            $table->string('resumen_cuentas_cerradas', 4)->nullable();
            $table->string('resumen_cuentas_con_morosidad_actual', 4)->nullable();
            $table->string('resumen_cuentas_con_historial_de_morosidad', 4)->nullable();
            $table->string('resumen_cuentas_en_aclaracion', 2)->nullable();
            $table->string('resumen_solicitudes_de_consulta', 2)->nullable();
            $table->string('resumen_nueva_direccion_ult_60_dias', 1)->nullable();
            $table->string('resumen_mensaje_de_alerta', 8)->nullable();
            $table->string('resumen_declarativa', 1)->nullable();
            $table->string('resumen_moneda_del_credito', 2)->nullable();
            $table->string('resumen_total_creditos_max_rev_sinlim', 9)->nullable();
            $table->string('resumen_total_limites_de_credito_rev_sinlim', 9)->nullable();
            $table->string('resumen_total_saldos_actuales_rev_sinlim', 10)->nullable();
            $table->string('resumen_total_saldos_vencidos_rev_sinlim', 9)->nullable();
            $table->string('resumen_total_importe_de_pago_rev_sinlim', 9)->nullable();
            $table->string('resumen_porcentaje_limite_de_credito_utilizado_rev_sinlim', 3)->nullable();
            $table->string('resumen_total_creditos_max_fijos_hipo', 9)->nullable();
            $table->string('resumen_total_saldos_actuales_fijos_hipo', 10)->nullable();
            $table->string('resumen_total_saldos_vencidos_fijos_hipo', 9)->nullable();
            $table->string('resumen_total_importe_de_pago_fijos_hipo', 9)->nullable();
            $table->string('resumen_cuentas_mop96', 2)->nullable();
            $table->string('resumen_cuentas_mop97', 2)->nullable();
            $table->string('resumen_cuentas_mop99', 2)->nullable();
            $table->string('resumen_fecha_apertura_cuenta_antigua', 8)->nullable();
            $table->string('resumen_fecha_apertura_cuenta_reciente', 8)->nullable();
            $table->string('resumen_num_solicitudes_informe_buro', 2)->nullable();
            $table->string('resumen_fecha_consulta_reciente', 8)->nullable();
            $table->string('resumen_num_cuentas_despacho_cobranza', 2)->nullable();
            $table->string('resumen_fecha_apertura_cuenta_en_despacho_reciente', 8)->nullable();
            $table->string('resumen_num_solicitudes_informe_por_despachos', 2)->nullable();
            $table->string('resumen_fecha_consulta_por_despacho_reciente', 8)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_resumen');
    }
}
