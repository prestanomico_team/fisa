<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlpTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->longText('alp_request')->nullable();
            $table->longText('alp_response')->nullable();
            $table->text('alp_result', 255)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Alp');
    }
}
