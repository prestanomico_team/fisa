<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClienteEstrellaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_estrella', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id')->nullable();
            $table->string('numcreditoactual', 40)->nullable();
            $table->string('nombre', 50)->nullable();
            $table->string('segundonombre', 50)->nullable();
            $table->string('apellidopaterno', 50)->nullable();
            $table->string('apellidomaterno', 50)->nullable();
            $table->string('fechadenacimiento', 10)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('rfc', 13)->nullable();
            $table->string('telefono', 15)->nullable();
            $table->string('genero', 10)->nullable();
            $table->string('estadocivil', 25)->nullable();
            $table->string('estadodenacimiento', 100)->nullable();
            $table->string('gradodeestudios', 25)->nullable();
            $table->string('tipodevivienda', 25)->nullable();
            $table->string('teimpoderecidencia', 5)->nullable();
            $table->string('telefonodecasa', 15)->nullable();
            $table->string('num_dependientes', 5)->nullable();
            $table->string('gastos_familiares', 10)->nullable();
            $table->string('empresa', 50)->nullable();
            $table->string('puestoqueocupa', 25)->nullable();
            $table->string('ingresoreal', 10)->nullable();
            $table->string('tiempolaborandoenlaempresa', 5)->nullable();
            $table->string('telefonodecompania', 15)->nullable();
            $table->string('calle', 50)->nullable();
            $table->string('numerointdedomicilio', 10)->nullable();
            $table->string('numerodedomicilio', 10)->nullable();
            $table->string('colonia', 50)->nullable();
            $table->string('delegacion', 50)->nullable();
            $table->string('ciudad', 50)->nullable();
            $table->string('estado', 30)->nullable();
            $table->string('codigopostal', 5)->nullable();
            $table->string('tienecreditohipotecario', 5)->nullable();
            $table->string('tienecreditoautomotriz', 5)->nullable();
            $table->string('tienetarjeta', 5)->nullable();
            $table->string('ultimoscuatrodigitosdelatarjeta', 5)->nullable();
            $table->string('encontrado_bc', 5)->nullable();
            $table->string('monto', 10)->nullable();
            $table->string('tasa', 25)->nullable();
            $table->string('plazo', 30)->nullable();
            $table->string('pago', 10)->nullable();
            $table->string('finalidad', 30)->nullable();
            $table->boolean('procesado')->nullable();
            $table->string('uuid', 50)->nullable();
            $table->boolean('email_enviado')->nullable();
            $table->string('id_email', 25)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id'], 'prospecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_estrella');
    }
}
