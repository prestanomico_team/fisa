<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaCampanaToClienteEstrellaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cliente_estrella', function (Blueprint $table) {
            $table->date('fecha_campana')->after('prospecto_id')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cliente_estrella', function (Blueprint $table) {
            $table->dropColumn([
                'fecha_campana'
            ]);
        });
    }
}
