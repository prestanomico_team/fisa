<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosConvenioTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_convenio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solicitud_id');
            $table->bigInteger('prospecto_id');
            $table->string('sucursal');
            $table->string('embajador');
            $table->string('telefono', 15);
            $table->timestamps();
            $table->index(['solicitud_id', 'prospecto_id']);
            $table->index(['solicitud_id']);
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('datos_convenio');
    }
}
