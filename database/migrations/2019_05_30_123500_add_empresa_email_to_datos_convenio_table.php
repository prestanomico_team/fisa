<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaEmailToDatosConvenioTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_convenio', function (Blueprint $table) {
            $table->string('empresa', 255)->after('prospecto_id')->nullable();
            $table->string('email', 255)->after('telefono')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_convenio', function (Blueprint $table) {
            $table->dropColumn([
                'empresa',
                'email'
            ]);
        });
    }
}
