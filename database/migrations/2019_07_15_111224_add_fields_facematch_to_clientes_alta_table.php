<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsFacematchToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->boolean('aplica_facematch')->nullable()->after('envio_notificaciones_at');
            $table->boolean('facematch')->nullable()->after('aplica_facematch');
            $table->longText('facematch_error')->nullable()->after('facematch');
            $table->dateTime('facematch_at')->nullable()->after('facematch_error');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'aplica_facematch',
                'facematch',
                'facematch_error',
                'facematch_at',
            ]);
        });
    }
}
