<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddElegidaToRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->boolean('elegida')->after('situaciones')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn([
                'elegida'
            ]);
        });
    }
}
