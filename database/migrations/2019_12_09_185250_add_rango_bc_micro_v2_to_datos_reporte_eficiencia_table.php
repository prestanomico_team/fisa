<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRangoBcMicroV2ToDatosReporteEficienciaTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_reporte_eficiencia', function (Blueprint $table) {
            $table->string('RangoBCMicro_V2', 255)->after('RangoBCMicro')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_reporte_eficiencia', function (Blueprint $table) {
            $table->dropColumn([
                'RangoBCMicro_V2'
            ]);
        });
    }
}
