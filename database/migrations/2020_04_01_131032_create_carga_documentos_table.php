<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargaDocumentosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carga_documentos', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('solicitud_id');
            $table->unsignedBigInteger('prospecto_id');
            $table->boolean('aplica_facematch')->nullable();
            $table->boolean('id_front')->nullable();
            $table->boolean('id_back')->nullable();
            $table->boolean('selfie')->nullable();
            $table->boolean('facematch_completo')->nullable();

            $table->boolean('aplica_referencias')->nullable();
            $table->boolean('referencias_completo')->nullable();

            $table->boolean('aplica_cuenta_clabe')->nullable();
            $table->boolean('cuenta_clabe_completo')->nullable();

            $table->boolean('aplica_comprobante_domicilio')->nullable();
            $table->boolean('comprobante_domicilio_completo')->nullable();

            $table->boolean('aplica_comprobante_ingresos')->nullable();
            $table->string('frecuencia', 25)->nullable();
            $table->string('tipo_comprobante', 25)->nullable();
            $table->string('detalle_documento', 25)->nullable();
            $table->integer('numero_comprobantes')->nullable();
            $table->boolean('comprobante_ingresos_completo')->nullable();

            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
            $table->index(['prospecto_id'], 'prospecto');
            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carga_documentos');
    }
}
