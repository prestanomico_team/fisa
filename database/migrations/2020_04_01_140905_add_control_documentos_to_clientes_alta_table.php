<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControlDocumentosToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->boolean('aplica_referencias')->nullable()->after('facematch_at');
            $table->boolean('alta_referencias')->nullable()->after('aplica_referencias');
            $table->longText('referencias_error')->nullable()->after('alta_referencias');
            $table->dateTime('alta_referencias_at')->nullable()->after('referencias_error');

            $table->boolean('aplica_cuenta_clabe')->nullable()->after('alta_referencias_at');
            $table->boolean('alta_cuenta_clabe')->nullable()->after('aplica_cuenta_clabe');
            $table->longText('cuenta_clabe_error')->nullable()->after('alta_cuenta_clabe');
            $table->dateTime('alta_cuenta_clabe_at')->nullable()->after('cuenta_clabe_error');

            $table->boolean('aplica_comprobante_domicilio')->nullable()->after('alta_cuenta_clabe_at');
            $table->boolean('alta_comprobante_domicilio')->nullable()->after('aplica_comprobante_domicilio');
            $table->longText('comprobante_domicilio_error')->nullable()->after('alta_comprobante_domicilio');
            $table->dateTime('alta_comprobante_domicilio_at')->nullable()->after('comprobante_domicilio_error');

            $table->boolean('aplica_comprobante_ingresos')->nullable()->after('alta_comprobante_domicilio_at');
            $table->boolean('alta_comprobante_ingresos')->nullable()->after('aplica_comprobante_ingresos');
            $table->longText('comprobante_ingresos_error')->nullable()->after('alta_comprobante_ingresos');
            $table->dateTime('alta_comprobante_ingresos_at')->nullable()->after('comprobante_ingresos_error');
        });
    }

    /**
     *  Ejecuta las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'aplica_referencias',
                'alta_referencias',
                'referencias_error',
                'alta_referencias_at',
                'aplica_cuenta_clabe',
                'alta_cuenta_clabe',
                'cuenta_clabe_error',
                'alta_cuenta_clabe_at',
                'aplica_comprobante_domicilio',
                'alta_comprobante_domicilio',
                'comprobante_domicilio_error',
                'alta_comprobante_domicilio_at',
                'aplica_comprobante_ingresos',
                'alta_comprobante_ingresos',
                'comprobante_ingresos_error',
                'alta_comprobante_ingresos_at'
            ]);
        });
    }
}
