<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNivelEstudiosToDetalleSolicitudesMrTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_maquina_riesgos')->table('detalle_solicitudes_mr', function (Blueprint $table) {
            $table->string('NIVEL_ESTUDIOS', 50)->after('NUM_DEPENDIENTES')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_maquina_riesgos')->table('detalle_solicitudes_mr', function (Blueprint $table) {
            $table->dropColumn([
                'NIVEL_ESTUDIOS'
            ]);
        });
    }
}