<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControlDocumentosToProductosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->boolean('captura_referencias')->nullable()->after('carga_identificacion_selfie');
            $table->boolean('captura_cuenta_clabe')->nullable()->after('captura_referencias');
            $table->boolean('carga_comprobante_domicilio')->nullable()->after('captura_cuenta_clabe');
            $table->boolean('carga_comprobante_ingresos')->nullable()->after('carga_comprobante_domicilio');
        });
    }

    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn([
                'captura_referencias',
                'captura_cuenta_clabe',
                'carga_comprobante_domicilio',
                'carga_comprobante_ingresos'
            ]);
        });
    }
}
