<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenciasSolicitudTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referencias_solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solicitud_id');
            $table->string('primer_nombre_ref1', 100);
            $table->string('segundo_nombre_ref1', 100);
            $table->string('apellido_paterno_ref1', 100);
            $table->string('apellido_materno_ref1', 100);
            $table->string('tipo_relacion_ref1', 50);
            $table->string('telefono_ref1', 10);
            $table->string('primer_nombre_ref2', 100);
            $table->string('segundo_nombre_ref2', 100);
            $table->string('apellido_paterno_ref2', 100);
            $table->string('apellido_materno_ref2', 100);
            $table->string('tipo_relacion_ref2', 50);
            $table->string('telefono_ref2', 10);
            $table->string('primer_nombre_ref3', 100);
            $table->string('segundo_nombre_ref3', 100);
            $table->string('apellido_paterno_ref3', 100);
            $table->string('apellido_materno_ref3', 100);
            $table->string('tipo_relacion_ref3', 50);
            $table->string('telefono_ref3', 10);
            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referencias');
    }
}
