<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaClabeSolicitudTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_clabe_solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solicitud_id');
            $table->integer('banco_id')->nullable();
            $table->string('banco', 100)->nullable();
            $table->string('clabe_interbancaria', 18)->nullable();
            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_clabe_solicitud');
    }
}
