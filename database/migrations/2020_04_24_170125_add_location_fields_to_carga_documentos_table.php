<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationFieldsToCargaDocumentosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->double('latitud',15,8)->after('aplica_comprobante_domicilio')->nullable();
            $table->double('longitud',15,8)->after('latitud')->nullable();
            $table->mediumText('location_error')->after('longitud')->nullable();
            $table->double('latitud_reverse',15,8)->after('location_error')->nullable();
            $table->double('longitud_reverse',15,8)->after('latitud_reverse')->nullable();
            $table->mediumText('reverse_address')->after('longitud_reverse')->nullable();
            $table->mediumText('reverse_error')->after('reverse_address')->nullable();
            $table->decimal('distancia',10,2)->after('longitud_reverse')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->dropColumn([
                'latitud',
                'longitud',
                'location_error',
                'latitud_reverse',
                'longitud_reverse',
                'reverse_address',
                'reverse_error',
                'distancia'
            ]);
        });
    }
}
