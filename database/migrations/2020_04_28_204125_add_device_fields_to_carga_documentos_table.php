<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceFieldsToCargaDocumentosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->string('device')->after('aplica_comprobante_domicilio')->nullable();
            $table->string('browser')->after('device')->nullable();
            $table->string('platform')->after('browser')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->dropColumn([
                'device',
                'browser',
                'platform'
            ]);
        });
    }
}
