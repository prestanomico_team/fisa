<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdFmpToUsersConvenioTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_convenio', function (Blueprint $table) {
            $table->string('id_fmp')->after('sucursal')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_convenio', function (Blueprint $table) {
            $table->dropColumn([
                'id_fmp'
            ]);
        });
    }
}
