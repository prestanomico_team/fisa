<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfertasRenovacionTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas_renovacion', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->bigInteger('id_oferta');
            $table->string('rfc');
            $table->double('monto_oferta1', 8, 2);
            $table->integer('tasa_oferta1');
            $table->integer('plazo_oferta1');
            $table->double('pago_oferta1', 8, 2);
            $table->double('monto_oferta2', 8, 2);
            $table->integer('tasa_oferta2');
            $table->integer('plazo_oferta2');
            $table->double('pago_oferta2', 8, 2);
            $table->date('vigencia')->nullable();
            $table->bigInteger('id_transaccion')->nullable();
            $table->date('fecha_creacion')->nullable();

            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->index(['uuid'], 'uuid');
            $table->index(['id_oferta'], 'id_oferta');
            $table->index(['rfc'], 'rfc');
            $table->index(['fecha_creacion'], 'fecha_creacion');
            $table->index(['id_oferta', 'rfc', 'fecha_creacion'], 'id-rfc-creacion');

        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_aef_findep');
    }
}
