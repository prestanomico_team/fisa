<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdSolicitudToOfertasRenovacionTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ofertas_renovacion', function (Blueprint $table) {
            $table->bigInteger('solicitud_id')->after('uuid')->nullable();

            $table->index(['solicitud_id'], 'solicitud_id');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofertas_renovacion', function (Blueprint $table) {
            $table->dropColumn([
                'solicitud_id'
            ]);
        });
    }
}
