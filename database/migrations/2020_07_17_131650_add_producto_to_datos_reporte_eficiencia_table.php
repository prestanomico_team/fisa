<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductoToDatosReporteEficienciaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_reporte_eficiencia', function (Blueprint $table) {
            $table->string('Producto', 50)->after('solicitud_id')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_reporte_eficiencia', function (Blueprint $table) {
            $table->dropColumn([
                'Producto'
            ]);
        });
    }
}
