<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScoreNoHitToBcScoresTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_scores', function (Blueprint $table) {
            $table->string('score_no_hit', 5)->after('micro_razon3')->nullable();
            $table->string('score_no_hit_razon1', 255)->after('score_no_hit')->nullable();
            $table->string('exclusion_no_hit', 255)->after('score_no_hit_razon1')->nullable();
            $table->string('score_no_hit_error', 255)->after('exclusion_no_hit')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bc_scores', function (Blueprint $table) {
            $table->dropColumn([
                'score_no_hit',
                'score_no_hit_razon1',
                'exclusion_no_hit',
                'score_no_hit_error',
            ]);
        });
    }
}
