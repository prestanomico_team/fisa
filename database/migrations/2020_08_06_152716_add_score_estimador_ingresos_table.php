<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScoreEstimadorIngresosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_scores', function (Blueprint $table) {
            $table->string('estimador_ingresos_valor', 5)->after('score_no_hit_error')->nullable();
            $table->string('estimador_ingresos_razon1', 255)->after('estimador_ingresos_valor')->nullable();
            $table->string('estimador_ingresos_razon2', 255)->after('estimador_ingresos_razon1')->nullable();
            $table->string('estimador_ingresos_razon3', 255)->after('estimador_ingresos_razon2')->nullable();
            $table->string('exclusion_estimador_ingresos', 255)->after('estimador_ingresos_razon3')->nullable();
            $table->string('estimador_ingresos_error', 255)->after('exclusion_estimador_ingresos')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bc_scores', function (Blueprint $table) {
            $table->dropColumn([
                'estimador_ingresos_valor',
                'estimador_ingresos_razon1',
                'estimador_ingresos_razon2',
                'estimador_ingresos_razon3',
                'exclusion_estimador_ingresos',
                'estimador_ingresos_error',
            ]);
        });
    }
}
