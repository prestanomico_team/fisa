<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosExperianTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos_experian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('campo', 255)->nullable();
            $table->string('valor_prestanomico', 255)->nullable();
            $table->string('valor_experian', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogos_experian');
    }
}
