<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperianTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experian_journey', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solicitud_id');
            $table->longText('prospecto_request')->nullable();
            $table->longText('prospecto_response')->nullable();
            $table->longText('instrumentar_bc_request')->nullable();
            $table->longText('instrumentar_bc_response')->nullable();
            $table->longText('evaluar_mko_request')->nullable();
            $table->longText('evaluar_mko_response')->nullable();
            $table->boolean('evaluar_mko_status');
            $table->longText('generar_oferta_request')->nullable();
            $table->longText('generar_oferta_response')->nullable();
            $table->boolean('generar_oferta_status');
            $table->longText('consultar_oferta_request')->nullable();
            $table->longText('consultar_oferta_response')->nullable();
            $table->boolean('consultar_oferta_status');
            $table->longText('marcar_oferta_request')->nullable();
            $table->longText('marcar_oferta_response')->nullable();
            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experian_journey');
    }
}
