<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasEvaluacionExperianTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_evaluacion_experian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->boolean('ejecucion_experian')->nullable();
            $table->mediumText('status_ejecucion_experian')->nullable();
            $table->mediumText('modeloEvaluacion')->nullable();
            $table->string('decision', 255)->nullable();
            $table->string('status_oferta', 25)->nullable();
            $table->string('motivo_rechazo', 100)->nullable();
            $table->string('descripcion_otro', 255)->nullable();
            $table->integer('plantilla_comunicacion')->unsigned()->nullable();
            $table->boolean('pantallas_extra')->nullable();
            $table->string('situaciones', 255)->nullable();
            $table->boolean('cuestionario_dinamico_guardado')->nullable();
            $table->string('status_guardado')->nullable();
            $table->boolean('oferta_minima')->nullable();
            $table->boolean('simplificado')->nullable();
            $table->boolean('facematch')->nullable();
            $table->boolean('carga_identificacion_selfie')->nullable();
            $table->boolean('subir_documentos')->nullable();
            $table->boolean('elegida')->nullable();
            $table->text('oferta_id', 255)->nullable();
            $table->string('tipo_poblacion', 50)->nullable();
            $table->string('tipo_oferta', 50)->nullable();
            $table->string('monto', 50)->nullable();
            $table->string('plazo', 50)->nullable();
            $table->string('pago', 50)->nullable();
            $table->string('tasa', 50)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id', 'tipo_oferta'], 'prospecto_solicitud_oferta');
        });

    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_evaluacion_experian');
    }
}
