<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfertaPredominanteTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oferta_predominante', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->boolean('ejecucion_modelo')->nullable();
            $table->string('modeloEvaluacion', 50)->nullable();
            $table->string('ofertaEvaluacion', 50)->nullable();
            $table->string('decision', 255)->nullable();
            $table->integer('plantilla_comunicacion')->unsigned()->nullable();
            $table->string('status_oferta', 25)->nullable();
            $table->string('motivo_rechazo', 100)->nullable();
            $table->string('descripcion_otro', 255)->nullable();
            $table->boolean('pantallas_extra')->nullable();
            $table->string('situaciones', 255)->nullable();
            $table->boolean('cuestionario_dinamico_guardado')->nullable();
            $table->string('status_guardado')->nullable();
            $table->boolean('oferta_minima')->nullable();
            $table->boolean('simplificado')->nullable();
            $table->boolean('facematch')->nullable();
            $table->boolean('carga_identificacion_selfie')->nullable();
            $table->boolean('subir_documentos')->nullable();
            $table->boolean('elegida')->nullable();
            $table->string('oferta_id', 255)->nullable();
            $table->string('tipo_poblacion', 50)->nullable();
            $table->string('tipo_oferta', 50)->nullable();
            $table->string('monto', 50)->nullable();
            $table->string('plazo', 50)->nullable();
            $table->string('pago', 50)->nullable();
            $table->string('tasa', 50)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id', 'tipo_oferta'], 'prospecto_solicitud_toferta');
            $table->index(['prospecto_id', 'solicitud_id', 'oferta_id'], 'prospecto_solicitud_oferta');
        });

    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oferta_predominante');
    }
}
