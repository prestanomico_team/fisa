<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificadosFieldsToCargaDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->boolean('aplica_certificados_deuda')->after('comprobante_ingresos_completo')->nullable();
            $table->boolean('certificados_deuda_completo')->after('aplica_certificados_deuda')->nullable();
            $table->integer('numero_certificados_deuda')->after('certificados_deuda_completo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->dropColumn([
                'aplica_certificados_deuda',
                'certificados_deuda_completo',
                'numero_certificados_deuda'
            ]);
        });
    }
}
