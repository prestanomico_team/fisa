<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCargaDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->boolean('certificados')->after('numero_certificados_deuda')->nullable();
            $table->boolean('solicitud')->after('certificados')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carga_documentos', function (Blueprint $table) {
            $table->dropColumn([
                'certificados',
                'solicitud'
            ]);
        });
    }
}
