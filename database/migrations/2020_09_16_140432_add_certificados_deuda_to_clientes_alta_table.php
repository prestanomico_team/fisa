<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificadosDeudaToClientesAltaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->boolean('aplica_certificados_deuda')->nullable()->after('alta_comprobante_ingresos_at');
            $table->boolean('alta_certificados_deuda')->nullable()->after('aplica_certificados_deuda');
            $table->longText('certificados_deuda_error')->nullable()->after('alta_certificados_deuda');
            $table->dateTime('alta_certificados_deuda_at')->nullable()->after('certificados_deuda_error');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'aplica_certificados_deuda',
                'alta_certificados_deuda',
                'certificados_deuda_error',
                'alta_certificados_deuda_at'
            ]);
        });
    }
}
