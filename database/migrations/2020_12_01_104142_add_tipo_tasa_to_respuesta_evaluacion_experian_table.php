<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoTasaToRespuestaEvaluacionExperianTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuesta_evaluacion_experian', function (Blueprint $table) {
            $table->string('tipo_tasa', 255)->after('tasa')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuesta_evaluacion_experian', function (Blueprint $table) {
            $table->dropColumn([
                'tipo_tasa'
            ]);
        });
    }
}
