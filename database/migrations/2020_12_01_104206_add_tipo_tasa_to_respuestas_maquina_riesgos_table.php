<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoTasaToRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->string('tipo_tasa', 255)->after('tasa')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn([
                'tipo_tasa'
            ]);
        });
    }
}
