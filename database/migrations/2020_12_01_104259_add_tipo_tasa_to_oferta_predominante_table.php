<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoTasaToOfertaPredominanteTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oferta_predominante', function (Blueprint $table) {
            $table->string('tipo_tasa', 255)->after('tasa')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oferta_predominante', function (Blueprint $table) {
            $table->dropColumn([
                'tipo_tasa'
            ]);
        });
    }
}
