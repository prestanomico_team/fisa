<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRenovacionesTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_renovaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50)->nullable();
            $table->string('apellido_paterno', 50)->nullable();
            $table->string('apellido_materno', 50)->nullable();
            $table->string('celular', 10)->nullable();
            $table->string('rfc', 13)->nullable();
            $table->string('email', 100)->nullable();

            $table->longText('renovaciones_request')->nullable();
            $table->longText('renovaciones_response')->nullable();

            $table->timestamps();

            $table->index(['email'], 'email');
            $table->index(['celular'], 'celular');
            $table->index(['rfc'], 'rfc');
            $table->index(['nombre'], 'nombre');
        });

    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_renovaciones');
    }
}
