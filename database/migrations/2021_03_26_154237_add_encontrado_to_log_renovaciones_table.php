<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEncontradoToLogRenovacionesTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_renovaciones', function (Blueprint $table) {
            $table->boolean('encontrado')->after('renovaciones_request')->nullable();

            $table->index(['encontrado'], 'encontrado');
            $table->index(['created_at'], 'creado');

            $table->index(['encontrado', 'created_at'], 'encontrado_creado');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_renovaciones', function (Blueprint $table) {
            $table->dropColumn([
                'encontrado'
            ]);
        });
    }
}
