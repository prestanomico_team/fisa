(function(){
	if($("#sms_register_test_page").length){
		var go_after_conf = 'domicilio'; //global var used for forwarding after login. default is domicilio

		//=========== SET ON CLICK FOR PRUEBA LOGIN LINK BUTTON =====================
		$("#open-login-test-btn").on('click',function(){
			$("#select-db-row").addClass("hidden");
		});

		//=========== SET ON CLICK FOR LOGIN MODAL SUBMIT BUTTON ====================
		$("#prospect_login_submit").on('click', function(){
			$("#prospect_login_submit").addClass('hidden');
			$("#prospect_login_loading").removeClass('hidden');
			//collect the gen_sms_register_form data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#prospect_login_form").attr('action'),
			  data: $("#prospect_login_form").serialize()
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if(res.code == '00'){
		  		$("#prospect-login-result").html('<div class="alert alert-success"><p>'+res.ult_punto_report.instructions+'</p><p class="text-center"><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#login-prueba-modal">OK</a></p></div>')
		  		//set the forward_to to a global var to check after they confirm sms code
		  		if(res.ult_punto_report.forward_to != 'simulador'){//for simulador, we leave the default value of domicilio
		  			go_after_conf = res.ult_punto_report.forward_to;
		  			//they dont need the register form so hide it
		  			$("#gen_sms_register_form").addClass('hidden');
		  		}
		  		//show the all forms container to show whatever part they need to start on 
		  		$("#all_forms_container").removeClass("hidden");
		  	}else{
		  		//error
		  		$("#prospect-login-result").html('<div class="alert alert-danger"><p>Code '+res.code+' : '+res.message+'</p></div>')
		  	}
		  	$("#prospect_login_submit").removeClass('hidden');
				$("#prospect_login_loading").addClass('hidden');

				//send to the appropriate location in the form based on the forward_to value
				if(res.ult_punto_report.forward_to == "simulador"){
					//set the existing_user in the simulador form to the prospect id
					$("#simulador_existing_user").val(res.prospect_id);
					//this follows the normal flow and a new sms will be sent when they submit the first (register) form
				}else{
					//skip simulador and register forms and send them to the conf_sms screen
					//set the prospect_id hidden field value in the conf_sms_code_form
		  		$("#prospect_conf_id").val(res.prospect_id);
		  		$("#solic_conf_id").val(res.solic_id);
					//add the sms code to the conf_code field (only in back)
					$("#conf_code").val(res.sms_code);
					//show conf_sms form
		  		$("#conf_form_container").removeClass('hidden');
				}
			})
		  .fail(function() {
		    console.error( "error iniciando sesión" );
		  });
		});

		//=============== SET ON CHANGE FOR DB SOURCE ==============
		$("#db_source_select").on('change', function(){
			db_selected = $(this).val();
			if(db_selected != ''){
				$("#all_forms_container").removeClass("hidden");
				$("#db_source_bc").val(db_selected);
				$("#db_source_hawk").val(db_selected);
				$("#db_source_fico").val(db_selected);
				if(db_selected == 'mock_db'){
					$("#select-mock-person").removeClass("hidden");
				}else{
					$("#select-mock-person").addClass("hidden");
				}
			}else{
				$("#all_forms_container").addClass("hidden");
				$("#select-mock-person").addClass("hidden");
			}
		});

		//=============== SET ON CHANGE FOR PREFILL MOCK PERSON ==============
		$("#mock-person-field").on('change', function(){
			person_id = $(this).val();
			clear_form_messages();
			if(person_id != ''){
				var per = mocks_arr[person_id];
				//SET VALUES FOR REGISTER FORM
				$("#prestamo").val(10000);
				$("#finalidad_custom").val('lorem ipsum dolor sit amet. lorem ipsum dolor sit amet.');
				$("#reg_nombre").val(per.nombre);
				if(per.seg_nombre != ''){
					$("#reg_nombre").val(per.nombre+' '+per.seg_nombre);
				}
				$("#reg_apellido_p").val(per.apellido_p);
				$("#reg_apellido_m").val(per.apellido_m);
	
				//generate random numbers
				num_a = Math.floor(Math.random() * 999);
				num_b = Math.floor(Math.random() * 999);
				$("#reg_email").val(num_a+"unique"+num_b+"@something.com");
				$("#reg_password").val("Kamikaze1");
				$("#reg_password_conf").val("Kamikaze1");

				//SET THE VALUES FOR DOMICILIO FORM 
				$("#solic_dom_calle").val(per.dom_calle);
				$("#solic_dom_num_ext").val(per.dom_num_ext);
				$("#solic_dom_num_int").val(per.dom_num_int);
				//pad the cp up to 5 characters
				pad = '00000';
				cp_val = (pad + per.dom_cp).slice(-pad.length)
				$("#solic_dom_cp").val(cp_val);
				$("#solic_dom_cp").trigger("keyup");
				//set the colonia after options have been populated
				setTimeout(function(){ 
					$("#solic_dom_colonia > option").each(function() {
					    if(this.value == per.dom_colonia){
					    	this.selected = true;
					    }
					});
				}, 1500);

				//SET THE VALUES FOR DATOS PERSONALES FORM
				$("#solic_sexo").val(per.sexo);
				$("#solic_sexo > option").each(function(){if(this.value == per.sexo){this.selected = true;}});
				$("#solic_fecha_nac_dd").val(per.fecha_nac_dd);
				$("#solic_fecha_nac_mm").val(per.fecha_nac_mm);
				$("#solic_fecha_nac_yyyy").val(per.fecha_nac_yyyy);
				$("#solic_lugar_nac_ciudad").val("Aguascalientes");
				$("#solic_rfc").val(per.rfc);
				$("#solic_curp").val(per.curp);
				$("#solic_telefono_casa").val("55555555555");
				$("#solic_estado_civil > option").each(function(){if(this.value == per.estado_civil){this.selected = true;}});
				$("#solic_nivel_estudios > option").each(function(){if(this.value == per.nivel_estudios){this.selected = true;}});
				
				//SET THE VALUES FOR CUENTAS DE CREDITO
				$("#solic_credito_hipotecario_n").prop("checked","checked");//default is no
				$("#solic_credito_automotriz_n").prop("checked","checked");//default is no
				$("#solic_credito_tdc_n").prop("checked","checked");//default is no
				if(per.credito_hipo == 1){$("#solic_credito_hipotecario_y").prop("checked","checked")};
				if(per.credito_auto == 1){$("#solic_credito_automotriz_y").prop("checked","checked")};
				if(per.credito_banc == 1){$("#solic_credito_tdc_y").prop("checked","checked")};
				if(per.credito_banc_num != ''){ $("#solic_num_tdc").val(per.credito_banc_num); }else{ $("#solic_num_tdc").val(''); }
				$("#solic_autorizar").prop('checked','checked');

				//SET THE VALUES FOR DATOS ADICIONALES
				$("#solic_residencia > option").each(function(){if(this.value == per.tipo_residencia){this.selected = true;}});
				$("#solic_ingreso_mensual").val(per.ingreso_mensual);
				$("#solic_antiguedad_empleo > option").each(function(){if(this.value == per.antiguedad_empleo){this.selected = true;}});
				$("#solic_antiguedad_domicilio > option").each(function(){if(this.value == per.antiguedad_domicilio){this.selected = true;}});
				$("#solic_gastos_mensuales").val(per.gastos_familiares);
				$("#solic_num_dependientes > option").each(function(){if(this.value == per.numero_dependientes){this.selected = true;}});
				$("#solic_ocupacion > option").each(function(){if(this.value == per.ocupacion){this.selected = true;}});
				$("#solic_princp_fuente_ingr > option").each(function(){if(this.value == per.fuente_ingresos){this.selected = true;}});
				$("#solic_tel_empleo").val(per.telefono_empleo);

			}
		});

		function clear_form_messages(){
			//clear any previous prospect and solic ids
			$("#prospect_conf_id").val('');
			$("#solic_conf_id").val('');
			$("#prospect_dom_id").val('');
			$("#solic_dom_id").val('');
			$("#prospect_dp_id").val('');
			$("#solic_dp_id").val('');
			$("#prospect_cc_id").val('');
			$("#solic_cc_id").val('');
			$("#prospect_bc_id").val('');
			$("#solic_bc_id").val('');
			$("#prospect_hawk_id").val('');
			$("#solic_hawk_id").val('');
			$("#prospect_adic_id").val('');
			$("#solic_adic_id").val('');
			$("#prospect_alp_id").val('');
			$("#solic_alp_id").val('');
			//clear any previous result messages
			$("#registro-result-msg").html('');
			$("#verify-result-msg").html('');
			$("#domicilio-result-msg").html('');
			$("#datos-personales-result-msg").html('');
			$("#cuentas-credito-result-msg").html('');
			$("#bc-score-result-msg").html('');
			$("#hawk-result-msg").html('');
			$("#datos-adic-result-msg").html('');
			$("#alp-result-msg").html('');
			//hide all but the first form
			$("#conf_form_container").addClass('hidden');
			$("#dom_form_container").addClass('hidden');
			$("#datos_per_form_container").addClass('hidden');
			$("#cuentas_form_container").addClass('hidden');
			$("#solic_bc_form_container").addClass('hidden');
			$("#solic_hawk_form_container").addClass('hidden');
			$("#datos_adic_form_container").addClass('hidden');
			$("#fico_alp_form_container").addClass('hidden');
		}

		//================ SET ON CLICK FOR SHOW ALL PANELS ================
		$("#show_all_panels").on('click',function(){
			$("#all_forms_container").removeClass("hidden");
			$("#conf_form_container").removeClass('hidden');
			$("#dom_form_container").removeClass('hidden');
			$("#datos_per_form_container").removeClass('hidden');
			$("#cuentas_form_container").removeClass('hidden');
			$("#solic_bc_form_container").removeClass('hidden');
			$("#solic_hawk_form_container").removeClass('hidden');
			$("#datos_adic_form_container").removeClass('hidden');
			$("#fico_alp_form_container").removeClass('hidden');
		});

		//========== SET ON CLICK FOR SIMULADOR NEXT BUTTON ============
		$("#simulador_next_btn").on("click",function(){
			//if the existing_user field is empty than show the register form
			if($("#simulador_existing_user").val() == ''){
				$("#registro_datos_row").removeClass("hidden");
			}else{
				//the user already exists so don't validate register fields and send form as is
				$("#simulador_next_btn").addClass("hidden");
				$("#simulador_loading").removeClass("hidden");
				//collect the gen_sms_register_form data and send via ajax
				$.ajax({
		    	method: "POST",
				  url: $("#gen_sms_register_form").attr('action'),
				  data: $("#gen_sms_register_form").serialize()
				})
			  .done(function( response ) {
			  	console.log(response);
			  	res = $.parseJSON(response);
			  	//console.log(res);
			  	if(res.response == 'OK'){
			  		//set the prospect_id hidden field value in the conf_sms_code_form
			  		$("#prospect_conf_id").val(res.prospect_id);
			  		$("#solic_conf_id").val(res.solic_id);
			  		//set the success message
			  		$("#registro-result-msg").html('<div class="alert alert-success"><p>Nueva solicitud con id '+res.solic_id+' creada con éxito para el usuario con id '+res.prospect_id+'. Seguir a confirmar SMS.</p></div>');
			  		//show next form
			  		$("#conf_form_container").removeClass('hidden');
			  		//add the sms code to the conf_code field
			  		$("#conf_code").val(res.sms_code);
			  	}else{
			  		//show error message
			  		var isLdapError = '';
			  		if(res.code){
			  			isLdapError = 'LDAP Error code - '+res.code+' : ';
			  		}
			  		$("#registro-result-msg").html('<div class="alert alert-danger"><p>'+isLdapError+res.message+'</p></div>');
			  	}
			  	$("#simulador_next_btn").removeClass("hidden");
					$("#simulador_loading").addClass("hidden");
			  })
			  .fail(function() {
			    console.error( "error generating sms_code" );
			    $("#simulador_next_btn").removeClass("hidden");
					$("#simulador_loading").addClass("hidden");
			  });
			}
		});
		
		//=============== STEP 1 REGISTER FORM =========================
		//set the gen_sms_register_button button's on click event
		$("#gen_sms_register_button").on('click', function(){
			$("#gen_sms_register_button").addClass("hidden");
			$("#gen_sms_loading").removeClass("hidden");
			//collect the gen_sms_register_form data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#gen_sms_register_form").attr('action'),
			  data: $("#gen_sms_register_form").serialize()
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	//console.log(res);
		  	if(res.response == 'OK'){
		  		//set the prospect_id hidden field value in the conf_sms_code_form
		  		$("#prospect_conf_id").val(res.prospect_id);
		  		$("#solic_conf_id").val(res.solic_id);
		  		//set the success message
		  		$("#registro-result-msg").html('<div class="alert alert-success"><p>Usuario '+res.prospect_id+' creado éxitosamente.</p></div>');
		  		//show next form
		  		$("#conf_form_container").removeClass('hidden');
		  		//add the sms code to the conf_code field
		  		$("#conf_code").val(res.sms_code);
		  	}else{
		  		//show error message
		  		var isLdapError = '';
		  		if(res.code){
		  			isLdapError = 'LDAP Error code - '+res.code+' : ';
		  		}
		  		$("#registro-result-msg").html('<div class="alert alert-danger"><p>'+isLdapError+res.message+'</p></div>');
		  	}
		  	$("#gen_sms_register_button").removeClass("hidden");
				$("#gen_sms_loading").addClass("hidden");
			})
		  .fail(function() {
		    console.error( "error generating sms_code" );
		    $("#gen_sms_register_button").removeClass("hidden");
				$("#gen_sms_loading").addClass("hidden");
		  });
		});

		//=============== STEP 2 CONFIRM CODE FORM =====================
		//set the conf_sms_code_submit button's on click event
		$("#conf_sms_code_submit").on('click', function(){
			$("#conf_sms_code_submit").addClass("hidden");
			$("#conf_sms_loading").removeClass("hidden");
			//collect the data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#conf_sms_code_form").attr('action'),
			  data: $("#conf_sms_code_form").serialize()
			})
		  .done(function( response ) {
		  	res = $.parseJSON(response);
		  	console.log(res);
		  	if(res.response == 'OK'){
		  		if(go_after_conf == "domicilio"){
						//set the prospect_id hidden field value in the domicilio form
			  		$("#prospect_dom_id").val(res.prospect_id);
			  		$("#solic_dom_id").val(res.solic_id);
			  		//show success message
			  		$("#verify-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>')
			  		//show next form
			  		$("#dom_form_container").removeClass('hidden');
					}
					if(go_after_conf == "datos personales"){
						//set the prospect_dp_id to the prospect_id and solic_id for the next form
						$("#prospect_dp_id").val(res.prospect_id);
						$("#solic_dp_id").val(res.solic_id);
						//set the success message
						$("#verify-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
			  		//show next form
			  		$("#datos_per_form_container").removeClass('hidden');
					}
					if(go_after_conf == "cuentas de credito"){
						//set the prospect_cc_id to the prospect_id
						$("#prospect_cc_id").val(res.prospect_id);
						//set the solic_cc_id to the solic_id
						$("#solic_cc_id").val(res.solic_id);
						//show the success message
						$("#verify-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
			  		//show next form
			  		$("#cuentas_form_container").removeClass('hidden');
					}
					if(go_after_conf == "datos elegible"){
						//set the prospect_id and solic_id in the datos adic form
			  		$("#prospect_adic_id").val(res.prospect_id);
			  		$("#solic_adic_id").val(res.solic_id);
			  		//show success message
			  		$("#verify-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
			  		//show next form
			  		$("#datos_adic_form_container").removeClass('hidden');
			  		//set the tipo_caso hidden field value to Datos Elegible
			  		$("#datos_caso").val("Datos Elegible");
					}
					if(go_after_conf == "datos reporte"){
						//set the prospect_id and solic_id in the datos adic form
			  		$("#prospect_adic_id").val(res.prospect_id);
			  		$("#solic_adic_id").val(res.solic_id);
			  		//show success message
			  		$("#verify-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
			  		//show the next form
			  		$("#datos_adic_form_container").removeClass('hidden');
			  		//set the tipo_caso hidden field value to Datos Elegible
						$("#datos_caso").val("Datos Reporte");//this tells backend function to use the normal Flow instead of Elegible Flow
					}
		  	}else{
		  		//show error message
		  		console.warn('invalid activation code');
		  		$("#verify-result-msg").html('<div class="alert alert-danger"><p>'+res.message+'</p></div>')
					$("#conf_code").focus();
		  	}
		  	$("#conf_sms_code_submit").removeClass("hidden");
				$("#conf_sms_loading").addClass("hidden");
			})
		  .fail(function() {
		    console.error( "error confirming sms_code" );
		    $("#conf_sms_code_submit").removeClass("hidden");
				$("#conf_sms_loading").addClass("hidden");
		  });
		});
		
		//============ STEP 3 REGISTRO DOMICILIO (1/4) =================
		$("#registro_domicilio_submit").on('click', function(){
			$("#registro_domicilio_submit").addClass("hidden");
			$("#registro_dom_loading").removeClass("hidden");
			//collect the domicilio data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#registro_domicilio_form").attr('action'),
			  data: $("#registro_domicilio_form").serialize()
			}).done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if( res.response == "OK" ){
					//set the prospect_dp_id to the prospect_id and solic_id for the next form
					$("#prospect_dp_id").val(res.prospect_id);
					$("#solic_dp_id").val(res.solic_id);
					//set the success message
					$("#domicilio-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
		  		//show next form
		  		$("#datos_per_form_container").removeClass('hidden');
		  	}else{
		  		//show error message
		  		$("#domicilio-result-msg").html('<div class="alert alert-danger"><p>'+res.message+'</p></div>');
		  	}
		  	$("#registro_domicilio_submit").removeClass("hidden");
				$("#registro_dom_loading").addClass("hidden");
		  })
		  .fail(function() {
		    console.error( "error sending ajax for domicilio" );
		    $("#registro_domicilio_submit").removeClass("hidden");
				$("#registro_dom_loading").addClass("hidden");
		  });
		});

		//============ STEP 4 REGISTRO DATOS PERSONALES (2/4)===========
		$("#datos_personales_submit").on('click', function(){
			$("#datos_personales_submit").addClass("hidden");
			$("#datos_per_loading").removeClass("hidden");
			//collect the datos personales data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#datos_personales_form").attr('action'),
			  data: $("#datos_personales_form").serialize()
			}).done(function( response ){
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if( res.response == "OK" ){
					//set the prospect_cc_id to the prospect_id
					$("#prospect_cc_id").val(res.prospect_id);
					//set the solic_cc_id to the solic_id
					$("#solic_cc_id").val(res.solic_id);
					//show the success message
					$("#datos-personales-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
		  		//show next form
		  		$("#cuentas_form_container").removeClass('hidden');
		  	}else{
		  		//show the error message
		  		$("#datos-personales-result-msg").html('<div class="alert alert-danger"><p>'+res.message+'</p></div>');
		  	}
		  	$("#datos_personales_submit").removeClass("hidden");
				$("#datos_per_loading").addClass("hidden");
		  })
		  .fail(function() {
		    console.error( "error sending ajax for datos personales" );
		    $("#datos_personales_submit").removeClass("hidden");
				$("#datos_per_loading").addClass("hidden");
		  });
		});

		//============ STEP 5 REGISTRO CUENTAS DE CRÉDITO (3/4)=========
		$("#cuentas_credito_submit").on('click', function(){
			$("#cuentas_credito_submit").addClass("hidden");
			$("#cuentas_cred_loading").removeClass("hidden");
			//collect the datos personales data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#cuentas_credito_form").attr('action'),
			  data: $("#cuentas_credito_form").serialize()
			}).done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if( res.response == "OK" ){
					//set the prospect_bc_id to the prospect_id and solic_id
					$("#prospect_bc_id").val(res.prospect_id);
					$("#solic_bc_id").val(res.solic_id);
					//show success message
					$("#cuentas-credito-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
					//show the next form
					$("#solic_bc_form_container").removeClass('hidden');
					//execute the BC Score Request
					sendBCRequest();
		  	}else{
		  		$("#cuentas-credito-result-msg").html('<div class="alert alert-danger"><p>'+res.message+'</p></div>');
		  	}
		  	$("#cuentas_credito_submit").removeClass("hidden");
				$("#cuentas_cred_loading").addClass("hidden");
		  })
		  .fail(function() {
		    console.error( "error sending ajax for cuentas de crédito" );
		    $("#cuentas_credito_submit").removeClass("hidden");
				$("#cuentas_cred_loading").addClass("hidden");
		  });
		});

		//=============== STEP 6 SEND SOLICITUD BC SCORE ===============
		function sendBCRequest(){
			$("#bc-score-result-msg").html('Iniciando consulta BC Score...');
			//collect the data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#solicitud_bc_form").attr('action'),
			  data: $("#solicitud_bc_form").serialize()
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if( res.stat == 'No Encontrado' ){
		  		$("#bc-score-result-msg").html('<div class="alert alert-danger"><p>Falta información de Buró (no encontrado)</p></div>');
		  	}

		  	if( res.stat == 'Error BC' ){
		  		$("#bc-score-result-msg").html('<div class="alert alert-danger"><p>Error en sistema BC (experimentando problemas)</p></div>');
		  	}

		  	//check if the user qualifies under -008 -009 or normal bc
		  	if( res.stat == "Datos Elegible" ){
		  		//set the prospect_id and solic_id in the datos adic form
		  		$("#prospect_adic_id").val(res.prospect_id);
		  		$("#solic_adic_id").val(res.solic_id);
		  		//show success message
		  		$("#bc-score-result-msg").html('<div class="alert alert-success"><p>Puede ser Elegible, mandar a Datos Adicionales (saltando 2a llamada)</p></div>');
		  		//show next form
		  		$("#datos_adic_form_container").removeClass('hidden');
		  		//set the tipo_caso hidden field value to Datos Elegible
		  		$("#datos_caso").val("Datos Elegible");
		  	}

		  	//passed the bc score normal check, proceed to the segunda llamada form
		  	if( res.stat == "Califica BC Score" ){		
					//set the prospect_id  and solic_id in the hawk form
					$("#prospect_hawk_id").val(res.prospect_id);
					$("#solic_hawk_id").val(res.solic_id);
					//Show success message
					$("#bc-score-result-msg").html('<div class="alert alert-success"><p>Si Califica (BC Score,Edad,Lugar). Iniciando 2a llamada...</p></div>');
					//show the next form
					$("#solic_hawk_form_container").removeClass('hidden');
					//execute the Reporte Request
					sendReportRequest();
		  	}
		  	
		  	if( res.stat == "No Califica" ){
		  		$("#bc-score-result-msg").html('<div class="alert alert-danger"><p>'+res.result+'</p></div>');
		  	}
			})
		  .fail(function() {
		    console.error( "error sending credit score ajax request" );
		  });
		}		

		//=============== STEP 7 SOLICITUD HAWK ========================
		function sendReportRequest(){
			//collect the data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#solicitud_hawk_form").attr('action'),
			  data: $("#solicitud_hawk_form").serialize()
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if( res.stat == 'No Encontrado' ){
		  		$("#hawk-result-msg").html('<div class="alert alert-danger"><p>Falta información de Buró (no encontrado)</p></div>');
		  	}

		  	if( res.stat == 'Error BC' ){
		  		$("#hawk-result-msg").html('<div class="alert alert-danger"><p>Error en sistema BC (experimentando problemas)</p></div>');
		  	}

		  	if(res.stat == "Calculos 1 OK"){
		  		//set the prospect_id  and solic_id in the Datos Adicionales
					$("#prospect_adic_id").val(res.prospect_id);
					$("#solic_adic_id").val(res.solic_id);
		  		//show success message
		  		$("#hawk-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
		  		//show the next form
		  		$("#datos_adic_form_container").removeClass('hidden');
		  		//set the tipo_caso hidden field value to Datos Elegible
					$("#datos_caso").val("Datos Reporte");//this tells backend function to use the normal Flow instead of Elegible Flow
		  	}
			})
		  .fail(function() {
		    console.error( "error sending AJAX for hawk report" );
		  });
		}

		//=============== STEP 8 DATOS ADICIONALES =====================
		$("#datos_adic_submit").on("click", function(e){
			$("#datos_adic_submit").addClass("hidden");
			$("#datos_adic_loading").removeClass("hidden");
			e.preventDefault();
			//send post request for datos adicionales (4/4)
			$.ajax({
	    	method: "POST",
			  url: '/datos_adicionales',
			  data: $("#datos_adic_form").serialize() //includes datos_caso routing value
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	if(res.stat == 'Elegible'){
			  	//show validation message
			  	$("#datos-adic-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
				}
				if(res.stat == 'No Elegible'){
			  	//show validation message
			  	$("#datos-adic-result-msg").html('<div class="alert alert-danger"><p>'+res.message+'</p></div>');
				}
				if(res.stat == 'Calculos 2 OK'){
					//set prospect and solic ids for next form
					$("#prospect_alp_id").val(res.prospect_id);
					$("#solic_alp_id").val(res.solic_id);
					//show success message
					$("#datos-adic-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p></div>');
					//show next form
					$("#fico_alp_form_container").removeClass("hidden");
					//run the next ajax
					sendALPRequest();
				}
				$("#datos_adic_submit").removeClass("hidden");
				$("#datos_adic_loading").addClass("hidden");
			})
		  .fail(function() {
		    console.log( "error sending AJAX for datos adicionales" );
		    $("#datos_adic_submit").removeClass("hidden");
				$("#datos_adic_loading").addClass("hidden");
		  });
		});

		//=============== STEP 9 SOLICITUD FICO ALP ========================
		function sendALPRequest(){
			//collect the data and send via ajax
			$.ajax({
	    	method: "POST",
			  url: $("#solicitud_fico_alp_form").attr('action'),
			  data: $("#solicitud_fico_alp_form").serialize()
			})
		  .done(function( response ) {
		  	console.log(response);
		  	res = $.parseJSON(response);
		  	//======== *This is for the backend test only, don't need to do it in front =======
			  	calcs_table = '<table class="table">';
			  	$.each(res.calculos, function(key, value) {
			  		if(key !== 'mop1_report' && key !== 'cuentas_con_mop2_debug' && key !== 'cuentas_con_mop3_debug' && key !== 'pago_mensual_debug' && key !== 'fico_alp_request' && key !== 'fico_alp_response'){
					    calcs_table += '<tr><th width="40%">'+key+'</th><td>'+value+'</td></tr>';
					  }	
					});
					calcs_table += '</table>';
				//=================================================================================
		  	if(res.stat == "Califica Pre-Aprobado"){
		  		//Status = Pre-Aprobado
					$("#alp-result-msg").html('<div class="alert alert-success"><p>'+res.message+'</p> '+calcs_table+'</div>');
		  	}
		  	if(res.stat == "Califica Grupo B"){
		  		//Status = Revisón Comité
					$("#alp-result-msg").html('<div class="alert alert-warning"><p>'+res.message+'</p> '+calcs_table+'</div>');
		  	}
		  	if(res.stat == "Califica A"){
		  		//Status = Revisión Comité
		  		$("#alp-result-msg").html('<div class="alert alert-warning"><p>'+res.message+' : '+res.fallados+'</p> '+calcs_table+'</div>');
		  	}
		  	if(res.stat == "No Califica A"){
		  		//Status = Denegado
		  		$("#alp-result-msg").html('<div class="alert alert-danger"><p>'+res.message+' : '+res.fallados+'</p> '+calcs_table+'</div>');
		  	}
			})
		  .fail(function() {
		    console.error( "error sending AJAX for FICO ALP report" );
		  });
		}



		//=============== set the resend_code_link click event ================
		$("#resend_code_link").on('click', function(e){
			e.preventDefault();
			prospect_id = $("#prospect_id").val();
			//send request for code resend
			$.ajax({
	    	method: "POST",
			  url: '/resend_code',
			  data: $("#conf_hid_code_form").serialize() //includes token and prospect_id
			})
		  .done(function( response ) {
		  	res = $.parseJSON(response);
		  	if(res.response == 'OK'){
			  	//show the simulated code in the info message
		  		$("#info-message").html('Codigo Reenviado por SMS: '+res.hid_code);
					$("#info-message").removeClass('hidden');
				}else{
					$("#error-message").html('Error reenviando el código.');
					$("#error-message").removeClass('hidden');
				}
			})
		  .fail(function() {
		    console.log( "error resending hid_code" );
		  });
		});

		//set event function for zip code entry 
		$("#solic_dom_cp").on("keyup", function(){
			value = $(this).val();
			if(value.length == 5){
				$.ajax({
		    	method: "GET",
				  url: "/api/geo/"+value
				})
			  .done(function( response ) {
			  	console.log(response);
			  	//clear any previous values
			  	$("#solic_dom_colonia").val('');
		  		$("#solic_dom_colonia").html('<option>--Ingrese Código Postal--</option>');
					$("#solic_dom_deleg").val('');
					$("#solic_dom_ciudad").val('');
					$("#solic_dom_estado").val('');
					//enable any previously readonly fields
					$("#solic_dom_colonia").attr("readonly",false);
					$("#solic_dom_deleg").attr("readonly",false);
					$("#solic_dom_ciudad").attr("readonly",false);
					$("#solic_dom_estado").attr("readonly",false);
					//show the select for colonia
		  		$("#solic_dom_colonia").removeClass('hidden');
		  		//hide the manual text input for colonia
		  		$("#solic_dom_colonia_manual").addClass('hidden');

			  	res = $.parseJSON(response);
			  	if(res.response == 'OK'){
				  	//fill colonia deleg_munic, ciudad, and estado automagically
				  	//create options string for colonia dropdown
				  	var colonias_opts = '';
				  	for(i=0; i < res.colonias.length; i++){
				  		colonias_opts += '<option value="'+res.colonias[i]+'">'+res.colonias[i]+'</option>';
				  	}
				  	$("#solic_dom_colonia").html(colonias_opts);
				  	$("#solic_dom_colonia").attr("placeholder","");
				  	if(res.colonias.length > 1){
				  		$("#solic_dom_colonia").attr("readonly",false);
				  	}else{
				  		$("#solic_dom_colonia").attr("readonly",true);
				  	}

				  	//fill deleg_munic
				  	$("#solic_dom_deleg").val(res.deleg_munic);
				  	$("#solic_dom_deleg").attr("placeholder","");
				  	$("#solic_dom_deleg").attr("readonly",true);

				  	//fill ciudad
				  	if(res.ciudad == "Ciudad de México"){
				  		//remove accents
				  		res.ciudad = "Ciudad de Mexico";
				  	}
				  	if(res.ciudad == "Querétaro"){
				  		//remove accents
				  		res.ciudad = "Queretaro";
				  	}
				  	$("#solic_dom_ciudad").val(res.ciudad);
				  	$("#solic_dom_ciudad").attr("placeholder","");
				  	$("#solic_dom_ciudad").attr("readonly",true);

				  	//fill estado
				  	estado_codes = {
						  "Aguascalientes"				:	"AGS",
						  "Baja California"				:	"BCN",
						  "Baja California Sur"		:	"BCS",
						  "Campeche"							:	"CAM",
						  "Chiapas"								:	"CHS",
						  "Chihuahua"							:	"CHI",
						  "Coahuila"							:	"COA",
						  "Colima"								:	"COL",
						  "Ciudad de México"			:	"DF",
					 		"Distrito Federal"			:	"DF",
						  "Durango"								:	"DGO",
					 		"México"								:	"EM",
						  "Guanajuato"						:	"GTO",
						  "Guerrero"							:	"GRO",
						  "Hidalgo"								:	"HGO",
						  "Jalisco"								:	"JAL",
						 	"Michoacán"							:	"MIC",
						  "Morelos"								:	"MOR",
						  "Nayarit"								:	"NAY",
					 		"Nuevo León"						:	"NL",
						  "Oaxaca"								:	"OAX",
						  "Puebla"								:	"PUE",
						  "Querétaro"							:	"QRO",
					 		"Quintana Roo"					:	"QR",
						  "San Luis Potosí"				:	"SLP",
						  "Sinaloa"								:	"SIN",
						  "Sonora"								:	"SON",
						  "Tabasco"								:	"TAB",
						  "Tamaulipas"						:	"TAM",
						  "Tlaxcala"							:	"TLA",
						  "Veracruz"							:	"VER",
						  "Yucatán"								:	"YUC",
					  	"Zacatecas"							: "ZAC",
						};
				  	$("#solic_dom_estado").html('<option value="'+estado_codes[res.estado]+'">'+res.estado+'</option>');
				  	$("#solic_dom_estado").attr("readonly",true);
				  }else{
				  	if(res.response == "error"){
				  		alert(res.message);
				  		//reset everything
				  		$("#solic_dom_colonia").attr("readonly",true);
							$("#solic_dom_deleg").attr("readonly",true);
							$("#solic_dom_ciudad").attr("readonly",true);
							$("#solic_dom_estado").attr("readonly",true);
							$("#solic_dom_colonia").html('<option>--Ingrese Código Postal--</option>');
				  		$("#solic_dom_deleg").attr("placeholder","--Ingrese Código Postal--");
							$("#solic_dom_ciudad").attr("placeholder","--Ingrese Código Postal--");
							$("#solic_dom_estado").html("<option>--Ingrese Código Postal--</option>");
				  	}

				  	if(res.response == "Not Found"){
				  		//reset and hide the select for colonia
				  		$("#solic_dom_colonia").html('<option>--Ingrese Código Postal--</option>');
				  		//clear value of the colonia select  !important
				  		$("#solic_dom_colonia").val('');
				  		$("#solic_dom_colonia").addClass('hidden');
				  		//show the manual text input for colonia
				  		$("#solic_dom_colonia_manual").removeClass('hidden');
				  		//empty the rest of the fields for input
				  		$("#solic_dom_deleg").attr("placeholder","");
							$("#solic_dom_ciudad").attr("placeholder","");
							//show the state options
							$("#solic_dom_estado").html('<option value="AGS">AGS - Aguascalientes</option> <option value="BCN">BCN - Baja California Norte</option> <option value="BCS">BCS - Baja California Sur</option> <option value="CAM">CAM - Campeche</option> <option value="CHS">CHS - Chiapas</option> <option value="CHI">CHI - Chihuahua</option> <option value="COA">COA - Coahuila</option> <option value="COL">COL - Colima</option> <option value="DF">DF - Distrito Federal</option> <option value="DGO">DGO - Durango</option> <option value="EM">EM - Estado de México</option> <option value="GTO">GTO - Guanajuato</option> <option value="GRO">GRO - Guerrero</option> <option value="HGO">HGO - Hidalgo</option> <option value="JAL">JAL - Jalisco</option> <option value="MICH">MICH - Michoacán</option> <option value="MOR">MOR - Morelos</option> <option value="NAY">NAY - Nayarit</option> <option value="NL">NL - Nuevo León</option> <option value="OAX">OAX - Oaxaca</option> <option value="PUE">PUE - Puebla</option> <option value="QRO">QRO - Querétaro</option> <option value="QR">QR - Quintana Roo</option> <option value="SLP">SLP - San Luis Potosí</option> <option value="SIN">SIN - Sinaloa</option> <option value="SON">SON - Sonora</option> <option value="TAB">TAB - Tabasco</option> <option value="TAM">TAM - Tamaulipas</option> <option value="TLA">TLA - Tlaxcala</option> <option value="VER">VER - Veracruz</option> <option value="YUC">YUC - Yucatán</option> <option value="ZAC">ZAC - Zacatecas</option>');
				  	}
				  }
				})
			  .fail(function() {
			    console.error( "error requesting cp values" );
			  });
			}
		});
		
		$("#ya-registrado-link").on('click', function(){
			//show the login form
			$("#prospect_login_form").removeClass('hidden');
		});
	}

	if($("#download_solics_btn").length){
		//set the on click event for the download button
		$("#download_solics_btn").on('click',function(e){
			e.preventDefault();
			//set the action for the form to the correct url
			fr_month = $("#full_report_month").val();
			fr_year = $("#full_report_year").val();
			$("#download_solics_form").attr('action','/panel/report-completo-csv/'+fr_month+'/'+fr_year);
			//submit the form
			$("#download_solics_form").submit();
		});
	}

	if($("#download_cuentas_btn").length){
		//set the on click event for the download button
		$("#download_cuentas_btn").on('click',function(e){
			e.preventDefault();
			//set the action for the form to the correct url
			fr_month = $("#full_report_month").val();
			fr_year = $("#full_report_year").val();
			$("#download_cuentas_form").attr('action','/panel/report-completo-csv/'+fr_month+'/'+fr_year);
			//submit the form
			$("#download_cuentas_form").submit();
		});
	}

	if($("#download_consultas_btn").length){
		//set the on click event for the download button
		$("#download_conulstas_btn").on('click',function(e){
			e.preventDefault();
			//set the action for the form to the correct url
			fr_month = $("#full_report_month").val();
			fr_year = $("#full_report_year").val();
			$("#download_consultas_form").attr('action','/panel/report-completo-csv/'+fr_month+'/'+fr_year);
			//submit the form
			$("#download_consultas_form").submit();
		});
	}

	if($("#mock_cuentas_edit_form").length){
		//set on click for cuenta_delete_btn class
		$(".cuenta_delete_btn").on('click', function(e){
			e.preventDefault();
			//remove the section for target cuenta from the form (DOM)
			cuenta_index = $(this).data('target');
			person_id = $(this).data('person');
			$("#mock-person-modal-"+person_id+" #cuenta_num_"+cuenta_index).fadeOut(400);
			setTimeout(function(){
				//set the cuenta id for delete in the set_for_delete[] hidden input
				if($("#mock-person-modal-"+person_id+" #set_for_delete").val() == ""){
					//is first num so no comma
					$("#mock-person-modal-"+person_id+" #set_for_delete").val(cuenta_index);
				}else{
					cur_set_str = $("#mock-person-modal-"+person_id+" #set_for_delete").val();
					$("#mock-person-modal-"+person_id+" #set_for_delete").val(cur_set_str+','+cuenta_index);
				}
				current_set = $("#mock-person-modal-"+person_id+" #set_for_delete").val().split(',');
				console.log('set for delete: ');
				console.log(current_set);
			}, 500);
		});


		//set on click for nueva_cuenta_btn class
		$(".nueva_cuenta_btn").on('click',function(){
			//get the person id 
			person_id = $(this).data('person');
			//get the current cuentas count
			cuentas_count = parseInt($("#cuentas_count_"+person_id).val());
			new_count = cuentas_count + 1;
			//add a new element to the bottom of the persons cuentas list
			$("#cuentas_container_"+person_id).append('\
				<div id="cuenta_num_'+new_count+'">\
					<h3>Cuenta Num. '+new_count+'</h3>\
					<table class="table table-striped">\
						<tr>\
							<th width="30%">cuenta_fecha_actualizacion</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_actualizacion" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_impugnado</th>\
							<td><input type="text" name="'+new_count+'_cuenta_impugnado" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_clave_member_code</th>\
							<td><input type="text" name="'+new_count+'_cuenta_clave_member_code" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_nombre_usuario</th>\
							<td><input type="text" name="'+new_count+'_cuenta_nombre_usuario" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_num_tel</th>\
							<td><input type="text" name="'+new_count+'_cuenta_num_tel" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_num_cuenta</th>\
							<td><input type="text" name="'+new_count+'_cuenta_num_cuenta" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_responsabilidad</th>\
							<td><input type="text" name="'+new_count+'_cuenta_responsabilidad" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_tipo</th>\
							<td><input type="text" name="'+new_count+'_cuenta_tipo" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_contrato_producto</th>\
							<td><input type="text" name="'+new_count+'_cuenta_contrato_producto" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_moneda</th>\
							<td><input type="text" name="'+new_count+'_cuenta_moneda" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_importe_evaluo</th>\
							<td><input type="text" name="'+new_count+'_cuenta_importe_evaluo" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_num_pagos</th>\
							<td><input type="text" name="'+new_count+'_cuenta_num_pagos" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_freguencia_pagos</th>\
							<td><input type="text" name="'+new_count+'_cuenta_freguencia_pagos" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_monto_pagar</th>\
							<td><input type="text" name="'+new_count+'_cuenta_monto_pagar" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_apertura</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_apertura" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_ult_pago</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_ult_pago" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_ult_compra</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_ult_compra" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_cierre</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_cierre" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_reporte</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_reporte" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_modo_reporte</th>\
							<td><input type="text" name="'+new_count+'_cuenta_modo_reporte" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_ult_fecha_cero</th>\
							<td><input type="text" name="'+new_count+'_cuenta_ult_fecha_cero" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_garantia</th>\
							<td><input type="text" name="'+new_count+'_cuenta_garantia" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_cred_max_aut</th>\
							<td><input type="text" name="'+new_count+'_cuenta_cred_max_aut" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_saldo_actual</th>\
							<td><input type="text" name="'+new_count+'_cuenta_saldo_actual" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_limite_credito</th>\
							<td><input type="text" name="'+new_count+'_cuenta_limite_credito" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_saldo_vencido</th>\
							<td><input type="text" name="'+new_count+'_cuenta_saldo_vencido" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_num_pagos_vencidos</th>\
							<td><input type="text" name="'+new_count+'_cuenta_num_pagos_vencidos" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_mop</th>\
							<td><input type="text" name="'+new_count+'_cuenta_mop" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_hist_pagos</th>\
							<td><input type="text" name="'+new_count+'_cuenta_hist_pagos" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_hist_pagos_fecha_reciente</th>\
							<td><input type="text" name="'+new_count+'_cuenta_hist_pagos_fecha_reciente" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_hist_pagos_fecha_antigua</th>\
							<td><input type="text" name="'+new_count+'_cuenta_hist_pagos_fecha_antigua" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_clave_observacion</th>\
							<td><input type="text" name="'+new_count+'_cuenta_clave_observacion" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_total_pagos</th>\
							<td><input type="text" name="'+new_count+'_cuenta_total_pagos" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_total_pagos_mop2</th>\
							<td><input type="text" name="'+new_count+'_cuenta_total_pagos_mop2" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_total_pagos_mop3</th>\
							<td><input type="text" name="'+new_count+'_cuenta_total_pagos_mop3" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_total_pagos_mop4</th>\
							<td><input type="text" name="'+new_count+'_cuenta_total_pagos_mop4" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_total_pagos_mop5_plus</th>\
							<td><input type="text" name="'+new_count+'_cuenta_total_pagos_mop5_plus" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_saldo_morosidad_mas_alta</th>\
							<td><input type="text" name="'+new_count+'_cuenta_saldo_morosidad_mas_alta" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_morosidad_mas_alta</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_morosidad_mas_alta" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_clasif_puntualidad_de_pago</th>\
							<td><input type="text" name="'+new_count+'_cuenta_clasif_puntualidad_de_pago" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_fecha_inicio_reestructura</th>\
							<td><input type="text" name="'+new_count+'_cuenta_fecha_inicio_reestructura" value=""></td>\
						</tr>\
						<tr>\
							<th width="30%">cuenta_monto_ultimo_pago</th>\
							<td><input type="text" name="'+new_count+'_cuenta_monto_ultimo_pago" value=""></td>\
						</tr>\
					</table>\
				<hr><br>\
				</div>');

			//update the cuentas count 
			$("#cuentas_count_"+person_id).val(new_count);
			//console.log($("#cuentas_count_"+person_id).val());
			$('#mock-person-modal-'+person_id).animate({ 
	      scrollTop: $("#mock-person-modal-"+person_id+" #cuenta_num_"+new_count).offset().top 
	    }, 300);
		});

		//set on click for go_to_end_btn
		$(".go_to_end_btn").on('click',function(){
			//get the person id 
			person_id = $(this).data('person');
			$('#mock-person-modal-'+person_id).animate({ 
	      scrollTop: $("#mock-person-modal-"+person_id+" #cuentas_end_shortcut").offset().top 
	    }, 300);
		});
	}

	if($(".view_decryptd_pw").length){
		//set on click for view_decryptd_pw
		$(".view_decryptd_pw").on('click',function(){
				$(".decryptd_pw").attr('type','text');
				$(".decryptd_pw").css('background-color','red');
				$(".decryptd_pw").css('color','#fff');
				$(this).addClass('hidden');
				$(".hide_decryptd_pw").removeClass('hidden');
		});

		//set on click for hide_decryptd_pw
		$(".hide_decryptd_pw").on('click',function(){
				$(".decryptd_pw").attr('type','password');
				$(".decryptd_pw").css('background-color','#fff');
				$(".decryptd_pw").css('color','#333');
				$(this).addClass('hidden');
				$(".view_decryptd_pw").removeClass('hidden');
		});
	}

	if($("#filter-errors-form").length){
		//set on change for filter-by-message select
		$("#filter-by-message").on('change',function(){
			show_index = $(this).val();
			if(show_index != ''){
				//hide all rows
				$( ".error_row" ).addClass('hidden');
				//show only rows with this type index
				$( ".error_row.tipo_"+show_index).removeClass('hidden');
			}else{
				//show all rows
				$( ".error_row" ).removeClass('hidden');
			}
		});
	}

	if($("#cp-console-container").length){
		//function to set on click for the .verify-btn class
		setVerifyOnClick = function(){
			$(".verify-btn").on('click',function(e){
				//get this cp_row elements
				i = $(this).data('number');
				cp_row = $("#cp_"+i); 
				dataset = cp_row[0].dataset;
				csrf = $("[name='_token'").val();
				
				//change status for this row to Verificando...
				$("#cp_status_"+i).html('<strong style="color:GoldenRod;">Verificando...</strong>');
				
				console.log('running update');
				$.ajax({
					cache: false,
		    	method: "POST",
				  url: "/panel/cp-update-verify",
				  data: {_token : csrf, cp : dataset.cp, colonia : dataset.colonia, deleg_munic : dataset.delegMunic, ciudad : dataset.ciudad, estado : dataset.estado }
				})
			  .done(function( response ) {
			  	console.log(response);
			   	res = $.parseJSON(response);
			  	if(res.stat == 'cp up to date'){
					  $("#cp_status_"+i).html('<strong style="color:Grey;">OK</strong>');
					}
				  if(res.stat == 'no matches found'){
				  	$("#cp_status_"+i).html('<strong style="color:LimeGreen;">Añadido</strong>');
				  }
				})
			});
		}
		//set on click for the #update-cp-submit-btn (gets the current list from CSV file)
		$("#update-cp-submit-btn").on('click',function(e){
			value = $("#update-estado").val(); 
			//set progress modal title to reflect current state
			$("#progress-modal-title").html('Procesando Lista: '+value);
			csrf = $("[name='_token']").val();
			if(value != ''){
				//show loading btn
				$("#update-cp-submit-btn").addClass('hidden');
				$("#update-cp-submit-btn-loading").removeClass('hidden');
				//clear contents of title and panel
				$("#cp-panel-title").html('Cargando...');
				$("#cp-table-body").html('');
				//hide the run update btn
				$("#run-cp-update-btn").addClass('hidden');
				$.ajax({
		    	method: "POST",
				  url: "/panel/cp-get-state",
				  data: {estado : value, _token : csrf}
				})
			  .done(function( response ) {
			  	//console.log(response);
			   	res = $.parseJSON(response);
			  	if(res.stat){
				  	//hide loading btn
						$("#update-cp-submit-btn").removeClass('hidden');
						$("#update-cp-submit-btn-loading").addClass('hidden');
						//set title for results panel
						$("#cp-panel-title").html('Codigos Postales de '+res.estado);
						cps = res.data;
						//add the content one row at a time
						for(i=0; i < cps.length; i++){
							if(i == 0){
								//is header row
								row_str = '<tr><th>#</th><th>'+cps[i][0]+'</th><th>'+cps[i][1]+'</th><th>'+cps[i][2]+'</th><th>'+cps[i][3]+'</th><th>'+cps[i][4]+'</th><th>status</th><th>Verificar</th></tr>';
							}else{
								row_str = '<tr class="cp_row" id="cp_'+i+'" data-num="'+i+'" data-cp="'+cps[i][0]+'" data-colonia="'+cps[i][1]+'" data-deleg-munic="'+cps[i][2]+'" data-ciudad="'+cps[i][3]+'" data-estado="'+cps[i][4]+'"><td>'+i+'</td><td>'+cps[i][0]+'</td><td>'+cps[i][1]+'</td><td>'+cps[i][2]+'</td><td>'+cps[i][3]+'</td><td>'+cps[i][4]+'</td><td id="cp_status_'+i+'"></td><td><button id="verify_btn_'+i+'" data-number="'+i+'" class="btn btn-warning btn-xs verify-btn">Verificar</button></td></tr>';
							}
							$("#cp-table-body").append(row_str);
						}
						setVerifyOnClick();
						//show the run update btn
						$("#run-cp-update-btn").removeClass('hidden');
				  }else{
				  	//clear contents of title and panel
						$("#cp-panel-title").html('Cargando...');
						$("#cp-table-body").html('');
						//hide the run update btn
						$("#run-cp-update-btn").addClass('hidden');
				  }
				});
			}
		});

		$("#cancel-process-list").on('click',function(e){
			i = 99999999;
		});


		$("#run-cp-update-btn").on('click',function(e){
			//get all rows
			cp_rows = $(".cp_row");
			total_rows = cp_rows.length;
			i=0;
			function updateAddedScroll(){
        var element = document.getElementById("new_cps_added");
        element.scrollTop = element.scrollHeight;
			}
			verifyOrUpdate = function(){
				if(i < cp_rows.length){
					dataset = cp_rows[i].dataset;
					csrf = $("[name='_token'").val();
					//set the modal progress bar
					percentage = Math.round((i/total_rows)*100);
					$(".progress-bar").css('width',percentage+'%');
					//set modal description
					desc_city = dataset.ciudad;
					if(!dataset.ciudad){
						desc_city = '['+dataset.delegMunic+']';
					}
					processing_desc = dataset.cp+' - '+dataset.colonia+', '+desc_city;
					$("#current_process_description").html('procesando: '+processing_desc);
					$.ajax({
						cache: false,
			    	method: "POST",
					  url: "/panel/cp-update-verify",
					  data: {_token : csrf, cp : dataset.cp, colonia : dataset.colonia, deleg_munic : dataset.delegMunic, ciudad : dataset.ciudad, estado : dataset.estado }
					})
				  .done(function( response ) {
				  	//console.log(response);
				   	res = $.parseJSON(response);
				  	if(res.stat == 'cp up to date'){
						  $("#cp_status_"+dataset.num).html('<strong style="color:Grey;">OK</strong>');
						}
					  if(res.stat == 'no matches found'){
					  	$("#cp_status_"+dataset.num).html('<strong style="color:LimeGreen;">Añadido</strong>');
					  	//show in modal
					  	$("#new_cps_added").append('id: '+res.id+', cp: '+processing_desc+'<br>');
					  	//keep scroll to the bottom
					  	updateAddedScroll();
					  }
					  i++;
					  verifyOrUpdate(i);
					})
				}else{
					console.log('cascade complete');
					$("#current_process_description").html('Proceso Terminado.');
					$("#process-list-ok").removeClass('hidden');
					$("#cancel-process-list").addClass('hidden');
					//show individual update buttons
					$(".verify-btn").removeClass("hidden");
				}
			}
			//hide individual update buttons
			$(".verify-btn").addClass("hidden");
			//reset and show the modal
			$("#new_cps_added").html('');
			$("#current_process_description").html('');
			if(!$("#process-list-ok").hasClass('hidden')){
				$("#process-list-ok").addClass('hidden');
			}
			if($("#cancel-process-list").hasClass('hidden')){
				$("#cancel-process-list").removeClass('hidden');
			}
			$('#progress-modal').modal('show');
			//start cascade function
			verifyOrUpdate(i);
		});
	}

})();