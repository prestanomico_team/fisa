'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require("gulp-sourcemaps");
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglifyjs');

/******** minify css task **********/
gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
});

/******** minify js tasks **********/
/* basejs */
gulp.task('basejs', function() {
	return gulp.src([
		'./js/jquery/jquery.js',
		'./js/jquery/jquery-ui.js',
		'./js/jquery/jquery.cookie.js',
		'./js/owl.carousel.js',
		'./js/jquery.magnific-popup.js'
	])
	.pipe(uglify('base.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* appjs */
gulp.task('appjs', function() {
	return gulp.src([
		'./js/refactor/close_window.js',
		'./js/refactor/select.js',
		'./js/refactor/menu.js'
	])
	.pipe(uglify('app.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* faqs */
gulp.task('faqsjs', function() {
	return gulp.src([
		'./js/refactor/faqs.js'
	])
	.pipe(uglify('faqs.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* functions */
gulp.task('functionsjs', function() {
	return gulp.src([
		'./js/functions.js'
	])
	.pipe(uglify('functions.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* pagos */
gulp.task('pagosjs', function() {
	return gulp.src([
		'./js/pagos.js'
	])
	.pipe(uglify('pagos.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* pagos tarjeta */
gulp.task('pagostarjetajs', function() {
	return gulp.src([
		'./js/pagos-tarjeta.js'
	])
	.pipe(uglify('pagos-tarjeta.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* transfer */
gulp.task('transferjs', function() {
	return gulp.src([
		'./js/jquery/jquery.js',
    './js/jquery/jquery.cookie.js',
    './js/refactor/referrer.js'
	])
	.pipe(uglify('transfer.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* carousel */
gulp.task('carouseljs', function() {
	return gulp.src([
		'./js/carousel.js'
	])
	.pipe(uglify('carousel.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* modals */
gulp.task('modalsjs', function() {
	return gulp.src([
		'./js/refactor/modals.js'
	])
	.pipe(uglify('modals.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* register */
gulp.task('registerjs', function() {
	return gulp.src([
		'./js/refactor/registro.js'
	])
	.pipe(uglify('register.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
/* login */
gulp.task('loginjs', function() {
	return gulp.src([
		'./js/refactor/login.js'
	])
	.pipe(uglify('login.min.js', {}))
	.pipe(gulp.dest('./js/minify'));
});
