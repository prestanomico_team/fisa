var interes = 0.32, mesesValue = 0, prestamoValue = 0, pagoFijo = 0;

function nextPestamo(step){
	if(step == 1){
		$('#prestamo-paso-1').addClass('hidden');
		$('#prestamo-paso-2').removeClass('hidden');
		$('#prestamo-paso-3').addClass('hidden');
		mesesSlider.refresh();
	} else if(step == 2){
		$('#prestamo-paso-1').addClass('hidden');
		$('#prestamo-paso-2').addClass('hidden');
		$('#prestamo-paso-3').removeClass('hidden');
		interes = 0.32;
		mesesValue = mesesSlider.getValue();
		prestamoValue = prestamoSlider.getValue();
		if(prestamoValue >= 300000 && prestamoValue <= 500000){
			interes = 0.32;
		} else if(prestamoValue > 500000 && prestamoValue <= 1000000){
			interes = 0.28;
		} else if(prestamoValue > 1000000 && prestamoValue <= 1500000){
			interes = 0.20;
		} else if(prestamoValue > 1500000 && prestamoValue <= 2000000){
			interes = 0.18;
		} else { 
			interes = 0.32;
		}
		pagoFijo = pmt(interes/12, mesesValue, (prestamoValue * -1));
		$("#prestamoHipotecarioCantidad").text(moneyFormat(prestamoValue));
		$("#prestamoHipotecarioPagoFIjo").text(moneyFormat(pagoFijo));
		$("#mesesHipotecarioCantidad").text(mesesValue);
	} else if(step == 3){
		$('#sendInfo').modal();
	}
}

function changeCalculadoraHipotecaria(){
	interes = 0.32;
	mesesValue = $("#hipotecario_calculadora_meses").val();
	prestamoValue = $("#hipotecario_calculadora_prestamo").val();
	if(mesesValue != null && prestamoValue != null){
		mesesValue = parseInt(mesesValue);
		prestamoValue = parseInt(prestamoValue);
		if(prestamoValue >= 300000 && prestamoValue <= 500000){
			interes = 0.32;
		} else if(prestamoValue > 500000 && prestamoValue <= 1000000){
			interes = 0.28;
		} else if(prestamoValue > 1000000 && prestamoValue <= 1500000){
			interes = 0.20;
		} else if(prestamoValue > 1500000 && prestamoValue <= 2000000){
			interes = 0.18;
		} else { 
			interes = 0.32;
		}
		pagoFijo = pmt(interes/12, mesesValue, (prestamoValue * -1));
		$("#prestamoHipotecarioCantidad").text(moneyFormat(prestamoValue));
		$("#prestamoHipotecarioPagoFIjo").text(moneyFormat(pagoFijo));
		$("#mesesHipotecarioCantidad").text(mesesValue);
	}
}

function prevPestamo(step){
	if(step == 1){
		$('#prestamo-paso-1').removeClass('hidden');
		$('#prestamo-paso-2').addClass('hidden');
		$('#prestamo-paso-3').addClass('hidden');
		prestamoSlider.refresh().on('change', changeValuePrestamo);
		mesesSlider.refresh().on('change', changeValuePrestamo);
	}
}

document.addEventListener('DOMContentLoaded', function() {
	$('#SendData').on('click', function(){
		var userName = $('#sendInfo #sendDataName').val();
		var userEmail = $('#sendInfo #sendDataEmail').val();
		var userTel = $('#sendInfo #sendDataTel').val();
		if (userName == '') {
			$('#error2').removeClass('hidden');
		} else if (userEmail == '' || !validateEmail(userEmail) ) {
			$('#error2').removeClass('hidden');
		} else if(userTel == ''){
			$('#error2').removeClass('hidden');
		} else {
			$.ajax({
				url: siteURL+'sendcalculo',
				type: 'post',
				data: {
					action: 'CalculoHipotecario',
					nombre: userName,
					correo: userEmail,
					telefono: userTel,
					prestamo: prestamoValue,
					meses: mesesValue,
					tasa: interes,
					pagoFijo: pagoFijo,
				},
				beforeSend: function() {},
				success: function(result) {
					$('#sendInfo').modal('toggle');
					$('#thankscal').modal();
					document.getElementById("downloadTablaPagos").setAttribute("href", siteFilesURL+'files_temp/'+result);
				}
			});
		}
	});

	$('#closeThanks').on('click', function(){
		$('#thankscal').modal('toggle');
		$('#sendDataTel').val('');
		$('#sendDataName').val('');
		$('#sendDataEmail').val('');
		$('#prestamoHipotecarioPagoFIjo').text('$0.00');
		$('#hipotecario_calculadora_meses').prop('selectedIndex',0);
		$('#hipotecario_calculadora_prestamo').prop('selectedIndex',0);
	});
}, false);

/*
var changeValuePrestamo = function(){ if(prestamoSlider.getValue() < 300000){ prestamoSlider.setValue(300000); } }

var prestamoSlider = new Slider("#prestamoHipotecario", {
	min: 0, max: 2000000, step: 5000, value: 300000,
	ticks: [0, 500000, 1000000, 1500000, 2000000],
	ticks_labels: ['$0', '$500,000', '$1,000,000', '$1,500,000', '$2,000,000'],
}).on('change', changeValuePrestamo);

var mesesSlider = new Slider("#mesesHipotecario", {
	min: 36, max: 60, step: 12, value: 60, ticks: [36, 48, 60],
	ticks_labels: ['36 meses', '48 meses', '60 meses'],
});

var updateSliderHipotecario = function(){
	value = prestamoSlider.getValue();
	width = document.getElementById('calculadora-hipotecario').offsetWidth;
	if(width <= 450) {
		prestamoSlider.destroy();
		prestamoSlider = new Slider("#prestamoHipotecario", {
			min: 0, max: 2000000, step: 5000, value: value,
			ticks: [0, 500000, 1000000, 1500000, 2000000],
			ticks_labels: ['$0', '$500k', '$1M', '$1.5M', '$2M'],
		}).on('change', changeValuePrestamo);
	} else {
		prestamoSlider.destroy();
		prestamoSlider = new Slider("#prestamoHipotecario", {
			min: 0, max: 2000000, step: 5000, value: value,
			ticks: [0, 500000, 1000000, 1500000, 2000000],
			ticks_labels: ['$0', '$500,000', '$1,000,000', '$1,500,000', '$2,000,000'],
		}).on('change', changeValuePrestamo);
	}
}

updateSliderHipotecario();
window.addEventListener("resize", updateSliderHipotecario);*/