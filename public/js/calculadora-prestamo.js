var interes = 0.45, mesesValue = 0, prestamoValue = 0, pagoFijo = 0;
function nextPestamo(step){
	if(step == 2){
		$('#info_prestamo_personal').removeClass('hidden');
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').removeClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 3){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').removeClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 4){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').removeClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 5){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').removeClass('hidden');
	}
}
function prevPestamo(step){
	if(step == 1){
		$('#presonal_paso_1').removeClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
		$('#info_prestamo_personal').addClass('hidden');
	} else if(step == 2){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').removeClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 3){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').removeClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 4){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').removeClass('hidden');
		$('#presonal_paso_5').addClass('hidden');
	} else if(step == 5){
		$('#presonal_paso_1').addClass('hidden');
		$('#presonal_paso_2').addClass('hidden');
		$('#presonal_paso_3').addClass('hidden');
		$('#presonal_paso_4').addClass('hidden');
		$('#presonal_paso_5').removeClass('hidden');
	}
}

function changeCalculadoraPersonal(){
	interes = 0.45;
	mesesTexto = $("#plazo option:selected").text();
	mesesTexto = $("#plazo").find('option:selected').text();
    mesesTexto = mesesTexto.split(' ', 2);

	if ($.isNumeric(mesesTexto[0])) {
		if (mesesTexto[1] == 'Quincenas' || mesesTexto[1] == 'QUINCENAS') {
			mesesValue = mesesTexto[0] / 2;
		} else {
			mesesValue = mesesTexto[0];
		}

		prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
		prestamoValue = prestamoValue * 1000;

		finalidadValue = $( "#finalidad option:selected" ).text();
		if(mesesValue != null && prestamoValue != null){
			pagoFijo = pmt(interes/12*1.16, mesesValue, (prestamoValue * -1));
			$("#prestamoPersonalCantidad").text(moneyFormat(parseInt(prestamoValue)));
			$("#prestamoPersonalPagoFIjo").text(moneyFormat(parseInt(pagoFijo)));
			$("#mesesPersonalCantidad").text(mesesValue);
			$('#pago_estimado').val(parseInt(pagoFijo));

			$("#info_general_prestamo").text(moneyFormat(parseInt(prestamoValue)));
			$("#info_general_plazo").text(mesesValue + ' meses');
			$("#info_general_finalidad").text(finalidadValue);
		}
	}
}

function changeCalculadoraSindicalizados(){
	interes = 0.18;
	mesesTexto = $("#plazo option:selected").text();
	mesesTexto = $("#plazo").find('option:selected').text();
    mesesTexto = mesesTexto.split(' ', 2);

	if ($.isNumeric(mesesTexto[0])) {
		if (mesesTexto[1] == 'Quincenas' || mesesTexto[1] == 'QUINCENAS') {
			mesesValue = mesesTexto[0] / 2;
		} else {
			mesesValue = mesesTexto[0];
		}

		prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
		prestamoValue = prestamoValue * 1000;

		finalidadValue = $( "#finalidad option:selected" ).text();
		if(mesesValue != null && prestamoValue != null){
			pagoFijo = pmt(interes/12*1.16, mesesValue, (prestamoValue * -1));
			$("#prestamoPersonalCantidad").text(moneyFormat(parseInt(prestamoValue)));
			$("#prestamoPersonalPagoFIjo").text(moneyFormat(parseInt(pagoFijo)));
			$("#mesesPersonalCantidad").text(mesesValue);
			$('#pago_estimado').val(parseInt(pagoFijo));

			$("#info_general_prestamo").text(moneyFormat(parseInt(prestamoValue)));
			$("#info_general_plazo").text(mesesValue + ' meses');
			$("#info_general_finalidad").text(finalidadValue);
		}
	}
}

document.addEventListener('DOMContentLoaded', function() {
	$('#SendData').on('click', function(){
		var userName = $('#sendInfo #sendDataName').val();
		var userEmail = $('#sendInfo #sendDataEmail').val();
		var userTel = $('#sendInfo #sendDataTel').val();
		if (userName == '') {
			$('#error2').removeClass('hidden');
		} else if (userEmail == '' || !validateEmail(userEmail) ) {
			$('#error2').removeClass('hidden');
		} else if(userTel == ''){
			$('#error2').removeClass('hidden');
		} else {
			$.ajax({
				url: siteURL+'sendcalculo',
				type: 'post',
				data: {
					action: 'CalculoPersonal',
					nombre: userName,
					correo: userEmail,
					telefono: userTel,
					prestamo: prestamoValue,
					meses: mesesValue,
					tasa: interes,
					pagoFijo: pagoFijo,
				},
				beforeSend: function() {},
				success: function(result) {
					$('#sendInfo').modal('toggle');
					$('#thankscal').modal();
					document.getElementById("downloadTablaPagos").setAttribute("href", siteFilesURL+'files_temp/'+result);
				}
			});
		}
	});

	$('#closeThanks').on('click', function(){
		$('#thankscal').modal('toggle');
		$('#sendDataTel').val('');
		$('#sendDataName').val('');
		$('#sendDataEmail').val('');
		$('#prestamoPersonalPagoFIjo').text('$0.00');
		$('#personal_calculadora_meses').prop('selectedIndex',0);
		$('#personal_calculadora_prestamo').prop('selectedIndex',0);
	});
}, false);


/*
var changeValuePrestamo = function(){ if(prestamoSlider.getValue() < 5000){ prestamoSlider.setValue(5000); } }

var prestamoSlider = new Slider("#prestamoPersonal", {
	min: 0, max: 50000, step: 1000, value: 5000,
	ticks: [0, 10000, 20000, 30000, 40000, 50000],
	ticks_labels: ['$0', '$10,000', '$20,000', '$30,000', '$40,000', '$50,000'],
}).on('change', changeValuePrestamo);

var mesesSlider = new Slider("#mesesPersonal", {
	min: 12, max: 36, step: 12, value: 24, ticks: [12, 24, 36],
	ticks_labels: ['12 meses', '24 meses', '36 meses'],
});

var updateSliderPrestamo = function(){
	value = prestamoSlider.getValue();
	width = document.getElementById('calculadora-personal').offsetWidth;
	if(width <= 450) {
		prestamoSlider.destroy();
		prestamoSlider = new Slider("#prestamoPersonal", {
			min: 0, max: 50000, step: 1000, value: value,
			ticks: [0, 10000, 20000, 30000, 40000, 50000],
			ticks_labels: ['$0', '$10k', '', '$30k', '', '$50k'],
		}).on('change', changeValuePrestamo);
	} else {
		prestamoSlider.destroy();
		prestamoSlider = new Slider("#prestamoPersonal", {
			min: 0, max: 50000, step: 1000, value: value,
			ticks: [0, 10000, 20000, 30000, 40000, 50000],
			ticks_labels: ['$0', '$10,000', '$20,000', '$30,000', '$40,000', '$50,000'],
		}).on('change', changeValuePrestamo);
	}
}

updateSliderPrestamo();
window.addEventListener("resize", updateSliderPrestamo);*/
