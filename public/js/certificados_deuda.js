if (document.location.pathname.includes("certificados_deuda")) {
    
    //Dropzone.autoDiscover = false;
    Dropzone.options.myDropzone = {
        url: '/webapp/certificados_deuda/save',
        autoProcessQueue: false,
        maxFiles: 10,
        maxFilesize: 10,
        uploadMultiple: true,
        parallelUploads: 10,
        acceptedFiles: 'image/*,application/pdf',
        addRemoveLinks: true,
        timeout: 600000,
        preventDuplicates: true,
        dictDefaultMessage: "Arrastra y suelta o da click aquí para subir los certificados de deuda.",
        dictMaxFilesExceeded: "Solo puedes subir 10 archivos como máximo.",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        accept: function(file, done) {
            console.log(file);
            var dzClosure = this;
            done();

            $('#num_certificados').val(dzClosure.getAcceptedFiles().length);
            $('.dz-message').show();
            
            //$('.dz-button').html('Arrastra y suelta o da click aquí para continuar agregando archivos');

            if (dzClosure.getAcceptedFiles().length >= 1) {

                $("html, body").animate({ scrollTop: $(".subirarchivos").offset().top }, 1000);
                //$('#subirArchivos').show();
                $("#certificados").val(1);
                if ($("#solicitud").val() == 1 && $("#certificados").val() == 1){
                    $('#subirArchivos').show();
                }
                document.getElementById("subirArchivos").addEventListener("click", function(e) {

                    e.preventDefault();
                    e.stopPropagation();
                    dzClosure.processQueue();

                    Swal.fire({
                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Cargando archivos... </br> Por favor no cierres ni actualices la página. </p></center>',
                       customClass: 'modalLoading',
                       showCancelButton: false,
                       showConfirmButton:false,
                       allowOutsideClick: false
                   });

                });
            }

        },
        init: function() {
            var dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

            this.on("sendingmultiple", function(file, xhr, formData) {
                formData.append('num_certificados', $('#num_certificados').val());
                formData.append('tipo_archivo', 'certificados');
            });

            this.on("error", function(file, message) {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });
               this.removeFile(file);

            });

            this.on("removedfile", function(file, message) {
                if (dzClosure.getAcceptedFiles().length < 10) {
                    
                    
                }
                if (dzClosure.getAcceptedFiles().length == 0) {
                    $("#certificados").val(0);
                    
                    if ($("#solicitud").val() == 0 || $("#certificados").val() == 0){
                        $('#subirArchivos').hide();
                    }
                }
                
                $('#num_certificados').val(dzClosure.getAcceptedFiles().length);
            });

            this.on("success", function (response) {

                if (response.xhr.status == 200) {

                    var respuesta = JSON.parse(response.xhr.response);
                    if (respuesta.success == true) {
                        
                        if (!respuesta.hasOwnProperty('carga_completada')) {
                            $('.dz-remove').hide();
                            if (respuesta.hasOwnProperty('modal')) {
                                Swal.fire({
                                    html: respuesta.modal,
                                    showConfirmButton: false,
                                    allowOutsideClick: false,
                                    customClass: 'modalstatus'
                                });
                            }
                        } else if (respuesta.hasOwnProperty('siguiente_paso')) {

                            var paso = respuesta.siguiente_paso;

                            Swal.fire({
                                title: "Carga de archivos completa",
                                text: "Certificados de deuda guardados con éxito",
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    Swal.fire({
                                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Redirigiendo... </p></center>',
                                       customClass: 'modalLoading',
                                       showCancelButton: false,
                                       showConfirmButton:false,
                                       allowOutsideClick: false
                                   });
                                   location.href = '/webapp/' + paso;
                                }
                            });

                        }

                    } else {

                        if (respuesta.hasOwnProperty('reload')) {

                            Swal.fire({
                                title: "La sesión expiro",
                                text: 'Recarga la página para continuar',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                   location.reload();
                               }
                           });

                        } else if (respuesta.hasOwnProperty('archivo_invalido')) {
                            this.removeAllFiles( true );
                            Swal.fire({
                                title: 'No se pueden cargar los archivos',
                                text: 'Alguno de los archivos presenta un formato inválido',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            });
                        }

                    }

                }

            });

        }
    }
    Dropzone.options.myDropzoneSolic = {
        url: '/webapp/certificados_deuda/save',
        autoProcessQueue: false,
        maxFiles: 1,
        maxFilesize: 10,
        uploadMultiple: true,
        parallelUploads: 10,
        acceptedFiles: 'image/*,application/pdf',
        addRemoveLinks: true,
        timeout: 200000,
        preventDuplicates: true,
        dictDefaultMessage: "Arrastra y suelta o da click aquí para subir la solicitud de crédito.",
        dictMaxFilesExceeded: "Solo puedes subir 1 archivo como máximo.",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        accept: function(file, done) {
            console.log(file);
            var dzClosure = this;
            done();

            $('.dz-message').show();
            
            if (dzClosure.getAcceptedFiles().length >= 1) {

                $("html, body").animate({ scrollTop: $(".subirarchivos").offset().top }, 1000);
                $("#solicitud").val(1);
                if ($("#solicitud").val() == 1 && $("#certificados").val() == 1){
                    $('#subirArchivos').show();
                }
               
                

                document.getElementById("subirArchivos").addEventListener("click", function(e) {

                    e.preventDefault();
                    e.stopPropagation();
                    dzClosure.processQueue();

                    Swal.fire({
                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Cargando archivos... </br> Por favor no cierres ni actualices la página. </p></center>',
                       customClass: 'modalLoading',
                       showCancelButton: false,
                       showConfirmButton:false,
                       allowOutsideClick: false
                   });

                });
            }

        },
        init: function() {
            var dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

            this.on("sendingmultiple", function(file, xhr, formData) {
                formData.append('tipo_archivo', 'solicitud');
            });

            this.on("error", function(file, message) {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });
               this.removeFile(file);

            });

            this.on("removedfile", function(file, message) {
                
                if (dzClosure.getAcceptedFiles().length == 0) {
                    $("#solicitud").val(0);
                    
                    if ($("#solicitud").val() == 0 || $("#certificados").val() == 0){
                        $('#subirArchivos').hide();
                    }
                }
                
                
            });

            this.on("success", function (response) {

                if (response.xhr.status == 200) {

                    var respuesta = JSON.parse(response.xhr.response);
                    if (respuesta.success == true) {
                        $('.dz-remove').hide();
                        if (respuesta.hasOwnProperty('modal')) {
                            Swal.fire({
                                html: respuesta.modal,
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                customClass: 'modalstatus'
                            });
                        } else if (respuesta.hasOwnProperty('siguiente_paso')) {

                            var paso = respuesta.siguiente_paso;

                            Swal.fire({
                                title: "Carga de archivos completa",
                                text: "Certificados de deuda guardados con éxito",
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    Swal.fire({
                                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Redirigiendo... </p></center>',
                                       customClass: 'modalLoading',
                                       showCancelButton: false,
                                       showConfirmButton:false,
                                       allowOutsideClick: false
                                   });
                                   location.href = '/webapp/' + paso;
                                }
                            });

                        }

                    } else {

                        if (respuesta.hasOwnProperty('reload')) {

                            Swal.fire({
                                title: "La sesión expiro",
                                text: 'Recarga la página para continuar',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                   location.reload();
                               }
                           });

                        } else if (respuesta.hasOwnProperty('archivo_invalido')) {
                            this.removeAllFiles( true );
                            Swal.fire({
                                title: 'No se pueden cargar los archivos',
                                text: 'Alguno de los archivos presenta un formato inválido',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            });
                        }

                    }

                }

            });

        }
    }
    //Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastra y suelta o da click aquí para subir los archivos.";
    Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar archivo.";
    Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir este tipo de archivos, solo Imagenes o PDF.";
    //Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes subir 10 archivos como máximo.";
    Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es muy grande ({{filesize}} MB). El tamaño máximo que puedes subir es de: {{maxFilesize}} MB.";
    Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
    Dropzone.prototype.defaultOptions.dictDuplicateFile = "No se pueden subir archivos duplicados";

    Dropzone.prototype.isFileExist = function(file) {
      var i;
      if(this.files.length > 0) {
        for(i = 0; i < this.files.length; i++) {
          if(this.files[i].name === file.name
            && this.files[i].size === file.size
            && this.files[i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
           {
               return true;
           }
        }
      }
      return false;
    };
    
    
    Dropzone.prototype.addFile = function(file) {
      file.upload = {
        progress: 0,
        total: file.size,
        bytesSent: 0
      };
      if (this.options.preventDuplicates && this.isFileExist(file)) {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: this.options.dictDuplicateFile,
          type: 'error',
          showConfirmButton: true,
          customClass: 'modalError',
          allowOutsideClick: false
        });
        return;
      }
      this.files.push(file);
      file.status = Dropzone.ADDED;
      this.emit("addedfile", file);
      this._enqueueThumbnail(file);
      return this.accept(file, (function(_this) {
        return function(error) {
          if (error) {
            file.accepted = false;
            _this._errorProcessing([file], error);
          } else {
            file.accepted = true;
            if (_this.options.autoQueue) {
              _this.enqueueFile(file);
            }
          }
          return _this._updateMaxFilesReachedClass();
        };
      })(this));
    };

}
