if (document.location.pathname.includes("comprobante_domicilio")) {

    function comprobante(tipo_comprobante) {
        Swal.fire({
            title: 'Ejemplo',
            html:
            '<img src="/images/webapp/comprobante_' + tipo_comprobante + '.png" style="max-width: 300px;">',
            showConfirmButton: true,
            confirmButtonText: 'Ok',
            customClass: 'tipsWebapp'
        });
    }

    $(document).ready(function() {
        if (navigator.geolocation) {
          var options = {enableHighAccuracy: true, maximumAge: 0};
          navigator.geolocation.getCurrentPosition(showPosition, errorHandler, options);
          navigator.geolocation.watchPosition(showPosition, errorHandler, options);
        } else {
            $('#latitud_reverse').val('');
            $('#longitud_reverse').val('');
            $('#reverse_error').val('Los valores de latitud/longitud del domicilio donde realiza la carga del comprobante de domicilio no se pudieron obtener: Navegador no compatible con geolocalización');
            $('#distancia').val(0);
        }
        var geocoder;
        geocoder = new google.maps.Geocoder();
    });

    function showPosition(position) {

        $('#latitud_reverse').val(position.coords.latitude)
        $('#longitud_reverse').val(position.coords.longitude)

        var latitud = $('#latitud').val();
        var longitud = $('#longitud').val();
        var latitud_reverse = $('#latitud_reverse').val();
        var longitud_reverse = $('#longitud_reverse').val();

        if (latitud != '' || longitud != '') {
            var p1 = new google.maps.LatLng(latitud, longitud);
            var p2 = new google.maps.LatLng(latitud_reverse, longitud_reverse);
            var distancia = calcDistance(p1, p2);
            $('#distancia').val(distancia);
        } else {
            $('#distancia').val(0);
        }

    }

    function errorHandler(error) {

        $('#latitud_reverse').val('');
        $('#longitud_reverse').val('');
        $('#distancia').val(0);
        var error = '';
        switch (error.code) {
        case error.PERMISSION_DENIED:
            error = 'Los valores de latitud/longitud del domicilio donde realiza la carga del comprobante de domicilio no se pudieron obtener: Permiso denegado por el usuario';
            break;
        case error.POSITION_UNAVAILABLE:
            error = 'Los valores de latitud/longitud del domicilio donde realiza la carga del comprobante de domicilio no se pudieron obtener: Posición no disponible';
            break;
        case error.TIMEOUT:
            error = 'Los valores de latitud/longitud del domicilio donde realiza la carga del comprobante de domicilio no se pudieron obtener: Tiempo de espera agotado';
            break;
        default:
            error = 'Los valores de latitud/longitud del domicilio donde realiza la carga del comprobante de domicilio no se pudieron obtener: ' + error.code;
        }
        $('#reverse_error').val(error);

    }

	//calculates distance between two points in km's
	function calcDistance(p1, p2) {
	  return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2)).toFixed(2);
    }

    $('#documentosFaltantes').html('Número de documentos por agregar: 2 </br>');
    Dropzone.options.myDropzone = {
        url: '/webapp/comprobante_domicilio/save',
        autoProcessQueue: false,
        maxFiles: 2,
        maxFilesize: 10,
        uploadMultiple: true,
        parallelUploads: 2,
        acceptedFiles: 'image/*,application/pdf',
        addRemoveLinks: true,
        timeout: 200000,
        preventDuplicates: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        accept: function(file, done) {
            console.log(file);
            var dzClosure = this;
            done();

            var archivos = dzClosure.options.maxFiles - dzClosure.getAcceptedFiles().length;
            $('#documentosFaltantes').html('Número de documentos por agregar: ' + archivos);
            $('.dz-message').show();
            $('.dz-button').html('Arrastra y suelta o da click aquí para continuar agregando archivos');

            if (dzClosure.getAcceptedFiles().length == 2) {

                $("html, body").animate({ scrollTop: $(".subirarchivos").offset().top }, 1000);
                $('#subirArchivos').show();
                $('#documentosFaltantes').hide();
                $('.dz-message').hide();

                document.getElementById("subirArchivos").addEventListener("click", function(e) {

                    e.preventDefault();
                    e.stopPropagation();
                    dzClosure.processQueue();

                    Swal.fire({
                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Cargando archivos... </br> Por favor no cierres ni actualices la página. </p></center>',
                       customClass: 'modalLoading',
                       showCancelButton: false,
                       showConfirmButton:false,
                       allowOutsideClick: false
                   });

                });
            }

        },
        init: function() {
            var dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

            this.on("sendingmultiple", function(file, xhr, formData) {
              formData.append('latitud', $('#latitud').val());
              formData.append('longitud', $('#longitud').val());
              formData.append('latitud_reverse', $('#latitud_reverse').val());
              formData.append('longitud_reverse', $('#longitud_reverse').val());
              formData.append('distancia', $('#distancia').val());
              formData.append('location_error', $('#location_error').val());
              formData.append('reverse_error', $('#reverse_error').val());
              formData.append('device', $('#device').val());
              formData.append('browser', $('#browser').val());
              formData.append('platform', $('#platform').val());
            });

            this.on("error", function(file, message) {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });
               this.removeFile(file);

            });

            this.on("removedfile", function(file, message) {
                if (dzClosure.getAcceptedFiles().length < 2) {
                    $('#subirArchivos').hide();
                    $('.dz-button').html('Arrastra y suelta o da click aquí para continuar agregando archivos');
                    $('.dz-message').show();
                }
                if (dzClosure.getAcceptedFiles().length == 0) {
                    $('.dz-button').html('Arrastra y suelta o da click aquí para agregar los archivos');
                    $('.dz-message').show();
                }
                var archivos = dzClosure.options.maxFiles - dzClosure.getAcceptedFiles().length;
                $('#documentosFaltantes').html('Número de documentos por agregar: ' + archivos);
                $('#documentosFaltantes').show();
            });

            this.on("success", function (response) {

                if (response.xhr.status == 200) {

                    var respuesta = JSON.parse(response.xhr.response);
                    if (respuesta.success == true) {
                        $('.dz-remove').hide();
                        if (respuesta.hasOwnProperty('modal')) {
                            Swal.fire({
                                html: respuesta.modal,
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                customClass: 'modalstatus'
                            });
                        } else if (respuesta.hasOwnProperty('siguiente_paso')) {

                            var paso = respuesta.siguiente_paso;

                            Swal.fire({
                                title: "Carga de archivos completa",
                                text: "Comprobante de domicilio guardado con éxito",
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    Swal.fire({
                                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Redirigiendo... </p></center>',
                                       customClass: 'modalLoading',
                                       showCancelButton: false,
                                       showConfirmButton:false,
                                       allowOutsideClick: false
                                   });
                                   location.href = '/webapp/' + paso;
                                }
                            });

                        }

                    } else {

                        if (respuesta.hasOwnProperty('reload')) {

                            Swal.fire({
                                title: "La sesión expiro",
                                text: 'Recarga la página para continuar',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                   location.reload();
                               }
                           });

                        } else if (respuesta.hasOwnProperty('archivo_invalido')) {
                            this.removeAllFiles( true );
                            Swal.fire({
                                title: 'No se pueden cargar los archivos',
                                text: 'Alguno de los archivos presenta un formato inválido',
                                type: 'error',
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            });
                        }

                    }

                }

            });

        }
    }

    Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastra y suelta o da click aquí para subir los archivos.";
    Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar archivo.";
    Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir este tipo de archivos, solo Imagenes o PDF.";
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes subir 2 archivos como máximo.";
    Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es muy grande ({{filesize}} MB). El tamaño máximo que puedes subir es de: {{maxFilesize}} MB.";
    Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
    Dropzone.prototype.defaultOptions.dictDuplicateFile = "No se pueden subir archivos duplicados";

    Dropzone.prototype.isFileExist = function(file) {
      var i;
      if(this.files.length > 0) {
        for(i = 0; i < this.files.length; i++) {
          if(this.files[i].name === file.name
            && this.files[i].size === file.size
            && this.files[i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
           {
               return true;
           }
        }
      }
      return false;
    };

    Dropzone.prototype.addFile = function(file) {
      file.upload = {
        progress: 0,
        total: file.size,
        bytesSent: 0
      };
      if (this.options.preventDuplicates && this.isFileExist(file)) {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: this.options.dictDuplicateFile,
          type: 'error',
          showConfirmButton: true,
          customClass: 'modalError',
          allowOutsideClick: false
        });
        return;
      }
      this.files.push(file);
      file.status = Dropzone.ADDED;
      this.emit("addedfile", file);
      this._enqueueThumbnail(file);
      return this.accept(file, (function(_this) {
        return function(error) {
          if (error) {
              console.log(error);
            file.accepted = false;
            _this._errorProcessing([file], error);
          } else {
            file.accepted = true;
            if (_this.options.autoQueue) {
              _this.enqueueFile(file);
            }
          }
          return _this._updateMaxFilesReachedClass();
        };
      })(this));
    };

}
