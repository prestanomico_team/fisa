if (document.location.pathname.includes("comprobante_ingresos")) {

    Dropzone.autoDiscover = false;
    var myDropzone = null;

    function dropZone(tipo) {

        var no_archivos = $('#frecuencia').val();
        var tipo_comprobante = $('#tipo_comprobante').val();
        var tipo_archivos = 'image/*,application/pdf';
        var permitidos = 'Imagenes o PDF';
        if (tipo_comprobante == 'estado_cuenta') {
            tipo_archivos = 'application/pdf'
            permitidos = 'PDF';
        }

        var frecuencia = $( "#frecuencia option:selected" ).text();

        if (tipo == 'frecuencia') {

            $('.dz-button').html('Arrastra y suelta o da click aquí para agregar los archivos');
            if (frecuencia == 'Otro') {
                $('.alterno').show();
                $('.regular').hide();
            } else {
                $('.alterno').hide();
                $('.regular').show();
            }

            $("#tipo_comprobante option:selected").removeAttr("selected");
            $("#tipo_comprobante option[value='']").attr('selected', 'selected');
            $("#tipo_comprobante").val('');
            $('#documentosFaltantes').hide();

        }

        if (no_archivos !== null && tipo_comprobante !== null) {

            if (frecuencia == 'Otro') {
                var listado = '<ul><li>Una fotografía de cuerpo entero dentro de tu negocio.</li><li>Una fotografía panorámica que incluya fachada y puerta.</li></ul>';
                $('#documentosAlternos').html(listado);
                $('#documentosAlternos').show();
            } else {
                $('#documentosAlternos').html('');
                $('#documentosAlternos').hide();
            }

            $('#documentosFaltantes').show();
            $('#documentosFaltantes').html('Número de documentos por agregar: ' + no_archivos);
            Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastra y suelta o da click aquí para agregar los archivos";
            Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar archivo.";
            Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir este tipo de archivos, solo " + permitidos;
            Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes subir " + no_archivos + " archivos.";
            Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es muy grande ({{filesize}} MB). El tamaño máximo que puedes subir es de: {{maxFilesize}} MB.";
            Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
            Dropzone.prototype.defaultOptions.dictDuplicateFile = "No se pueden subir archivos duplicados";

            Dropzone.prototype.isFileExist = function(file) {
              var i;
              if(this.files.length > 0) {
                for(i = 0; i < this.files.length; i++) {
                  if(this.files[i].name === file.name
                    && this.files[i].size === file.size
                    && this.files[i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                   {
                       return true;
                   }
                }
              }
              return false;
            };

            Dropzone.prototype.addFile = function(file) {
              file.upload = {
                progress: 0,
                total: file.size,
                bytesSent: 0
              };
              if (this.options.preventDuplicates && this.isFileExist(file)) {
                Swal.fire({
                  title: "Ooops.. Surgió un problema",
                  text: this.options.dictDuplicateFile,
                  type: 'error',
                  showConfirmButton: true,
                  customClass: 'modalError',
                  allowOutsideClick: false
                });
                return;
              }
              this.files.push(file);
              file.status = Dropzone.ADDED;
              this.emit("addedfile", file);
              this._enqueueThumbnail(file);
              return this.accept(file, (function(_this) {
                return function(error) {
                  if (error) {
                    file.accepted = false;
                    _this._errorProcessing([file], error);
                  } else {
                    file.accepted = true;
                    if (_this.options.autoQueue) {
                      _this.enqueueFile(file);
                    }
                  }
                  return _this._updateMaxFilesReachedClass();
                };
              })(this));
            };

            if (Dropzone.instances.length == 0) {

                myDropzone = new Dropzone("div#myDropzone", {
                    url: '/webapp/comprobante_ingresos/save',
                    autoProcessQueue: false,
                    maxFiles: no_archivos,
                    maxFilesize: 10,
                    uploadMultiple: true,
                    parallelUploads: no_archivos,
                    acceptedFiles: tipo_archivos,
                    addRemoveLinks: true,
                    timeout: 420000,
                    forceFallback: false,
                    preventDuplicates: true,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    accept: function(file, done) {
                        console.log(file);
                        var dzClosure = this;
                        done();

                        var archivos = myDropzone.options.maxFiles - myDropzone.getAcceptedFiles().length;
                        $('#documentosFaltantes').html('Número de documentos por agregar: ' + archivos);
                        $('.dz-message').show();
                        $('.dz-button').html('Arrastra y suelta o da click aquí para continuar agregando archivos');

                        if (myDropzone.getAcceptedFiles().length == myDropzone.options.maxFiles) {

                            $("html, body").animate({ scrollTop: $(".subirarchivos").offset().top }, 1000);
                            $('#subirArchivos').show();
                            $('#documentosFaltantes').hide();
                            $('.dz-message').hide();

                            document.getElementById("subirArchivos").addEventListener("click", function(e) {

                                e.preventDefault();
                                e.stopPropagation();
                                dzClosure.processQueue();

                                Swal.fire({
                                   html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Cargando archivos... </br> Por favor no cierres ni actualices la página. </p></center>',
                                   customClass: 'modalLoading',
                                   showCancelButton: false,
                                   showConfirmButton:false,
                                   allowOutsideClick: false
                               });

                            });
                        }
                    },
                    init: function() {

                        this.on("sendingmultiple", function(file, xhr, formData) {
                          formData.append('no_archivos', $('#frecuencia').val());
                          formData.append('tipo_comprobante', $('#tipo_comprobante').val());
                          formData.append('frecuencia', $( "#frecuencia option:selected" ).text());
                        });

                        this.on("error", function(file, message) {
                            Swal.fire({
                                title: "Ooops.. Surgió un problema",
                                text: message,
                                type: 'error',
                                showConfirmButton: true,
                                customClass: 'modalError',
                                allowOutsideClick: false
                            });
                            this.removeFile(file);
                        });

                        this.on("removedfile", function(file, message) {
                            if (myDropzone.getAcceptedFiles().length < myDropzone.options.maxFiles) {
                                $('#subirArchivos').hide();
                                $('.dz-button').html('Arrastra y suelta o da click aquí para agregar los archivos');
                                $('.dz-message').show();
                            }
                            if (myDropzone.getAcceptedFiles().length == 0) {
                                $('.dz-button').html('Arrastra y suelta o da click aquí para agregar los archivos');
                                $('.dz-message').show();
                            }
                            var archivos = myDropzone.options.maxFiles - myDropzone.getAcceptedFiles().length;
                            $('#documentosFaltantes').html('Número de documentos por agregar: ' + archivos);
                            $('#documentosFaltantes').show();
                        });

                        this.on("success", function (response) {

                            if (response.xhr.status == 200) {
                                console.log(response);
                                var respuesta = JSON.parse(response.xhr.response);
                                if (respuesta.success == true) {
                                    $('.dz-remove').hide();
                                    if (respuesta.hasOwnProperty('modal')) {

                                        $('.active').addClass("complete");
                                        $('li').removeClass("active");
                                        $('#paso_actual').addClass("fas fa-check");

                                        Swal.fire({
                                            html: respuesta.modal,
                                            showConfirmButton: false,
                                            allowOutsideClick: false,
                                            customClass: 'modalstatus'
                                        });
                                    } else if (respuesta.hasOwnProperty('siguiente_paso')) {

                                        var paso = respuesta.siguiente_paso;
                                        $('.active').addClass("complete");
                                        $('li').removeClass("active");
                                        $('#paso_actual').addClass("fas fa-check");

                                        Swal.fire({
                                            title: "Carga de archivos completa",
                                            text: "Comprobantes de ingresos guardados con éxito",
                                            showConfirmButton: true,
                                            confirmButtonText: 'Aceptar',
                                            customClass: 'modalError',
                                            allowOutsideClick: false
                                        }).then((result) => {
                                            if (result.value) {
                                                Swal.fire({
                                                   html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p> Redirigiendo... </p></center>',
                                                   customClass: 'modalLoading',
                                                   showCancelButton: false,
                                                   showConfirmButton:false,
                                                   allowOutsideClick: false
                                               });
                                                location.href = '/webapp/' + paso;
                                            }
                                        });

                                    }

                                } else {

                                    if (respuesta.hasOwnProperty('reload')) {

                                        Swal.fire({
                                            title: "La sesión expiro",
                                            text: 'Recarga la página para continuar',
                                            type: 'error',
                                            showConfirmButton: true,
                                            confirmButtonText: 'Aceptar',
                                            customClass: 'modalError',
                                            allowOutsideClick: false
                                        }).then((result) => {
                                            if (result.value) {
                                               location.reload();
                                           }
                                       });

                                    } else if (respuesta.hasOwnProperty('archivo_invalido')) {
                                        this.removeAllFiles( true );
                                        Swal.fire({
                                            title: 'No se pueden cargar los archivos',
                                            text: 'Alguno de los archivos presenta un formato inválido',
                                            type: 'error',
                                            showConfirmButton: true,
                                            confirmButtonText: 'Aceptar',
                                            customClass: 'modalError',
                                            allowOutsideClick: false
                                        });
                                    }

                                }

                            }

                        });
                    }
                });

            } else {


                myDropzone.options.dictMaxFilesExceeded = "Solo puedes subir " + no_archivos + " archivos.";
                myDropzone.options.dictInvalidFileType = "No puedes subir este tipo de archivos, solo " + permitidos;
                myDropzone.options.maxFiles = no_archivos;
                myDropzone.options.acceptedFiles = tipo_archivos;
                myDropzone.options.parallelUploads = no_archivos;
                $('#documentosFaltantes').html('Número de documentos por agregar: ' + no_archivos);
                $('#documentosFaltantes').show();

                if (myDropzone.getAcceptedFiles().length > 0) {
                    var archivos = myDropzone.getAcceptedFiles();
                    $.each(archivos, function(index, archivo) {
                        myDropzone.removeFile(archivo);
                    });
                }

            }

            $('#myDropzone').show();

        }

    }

}
