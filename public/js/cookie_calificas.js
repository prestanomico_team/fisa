//Script to save in input referencia the referrer if it is of "TRANSFER" or  "SITIO"
var SaveReferrer = function() {
    "use strict";
    function start() {
          $('.checa-calificas').on("click",function(e) {
              var segments = window.location.pathname.split( '/' );
              var landing = segments[1].toUpperCase();
              e.preventDefault();
              $.cookie('checa_calificas', 1, { path: '/', expires: 1})
              location.href = '/'+segments[2];
          });
          $('.checa-calificas-hijos').on("click",function(e) {
              var segments = window.location.pathname.split( '/' );
              var landing = segments[1].toUpperCase();
              e.preventDefault();
              $.cookie('checa_calificas', 1, { path: '/', expires: 1})
              location.href = '/registro';
          });
      }
      return {
          start:start
      }
  }();
  
  $(function() {
      SaveReferrer.start();
  });
  