function guardarCuentaClabe() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.lerror').text('');
    var texto = 'Guardando cuenta CLABE...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });
    var datosCuenta = $('#formCuentaCable').serialize();
    axios.post('/webapp/cuenta_clabe/save', datosCuenta).then(function (response) {
        $('.lerror').text('');
        if (response.data.success == true) {

            Swal.close();
            if (response.data.hasOwnProperty('modal')) {
                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            } else if (response.data.hasOwnProperty('siguiente_paso')) {

                var paso = response.data.siguiente_paso;
                location.href = '/webapp/' + paso;

            }

        } else {

            Swal.close();
            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/registro#login';
                    }
               });

           } else if (response.data.hasOwnProperty('errores')) {
               Swal.fire({
                   title: "Error al guardar",
                   text: 'Revisa los campos en rojo',
                   type: 'error',
                   showConfirmButton: true,
                   showCancelButton: false,
                   confirmButtonText: 'Aceptar',
                   customClass: 'modalError',
                   allowOutsideClick: false
               });

               $.each(response.data.errores, function(index, error) {
                   $('#lerror_' + index).css('color', 'red');
                   $('#lerror_' + index).text(error);
               });
           }


        }

    }).catch(function (error) {

        Swal.close();
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function validateCuenta() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.lerror').text('');
    OpenModal('#guardandoDatos');
    var datosCuenta = $('#formCuentaCable').serialize();
    axios.post('/cuenta_clabe/validate', datosCuenta).then(function (response) {
        $('.lerror').text('');
        if (response.data.success == true) {



        } else {



        }

    }).catch(function (error) {


    });

}

function getBancoDescripcion() {
    var optionText = $('#banco option:selected').text();
    $('#descripcion_banco').val(optionText);
}
