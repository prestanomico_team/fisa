/**
 * IPG Mediabrands
 *
 *
 * This file contains IPG Mediabrands Information
 * Use, disclosure or reproduction is prohibited.
 *
 * @author    IPG Mediabrands / Benjamín García

 */


$(document).ready(function () {
  homeSlider();
  testimonialsSlider();
  $('[data-toggle="tooltip"]').tooltip();
  contactButtons();
  tooltips();
  $("#formVerificarSMS").keypress(function(e) {
      //Enter key
      if (e.which == 13) {
        return false;
      }
  });
});


function register() {

  var dateForm = $('.wpcf7 #fechanacimiento');
  dateForm.daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
  });

  dateForm.val('Fecha de Nacimiento');



  // Selección de pantalla el enviar

  document.addEventListener('wpcf7mailsent', function (event) {

    var selectionHan = $(".wpcf7 [name='cliente']:checked").val();
    setCookie("registro", 'si', 365);
    if (selectionHan == 'No') {
      var redirectURL = siteURL + 'gracias';
      window.location.replace(redirectURL);
    } else {
      var redirectURL = siteURL + 'gracias-fmp';
      window.location.replace(redirectURL);
    }
  }, false);

  // Funciones para craear selects
  $('.wpcf7 #gen, .wpcf7 #product').attr('required', 'true');
  $('.wpcf7 #gen option:first, .wpcf7 #product option:first').attr('value', '');
  $('.wpcf7 #gen option:first, .wpcf7 #product option:first').prop('disabled', 'disabled');
  $('.wpcf7 #gen option:first, .wpcf7 #product option:first').attr('selected', 'selected');

  // Ocultar campo No. Monte

  $(".wpcf7 input[type='radio'][name='cliente']").on('click', function () {
    var selection = $(".wpcf7 [name='cliente']:checked").val();
    if (selection == 'No') {
      $('#member').hide();
    } else {
      $('#member').show();
    }
  });

  $('.fa-question-circle').on('click', function () {
    $('#cardfmp').show();
  });
  $('.fa-times').on('click', function () {
    $('#cardfmp').hide();
  });

}

function pmt(rate_per_period, number_of_payments, present_value, future_value, type) {
  future_value = typeof future_value !== 'undefined' ? future_value : 0;
  type = typeof type !== 'undefined' ? type : 0;
  if (rate_per_period != 0.0) {
    var q = Math.pow(1 + rate_per_period, number_of_payments);
    return -(rate_per_period * (future_value + (q * present_value))) / ((-1 + q) * (1 + rate_per_period * (type)));
  } else if (number_of_payments != 0.0) {
    return -(future_value + present_value) / number_of_payments;
  }
  return 0;
}
function moneyFormat(value, decimals){
  if (decimals === undefined) { decimals = 2; }
  var price = (value).toFixed(decimals);
  return '$'+price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function validateEmail(email) {
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if(email.match(mailformat)){
    return true;
  } else {
    return false;
  }
}

function homeSlider() {
  $('.home-slider').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 7000,
    responsive: {
      0: {
        items: 1,
        nav: false,
        dots: true
      },
      600: {
        items: 1,
        nav: false,
        dots: true
      },
      1000: {
        items: 1,
        nav: false,
        dots: true
      }
    }
  })
}
function tooltips() {
  $(function () {
    $('[data-toggle="popover"]').popover();
  });
}
function testimonialsSlider() {
  $('.testimonials-slider').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true,
        dots: false
      },
      600: {
        items: 3,
        nav: true,
        dots: false
      },
      1000: {
        items: 4,
        nav: true,
        dots: false,
        loop: false
      }
    },
    navText: ["<div class='fas fa-chevron-left'>", "<div class='fas fa-chevron-right'>"]
  })
}

function contactButtons() {
  $('#principal-action').on('click', function () {

    if ($('.contact-actions').hasClass('actions-closed')) {

      $('.contact-actions').removeClass('actions-closed');
      $('.contact-actions').addClass('actions-opened');
      $('#principal-action').html('<i class="fas fa-times"></i>');

    } else {

      $('.contact-actions').addClass('actions-closed');
      $('.contact-actions').removeClass('actions-opened');
      $('#principal-action').html('<i class="fas fa-user"></i>');

    }

  });
}

function welcomeModal() {
  if (getCookie("registro") != '' ) {

  }else{
    /* $('#welcomeModale').modal(); */
    setCookie("registro", true, 365);
  }

}

function productsSlider() {
  $('#slider-benefits').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    nextArrow: '<i class="fas fa-chevron-right"></i>',
    prevArrow: '<i class="fas fa-chevron-left"></i>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
  $('#slider-proccess').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    nextArrow: '<i class="fas fa-chevron-right"></i>',
    prevArrow: '<i class="fas fa-chevron-left"></i>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });

}

function setCookie(cname, cvalue, exdays) { var d = new Date(); d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000)); var expires = "expires=" + d.toUTCString(); document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/" }
function getCookie(cname) { var name = cname + "="; var ca = document.cookie.split(';'); for (var i = 0; i < ca.length; i++) { var c = ca[i]; while (c.charAt(0) == ' ') { c = c.substring(1) } if (c.indexOf(name) == 0) { return c.substring(name.length, c.length) } } return "" }
