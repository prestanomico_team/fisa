function iniciarSesion() {

    $("#loginError").html('');
    var texto = 'Iniciando sesión...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });

    var datos = $('#formLogin').serializeArray();
    datos = getFormData(datos);

    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/login/prospecto', datos)
    .then(function (response) {

        if (response.data.success == true) {
            //location.href = '/#';
            var scrolTop = '#simulador';
            $('#loginModal').modal('hide');
            $('#formRegistro').trigger("reset");
            $('#formSimulador').trigger("reset");
            $('#formVerificarSMS').trigger("reset");
            $('#prestamoPersonalPagoFIjo').html('$0.00');

            if (response.data.hasOwnProperty('eventTM')) {
                ga(function(tracker) {
                    tracker.set('dimension1', response.data.eventTM[0].clientId);
                    tracker.set('dimension2', response.data.eventTM[0].userId);
                    tracker.set('dimension3', response.data.eventTM[0].solicId);
                });
                ga('send', 'pageview');
            }

            if (response.data.nueva_solicitud == false) {
                $('#' + response.data.formulario).show();
                $('#' + response.data.oculta).hide();
                $('#formRegistro').hide();
                $('#formRenovaciones').hide();
                $('#linkNoSesion').hide();
                $('#linkSesion').show();
                swal.close();
                var scrolTop = '#info_prestamo_personal';
                $('#info_prestamo_personal').show();
                $("#info_general_prestamo").text(response.data.prestamo);
                $("#info_general_plazo").text(response.data.plazo);
                $("#info_general_finalidad").text(response.data.finalidad);

                if (response.data.cuestionario != null) {
                    $('#cuestionarioDinamico').hide();
                    $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                    var cuestionarios = response.data.cuestionario;

                    $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                    var formulario = "";
                    var situaciones = "";
                    formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
                    formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
                    $.each(cuestionarios, function(key, cuestionario) {
                        formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                        formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                        if (cuestionario.introduccion != null) {
                            formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                        } else {
                            formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                        }

                        $.each(cuestionario.cuestionario, function(index, pregunta) {

                            oculto = '';
                            change = '';
                            largo = 'col-12';
                            width = '100%';
                            if (pregunta.oculto == 1) {
                                oculto = 'display:none';
                            }
                            if (pregunta.grid_estilo != null) {
                                largo = pregunta.grid_estilo;
                            }
                            if (pregunta.ancho_estilo != null) {
                                width = pregunta.ancho_estilo;
                            }
                            id = cuestionario.situacion + '_' + pregunta.id;

                            formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto +"'>";
                            situaciones = situaciones + cuestionario.situacion + '|';
                            if (pregunta.depende != '') {
                                var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                                change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                            }

                            if (pregunta.tipo == 'text') {
                                formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                                var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                                formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                            }

                            if (pregunta.tipo == 'textarea') {
                                formulario = formulario + '<label id="label_' + id +'" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                                var style = 'text-transform: uppercase; float: none important!; resize: none;';
                                formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                            }

                            if (pregunta.tipo == 'select') {
                                formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                                formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                                formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                                opciones = pregunta.opciones.split('|');
                                var opcionesLista = '';
                                $.each(opciones, function(indexO, opcion) {
                                    opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                                });
                                formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                            }
                            formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                            formulario = formulario + "</div>";
                        });

                        $('#cuestionarioDinamico form').append(formulario);
                        formulario = "";

                    });

                    $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
                    $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                    $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
                    $('#situaciones').val(situaciones);
                }
                playinterval();
            } else {
                $('#' + response.data.formulario).show();
                $('#formRegistro').hide();
                swal.close();
                $('#nueva_solicitud').val('true');
            }

            $('#linkNoSesion').hide();
            $('#linkSesion').show();
            $("#goFormularioReactivaNL").hide();
            $("#goFormularioMA").hide();
            $('#nombreProspecto').html(response.data.nombre_prospecto);
            $('#show_celular').html(response.data.celular);

            if (response.data.modal != null) {

                if (response.data.hasOwnProperty('mostrar_oferta')) {

                    if (response.data.mostrar_oferta == true) {
                        Swal.fire({
                            html: response.data.modal,
                            showConfirmButton: false,
                            allowOutsideClick: false,
                            confirmButtonText: 'Aceptar',
                            customClass: 'modalstatus'
                        }).then((result) => {
                           if (result.value) {
                              $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                           }
                       });
                    } else {
                        Swal.fire({
                            html: response.data.modal,
                            showConfirmButton: true,
                            allowOutsideClick: false,
                            confirmButtonText: 'Aceptar',
                            customClass: 'modalstatus'
                        }).then((result) => {
                           if (result.value) {
                              $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                           }
                       });
                    }

                } else {

                    Swal.fire({
                        html: response.data.modal,
                        showConfirmButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalstatus'
                    }).then((result) => {
                       if (result.value) {
                          $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                       }
                   });

                }


            }

        } else {

            $("#loginError").html(response.data.message);
            swal.close();
        }
    })
    .catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            
            if (error.response.hasOwnProperty('data')) {
                descripcionError = error.response.data.message;
            } else {
                descripcionError = error.response.status + ': ' + error.response.responseText;
            }

        } else {
            descripcionError = error;
        }

        $("#loginError").html(descripcionError);
        swal.close();

    });

}
