if (document.location.pathname.includes("password-restore")) {

    /**
     * Realiza la el envio de los datos del formulario para la función que envia el
     * email de restauración de contraseña
     */
    function emailPasswordRestore() {

        var texto = 'Validando usuario...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
        });

        var datos = $('#form-password-restore').serializeArray();
        datos = getFormData(datos);

        axios.post('/email-password-restore', datos)
        .then(function (response) {

            if (response.data.success == false) {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.close();
                    if (response.data.hasOwnProperty('errores') == true) {
                        $.each(response.data.errores, function( key, value ){
                            $('#email-help').html(value);
                        });
                    } else {
                        Swal.fire({
                            title: "Ooops.. Surgió un problema",
                            text: response.data.message,
                            type: 'error',
                            showConfirmButton: true,
                            customClass: 'modalError',
                            allowOutsideClick: false
                       });
                    }

                }

            } else {

                Swal.close();
                $('#temporary-password').show();
                $('#password-restore').hide();
                $('#email_usuario').val(response.data.email);
                if (response.data.celular != '') {
                    $('#celular_enviado').html(response.data.celular);
                } else {
                    $('#celular_enviado').html('registrado');
                }

            }

        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

    /**
     * Realiza el envio de los datos del formulario para la función que valida el
     * password temporal
     */
    function validateTemporaryPassword() {

        var texto = 'Validando código de verificación...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
        });

        var datos = $('#form-temporary-password').serializeArray();
        datos = getFormData(datos);

        axios.post('/temporary-password', datos)
        .then(function (response) {

            if (response.data.success == false) {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.close();
                    if (response.data.hasOwnProperty('errores')) {
                        $.each(response.data.errores, function( key, value ){
                            $('#' + key + '-help').html(value);
                        });
                    } else {
                        Swal.fire({
                            title: "Ooops.. Surgió un problema",
                            text: response.data.message,
                            type: 'error',
                            showConfirmButton: true,
                            customClass: 'modalError',
                            allowOutsideClick: false
                       });
                    }

                }

            } else {

                Swal.close();
                $('#password-update-ok').show();
                $('#temporary-password').hide();

            }

        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

    /**
     * Muestra/oculta el valor del campo contraseña temporal
     */
    function iniciarSesionRP() {
        location.href = '/#loginModal';
    }

    $('#new_password').on('keyup', function() {
       var pass = IsValidPwd(this.value);
    });

    var codigoVerificacion = document.querySelector('#codigo_verificacion');
    codigoVerificacion.addEventListener('keydown', keydownHandler);
    codigoVerificacion.addEventListener('input', inputHandler);

}
