function guardarPerfil() {

    var data = new FormData($('#formPerfil')[0]);
    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/panel/usuarios/perfiles/save",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Guardando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Gurdando datos",
                text: "El nuevo perfil se ha guardado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {
                window.location.href = '/panel/usuarios/perfiles';
            });

        } else {

            swal({
                title: "Se presentó un problema al guardar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

            $.each(resultado.errores, function( key, value ){
                $('#error_' + key).html(value);
            });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al guardar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });
}

function actualizarPerfil() {

    $('#permisos_to option').prop('selected', true);

    var permisos = $('#permisos_to').val();
    var data = new FormData($('#datos_usuario')[0]);
    var id = $('#usuario_id').val();

    data.append('permisos', permisos);

    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/panel/usuarios/perfiles/" + id,
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Actualizando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Actualizando datos",
                text: "Los datos del perfil se han actualizado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {
                window.location.href = '/panel/usuarios/perfiles';
            });

        } else {

            swal({
                title: "Se presentó un problema al actualizar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

           $.each(resultado.errores, function( key, value ){
               $('#error_' + key).html(value);
           });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al actualizar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });
}
