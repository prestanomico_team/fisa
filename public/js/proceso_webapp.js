function iniciarProceso() {
    var OPTIONS_VIDEO = {
        pathImages: "/webapp_files/facematch/img/",
        modeCapture: IcarSDK.MODE_CAPTURE.DOCUMENT_IDCARD,
        callBackFeedBackFunction: onFeedBackCallBack,
        flip_video: false
    }
    IcarSDK.video.initialize(videoInput, OPTIONS_VIDEO);
    IcarSDK.video.getNumberOfCameras(onNumCamsReceivedCallback);

    var OPTIONS_DOCUMENTS = {
        type_document: IcarSDK.TYPE_DOC.IDCARD,
        MAX_ATTEMPTS: -1,
        fontText: "bold 50px Arial",
        colorText: "#c91f28",
        multiplicationFactorBlurring: 2.5,
        MSG_PLACE_DOC_INSIDE: "COLOCA LA IDENTIFICACIÓN\nDENTRO",
        MSG_DOC_OK: "PERMANECE QUIETO",
        MSG_DARK_IMAGE: "IMAGEN OSCURA",
        MSG_BLURRED_IMAGE: "IMAGEN BORROSA",
        MSG_TRY_AGAIN: "INTENTA ENFOCAR EL DOCUMENTO\nACERCANDOLO E INTENTA DE NUEVO"
    }
    IcarSDK.documentCapture.create(videoInput, requestFrameCallback, onDocumentResultCallback, OPTIONS_DOCUMENTS);
    setTimeout(function() {
    }, 2000);
    setTimeout(function() {
        $('#cargando').hide();
        $('#videoDiv').show();
    }, 2000);
}

function iniciarProcesoSelfie () {
    $('#cargando').show();
    setTimeout(function() {}, 2000);
    var OPTIONS_VIDEO = {
        pathImages: "/webapp_files/facematch/img/",
        modeCapture: IcarSDK.MODE_CAPTURE.FACE,
        callBackFeedBackFunction: onFeedBackCallBackFace,
        flip_video: false
    }
    IcarSDK.video.initialize(videoInput, OPTIONS_VIDEO);
    IcarSDK.video.getNumberOfCameras(onNumCamsReceivedCallback);

    var OPTIONS_FACE_CAPTURE = {
        pathImages: "/webapp_files/facematch/img/",
        fontText: "bold 25px Arial",
        colorText: "#c91f28",
        MIN_INTEROCULAR_DISTANCE: 0.4,
        MAX_INTEROCULAR_DISTANCE: 0.13,
        NUMBER_SEC_WAITING_STRING_PROCESS: 3,
        MSG_PLACE_FACE_INSIDE: "COLOCA EL ROSTRO DENTRO",
        MSG_STAY_STILL_AND_OPEN_EYES: "PERMANECE QUIETO Y ABRE LOS OJOS",
        MSG_NO_EYES_DETECTED: "OJOS NO DETECTADOS",
        MSG_COME_CLOSER: "POR FAVOR, ACERCATE",
        MSG_GO_BACK: "POR FAVOR, ALEJATE",
        MSG_BLURRED_IMAGE: "IMAGEN BORROSA",
        MSG_STAY_STILL: "POR FAVOR, PERMANECE QUIETO",
        MSG_PLACE_HEAD_CENTERED: "POR FAVOR, COLOCA EL ROSTRO CENTRADO Y HACIA EL FRENTE",
        MSG_TAKE_OFF_GLASSES: "SI USAS LENTES, POR FAVOR RETIRATELOS",
        MSG_BLINK_EYES: "POR FAVOR, PARPADEA LENTAMENTE",
        MSG_ABORTED: "PROCESO INTERRUMPIDO",
        MSG_STARTING_PROCESS: "INICIANDO\n\nEN %n SEGUNDOS\n\nABRE LOS OJOS Y\nSIGUE LAS INSTRUCCIONES",
        MSG_RESTARTING_PROCESS: "REINICIANDO \n\nEN %n SEGUNDOS\n\nABRE LOS OJOS Y\nSIGUE LAS INSTRUCCIONES",
        MSG_WRONG_POS_DEVICE: "COLOCA EL DISPOSITIVO DE FRENTE",
    }
    IcarSDK.faceCapture.create(videoInput, requestFrameCallback, onFaceCaptureResultCallback, OPTIONS_FACE_CAPTURE);
    setTimeout(function() {
    }, 2000);
    setTimeout(function() {
        $('#cargando').hide();
        $('#videoDiv').show();
    }, 2000);
}

function onNumCamsReceivedCallback(numberCams){
    if (numberCams>1){

    }
}

function askForChangeCameraFunction(){
    IcarSDK.documentCapture.stop(changeCameraFunction);
    hideButtons_stopMode();
}

function changeCameraFunction(){
    IcarSDK.video.changeCamera();
}

function repetirPasoIne() {
    $('#cargando').show();
    setTimeout(function() {
    }, 2000);
    iniciarProceso();
    setTimeout(function() {
        $('#resultado').hide();
        $('#tomar-foto').show();
    }, 2000);
}

function repetirPasoSelfie() {
    $('#cargando').show();
    setTimeout(function() {
    }, 2000);
    iniciarProcesoSelfie();
    setTimeout(function() {
        $('#resultado').hide();
        $('#tomar-foto').show();
    }, 2000);
}

// function to do the request of the frame to the video
//'onFrameReceivedCallback' must to be the next parameters:
// - imageData: frame of the video
// - hashCode: hashCode of the image
function requestFrameCallback(onFrameReceivedCallback) {
    IcarSDK.video.requestFrame(onFrameReceivedCallback);
}


// Return function that the process will use when it will finish
// params:
//  - answerResult: property when is stored the result of the process:
//				IcarSDK.ResultProcess.OK
//				IcarSDK.ResultProcess.FAIL
//              IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED
//  - modeCapture: mode of the capture:
//              IcarSDK.CaptureMode.MANUAL_TRIGGER
//              IcarSDK.CaptureMode.AUTO_TRIGGER
//  - imageResult: property where is stored the image captured
//  - hashCode: property that contains the hashcode generated from the image
//              result and the private key.
function onDocumentResultCallback(answerResult, modeCapture, imageResult, hashCode){

    if (answerResult == IcarSDK.ResultProcess.OK) {
        IcarSDK.video.cleanUp();
        Swal.fire({
          type: 'success',
          title: '¡Documento capturado!',
          showConfirmButton: false,
          timer: 1500
        });

        var newCanvas = document.createElement('canvas');
        newCanvas.width = imageResult.width;
        newCanvas.height = imageResult.height;
        newCanvas.getContext("2d").putImageData(imageResult, 0, 0);

        var canvasResult = document.getElementById("resultImage")
        canvasResult.className = imageResult.className;
        canvasResult.width = imageResult.width;
        canvasResult.height = imageResult.height;
        var context = canvasResult.getContext('2d');
        context.clearRect(0, 0, canvasResult.width, canvasResult.height);
        context.scale($('#videoInput').width/Math.max(newCanvas.width, newCanvas.height), $('#videoInput').width/Math.max(newCanvas.width, newCanvas.height));
        context.drawImage(newCanvas, 0, 0);

        $('#resultado').show();
        $('#tomar-foto').hide();

    }else if(answerResult == IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED){
        alert("Number attempts exceeded!!\nPlease, try again.");
    }else{
        alert("OHPSSS!!!, Process stopped");
    }

}

// Return function that the process will use when it will finish
// params:
//  - answerResult: property when is stored the result of the process:
//				IcarSDK.ResultProcess.OK
//				IcarSDK.ResultProcess.FAIL
//              IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED
//  - modeCapture: mode of the capture:
//              IcarSDK.CaptureMode.MANUAL_TRIGGER
//              IcarSDK.CaptureMode.AUTO_TRIGGER
//  - imageResult: property where is stored the image captured
//  - hashCode: property that contains the hashcode generated from the image
//              result and the private key.
function onFaceCaptureResultCallback(answerResult, modeCapture, imageResult, hashCode){

    if (answerResult == IcarSDK.ResultProcess.OK) {
        IcarSDK.video.cleanUp();
        Swal.fire({
          type: 'success',
          title: '¡Imagen capturada!',
          showConfirmButton: false,
          timer: 1500
        });

        var newCanvas = document.createElement('canvas');
        newCanvas.width = imageResult.width;
        newCanvas.height = imageResult.height;
        newCanvas.getContext("2d").putImageData(imageResult, 0, 0);

        var canvasResult = document.getElementById("resultImage")
        canvasResult.className = imageResult.className;
        canvasResult.width = imageResult.width;
        canvasResult.height = imageResult.height;
        var context = canvasResult.getContext('2d');
        context.clearRect(0, 0, canvasResult.width, canvasResult.height);
        context.scale($('#videoInput').width/Math.max(newCanvas.width, newCanvas.height), $('#videoInput').width/Math.max(newCanvas.width, newCanvas.height));
        context.drawImage(newCanvas, 0, 0);

        $('#resultado').show();
        $('#tomar-foto').hide();

    }else if(answerResult == IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED){
        alert("Number attempts exceeded!!\nPlease, try again.");
    }else{
        alert("OHPSSS!!!, Process stopped");
    }

}


// FUNCTION: onFeedBackCallBack
// Callback function to be called if there is any problem
// executing the webSDK
// params:
//  - resultFeedBack: property where is indicated the result of
//           the execution. The possible values are:
//              IcarSDK.RESULT_VIDEO.OK
//              IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES
//				IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION
//				IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA
//				IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER
//				IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR
function onFeedBackCallBack(resultFeedBack){
    switch(resultFeedBack){
        case IcarSDK.RESULT_VIDEO.OK:
            // Video iniciado correctamente
            startFunction();
            break;
        case IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES:
            alert("Cámara no detectada");
            break;
        case IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION:
            alert("No se concedieron permisos para usar la cámara");
            break;
        case IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA:
            alert("La cámara esta siendo usada en otro proceso");
            break;
        case IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER:
            alert("El navegador no es compatible");
            break;
        case IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR:
            alert("Error desconocido al iniciar la cámara");
            break;
    }
}

function onFeedBackCallBackFace(resultFeedBack){
    switch(resultFeedBack){
        case IcarSDK.RESULT_VIDEO.OK:
            // Video iniciado correctamente
            startFunctionFace();
            break;
        case IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES:
            alert("Cámara no detectada");
            break;
        case IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION:
            alert("No se concedieron permisos para usar la cámara");
            break;
        case IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA:
            alert("La cámara esta siendo usada en otro proceso");
            break;
        case IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER:
            alert("El navegador no es compatible");
            break;
        case IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR:
            alert("Error desconocido al iniciar la cámara");
            break;
    }
}


function startFunction(){
    IcarSDK.documentCapture.start();
}

function startFunctionFace(){
    IcarSDK.faceCapture.start();
}

function stopFunctionFace(){
    IcarSDK.faceCapture.stop();
}

function stopFunction(){
    IcarSDK.documentCapture.stop();
}

function saveImage(tipoDocumento, detalleDocumento) {

    var texto = 'Guardando imagen...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var canvas = document.getElementById('resultImage');
    var dataURL = canvas.toDataURL();
    axios.post('/webapp/subirDocumento', {
        imgBase64: dataURL,
        tipo_documento: tipoDocumento,
        detalle_documento: detalleDocumento
    }).then(function(response) {

      if(response.status == 200) {

          if (response.data.termina_proceso == true) {

              if (response.data.hasOwnProperty('modal')) {
                  Swal.fire({
                      html: response.data.modal,
                      showConfirmButton: false,
                      allowOutsideClick: false,
                      customClass: 'modalstatus'
                  });
              } else if (response.data.hasOwnProperty('siguiente_paso')) {

                  var paso = response.data.siguiente_paso;
                  if (paso != '') {
                      location.href = '/webapp/' + paso;
                  }

              }

          } else {

              if (response.data.hasOwnProperty('siguiente_paso')) {
                  var paso = response.data.siguiente_paso;
                  location.href = '/webapp/' + paso;
              }

          }
      }

    })
    .catch(function(error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function ayuda(tip) {
    Swal.fire({
        title: 'Algunos consejos',
        html:
        '<img src="/images/webapp/' + tip + '.jpg" style="max-width: 300px;">',
        showConfirmButton: true,
        confirmButtonText: 'Entendido',
        customClass: 'tipsWebapp'
    });
}

function launchModal() {
    Swal.fire({
        html: '',
        showConfirmButton: true,
        allowOutsideClick: false,
        customClass: 'modalDocumentos'
    });
}

function saveImagePortal(tipoDocumento, detalleDocumento) {

    var texto = 'Guardando imagen...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var canvas = document.getElementById('resultImage');
    var dataURL = canvas.toDataURL();
    var pageURL = window.location.href;
    var prospecto_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);
   
    axios.post('/webapp/portal/subirDocumento', {
        imgBase64: dataURL,
        tipo_documento: tipoDocumento,
        detalle_documento: detalleDocumento,
        prospecto_id: prospecto_id
    }).then(function(response) {

      if(response.status == 200) {

          if (response.data.termina_proceso == true) {
            console.log(response);
              if (response.data.hasOwnProperty('modal')) {
                  Swal.fire({
                      html: response.data.modal,
                      showConfirmButton: false,
                      allowOutsideClick: false,
                      customClass: 'modalstatus'
                  });
              } else if (response.data.hasOwnProperty('siguiente_paso')) {

                  var paso = response.data.siguiente_paso;
                  if (paso != '') {
                      location.href = '/webapp/portal/' + paso + '/' + prospecto_id;
                      
                  }

              }

          } else {

              if (response.data.hasOwnProperty('siguiente_paso')) {
                  var paso = response.data.siguiente_paso;
                  location.href = '/webapp/portal/' + paso + '/' + prospecto_id;
              }

          }
      }

    })
    .catch(function(error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function saveSelfiePortal(tipoDocumento, detalleDocumento) {

    var texto = 'Guardando imagen...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var canvas = document.getElementById('resultImage');
    var dataURL = canvas.toDataURL();
    var pageURL = window.location.href;
    var prospecto_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);
   
    axios.post('/webapp/portal/subirDocumento', {
        imgBase64: dataURL,
        tipo_documento: tipoDocumento,
        detalle_documento: detalleDocumento,
        prospecto_id: prospecto_id
    }).then(function(response) {

      if(response.status == 200) {

          if (response.data.termina_proceso == true) {
            console.log(response);
              if (response.data.hasOwnProperty('modal')) {
                  Swal.fire({
                      html: response.data.modal,
                      showConfirmButton: false,
                      allowOutsideClick: false,
                      customClass: 'modalstatus'
                  });
              } else if (response.data.hasOwnProperty('siguiente_paso')) {

                  var paso = response.data.siguiente_paso;
                  if (paso != '') {
                      location.href = '/panel/producto/captura/' + response.data.origen;
                      
                  }

              } else if (response.data.termina_proceso == true) {
                location.href = '/panel/producto/captura/' + response.data.origen;
              }

          } else {

              if (response.data.hasOwnProperty('siguiente_paso')) {
                  var paso = response.data.siguiente_paso;
                  location.href = '/panel/producto/captura/sucursal';
              }

          }
      }

    })
    .catch(function(error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

$(document).on('click', "#terminaFlujo", function() {
    location.href = '/'
});
