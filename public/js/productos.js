function guardarProducto() {

    $('#plazos_to option').prop('selected', true);
    $('#finalidades_to option').prop('selected', true);
    $('#ocupaciones_to option').prop('selected', true);

    var plazos = $('#plazos_to').val();
    var finalidades = $('#finalidades_to').val();
    var ocupaciones = $('#ocupaciones_to').val();
    var data = new FormData($('#datos_producto')[0]);

    data.append('plazos', plazos);
    data.append('finalidades', finalidades);
    data.append('ocupaciones', ocupaciones);
    data.append('monto_minimo', $('#monto_minimo').val());
    data.append('monto_maximo', $('#monto_maximo').val());
    data.append('simplificado_monto_minimo', $('#simplificado_monto_minimo').val());
    data.append('simplificado_monto_maximo', $('#simplificado_monto_maximo').val());
    data.append('do_monto_maximo', $('#do_monto_maximo').val());
    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/save",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Guardando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Gurdando datos",
                text: "El nuevo producto se ha guardado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {
                window.location.href = '/productos';
            });

        } else {

            swal({
                title: "Se presentó un problema al guardar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

            $.each(resultado.errores, function( key, value ){
                $('#error_' + key).html(value);
            });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al guardar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });
}

function actualizarProducto() {

    $('#plazos_to option').prop('selected', true);
    $('#finalidades_to option').prop('selected', true);
    $('#ocupaciones_to option').prop('selected', true);

    var plazos = $('#plazos_to').val();
    var finalidades = $('#finalidades_to').val();
    var ocupaciones = $('#ocupaciones_to').val();
    var data = new FormData($('#datos_producto')[0]);
    var id = $('#producto_id').val();
    var logo_name = $('#logo_name').val();

    data.append('plazos', plazos);
    data.append('finalidades', finalidades);
    data.append('ocupaciones', ocupaciones);
    data.append('logo_name', logo_name);
    data.append('monto_minimo', $('#monto_minimo').val());
    data.append('monto_maximo', $('#monto_maximo').val());
    data.append('simplificado_monto_minimo', $('#simplificado_monto_minimo').val());
    data.append('simplificado_monto_maximo', $('#simplificado_monto_maximo').val());
    data.append('do_monto_maximo', $('#do_monto_maximo').val());

    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/" + id,
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Actualizando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Actualizando datos",
                text: "Los datos del producto se han actualizado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {
                window.location.href = '/productos';
            });

        } else {

            swal({
                title: "Se presentó un problema al actualizar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

           $.each(resultado.errores, function( key, value ){
               $('#error_' + key).html(value);
           });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al actualizar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });
}

function coberturaProducto() {

    var data = new FormData($('#cobertura_producto')[0]);
    var id = $('#producto_id').val();
    var campo_cobertura = $('#campo_cobertura').val();

    data.append('campo_cobertura', $('#campo_cobertura').val());

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/" + id + "/cobertura",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Actualizando cobertura',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Actualizando cobertura",
                text: "La cobertura del producto se ha actualizado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {
                window.location.href = '/productos';
            });

        } else {

            swal({
                title: "Se presentó un problema al actualizar la cobertura.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

           $.each(resultado.errores, function( key, value ){
               $('#error_' + key).html(value);
           });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al actualizar la cobertura.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });

}

function agregarPlazo() {

    var data = new FormData($('#formPlazos')[0]);
    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/plazos/agregar",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Guardando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Gurdando datos",
                text: "El nuevo plazo se ha guardado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {

                var rowCount = $('table#tablaPlazos tr:last').index() + 2;
                $("#tablaPlazos").append('<tr><td>' + rowCount + '</td><td>'
                    + $('#duracion').val() + '</td><td>'
                    + $('#plazo').val() + '</td></tr>');

                $('#duracion').val('');
                $('#plazo').val('');

            });

        } else {

            swal({
                title: "Se presentó un problema al guardar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

            $.each(resultado.errores, function( key, value ){
                $('#error_' + key).html(value);
            });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al guardar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });

}

function agregarFinalidad() {

    var data = new FormData($('#formFinalidad')[0]);
    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/finalidad/agregar",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Guardando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Gurdando datos",
                text: "El nuevo plazo se ha guardado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {

                var rowCount = $('table#tablaFinalidad tr:last').index() + 2;
                $("#tablaFinalidad").append('<tr><td>' + rowCount + '</td><td>'
                    + $('#finalidad').val() + '</td><td>');

                $('#finalidad').val('');

            });

        } else {

            swal({
                title: "Se presentó un problema al guardar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

            $.each(resultado.errores, function( key, value ){
                $('#error_' + key).html(value);
            });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al guardar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });

}

function agregarOcupacion() {

    var data = new FormData($('#formOcupacion')[0]);
    $('.help-block').html('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/productos/ocupacion/agregar",
        dataType: "json",
        beforeSend: function() {
            swal({
                title: 'Guardando datos',
                text: 'Espere por favor...',
                onOpen: () => {
                    swal.showLoading()
                }
            });
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            swal({
                title: "Gurdando datos",
                text: "El nuevo plazo se ha guardado con éxito.",
                type: 'success',
                showConfirmButton:true
            }).then((result) => {

                var rowCount = $('table#tablaOcupacion tr:last').index() + 2;
                $("#tablaOcupacion").append('<tr><td>' + rowCount + '</td><td>'
                    + $('#ocupacion').val() + '</td><td>');

                $('#ocupacion').val('');

            });

        } else {

            swal({
                title: "Se presentó un problema al guardar la información.",
                text: resultado.message,
                type: 'error',
                showConfirmButton: true
           });

            $.each(resultado.errores, function( key, value ){
                $('#error_' + key).html(value);
            });

        }

    }).fail(function(resultado, textStatus, errorThrow) {

        swal({
            title: "Se presentó un problema al guardar la información.",
            text: resultado.status + ': ' + errorThrow,
            type: 'error',
            showConfirmButton: true
       });

    });

}

if (window.location.pathname != '/productos') {

    $(document).on('click', '#close-preview', function(){
        $('.image-preview').popover('hide');
        window.popover = false;
    });

    $(function() {
        window.popover = false;
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");

        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: ".......................................",
            placement:'right'
        });

        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Buscar");
            $("#logo_name").val('');
        });

        var image = $('#logo_name').val();

        if (image != null || image != undefined) {
            var ruta = '/images/iconos/';
            loadImage(null, image, ruta);
        }

        $(".image-preview-input input:file").change(function (){
            loadImage(this);
        });

        $(".file-input input:file").change(function (){
            var file = this.files[0];
            $(".file-filename").val(file.name);
        });

        function loadImage($this, image = null, ruta = null) {

            var img = $('<img/>', {
                id: 'dynamic',
                width:90,
                height:35,
                src: ruta + image,
            });

            if (image == null) {
                var file = $this.files[0];
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Cambiar");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                    $("#logo_name").val('');
                }
                reader.readAsDataURL(file);
            } else {
                $(".image-preview-input-title").text("Cambiar");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(image);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }

            $('.image-preview').hover(
                function () {
                    if (window.popover == false) {
                        $('.image-preview').popover('show');
                        window.popover = true;
                    }
                },
            );

            $('.image-preview').mouseleave(
                function () {
                    if (window.popover == false) {
                        $('.image-preview').popover('show');
                    }
                },
            );
        }

        // Datepickers inputs vigencias
        $('#vigencia_de').datetimepicker({
            format: 'dd-M-yyyy',
            minView: 2,
            language: 'es',
            startDate: new Date(),
            autoclose: true,
            pickerPosition: 'top-right',
            linkField: 'vigencia_de_format',
            linkFormat: 'yyyy-mm-dd'
        }).on('changeDate', function(ev) {
            var date = new Date(ev.date.setDate(ev.date.getDate() + 1));
            $('#vigencia_hasta').datetimepicker('setStartDate', date);
            $('#vigencia_hasta').datetimepicker('setInitialDate', date);
            $('#vigencia_hasta').prop('disabled', false);

        });

        $('#vigencia_hasta').datetimepicker({
            format: 'dd-M-yyyy',
            minView: 2,
            language: 'es',
            autoclose: true,
            pickerPosition: 'top-right',
            linkField: 'vigencia_hasta_format',
            linkFormat: 'yyyy-mm-dd'
        });

        $('#vigencia_hasta').prop('disabled', true);

        Inputmask.extendAliases({
            "numeric" : {
                "digits" : 0,
                "digitsOptional" : false,
                "groupSeparator" : ",",
                "radixFocus" : true,
                "autoGroup" : true,
                "autoUnmask": true,
                "removeMaskOnSubmit" : true,
                "min" : -100,
                "max" : 999999999,
            }
        });

        // Inputmask inputs bc score, cat, comisión apertura
        $('#bc_score').inputmask("numeric", {"max" : 1000});
        $('#micro_score').inputmask("numeric", {"max" : 10000});
        $('#cat').inputmask("numeric", {"digits" : 2});
        $('#comision_apertura').inputmask("numeric", {"min": 0, "digits" : 2});
        $('#nombre_producto').change(function() {
            var nombre_campo = slug($('#nombre_producto').val());
            $('#campo_cobertura').val('cobertura_' + nombre_campo);
        });
        $('#do_monto_maximo').inputmask("numeric", {"min": 0});

        // Inputmask inputs monto mínimo y máximo
        $('#monto_minimo').inputmask("numeric").change(function() {
            Inputmask.extendAliases({
                "numeric_maximo" : {
                    "alias":"numeric",
                    "min" : parseInt($('#monto_minimo').val()) + 1,
                }
            });
            $('#monto_maximo').inputmask("numeric_maximo");
        });
        $('#monto_maximo').inputmask("numeric");


        // Inputmask inputs monto mínimo y máximo
        $('#simplificado_monto_minimo').inputmask("numeric").change(function() {
            Inputmask.extendAliases({
                "numeric_maximo" : {
                    "alias":"numeric",
                    "min" : parseInt($('#simplificado_monto_minimo').val()) + 1,
                }
            });
            $('#simplificado_monto_maximo').inputmask("numeric_maximo");
        });
        $('#simplificado_monto_maximo').inputmask("numeric");

        Inputmask.extendAliases({
            "tasa" : {
                "alias":"numeric",
                "digits" : 2,
            }
        });

        // Inputmask inputs edad mínima y máxima
        $('#edad_minima').inputmask("numeric", {"max" : 100}).change(function() {
            Inputmask.extendAliases({
                "edad_maxima" : {
                    "alias":"numeric",
                    "min" : parseInt($('#edad_minima').val()) + 1,
                    "max" : 100,
                }
            });
            $('#edad_maxima').inputmask("edad_maxima");
        });
        $('#edad_maxima').inputmask("numeric", {"max" : 100});

        Inputmask.extendAliases({
            "tasa" : {
                "alias":"numeric",
                "digits" : 2,
            }
        });

        // Inputmask inputs tasa mínima y máxima
        $('#tasa_minima').inputmask("tasa").change(function() {
            Inputmask.extendAliases({
                "tasa_maxima" : {
                    "alias":"tasa",
                    "min" : parseFloat($('#tasa_minima').val()) + 1,
                }
            });
            $('#tasa_maxima').inputmask("tasa_maxima");
        });

        $('#tasa_maxima').inputmask("tasa");

        $('#vigente').change(function() {

            if ( !$('#vigente').prop('checked') ) {
                $('#div_hasta').show();
                if ($('#vigencia_hasta').val() != '') {
                    $('#vigencia_hasta').prop('disabled', false);
                }
            } else {
                $('#div_hasta').hide();
                $('#vigencia_hasta').val('');
                $('#vigencia_hasta_format').val('');
            }

        });

        $('#tipo').change(function() {

            if (this.value == 'Nómina' || this.value == 'Convenio') {
                $('#div_empresa').show();
            } else {
                $('#div_empresa').hide();
                $('#empresa').val('');
            }

        });

        $('#tasa_fija').change(function() {
            if (this.checked) {
                $('#div_tasa_max').hide();
                $('#tasa_maxima').val('');
            } else {
                $('#div_tasa_max').show();
            }

        });

    });

} else {
    Inputmask.extendAliases({
        "numeric" : {
            "digits" : 0,
            "digitsOptional" : false,
            "groupSeparator" : ",",
            "autoUnmask": true,
            "removeMaskOnSubmit" : true,
            "max" : 100,
        }
    });
    $('#duracion').inputmask("numeric", {"max" : 100});
}

var slug = function(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '_').
    replace(/-+/g, '_').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
}


function cambioTipoMontoSimplificado(input) {
    if (input.value == 'definido') {
        $('#div_monto_definido_simplificado').show();
    } else {
        $('#div_monto_definido_simplificado').hide();
        $('#simplificado_monto_maximo').val('');
        $('#simplificado_monto_minimo').val('');
    }
}


function cambioTipoMontoDO(input) {
    if (input.value == 'definido') {
        $('#div_monto_definido_do').show();
    } else {
        $('#div_monto_definido_do').hide();
    }
}

function cambioProcesoSimplificado(input) {
    if (input.checked == true) {
        $('#tipo_monto_simplificado_producto').prop("disabled", false);
        $('#tipo_monto_simplificado_definido').prop("disabled", false);
    } else {
        $('#tipo_monto_simplificado_producto').prop("disabled", true);
        $('#tipo_monto_simplificado_definido').prop("disabled", true);
    }
    $('#simplificado_monto_maximo').val('');
    $('#simplificado_monto_minimo').val('');
    $('#tipo_monto_simplificado_producto').prop("checked", false);
    $('#tipo_monto_simplificado_definido').prop("checked", false);
    $('#div_monto_definido_simplificado').hide();
    $('#facematch_simplificado').prop("checked", false);
    $('#carga_identificacion_selfie_simplificado').prop("checked", false);
}


function cambioDobleOferta(input) {
    if (input.checked == true) {
        $('#tipo_monto_do_producto').prop("disabled", false);
        $('#tipo_monto_do_definido').prop("disabled", false);
    } else {
        $('#tipo_monto_do_producto').prop("disabled", true);
        $('#tipo_monto_do_definido').prop("disabled", true);
    }
    $('#do_monto_maximo').val('');
    $('#tipo_monto_do_producto').prop("checked", false);
    $('#tipo_monto_do_definido').prop("checked", false);
    $('#div_monto_definido_do').hide();
}


function cambioCapturaReferencias(input) {
    if (input.checked == true) {
        $('#envio_sms_referencias').prop("disabled", false);
    } else {
        $('#envio_sms_referencias').prop("disabled", true);
    }
    $('#envio_sms_referencias').prop("checked", false);
}


function cambioProcesoExperian(input) {
    if (input.checked == true) {
        $('#producto_experian').prop("disabled", false);
    } else {
        $('#producto_experian').prop("disabled", true);
    }
    $('#producto_experian').val('');
}
