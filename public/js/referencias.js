function guardarReferencias() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.lerror').text('');
    var texto = 'Guardando referencias...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });
    var referencias = $('#formReferencias').serialize();
    axios.post('/webapp/referencias/save', referencias).then(function (response) {
        $('.lerror').text('');
        if (response.data.success == true) {

            Swal.close();
            if (response.data.hasOwnProperty('modal')) {
                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            } else if (response.data.hasOwnProperty('siguiente_paso')) {

                var paso = response.data.siguiente_paso;
                location.href = '/webapp/' + paso;

            }

        } else {

            Swal.close();
            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/registro#login';
                    }
               });

           } else if (response.data.hasOwnProperty('errores')) {
               Swal.fire({
                   title: "Error al guardar",
                   text: 'Revisa los campos en rojo',
                   type: 'error',
                   showConfirmButton: true,
                   showCancelButton: false,
                   confirmButtonText: 'Aceptar',
                   customClass: 'modalError',
                   allowOutsideClick: false
               });

               $.each(response.data.errores, function(index, error) {
                   $('#lerror_' + index).css('color', 'red');
                   $('#lerror_' + index).text(error);
               });
           }


        }

    }).catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}


if (document.location.pathname.includes("cuenta_clabe") || document.location.pathname.includes("referencias")) {
    window.onload = function () {

        var oldCursor, oldValue, regex = '';

        if (document.location.pathname.includes("cuenta_clabe")) {
            regex = new RegExp(/^\d{0,18}$/g);
        }

        if (document.location.pathname.includes("referencias")) {
            regex = new RegExp(/^\d{0,10}$/g);
        }

        unmask = function(value) {
          var output = value.replace(new RegExp(/[^\d]/, 'g'), '');
          return output;
        }
        keydownHandler = function(e) {
          var el = e.target;
          oldValue = el.value;
          oldCursor = el.selectionEnd;
        }
        inputHandler = function(e) {
            var el = e.target,
            newCursorPosition,
            newValue = unmask(el.value);

            if(newValue.match(regex)) {
              el.value = newValue;
            } else {
              el.value = oldValue;
            }
        }

        if (document.location.pathname.includes("referencias")) {
            var teletelefono_ref1 = document.querySelector('#telefono_ref1');
            teletelefono_ref1.addEventListener('keydown', keydownHandler);
            teletelefono_ref1.addEventListener('input', inputHandler);

            var teletelefono_ref2 = document.querySelector('#telefono_ref2');
            teletelefono_ref2.addEventListener('keydown', keydownHandler);
            teletelefono_ref2.addEventListener('input', inputHandler);

            var teletelefono_ref3 = document.querySelector('#telefono_ref3');
            teletelefono_ref3.addEventListener('keydown', keydownHandler);
            teletelefono_ref3.addEventListener('input', inputHandler);
        }

        if (document.location.pathname.includes("cuenta_clabe")) {
            var clabe_interbancaria = document.querySelector('#clabe_interbancaria');
            clabe_interbancaria.addEventListener('keydown', keydownHandler);
            clabe_interbancaria.addEventListener('input', inputHandler);
        }
    }
}
