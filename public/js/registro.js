function validarSimulador() {

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);
    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesSimulador();
    var nuevaSolicitud = $('#nueva_solicitud').val();
    $('#validacionesSimulador').html(validaciones);

    var datos_faltantes = '';
    if (validaciones == '') {

        axios.post('/validaciones', {
            datos: datos,
            formulario: 'simulador'
        }).then(function (response) {

            if (response.data.success == false) {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {
                    datos_faltantes += '- Verifica los campos en rojo.';
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                }

            } else {

                if (nuevaSolicitud == 'false' && validaciones == '') {
                    getForm('registro', 'simulador');
                    $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                } else if(nuevaSolicitud == 'true' && validaciones == '') {
                    registroNuevaSolicitud()
                }

            }

        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

}

function registroProspecto() {

    var datos = $('#formSimulador,#formRegistro').serializeArray();
    datos = getFormData(datos);

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesRegistro();
    $('#validacionesRegistro').html(validaciones);

    if (validaciones == '') {

        var texto = 'Registrando usuario...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        var clientId = 0;
        ga(function(tracker) {
            clientId = tracker.get('clientId');
        });
        datos['clientId'] = clientId;

        axios.post('/solicitud/registro', datos)
        .then(function (response) {

            if (response.data.success == true) {

                $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                $("#goFormularioReactivaNL").hide();
                $("#goFormularioMA").hide();
                datos['prospecto_id'] = response.data.prospecto_id

                if (response.data.hasOwnProperty('eventTM')) {
                    var eventos = response.data.eventTM;
                    $.each(eventos, function(key, evento) {
                        dataLayer.push(evento);
                    });
                }

                if (response.data.siguiente_paso == 'sms') {
                    envia_sms(datos);
                }

            } else {

                swal.close();
                if (response.data.hasOwnProperty('errores')) {

                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesRegistro').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('siguiente_paso')) {

                    if (response.data.siguiente_paso == 'login') {
                        Swal.fire({
                            title: "Usuario Registrado",
                            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                            type: 'error',
                            showConfirmButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Iniciar Sesión',
                            customClass: 'modalError',
                            cancelButtonText: 'Cancelar',
                            allowOutsideClick: false
                        }).then((result) => {
                            if (result.value) {
                               $('#loginModal').modal('show');
                           }
                       });
                    }

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Reintentar',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                   }).then((result) => {
                      if (result.value) {
                          var texto = 'Registrando usuario...';
                          Swal.fire({
                             html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                             customClass: 'modalLoading',
                             showCancelButton: false,
                             showConfirmButton:false,
                             allowOutsideClick: false
                         });
                         this.registroProspecto(datos);
                      }
                  });

               }
            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }
}
function registroProspectoSindicalizado() {
    var datos = $('#formSimulador,#formRegistro').serializeArray();
    datos = getFormData(datos);

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesRegistro();
    $('#validacionesRegistro').html(validaciones);

    if (validaciones == '') {

        var texto = 'Registrando usuario...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        var clientId = 0;
        ga(function(tracker) {
            clientId = tracker.get('clientId');
        });
        datos['clientId'] = clientId;

        axios.post('/sindicalizados/registro', datos)
        .then(function (response) {

            if (response.data.success == true) {

                $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                $("#goFormularioReactivaNL").hide();
                $("#goFormularioMA").hide();
                datos['prospecto_id'] = response.data.prospecto_id

                if (response.data.hasOwnProperty('eventTM')) {
                    var eventos = response.data.eventTM;
                    $.each(eventos, function(key, evento) {
                        dataLayer.push(evento);
                    });
                }

                if (response.data.siguiente_paso == 'sms') {
                    envia_sms(datos);
                }

            } else {

                swal.close();
                if (response.data.hasOwnProperty('errores')) {

                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesRegistro').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('siguiente_paso')) {

                    if (response.data.siguiente_paso == 'login') {
                        Swal.fire({
                            title: "Usuario Registrado",
                            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                            type: 'error',
                            showConfirmButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Iniciar Sesión',
                            customClass: 'modalError',
                            cancelButtonText: 'Cancelar',
                            allowOutsideClick: false
                        }).then((result) => {
                            if (result.value) {
                               $('#loginModal').modal('show');
                           }
                       });
                    }

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Reintentar',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                   }).then((result) => {
                      if (result.value) {
                          var texto = 'Registrando usuario...';
                          Swal.fire({
                             html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                             customClass: 'modalLoading',
                             showCancelButton: false,
                             showConfirmButton:false,
                             allowOutsideClick: false
                         });
                         this.registroProspecto(datos);
                      }
                  });

               }
            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }
}
function registroNuevaSolicitud(datos) {

    var texto = 'Registrando nueva solicitud...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
	prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;
    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/solicitud/registro/solicitud', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        if (response.data.success == true) {

            var datos = [];
            datos['prospecto_id'] = response.data.prospecto_id;
            datos['celular'] = response.data.celular;
            envia_sms_solicitud(datos)

        } else {
            swal.close();
        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function getFormData(datos){
    var unindexed_array = datos;
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function crea_usuario_ldap(datos) {
    axios.post('/solicitud/registro/ldap', datos)
    .then(function (response) {
        if (response.data.success == true) {
            if (response.data.siguiente_paso == 'sms') {
              envia_sms(datos);
            }
        } else {

            if (response.data.responseLDAP.code == '999') {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Hubo un problema al crear el usuario en LDAP, favor de reintentar.',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'moda',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                    // onOpen: () => swal.getTitle().style.order = -1
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Registrando usuario...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                     this.crea_usuario_ldap(datos);
                  }
              });

           } else {

               Swal.fire({
                   title: "Ooops.. Surgió un problema",
                   text: response.data.message,
                   type: 'error',
                   showConfirmButton: true,
                   customClass: 'modalError',
                   allowOutsideClick: false
              });

           }
        }
    })
    .catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });
}

function envia_sms(datos) {

    axios.post('/solicitud/registro/sms', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'registro')
            $('#info_prestamo_personal').show();
            $('#show_celular').html(response.data.celular);
            swal.close();
        } else {
            if (response.data.actualizar_celular == true) {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        var telefonoCelular = document.querySelector('#celularCorreccion');
                        telefonoCelular.addEventListener('keydown', keydownHandler);
                        telefonoCelular.addEventListener('input', inputHandler);
                    },
                    preConfirm: (e) => {
                        var validacion = validacionesCelular()
                        if (validacion == '') {
                          return true;
                        } else {
                          return false;
                        }
                    }
               }).then((result) => {
                  if (result.value) {
                      var datosCorreccion = $('#formCorreccionCelular').serializeArray();
                      datosCorreccion = getFormData(datosCorreccion);
                      datos['celular'] = datosCorreccion['celular'];
                      datos['actualizar_celular'] = 1;
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                      envia_sms(datos);
                  }
              });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });
}

function reenviar_sms() {

    var texto = 'Reenviando SMS...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });

    axios.post('/solicitud/resend_code')
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            playinterval();
            Swal.fire({
                title: 'Envío exitoso...',
                text: response.data.message,
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',
                customClass: 'moda',
                allowOutsideClick: false
           });

        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                  }
              });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });

}

function envia_sms_solicitud(datos) {

    axios.post('/solicitud/registro/sms', {
        celular: datos['celular'],
        prospecto_id: datos['prospecto_id']
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'simulador');
            $('#info_prestamo_personal').show();
            swal.close();
        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                  }
              });

            } else {

                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });
}

function valida_sms() {
    var datos = $('#formVerificarSMS').serializeArray();
    datos = getFormData(datos);

    var texto = 'Validando SMS...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

   axios.get('/token/prospecto')
    .then(function (response) {

        axios({
            method: 'post',
            url: '/api/conf_sms',
            data: {
                datos
                },
            headers: {
                Authorization: 'Bearer ' + response.data
            }
        })
        .then(function (response) {
            //console.log(response);
            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true && (response.data.siguiente_paso.cambaceo == true || response.data.siguiente_paso.renovacion == true)) {
                if (response.data.siguiente_paso.cambaceo == true) {
                    if (response.data.siguiente_paso.datos_capturados != null) {
                        $.each(response.data.siguiente_paso.datos_capturados, function(key, dato) {
                            if (key == 'antiguedad_domicilio') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'numero_dependientes') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key== 'fuente_ingresos') {
                                $('#' + key).val(capitalize(dato.toLowerCase()));
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'gastos_mensuales') {
                                $('#' + key).val(number_format(dato));
                                //$('#' + key).prop('disabled', true);
                            } else if (key == 'ingreso_mensual') {
                                $('#' + key).val(number_format(dato));
                                //$('#' + key).prop('disabled', true);
                            } else if (key == 'ocupacion') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'residencia') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'empresa') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'antiguedad_empleo') {
                                $('#' + key).val(dato);
                                //$('#' + key).prop('disabled', true);
                            } else if (key == 'telefono_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'calle_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'no_exterior_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'no_interior_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'codigo_postal_empleo') {
                                $('#' + key).val(dato);
                                //$('#' + key).prop('disabled', true);
                            } else if (key == 'select_colonia_empleo') {
                                $('#' + key).val(dato);
                                //$('#' + key).prop('disabled', true);
                            } else if (key == 'delegacion_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'ciudad_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'estado_empleo') {
                                $('#' + key).val(dato);
                                $('#' + key).prop('disabled', true);
                            } else if (key == 'fecha_ingreso') {
                                $('#' + key).val(dato);
                                $('#' + key).combodate('setValue', response.data.siguiente_paso.datos_capturados.fecha_ingreso);

                            }

                        });
                    }
                    getForm(response.data.siguiente_paso.formulario, 'verificar_codigo')
                    swal.close();
                }
                if (response.data.siguiente_paso.renovacion == true) {
                    if (response.data.siguiente_paso.hasOwnProperty('datos_capturados')) {
                        if (response.data.siguiente_paso.datos_capturados != null ) {
                            $.each(response.data.siguiente_paso.datos_capturados, function(key, dato) {
                                if (key == 'rfc') {
                                    $('#' + key).val(dato);
                                    $('#' + key).prop('disabled', true);
                                } else if (key == 'fecha_nacimiento') {
                                    $('#' + key).val(dato);
                                    $('#' + key).combodate('setValue', response.data.siguiente_paso.datos_capturados.fecha_nacimiento);

                                }
                            });
                        }

                    }
                    getForm(response.data.siguiente_paso.formulario, 'verificar_codigo')
                    swal.close();
                }

            } else if (response.data.success == true) {
                getForm(response.data.siguiente_paso.formulario, 'verificar_codigo')
                swal.close();
            } else {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                   });

               }

            }

        })
        .catch(function (error) {

            if (error.response.hasOwnProperty('headers')) {

                var seconds = error.response.headers["retry-after"];
                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                mensaje = error.response.status == 422 ? "Código Invalido": "Intente en:" +seconds +" segundos";

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: mensaje,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });

            } else {

                swal({
                    title: 'Ocurrio un error al procesar la solicitud',
                    text: error,
                    allowOutsideClick: false,
                });

            }

        });


    })
    .catch(function (error) {
        swal({
            title: 'Ocurrio un error al procesar la solicitud',
            text: error,
            allowOutsideClick: false,
        })

    });

    //console.log(token);

}

function actualiza_contraseña() {
    var texto = 'Validando código de verificación...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
        });

        var datos = $('#form-restaurar_password').serializeArray();
        datos = getFormData(datos);

        axios.post('/solicitud/actualizar_contrasena', datos)
        .then(function (response) {
            // $('#restaurar_contraseña').hide();
            if (response.data.success == false) {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.close();
                    if (response.data.hasOwnProperty('errores')) {
                        $.each(response.data.errores, function( key, value ){
                            $('#' + key + '-help').html(value);
                        });
                    } else {
                        Swal.fire({
                            title: "Ooops.. Surgió un problema",
                            text: response.data.message,
                            type: 'error',
                            showConfirmButton: true,
                            customClass: 'modalError',
                            allowOutsideClick: false
                       });
                    }

                }

            } else {
                getForm(response.data.siguiente_paso.formulario, 'verificar_codigo')
                Swal.close();
                $('#restaurar_contraseña').hide();

            }

        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
}

$('#new_password').on('keyup', function() {
    var pass = IsValidPwd(this.value);
 });

function datos_domicilio() {

    var validaciones = validacionesDomicilio();
    $('#validacionesDomicilio').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosDomicilio').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosDomicilio').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosDomicilio').serializeArray();
        datos = getFormData(datos);
        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        axios.post('/solicitud/reg_domicilio', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {
                if (response.data.hasOwnProperty('datos_personales')) {
                    $('#rfc').val(response.data.datos_personales.rfc);
                    $('#rfc').prop('disabled', true);
                    $('#fecha_nacimiento').combodate('setDisabled', true);
                    $('#fecha_nacimiento').combodate('setValue', response.data.datos_personales.fecha_nacimiento);
                }
                getForm('datos_personales', 'datos_domicilio')
                swal.close();
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDomicilio').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }

}

function datos_personales() {

    var validaciones = validacionesDatosPersonales();
    $('#validacionesDatosPersonales').html(validaciones);

    var validar_rfc = validateRFC();
    if (validar_rfc['success'] == false) {
         // En lugar de mostrar el error del RFC, se almacena en una
         // variable para forma
        $('#error_rfc').val(validar_rfc['msj']);
    } else {
        $('#error_rfc').val('');
        $("#rfc-error").html("");
    }

    if (validaciones == '') {

        var disabledSelect = $('#formDatosPersonales').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosPersonales').find('input:disabled').removeAttr('disabled');
        var datos = $('#formDatosPersonales').serializeArray();
        datos = getFormData(datos);
        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        axios.post('/solicitud/datos_personales', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {

                if (response.data.aplica_buro == true) {
                    console.log(response);
                    getForm('datos_buro', 'datos_personales');
                } else {
                    getForm('datos_ingreso', 'datos_personales');
                }
                swal.close();

            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosPersonales').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }

}

function datos_buro() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });
    var seconds = 6;
    var validaciones = validacionesDatosBuro();
    $('#validacionesDatosBuro').html(validaciones);

    if (validaciones == '') {

        var datos = $('#formDatosBuro').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        axios.post('/solicitud/cuentas_de_credito', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {

                if (response.data.hasOwnProperty('eventTM')) {
                    dataLayer.push(response.data.eventTM);
                }

                if (response.data.siguiente_paso == 'primera_llamada_bc' || response.data.siguiente_paso == 'primera_llamada_bc_experian') {

                    var texto = 'Realizando consulta a Buró de Crédito...';
                    var productoTemp = response.data.productoTemp;
                    swal({
                       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                       customClass: 'modalLoading',
                       showCancelButton: false,
                       showConfirmButton:false,
                       allowOutsideClick: false
                    });

                    var url = '/solicitud_bc_score';
                    if (response.data.siguiente_paso == 'primera_llamada_bc_experian') {
                        var url = '/solicitud_bc_score_experian';
                    }

                    primeraLlamadaBC(
                        response.data.prospecto_id,
                        response.data.solicitud_id,
                        url,
                        productoTemp,
                        seconds,
                    );
                }

            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    var errores = '';
                    $.each(response.data.errores, function(key, error) {
                        errores += error + '</br>';
                    });
                    $('#validacionesDatosBuro').html(errores);

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {
                    $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                   });
               }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

}

function BCCorreccion(datos, url) {
    // Identificar de que proceso viene
    var texto = 'Actualizando datos...';
    swal({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud/correccionBC', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

        if (response.data.success == true) {

            if (response.data.siguiente_paso == 'primera_llamada_bc') {

                var texto = 'Realizando consulta a Buró de Crédito...';
                swal({
                   html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                   customClass: 'modalLoading',
                   showCancelButton: false,
                   showConfirmButton:false,
                   allowOutsideClick: false
                });
                primeraLlamadaBC(
                    response.data.prospecto_id,
                    response.data.solicitud_id,
                    url
                );
            }

        } else {

            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/#loginModal';
                        location.reload(true);
                    }
               });

           } else {

               Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });

           }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function datos_ingreso() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosIngreso();
    $('#validacionesDatosIngreso').html(validaciones);

    if (validaciones == '') {

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
        });

        var disabled = $('#formDatosIngreso').find('option:disabled').removeAttr('disabled');
        var datos = $('#formDatosIngreso').serializeArray();
        datos = getFormData(datos);
        disabled.attr('disabled','disabled');

        ingreso = $("#ingreso_mensual").maskMoney('unmasked')[0];
		ingreso = ingreso * 1000;
        datos['ingreso_mensual'] = ingreso;

        gastoMensual = $("#gastos_mensuales").maskMoney('unmasked')[0];
		gastoMensual = gastoMensual * 1000;
        datos['gastos_mensuales'] = gastoMensual;
        datos['clientId'] = clientId;


        axios.post('/solicitud/datos_adicionales', datos)
        .then(function (response) {

            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/#loginModal';
                        location.reload(true);
                    }
               });

           } else {

                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

                if (response.data.hasOwnProperty('eventTM')) {
                    dataLayer.push(response.data.eventTM);
                }
                if (response.data.siguiente_paso == 'datos_empleo') {
                    getForm('datos_empleo', 'datos_ingreso');
                    swal.close()
                } else if (response.data.siguiente_paso == 'alp') {
                    sendALPRequest(
                        response.data.prospecto_id,
                        response.data.solicitud_id,
                        'datos_ingreso'
                    );
                } else if (response.data.siguiente_paso == 'alp_experian') {
                    sendALPExperianRequest(
                        response.data.prospecto_id,
                        response.data.solicitud_id,
                        'datos_ingreso'
                    );
                } else if (response.data.siguiente_paso == 'modal') {
                    Swal.fire({
                        title: response.data.stat,
                        html: response.data.modal,
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/';
                        }
                    });
                }
            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

}

function datos_empleo() {

    var validaciones = validacionesDatosEmpleo();
    $('#validacionesDatosEmpleo').html(validaciones);

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    if (validaciones == '') {

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
        });

        var disabledSelect = $('#formDatosEmpleo').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosEmpleo').find('input:disabled').removeAttr('disabled');
        var datos = $('#formDatosEmpleo').serializeArray();
        datos = getFormData(datos);
        disabled.attr('disabled','disabled');
        disabledSelect.attr('disabled','disabled');
        datos['clientId'] = clientId

        axios.post('/solicitud/datos_empleo', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {

                if (response.data.siguiente_paso == 'alp') {
                    sendALPRequest(
                        response.data.prospecto_id,
                        response.data.solicitud_id,
                        'datos_empleo'
                    );
                }

                if (response.data.siguiente_paso == 'alp_experian') {
                    sendALPExperianRequest(
                        response.data.prospecto_id,
                        response.data.solicitud_id,
                        'datos_empleo'
                    );
                }

                if (response.data.siguiente_paso == 'segunda_llamada' || response.data.siguiente_paso == 'segunda_llamada_experian') {
                    var url = '/solicitud_hawk_experian';
                    if (response.data.siguiente_paso == 'segunda_llamada_experian') {
                        var url = '/solicitud_hawk_experian';
                    }
                    segundaLlamadaBC(response.data.prospecto_id, response.data.solicitud_id, true, url)
                }

            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpleo').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

}

function getCP (cp, empleo = false) {
    var idEmpleo = '';
    if (empleo == true) {
        idEmpleo = '_empleo';
    }
    if(cp.value.length == 5) {
        $.ajax({
            method: "GET",
            url: "/api/geo/" + cp.value
        })
        .done(function( response ) {

            $("#colonia" + idEmpleo).val('');
            $("#colonia" + idEmpleo).attr("placeholder","Ingrese Código Postal");
            $("#colonia_id .trigger-scroll").html('<label style="color: #b7b7b7;">Ingrese Código Postal</label>');
            $("#delegacion" + idEmpleo).val('');
            $("#delegacion" + idEmpleo).attr("placeholder","Ingrese Código Postal");
            $("#ciudad" + idEmpleo).val('');
            $("#ciudad" + idEmpleo).attr("placeholder","Ingrese Código Postal");
            $("#estado" + idEmpleo).val('');
            $("#estado" + idEmpleo).attr("placeholder","Ingrese Código Postal");
            $('#estado_id .trigger-scroll').html('<label style="color: #b7b7b7;">Ingrese Código Postal</label>');

            if(response.success == true) {

                $('#select_colonia' + idEmpleo).empty();
                $('#select_colonia' + idEmpleo).append($('<option>', {
                    value: '',
                    text : 'Seleccionar Colonia',
                    disabled : true,
                    selected : true
                }));

                for (var i = 0; i < response.colonias.length; i++) {

                    $('#select_colonia' + idEmpleo).append($('<option>', {
                        value: response.colonias[i].id,
                        text : response.colonias[i].colonia
                    }));

                }

                $("#id_colonia" + idEmpleo).val(response.id_colonia);
                $("#delegacion" + idEmpleo).val(response.deleg_munic);
                $("#id_deleg_munic" + idEmpleo).val(response.id_deleg_munic);
                $("#ciudad" + idEmpleo).val(response.ciudad);
                $("#id_ciudad" + idEmpleo).val(response.id_ciudad);
                if (response.ciudad == '') {
                    $("#ciudad" + idEmpleo).attr("placeholder", "NO APLICA");
                }
                $("#estado" + idEmpleo).val(response.estado);
                $("#id_estado" + idEmpleo).val(response.id_estado);
                $("#codigo_estado" + idEmpleo).val(response.codigo_estado);
                if (empleo == false) {
                    $("#cobertura").val(response.cobertura);
                }

            } else if (response.response == "error") {

                $("#delegacion" + idEmpleo).attr("readonly", !0);
                $("#ciudad" + idEmpleo).attr("readonly", !0);
                $("#colonia" + idEmpleo).html("Ingrese Código Postal");
                $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
                $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
                $("#estado" + idEmpleo).html("Ingrese Código Postal");
                alert(response.message);

            } else if (response.response == "Not Found") {

                $("#cp-error" + idEmpleo).html("C.P. no encontrado").slideDown("slow");
                $("#delegacion" + idEmpleo).val("");
                $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $("#ciudad" + idEmpleo).val("");
                $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $("#estado" + idEmpleo).val("");
                $("#estado" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $('#select_colonia' + idEmpleo).empty();
                $('#select_colonia' + idEmpleo).append($('<option>', {
                    value: 0,
                    text : 'Ingrese Código Postal Válido'
                }));

            }
        }).
        fail(function() {

        });

    } else {

        $("#colonia" + idEmpleo).val('');
        $("#delegacion" + idEmpleo).val('');
        $("#delegacion" + idEmpleo).attr("placeholder","Ingrese Código Postal");
        $("#ciudad" + idEmpleo).val('');
        $("#ciudad" + idEmpleo).attr("placeholder","Ingrese Código Postal");
        $("#estado" + idEmpleo).val('');
        $("#estado" + idEmpleo).attr("placeholder","Ingrese Código Postal");

    }

}

$("#select_colonia").on("change", function(e) {
    var colonia = $(this).find('option:selected').text();
    $('#colonia').val(colonia);
});

$("#select_colonia_empleo").on("change", function(e) {
    var colonia = $(this).find('option:selected').text();
    $('#colonia_empleo').val(colonia);
});

function cambioEstadoNacimiento() {

    var estado = $('#estado_nacimiento').val();
    if (estado != "0") {
        var retries = 3;
        var ajaxSettings = {
            cache: false,
            method: "GET",
            url: "/api/ciudades/" + estado,
            dataType: "json",
            timeout: 6000,
            beforeSend: function() {

           },
        };

        $.ajax(ajaxSettings)
        .done(function(response){ onDone(response) })
        .fail(function(xhr, textStatus, errorThrown){ onFail(xhr, textStatus, errorThrown) });

        function onFail(xhr, textStatus, errorThrown){

           if (retries > 0) {
                retries = retries - 1;
                $.ajax(ajaxSettings)
                .done(function(response){ onDone(response) })
                .fail(function(xhr, textStatus, errorThrown){ onFail(xhr, textStatus, errorThrown) });
            } else {
                swal({
                  text: "Ooops.. Surgió un problema al buscar las Ciudades, por favor selecciona el Estado en el campo Lugar de Nacimiento de nuevo",
                  showConfirmButton:true
                });
               $('#estado_nacimiento').val("0").change();
            }

        }

        function onDone(response){

            if (response.success == true) {
                $('#ciudad_nacimiento').empty();
                $('#ciudad_nacimiento').append($('<option>', {
                    value: '',
                    text : 'Ciudad de nacimiento*',
                    disabled : true,
                    selected : true
                }));
                for (var i = 0; i < response.count; i++) {

                    $('#ciudad_nacimiento').append($('<option>', {
                        value: response.municipios[i],
                        text : response.municipios[i]
                    }));

                }
            } else {
                $('#ciudad_nacimiento').empty();
                $('#ciudad_nacimiento').append($('<option>', {
                    value: '',
                    text : 'Ciudad de nacimiento*',
                    disabled : true,
                    selected : true
                }));
            }

        }
    }
}

function primeraLlamadaBC(prospecto_id, solicitud_id, url, productoTemp, seconds) {
    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    axios.post(url, {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        clientId: clientId,
        productoTemp: productoTemp,
        seconds: seconds,
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.hasOwnProperty('eventTM')) {
            var eventos = response.data.eventTM;
            $.each(eventos, function(key, evento) {
                dataLayer.push(evento);
            });
        }

        var urlSegundaLlamada = '/solicitud_hawk';
        if (url.includes('experian')) {
            urlSegundaLlamada = '/solicitud_hawk_experian';
        }

        if (response.data.success == true) {

            if (response.data.stat == "Califica BC Score") {
                var productoTemp = response.data.productoTemp;
                if (response.data.lead == 'askrobin') {
                    sendPixelAskRobin('3', response.data.lead_id, prospecto_id);
                }
                segundaLlamadaBC(prospecto_id, solicitud_id, false, urlSegundaLlamada, productoTemp);

            } else if (response.data.stat == "Datos Elegible") {
                getForm('datos_elegible', 'datos_buro')
                swal.close();
            } else if (response.data.stat == "Califica BC Score Elegible") {
                segundaLlamadaBCScoreNoHit(prospecto_id, solicitud_id, false);
            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });

            }

        } else {

            if (response.data.hasOwnProperty('maximosReintentos')) {
                Swal.fire({
                    title: 'Falta información de buró',
                    html: response.data.message,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                      location.href = response.data.redirect;
                    }
                });
            }

            if (response.data.stat == 'Error BC No Response') {
                var seconds = response.data.seconds;
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    cancelButtonText: 'Cancelar',
                    customClass: 'modalError',
                    allowOutsideClick: false
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Realizando consulta a Buró de Crédito...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                     primeraLlamadaBC(prospecto_id, solicitud_id, url, productoTemp, seconds);
                  }
              });
           }

           if (response.data.stat == 'No Autenticado') {
                Swal.fire({
                   title: response.data.stat,
                   html: 'No pudimos encontrar tu información en Buró de Crédito. <br><br> Revisa la ayuda que aparece en cada una de las preguntas y verifica que la informacíon proporcionada sea la correcta',
                   type: 'error',
                   showConfirmButton: true,
                   confirmButtonText: 'Aceptar',
                   customClass: 'modalError',
                   allowOutsideClick: false
               });
            }

            if (response.data.stat == 'Error en Captura') {
                Swal.fire({
                    title: 'La consulta a Buró de Crédito arrojo un error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        $('#fecha_nacimientoBC').combodate({
                          maxYear: moment().get('year') - response.data.edad_minima,
                          minYear: moment().get('year') - response.data.edad_maxima,
                          firstItem: 'name',
                          smartDays: true
                        });
                        $('.combodate').css({"width": "100%"});

                        $("#formCorreccion #year").on("change", function(e) {
                            var valor = $(this).find('option:selected').text();
                            $('#formCorreccion #year').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });

                        $("#formCorreccion #month").on("change", function(e) {
                            var valor = $(this).find('option:selected').val();
                            $('#formCorreccion #month').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });

                        $("#formCorreccion #day").on("change", function(e) {
                            var valor = $(this).find('option:selected').text();
                            valor = parseInt(valor);
                            $('#formCorreccion #day').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });
                    },
                    preConfirm: (e) => {
                        var validacion = validacionesBCCorreccion()
                        if (validacion == '') {
                          return true;
                        } else {
                          return false;
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                       var datos = $('#formCorreccion').serializeArray();
                       datos = getFormData(datos);
                       var url = '/solicitud_bc_score';
                       if (response.data.siguiente_paso == 'primera_llamada_bc_experian') {
                           var url = '/solicitud_bc_score_experian';
                       }
                       BCCorreccion(datos, url)
                    }
                });
            }

            if (response.data.stat == 'No Califica') {
                Swal.fire({
                    title: response.data.stat,
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/';
                    }
                });
          }

          if (response.data.stat == 'No Encontrado') {
              Swal.fire({
                  title: 'Falta información de buró',
                  html: response.data.modal,
                  showConfirmButton: true,
                  confirmButtonText: 'Aceptar',
                  customClass: 'modalError',
                  allowOutsideClick: false
              }).then((result) => {
                  if (result.value) {
                    location.href = '/';
                  }
              });
          }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });
}

function segundaLlamadaBC(prospecto_id, solicitud_id, elegible = false, url, producto) {
    axios.post(url, {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        elegible: elegible,
        producto: producto
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.stat == "Segunda Llamada BC OK") {
            getForm('datos_ingreso', 'datos_buro')
            swal.close();
        } else if (response.data.stat == "Segunda Llamada BC OK Elegible") {
            sendALPExperianRequest(
                response.data.prospecto_id,
                response.data.solicitud_id,
                'datos_empleo'
            );
        } else if (response.data.stat == "No Encontrado") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then((result) => {
              if (result.value) {
                  var texto = 'Realizando consulta a Buró de Crédito...';
                  Swal.fire({
                     html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                     customClass: 'modalLoading',
                     showCancelButton: false,
                     showConfirmButton:false,
                     allowOutsideClick: false
                 });
                 segundaLlamadaBC(prospecto_id, solicitud_id, false, url);
              }
            });

        } else if (response.data.stat == "Error BC") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then((result) => {
              if (result.value) {
                  var texto = 'Realizando consulta a Buró de Crédito...';
                  Swal.fire({
                     html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                     customClass: 'modalLoading',
                     showCancelButton: false,
                     showConfirmButton:false,
                     allowOutsideClick: false
                 });
                 segundaLlamadaBC(prospecto_id, solicitud_id, false, url);
              }
            });

        }

    })
    .catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function segundaLlamadaBCScoreNoHit(prospecto_id, solicitud_id, elegible = false) {
    axios.post('/solicitud_score_no_hit', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        elegible: elegible
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.stat == "Segunda Llamada BC OK") {
            getForm('datos_ingreso', 'datos_buro')
            swal.close();
        } else if (response.data.stat == "Segunda Llamada BC OK Elegible") {
            sendALPExperianRequest(
                response.data.prospecto_id,
                response.data.solicitud_id,
                'datos_empleo'
            );
        } else if (response.data.stat == "No Encontrado") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then((result) => {
              if (result.value) {
                  var texto = 'Realizando consulta a Buró de Crédito...';
                  Swal.fire({
                     html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                     customClass: 'modalLoading',
                     showCancelButton: false,
                     showConfirmButton:false,
                     allowOutsideClick: false
                 });
                 segundaLlamadaBCScoreNoHit(prospecto_id, solicitud_id, false);
              }
            });

        } else if (response.data.stat == "Error BC") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then((result) => {
              if (result.value) {
                  var texto = 'Realizando consulta a Buró de Crédito...';
                  Swal.fire({
                     html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                     customClass: 'modalLoading',
                     showCancelButton: false,
                     showConfirmButton:false,
                     allowOutsideClick: false
                 });
                 segundaLlamadaBCScoreNoHit(prospecto_id, solicitud_id, false);
              }
            });

        }

    })
    .catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function sendALPRequest (prospecto_id, solicitud_id, ocultar) {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var texto = 'Estamos procesando tu solicitud...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud_fico_alp', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        clientId: clientId
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        var respuesta = response.data;
        if (respuesta.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            var tipoPoblacion = respuesta.tipo_poblacion;
            var pantallasExtra = 0;

            $('#encabezadoModal').html(respuesta.plantilla.modal_encabezado);
            $('#imgModal').html(respuesta.plantilla.modal_img);
            $('#cuerpoModal').html(respuesta.plantilla.modal_cuerpo);
            var decision = respuesta.decision;

            if (tipoPoblacion == 'oferta_normal' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_normal.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_incrementada' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_doble' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            }

            // Si la respuesta a la decision del stored procedure es 3 (Invitación
            // a continuar) y no hay pantallas extras se debe mostrar el modal
            // de oferta
            if (pantallasExtra == 0 && decision == 3) {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modaloferta'
                });

            } else if (pantallasExtra == 1 && decision == 3) {
            // Si la respuesta a la decisión del stored procedure es 3 (Invitación
            // a continuar) y hay pantallas extras se debe mostrar el cuestionario
            // fijo

                // Creando el cuestionario dinámico
                var cuestionarios = respuesta.cuestionario;
                $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                var formulario = "";
                var situaciones = "";
                formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
                formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
                $.each(cuestionarios, function(key, cuestionario) {
                    formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                    formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                    if (cuestionario.introduccion != null) {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                    } else {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                    }

                    $.each(cuestionario.cuestionario, function(index, pregunta) {

                        oculto = '';
                        change = '';
                        largo = 'col-12';
                        width = '100%';
                        if (pregunta.oculto == 1) {
                            oculto = 'display:none';
                        }
                        if (pregunta.grid_estilo != null) {
                            largo = pregunta.grid_estilo;
                        }
                        if (pregunta.ancho_estilo != null) {
                            width = pregunta.ancho_estilo;
                        }
                        id = cuestionario.situacion + '_' + pregunta.id;

                        formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto +"'>";
                        situaciones = situaciones + cuestionario.situacion + '|';
                        if (pregunta.depende != '') {
                            var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                            change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                        }

                        if (pregunta.tipo == 'text') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                            formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                        }

                        if (pregunta.tipo == 'textarea') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; float: none important!; resize: none;';
                            formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                        }

                        if (pregunta.tipo == 'select') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                            formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                            opciones = pregunta.opciones.split('|');
                            var opcionesLista = '';
                            $.each(opciones, function(indexO, opcion) {
                                opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                            });
                            formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                        }
                        formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                        formulario = formulario + "</div>";
                    });

                    $('#cuestionarioDinamico form').append(formulario);
                    formulario = "";

                });

                $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="modelo" name="modelo" value="Prestanómico"/>');
                $('#situaciones').val(situaciones);

                $('#' + ocultar).hide();
                $('#cuestionarioDinamico').show();
                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                swal.close();


            } else if (decision == 2 || decision == 1) {
            // Si la respuesta a la decisión del stored procedure es 2 (Oferta
            // diferida) ó 1 (Rechazado) solo se debe mostrar el modal del status
            // de la solicitud
                Swal.fire({
                    html: respuesta.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });

            }

        } else {

        }

    })
    .catch(function (error) {
        swal.close();
    });

}

function sendALPExperianRequest(prospecto_id, solicitud_id, ocultar) {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var texto = 'Estamos procesando tu solicitud...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud_fico_alp_experian', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        clientId: clientId
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        var respuesta = response.data;
        if (respuesta.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            var tipoPoblacion = respuesta.tipo_poblacion;
            var decision = respuesta.decision;
            var pantallasExtra = respuesta.cuestionarioDinamico;

            //$('#encabezadoModal').html(respuesta.plantilla.modal_encabezado);
            //$('#imgModal').html(respuesta.plantilla.modal_img);
            //$('#cuerpoModal').html(respuesta.plantilla.modal_cuerpo);

            // Si la respuesta a la decision del stored procedure es 3 (Invitación
            // a continuar) y no hay pantallas extras se debe mostrar el modal
            // de oferta
            if (pantallasExtra == 0 && decision == 3) {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modaloferta'
                });

            } else if (pantallasExtra == 1 && decision == 3) {
            // Si la respuesta a la decisión del stored procedure es 3 (Invitación
            // a continuar) y hay pantallas extras se debe mostrar el cuestionario
            // fijo

                // Creando el cuestionario dinámico
                var cuestionarios = respuesta.cuestionario;
                $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                var formulario = "";
                var situaciones = "";
                formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
                formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
                $.each(cuestionarios, function(key, cuestionario) {
                    formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                    formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                    if (cuestionario.introduccion != null) {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                    } else {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                    }

                    $.each(cuestionario.cuestionario, function(index, pregunta) {

                        oculto = '';
                        change = '';
                        largo = 'col-12';
                        width = '100%';
                        if (pregunta.oculto == 1) {
                            oculto = 'display:none';
                        }
                        if (pregunta.grid_estilo != null) {
                            largo = pregunta.grid_estilo;
                        }
                        if (pregunta.ancho_estilo != null) {
                            width = pregunta.ancho_estilo;
                        }
                        id = cuestionario.situacion + '_' + pregunta.id;

                        formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto +"'>";
                        situaciones = situaciones + cuestionario.situacion + '|';
                        if (pregunta.depende != '') {
                            var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                            change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                        }

                        if (pregunta.tipo == 'text') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                            formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                        }

                        if (pregunta.tipo == 'textarea') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; float: none important!; resize: none;';
                            formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                        }

                        if (pregunta.tipo == 'select') {
                            formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                            formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                            opciones = pregunta.opciones.split('|');
                            var opcionesLista = '';
                            $.each(opciones, function(indexO, opcion) {
                                opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                            });
                            formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                        }
                        formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                        formulario = formulario + "</div>";
                    });

                    $('#cuestionarioDinamico form').append(formulario);
                    formulario = "";

                });

                $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="modelo" name="modelo" value="' + response.data.modeloPredominante + '"/>');
                $('#situaciones').val(situaciones);

                $('#' + ocultar).hide();
                $('#cuestionarioDinamico').show();
                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                swal.close();


            } else if (decision == 2 || decision == 1) {
            // Si la respuesta a la decisión del stored procedure es 2 (Oferta
            // diferida) ó 1 (Rechazado) solo se debe mostrar el modal del status
            // de la solicitud
                Swal.fire({
                    html: respuesta.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });

            }

        } else {

        }

    })
    .catch(function (error) {
        swal.close();
    });

}

function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function capitalize(word) {
    return word[0].toUpperCase() + word.slice(1);
}

function parseHttpHeaders(httpHeaders) {
    return httpHeaders.split("\n")
     .map(x=>x.split(/: */,2))
     .filter(x=>x[0])
     .reduce((ac, x)=>{ac[x[0]] = x[1];return ac;}, {});
}

function getForm(siguientePaso = null, anterior = null) {

	axios.get('/getForm', {
      	params: {
      		paso: siguientePaso,
		}
	})
	.then(function(response) {
        $('#loading-form').hide();
		$('#' + response.data.formulario).show();

		if (anterior != null) {
			$('#' + anterior).hide();
		} else if (response.data.oculta != null && response.data.nueva_solicitud == false) {
            $('#info_prestamo_personal').show();
            $('#' + response.data.oculta).hide();
            $('#renovaciones').hide();
        }

        if (response.data.formulario == 'verificar_codigo') {
            $('#cuestionarioDinamico').hide();
            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            playinterval();
        }

        if (response.data.nueva_solicitud == true) {
            $('#nueva_solicitud').val('true');
        }

        if (response.data.hasOwnProperty('ultimo_status')) {
            if (response.data.ultimo_status == 'Registro') {
                Swal.fire({
                    title: "Aviso Importante",
                    html: response.data.modal_confianza,
                    showConfirmButton: true,
                    customClass: 'modalConfianza',
                    allowOutsideClick: false
               });
            }
        }

        if (response.data.formulario == 'datos_personales') {
            if (response.data.renovacion == true) {
                console.log(response);
                if (response.data.hasOwnProperty('datos_capturados')) {
                    $.each(response.data.datos_capturados, function(key, dato) {
                        if (key == 'rfc') {
                            $('#' + key).val(dato);
                            $('#' + key).prop('disabled', true);
                        } else if (key == 'fecha_nacimiento') {
                            $('#' + key).val(dato);
                            $('#' + key).combodate('setValue', response.data.datos_capturados.fecha_nacimiento);

                        }
                    });
                }
            }
        }

        if (response.data.hasOwnProperty('sesionIniciada')) {
            $('#linkNoSesion').hide();
            $('#linkSesion').show();
            $('#nombreProspecto').html(response.data.nombre_prospecto);
        }

        if (response.data.redirect != null) {
            var texto = 'Faltan documentos por completar </br> Redirigiendo...';
            Swal.fire({
               html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
               customClass: 'modalLoading',
               showCancelButton: false,
               showConfirmButton:false,
               allowOutsideClick: false
           });
            location.href = response.data.redirect;
        } else if (response.data.cuestionario != null) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            var cuestionarios = response.data.cuestionario;

            $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
            var formulario = "";
            var situaciones = "";
            formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
            formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
            $.each(cuestionarios, function(key, cuestionario) {
                formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                if (cuestionario.introduccion != null) {
                    formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                } else {
                    formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                }

                $.each(cuestionario.cuestionario, function(index, pregunta) {

                    oculto = '';
                    change = '';
                    largo = 'col-12';
                    width = '100%';
                    if (pregunta.oculto == 1) {
                        oculto = 'display:none';
                    }
                    if (pregunta.grid_estilo != null) {
                        largo = pregunta.grid_estilo;
                    }
                    if (pregunta.ancho_estilo != null) {
                        width = pregunta.ancho_estilo;
                    }
                    id = cuestionario.situacion + '_' + pregunta.id;

                    formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto +"'>";
                    situaciones = situaciones + cuestionario.situacion + '|';
                    if (pregunta.depende != '') {
                        var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                        change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                    }

                    if (pregunta.tipo == 'text') {
                        formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                        var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                        formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                    }

                    if (pregunta.tipo == 'textarea') {
                        formulario = formulario + '<label id="label_' + id +'" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                        var style = 'text-transform: uppercase; float: none important!; resize: none;';
                        formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                    }

                    if (pregunta.tipo == 'select') {
                        formulario = formulario + '<label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label>';
                        formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                        formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                        opciones = pregunta.opciones.split('|');
                        var opcionesLista = '';
                        $.each(opciones, function(indexO, opcion) {
                            opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                        });
                        formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                    }
                    formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                    formulario = formulario + "</div>";
                });

                $('#cuestionarioDinamico form').append(formulario);
                formulario = "";

            });

            $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
            $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
            $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
            $('#cuestionarioDinamico form').append('<input type="hidden" id="modelo" name="modelo" value="' + response.data.modelo + '"/>');
            $('#situaciones').val(situaciones);

        } else if (response.data.mostrar_oferta == true) {

            Swal.fire({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modaloferta'
            });

        }

	})
	.catch(function(error) {

	});

}
