function focusFormulario() {
    $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
}

function getRFC(uuid) {

    axios.post('/renovaciones/buscarRFC', {uuid:uuid})
    .then(function (response) {

        if (response.data.success == true) {

            $('#rfc_renovaciones').val(response.data.rfc);
            $('#fecha_nacimiento_renovaciones').combodate('setValue', response.data.fecha_nacimiento);

        } else {

            if (response.data.hasOwnProperty('errores')) {
                var msj = '';
                $.each(response.data.errores, function(key, error) {
                    msj = msj + error;
                });

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: msj,
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                });

            } else if (response.data.siguiente_paso == 'modal') {
               Swal.fire({
                   title: response.data.stat,
                   html: response.data.modal,
                   showConfirmButton: true,
                   confirmButtonText: 'Aceptar',
                   customClass: 'modalError',
                   allowOutsideClick: false
               })
           }

        }

    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}

function registroRenovaciones() {

    var datos = $('#formRenovaciones').serializeArray();
    datos = getFormData(datos);

    var validaciones = '';
    var validaciones = validacionesRenovaciones();
    $('#validacionesRegistro').html(validaciones);

    if (validaciones == '') {

        var texto = 'Buscando oferta...';
        Swal.fire({
           html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        var clientId = 0;
        ga(function(tracker) {
            clientId = tracker.get('clientId');
        });
        datos['clientId'] = clientId;

        axios.post('/renovaciones/buscarOferta', datos)
        .then(function (response) {

            if (response.data.success == true) {

                $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                datos['prospecto_id'] = response.data.prospecto_id

                $("#info_general_prestamo").text(moneyFormat(response.data.prestamo.monto));
    			$("#info_general_plazo").text(response.data.prestamo.plazo);
    			$("#info_general_finalidad").text(response.data.prestamo.finalidad);
                $("#goFormularioRenovaciones").hide();

                if (response.data.hasOwnProperty('eventTM')) {
                    var eventos = response.data.eventTM;
                    $.each(eventos, function(key, evento) {
                        dataLayer.push(evento);
                    });
                }

                if (response.data.siguiente_paso == 'sms') {
                    envia_sms_renovaciones(datos);
                }

            } else {

                swal.close();
                if (response.data.hasOwnProperty('errores')) {

                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesRegistro').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('siguiente_paso')) {

                    if (response.data.siguiente_paso == 'login') {
                        Swal.fire({
                            title: "Usuario Registrado",
                            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                            type: 'error',
                            showConfirmButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Aceptar',
                            customClass: 'modalError',
                            cancelButtonText: 'Cancelar',
                            allowOutsideClick: false
                        }).then((result) => {
                            if (result.value) {
                                $('#loginModal').modal('show');
                                var email = $('#email').val();
                                $('#email_login').val(email);
                            }
                       });
                    } else if (response.data.siguiente_paso == 'modal') {

                        if (response.data.stat == 'Solicitud encontrada') {
                            console.log(response.data.stat);
                            Swal.fire({
                                title: response.data.stat,
                                html: response.data.modal,
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    $('#loginModal').modal('show');
                                }
                            });

                        } else {

                            Swal.fire({
                                title: response.data.stat,
                                html: response.data.modal,
                                showConfirmButton: true,
                                confirmButtonText: 'Aceptar',
                                customClass: 'modalError',
                                allowOutsideClick: false
                            });

                        }

                   }

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Reintentar',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                   }).then((result) => {
                      if (result.value) {
                          var texto = 'Registrando usuario...';
                          Swal.fire({
                             html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                             customClass: 'modalLoading',
                             showCancelButton: false,
                             showConfirmButton:false,
                             allowOutsideClick: false
                         });
                      }
                  });

               }
            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error + ': ' + error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }

}

function envia_sms_renovaciones(datos) {

    axios.post('/solicitud/registro/sms', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'renovaciones')
            $('#info_prestamo_personal').show();
            $('#show_celular').html(response.data.celular);
            swal.close();
        } else {

            if (response.data.actualizar_celular == true) {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        var telefonoCelular = document.querySelector('#celularCorreccion');
                        telefonoCelular.addEventListener('keydown', keydownHandler);
                        telefonoCelular.addEventListener('input', inputHandler);
                    },
                    preConfirm: (e) => {
                        var validacion = validacionesCelular()
                        if (validacion == '') {
                          return true;
                        } else {
                          return false;
                        }
                    }
               }).then((result) => {
                  if (result.value) {
                      var datosCorreccion = $('#formCorreccionCelular').serializeArray();
                      datosCorreccion = getFormData(datosCorreccion);
                      datos['celular'] = datosCorreccion['celular'];
                      datos['actualizar_celular'] = 1;
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                      envia_sms_renovaciones(datos);
                  }
              });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });
}
