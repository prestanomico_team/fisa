function selectoferta(oferta) {
    $('.btn-oferta').removeClass('btn-oferta-clicked');
    $(oferta).addClass('btn-oferta-clicked');
    $('.datos_oferta').hide();
    $('.propuesta').hide();
    setTimeout(function(){
        $('#datos_' + oferta.id).show();
        $('#oferta_seleccionada').val($('#oferta_' + oferta.id).val());
        $('#oferta_id').val($('#oferta_id_' + oferta.id).val());
        $('.datos_oferta').show();
    }, 100);
}

/**
 * Envia la petición para guardar el status de la oferta, cierra el modal oferta
 * y abre el de cuestionario fijo
 */
$(document).on('click', "#aceptarOferta", function() {

    var prospecto_id = document.getElementById("prospecto_id").value;
    var producto = document.getElementById("producto").value;
    var texto = 'Guardando status de la oferta...';
    var modelo = $('#oferta_modelo').val();
    var oferta_id = $('#oferta_id').val();

    swal({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    axios.post('/solicitud/statusOferta', {
        'status_oferta': 'Oferta Aceptada',
        'clientId': clientId,
        'producto': producto,
        'prospecto_id': prospecto_id,
        'modelo'        : modelo,
        'oferta_id'     : oferta_id,
    })
    .then(function (response) {
        
        if (response.data.hasOwnProperty('eventTM')) {
            var eventos = response.data.eventTM;
            $.each(eventos, function(key, evento) {
                dataLayer.push(evento);
            });
        }
        if (response.data.carga_documentos == true && response.data.modal != null) {
            console.log(response);
            swal({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modalstatus'
            });
        } else if (response.data.carga_documentos == true) {
            
            var siguiente_paso = response.data.siguiente_paso;
            location.href = '/webapp/portal/' + siguiente_paso + '/' + prospecto_id; 
            
        } else {

            swal({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modalstatus'
            });
        }

    })
    .catch(function (error) {

        swal({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#aceptarOfertaDoble", function() {

    var tipo_oferta = $('#oferta_seleccionada').val();
    var clientId = 0;
    var prospecto_id = document.getElementById("prospecto_id").value;
    var producto = document.getElementById("producto").value;
    var modelo = $('#oferta_modelo').val();
    var oferta_id = $('#oferta_id').val();

    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    swal({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud/statusOfertaDoble', {
        'status_oferta' : 'Oferta Aceptada',
        'tipo_oferta'   : tipo_oferta,
        'clientId': clientId,
        'producto': producto,
        'prospecto_id': prospecto_id,
        'modelo'        : modelo,
        'oferta_id'     : oferta_id,
    })
    .then(function (response) {

        if (response.data.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }
            if (response.data.carga_documentos == true && response.data.modal != null) {
                console.log(response);
                swal({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            } else if (response.data.carga_documentos == true) {
                var siguiente_paso = response.data.siguiente_paso;
                location.href = '/webapp/portal/' + siguiente_paso + '/' + prospecto_id; 
                
                
            } else {

                swal({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });

            }

        } else {

            if (response.data.hasOwnProperty('reload')) {

                swal({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/#loginModal';
                        location.reload(true);
                    }
               });

           } else {
               
                swal({
                    title: "Ooops.. Surgió un problema",
                    text: error,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });

            }
        }
    })
    .catch(function (error) {

        swal({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#rechazarOferta", function() {
    $('#tituloOferta').hide();
    $('#emojiOferta').hide();
    $('#seccionOferta').hide();
    $('#botonesOferta').hide();
    $('#tituloRechazo').show();
    $('#seccionRechazo').show();
    $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }

}

$(document).on('click', "#cancelarRechazarOferta", function() {

    $('#botonesOferta').show();
    $('#seccionOferta').show();
    $('#emojiOferta').show();
    $('#tituloOferta').show();
    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');

});

$(document).on('click', "#terminaFlujo", function() {
    location.reload();
});

$(document).on('click', "#confirmaRechazarOferta", function() {

    var motivo_rechazo = $('#motivo_rechazo').val();
    var descripcion_otro = $('#textOtroRechazo').val();
    var prospecto_id = document.getElementById("prospecto_id").value;
    var modelo = $('#oferta_modelo').val();

    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    swal({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton:false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/rechazarOferta', {
        'status_oferta' : 'Oferta Rechazada',
        'modelo'            : modelo,
        'motivo' : true,
        'motivo_rechazo' : motivo_rechazo,
        'descripcion_otro' : descripcion_otro,
        'clientId': clientId,
        'prospecto_id': prospecto_id,
    })
    .then(function (response) {

        if (response.data.success == true) {
            location.reload();
        } else {
            swal({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
        }
    })
    .catch(function (error) {

        swal({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});
