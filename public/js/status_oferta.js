function selectoferta(oferta) {
    $('.btn-oferta').removeClass('btn-oferta-clicked');
    $(oferta).addClass('btn-oferta-clicked');
    $('.datos_oferta').hide();
    $('.propuesta').hide();
    setTimeout(function(){
        $('#datos_' + oferta.id).show();
        $('#oferta_seleccionada').val($('#oferta_' + oferta.id).val());
        $('#oferta_id').val($('#oferta_id_' + oferta.id).val());
        $('.datos_oferta').show();
    }, 100);
}

/**
 * Envia la petición para guardar el status de la oferta, cierra el modal oferta
 * y abre el de cuestionario fijo
 */
$(document).on('click', "#aceptarOferta", function() {

    var clientId = 0;
    var modelo = $('#oferta_modelo').val();
    var oferta_id = $('#oferta_id').val();

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    console.log($('#oferta_modelo'))
    console.log($('#oferta_id'))
    axios.post('/solicitud/status_oferta', {
        'status_oferta' : 'Oferta Aceptada',
        'modelo'        : modelo,
        'oferta_id'     : oferta_id,
        'clientId'      : clientId,
    })
    .then(function (response) {

        if (response.data.hasOwnProperty('reload')) {

            Swal.fire({
                title: "La sesión expiro",
                text: 'Inicia sesión para continuar',
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Iniciar Sesión',
                customClass: 'modalError',
                cancelButtonText: 'Cancelar',
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    location.href = '/#loginModal';
                    location.reload(true);
                }
           });

       } else {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            if (response.data.carga_documentos == true) {
                var siguiente_paso = response.data.siguiente_paso
                location.href = '/webapp/' + siguiente_paso;
            } else {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            }

        }

    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#aceptarOfertaDoble", function() {

    var tipo_oferta = $('#oferta_seleccionada').val();
    var modelo = $('#oferta_modelo').val();
    var oferta_id = $('#oferta_id').val();
    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud/status_oferta_doble', {
        'status_oferta' : 'Oferta Aceptada',
        'tipo_oferta'   : tipo_oferta,
        'modelo'        : modelo,
        'oferta_id'     : oferta_id,
        'clientId'      : clientId
    })
    .then(function (response) {

        if (response.data.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            if (response.data.carga_documentos == true) {
                var siguiente_paso = response.data.siguiente_paso
                location.href = '/webapp/' + siguiente_paso;
            } else {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });

            }

        } else {

            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/#loginModal';
                        location.reload(true);
                    }
               });

           } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.status + ': ' + response.responseText,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });

            }
        }
    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#rechazarOferta", function() {
    $('#tituloOferta').hide();
    $('#emojiOferta').hide();
    $('#seccionOferta').hide();
    $('#botonesOferta').hide();
    $('#tituloRechazo').show();
    $('#seccionRechazo').show();
    $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }

}

$(document).on('click', "#cancelarRechazarOferta", function() {

    $('#botonesOferta').show();
    $('#seccionOferta').show();
    $('#emojiOferta').show();
    $('#tituloOferta').show();
    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');

});

$(document).on('click', "#confirmaRechazarOferta", function() {

    var motivo_rechazo = $('#motivo_rechazo').val();
    var descripcion_otro = $('#textOtroRechazo').val();
    var modelo = $('#oferta_modelo').val();

    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton:false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/rechazar_oferta', {
        'status_oferta'     : 'Oferta Rechazada',
        'modelo'            : modelo,
        'motivo'            : true,
        'motivo_rechazo'    : motivo_rechazo,
        'descripcion_otro'  : descripcion_otro,
        'clientId'          : clientId
    })
    .then(function (response) {

        if (response.data.success == true) {
            window.location.href = '/';
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
        }
    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});
