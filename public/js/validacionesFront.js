function validacionesSimulador(datos) {

    var monto_minimo = parseInt($('#monto_minimo').val());
    var monto_maximo = parseInt($('#monto_maximo').val());
    var datos_faltantes = '';
    $(".error").removeClass("error");

    if ($("#plazo").val() == null) {
        datos_faltantes += '- Selecciona un Plazo. <br>';
        $("#plazo").addClass('error');
    }

    if ($("#finalidad").val() == null) {
        datos_faltantes += '- Selecciona la Finalidad de tu préstamo. <br>';
        $("#finalidad").addClass('error');
    }

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    if (prestamoValue == 0 || ( parseInt(prestamoValue) < monto_minimo || parseInt(prestamoValue) > monto_maximo)) {
        datos_faltantes += '- El Monto del préstamo debe ser entre ' + formatoMoneda(monto_minimo) + ' y ' + formatoMoneda(monto_maximo) + '. <br>';
        $("#monto_prestamo").addClass('error');
    }

    if ($("#finalidad_custom").val().length == 0 || $("#finalidad_custom").val().length < 20) {
        datos_faltantes += '- El campo Nos gustaría conocerte debe contener mínimo 20 caracteres y máximo 200. <br>';
        $("#finalidad_custom").addClass('error');
    }

    return datos_faltantes;
}

function validacionesRegistro() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formRegistro .required').each(function() {
        if (this.value == '') {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'celular':
                    campo = 'Telefono celular';
                    break;
                case 'contraseña':
                    campo = 'Contraseña';
                    break;
                case 'confirmacion_contraseña':
                    campo = 'Confirma tu contraseña';
                    break;

            }
            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'email' && (this.value != '')) {

            var email = IsEmail(this.value);
            if (email == false) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('Ingresa un correo válido');
                check_especial = true;
            }

        }

        if (this.name == 'celular' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }

        if (this.name == 'confirmacion_contraseña' && (this.value != '' && $('#contraseña').val() != '')) {
            if (this.value != $('#contraseña').val()) {
                $('#' + this.id).addClass('error');
                $('#contraseña').addClass('error');
                $('#'+this.id+'-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                $('#contraseña-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                check_especial = true;
            }
        }

        if ((this.name == 'nombres' || this.name == 'apellido_paterno') && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }

        if (this.checked == false && this.name == 'acepto_avisotyc') {
            datos_faltantes += '- Se debe autorizar los términos y condiciones. <br>';
            check_especial = true;
        }
    });



    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesCelular() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formCorreccionCelular .required').each(function() {
        if (this.name == 'celular' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo';
    }
    return datos_faltantes;
}

function validacionesRenovaciones(datos) {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formRenovaciones .required').each(function() {

        if (this.value == '') {
            campo = this.placeholder;

            if (this.name == 'fecha_nacimiento_renovaciones') {
                $('.combodate').addClass('error');
            } else {
                $('#' + this.id).addClass('error');
            }

            switch (this.name) {
                case 'celular':
                    campo = 'Telefono celular';
                    break;
                case 'contraseña':
                    campo = 'Contraseña';
                    break;
                case 'confirmacion_contraseña':
                    campo = 'Confirma tu contraseña';
                    break;
                case 'rfc_renovaciones':
                    campo = 'RFC';
                    break;

            }
            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'email' && (this.value != '')) {

            var email = IsEmail(this.value);
            if (email == false) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('Ingresa un correo válido');
                check_especial = true;
            }

        }

        if (this.name == 'celular' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }

        if (this.name == 'confirmacion_contraseña' && (this.value != '' && $('#contraseña').val() != '')) {
            if (this.value != $('#contraseña').val()) {
                $('#' + this.id).addClass('error');
                $('#contraseña').addClass('error');
                $('#'+this.id+'-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                $('#contraseña-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                check_especial = true;
            }
        }

        if ((this.name == 'nombres' || this.name == 'apellido_paterno') && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }

        if (this.name == 'fecha_nacimiento_renovaciones') {

            if ($('#day').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un dia de nacimiento. <br>';
            }
            if ($('#month').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un mes de nacimiento. <br>';
            }
            if ($('#year').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un año de nacimiento. <br>';
            }
        }

        if (this.checked == false && this.name == 'acepto_avisotyc') {
            $('#acepto_avisotyc').addClass('errorTitle');
            datos_faltantes += '- Se debe autorizar los términos y condiciones. <br>';
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDomicilio() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosDomicilio .required').each(function() {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'select_colonia':
                    campo = 'Colonia';
                    break;

            }
            if (this.name == 'select_colonia') {
                if ($('#codigo_postal').val() == '') {
                    datos_faltantes += ' - Ingresa un C.P. para selecionar una ' + campo + '. <br>';
                } else {
                    datos_faltantes += ' - Selecciona una ' + campo + '. <br>';
                }
            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }
        }

        if (this.name == 'codigo_postal' && (this.value != '' && this.value.length < 5)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El código postal de tener 5 números.');
            check_especial = true;
        }

        if (this.name == 'calle' && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('La calle debe tener al menos 2 caracteres.');
            check_especial = true;
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosPersonales() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosPersonales .required').each(function() {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            if (this.name == 'fecha_nacimiento') {
                $('.combodate').addClass('error');
            } else {
                $('#' + this.id).addClass('error');
            }

            switch (this.name) {
                case 'sexo':
                    campo = 'Sexo';
                    break;
                case 'estado_nacimiento':
                    campo = 'Estado de nacimiento';
                    break;
                case 'ciudad_nacimiento':
                    campo = 'Ciudad de nacimiento';
                    break;
                case 'telefono_casa':
                    campo = 'Telefono de casa';
                    break;
                case 'estado_civil':
                    campo = 'Estado civil';
                    break;
                case 'nivel_estudios':
                    campo = 'Nivel de estudios';
                    break;

            }

            if (this.name == 'fecha_nacimiento') {

                if ($('#day').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un dia de nacimiento. <br>';
                }
                if ($('#month').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un mes de nacimiento. <br>';
                }
                if ($('#year').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un año de nacimiento. <br>';
                }
            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }
        }


        if (this.name == 'telefono_casa' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono de casa debe ser de 10 dígitos.');
            check_especial = true;
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosBuro() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosBuro .required').each(function() {

        if (this.checked == false) {
            if (this.name == 'acepto_consulta') {
                $('#label_autorizo').addClass('errorTitle');
                datos_faltantes += '- Se debe autorizar la consulta a Buró de Crédito. <br>';
            } else {

                valor = $('input[name=' + this.name + ']:checked').val();
                if (valor == undefined) {
                    check_especial = true;
                    $('.' + this.name + '-title').addClass('errorTitle');
                    $('#' + this.name + '-help').html('Selecciona una respuesta');
                }

                if (this.name == 'credito_bancario' && valor == 'Si' && $('#digitos_tarjeta').val() == '' ) {
                    $('#digitos_tarjeta').addClass('error');
                    datos_faltantes += '- Ingresa los ultimos 4 dígitos de alguna tarjeta de crédito.  <br>';
                }

            }
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo';
    }

    return datos_faltantes;

}

function validacionesBCCorreccion() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formCorreccion .required').each(function() {

        if (this.name == 'rfc' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#' + this.id+'-help').css('color', 'red');
            $('#'+this.id+'-help').html('Deben ser 10 caracteres.');
            check_especial = true;
        }

        if (this.name == 'homoclave' && (this.value != '' && this.value.length < 3)) {
            $('#' + this.id).addClass('error');
            $('#' + this.id+'-help').css('color', 'red');
            $('#'+this.id+'-help').html('Deben ser 2 caracteres.');
            check_especial = true;
        }

        if (this.name == 'fecha_nacimiento') {

            if ($('#formCorreccion #day').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un dia de nacimiento. <br>';
                $('#formCorreccion #day').addClass('error');
            }
            if ($('#formCorreccion #month').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un mes de nacimiento. <br>';
                $('#formCorreccion #month').addClass('error');
            }
            if ($('#formCorreccion #year').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un año de nacimiento. <br>';
                $('#formCorreccion #year').addClass('error');
            }
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo';
    }
    return datos_faltantes;
}

function validacionesDatosIngreso() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosIngreso .required').each(function() {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'ocupacion':
                    campo = 'Ocupacion';
                    break;
                case 'fuente_ingresos':
                    campo = 'Fuente de Ingresos';
                    break;
                case 'ingreso_mensual':
                    campo = 'Ingreso mensual comprobable';
                    break;
                case 'residencia':
                    campo = 'Residencia';
                    break;
                case 'empresa':
                    campo = 'Nombre de empresa/lugar de trabajo';
                    break;
                case 'antiguedad_empleo':
                    campo = 'Antigüedad en empleo';
                    break;
                case 'antiguedad_domicilio':
                    campo = 'Antigüedad en domicilio';
                    break;
                case 'numero_dependientes':
                    campo = 'Dependientes económicos';
                    break;
                case 'gastos_familiares':
                    campo = 'Monto gastos familiares mensuales';
                    break;

            }

            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'ingreso_mensual' && this.value != '') {
            valor = $("#ingreso_mensual").maskMoney('unmasked')[0];
            valor = valor * 1000;
            if (valor < 1000) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El ingreso minimo es de $ 1,000.00');
                check_especial = true;
            }
        }

        if (this.name == 'gastos_mensuales' && this.value != '') {
            valor = $("#gastos_mensuales").maskMoney('unmasked')[0];
            valor = valor * 1000;
            if (valor <= 0) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El montomo debe ser mayor a $ 0');
                check_especial = true;
            }
        }

        if (this.name == 'telefono_empleo' && this.value != '') {
            if (this.value.length < 10) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El teléfono del empleo debe ser de 10 dígitos');
                check_especial = true;
            }
        }

        if (this.name == 'empresa' && this.value != '') {
            if (this.value.length < 5) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El nombre de la empresa/lugar de trabajo debe tener mínimo 5 caracteres');
                check_especial = true;
            }
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

function validacionesDatosEmpleo() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosEmpleo .required').each(function() {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            if (this.name == 'fecha_ingreso') {
                $('.combodate').addClass('error');
            } else {
                $('#' + this.id).addClass('error');
            }

            switch (this.name) {
                case 'empresa':
                    campo = 'Nombre de empresa/lugar de trabajo';
                    break;
                case 'antiguedad_empleo':
                    campo = 'Antigüedad en empleo';
                    break;
                case 'select_colonia_empleo':
                    campo = 'Colonia';
                    break;
            }

            if (this.name == 'select_colonia_empleo') {
                if ($('#codigo_postal_empleo').val() == '') {
                    datos_faltantes += ' - Ingresa un C.P. para selecionar una ' + campo + '. <br>';
                } else {
                    datos_faltantes += ' - Selecciona una ' + campo + '. <br>';
                }
            }

            if (this.name == 'fecha_ingreso') {

                if ($('#formDatosEmpleo #day').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un dia de ingreso. <br>';
                }
                if ($('#formDatosEmpleo #month').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un mes de ingreso. <br>';
                }
                if ($('#formDatosEmpleo #year').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un año de ingreso. <br>';
                }

            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }


        }



        if (this.name == 'calle_empleo' && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('La calle debe tener al menos 2 caracteres.');
            check_especial = true;
        }

        if (this.name == 'codigo_postal_empleo' && (this.value != '' && this.value.length < 5)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El código postal de tener 5 números.');
            check_especial = true;
        }

        if (this.name == 'telefono_empleo' && this.value != '') {
            if (this.value.length < 10) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El teléfono del empleo debe ser de 10 dígitos');
                check_especial = true;
            }
        }

        if (this.name == 'empresa' && this.value != '') {
            if (this.value.length < 5) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El nombre de la empresa/lugar de trabajo debe tener mínimo 5 caracteres');
                check_especial = true;
            }
        }

    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

function validacionesNuevaContraseña() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#new_password').on('keyup', function() {
        var pass = IsValidPwd(this.value);
    });

    $('#restaurarPassword .required').each(function() {
        if (this.value == '') {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'new_password':
                    campo = 'Contraseña';
                    break;
                case 'repetir_new_password':
                    campo = 'Confirma tu contraseña';
                    break;

            }
            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'repetir_new_password' && (this.value != '' && $('#new_password').val() != '')) {
            if (this.value != $('#new_password').val()) {
                $('#' + this.id).addClass('error');
                $('#new_password').addClass('error');
                $('#'+this.id+'-help').html('El campo nueva contraseña y confirmación de contraseña no son iguales');
                $('#contraseña-help').html('El campo nueva contraseña y confirmación de contraseña no son iguales');
                check_especial = true;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

var formatoMoneda = function(input) {
	return '$' + numeroComas(input);
}

var numeroComas = function(n){
	var parts = n.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

$(':input').on('focus', function() {

    $(this).removeClass('error');
    $('#' + this.id + '-help').html('');

    if (this.name == 'credito_hipotecario' || this.name == 'credito_automotriz' || this.name == 'credito_bancario') {
        $('#' + this.name + '-help').html('');
        $('.' + this.name + '-title').removeClass('errorTitle');
    }

    if (this.name == 'acepto_consulta') {
        $('#label_autorizo').removeClass('errorTitle');
    }

});

$('#contraseña').on('keyup', function() {
   var pass = IsValidPwd(this.value);
});

$('#new_password').on('keyup', function() {
    var pass = IsValidPwdAct(this.value);
});

$('#password_login').on('blur', function() {
    if (this.value != '') {
        var pass = IsValidPwdLogin(this.value);
        if (pass == false) {
           $('#' + this.id).addClass('error');
           $('#'+this.id+'-help').html('La contraseña no cumple con el formato: <br> Al menos 1 letra mayúscula, 1 letra minúscula, 1 número y al menos 8 carateres.');
           $("#buttonLogin").prop("disabled", true);
        } else {
           $("#buttonLogin").prop("disabled", false);
        }
    }
});

$('input[name=credito_hipotecario]').on('change', function() {
   $('#' + this.name + '_descripcion_Si').hide();
   $('#' + this.name + '_descripcion_No').hide();
   $('#' + this.name + '_descripcion_' + this.value).show();
});

$('input[name=credito_automotriz]').on('change', function() {
   $('#' + this.name + '_descripcion_Si').hide();
   $('#' + this.name + '_descripcion_No').hide();
   $('#' + this.name + '_descripcion_' + this.value).show();
});

$('input[name=credito_bancario]').on('change', function() {
   $('#' + this.name + '_descripcion_Si').hide();
   $('#' + this.name + '_descripcion_No').hide();
   $('#' + this.name + '_descripcion_' + this.value).show();

   $('#digitos_tarjeta').val('');
   $('#descripcion_digitos').hide();
   if (this.value == 'Si') {
       $('#div_digitos_tarjeta').show();
   } else {
       $('#div_digitos_tarjeta').hide();
   }

});

$('#digitos_tarjeta').on('change', function() {
   $('#descripcion_digitos').show();
});

$('#email').on('change', function() {
   var email = IsEmail(this.value);
   if (email == false) {
       $('#' + this.id).addClass('error');
       $('#'+this.id+'-help').html('Ingresa un correo válido');
   }
});

$('#email_login').on('blur', function() {
    if (this.value != '') {
        var email = IsEmail(this.value);
        if (email == false) {
           $('#' + this.id).addClass('error');
           $('#'+this.id+'-help').html('Ingresa un correo válido');
           $("#buttonLogin").prop("disabled", true);
        } else {
           $("#buttonLogin").prop("disabled", false);
        }
    }
});

$(document).on('click', "#terminaFlujo", function() {
    location.href = '/'
});

function IsEmail(valor) {
    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/.test(valor)
}

/**
 * Verifica que la contraseña que ingresa el prospecto cumpla con la politica
 * de contraseñas
 *
 * @return {boolean} Resultado de la validación de la contraseña
 */
function IsValidPwd(valor) {

    $('.helperPassword').show();
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if (valor.length >= 8) {
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
        valid_len = true;
	} else {
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}

	if (ucase.test(valor)) {
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
        valid_ucase = true;
	} else {
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}

	if (lcase.test(valor)) {
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
        valid_lcase = true;
	} else {
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}

	if (num.test(valor)) {
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
        valid_num = true;
	} else {
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}

function IsValidPwdAct(valor) {

    $('.helper').show();
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if (valor.length >= 8) {
		$("#char").removeClass("glyphicon-remove");
		$("#char").addClass("glyphicon-ok");
		$("#char").css("color","#00A41E");
        valid_len = true;
	} else {
		$("#char").removeClass("glyphicon-ok");
		$("#char").addClass("glyphicon-remove");
		$("#char").css("color","#FF0004");
	}

	if (ucase.test(valor)) {
		$("#Ucase").removeClass("glyphicon-remove");
		$("#Ucase").addClass("glyphicon-ok");
		$("#Ucase").css("color","#00A41E");
        valid_ucase = true;
	} else {
		$("#Ucase").removeClass("glyphicon-ok");
		$("#Ucase").addClass("glyphicon-remove");
		$("#Ucase").css("color","#FF0004");
	}

	if (lcase.test(valor)) {
		$("#Lcase").removeClass("glyphicon-remove");
		$("#Lcase").addClass("glyphicon-ok");
		$("#Lcase").css("color","#00A41E");
        valid_lcase = true;
	} else {
		$("#Lcase").removeClass("glyphicon-ok");
		$("#Lcase").addClass("glyphicon-remove");
		$("#Lcase").css("color","#FF0004");
	}

	if (num.test(valor)) {
		$("#Num").removeClass("glyphicon-remove");
		$("#Num").addClass("glyphicon-ok");
		$("#Num").css("color","#00A41E");
        valid_num = true;
	} else {
		$("#Num").removeClass("glyphicon-ok");
		$("#Num").addClass("glyphicon-remove");
		$("#Num").css("color","#FF0004");
	}

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}

function IsValidPwdLogin(valor) {

    $('.helperPassword').show();
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if (valor.length >= 8) {
        valid_len = true;
	}

	if (ucase.test(valor)) {
        valid_ucase = true;
	}

	if (lcase.test(valor)) {
        valid_lcase = true;
	}

	if (num.test(valor)) {
        valid_num = true;
	}
    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}


var oldCursor, oldValue = '';
regex = new RegExp(/^\d{0,10}$/g);
unmask = function(value) {
  var output = value.replace(new RegExp(/[^\d]/, 'g'), '');
  return output;
}
keydownHandler = function(e) {
  var el = e.target;
  oldValue = el.value;
  oldCursor = el.selectionEnd;
}
inputHandler = function(e) {
    var el = e.target,
    newCursorPosition,
    newValue = unmask(el.value);

    if(newValue.match(regex)) {
      el.value = newValue;
    } else {
      el.value = oldValue;
    }
}

var codigoPostal = document.querySelector('#codigo_postal');
codigoPostal.addEventListener('keydown', keydownHandler);
codigoPostal.addEventListener('input', inputHandler);

var telefonoCasa = document.querySelector('#telefono_casa');
telefonoCasa.addEventListener('keydown', keydownHandler);
telefonoCasa.addEventListener('input', inputHandler);

if($('#digitos_tarjeta').length) {
    var digitosTDC = document.querySelector('#digitos_tarjeta');
    digitosTDC.addEventListener('keydown', keydownHandler);
    digitosTDC.addEventListener('input', inputHandler);
}

var telefonoEmpleo = document.querySelector('#telefono_empleo');
telefonoEmpleo.addEventListener('keydown', keydownHandler);
telefonoEmpleo.addEventListener('input', inputHandler);

var telefonoCelular = document.querySelector('#celular');
telefonoCelular.addEventListener('keydown', keydownHandler);
telefonoCelular.addEventListener('input', inputHandler);

var telefonoCelular = document.querySelector('#celular');
telefonoCelular.addEventListener('keydown', keydownHandler);
telefonoCelular.addEventListener('input', inputHandler);

var codigoVerificacion = document.querySelector('#conf_code');
codigoVerificacion.addEventListener('keydown', keydownHandler);
codigoVerificacion.addEventListener('input', inputHandler);

$('#monto_prestamo').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#ingreso_mensual').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#gastos_mensuales').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

var interval;
function makeTimer(endTime) {

    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;
    if (timeLeft <= 0) {
        stopinterval();
    }

    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $('#timerResend').html(minutes + ':' + seconds);
}

function playinterval() {
    $('#timerResend').show();
    $('#reenviarSMS').removeAttr('onclick', 'reenviar_sms();');
    $('#reenviarSMS').addClass('disabled');
    var newTime = new Date();
    newTime = new Date(newTime.getTime() + 5 * 60000);
    interval = setInterval(function() { makeTimer(newTime); }, 1000);
    return false;
}

function stopinterval() {
    $('#timerResend').hide();
    $('#reenviarSMS').attr('onclick', 'reenviar_sms();');
    $('#reenviarSMS').removeClass('disabled');
    clearInterval(interval);
    return false;
}

/**
 * Valida que el RFC capturado sea correcto en cuanto a estructura
 *
 * @return {boolean} Resultado de la validación del CURP
 */
function validateRFC() {

    var year = $('.year').val();
    var month = parseInt($('.month').val());
    var day = parseInt($('.day').val());
    var rfc = $('#rfc').val();
    var response = [];
    console.log(year);
    console.log(month);
    console.log(day);
    console.log(rfc);
    if (year != '' && month != '' && day != '' && rfc != '') {

        // Validando la expresión regular del RFC
        // 4 Letras, 2 digitos año de nacimiento, 2 digitos mes de nacimiento
        // 2 digitos dia de nacimiento y si se captura la homoclave 2 primeros
        // digos cualquier letra o número y el ultimo cualquier número o letra A (0-9A)
        var matches = rfc.match(/^[A-Z]{4}([0-9]{2})([0-1][0-9])([0-3][0-9])(?:[A-Z0-9]{2}[A-Z0-9])?$/i);

        if (!matches) {
            response['success'] = false;
            response['msj'] = 'La estructura del RFC es inválida';
            return response;
        }

        // Validando fecha de nacimiento
        year_m = rfc.substring(4, 6);
        year_c = year.substring(2, 4);
        month_m = parseInt(rfc.substring(6, 8)) - 1;
        day_m = parseInt(rfc.substring(8, 10));

        if (year_c == year_m && month == month_m && day == day_m) {
            var date = new Date(year_m, month_m, day_m);

            if (date.getFullYear().toString().substr(2, 2) == year_m && date.getMonth() == month_m && date.getDate() == day_m) {
                response['success'] = true;
                response['msj'] = 'RFC correcto';
                return response;
            } else {
                response['success'] = false;
                response['msj'] = 'La fecha de nacimiento en el RFC es inválida';
                return response;
            }

        } else {
            response['success'] = false;
            response['msj'] = 'La fecha de nacimiento en el RFC no coincide con la capturada';
            return response;
        }
    } else {
        response['success'] = true;
        return response;
    }
}
