@extends('crm.app')
@section('content')
<link href="/css/font-awesome.css" rel="stylesheet" >
<link href="/css/bootstrap-social.css" rel="stylesheet" >
<style>
    label {
        text-transform: none;
        font-weight: normal;
    }
</style>
<div id="login-panel" class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Login Panel</h3>
    </div>
    <div class="panel-body">
        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}

            <div>
                <h5><b>Usuarios</b></h5>
                <a class="btn btn-block btn-social btn-microsoft" style="text-align:center" href="/login/live">
                    <span class="fab fa-microsoft" style="width: 52px"></span>
                    Iniciar sesión con Microsoft Office 365
                </a>
            </div>
            <hr>
            <h5><b>Solo para administrador</b></h5>
            <div class="form-group">
                <label for="email">Correo Electrónico</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="new-email">
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" id="password" name="password" class="form-control" id="password" autocomplete="new-password">
            </div>
            <center><button class="btn btn-default" type="submit">Iniciar sesión</button></center>
            <hr>

        </form>
    </div>
</div>
@endsection
