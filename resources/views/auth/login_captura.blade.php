@extends('crm.app')
@section('content')
<title>Prestanómico | Portal de Captura </title>
<style>
    label {
        text-transform: none;
        font-weight: normal;
    }
    .alert-danger {
        color: #a94442 !important;
    }
</style>
<div id="login-panel" class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Login Captura Solicitud</h3>
    </div>
    <div class="panel-body">
        <form method="POST" id="login_captura">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="email">Correo Electrónico</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="new-email">
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" id="password" name="password" class="form-control">
                <input type="hidden" id="producto" name="producto" class="form-control" value="{{ $producto }}">
            </div>
            @if(env('GOOGLE_RECAPTCHA_KEY'))
            <center>
                <div class="g-recaptcha"
                    data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                </div>
                <div class="alert alert-danger" style="margin-bottom: 0px; display:none" id="captcha_errores"><label id="captcha_errores"></label></div>
            </center>
            @endif
            <br >
            <center><button class="btn btn-default" type="button" onclick="loginCaptura()">Iniciar sesión</button></center>
            <hr>
            <div class="alert alert-danger" style="margin-bottom: 0px; display:none" id="alert_errores"><label id="login_errores"></label></div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="/back/js/sweetalert2.all.min.js"></script>
<script>
function loginCaptura() {

    var producto = $('#producto').val();
    $('#alert_errores').hide();
    $('#login_errores').html('');

    $.ajax({
        url:'/captura_solicitud/' + producto,
        data: new FormData($("#login_captura")[0]),
        dataType:'json',
        async:false,
        type:'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        processData: false,
        contentType: false,
        success: function(response) {

            if (response.success == true) {
                location.href = response.redirect;
            } else if (response.captcha == false) {
                $('#captcha_errores').show();
                $.each(response.errores, function(key, error) {
                    console.log(key);
                    $('#captcha_errores').html(error);
                });
            } else if (response.oportunidades > 1) {
                    $('#alert_errores').show();
                    $('#login_errores').html(response.message + '<br>' + response.oportunidades + ' intentos restantes.');
                } else {
                    $('#login_errores').html(response.message + '<br>' + response.oportunidades + ' intento restante.');
                }

           // }

        }, error: function(response) {
            if (response.status == 423) {
                $('#alert_errores').show();
                if (response.hasOwnProperty("responseJSON")) {
                    
                    $.each(response.responseJSON, function(error, value) {
                        $('#login_errores').html(value);
                    });
                    
                }
            }
        }
    });

}
</script>
@endsection
