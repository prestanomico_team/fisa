@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
@if(Auth::user()->can('alta-automatica'))
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="col-xs-12">
						<center>
							<h4 class="box-title">
								Alta de Clientes T24
							</h4>
						</center>
					</div>
					<br><br>
					<div class="clearfix col-xs-12" style="font-size: 12px;">
						<div class="col-xs-6">
							<label>Cargar Layout</label>
							<form method="post" enctype="multipart/form-data" id="upload_form">
							    <input type="file" name="file" id="fileToUpload" style="padding-bottom:5px;">
							    <input type="button" value="Enviar" name="submit" id='enviar_layout'>
								<input type="hidden" name="_token" value="{{ csrf_token()}}">
							</form>
						</div>
						<div class="col-xs-3">
							<label>Cargar Layout Renovaciones</label>
							<form method="post" enctype="multipart/form-data" id="upload_form_renovaciones">
							    <input type="file" name="file" id="fileToUpload" style="padding-bottom:5px;">
							    <input type="button" value="Enviar" name="submit" id='enviar_layout_renovaciones'>
								<input type="hidden" name="_token" value="{{ csrf_token()}}">
							</form>
						</div>
						<div class="col-xs-6" style="text-align: -webkit-right; float: right; width: auto;">
							<form method="get">
								<div class="input-group input-group-sm" style="width: 450px;">
									@php
										$busqueda = '';
									@endphp
									@if(isset($search))
										@php
											$busqueda = $search;
										@endphp
									@endif
									<input type="text" name="q" id="q" class="form-control pull-right" placeholder="Buscar" value="{{ $busqueda }}">
									<div class="input-group-btn">
										<label class="btn btn-default"><i class="fa fa-search"></i></label>
										<label class="btn btn-default" onclick="limipar_busqueda()"><i class="fa fa-remove"></i></label>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="box-body" style="font-size: 12px;">
					<div class="col-sm-12">
						<div class="box-body" id="lista">
							@include('crm.lista_clientes', ['altas' => $altas])
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="/back/js/sweetalert2.all.min.js"></script>
<script>
$("#enviar_layout").click(function() {
	swal({
		title: "Cargando Layout",
		text: "Espere por favor",
		showConfirmButton:false,
		onOpen: function () {
			swal.showLoading()
		}
	});

	setTimeout(function() {
		$.ajax({
			url:'carga-layout',
			data: new FormData($("#upload_form")[0]),
			dataType:'json',
			async:false,
			type:'post',
			processData: false,
			contentType: false,
			success: function(response) {
				if (response.success == "true") {
					swal.close();
					location.reload();
				}

			},
		});
	}, 3000);

});
$("#enviar_layout_renovaciones").click(function() {
	swal({
		title: "Cargando Layout",
		text: "Espere por favor",
		showConfirmButton:false,
		onOpen: function () {
			swal.showLoading()
		}
	});

	setTimeout(function() {
		$.ajax({
			url:'/renovaciones/cargaLayout',
			data: new FormData($("#upload_form_renovaciones")[0]),
			dataType:'json',
			async:false,
			type:'post',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			processData: false,
			contentType: false,
			success: function(response) {
				if (response.success == "true") {
					swal.close();
					location.reload();
				}

			},
		});
	}, 3000);

});
$(function()
{
	$('table').on('click', 'tr.parent .fa-chevron-down', function() {
		$(this).closest('tbody').toggleClass('open');
	});
	$( "#q" ).autocomplete({
		source: function (request, response) {
			$.ajax({
				type: "GET",
				url: "search/autocomplete",
				dataType: "json",
				data: {
					term: request.term
				},
				error: function (xhr, textStatus, errorThrown) {
					alert('Error: ' + xhr.responseText);
				},
				success: function (data) {
					var res = data.content;
					$('#lista').html(data.view);
					$('table').on('click', 'tr.parent .fa-chevron-down', function() {
						$(this).closest('tbody').toggleClass('open');
					});
					response($.map(res, function (item) {
						return {
							label: item.label,
							value: item.value
						}
					}));
				}
			});
		},
		minLength: 2,
		select: function(event, ui) {
			$('#q').val(ui.item.value);
		},
		response: function( event, ui) {

		}
	});
});

function limipar_busqueda() {
	var url = document.URL;
    var shortUrl = url.substring(0, url.lastIndexOf("?"));
	window.location.href = shortUrl;
}

function altaCliente(idAltaCliente) {

	$.ajax({

		url:'/alta-cliente-t24',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		beforeSend: function() {
			swal({
				title: "Generando alta de cliente",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function altaSolicitud(idAltaCliente) {

	$.ajax({

		url:'/alta-solicitud-t24',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		beforeSend: function() {
			swal({
				title: "Generando alta de solicitud",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function ligarUsuario(idAltaCliente) {

	$.ajax({

		url:'/ligar-usuario-ldap',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		beforeSend: function() {
			swal({
				title: "Generando ligue de usuario",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function enviarEmail(idAltaCliente) {

	$.ajax({

		url:'/enviar-email',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		beforeSend: function() {
			swal({
				title: "Generando envío de email",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarFacematch(idAltaCliente) {

	$.ajax({

		url:'/webapp/facematch/procesaFaceMatch',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando FaceMatch",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarReferencias(idAltaCliente) {

	$.ajax({

		url:'/webapp/referencias/procesar',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando Referencias",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarCuentaClabe(idAltaCliente) {

	$.ajax({

		url:'/webapp/cuenta_clabe/procesar',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando Referencias",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarCuentaClabe(idAltaCliente) {

	$.ajax({

		url:'/webapp/cuenta_clabe/procesar',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando Cuenta Clabe",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarComprobanteDomicilio(idAltaCliente) {

	$.ajax({

		url:'/webapp/comprobante_domicilio/procesar',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando Comprobante Domicilio",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarComprobanteIngresos(idAltaCliente) {

	$.ajax({

		url:'/webapp/comprobante_ingresos/procesar',
		data: { idAltaCliente: idAltaCliente },
		dataType:'json',
		type:'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		beforeSend: function() {
			swal({
				title: "Reprocesando Comprobante Domicilio",
				text: "Espere por favor",
				showConfirmButton:false,
				onOpen: function () {
					swal.showLoading()
				}
			});
	    },
		success: function(response) {
			swal.close();
		},
	});

}

function reprocesarCertificadosDeuda(idAltaCliente) {

$.ajax({

	url:'/webapp/certificados_deuda/procesar',
	data: { idAltaCliente: idAltaCliente },
	dataType:'json',
	type:'post',
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	beforeSend: function() {
		swal({
			title: "Reprocesando Certificados de deuda",
			text: "Espere por favor",
			showConfirmButton:false,
			onOpen: function () {
				swal.showLoading()
			}
		});
	},
	success: function(response) {
		swal.close();
	},
});

}

setInterval(function() {
	$.ajax({
		url:'alta-clientes/update_lista',
		dataType:'json',
		type:'get',
		processData: false,
		contentType: false,
		success: function(response) {
			$('#lista').html(response.view);
			$('table').on('click', 'tr.parent .fa-chevron-down', function() {
				$(this).closest('tbody').toggleClass('open');
			});
		},
	});
}, 120000);
</script>
@else
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
						<center>
							<h4 class="box-title">
								Alta de Clientes T24
							</h4>
						</center>
						<hr>
						<center>
							<br>
							<h4>No tienes privilegios para realizar esta acción</h4>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endsection
