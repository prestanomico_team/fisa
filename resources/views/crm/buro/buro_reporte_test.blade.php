@extends('crm.app')
@section('content')
		<div class="container" ng-controller="MainController as main">
			<div class="content">
				<header>
					<div class="title">
						<h2>Generador de Consulta a Buró de Crédito (<strong class="kamikaze-brand">REPORTE COMPLETO</strong>)</lab></h2>
					</div>
				</header>
				<form id="gen_report_request_form" method="POST">
					@include('crm.buro.segments_reporte.seg_encabezado')
					@include('crm.buro.segments_reporte.seg_nombre')
					@include('crm.buro.segments_reporte.seg_direccion')
					@include('crm.buro.segments_reporte.seg_empleo')
					@include('crm.buro.segments_reporte.seg_referencias')
					@include('crm.buro.segments_reporte.seg_cierre')
				</form>
			</div>
		</div>
@endsection