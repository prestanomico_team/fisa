<!-- ================================================================================== -->
<!-- ================================= SEGMENTO AUTENTICACION ============================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title"><strong>Segmento de Autenticación - AU</strong> <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_aut">?</span></h2>
		<div id="desc_aut" class="collapse">
			<p>El segmento contiene las referencias crediticias necesarias para autenticar a la persona contra la información reportada a la base de datos de Buró de Crédito.</p>
			<p>Es un segmento requerido compuesto por siete etiquetas.</p>
		</div>
		<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-aut">Formulario</button>
	</div>
	<div id="segmento-aut" class="panel-body collapse">
		<section id="aut_preview">
			Preview: 
			<ul id="segmento_tipo_reporte" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta_auth_tipo_reporte">AU</li>
				<li class="seg-count" id="seg-len_auth_tipo_reporte">3</li>
				<li id="seg-auth_tipo_reporte">RCN</li>

				<li class="seg-label" id="seg-etiqueta_auth_tipo_salida">00</li>
				<li class="seg-count" id="seg-len_auth_tipo_salida">1</li>
				<li id="seg-auth_tipo_salida">1</li>

				<li class="seg-label" id="seg-etiqueta_auth_ref_operador">01</li>
				<li class="seg-count" id="seg-len_auth_ref_operador">25</li>
				<li id="seg-auth_ref_operador">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
				
				<li class="seg-label" id="seg-etiqueta_auth_cuenta_con_tarjeta">02</li>
				<li class="seg-count" id="seg-len_auth_cuenta_con_tarjeta"><% segs.auth.cuenta_con_tarjeta | stringLength %></li>
				<li id="seg-auth_cuenta_con_tarjeta"><% segs.auth.cuenta_con_tarjeta %></li>

				<li class="seg-label" id="seg-etiqueta_auth_num_tarjeta">04</li>
				<li class="seg-count" id="seg-len_auth_num_tarjeta"><% segs.auth.num_tarjeta | stringLength %></li>
				<li id="seg-auth_num_tarjeta"><% segs.auth.num_tarjeta %></li>

				<li class="seg-label" id="seg-etiqueta_auth_cred_hipotecario">07</li>
				<li class="seg-count" id="seg-len_auth_cred_hipotecario"><% segs.auth.cred_hipotecario | stringLength %></li>
				<li id="seg-auth_cred_hipotecario"><% segs.auth.cred_hipotecario %></li>

				<li class="seg-label" id="seg-etiqueta_auth_cred_automotriz">11</li>
				<li class="seg-count" id="seg-len_auth_cred_automotriz"><% segs.auth.cred_automotriz | stringLength %></li>
				<li id="seg-auth_cred_automotriz"><% segs.auth.cred_automotriz %></li>
				
			</ul>
			<hr>
		</section>
		<section id="aut_settings">
			<div class="form-group">
				<label for="auth_tipo_reporte">AU - TIPO DE REPORTE <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_tipo_reporte">?</span></label>
				<div id="tip_auth_tipo_reporte" class="collapse">
					<p>Debe capturarse la identificación RCN.</p>
					<strong>De no incluirse el dato o ingresar un valor diferente, se rechazará la solicitud de consulta.</strong>
				</div>
				<input type="text" id="auth_tipo_reporte" name="auth_tipo_reporte" class="form-control" value="RCN" disabled>
				<small><strong class="req-label">Requerido</strong> | Longitud Fija | 3 caracteres | Alfabético </small>
			</div>

			<div class="form-group">
				<label for="auth_tipo_salida">00 - TIPO DE SALIDA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_tipo_salida">?</span></label>
				<div id="tip_auth_tipo_salida" class="collapse">
					<p>Para consultar la combinación Autenticador y Prospector, el código de tipo de salida debe ser siempre “1”.</p>
					<strong>De no incluirse el dato o ingresar un valor diferente, se rechazará la solicitud de consulta.</strong>
				</div>
				<input type="text" id="auth_tipo_salida" name="auth_tipo_salida" class="form-control" value="1" disabled>
				<small><strong class="req-label">Requerido</strong> | Longitud Fija | 1 caracteres | Numérico </small>
			</div>

			<div class="form-group">
				<label for="auth_ref_operador">01 - REFERENCIA DEL OPERADOR <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_ref_operador">?</span></label>
				<div id="tip_auth_ref_operador" class="collapse">
					<p>Este campo es de uso exclusivo para el Usuario. Sirve para ingresar un dato de identificación propio del Usuario para referenciar la consulta del Cliente; el dato se presentará en la respuesta.</p>
					<p>Consideraciones:</p>
					<ul>
						<li>El campo es REQUERIDO. En caso de NO usar la referencia, SÍ debe incluirse la etiqueta en el archivo de consulta y debe enviarse con 25 espacios en blanco.</li>
						<li>Si la referencia ingresada es menor a 25 caracteres, deben incluirse espacios a la izquierda hasta cumplir la longitud máxima.</li>
					</ul>
					<strong>Si se omite la etiqueta, se rechaza la solicitud de consulta.</strong>
				</div>
				<input type="text" id="auth_ref_operador" name="auth_ref_operador" class="form-control" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" disabled>
				<small><strong class="req-label">Requerido</strong> | Longitud Fija | 25 caracteres | Alfanumérico </small>
			</div>

			<div class="form-group">
				<label for="auth_cuenta_con_tarjeta">02 - CUENTA CON TARJETA DE CRÉDITO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_cuenta_con_tarjeta">?</span></label>
				<div id="tip_auth_cuenta_con_tarjeta" class="collapse">
					<p>Los valores permitidos son:</p>
					<p>V = Verdadero</p>
					<p>F = Falso</p>
					<p>Consideraciones:</p>
					<ul>
						<li>La etiqueta es requerida.</li>
						<li>Se deben considerar tarjetas de crédito de tipo revolvente.</li>
						<li>No son válidas referencias a tarjetas de servicio, adicionales, de débito o de nómina.</li>
					</ul>
					<strong>Cuando no se incluye un dato válido, el sistema considerará el valor “F” = falso.</strong>
				</div>
				<select id="auth_cuenta_con_tarjeta" name="auth_cuenta_con_tarjeta" ng-model="segs.auth.cuenta_con_tarjeta" class="form-control">
					<option></option>
					<option value="V">V: Verdadero</option>
					<option value="F">F: Falso</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Longitud Fija | 1 caracter | Alfabético </small>
			</div>

			<div class="form-group">
				<label for="auth_num_tarjeta">04 - ÚLTIMOS CUATRO DÍGITOS DE LA TARJETA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_num_tarjeta">?</span></label>
				<div id="tip_auth_num_tarjeta" class="collapse">
					<p>Se deben ingresar los últimos 4 dígitos correspondientes a la tarjeta de crédito.</p>
					<p>Consideraciones:</p>
					<ul>
						<li>El campo está relacionado con la etiqueta 02 – CUENTA CON TARJETA DE CRÉDITO.</li>
						<li>Cuando en la etiqueta 02 se ingresa el valor V (verdadero), esta etiqueta se vuelve requerida y, por consecuencia, deben enviarse los cuatro últimos números de la tarjeta de crédito.</li>
					</ul>
					<strong>Si se omite la etiqueta, se rechaza la solicitud de consulta.</strong>
				</div>
				<input type="text" id="auth_num_tarjeta" name="auth_num_tarjeta" class="form-control" ng-model="segs.auth.num_tarjeta" maxlength="4">
				<small><strong class="req-label">Condicional</strong> | Longitud Variable | 4 caracteres max | Alfanumérico </small>
			</div>

			<div class="form-group">
				<label for="auth_cred_hipotecario">07 - HA EJERCIDO UN CRÉDITO HIPOTECARIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_cred_hipotecario">?</span></label>
				<div id="tip_auth_cred_hipotecario" class="collapse">
					<p>Los valores permitidos son:</p>
					<p>V = Verdadero</p>
					<p>F = Falso</p>
					<p>Consideraciones:</p>
					<ul>
						<li>La etiqueta es requerida.</li>
						<li>Se debe ingresar el valor V (verdadero) cuando la persona a autenticar tiene contratado un crédito de tipo hipotecario y lo está pagando al momento de la consulta.</li>
					</ul>
					<strong>Cuando no se incluye un dato válido, el sistema considerará el valor “F” = falso.</strong>
				</div>
				<select id="auth_cred_hipotecario" name="auth_cred_hipotecario" class="form-control" ng-model="segs.auth.cred_hipotecario">
					<option></option>
					<option value="V">Verdadero</option>
					<option value="F">Falso</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Longitud Variable | 4 caracteres max | Alfanumérico </small>
			</div>

			<div class="form-group">
				<label for="auth_cred_automotriz">11 - HA EJERCIDO UN CRÉDITO AUTOMOTRIZ EN LOS ÚLTIMOS 24 MESES <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_auth_cred_automotriz">?</span></label>
				<div id="tip_auth_cred_automotriz" class="collapse">
					<p>Los valores permitidos son:</p>
					<p>V = Verdadero</p>
					<p>F = Falso</p>
					<p>Consideraciones:</p>
					<ul>
						<li>La etiqueta es requerida.</li>
						<li>Se debe ingresar el valor V (verdadero) cuando la persona a autenticar tiene contratado o ha tendido, un crédito de tipo automotriz; por lo tanto lo está pagando al momento de la consulta o se encuentra liquidado.</li>
					</ul>
					<strong>Cuando no se incluye un dato válido, el sistema considerará el valor “F” = falso.</strong>
				</div>
				<select id="auth_cred_automotriz" name="auth_cred_automotriz" class="form-control" ng-model="segs.auth.cred_automotriz">
					<option></option>
					<option value="V">Verdadero</option>
					<option value="F">Falso</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Longitud Fija | 1 caracter | Alfanumérico </small>
			</div>

		</section>
	</div>
</div>