<!-- ================================================================================== -->
<!-- ================================= SEGMENTO EMPLEO ================================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Empleo del Cliente - PE</strong>  <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_empleo">?</span></h2>
  	<div id="desc_empleo" class="collapse">
  		<p>Contiene los datos del empleo del Cliente.</p>
  		<p>Es un segmento opcional compuesto por 19 etiquetas.</p>
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-empleo">Formulario</button>
  </div>
  <div id="segmento-empleo" class="panel-body collapse">
  	<section id="empleo_preview">
	  	Preview: 
			<ul id="segmento_empleo" class="preview_segmento_list">
				<span ng-show="segs.emp.nombre_razon_social">
					<li class="seg-label" id="seg-etiqueta_nombre_razon_social">PE</li>
					<li class="seg-count" id="seg-len_nombre_razon_social"><% segs.emp.nombre_razon_social | stringLength %></li>
					<li id="seg-nombre_razon_social"><% segs.emp.nombre_razon_social %></li>
				</span>

				<span ng-show="segs.emp.primer_linea_dir">
					<li class="seg-label" id="seg-etiqueta_primer_linea_dir">00</li>
					<li class="seg-count" id="seg-len_primer_linea_dir"><% segs.emp.primer_linea_dir | stringLength %></li>
					<li id="seg-primer_linea_dir"><% segs.emp.primer_linea_dir %></li>
				</span>

				<span ng-show="segs.emp.segunda_linea_dir">
					<li class="seg-label" id="seg-etiqueta_segunda_linea_dir">01</li>
					<li class="seg-count" id="seg-len_segunda_linea_dir"><% segs.emp.segunda_linea_dir | stringLength %></li>
					<li id="seg-segunda_linea_dir"><% segs.emp.segunda_linea_dir %></li>
				</span>

				<span ng-show="segs.emp.colonia">
					<li class="seg-label" id="seg-etiqueta_colonia">02</li>
					<li class="seg-count" id="seg-len_colonia"><% segs.emp.colonia | stringLength %></li>
					<li id="seg-colonia"><% segs.emp.colonia %></li>
				</span>

				<span ng-show="segs.emp.delegacion">
					<li class="seg-label" id="seg-etiqueta_delegacion">03</li>
					<li class="seg-count" id="seg-len_delegacion"><% segs.emp.delegacion | stringLength %></li>
					<li id="seg-delegacion"><% segs.emp.delegacion %></li>
				</span>

				<span ng-show="segs.emp.ciudad">
					<li class="seg-label" id="seg-etiqueta_ciudad">04</li>
					<li class="seg-count" id="seg-len_ciudad"><% segs.emp.ciudad | stringLength %></li>
					<li id="seg-ciudad"><% segs.emp.ciudad %></li>
				</span>

				<span ng-show="segs.emp.estado">
					<li class="seg-label" id="seg-etiqueta_estado">05</li>
					<li class="seg-count" id="seg-len_estado"><% segs.emp.estado | stringLength %></li>
					<li id="seg-estado"><% segs.emp.estado %></li>
				</span>

				<span ng-show="segs.emp.codigo_postal">
					<li class="seg-label" id="seg-etiqueta_codigo_postal">06</li>
					<li class="seg-count" id="seg-codigo_postal"><% segs.emp.codigo_postal | stringLength %></li>
					<li id="seg-codigo_postal"><% segs.emp.codigo_postal %></li>
				</span>

				<span ng-show="segs.emp.telefono">
					<li class="seg-label" id="seg-etiqueta_telefono">07</li>
					<li class="seg-count" id="seg-len_telefono"><% segs.emp.telefono | stringLength %></li>
					<li id="seg-telefono"><% segs.emp.telefono %></li>
				</span>

				<span ng-show="segs.emp.ext_tel">
					<li class="seg-label" id="seg-etiqueta_ext_tel">08</li>
					<li class="seg-count" id="seg-len_ext_tel"><% segs.emp.ext_tel | stringLength %></li>
					<li id="seg-ext_tel"><% segs.emp.ext_tel %></li>
				</span>

				<span ng-show="segs.emp.fax">
					<li class="seg-label" id="seg-etiqueta_fax">09</li>
					<li class="seg-count" id="seg-len_fax"><% segs.emp.fax | stringLength %></li>
					<li id="seg-fax"><% segs.emp.fax %></li>
				</span>

				<span ng-show="segs.emp.cargo_ocupacion">
					<li class="seg-label" id="seg-etiqueta_cargo_ocupacion">10</li>
					<li class="seg-count" id="seg-len_cargo_ocupacion"><% segs.emp.cargo_ocupacion | stringLength %></li>
					<li id="seg-cargo_ocupacion"><% segs.emp.cargo_ocupacion %></li>
				</span>

				<span ng-show="segs.emp.fecha_contrat">
					<li class="seg-label" id="seg-etiqueta_fecha_contrat">11</li>
					<li class="seg-count" id="seg-len_fecha_contrat"><% segs.emp.fecha_contrat | stringLength %></li>
					<li id="seg-fecha_contrat"><% segs.emp.fecha_contrat %></li>
				</span>

				<span ng-show="segs.emp.clave_modeda">
					<li class="seg-label" id="seg-etiqueta_clave_modeda">12</li>
					<li class="seg-count" id="seg-len_clave_modeda"><% segs.emp.clave_modeda | stringLength %></li>
					<li id="seg-clave_modeda"><% segs.emp.clave_modeda %></li>
				</span>

				<span ng-show="segs.emp.monto_sueldo">
					<li class="seg-label" id="seg-etiqueta_monto_sueldo">13</li>
					<li class="seg-count" id="seg-len_monto_sueldo"><% segs.emp.monto_sueldo | stringLength %></li>
					<li id="seg-monto_sueldo"><% segs.emp.monto_sueldo %></li>
				</span>

				<span ng-show="segs.emp.periodo_pago">
					<li class="seg-label" id="seg-etiqueta_periodo_pago">14</li>
					<li class="seg-count" id="seg-len_periodo_pagado"><% segs.emp.periodo_pagado | stringLength %></li>
					<li id="seg-periodo_pago"><% segs.emp.periodo_pago %></li>
				</span>

				<span ng-show="segs.emp.num_empleado">
					<li class="seg-label" id="seg-etiqueta_num_empleado">15</li>
					<li class="seg-count" id="seg-len_num_empleado"><% segs.emp.num_empleado | stringLength %></li>
					<li id="seg-num_empleado"><% segs.emp.num_empleado %></li>
				</span>

				<span ng-show="segs.emp.fecha_ult_dia">
					<li class="seg-label" id="seg-etiqueta_fecha_ult_dia">16</li>
					<li class="seg-count" id="seg-len_fecha_ult_dia"><% segs.emp.fecha_ult_dia | stringLength %></li>
					<li id="seg-fecha_ult_dia"><% segs.emp.fecha_ult_dia %></li>
				</span>

				<li class="seg-label" id="seg-etiqueta_origen_razon_social">20</li>
				<li id="seg-origen_razon_social">MX</li>

			</ul>
			<hr>
		</section>
		<section id="empleo_settings">
			<div class="form-group">
				<label for="emp_nombre_razon_social">PE - NOMBRE O RAZÓN SOCIAL DEL EMPLEADOR <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_nombre_razon_social">?</span></label>
				<div id="tip_emp_nombre_razon_social" class="collapse">
					<p>
						Contiene el nombre del empleador del Cliente, ya sea nombre de una Persona Física o la Razón Social de una empresa.
						<ul>
							<li>Si no se tiene el nombre, Deberá incluir la frase <strong>NO PROPORCIONADO</strong>.</li>
							<li><strong>Si no se incluye un dato no se incluirán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_nombre_razon_social" name="emp_nombre_razon_social" class="form-control" ng-model="segs.emp.nombre_razon_social" maxlength="40">
				<small>Opcional | Etiqueta: PE | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_primer_linea_dir">00 - PRIMER LÍNEA DE DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_primer_linea_dir">?</span></label>
				<div id="tip_emp_primer_linea_dir" class="collapse">
					<p>
						Contiene la dirección del domicilio del Empleador.
						<ul>
							<li>Incluir: calle o similar, número exterior e interior cuando existan.</li>
							<li><strong>Si no se incluye un dato válido, no se registrarán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_primer_linea_dir" name="emp_primer_linea_dir" class="form-control" ng-model="segs.emp.primer_linea_dir" maxlength="40">
				<small>Opcional | Etiqueta: 00 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_segunda_linea_dir">01 - SEGUNDA LÍNEA DE DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_segunda_linea_dir">?</span></label>
				<div id="tip_emp_segunda_linea_dir" class="collapse">
					<p>
						Este campo es usado cuando no es suficiente el campo anterior de “Primer línea de Dirección”
					</p>
				</div>
				<input type="text" id="emp_segunda_linea_dir" name="emp_segunda_linea_dir" class="form-control" ng-model="segs.emp.segunda_linea_dir" maxlength="40">
				<small>Opcional | Etiqueta: 01 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_colonia">02 - COLONIA O POBLACIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_colonia">?</span></label>
				<div id="tip_emp_colonia" class="collapse">
					<p>
						Reportar la Colonia o población si se tiene disponible.
					</p>
				</div>
				<input type="text" id="emp_colonia" name="emp_colonia" class="form-control" ng-model="segs.emp.colonia" maxlength="40">
				<small>Opcional | Etiqueta: 02 | Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_delegacion">03 - DELEGACIÓN O MUNICIPIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_delegacion">?</span></label>
				<div id="tip_emp_delegacion" class="collapse">
					<p>
						Reportar la Delegación o el Municipio si es que se tiene disponible.
						<ul>
							<li><strong>En caso de no reportar la Delegación o el Municipio, el campo 04 de Ciudad se hace requerido.</strong></li>
							<li><strong>En caso de no reportar la Ciudad, el campo de Delegación o Municipio se hace requerido.</strong></li>
							<li><strong>Si no se aplica uno de los 2 puntos anteriores, no se incluirán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_delegacion" name="emp_delegacion" class="form-control" ng-model="segs.emp.delegacion" maxlength="40">
				<small>Opcional | Etiqueta: 03 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_ciudad">04 - CIUDAD <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_ciudad">?</span></label>
				<div id="tip_emp_ciudad" class="collapse">
					<p>
						Reportar el nombre de la ciudad donde trabaja el Cliente.
						<ul>
							<li><strong>En caso de no reportar el campo 03 de “Delegación o Municipio”, el campo de Ciudad se hace requerido.</strong></li>
							<li><strong>En caso de no reportar la Ciudad, el campo de Delegación o Municipio se hace requerido.</strong></li>
							<li><strong>Si no se aplica uno de los 2 puntos anteriores, no se incluirán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_ciudad" name="emp_ciudad" class="form-control" ng-model="segs.emp.ciudad" maxlength="40">
				<small>Opcional | Etiqueta: 04 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_estado">05 - ESTADO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_estado">?</span></label>
				<div id="tip_emp_estado" class="collapse">
					<p>
						Contiene el código del estado de la República Mexicana del domicilio del Empleador.
						<ul>
							<li><strong>Si no se reporta un dato válido no se incluirán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<select id="emp_estado" name="emp_estado" class="form-control" ng-model="segs.emp.estado">
					<option value="AGS">AGS - Aguascalientes
					<option value="BCN">BCN - Baja California Norte
					<option value="BCS">BCS - Baja California Sur
					<option value="CAM">CAM - Campeche
					<option value="CHS">CHS - Chiapas
					<option value="CHI">CHI - Chihuahua
					<option value="COA">COA - Coahuila
					<option value="COL">COL - Colima
					<option value="DF">DF - Distrito Federal
					<option value="DGO">DGO - Durango
					<option value="EM">EM - Estado de México
					<option value="GTO">GTO - Guanajuato</option>
					<option value="GRO">GRO - Guerrero</option>
					<option value="HGO">HGO - Hidalgo</option>
					<option value="JAL">JAL - Jalisco</option>
					<option value="MICH">MICH - Michoacán</option>
					<option value="MOR">MOR - Morelos</option>
					<option value="NAY">NAY - Nayarit</option>
					<option value="NL">NL - Nuevo León</option>
					<option value="OAX">OAX - Oaxaca</option>
					<option value="PUE">PUE - Puebla</option>
					<option value="QRO">QRO - Querétaro</option>
					<option value="QR">QR - Quintana Roo</option>
					<option value="SLP">SLP - San Luis Potosí</option>
					<option value="SIN">SIN - Sinaloa</option>
					<option value="SON">SON - Sonora</option>
					<option value="TAB">TAB - Tabasco</option>
					<option value="TAM">TAM - Tamaulipas</option>
					<option value="TLA">TLA - Tlaxcala</option>
					<option value="VER">VER - Veracruz</option>
					<option value="YUC">YUC - Yucatán</option>
					<option value="ZAC">ZAC - Zacatecas</option>
				</select>
				<small>Opcional | Etiqueta: 05 | Logitud Variable | 04 caracteres max | Alfabético</small>
			</div>

			<div class="form-group">
				<label for="emp_codigo_postal">06 - CÓDIGO POSTAL <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_codigo_postal">?</span></label>
				<div id="tip_emp_codigo_postal" class="collapse">
					<p>
						Se reporta el Código Postal correspondiente, debe ser de exactamente 5 posiciones.
						<ul>
							<li><strong>Este dato se validará de acuerdo a la lista de SEPOMEX, y concordar con el Estado, Delegación o Municipio y Ciudad.</strong></li>
							<li><strong>Si no se reporta un dato válido no se incluirán los datos completos del Empleo.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_codigo_postal" name="emp_codigo_postal" class="form-control" ng-model="segs.emp.codigo_postal" maxlength="5">
				<small>Opcional | Etiqueta: 06 | Logitud Fija | 5 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_telefono">07 - NÚMERO DE TELÉFONO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_telefono">?</span></label>
				<div id="tip_emp_telefono" class="collapse">
					<p>
						Contiene el número telefónico del empleo del Cliente. <br>
						El formato es: <br>
						<ul>
							<li><strong>Código de área + hasta 8 dígitos del teléfono.</strong></li>
							<li><strong>Longitud mínima es de 5 dígitos, no usar guiones, no repetir el mismo número, ejemplo: 00000 ó 55555 ó 77777, etc..</strong></li>
							<li><strong>Ejemplo: número telefónico de la CD. de México se reporta: 5554494949.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_telefono" name="emp_telefono" class="form-control" ng-model="segs.emp.telefono" maxlength="11">
				<small>Opcional | Etiqueta: 07 | Logitud Variable | 11 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_ext_tel">08 - EXTENSIÓN TELEFÓNICA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_ext_tel">?</span></label>
				<div id="tip_emp_ext_tel" class="collapse">
					<p>
						Reportar si se cuenta con el dato.
					</p>
				</div>
				<input type="text" id="emp_ext_tel" name="emp_ext_tel" class="form-control" ng-model="segs.emp.ext_tel" maxlength="8">
				<small>Opcional | Etiqueta: 08 | Logitud Variable | 8 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_fax">09 - NÚMERO DE FAX EN ESTA DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_fax">?</span></label>
				<div id="tip_emp_fax" class="collapse">
					<p>
						Contiene el número telefónico del Fax en el empleo del Cliente. <br>
						El formato es: <br>
						<ul>
							<li><strong>Código de área + hasta 8 dígitos del teléfono.</strong></li>
							<li><strong>Longitud mínima es de 5 dígitos, no usar guiones, no repetir el mismo número, ejemplo: 00000 ó 55555 ó 77777, etc..</strong></li>
							<li><strong>Ejemplo: número telefónico de la CD. de México se reporta: 5554494949.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_fax" name="emp_fax" class="form-control" ng-model="segs.emp.fax" maxlength="11">
				<small>Opcional | Etiqueta: 09 | Logitud Variable | 11 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_cargo_ocupacion">10 - CARGO U OCUPACIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_cargo_ocupacion">?</span></label>
				<div id="tip_emp_cargo_ocupacion" class="collapse">
					<p>
						Reportar el título o posición como empleado del Cliente, si se tiene disponible.
					</p>
				</div>
				<input type="text" id="emp_cargo_ocupacion" name="emp_cargo_ocupacion" class="form-control" ng-model="segs.emp.cargo_ocupacion" maxlength="30">
				<small>Opcional | Etiqueta: 10 | Logitud Variable | 30 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_fecha_contrat">11 - FECHA DE CONTRATACIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_fecha_contrat">?</span></label>
				<div id="tip_emp_fecha_contrat" class="collapse">
					<p>
						Contiene la fecha en la que el empleador contrató al Cliente<br>
						El formato es DDMMAAAA:
						<ul>
							<li><strong>DD: número entre 01- 31.</strong></li>
							<li><strong>MM: número entre 01-12.</strong></li>
							<li><strong>AAAA: año.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_fecha_contrat" name="emp_fecha_contrat" class="form-control" ng-model="segs.emp.fecha_contrat" maxlength="8">
				<small>Opcional | Etiqueta: 11 | Logitud Fija | 8 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_clave_modeda">12 - CLAVE DE LA MONEDA DE PAGO DEL SUELDO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_clave_modeda">?</span></label>
				<div id="tip_emp_clave_modeda" class="collapse">
					<p>
						Se refiere a la moneda con la cual le pagan al Cliente su Sueldo o Salario.
						<ul>
							<li><strong>Los valores permitidos se presentan en el ANEXO 10 de “PAÍSES Y MONEDAS”.</strong></li>
							<li><strong>Si se proporciona este dato, los campos de “MONTO DE SUELDO O SALARIO” y “PERIODO DE PAGO O BASE SALARIAL” son Requeridos.</strong></li>
						</ul>
					</p>
				</div>
				<select id="emp_clave_modeda" name="emp_clave_modeda" class="form-control" ng-model="segs.emp.clave_modeda">
					<option value="MX">MX</option>
				</select>
				<small>Opcional | Etiqueta: 12 | Logitud Fija | 2 caracteres | Alfabético</small>
			</div>

			<div class="form-group">
				<label for="emp_monto_sueldo">13 - MONTO DE SUELDO O SALARIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_monto_sueldo">?</span></label>
				<div id="tip_emp_monto_sueldo" class="collapse">
					<p>
						Indicar el monto de ingreso por Sueldo o Salario de acuerdo al período que su empleador realice el pago.
						<ul>
							<li><strong>El Campo 14 indicará este período.</strong></li>
							<li><strong>Si se reporta la “CLAVE DE MONEDA DE PAGO DEL SUELDO”, este campo es Requerido.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_monto_sueldo" name="emp_monto_sueldo" class="form-control" ng-model="segs.emp.monto_sueldo" maxlength="9">
				<small>Opcional | Etiqueta: 13 | Logitud Variable | 9 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_periodo_pago">14 - PERIODO DE PAGO O BASE SALARIAL <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_periodo_pago">?</span></label>
				<div id="tip_emp_periodo_pago" class="collapse">
					<p>
						Reportar si se cuenta con el dato.
					</p>
				</div>
				<select id="emp_periodo_pago" name="emp_periodo_pago" class="form-control" ng-model="segs.emp.periodo_pago">
					<option value="B">B - Bimestral</option>
					<option value="D">D - Diario</option>
					<option value="H">H - Por Hora</option>
					<option value="K">K - Catorcenal</option>
					<option value="M">M - Mensual</option>
					<option value="S">S - Quincenal</option>
					<option value="W">W - Semanal</option>
					<option value="Y">Y - Anual</option>
				</select>
				<small>Opcional | Etiqueta: 14 | Logitud Fija | 1 caracteres | Alfabético</small>
			</div>

			<div class="form-group">
				<label for="emp_num_empleado">15 - NÚMERO DE EMPLEADO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_num_empleado">?</span></label>
				<div id="tip_emp_num_empleado" class="collapse">
					<p>
						Indicar el número de empleado o de Nómina que le asignó el Empleador.
					</p>
				</div>
				<input type="text" id="emp_num_empleado" name="emp_num_empleado" class="form-control" ng-model="segs.emp.num_empleado" maxlength="15">
				<small>Opcional | Etiqueta: 15 | Logitud Variable | 15 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="emp_fecha_ult_dia">16 - FECHA DE ÚLTIMO DÍA DE EMPLEO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_fecha_ult_dia">?</span></label>
				<div id="tip_emp_fecha_ult_dia" class="collapse">
					<p>
						Indicar la fecha en que trabajó por última vez con el Empleador reportado.<br>
						El formato es DDMMAAAA:
						<ul>
							<li><strong>DD: número entre 01-31.</strong></li>
							<li><strong>MM: número entre 01-12.</strong></li>
							<li><strong>AAAA: año.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="emp_fecha_ult_dia" name="emp_fecha_ult_dia" class="form-control" ng-model="segs.emp.fecha_ult_dia" maxlength="8">
				<small>Opcional | Etiqueta: 16 | Logitud Fija | 8 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="emp_origen_razon_social">20 - ORIGEN DE LA RAZÓN SOCIAL Y DOMICILIO (PAÍS) <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_emp_origen_razon_social">?</span></label>
				<div id="tip_emp_origen_razon_social" class="collapse">
					<p>
						<ul>
							<li><strong>Indicar el origen de la razón social del empleo del Acreditado.</strong></li>
							<li><strong>La identificación incluye el domicilio, cuando éste es capturado con la Razón Social.</strong></li>
							<li><strong>El origen (país) del domicilio del Cliente se clasifica de acuerdo con el catálogo “PAÍSES” (ver Anexo 10 – código de países).</strong></li>
						</ul>
					</p>
				</div>
				<select id="emp_origen_razon_social" name="emp_origen_razon_social" class="form-control">
					<option value="MX">MX</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 20 | Logitud Fija | 2 caracteres max | Alfabético</small>
			</div>
		</section>
  </div>
</div>