@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
@if(Auth::user()->can('cliente-estrella'))
<div id="app" class="content">
	<cliente_estrella></cliente_estrella>
</div>
@endif
@endsection
@section('app_backoffice')
	<script type="text/javascript" src="/js/app_backoffice.js"></script>
@append
