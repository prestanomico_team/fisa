@extends('crm.app')
@section('content')
<style>
	th {
		text-align: center;
	}
	.form-control-sm {
		height: 28px;
		font-size: .85em;
	}
</style>
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="col-xs-12">
						<center>
							<h4 class="box-title">
								Cobertura Códigos Postales
							</h4>
						</center>
					</div>
					<br><br>
				</div>
				<div class="box-body" style="font-size: 12px;">
					<div class="col-sm-10 col-md-offset-1">
						<div class="box-body" id="lista_codigos">
							<div class="col-xs-12" style="text-align:center">
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover" style="table-layout:fixed;">
										<thead>
											<tr>
												<th width="13%">Código Postal</th>
												<th width="13%">Estado</th>
												<th width="13%">Municipio</th>
												<th width="50%">Colonias</th>
												<th width="10%">Cobertura</th>
											</tr>
											<tr>
												<th width="10%">
													<input type="text" value="{{ $cp }}" class="form-control form-control-sm" placeholder="Código Postal" id="cp" onkeyup="buscar()"></input>
												</th>
											</tr>
										</thead>
										<tbody id="codigos">
											@foreach ($codigos as $codigo)
											<tr>
												<td>{{ $codigo->codigo }}</td>
												<td>{{ $codigo->estado }}</td>
												<td>{{ $codigo->municipio }}</td>
												<td>{{ $codigo->colonias }}</td>
												<td>{{ $codigo->cobertura }}</td>
											</tr>
											@endforeach
											<tr>
												<td colspan="5">{{ $codigos->links() }}</td>
											</tr>
										</tbody>
									<table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
	function buscar() {
		var cp = $('#cp').val();
		if (cp.length >= 3 || cp.length == 0) {
			$.ajax({
				url:'buscar_cobertura',
				data: { cp: cp },
				dataType:'json',
				async:true,
				type:'get',
				success: function(response) {
					$("#codigos").empty();
					$("#codigos").html(response.view);
				},
			});
		}
	}
</script>
@endsection
