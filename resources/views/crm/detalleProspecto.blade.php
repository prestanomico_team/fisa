@extends('crm.app')
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<div class="container" style="width:100%">
	<div id="app" class="content">

	    <detalle_prospecto :prospecto_id={{ $prospecto_id }}></detalle_prospecto>

	</div>
</div>
@endsection
@section('app_backoffice')
	<script type="text/javascript" src="/js/app_backoffice.js?v=<?php echo microtime(); ?>"></script>
@append
