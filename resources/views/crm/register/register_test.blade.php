@extends('crm.app')
@section('content')
	<script>
	 var mocks_arr = [];
	<?php
		foreach($mocks as $mp){
			echo 'mocks_arr['.$mp->id.'] = {
					"nombre" : "'.$mp->nombre.'",
					"seg_nombre" : "'.$mp->seg_nombre.'",
					"apellido_p" : "'.$mp->apellido_p.'",
					"apellido_m" : "'.$mp->apellido_m.'",
					"fecha_nac_dd" : "'.$mp->fecha_nac_dd.'",
					"fecha_nac_mm" : "'.$mp->fecha_nac_mm.'",
					"fecha_nac_yyyy" : "'.$mp->fecha_nac_yyyy.'",
					"estado_civil" : "'.$mp->estado_civil.'",
					"sexo" : "'.$mp->sexo.'",
					"curp" : "'.$mp->curp.'",
					"rfc" : "'.$mp->rfc.'",
					"dom_calle" : "'.$mp->dom_calle.'",
					"dom_num_ext" : "'.$mp->dom_num_ext.'",
					"dom_num_int" : "'.$mp->dom_num_int.'",
					"dom_colonia" : "'.$mp->dom_colonia.'",
					"dom_deleg" : "'.$mp->dom_deleg.'",
					"dom_ciudad" : "'.$mp->dom_ciudad.'",
					"dom_estado" : "'.$mp->dom_estado.'",
					"dom_cp" : "'.$mp->dom_cp.'",
					"dom_tel_casa" : "'.$mp->dom_tel_casa.'",
					"credito_hipo" : "'.$mp->credito_hipo.'",
					"credito_auto" : "'.$mp->credito_auto.'",
					"credito_banc" : "'.$mp->credito_banc.'",
					"credito_banc_num" : "'.$mp->credito_banc_num.'",
					"ocupacion" : "'.$mp->ocupacion.'",
					"fuente_ingresos" : "'.$mp->fuente_ingresos.'",
					"nivel_estudios" : "'.$mp->nivel_estudios.'",
					"tipo_residencia" : "'.$mp->tipo_residencia.'",
					"ingreso_mensual" : "'.$mp->ingreso_mensual.'",
					"antiguedad_empleo" : "'.$mp->antiguedad_empleo.'",
					"antiguedad_domicilio" : "'.$mp->antiguedad_domicilio.'",
					"gastos_familiares" : "'.$mp->gastos_familiares.'",
					"numero_dependientes" : "'.$mp->numero_dependientes.'",
					"telefono_empleo" : "'.$mp->telefono_empleo.'"
			};';
		}
	?>
	</script>
	<div id="sms_register_test_page" class="container">
		<div class="content">
			<header>
				<div class="row">
					<div class="col-sm-6">
						<div class="title">
							<h2>Prueba de Registro (<strong class="kamikaze-brand">Prestanómico</strong>)</lab></h2>
						</div>
					</div>
					@if(Auth::user()->can('prueba-registro'))
					<div class="col-sm-6">
						<a  id="open-login-test-btn" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#login-prueba-modal">Probar Login</a>
					</div>
					@endif
				</div>
			</header>

			<div id="error-message" class="alert alert-danger hidden"></div>
			<div id="info-message" class="alert alert-info hidden"></div>
			@if(Auth::user()->can('prueba-registro'))
			<div id="select-db-row" class="row">
				<div class="col-sm-4">
					<div class="form-group">
							<h3 style="margin-bottom:-10px;">Usar Base de Datos:</h3><br>
							<select id="db_source_select" class="form-control" style="width:250px;">
								<option value=""></option>
								<option value="buro_de_credito">Buró de Crédito</option>
								<option value="mock_db">Tabla Local (BackOffice)</option>
							</select>
					</div>
				</div>
				<div class="col-sm-4">
					<div id="select-mock-person" class="form-group hidden">
							<h3 style="margin-bottom:-10px;">Prellenar Persona Mock:</h3><br>
							<select id="mock-person-field" class="form-control">
								<option value=""></option>
								<?php
									foreach($mocks as $mp){
										echo '<option value="'.$mp->id.'">'.$mp->nombre.' '.$mp->seg_nombre.' '.$mp->apellido_p.' '.$mp->apellido_m.'</option>';
									}
								?>
							</select>
					</div>
				</div>
			</div>


			<!-- Login Modal -->
			<div class="modal fade" id="login-prueba-modal" tabindex="-1" role="dialog" aria-labelledby="login-prueba-modal">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Prueba de Login</h4>
			      </div>
			      <div class="modal-body">
			        <form id="prospect_login_form" method="POST" action="/ldap/login/">
								{!! csrf_field() !!}
								<div class="row" style="padding:10px 60px 10px 60px;">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Correo Electrónico</label>
											<input type="email" name="correo" class="form-control">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label>Contraseña</label>
											<input type="password" name="password" class="form-control">
										</div>
									</div>
									<div class="col-sm-12">
										<div id="prospect-login-result">
										</div>
									</div>
								</div>

							</form>
			      </div>
			      <div class="modal-footer">
			        <a id="prospect_login_submit" class="btn btn-success">Iniciar Sesión</a>
			        <a id="prospect_login_loading" class="btn btn-default hidden">Procesando...</a>
			      </div>
			    </div>
			  </div>
			</div>



			<div id="all_forms_container" class="hidden">
				<a id="show_all_panels" class="btn btn-default btn-sm pull-right">Mostrar Todos</a>
				<!-- STEP 1 REGISTER FORM FOR ACTIVATE CODE -->
				<form id="gen_sms_register_form" method="POST" action="/register_prospect" style="padding:20px;background-color:#eee;">
					{!! csrf_field() !!}
					<input type="hidden" name="referencia" value="PRUEBA PANEL">
					<input type="hidden" name="existing_user" id="simulador_existing_user" value="">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="prestamo">Préstamo</label><br>
								<input type="number" id="prestamo" name="prestamo" class="form-control" placeholder="$">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="plazo">Plazo</label>
								<select id="plazo" name="plazo" class="form-control">
	                <option value="0">6 Meses</option>
	                <option value="1">12 Meses</option>
	                <option value="2">18 Meses</option>
	                <option value="3">12 Quincenas</option>
	                <option value="4">24 Quincenas</option>
	                <option value="5">36 Quincenas</option>
	              </select>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="finalidad">Finalidad</label>
								<select id="finalidad" name="finalidad" class="form-control">
	                <option value="Bienes para el hogar">Bienes para el hogar</option>
	                <option value="Bienes de uso personal">Bienes de uso personal</option>
	                <option value="Pago de tarjeta de crédito">Pago de tarjeta de crédito</option>
	                <option value="Pago préstamo personal">Pago préstamo personal</option>
	                <option value="Gastos escolares">Gastos escolares</option>
	                <option value="Vacaciones y viajes">Vacaciones y viajes</option>
	                <option value="Salud y gastos médicos">Salud y gastos médicos</option>
	                <option value="Inversión de tu negocio">Inversión de tu negocio</option>
	                <option value="Gastos de tu negocio">Gastos de tu negocio</option>
	                <option value="Reparación en casa">Reparación en casa</option>
	                <option value="Emergencia personal">Emergencia personal</option>
	                <option value="otra">otra</option>
	              </select>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="form-group">
								<label for="finalidad">NOS GUSTARÍA CONOCERTE</label><br>
								Cuéntanos un poco más acerca del préstamo que estás buscando.<br>
								<textarea id="finalidad_custom" name="finalidad_custom" cols="40" rows="4"></textarea>
							</div>
						</div>
						<div class="col-sm-7">
							<a id="simulador_next_btn" class="btn btn-success btn-sm" style="margin-top:100px;">Siguiente</a>
							<a id="simulador_loading" class="btn btn-default btn-sm hidden" style="margin-top:100px;">Procesando...</a>
						</div>
						<div class="col-sm-12">
							<hr>
						</div>
					</div>
					<div class="row hidden" id="registro_datos_row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_nombre">Nombre</label>
								<input type="text" id="reg_nombre" name="nombre" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_apellido_p">Apellido Paterno</label>
								<input type="text" id="reg_apellido_p" name="apellido_p" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_apellido_m">Apellido Materno</label>
								<input type="text" id="reg_apellido_m" name="apellido_m" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_cel">Teléfono Celular</label>
								<input type="text" id="reg_cel" name="cel" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_email">Correo Electrónico</label>
								<input type="email" id="reg_email" name="email" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_password">Contraseña</label>
								<input type="password" id="reg_password" name="password" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="reg_password_conf">Confirmar Contraseña</label>
								<input type="password" id="reg_password_conf" name="password_conf" class="form-control">
							</div>
						</div>

						<div class="col-sm-4">
							<a id="gen_sms_register_button" class="btn btn-success btn-sm" style="margin-top:25px;">Enviar Datos</a>
							<a id="gen_sms_loading" class="btn btn-default btn-sm hidden" style="margin-top:25px;">Enviando...</a>
							<input type="submit" value="submit" style="display:none;">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<strong>Resultado de Registro:</strong><br>
							<div id="registro-result-msg">
							</div>
						</div>
					</div>
				</form>

				<hr>

				<!-- STEP 2 CONFIRMATION FORM FOR ACTIVATE CODE -->
				<div id="conf_form_container" class="hidden">
					<form id="conf_sms_code_form" class="" method="POST" action="/conf_sms_code">
						{!! csrf_field() !!}
						<h3>Verifica tu Código</h3>
						<p>Anota el código de seguridad que recibiste via SMS</p>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_conf_id">ID de Prospecto</label>
									<input type="text" id="prospect_conf_id" name="prospect_conf_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-9">
								<div class="form-group">
									<label for="solic_conf_id">ID de Solicitud</label>
									<input type="text" id="solic_conf_id" name="solic_conf_id" class="form-control" style="width:250px;">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_conf_id">Código SMS</label>
									<input type="text" id="conf_code" name="conf_code" class="form-control" style="width:100px;">
								</div>

								<a href="#" id="resend_code_link">Reenviar Código</a>

							</div>

							<div class="col-sm-9">
								<a id="conf_sms_code_submit" class="btn btn-success btn-sm pull-left" style="margin-top:25px;">Verificar</a>
								<a id="conf_sms_loading" class="btn btn-default btn-sm hidden" style="margin-top:25px;">Enviando...</a>
								<input type="submit" value="submit" style="display:none;">
							</div>

							<div class="col-sm-12">
								<br>
								<strong>Resultado de Verificación:</strong><br>
								<div id="verify-result-msg">
								</div>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 3 REGISTRO DOMICILIO -->
				<div id="dom_form_container" class="hidden">
					<form id="registro_domicilio_form" method="POST" action="/reg_domicilio" style="padding:20px;background-color:#eee;">
						{!! csrf_field() !!}
						<h3>Solicitud de Crédito 1/4 (DOMICILIO)</h3>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_dom_id">ID de Prospecto</label>
									<input type="text" id="prospect_dom_id" name="prospect_dom_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_dom_id">ID de Solicitud</label>
									<input type="text" id="solic_dom_id" name="solic_dom_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-12">
								&nbsp;
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_calle">Domicilio Calle</label><br>
									<input type="text" id="solic_dom_calle" name="solic_dom_calle" class="form-control">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_num_ext">Domicilio Numeros</label><br>
									<input type="text" id="solic_dom_num_ext" name="solic_dom_num_ext" class="form-control" placeholder="Num. Ext" style="width:100px;display:inline-block">
									<input type="text" id="solic_dom_num_int" name="solic_dom_num_int" class="form-control" placeholder="Num. Int" style="width:100px;display:inline-block">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_cp">C.P.</label><br>
									<input type="text" id="solic_dom_cp" name="solic_dom_cp" class="form-control" maxlength="5">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_colonia">Colonia</label><br>
									<select id="solic_dom_colonia" name="solic_dom_colonia" class="form-control" readonly>
										<option>--Ingrese Código Postal--</option>
									</select>
									<input type="text" id="solic_dom_colonia_manual" name="solic_dom_colonia_manual" class="form-control hidden">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_deleg">Delegación o Municipio</label><br>
									<input type="text" id="solic_dom_deleg" name="solic_dom_deleg" class="form-control" placeholder="--Ingrese Código Postal--" readonly>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_ciudad">Ciudad</label><br>
									<input type="text" id="solic_dom_ciudad" name="solic_dom_ciudad" class="form-control" placeholder="--Ingrese Código Postal--" readonly>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_dom_estado">Estado</label><br>
									<select id="solic_dom_estado" name="solic_dom_estado" class="form-control" readonly>
										<option>--Ingrese Código Postal--</option>
									</select>
								</div>
							</div>

							<div class="col-sm-8">
								<a id="registro_domicilio_submit" class="btn btn-success btn-sm" style="margin-top:25px;">Siguiente</a>
								<a id="registro_dom_loading" class="btn btn-default btn-sm hidden" style="margin-top:25px;">Enviando...</a>
								<input type="submit" value="submit" style="display:none;">
							</div>

							<div class="col-sm-12">
								<strong>Resultado de Domicilio:</strong><br>
								<div id="domicilio-result-msg">
								</div>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 4 REGISTRO DATOS PERSONALES -->
				<div id="datos_per_form_container" class="hidden">
					<form id="datos_personales_form" method="POST" action="/datos_personales">
						{!! csrf_field() !!}
						<h3>Solicitud de Crédito 2/4 (DATOS PERSONALES)</h3>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_dp_id">ID de Prospecto</label>
									<input type="text" id="prospect_dp_id" name="prospect_dp_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_dp_id">ID de Solicitud</label>
									<input type="text" id="solic_dp_id" name="solic_dp_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-12">
								&nbsp;
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_sexo">Sexo</label>
									<select id="solic_sexo" name="solic_sexo" class="form-control">
										<option value="M">M</option>
										<option value="F">F</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_fecha_nac_dd">Fecha de Nacimiento</label><br>
									<input type="number" id="solic_fecha_nac_dd" name="solic_fecha_nac_dd" class="form-control" style="width:70px;display:inline-block">
									<input type="number" id="solic_fecha_nac_mm" name="solic_fecha_nac_mm" class="form-control" style="width:70px;display:inline-block">
									<input type="number" id="solic_fecha_nac_yyyy" name="solic_fecha_nac_yyyy" class="form-control" style="width:90px;display:inline-block">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_lugar_nac">Lugar de Nacimiento</label><br>
									<input type="text" id="solic_lugar_nac_ciudad" name="solic_lugar_nac_ciudad" class="form-control" placeholder="Ciudad" style="display:inline-block;width:40%;">
									<select id="solic_lugar_nac_estado" name="solic_lugar_nac_estado" class="form-control" style="display:inline-block;width:40%;">
										<option value="">--Estado--</option>
										<option value="Aguascalientes" selected>Aguascalientes</option>
										<option value="Baja California">Baja California</option>
										<option value="Baja California Sur">Baja California Sur</option>
										<option value="Campeche">Campeche</option>
										<option value="Chiapas">Chiapas</option>
										<option value="Chihuahua">Chihuahua</option>
										<option value="Coahuila">Coahuila</option>
										<option value="Colima">Colima</option>
										<option value="Distrito Federal">DF(CDMX)</option>
										<option value="Durango">Durango</option>
										<option value="Guanajuato">Guanajuato</option>
										<option value="Guerrero">Guerrero</option>
										<option value="Hidalgo">Hidalgo</option>
										<option value="Jalisco">Jalisco</option>
										<option value="México">México</option>
										<option value="Michoacán">Michoacán</option>
										<option value="Morelos">Morelos</option>
										<option value="Nayarit">Nayarit</option>
										<option value="Nuevo Leó">Nuevo León</option>
										<option value="Oaxaca">Oaxaca</option>
										<option value="Puebla">Puebla</option>
										<option value="Querétaro">Querétaro</option>
										<option value="Quintana Roo">Quintana Roo</option>
										<option value="San Luis Potosí">San Luis Potosí</option>
										<option value="Sinaloa">Sinaloa</option>
										<option value="Sonora">Sonora</option>
										<option value="Tabasco">Tabasco</option>
										<option value="Tamaulipas">Tamaulipas</option>
										<option value="Tlaxcala">Tlaxcala</option>
										<option value="Veracruz">Veracruz</option>
										<option value="Yucatán">Yucatán</option>
										<option value="Zacatecas">Zacatecas</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_rfc">RFC</label><br>
									<input type="text" id="solic_rfc" name="solic_rfc" class="form-control" maxlength="13">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_curp">CURP</label><br>
									<input type="text" id="solic_curp" name="solic_curp" class="form-control"  maxlength="18">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_telefono_casa">Teléfono de Casa</label><br>
									<input type="text" id="solic_telefono_casa" name="solic_telefono_casa" class="form-control">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_estado_civil">Estado Civil</label><br>
									<select id="solic_estado_civil" name="solic_estado_civil" class="form-control">
										<option></option>
										<option value="D">D - Divorciado</option>
										<option value="F">F - Unión Libre</option>
										<option value="M">M - Casado</option>
										<option value="S">S - Soltero</option>
										<option value="W">W - Viudo</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_nivel_estudios">Nivel de Estudios</label><br>
									<select id="solic_nivel_estudios" name="solic_nivel_estudios" class="form-control">
										<option></option>
										<option value="Secundaria">Secundaria</option>
										<option value="Preparatoria">Preparatoria</option>
										<option value="Carrera Técnica">Carrera Técnica</option>
										<option value="Licenciatura">Licenciatura</option>
										<option value="Posgrado">Posgrado</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<a id="datos_personales_submit" class="btn btn-success btn-sm" style="margin-top:25px;">Siguiente</a>
								<a id="datos_per_loading" class="btn btn-default btn-sm hidden" style="margin-top:25px;">Enviando...</a>
								<input type="submit" value="submit" style="display:none;">
							</div>

							<div class="col-sm-12">
								<strong>Resultado de Datos Personales:</strong><br>
								<div id="datos-personales-result-msg">
								</div>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 5 REGISTRO CUENTAS DE CRÉDITO -->
				<div id="cuentas_form_container" class="hidden">
					<form id="cuentas_credito_form" method="POST" action="/cuentas_de_credito" style="padding:20px;background-color:#eee;">
						{!! csrf_field() !!}
						<h3>Solicitud de Crédito 3/4 (CUENTAS DE CRÉDITO)</h3>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_cc_id">ID de Prospecto</label>
									<input type="text" id="prospect_cc_id" name="prospect_cc_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_cc_id">ID de Solicitud</label>
									<input type="text" id="solic_cc_id" name="solic_cc_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-12">
								&nbsp;
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<strong class="text-uppercase">¿Maneja Crédito Hipotecario?</strong><br>
									<div class="col-sm-3">
										<input type="radio" id="solic_credito_hipotecario_y" name="solic_credito_hipotecario" value="on"> <label for="solic_credito_hipotecario_y">SI</label>
									</div>
									<div class="col-sm-9">
										<input type="radio" id="solic_credito_hipotecario_n" name="solic_credito_hipotecario" value="off"> <label for="solic_credito_hipotecario_n">NO</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<strong class="text-uppercase">¿Maneja Crédito Automotriz?</strong><br>
									<div class="col-sm-3">
										<input type="radio" id="solic_credito_automotriz_y" name="solic_credito_automotriz" value="on"> <label for="solic_credito_automotriz_y">SI</label>
									</div>
									<div class="col-sm-9">
										<input type="radio" id="solic_credito_automotriz_n" name="solic_credito_automotriz" value="off"> <label for="solic_credito_automotriz_n">NO</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<strong class="text-uppercase">¿Maneja Tarjeta de Crédito Bancaria? (Titular)</strong><br>
									<div class="col-sm-3">
										<input type="radio" id="solic_credito_tdc_y" name="solic_credito_tdc" value="on"> <label for="solic_credito_tdc_y">SI</label>
									</div>
									<div class="col-sm-9">
										<input type="radio" id="solic_credito_tdc_n" name="solic_credito_tdc" value="off"> <label for="solic_credito_tdc_n">NO</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-sm-offset-8">
								<div class="form-group">
									<label for="solic_num_tdc">Ingresa los últimos 4 digitos de tu Tarjeta</label><br>
									<input type="text" id="solic_num_tdc" name="solic_num_tdc" class="form-control" maxlength="4">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<p class="autorizar-text">Hoy siendo {{ date('d/m/Y') }}, autorizo a Prestanómico S.A.P.I de C.V. a consultar mis antecedentes crediticios por única ocasión ante la Sociedad de Información Crediticia que estime conviniente, declarando que conozco la naturaleza, alcance y uso que Prestanómico S.A.P.I de C.V. hará de tal información.</p><br>
									<input type="checkbox" id="solic_autorizar" name="solic_autorizar"> SI
								</div>
							</div>

							<div class="col-sm-4">
								<a id="cuentas_credito_submit" class="btn btn-success btn-sm" style="margin-top:25px;">Siguiente</a>
								<a id="cuentas_cred_loading" class="btn btn-default btn-sm hidden" style="margin-top:25px;">Enviando...</a>
								<input type="submit" value="submit" style="display:none;">
							</div>

							<div class="col-sm-12">
								<br>
								<strong>Resultado de Cuentas de Crédito:</strong><br>
								<div id="cuentas-credito-result-msg">
								</div>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 6 Primera Llamada BC Score -->
				<div id="solic_bc_form_container" class="hidden">
					<form id="solicitud_bc_form" method="POST" action="/solicitud_bc_score">
						<div class="row">
							<div class="col-sm-12">
								<h3>Primera Llamada BC Score</h3>
							</div>
							{!! csrf_field() !!}
							<input type="hidden" id="db_source_bc" name="db_source" value="">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_bc_id">ID de Prospecto</label>
									<input type="text" id="prospect_bc_id" name="prospect_bc_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_bc_id">ID de Solicitud</label>
									<input type="text" id="solic_bc_id" name="solic_bc_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<input type="submit" value="submit" style="display:none;">
							</div>
							<div class="col-sm-12">
								<br>
								<strong>Resultado de BC Score:</strong><br>
								<div id="bc-score-result-msg">
								</div>
								<br><br>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 7 Segunda Llamada Reporte Completo -->
				<div id="solic_hawk_form_container" class="hidden">
					<form id="solicitud_hawk_form" method="POST" action="/solicitud_hawk">
						<div class="row">
							<div class="col-sm-12">
								<h3>Segunda Llamada Reporte Completo</h3>
							</div>
							{!! csrf_field() !!}
							<input type="hidden" id="db_source_hawk" name="db_source" value="">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_hawk_id">ID de Prospecto</label>
									<input type="text" id="prospect_hawk_id" name="prospect_hawk_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_hawk_id">ID de Solicitud</label>
									<input type="text" id="solic_hawk_id" name="solic_hawk_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<input type="submit" value="submit" style="display:none;">
							</div>
							<div class="col-sm-12">
								<br>
								<strong>Resultado de Segunda Llamada:</strong><br>
								<div id="hawk-result-msg">
								</div>
								<br><br>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 8 Datos Adicionales -->
				<div id="datos_adic_form_container" class="hidden">
					<form id="datos_adic_form" method="POST" action="/datos_adicionales" style="padding:20px;background-color:#eee;">
						{!! csrf_field() !!}
						<h3>Datos Adicionales</h3>
						<input type="hidden" id="datos_caso" name="datos_caso" value="">
						<p></p>
						<div class="row">
							<div class="col-sm-2">
								<div class="form-group">
									<label for="prospect_adic_id">ID de Prospecto</label>
									<input type="text" id="prospect_adic_id" name="prospect_adic_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="solic_adic_id">ID de Solicitud</label>
									<input type="text" id="solic_adic_id" name="solic_adic_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-8">
								&nbsp;
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_residencia">Residencia</label>
									<select id="solic_residencia" name="solic_residencia" class="form-control">
										<option></option>
										<option value="Propia">Propia</option>
										<option value="Renta">Renta</option>
										<option value="Con Familiares">Con Familiares</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_ingreso_mensual">Ingreso Total Mensual Actual</label><br>
									<input type="number" id="solic_ingreso_mensual" name="solic_ingreso_mensual" class="form-control">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_antiguedad_empleo">Antiguedad en empleo actual</label><br>
									<select id="solic_antiguedad_empleo" name="solic_antiguedad_empleo" class="form-control">
										<option></option>
										<option value="0">Menos de 1 año</option>
										<option value="1">1 año</option>
										<option value="2">2 años</option>
										<option value="3">3 años</option>
										<option value="4">4 años</option>
										<option value="5">5 años</option>
										<option value="6">6 años</option>
										<option value="7">7 años</option>
										<option value="8">8 años</option>
										<option value="9">9 años</option>
										<option value="10">10 años</option>
										<option value="11">Más de 10 años</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_antiguedad_domicilio">Antiguedad en domicilio actual</label><br>
									<select id="solic_antiguedad_domicilio" name="solic_antiguedad_domicilio" class="form-control">
										<option></option>
										<option value="0">Menos de 1 año</option>
										<option value="1">1 año</option>
										<option value="2">2 años</option>
										<option value="3">3 años</option>
										<option value="4">4 años</option>
										<option value="5">5 años</option>
										<option value="6">6 años</option>
										<option value="7">7 años</option>
										<option value="8">8 años</option>
										<option value="9">9 años</option>
										<option value="10">10 años</option>
										<option value="11">Más de 10 años</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_gastos_mensuales">Monto gastos familiares mensuales</label><br>
									<input type="text" id="solic_gastos_mensuales" name="solic_gastos_mensuales" class="form-control">
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_num_dependientes">Numero de Dependientes</label><br>
									<select id="solic_num_dependientes" name="solic_num_dependientes" class="form-control">
										<option></option>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_ocupacion">Ocupación</label><br>
									<select id="solic_ocupacion" name="solic_ocupacion" class="form-control">
										<option></option>
										<option value="Empleado Sector Público">Empleado Sector Público</option>
										<option value="Empleado Sector Privado">Empleado Sector Privado</option>
										<option value="Negocio Propio">Negocio Propio</option>
										<option value="Profesional Independiente">Profesional Independiente</option>
										<option value="Arrendador">Arrendador</option>
										<option value="Pensionado">Pensionado</option>
										<option value="Jubilado">Jubilado</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_princp_fuente_ingr">Principal Fuente de Ingresos</label><br>
									<select id="solic_princp_fuente_ingr" name="solic_princp_fuente_ingr" class="form-control">
										<option></option>
										<option value="Salario">Salario</option>
										<option value="Honorarios">Honorarios</option>
										<option value="Renta">Renta</option>
										<option value="Inmuebles">Inmuebles</option>
										<option value="Pensión">Pensión</option>
										<option value="Jubilación">Jubilación</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="solic_tel_empleo">Teléfono de Empleo</label><br>
									<input type="text" placeholder="10 Dígitos" id="solic_tel_empleo" name="solic_tel_empleo" class="form-control">
								</div>
							</div>

							<div class="col-sm-12 text-center">
								<a id="datos_adic_submit" class="btn btn-success btn-sm">Siguiente</a>
								<a id="datos_adic_loading" class="btn btn-default btn-sm hidden">Enviando...</a>
								<input type="submit" value="submit" style="display:none;">
							</div>

							<div class="col-sm-12">
								<br>
								<strong>Resultado de Datos Adicionales:</strong><br>
								<div id="datos-adic-result-msg">
								</div>
								<br><br>
							</div>
						</div>
					</form>
					<hr>
				</div>

				<!-- STEP 9 FICO ALP -->
				<div id="fico_alp_form_container" class="hidden">
					<form id="solicitud_fico_alp_form" method="POST" action="/solicitud_fico_alp">
						<div class="row">
							<div class="col-sm-12">
								<h3>Tercera Llamada FICO ALP</h3>
							</div>
							{!! csrf_field() !!}
							<input type="hidden" id="db_source_fico" name="db_source" value="">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="prospect_alp_id">ID de Prospecto</label>
									<input type="text" id="prospect_alp_id" name="prospect_alp_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="solic_alp_id">ID de Solicitud</label>
									<input type="text" id="solic_alp_id" name="solic_alp_id" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<input type="submit" value="submit" style="display:none;">
							</div>
							<div class="col-sm-12">
								<br>
								<strong>Resultado de Tercera Llamada:</strong><br>
								<div id="alp-result-msg">
								</div>
								<br><br>
							</div>
						</div>
					</form>
					<hr>
				</div>
			</div>
			@else
				<center>
					<br>
					<h4>No tienes privilegios para realizar esta acción</h4>
				</center>
			@endif
		</div>
	</div>
@endsection
