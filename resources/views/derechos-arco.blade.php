@extends('layouts.appV1')
@section('content')
<div class="container-fluid" style="background-color: #FFD359;">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding:6px">
                <div class="vticker text-center center-block" style="position: relative; height: 62px; overflow: hidden; display: table;">
                    <p style="font-weight: bold; font-size: 1.1em; display: table-cell; vertical-align: middle;">Si tienes dudas respecto a tu crédito LANAVE comunícate al: <a href="tel:5552695201" style="text-decoration: none; color: black;">(55) 5269 5201</a></p>
                </div>
            </div>
        </div>
    </div>
</div><br>
<section class="page-view">
    <div class="page-view-image container">
        <img src="https://financieramontedepiedad.com.mx/wp-content/uploads/2018/08/derechos-arco.png" alt="Derechos ARCO">
        <h1 class="page-view-title">Derechos ARCO</h1>
    </div>

    <div class="min-container container">
        <p>Procedimiento para ejercer los <strong>Derechos ARCO</strong></p>
        <p><strong>Procedimientos para que los titulares de datos personales ejerzan sus Derechos de Acceso, Rectificación, Cancelación, Oposición, negativa al tratamiento de datos personales y Oposición a transferencias.</strong></p>
        <p>Procedimiento para ejercer el derecho de ACCESO, RECTIFICACIÓN, CANCELACIÓN y OPOSICIÓN (ARCO)</p>
        <p>El titular o representante legal deberán:</p>
        <ul>
            <li>Descargar el formato ARCO que corresponda a su solicitud en nuestra página de Internet o solicitarlo al correo electrónico <strong><a href="mailto:une@financieramontedepiedad.com.mx">une@financieramontedepiedad.com.mx</a></strong> o al teléfono
                de contacto de la UNE mencionado en nuestra página de Internet, llenar y firmar el formato que corresponda a su solicitud y entregarlo en la dirección Calle Amberes Número 45, 3er Piso, Colonia Juárez, Alcaldía Cuauhtémoc, C.P. 06600,
                Ciudad de México o en su caso si radica en el interior de la República llamar al teléfono de la UNE para que le indiquen el lugar más cercano a su domicilio para que haga entrega del formato antes mencionado, el formato deberá de contener
                como mínimo la siguiente información:
                <ul>
                    <li>Nombre del titular de los datos personales</li>
                    <li>Domicilio del titular de los datos personales</li>
                    <li>Indicar el correo electrónico o medio por el cual se le notificará la respuesta de la solicitud</li>
                    <li>Tipo de derecho ARCO a ejercer</li>
                    <li>Descripción clara y precisa de los datos personales respecto de los cuales se busca ejercer el derecho</li>
                    <li>Cualquier elemento o documento que facilite la localización de los datos</li>
                    <li>Firma del titular</li>
                </ul>
            </li>
        </ul>
        <p>Para iniciar el trámite deberá proporcionar:</p>
        <ul>
            <li>Original y copia de la solicitud a tramitar</li>
            <li>Original y copia de su Identificación oficial o, en su caso, la documentación que acredite la personalidad del representante legal</li>
            <li>En caso de rectificación de datos, deberá presentar original y copia del documento que acredite la rectificación solicitada</li>
        </ul>
        <p>Procedimiento para la Oposición de transferencias o negativa al tratamiento de datos personales</p>
        <p>El titular o representante legal deberá:</p>
        <ol>
            <li>Descargar el formato ARCO de Oposición en nuestra página de Internet o solicitarlo al correo electrónico <strong><a href="mailto:une@financieramontedepiedad.com.mx">une@financieramontedepiedad.com.mx</a></strong> o al teléfono de contacto
                de la UNE mencionado en nuestra página de Internet, llenar y firmar el formato de oposición y entregarlo en la dirección Amberes Número 45, 3er Piso, Colonia Juárez, Alcaldía Cuauhtémoc, C.P. 06600, Ciudad de México o en su caso si radica
                en el interior de la República llamar al teléfono de la UNE para que le indiquen el lugar más cercano a su domicilio para que haga entrega del formato antes mencionado, el formato deberá de contener como mínimo la siguiente información:
                <ul>
                    <li>Nombre del titular de los datos personales</li>
                    <li>Domicilio del titular de los datos personales</li>
                    <li>Indicar el correo electrónico o medio por el cual se le notificará la respuesta de la solicitud</li>
                    <li>Tipo de derecho ARCO a ejercer (Oposición)</li>
                    <li>Descripción clara y precisa de los datos personales respecto de los cuales se busca ejercer el derecho</li>
                    <li>Cualquier elemento o documento que facilite la localización de los datos</li>
                    <li>Firma del titular</li>
                </ul>
            </li>
        </ol>
        <p>Para iniciar el trámite deberá proporcionar:</p>
        <ul>
            <li>Original y copia de la solicitud a tramitar</li>
            <li>Original y copia de su Identificación oficial o, en su caso, la documentación que acredite la personalidad del representante legal</li>
        </ul>
        <p>Consideraciones:</p>
        <ol>
            <li>Para el inicio del trámite deberá de presentar el original de cualquiera de las siguientes identificaciones oficiales vigentes:
                <ul>
                    <li>Credencial para votar</li>
                    <li>Cartilla del Servicio Militar Nacional (emitida con un máximo de 5 años al momento de su presentación)</li>
                    <li>Cédula Profesional</li>
                    <li>Pasaporte</li>
                    <li>Credencial del INAPAM</li>
                    <li>Credencial del IMSS o ISSSTE (con fotografía y firma)</li>
                    <li>En caso de extranjeros</li>
                    <li>Pasaporte</li>
                    <li>Forma migratoria</li>
                    <li>Matrícula consular</li>
                    <li>Visa de inmigrante o residente</li>
                    <li>Certificado de naturalización</li>
                </ul>
            </li>
            <li>La acreditación de la personalidad del representante legal puede ser mediante instrumento público, carta poder firmada ante dos testigos, o declaración en comparecencia personal del titular.</li>
            <li>El titular deberá indicar claramente los medios por los cuales se le notificará la respuesta a la solicitud, para estos efectos la respuesta se le enviará en un plazo máximo de 20 días hábiles posterior a la fecha de recepción de la solicitud.
                En caso de que no indique ningún medio de notificación de respuesta el titular deberá acudir personalmente a la dirección de las oficinas de Financiera Monte de Piedad.</li>
            <li>Es importante que se tenga en cuenta que para revocar el consentimiento del tratamiento de datos personales, no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación
                legal requiramos seguir tratando sus datos personales. Asimismo, deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de
                su relación con la Institución.</li>
        </ol>
        <p>En caso de duda favor de contactar al correo electrónico <strong><a href="mailto:une@financieramontedepiedad.com.mx">une@financieramontedepiedad.com.mx</a></strong> o al teléfono de la UNE que se menciona en nuestra página de Internet o si gusta
            hacerlo de manera personal acudir a la dirección antes mencionada al área de Protección de Datos.</p>
        <p><a class="general-button" href="/pdf/solicitud_de_derecho_de_rectificacion.pdf" target="_blank" rel="noopener noreferrer"><span>Solicitud de Derechos de Rectificación</span></a><a class="general-button" href="/pdf/solicitud_de_derecho_de_oposicion_negativa.pdf"
                target="_blank" rel="noopener noreferrer"><span>Solicitud de Derechos de Oposición o Negatividad al Tratamiento de DP</span></a><a class="general-button" href="/pdf/solicitud_de_derecho_de_cancelacion.pdf" target="_blank" rel="noopener noreferrer"><span>Solicitud de Derechos de Cancelación</span></a>
            <a
                class="general-button" href="/pdf/solicitud_de_derecho_de_acceso.pdf" target="_blank" rel="noopener noreferrer"><span>Solicitud de Derechos de Acceso</span></a>
        </p>
        <p><strong>Mecanismos para deshabilitar cookies</strong></p>
        <p>Para deshabilitar el uso de las tecnologías de Internet mencionadas en el aviso de privacidad, realice las siguientes actividades:</p>
        <p>Para rechazar los cookies ingrese al menú de opciones de su navegador y deshabilite la opción de las cookies.&nbsp; A continuación le mencionamos algunas opciones:</p>
        <p>Google Chrome</p>
        <p>Para deshabilitar las cookies en Google Chrome para Windows, sigue estos pasos:</p>
        <ul>
            <li>Haz clic en el menú Herramientas</li>
            <li>Selecciona Opciones</li>
            <li>Haz clic en la pestaña Avanzada</li>
            <li>En la sección &#8220;Privacidad&#8221;, haz clic en Configuración de contenido</li>
            <li>Para permitir cookies de origen y de terceros, selecciona la opción Permitir que se establezcan datos locales. Si solo quieres aceptar cookies de origen, activa la casilla de verificación situada junto a &#8220;Ignorar las excepciones y evitar
                que se habiliten las cookies de terceros&#8221;</li>
            <li>Para deshabilitar selecciona &#8220;bloquear los datos de los sitios y los cookies de terceros&#8221; así como &#8220;No permitir que se guarden datos de los sitios&#8221;</li>
        </ul>
        <p>Microsoft Internet Explorer</p>
        <p>Para deshabilitar las cookies en Internet Explorer 7 u 8, realiza estos pasos:</p>
        <ul>
            <li>Haz clic en Inicio &gt; Panel de control. (En el modo de vista clásica de Windows XP, haz clic en Inicio &gt; Configuración &gt; Panel de control)</li>
            <li>Haz doble clic en el icono Opciones de Internet</li>
            <li>Selecciona la pestaña Privacidad</li>
            <li>Haz clic en Avanzada</li>
            <li>En la ventana &#8220;Configuración avanzada de privacidad&#8221;, selecciona las opciones para deshabilitar las cookies</li>
            <li>En la ventana de opciones de Internet, haz clic en Aceptar para salir de ese cuadro de diálogo</li>
        </ul>
        <p>Mozilla Firefox</p>
        <p>Para deshabilitar las cookies en Mozilla Firefox 3.x para Windows, sigue estos pasos:</p>
        <ul>
            <li>Haz clic en Herramientas &gt; Opciones</li>
            <li>En el panel superior, haz clic en Privacidad</li>
            <li>En la sección &#8220;Firefox podrá:&#8221;, selecciona la opción Usar una configuración personalizada para el historial</li>
            <li>Selecciona la opción para deshabilitar las cookies</li>
            <li>Haz clic en Aceptar</li>
        </ul>
        <p>Para deshabilitar las cookies en Mozilla Firefox para Mac, sigue estos pasos:</p>
        <ul>
            <li>Accede al menú desplegable Firefox</li>
            <li>Selecciona Preferencias</li>
            <li>Haz clic en Privacidad</li>
            <li>En la sección &#8220;Firefox podrá:&#8221;, selecciona la opción Usar una configuración personalizada para el historial</li>
            <li>Selecciona la opción para deshabilitar las cookies</li>
            <li>Haz clic en Aceptar</li>
        </ul>
        <p>Safari</p>
        <p>Para deshabilitar las cookies en Safari, sigue estos pasos:</p>
        <ul>
            <li>Accede al menú desplegable Safari</li>
            <li>Selecciona Preferencias</li>
            <li>En el panel superior, haz clic en Seguridad</li>
            <li>Seleccionar la opción para desactivar las cookies</li>
            <li>Haz clic en Aceptar</li>
        </ul>
    </div>
</section>
@endsection
