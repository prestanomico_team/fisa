<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tu crédito en FINANCIERA MONTE DE PIEDAD está cada vez más cerca, continúa con tu solicitud.</title>

    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage,.mcnRetinaImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}

		body,#bodyTable{
			background-color:#e8e8e8;
		}

		#bodyCell{
			border-top:0;
		}

		h1{
			color:#202020;
			font-family:Helvetica;
			font-size:26px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:left;
		}

		h2{
			color:#202020;
			font-family:Helvetica;
			font-size:22px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:left;
		}

		h3{
			color:#202020;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:left;
		}

		h4{
			color:#202020;
			font-family:Helvetica;
			font-size:18px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:left;
		}

		#templatePreheader{
			background-color:#fafafa;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:0px;
			padding-bottom:0px;
		}

		#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
			color:#656565;
			font-family:Helvetica;
			font-size:12px;
			line-height:100%;
			text-align:left;
		}

		#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
			color:#656565;
			font-weight:normal;
			text-decoration:underline;
		}

		#templateHeader{
			background-color:#e8e8e8;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:9px;
			padding-bottom:0;
		}

		#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
			color:#202020;
			font-family:Helvetica;
			font-size:16px;
			line-height:150%;
			text-align:left;
		}

		#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
			color:#e8e8e8;
			font-weight:normal;
			text-decoration:underline;
		}

		#templateBody{
			background-color:#e8e8e8;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:0px;
			padding-bottom:0px;
		}

		#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
			color:#515151;
			font-family:Helvetica;
			font-size:16px;
			line-height:100%;
			text-align:left;
		}

		#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
			color:#0e9546;
			font-weight:normal;
			text-decoration:underline;
		}

		#templateColumns{
			background-color:#e8e8e8;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:0;
			padding-bottom:9px;
		}

		#templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
			color:#202020;
			font-family:Helvetica;
			font-size:16px;
			line-height:150%;
			text-align:left;
		}

		#templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
			color:#007C89;
			font-weight:normal;
			text-decoration:underline;
		}

		#templateFooter{
			background-color:#e8e8e8;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:9px;
			padding-bottom:9px;
		}

		#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
			color:#656565;
			font-family:Helvetica;
			font-size:12px;
			line-height:150%;
			text-align:center;
		}

    	#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
			color:#656565;
			font-weight:normal;
			text-decoration:underline;
		}
        @media only screen and (min-width:768px) {
            .templateContainer {
                width:600px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            body,table,td,p,a,li,blockquote {
                -webkit-text-size-adjust:none !important;
            }
        }

        @media only screen and (max-width: 480px) {
    		body {
    			width:100% !important;
    			min-width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#bodyCell {
    			padding-top:10px !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.columnWrapper {
    			max-width:100% !important;
    			width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnRetinaImage {
    			max-width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnImage {
    			width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
			max-width:100% !important;
			width:100% !important;
		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnBoxedTextContentContainer {
    			min-width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnImageGroupContent {
    			padding:9px !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
    			padding-top:9px !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
    			padding-top:18px !important;
    		}
        }
        @media only screen and (max-width: 480px) {
    		.mcnImageCardBottomImageContent {
    			padding-bottom:9px !important;
    		}
        }
        @media only screen and (max-width: 480px) {
    		.mcnImageGroupBlockInner{
    			padding-top:0 !important;
    			padding-bottom:0 !important;
    		}
        }
        @media only screen and (max-width: 480px) {
    		.mcnImageGroupBlockOuter {
    			padding-top:9px !important;
    			padding-bottom:9px !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnTextContent,.mcnBoxedTextContentColumn {
    			padding-right:18px !important;
    			padding-left:18px !important;
    		}
        }
        @media only screen and (max-width: 480px) {
    		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent {
    			padding-right:18px !important;
    			padding-bottom:0 !important;
    			padding-left:18px !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcpreview-image-uploader{
    			display:none !important;
    			width:100% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		h1 {
    			font-size:22px !important;
    			line-height:125% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		h2 {
    			font-size:20px !important;
    			line-height:125% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		h3 {
    			font-size:18px !important;
    			line-height:125% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		h4 {
    			font-size:16px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
    			font-size:14px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templatePreheader{
    			display:block !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p {
    			font-size:14px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
    			font-size:16px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
    			font-size:16px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
    			font-size:16px !important;
    			line-height:150% !important;
    		}

        }
        @media only screen and (max-width: 480px) {
    		#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
    			font-size:14px !important;
    			line-height:150% !important;
    		}
        }
</style>
</head>
    <body>
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top" id="templatePreheader">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="preheaderContainer"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateHeader">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="headerContainer"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateBody">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="bodyContainer"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateColumns">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top">
                                                <!--[if (gte mso 9)|(IE)]>
                                                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                                <tr>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                    <tr>
                                                        <td valign="top" class="columnContainer"></td>
                                                    </tr>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                    <tr>
                                                        <td valign="top" class="columnContainer"></td>
                                                    </tr>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                    <tr>
                                                        <td valign="top" class="columnContainer"></td>
                                                    </tr>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateFooter">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="footerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;"><img data-file-id="1171375" height="275" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/7f49796c-93c0-401c-a8f9-81e40a6883c4.jpg" style="border: 0px initial ; width: 600px; height: 275px; margin: 0px;" width="600"></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #92133E;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #FFFFFF;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;"><span style="font-size:24px"><span font-family:="">¡Hola,&nbsp;<strong>{{ $nombre }}</strong>!</span></span><br>
<br>
<strong>Tu&nbsp;solicitud ha sido Pre-aprobada</strong><br>
<br>
<span style="font-size:12px"><strong><em>"Préstamo Personal"</em>&nbsp;es un préstamo exclusivo para clientes seleccionados de Nacional Monte de Piedad</strong>&nbsp;diseñado a tu medida con pagos fijos mensuales, con el único propósito de ayudar a nuestros clientes a conseguir sus objetivos.</span><br>
<br>
<strong><span style="font-size:18px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Recibe tu dinero lo antes posible</span></span></strong><br>
&nbsp;</div>

<div style="text-align: justify;">&nbsp;</div>

<div style="text-align: center;"><a href="https://api.whatsapp.com/send?phone=525541634806&amp;text=Hola.%20Me%20interesa%20completar%20mi%20solicitud%20de%20inmediato.%20%C2%BFCu%C3%A1les%20son%20los%20documentos%20que%20tengo%20que%20enviar?" target="_blank"><img data-file-id="1191947" height="57" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/398ac496-28be-4acb-a2c3-895104bb6055.png" style="border: 0px  ; width: 260px; height: 57px; margin: 0px;" width="260"></a></div>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;">
<div style="text-align: right;">&nbsp;</div>

<div style="text-align: center;"><span style="color:#A52A2A"><span style="font-size:19px"><strong>Envía tu documentación:</strong></span></span></div>

<div style="text-align: justify;">&nbsp;</div>

<ol>
	<li style="text-align: justify;"><span style="font-size:13px">Comprobante de domicilio.</span></li>
	<li style="text-align: justify;"><span style="font-size:13px">Comprobante de ingresos.</span></li>
	<li style="text-align: justify;"><span style="font-size:13px">CLABE INTERBANCARIA a tu nombre</span></li>
	<li style="text-align: justify;"><span style="font-size:13px">3 referencias personales.</span></li>
</ol>
</div>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;"><span style="font-size:19px"><strong><span style="color:#a10843">¿Por qué ser tu primer opción?</span></strong></span>

<hr></div>

<ul>
	<li style="text-align: left;"><span style="font-size:13px">Somos una institución financiera respaldada por&nbsp;<em><strong>Nacional Monte de Piedad</strong></em>, con más de 244 años en el mercado.&nbsp;</span></li>
	<li style="text-align: left;"><span style="font-size:13px">Nuestro compromiso está en la construcción de un mejor futuro para nuestros clientes.</span></li>
	<li style="text-align: left;"><span style="font-size:13px">Solicitud en línea,&nbsp;sin necesidad de salir de casa.</span></li>
	<li style="text-align: left;"><span style="font-size:13px">Ofrecemos las tasas más competitivas y los plazos más cómodos, </span></li>
	<li style="text-align: left;"><span style="font-size:13px">Pagos mensuales.</span></li>
	<li style="text-align: left;"><span style="font-size:13px">Préstamos desde $5,000 hasta $50,000.</span></li>
	<li style="text-align: left;"><span style="font-size:13px"><strong>Sin comisión por apertura.</strong></span></li>
</ul>

<div style="text-align: center;"><a href="https://api.whatsapp.com/send?phone=525541634806&amp;text=Hola.%20Me%20interesa%20completar%20mi%20solicitud%20de%20inmediato.%20%C2%BFCu%C3%A1les%20son%20los%20documentos%20que%20tengo%20que%20enviar?" target="_blank"><img data-file-id="1191947" height="57" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/398ac496-28be-4acb-a2c3-895104bb6055.png" style="border: 0px  ; width: 260px; height: 57px; margin: 0px;" width="260"></a></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;"><span style="color:#a10843"><strong><span style="font-size:19px">¿Cuándo recibiré mi dinero?</span></strong></span></div>

<div style="text-align: left;"><br>
<span style="font-size:13px">Usualmente tardamos 48 horas hábiles en dar&nbsp;respuesta a tu&nbsp;solicitud de préstamo una vez que nos envias tu documentación.&nbsp;<br>
<br>
Para ayudarte a agilizar el proceso:</span></div>

<ol>
	<li style="text-align: justify;"><span style="font-size:13px">Asegúrate de enviarnos tu documentación completa&nbsp;y legible.</span></li>
	<li style="text-align: justify;"><span style="font-size:13px">Pide a tus referencias personales&nbsp; que se mantengan atentos&nbsp;para recibir nuestra llamada de verificación.</span></li>
</ol>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 125%;text-align: center;">
                                        <div style="text-align: center;"><span style="font-size:18px"><span style="color:#a10843"><strong>Contáctanos y con gusto te atendemos:</strong></span></span><br>
<br>
<span style="color:#000000">&nbsp;&nbsp;<a href="https://api.whatsapp.com/send?phone=525541634806&amp;text=Hola.%20%C2%BFCu%C3%A1les%20son%20los%20documentos%20que%20conforman%20mi%20expediente?" target="_blank"><img data-file-id="1171403" height="20" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/5149d694-cdd8-4e06-a1e9-3614259bb12f.png" style="border: 0px  ; width: 20px; height: 20px; margin: 0px;" width="20"></a>&nbsp;(55) 4163 4806<br>
<br>
<img data-file-id="1171399" height="20" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/e268364b-7bda-4353-894e-37065cf52d20.png" style="border: 0px initial ; width: 20px; height: 20px; margin: 0px;" width="20">&nbsp;documentaciondigital@financieramontedepiedad.com.mx</span></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <table border="0" cellpadding="0" cellspacing="0" style="width:100.0%;" width="100%">
	<tbody>
		<tr>
			<td>
			<div style="text-align: center;"><span style="font-size:11px"><strong>¿Aún con dudas?</strong></span></div>

			<div style="text-align: justify;"><span style="font-size:10px">Financiera Monte de Piedad en ningún momento te pedirá que realices algún pago anticipado por nuestro servicio.<br>
			Toda la comunicación se realizará a través de nuestro dominio de correo “documentaciondigital@financieramontedepiedad.com.mx”. Si te llega información por otra cuenta, favor de reportarlo.<br>
			Tu información está protegida por nuestros sistemas de seguridad. Para nuestra política de datos personales revisa nuestro <a href="https://financieramontedepiedad.com.mx/aviso-de-privacidad/" target="_blank">aviso de privacidad</a>.<br>
			El pago se realiza de manera automática con cargo a la cuenta donde se te depositó. ¡Olvídate de ir al banco! Solo asegúrate que en las fechas de pago (los días quince de cada mes) el monto total de tu cuota se encuentre en la cuenta.</span></div>
			</td>
		</tr>
	</tbody>
</table>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
