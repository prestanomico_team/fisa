<!doctype html>
<html lang="en"><head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/png" href="images/brand/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Leckerli+One" rel="stylesheet">
        <title>Financiera Monte de Piedad | Crédito Personal</title>
    </head>
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 36px;
        padding: 20px;
    }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700&amp;amp;subset=cyrillic" rel="stylesheet"></head><span id="warning-container"><i data-reactroot=""></i></span>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    @if ($exception->getMessage())
                    <h4 class="maintenance__title"><span>{{ $exception->getMessage() }}</span></h4>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
