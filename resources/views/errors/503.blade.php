<!doctype html>
<html lang="en"><head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/png" href="images/brand/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Leckerli+One" rel="stylesheet">
        <title>Financiera Monte de Piedad | Crédito Personal</title>
    </head>
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 36px;
        padding: 20px;
    }

    .maintenance {
       background-color: #000;
       color: #fff;
       width: 100%;
       height: 100vh;
       display: flex;
       flex-direction: column;
       align-items: center;
       justify-content: center;
       text-align: center;
       position: relative;
    }
    .maintenance__icon {
       width: 100px;
    }
    .maintenance__title {
       font-weight: 400;
       font-size: 1.45rem;
       margin: 0.5em auto 1em;
    }
    .maintenance__cursive {
       font-family: 'Leckerli One', cursive;
       font-size: 1.65rem;
    }
    .maintenance__leyenda {
       padding: 0 1em;
       margin: 1.5em 0;
       font-size: 0.85rem;
    }
    .maintenance__contacto {
       margin-bottom: 2em;
    }
    .contact__text {
       font-weight: 700;
       font-size: 0.9rem;
       margin: 0 0.5em;
       display: inline-block;
    }
    .icon-inline{
       width: 1em;
       height: 1em;
       display: inline-block;
       position: relative;
       top: 0.2em;
    }
    .icon-logo {
       width: 175px;
       display: block;
       margin: 0 auto;
    }
    .maintenance__logo {
       width: 100%;
       position: absolute;
       bottom: 2em;
       left: 0;
    }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700&amp;amp;subset=cyrillic" rel="stylesheet"></head><span id="warning-container"><i data-reactroot=""></i></span>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <img src="/images/error/reloj_mantenimiento.png" class="maintenance__icon" alt="Icono sitio en mantenimiento">
                <h1 class="maintenance__title">En unos minutos estaremos de regreso.</span></h1>
                <p class="maintenance__leyenda">En caso de dudas nos puedes contactar en los siguientes medios:</p>
                <div class="maintenance__contacto">
                   <p class="contact__text"><img src="/images/error/whatsapp.png" class="icon-inline"> (55) 4163 4806</p>
                   <p class="contact__text"><img src="/images/error/email.png" class="icon-inline"> documentaciondigital@financieramontedepiedad.com.mx</p>
                </div>
            </div>
            <div class="maintenance__logo">
                <img src="/images/brand/financiera_monte_de_piedad_logo.png" class="icon-logo" alt="logo empresa">
            </div>
        </div>
    </body>
</html>
