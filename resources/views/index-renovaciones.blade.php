@extends('layouts.renovaciones')
@section('content')
<div class="row">
    <div class="col col-sm-12 col-md-12 container product-header">
        <div class="item">
            <div class="slider-overlay"> </div>
            <img class="img-responsive" src="images/main/renovaciones-header.jpg" alt="Préstamo Personal">
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <br><br>
        <h2 class="secondary-title txt-center">Descubre la oferta de crédito que tenemos para ti.</h2>
        <div class="page-product-description txt-center">
            <p>Por haber sido un gran cliente de Financiera Monte de Piedad tienes una oferta de renovación de crédito.</p>
                <p>¡Completa tu solicitud, es muy fácil!</p> <br>
        </div>
    </div>
</div>
<div class="row">
    <h2 class="secondary-title txt-center">¿Cómo funciona?</h2>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_1.png">
            <p><strong>1. Regístrate como cliente</strong></p>
            <p>Crea tu cuenta y descubre tu oferta. Tu préstamo está pre-aprobado, ahora el proceso es en línea.</p>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_2.png">
            <p><strong>2. Envía tus documentos</strong></p>
            <p>Por tu seguridad antes de depositar el préstamo a tu cuenta verificaremos tus datos de identificación, domicilio e ingresos.</p>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_3.png">
            <p><strong>3. Recibe tu dinero</strong></p>
            <p>Una vez aprobado tu crédito podrás disponer de tu dinero con solo acudir a una sucursal de Nacional Monte de Piedad.</p>
        </div>
    </div>
</div>
<div class="row page-view gray">
    <div id="loading-form" class="loader"></div>
    @if (Auth::guard('prospecto')->check() && Session::has('nueva_solicitud') === false)
        @php
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = App\Prospecto::with('ultima_solicitud')->find($prospecto_id);
            $plazo = App\Plazo::find($prospecto->ultima_solicitud->plazo);
        @endphp
        <div id="info_prestamo_personal" style="display:none">
            <div class="col-lg-4 col-xs-12 ">Tu prestamo: <span id="info_general_prestamo" class="red"> $ {{ number_format($prospecto->ultima_solicitud->prestamo, 2) }} </span> </div>
            <div class="col-lg-3 col-xs-12">Tu plazo: <span id="info_general_plazo" class="red"> {{ $plazo->duracion }} {{ $plazo->plazo }}</span> </div>
            <div class="col-lg-5 col-xs-12">Tu finalidad: <span id="info_general_finalidad" class="red"> {{ $prospecto->ultima_solicitud->finalidad }} </span></div>
        </div>
    @else
        <div id="info_prestamo_personal" style="display:none">
            <div class="col-lg-4 col-xs-12 ">Tu prestamo: <span id="info_general_prestamo" class="red"></span></div>
            <div class="col-lg-3 col-xs-12">Tu plazo: <span id="info_general_plazo" class="red"></span></div>
            <div class="col-lg-5 col-xs-12">Tu finalidad: <span id="info_general_finalidad" class="red"></span></div>
        </div>
    @endif
    <div id="pasosSolicitud">
        @include('parts.refactor.renovaciones', $configuracion)
        @include('parts.refactor.verificar_codigo')
        @include('parts.refactor.datos_domicilio')
        @include('parts.refactor.datos_personales')
        @include('parts.refactor.datos_ingreso')
        @include('parts.refactor.datos_empleo')
        <div id="cuestionarioDinamico">
        </div>
    </div>
    <div class="col-xs-10 col-xs-offset-1 calculadoras-productos">
        <div class="legal">
            <br><br>
            <p>CAT promedio 56% Tasa de interés mensual 4.79%/ Tasa de interés anual promedio: 57.50%. Las cantidades, tasas de interés, fechas, plazos y demás referencias que se señalan en este documento son meramente informativos para el día en que
                se expide y para la persona a quien se le formula la estimación, sin que genere obligación a cargo de FINANCIERA MONTE DE PIEDAD, S.A DE C.V., S.F.P</p>
        </div>
    </div>
</div>
<div class="col-xs-10 col-xs-offset-1">
    <h2 class="secondary-title txt-center uppercase">¿Dudas en el proceso? <span class="black lowercase">Contáctanos:</span></h2>
    <h4 class="secondary-title txt-center uppercase">Lunes a Viernes de 10:00 a 18:00 hrs</h4>
    <h4 class="secondary-title txt-center uppercase">Sábados de 10:00 a 13:00 hrs</h4>
</div>

<div class="col-lg-2 col-md-2 col-md-offset-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
    <div class="page-product-description txt-center">
        <a href="https://api.whatsapp.com/send?phone=525541634806&text=Hola!%20Quiero%20renovar%20mi%20cr%C3%A9dito%20en%20linea" target="_blank"><img src="images/icons/whatsapp.png"></a>
        <a href="https://api.whatsapp.com/send?phone=525541634806&text=Hola!%20Quiero%20renovar%20mi%20cr%C3%A9dito%20en%20linea" target="_blank"><p>55 4163 4806</p></a>
    </div>
</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
    <div class="page-product-description txt-center">
        <img src="images/icons/phone.png">
        <p>55 5206 3809</p>
    </div>
</div>
</div><br>
@if (Auth::guard('prospecto')->check())
    <script>
        window.onload = function() {
            getForm();
            getComboDates();
        };
    </script>
@else
    <script>
        window.onload = function() {
            $.urlParam = function(name){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results==null) {
                   return null;
                }
                return decodeURI(results[1]) || 0;
            }
            $('#loading-form').hide();
            getComboDates();
            if(window.location.href.indexOf('#loginModal') != -1) {
                $('#loginModal').modal('show');
            }
            var uuid = ($.urlParam('uuid'));
            if (uuid !== null) {
                getRFC(uuid);
            }
        };
    </script>
@endif
<script>
function getComboDates() {
    moment.locale('es');
    $('#fecha_nacimiento').combodate({
        maxYear: moment().get('year') - <?php echo $configuracion['configuracion']['edad_minima'] ?>,
        minYear: moment().get('year') - <?php echo $configuracion['configuracion']['edad_maxima'] ?>,
        firstItem: 'name',
        smartDays: true,
        format: 'DD-MM-YYYY'
    });

    $('#fecha_nacimiento_renovaciones').combodate({
        maxYear: moment().get('year') - <?php echo $configuracion['configuracion']['edad_minima'] ?>,
        minYear: moment().get('year') - <?php echo $configuracion['configuracion']['edad_maxima'] ?>,
        firstItem: 'name',
        smartDays: true,
        format: 'DD-MM-YYYY'
    });

    $('#fecha_ingreso').combodate({
        maxYear: moment().get('year'),
        minYear: moment().get('year') - 100,
        firstItem: 'name',
        customClass: 'fecha_ingreso',
        smartDays: true,
        format: 'YYYY-MM-DD'
    });

    $("#formDatosPersonales #year").on("change", function(e) {
        var valor = $(this).find('option:selected').text();
        $('#formDatosPersonales #year').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });

    $("#formDatosPersonales #month").on("change", function(e) {
        var valor = $(this).find('option:selected').val();
        $('#formDatosPersonales #month').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });

    $("#formDatosPersonales #day").on("change", function(e) {
        var valor = $(this).find('option:selected').text();
        valor = parseInt(valor);
        $('#formDatosPersonales #day').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });

    $("#formDatosEmpleo #year").on("change", function(e) {
        var valor = $(this).find('option:selected').text();
        $('#formDatosEmpleo #year').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });

    $("#formDatosEmpleo #month").on("change", function(e) {
        var valor = $(this).find('option:selected').val();
        $('#formDatosEmpleo #month').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });

    $("#formDatosEmpleo #day").on("change", function(e) {
        var valor = $(this).find('option:selected').text();
        valor = parseInt(valor);
        $('#formDatosEmpleo #day').val(valor);
        $('.combodate').removeClass('error');
        $('#fecha_nacimiento-help').html('');
    });
}
</script>
@endsection
