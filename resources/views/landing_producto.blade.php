<!DOCTYPE html>
<html lang='en'>
	<head>
      	<meta charset="UTF-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1.0">
      	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      	<title>Prestanómico | Transfer</title>
		<meta name="description" content="Somos una empresa que brinda préstamos flexibles con una tasa de interés menor a la de los bancos regulares.">
		<meta name="keywords" content="Prestamos personales, Préstamos personales, crédito personal, credito personal, préstamos en línea, prestamos en linea, prestamos sin aval, creditos rapidos, créditos rápidos, préstamos móvil, buró de crédito, buro de credito, CAT, IVA">
      	<link rel="icon" type="image/png" href="/images/favicon_prestanomico_negro.png">
    	{{-- <link rel="stylesheet" href="/css/styles.css"/> --}}
   	</head>
   	<body>
			<!-- Google Tag Manager -->
	      <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KLDBCD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	      })(window,document,'script','dataLayer','GTM-KLDBCD');</script>
	      <!-- End Google Tag Manager -->
   		<section class="transfer center-container">
   			<div class="transfer__content absolute-center">
   				<ul class="transfer__logos">
   					<li><img src="/images/prestanomico_logo.png" alt="Logo prestanómico" class="transfer__logos--prestanomico"></li>
   					<li><img src="/images/transfer_logo.png" alt="Logo transfer" class="transfer__logos--transfer"></li>
					<li><img src="/images/saldazo_logo.png" alt="Logo saldazo" class="transfer__logos--saldazo"></li>
   				</ul>
   				<div class="transfer__text">
   					<h1>Esta oferta es <span class="cursive">especial</span> para ti.</h1>
   					<p>Invierte y crece tu negocio, aprovecha para comprar aquello que siempre has querido o bájale a tus deudas con mejores tasas de interés.*</p>
   				</div>
   				<div id="checaCalificasBtn" class="orange-btn">Checa si calificas</div>
   			</div>
   			<div class="transfer__disclaimer">
   				<p>*El préstamo es otorgado por Prestanómico SAPI de CV</p>
   			</div>
   		</section>

		@include('parts.refactor.footer')
			<script src='/js/all.js' type="text/javascript" charset="utf-8"></script>
			<script src='/js/webapp.js' type="text/javascript" charset="utf-8"></script>
			<script src='/js/cookie_calificas.js?v=<?php echo microtime(); ?>' type="text/javascript" charset="utf-8"></script>
   	</body>
</html>
