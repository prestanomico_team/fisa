@include('googletagmanager::head')
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
        @verbatim
            ga('create', 'UA-80377318-3', 'auto', {
                'cookieDomain': 'none'
            });
        @endverbatim
    @else
        @verbatim
            ga('create', 'UA-129520248-2', 'auto', {
                'cookieDomain': 'none'
            });
        @endverbatim
    @endif
    ga(function(tracker) {
        var clientId = tracker.get('clientId');
        tracker.set('dimension1', tracker.get('clientId'));
    });
    ga('send', 'pageview');
    </script>

@include('googletagmanager::body')