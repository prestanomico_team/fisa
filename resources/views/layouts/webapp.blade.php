<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset="utf-8">
    <title>Financiera Monte de Piedad - Carga de documentos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/images/favicon_prestanomico_negro.png">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    @php
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    @endphp

    <!--Google Search Console-->
    <meta name="google-site-verification" content="RQBze-kf4yQjVFtw5YCyCvSMNbMvGoia5IYCcOjBcao" /><head>
    <script src="/js/all_facematch.js?v=<?php echo microtime(); ?>"></script>
    @if(str_contains(url()->current(), 'facematch'))
        <script async defer src="/js/webapp/icarSDK.js"></script>
    @endif
    <link rel="stylesheet" href="/css/webapp/font-awesome.css">
	<link rel='stylesheet' href='/icon-webapp2/styles.css'>
	<link rel="stylesheet" href="/css/webapp/webapp.css?v=<?php echo microtime(); ?>">
    <link rel="stylesheet" href="/css/webapp/dropzone.min.css">
    <link rel="stylesheet" href="/css/modals.css?v=<?php echo microtime(); ?>">
    @include('googletagmanager::head')
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
        @verbatim
        ga('create', 'UA-80377318-3', 'auto', {
            'cookieDomain': 'none'
        });
        @endverbatim
    @else
        @verbatim
        ga('create', 'UA-129520248-2', 'auto', {
            'cookieDomain': 'none'
        });
        @endverbatim
    @endif
    ga(function(tracker) {
        var clientId = tracker.get('clientId');
        tracker.set('dimension1', tracker.get('clientId'));
        dataLayer.push({'clientId':clientId});
    });
    ga('send', 'pageview');
    </script>
</head>
<body id="laPaz">
    <header class="header">
        @include('webapp.header')
        @yield('head')
    </header>
    <main class="container">
        @include('parts.refactor.modalsWebapp')
        @yield('content')
    </main>
    @include('googletagmanager::body')
</body>
</html>
