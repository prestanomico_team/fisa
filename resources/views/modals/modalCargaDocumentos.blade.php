<div id="cargaDocumentos">
    <div class="modal__heading bg-blue">
      <h2 class="titulo-dinamico">¡Tu solicitud fue Preaprobada!</h2>
   </div>
   <div class="modal__body" id="#laPaz">
      <div class="modal__text">
        <div class="col-12 col-lg-12" id="navigator">
          <ul class="steps steps-3">
            <li><a href="#" title=""><img src="/images/icons/step_1-01.png"></img><br>1<br> Solicitud completa</a></li>
            <li><a href="#" title=""><img src="/images/icons/step_1-02.png"></img><b><br>2<br> Carga de documentos</a></b></li>
            <li class="current"><img src="/images/icons/step_1-03.png"><br><a href="#" title="">3<br> Recibe tu dinero</a></li>
          </ul>
        </div>
        <p>Para concluir, adjunta la documentación solicitada.</p>
        <p>Por favor ACEPTA los permisos para acceder a tu dispositivo.</p>
        <p>Entre más rápido lo compartas, más rápido obtendrás respuesta.</p>
        <p>Si tienes dudas envía un WhatsApp al <b class="secondary-title">(55) 4163 4806</b> y con gusto te apoyaremos.</p>
      </div>
   </div>
