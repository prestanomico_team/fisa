<div id="correccionCelular">
    <form id="formCorreccionCelular">
        <span>Verifica que el télefono celular capturado sea correcto</span>
        <br><br>
        <label>Celular:</label>
        <input id="celularCorreccion" name="celular" maxlength="10" type="text" placeholder="Teléfono celular *10 dígitos" class="required uppercase" value="{{ $celular }}">
    </form>
</div>
