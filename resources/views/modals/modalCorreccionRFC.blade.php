<div id="correccionesRFC">
    <form id="formCorreccion">
        <span>Verifica que la Fecha de Nacimiento y el RFC que capturaste sean los correctos</span>
        <br><br>
        <label>Fecha de nacimiento</label>
        <input id="fecha_nacimientoBC" name="fecha_nacimiento" placeholder="Fecha de Nacimiento BC" class="input-correcciones required" data-format="YYYY-MM-DD" value="{{ $fecha_nacimiento }}">
        <br>
        <label>RFC</label>
        <input class="input-correcciones required" id="rfcBC" name="rfc" type="text" placeholder="R.F.C." maxlength="10" value="{{ $rfc }}">
        <small style="font-size: 11px" id="rfcBC-help">El RFC esta conformado por 4 letras, y 6 números: los 2 últimos dígitos del año, 2 del mes y 2 del día de la fecha de nacimiento.</small>
        <br>
        <label>Homoclave</label>
        <br>
        @if($homoclave != '')
        <input id="homoclave" name="homoclave" class="input-correcciones required" type="text" placeholder="R.F.C." maxlength="3" value="{{ $homoclave }}"></input>
        <br>
        <small style="font-size: 11px" id="homoclave-help">Si no la recuerda, omítala</small>
        @endif
    </form>
</div>
