<style>
.swal2-header {
    height: 10px;
    background-color:#92133E;
    border-radius: 5px 5px 0px 0px;
}
</style>
<div id="modalOfertaDoble">
    <div class="modal__heading bg-blue">
        <h2 class="titulo-dinamico" id="tituloOferta">¡Felicidades! Tienes dos ofertas</h2>
        <h2 class="titulo-dinamico" id="tituloRechazo" style="display:none">Rechazo Oferta</h2>
        <div class="triangle"><div class="triangle__form"></div></div>
    </div>
    <div class="modal__body">
            <div class="modal__emoji" id="emojiOferta">
                <img src="/images/landings/comunidar/logo1.png" alt="icono_financiera" class="logo">
            </div><div class="modal__text" id="seccionOferta" style="width: -webkit-fill-available;">
            <p>Financiera Monte de Pieda te ofrece un crédito pensando en emprendedores como tú con una tasa de interés que hace que los pagos sean mas accesibles.¡Queremos reactivar a Nuevo Léon!</p>
            <div class="ofertas">
                <span class="btn btn-oferta" onclick="selectoferta(this)" style="margin-right:20px;" id="propuesta1"> {{ $datosOferta[0]['monto'] }} </span>
                <span class="btn btn-oferta" onclick="selectoferta(this)" style="" id="propuesta2"> {{ $datosOferta[1]['monto'] }} </span>
            </div>
            <div class="datos_oferta" style="display:none;">
                @foreach($datosOferta as $oferta)
                <div id="datos_propuesta{{ $loop->iteration }}" class="propuesta" style="text-align: justify; margin-bottom: 10px;  margin-top: 10px; padding-left: 15px; display:none;">
                    <ul>
                        <li><i><b>Monto:</b></i><span class="dato"> {{ $oferta['monto'] }}</span></li>
                        <li><i><b>Plazo:</b></i><span class="dato"> {{ $oferta['plazo'] }}</span></li>
                        <li><i><b>Tasa:</b></i><span class="dato"> {{ $oferta['tasa'] }}</span></li>
                        <li><i><b>Pago estimado:</b></i><span class="dato"> {{ $oferta['pago_estimado'] }}</span></li>
                    </ul>
                    <div id="botonesOferta" style="text-align: center">
                        <button class="btn btn-ok" type="button" id="aceptarOfertaDoble">
                            Aceptar Oferta Seleccionada
                        </button>
                    </div>
                    <input type="hidden" id="oferta_propuesta{{ $loop->iteration }}" value="{{ $oferta['tipo_oferta'] }}">
                    <input type="hidden" id="oferta_modelo" value="{{ $oferta['modelo'] }}">
                    <input type="hidden" id="oferta_id_propuesta{{ $loop->iteration }}" value="{{ $oferta['oferta_id'] }}">
                </div>
                @endforeach
                <p class="condiciones">
                    <i>CAT promedio 25%, para un crédito de $20,000 a un plazo de 24 meses y con una comisión por apertura del 2.5%. Fecha de cálculo 05 de Junio de 2020.</i>
                </p>
                <input type="hidden" id="oferta_seleccionada" value="">
                <input type="hidden" id="oferta_id" value="">
            </div>

            <div class="modal__btn" style="text-align:center; display: inline-block; margin: 1em 0 1em;">
                <div id="botonesOfertaOferta">
                    <button class="btn btn-notok" type="button" id="rechazarOferta">
                        Rechazar Oferta
                    </button>
                </div>
            </div>
        </div>
        <div class="modal__text calculadoras-productos" id="seccionRechazo" style="display:none; margin-bottom: 20px">
            <label id="lmotivo_rechazo"> ¿Por qué motivos rechazas la oferta? </label>
            <div class="withDecoration" style="margin-top: 25px;">
                <select class="pre-registro-input" name="motivo_rechazo" id="motivo_rechazo" onchange="cambioMotivoRechazo(this)">
                    <option value="SELECCIONA">SELECCIONA</option>
                    @foreach($motivos as $motivo)
                        <option value="{{ $motivo }}">{{ $motivo }}</option>
                    @endforeach
                </select>
                <div class="decoration"><i class="fas fa-chevron-down"></i></div>
            </div>
            <textarea id="textOtroRechazo" name="textOtroRechazo" maxlength="255" rows="3" style="display:none; margin-top: 10px; margin-bottom: 10px; text-transform: uppercase; padding: 0.5em; width: 100%">
            </textarea>
            <div id="botonesRechazo" style="display:none;">
                <div style="height: 50px;">
                    <button class="btn btn-ok" type="button" id="confirmaRechazarOferta">
                        Rechazar Oferta
                    </button>
                    <button  class="btn btn-notok" type="button" id="cancelarRechazarOferta">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
        <input name="modal_visible" id="modal_visible" type="hidden">
    </div>
</div>
