<div id="modal-body">
    <div class="modal__emoji" style="padding:0px; height:70px;">
        <img src="/images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
    </div>
    <p>No pudimos encontrar tus antecedentes crediticios con la información que nos proporcionaste.</p>
    <p>Por favor revisa que los datos que capturaste sean correctos y vuelve a intentarlo.</p>
</div>
