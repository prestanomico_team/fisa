<style>
    .swal2-header {
        height: 10px;
        background-color:#92133E;
        border-radius: 5px 5px 0px 0px;
    }
    </style>
    <div id="modalOferta">
        <div class="modal__heading bg-blue">
            <h3 class="titulo-dinamico" id="tituloOferta">¡Felicidades! Tu registro inicial ha sido existoso</h3>
        </div>
        <div class="modal__body">
            <form id="formOferta">
                <div class="modal__emoji" id="emojiOferta">
                    <img src="images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
                </div>
                <div class="modal__text" id="seccionOferta">
                    <p><span>Ahora, concluye tu solicitud, por favor, da aceptar para continuar con tu proceso e integración de expediente.</span></p>

                    <p class="modal__text">
                        Si tienes dudas envía un mensaje whatsapp al (55)25592413 y con gusto te apoyaremos.
                    </p>
                </div>
                <input type="hidden" id="oferta_modelo" value="{{ $datosOferta[0]['modelo'] }}">
                <input type="hidden" id="oferta_id" value="{{ $datosOferta[0]['oferta_id'] }}">
                <div class="modal__btn" style="text-align:center; display: inline-block;">
                    <div id="botonesOferta">
                        <button class="btn btn-ok" type="button" id="aceptarOferta">
                            Aceptar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
