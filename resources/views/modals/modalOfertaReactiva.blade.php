<style>
.swal2-header {
    height: 10px;
    background-color:#92133E;
    border-radius: 5px 5px 0px 0px;
}
</style>
<div id="modalOferta">
    <div class="modal__heading bg-blue">
        <h2 class="titulo-dinamico" id="tituloOferta">¡Felicidades! Tienes una oferta especial</h2>
        <h2 class="titulo-dinamico" id="tituloRechazo" style="display:none">Rechazo Oferta</h2>
    </div>
    <div class="modal__body">
        <form id="formOferta">
            <div class="modal__emoji" id="emojiOferta">
                <img src="/images/landings/comunidar/logo1.png" alt="icono_financiera" class="logo">
            </div>
            <div class="modal__text" id="seccionOferta">
                <p><span>Financiera Monte de Pieda te ofrece un crédito pensando en emprendedores como tú con una tasa de interés que hace que los pagos sean mas accesibles.¡Queremos reactivar a Nuevo Léon!</span></p>
                <ul style="text-align: justify; margin-bottom: 10px">
                    <li><i><b>Monto:</b></i><span class="dato"> {{ $datosOferta[0]['monto'] }}</span></li>
                    <li><i><b>Plazo:</b></i><span class="dato"> {{ $datosOferta[0]['plazo'] }}</span></li>
                    <li><i><b>Tasa:</b></i><span class="dato"> {{ $datosOferta[0]['tasa'] }}</span></li>
                    <li><i><b>Pago estimado:</b></i><span class="dato"> {{ $datosOferta[0]['pago_estimado'] }}</span></li>
                </ul>
                <p class="condiciones">
                    <i>CAT promedio 25%, para un crédito de $20,000 a un plazo de 24 meses y con una comisión por apertura del 2.5%. Fecha de cálculo 05 de Junio de 2020.</i>
                </p>
                <input type="hidden" id="oferta_modelo" value="{{ $datosOferta[0]['modelo'] }}">
                <input type="hidden" id="oferta_id" value="{{ $datosOferta[0]['oferta_id'] }}">
            </div>
            <div class="modal__text calculadoras-productos" id="seccionRechazo" style="display:none; margin-bottom: 20px">
                <label id="lmotivo_rechazo"> ¿Por qué motivos rechazas la oferta? </label>
                <div class="withDecoration" style="margin-top: 25px;">
                    <select class="pre-registro-input" name="motivo_rechazo" id="motivo_rechazo" onchange="cambioMotivoRechazo(this)">
                        <option value="SELECCIONA">SELECCIONA</option>
                        @foreach($motivos as $motivo)
                            <option value="{{ $motivo }}">{{ $motivo }}</option>
                        @endforeach
                    </select>
                    <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                </div>
                <textarea id="textOtroRechazo" name="textOtroRechazo" maxlength="255" rows="3" style="display:none; margin-top: 10px; margin-bottom: 10px; text-transform: uppercase; padding: 0.5em; width: 100%">
                </textarea>
            </div>
            <div class="modal__btn" style="text-align:center; display: inline-block;">
                <div id="botonesOferta">
                    <button class="btn btn-ok" type="button" id="aceptarOferta">
                        Aceptar Oferta
                    </button>
                    <button class="btn btn-notok" type="button" id="rechazarOferta">
                        Rechazar Oferta
                    </button>
                </div>
                <div id="botonesRechazo" style="display:none;">
                    <div style="height: 50px;">
                        <button class="btn btn-ok" type="button" id="confirmaRechazarOferta">
                            Rechazar Oferta
                        </button>
                        <button  class="btn btn-notok" type="button" id="cancelarRechazarOferta">
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
