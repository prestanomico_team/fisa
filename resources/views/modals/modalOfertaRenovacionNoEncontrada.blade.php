<div id="ofertaRenovacionNoEncontrada">
    <div class="modal__body">
        <div class="modal__emoji" style="padding:0px; height:70px;">
            <img src="/images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
        </div>
        <div class="modal__text">
            <p>Estimado Cliente, no pudimos encontrar la oferta.</p>
            <p>Para continuar con su trámite le pedimos se comunique al:</p>
            <p><b><i class="fa fa-phone"></i> (55) 5269-5201</b></p>
        </div>
    </div>
</div>
