<div id="modal-body">
    <div class="modal__emoji" style="padding:0px; height:70px;">
        <img src="/images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
    </div>
    <p class="bold">¡Gracias por tu solicitud!</p>
    <p>Nuestra política de crédito no nos permite autorizar tu préstamo en este momento.</p>
    <p>Esto puede deberse a factores como tu historial de crédito, nivel de endeudamiento, o simplemente a que todavía no atendemos la plaza en donde radicas.</p>
    <p>Esperamos en un futuro poder atenderte.</p>
</div>
