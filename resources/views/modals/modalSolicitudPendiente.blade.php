<div id="solicFound">
   <div class="modal__heading bg-blue">
      <h2 class="titulo-dinamico">Solicitud Pendiente</h2>
   </div>
   <div class="modal__body">
      <div class="modal__emoji">
         <img src="images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
      </div>
      <div class="modal__text">
         <b>¡La encontramos!</b>
         <p>Te informamos que tienes una solicitud pendiente.</p>
         <p>Debes terminar la solicitud <br>para continuar con el proceso.</p>
      </div>
   </div>
</div>
