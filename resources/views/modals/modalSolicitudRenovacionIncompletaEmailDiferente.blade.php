<div id="solcitudRenovacioIncompletaEmailDiferente">
   <div class="modal__body">
      <div class="modal__emoji">
         <img src="images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
      </div>
      <div class="modal__text">
          <b>¡La encontramos!</b>
          <p>Te informamos que la oferta de renovación esta registrada con un correo diferente al que capturaste.</p>
          <p>Debes iniciar sesión con ese correo para continuar con el proceso.</p>
      </div>
   </div>
</div>
