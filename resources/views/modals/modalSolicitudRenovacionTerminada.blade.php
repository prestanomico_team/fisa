<div id="solicFound">
   <div class="modal__body">
      <div class="modal__emoji">
         <img src="images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
      </div>
      <div class="modal__text">
         <b>¡La encontramos!</b>
         <p>Te informamos que ya cuentas con una solicitud calificada.</p>
         <p>Si aún no te ha contactado ninguno de nuestros
            ejecutivos y han pasado más de 24 hrs desde tu
            registro, por favor envíanos un correo a
            <br><b class="bold">documentaciondigital@financieramontedepiedad.com.mx</b>
            <br>para continuar con el proceso.</p>
      </div>
   </div>
</div>
