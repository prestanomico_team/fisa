<div id="statusSolicitud">
    <div class="modal__heading bg-blue">
        <h2 class="titulo-dinamico" id="encabezadoModal">{!! $tituloModal !!}</h2>
        <div class="triangle"><div class="triangle__form"></div></div>
    </div>
    <div class="modal__body">
        <div class="modal__emoji" id="imgModal">
            {!! $imgModal !!}
        </div>
        <div class="modal__text" id="cuerpoModal">
            {!! $cuerpoModal !!}
        </div>
        <div class="modal__btn">
            @if($continua == true)
            <button type="button">
                Continuar
            </button>
            @else
                @if(isset($redireccion))
                    @if($redireccion != '')
                    <a class="terminaFlujo" href="{{url($redireccion)}}">
                        Aceptar
                    </a>
                    @else
                    <a class="terminaFlujo" href="{{url()}}">
                        Aceptar
                    </a>
                    @endif
                @else
                <button type="button" id="terminaFlujo">
                    Aceptar
                </button>
                @endif
            @endif
        </div>
    </div>
</div>
