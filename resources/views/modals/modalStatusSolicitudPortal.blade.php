<div id="statusSolicitud">
    <div class="modal__heading bg-blue">
        <h2 class="titulo-dinamico" id="encabezadoModal">{!! $tituloModal !!}</h2>
        <div class="triangle"><div class="triangle__form"></div></div>
    </div>
    <div class="modal__body">
        <div class="modal__emoji" id="imgModal">
            {!! $imgModal !!}
        </div>
        <div class="modal__text" id="cuerpoModal">
            {!! $cuerpoModal !!}
        </div>
        <div class="modal__btn">
           
           
        <a class="terminaFlujo" href="/panel/producto/captura/cambaceo">
            Aceptar
        </a>
                   
        </div>
    </div>
</div>
