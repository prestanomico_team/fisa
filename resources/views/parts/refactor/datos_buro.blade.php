<div id="datos_buro" style="display:none">
    <form id="formDatosBuro" method="POST" action="/">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Autorización de consulta al Buró de Crédito</h2>
            <div class="calculadoras-productos">
                <small>
                    Para evaluar tu solicitud necesitamos consultar tus antecedentes crediticios.<br>
                    Para este fin, el Buró de Crédito nos pide tu respuesta a las siguientes preguntas.<br>
                    No es un requisito para nosotros que tengas tarjeta o cualquier otro tipo de crédito, pero sí es necesario <br>
                    que respondas de manera verdadera para que podamos obtener tu información del Buró.
                </small>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-xl-4 col-lg-4 col-sm-4 col-6 text-center" style="margin-bottom: 15px !important;">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12">
                        <span class="box-title credito_hipotecario-title">¿Cuentas con un crédito hipotecario a tu nombre?</span>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_hipotecario_si" name="credito_hipotecario" value="Si" class="custom-control-input required">
                        <label for="credito_hipotecario_si" class="custom-control-label">Si</label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_hipotecario_no" name="credito_hipotecario" value="No" class="custom-control-input required">
                        <label for="credito_hipotecario_no" class="custom-control-label">No</label>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12" style="margin-bottom: 15px !important;">
                        <p id="credito_hipotecario_descripcion_Si" style="text-align: justify; font-size:12px; display:none;"><b>SI</b> tengo actualmente a mi nombre, un crédito hipotecario, ya sea de banco, de INFONAVIT o de cualquier otro tipo.</p>
                        <p id="credito_hipotecario_descripcion_No" style="text-align: justify; font-size:12px; display:none;"><b>NO</b> tengo actualmente a mi nombre, un crédito hipotecario, ya sea de banco, de INFONAVIT o de cualquier otro tipo.</p>
                        <small id="credito_hipotecario-help" class="help"></small>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-4 col-6 text-center" style="margin-bottom: 15px !important;">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12">
                        <span class="box-title credito_automotriz-title">¿Has ejercido en los últimos dos años un crédito automotriz?</span>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_automotriz_si" name="credito_automotriz" value="Si" class="custom-control-input required">
                        <label for="credito_automotriz_si" class="custom-control-label">Si</label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_automotriz_no" name="credito_automotriz" value="No" class="custom-control-input required">
                        <label for="credito_automotriz_no" class="custom-control-label">No</label>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12" style="margin-bottom: 15px !important;">
                        <p id="credito_automotriz_descripcion_Si" style="text-align: justify; font-size:12px; display:none;"><b>SI</b> tengo actualmente, o <b>SI</b> tuve en los últimos 24 meses, un crédito automotriz con un banco o financiera. Confirmo que dicho crédito <b>NO</b> es, o <b>NO</b> fue de tipo autofinanciamiento como Autofin, Sicrea o similar.</p>
                        <p id="credito_automotriz_descripcion_No" style="text-align: justify; font-size:12px; display:none;"><b>NO</b> tengo actualmente, y <b>NO</b> tuve en los últimos 24 meses, un crédito automotriz con un banco o financiera. O bien, si financié mi auto, lo hice con un autofinanciamiento como Autofin, Sicrea o similar.</p>
                        <small id="credito_automotriz-help" class="help"></small>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-4 col-6 text-center">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12">
                        <span class="box-title credito_bancario-title">¿Eres titular de al menos una tarjeta de crédito bancaria?</span>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_bancario_si" name="credito_bancario" value="Si" class="custom-control-input required">
                        <label for="credito_bancario_si" class="custom-control-label">Si</label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-6 col-6">
                        <input type="radio" id="credito_bancario_no" name="credito_bancario" value="No" class="custom-control-input required">
                        <label for="credito_bancario_no" class="custom-control-label">No</label>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12" style="margin-bottom: 15px !important;">
                        <small id="credito_bancario-help" class="help"></small>
                        <p id="credito_bancario_descripcion_Si" style="text-align: justify; font-size:12px; display:none;"><b>SI</b> tengo actualmente al menos una tarjeta de crédito bancaria en la que yo soy el titular. Confirmo que ésta <b>NO</b> es una tarjeta de tienda comercial, y <b>NO</b> es una tarjeta de débito.</p>
                        <p id="credito_bancario_descripcion_No" style="text-align: justify; font-size:12px; display:none;"><b>NO</b> tengo actualmente ninguna tarjeta de crédito bancaria en la que yo sea el titular.  Mis tarjetas (si es que tengo) son de débito o de tienda comercial.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-4 col-lg-offset-8 col-xs-12">
                    <div class="col-lg-12 col-xs-12">
                        <div id="div_digitos_tarjeta" class="col-xl-12 col-lg-12 col-sm-12 col-12 text-center" style="display: none">
                            <label class="box-title" style="font-size: 14px;">Ingresa los últimos 4 dígitos de tu tarjeta</label>
                            <input type="text" id="digitos_tarjeta" name="digitos_tarjeta" maxlength="4" style="max-width: 125px; text-align:center; font-size: 14px;">
                            <p id="descripcion_digitos" style="text-align: justify; font-size:10px; display:none; margin-top: 10px;"><b>Estos son los 4 últimos dígitos del número de mi tarjeta de crédito, de la cual yo soy el titular y se encuentra vigente. El número de mi tarjeta no ha cambiado desde que la obtuve (por razones de reemplazo, o bien, por robo o extravío).</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-12 col-xs-12" style="margin: 50px 10px 10px 10px">
                    <label>
                        Hoy siendo @php echo Carbon\Carbon::now()->format('d/m/Y'); @endphp autorizo a Financiera Monte de Piedad a consultar mis antecedentes crediticios por única ocasión ante las Sociedades de Información
                        Crediticia que estime conveniente, declarando que conozco la naturaleza, alcance y uso que Financiera Monte de Piedad hará de tal información.
                    </label>
                    <div class="form-check">
                        <input class="form-check-input required" type="checkbox" value="acepto" id="acepto_consulta" name="acepto_consulta" v-model="acepto_consulta">
                        <label id="label_autorizo" class="form-check-label">Autorizo</label>
                    </div>
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesDatosBuro"></label>
                </div>
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 calculadora-personal mt-0">
                        <div class="text-right">
                            <a class="general-button" onclick="datos_buro()"><span>Siguiente</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
