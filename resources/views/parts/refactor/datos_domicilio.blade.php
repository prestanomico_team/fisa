<div id="datos_domicilio" style="display:none">
    <form id="formDatosDomicilio">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Solicitud de crédito</h2>
            <div class="calculadoras-productos"><small>Cuéntanos dónde vives.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="calle" name="calle" type="text" placeholder="Calle" maxlength="35" class="required uppercase">
                    <small id="calle-help" class="help"></small>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="no_exterior" name="no_exterior" type="text" placeholder="No. Ext." maxlength="5" class="required uppercase">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="no_interior" name="no_interior" type="text" placeholder="No. Int." maxlength="5" class="uppercase">
                    <small id="no_interior-help" class="help"></small>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="codigo_postal" name="codigo_postal" type="text" placeholder="C.P." maxlength="5" onkeyup="getCP(this)" class="required">
                    <small id="codigo_postal-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="select_colonia" name="select_colonia" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Ingrese Código Postal</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="delegacion" name="delegacion" type="text" placeholder="Delegación / Municipio" disabled="disabled" class="uppercase">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="ciudad" name="ciudad" type="text" placeholder="Ciudad" disabled="disabled" class="uppercase">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="estado" name="estado" type="text" placeholder="Estado" disabled="disabled" class="uppercase">
                </div>
                <input type="hidden" id="colonia" name="colonia" class="uppercase">
                <input type="hidden" id="id_colonia" name="id_colonia" class="">
                <input type="hidden" id="id_deleg_munic" name="id_deleg_munic" class="">
                <input type="hidden" id="id_estado" name="id_estado" class="">
                <input type="hidden" id="codigo_estado" name="codigo_estado" class="">
                <input type="hidden" id="cobertura" name="cobertura" class="">
                <input type="hidden" id="id_ciudad" name="id_ciudad" class="">
            </div>
        </div>
        <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
            <label id="validacionesDomicilio"></label>
        </div>
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 calculadora-personal mt-0">
                <div class="text-right">
                    <a class="general-button" onclick="datos_domicilio()"><span>Siguiente</span></a>
                </div>
            </div>
        </div>
    </form>
</div>
