<div id="datos_ingreso" style="display:none">
    <form id="formDatosIngreso">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Solicitud de crédito</h2>
            <div class="calculadoras-productos"><small>Cuéntanos sobre tu situación financiera.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="ocupacion" name="ocupacion" aria-required="true" aria-invalid="false" required="required">
                                    <option value="" disabled="" selected="selected">Ocupación*</option>
                                    <option value="EMPLEADO SECTOR PÚBLICO">Empleado Sector Público</option>
                                    <option value="EMPLEADO SECTOR PRIVADO">Empleado Sector Privado</option>
                                    <option value="NEGOCIO PROPIO">Negocio Propio</option>
                                    <option value="PROFESIONAL INDEPENDIENTE">Profesional Independiente</option>
                                    <option value="ARRENDADOR">Arrendador</option>
                                    <option value="PENSIONADO">Pensionado</option>
                                    <option value="JUBILADO">Jubilado</option>
                                    <option value="OTRO">Otro</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="ocupacion-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="fuente_ingresos" name="fuente_ingresos" aria-required="true" aria-invalid="false" required="required">
                                    <option value="" disabled="" selected="selected">Fuente de ingresos*</option>
                                    <option value="Salario">Salario</option>
                                    <option value="Honorarios">Honorarios</option>
                                    <option value="Renta">Renta</option>
                                    <option value="Inmuebles">Inmuebles</option>
                                    <option value="Pensión">Pensión</option>
                                    <option value="Jubilación">Jubilación</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="fuente_ingresos-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal">
                    <input id="ingreso_mensual" name="ingreso_mensual" type="text" placeholder="Ingreso mensual comprobable*" class="required">
                    <small id="ingreso_mensual-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="numero_dependientes" name="numero_dependientes" aria-required="true" aria-invalid="false" required="required">
                                    <option value="" disabled="" selected="selected">Dependientes económicos*</option>
                                    <option value="0">0</option>
        		                    <option value="1">1</option>
        		                    <option value="2">2</option>
        		                    <option value="3">3</option>
        		                    <option value="4">4</option>
        		                    <option value="5">5</option>
        		                    <option value="6">6</option>
        		                    <option value="7">7</option>
        		                    <option value="8">8</option>
        		                    <option value="9">9</option>
        		                    <option value="10">10</option>
        		                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="numero_dependientes-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="residencia" name="residencia" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Residencia*</option>
                                    <option value="PROPIA">Propia</option>
                                    <option value="RENTA">Renta</option>
                                    <option value="CON FAMILIARES">Con Familiares</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="residencia-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="antiguedad_domicilio" name="antiguedad_domicilio" aria-required="true" aria-invalid="false" required="required">
                                    <option value="" disabled="" selected="selected">Antigüedad en domicilio*</option>
                                    <option value="0">Menos de 1 año</option>
        		                    <option value="1">1 año</option>
        		                    <option value="2">2 años</option>
        		                    <option value="3">3 años</option>
        		                    <option value="4">4 años</option>
        		                    <option value="5">5 años</option>
        		                    <option value="6">6 años</option>
        		                    <option value="7">7 años</option>
        		                    <option value="8">8 años</option>
        		                    <option value="9">9 años</option>
        		                    <option value="10">10 años</option>
        		                    <option value="11">Más de 10 años</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="antiguedad_domicilio-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="gastos_mensuales" name="gastos_mensuales" type="text" placeholder="Monto gastos familiares mensuales*" class="required">
                    <small id="gastos_mensuales-help" class="help"></small>
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesDatosIngreso"></label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 calculadora-personal mt-0">
                    <div class="text-right">
                        <a class="general-button" onclick="datos_ingreso()"><span>Siguiente</span></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
