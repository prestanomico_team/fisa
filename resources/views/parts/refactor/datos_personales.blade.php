<div id="datos_personales" style="display:none">
    <form id="formDatosPersonales">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Solicitud de crédito</h2>
            <div class="calculadoras-productos"><small>Cuéntanos más sobre ti.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="sexo" name="sexo" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Sexo*</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal fecha_nacimiento">
                    <input id="fecha_nacimiento" name="fecha_nacimiento" type="text" placeholder="Fecha de nacimiento" data-format="DD-MM-YYYY" data-template="YYYY-MMM-DD" class="required">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="estado_nacimiento" name="estado_nacimiento" aria-required="true" aria-invalid="false" onchange="cambioEstadoNacimiento()">
                                    <option value="" disabled="" selected="selected">Estado de nacimiento*</option>
                                    @php
    									use App\Sepomex;
    									$estados = Sepomex::selectRAW('distinct(estado)')
    										->orderBy('estado')
    										->pluck('estado');
    								@endphp
    								@foreach($estados as $estado)
    									<option value="{{ $estado }}">{{ $estado }}</option>
    								@endforeach
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="ciudad_nacimiento" name="ciudad_nacimiento" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Ciudad de nacimiento*</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="telefono_casa" name="telefono_casa" type="text" placeholder="Telefono de casa *10 dígitos" maxlength="10" class="required">
                    <small id="telefono_casa-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="estado_civil" name="estado_civil" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Estado civil*</option>
                                    <option value="S">Soltero</option>
                                    <option value="F">Unión Libre</option>
                                    <option value="M">Casado</option>
                                    <option value="D">Divorciado</option>
                                    <option value="W">Viudo</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="nivel_estudios" name="nivel_estudios" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Nivel de estudios*</option>
                                    <option value="Primaria">Primaria</option>
                                    <option value="Secundaria">Secundaria</option>
                                    <option value="Preparatoria">Preparatoria</option>
                                    <option value="Licenciatura">Licenciatura / Ingeniería</option>
                                    <option value="Posgrado">Posgrado</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="rfc" name="rfc" type="text" placeholder="R.F.C." maxlength="13" class="uppercase">
                    <input id="error_rfc" name="error_rfc" type="hidden" class="uppercase">
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesDatosPersonales"></label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 calculadora-personal mt-0">
                    <div class="text-right">
                        <a class="general-button" onclick=""><span>Regresar</span></a>
                        <a class="general-button" onclick="datos_personales()"><span>Siguiente</span></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
