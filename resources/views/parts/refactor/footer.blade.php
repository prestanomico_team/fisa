
<div class="row page-view page-product no-margin">
	<div id="loginModal" class="modal fade" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-dialog-centered modal-login modal-sm ">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h2 class="titulo-dinamico">Iniciar Sesión</h2>
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        </div>
		        <div class="modal-body">
		            <form id="formLogin">
		                <div class="calculadoras-productos">
		                    <i class="fa fa-user"></i>
		                    <input id="email_login" type="text" class="pre-registro-input" placeholder="Correo Electrónico" autocomplete="new-email" name="email" required="required">
		                    <small id="email_login-help" class="help"></small>
		                </div>
		                <div class="calculadoras-productos">
		                    <i class="fa fa-lock"></i>
		                    <input id="password_login" type="password" class="pre-registro-input" placeholder="Contraseña" autocomplete="new-password" name="password" required="required">
		                    <small id="password_login-help" class="help"></small>
		                </div>
		                <div class="form-group">
		                    <input id="buttonLogin" type="button" onclick="iniciarSesion()" class="btn swal2-styled" value="Iniciar Sesión">
		                </div>
		            </form>
		            <span id="loginError" class="error" style="border-bottom: none !important; font-size: 13px;"></span>
		        </div>
		        <div class="modal-footer">
		            <a href="/password-restore" id="olvidePassword"><b>Olvide mi contraseña</b></a>
		        </div>
		    </div>
		</div>
	</div>
	<footer id="footer">
		<ul class="footer-links">
			<div class="container" style="text-align: justify; position: relative;">
				<li><a href="/aviso-privacidad" target="_blank" tabindex="-1">Aviso de Privacidad</a></li>
				<li><a href="/derechos-arco" target="_blank" tabindex="-1">Derechos ARCO</a></li>
				<li><a href="https://financieramontedepiedad.com.mx/pdf/padron_despachos_de_cobranza.pdf" target="_blank" tabindex="-1">Despachos de Cobranza</a></li>
				<li><a href="https://nmp.facturehoy.com/BFIFIN/index.htm?id=2" target="_blank" tabindex="-1">Facturación</a></li>
				<li><a href="https://financieramontedepiedad.com.mx/sistema-de-remuneracion/" target="_blank" tabindex="-1">Sistema de Remuneración</a></li>
				<li><a href="https://financieramontedepiedad.com.mx/estados-financieros" target="_blank" tabindex="-1">Estados Financieros</a></li>
				<!--li><a href="https://financieramontedepiedad.com.mx/buro-de-entidades-financieras">Buró de Entidades Financieras</a></li-->
				<li><a href="https://financieramontedepiedad.com.mx/quejas-y-aclaraciones/" target="_blank" tabindex="-1">Quejas y Aclaraciones(UNE)</a></li>
				<li><a href="https://financieramontedepiedad.com.mx/contratos/" target="_blank" tabindex="-1">Contratos</a></li>
				<li style="width: calc(100% - 20px); display: inline-block;">
					<a href="https://financieramontedepiedad.com.mx/pdf/codigo_de_etica_fmp.pdf" tabindex="-1" target="_blank">Código de ética</a>
					<a href="https://financieramontedepiedad.com.mx/informacion-costos-y-comisiones/" style="float:right;" tabindex="-1" target="_blank">Consulta los costos y comisiones de nuestros productos</a>
				</li>
			</div>
		</ul>
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 txt-center">
				<h4>Financiera Monte de Piedad, S.A de C.V., S.F.P</h4>
				<p>
					Blvd. Manuel Ávila Camacho No. 32, Col. Lomas de Chapultepec III Sección, C.P. 11000<br>
					Ciudad de México<br>
				</p>
				<h4>Unidad Especializada de Atención a Usuarios (UNE)</h4>
				<p>
					(55) 5269 5202 <br>
					<span style="padding: 5px; display: block;"><img src="/images/icons/whatsappb.png" height="18"> 55 7499 1178<br></span>
					<a href="mailto:une@financieramontedepiedad.com.mx" tabindex="-1">une@financieramontedepiedad.com.mx</a><br>
				</p>
				<p class="min-text">
					Cualquier acción violatoria a nuestro <a href="https://financieramontedepiedad.com.mx/pdf/codigo_de_etica.pdf" tabindex="-1" target="_blank">Código de Ética</a>, leyes y reglamentos comunícate a:<br>
					800-885-4632<br>
					<small>(800 - TU - LINEA)</small><br>
					<a href="mailto:tulineaetica@tipsanonimos.com">tulineaetica@tipsanonimos.com</a><br>
					<a href="https://www.tipsanonimos.com/tulineaetica">https://www.tipsanonimos.com/tulineaetica</a><br>
				</p>
				<br>
			</div>

		</div>

		<div class="footer-brands">
			<a href="https://www.gob.mx/condusef" tabindex="-1" target="_blank">
				<img src="/images/icons/logo-condusef.png" alt="Condusef">
			</a>
			<a href="https://financieramontedepiedad.com.mx/quejas-y-aclaraciones/" tabindex="-1" target="_blank">
				<img src="/images/icons/logo-une.png" alt="Unidad especializada de atención a cliente">
			</a>
			<a href="https://www.gob.mx/cnbv" tabindex="-1" target="_blank">
				<img src="/images/icons/logo-cnbb.png" alt="Comisión Nacional Bancaria de Valores">
			</a>
			<a href="https://financieramontedepiedad.com.mx/buro-de-entidades-financieras" tabindex="-1" target="_blank">
				<img src="/images/icons/logo-buro.png" alt="Condusef">
			</a>
			<a href=" http://www.banxico.org.mx/CAT/" target="_blank" tabindex="-1" target="_blank">
				<img src="/images/icons/logo-banco-mexico.jpg" alt="Banco de México">
			</a>
		</div>
	</footer>
	<script src="https://widget.sirena.app/get?token=40ca3dc168a24e499c3a317658e6d916"></script>
	<script type="text/javascript" src='/js/librerias.js'></script>
	<script type="text/javascript" src="/js/all.js?v=<?php echo microtime(); ?>"></script>
	<script type="text/javascript" src='/js/password_restore.js?v=<?php echo microtime(); ?>'></script>
	<script type="text/javascript" src='/js/cookie_calificas.js?v=<?php echo microtime(); ?>' charset="utf-8"></script>

	<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
	<script type="text/javascript">
		$(document).ready(function(){
			productsSlider();
			var clientId = 0;
	        ga(function(tracker) {
	            clientId = tracker.get('clientId');
				dataLayer.push({'clientId':clientId});
	        });
		});
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="js/lib/respond.js"></script>
	<script type="text/javascript" src="js/lib/modernizr.js"></script>
	<script type="text/javascript">
	  Modernizr.load({
	    test: Modernizr.geolocation,
	    yep : 'geo.js',
	    nope: 'geo-polyfill.js'
	  });
	</script>
	<![endif]-->
	<script type="text/javascript" src="js/calculadora-prestamo.js"></script>
	<script src="//tt.mbww.com/tt-8b0f03858cd08f401a7ffc4e119bc12bff1e50cacb1328ec913f506598720c0a.js" async></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!--
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129520248-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-129520248-1'); </script> -->
</div>
