<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Facebook tagging -->
	<meta property="fb:app_id" content=""/>
	<meta property="og:type"   content="website" />
	<meta property="og:url"    content="https://financieramontedepiedad.com.mx" />
	<meta property="og:title"  content="Financiera Monte de Piedad | Crédito Personal"/>
	<meta property="og:image"  content="" />
	<meta property="og:description"  content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades."/>
	<!-- End of facebook tagging -->
	<link rel="shortcut icon" href="images/brand/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta name="description" content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades.">
	<meta name="keywords" content="">
	<style>
	:focus {
		/* outline: 0 !important; */
	}
	</style>
	<link type="text/css" rel="stylesheet" href="/css/all_css.css?v=<?php echo microtime();?>">
	<link rel="canonical" href="https://solicitaloahora.financieramontedepiedad.com.mx/" />
	<link rel="stylesheet" href="/css/bootstrap-slider.css">
	<script src="/js/bootstrap-slider.js"></script>

	<title>Financiera Monte de Piedad | Crédito Personal</title>
	<link rel="icon" type="image/png" href="/images/brand/favicon.png">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/renovaciones">
					<img src="images/brand/financiera_monte_de_piedad_logo.png" alt="Financiera Monte de Piedad" title="">
				</a>
				@if (!Auth::guard('prospecto')->check())
				<button class="solicitaloahora_button" onclick="focusFormulario()" id="goFormularioRenovaciones">Solicitalo Ahora</button>
				@endif
			</div>

			<div id="navbar" class="navbar-collapse collapse">
				@if (Auth::guard('prospecto')->check())
				<ul class="nav navbar-nav navbar-right mt-4">
					<li class="nav-item dropdown">
				        <a class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('prospecto')->user()->nombres }} {{ Auth::guard('prospecto')->user()->apellido_paterno }} {{ Auth::guard('prospecto')->user()->apellido_materno }}</a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a>
				        </div>
			      	</li>
				</ul>
                @else
					<ul class="nav navbar-nav navbar-right mt-4">
						<li id="linkNoSesion"><a class="bold" href="#" data-toggle="modal" data-target="#loginModal">Iniciar sesión</a></li>
						<li id="linkSesion" style="display:none" class="nav-item dropdown">
					        <a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a>
					        </div>
				      	</li>
					</ul>
                @endif
			</div>
		</div>
	</nav>

</header>
