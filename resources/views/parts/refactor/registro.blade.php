@include('aviso-privacidadV2')
<div id="registro" style="display:none">
    <form id="formRegistro">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Registro</h2>
            <div class="calculadoras-productos"><small>Abre tu cuenta y sé parte de Financiera Monte de Piedad.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-12">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="nombres" name="nombres" type="text" maxlength="50" placeholder="Nombre" class="required uppercase">
                        <small id="nombres-help" class="help"></small>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="apellido_paterno" name="apellido_paterno" maxlength="50" type="text" placeholder="Apellido paterno" class="required uppercase">
                        <small id="apellido_paterno-help" class="help"></small>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="apellido_materno" name="apellido_materno" maxlength="50" type="text" placeholder="Apellido materno" class="uppercase">
                        <small id="apellido_materno-help" class="help"></small>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="email" name="email" type="text" maxlength="100" placeholder="Email" class="required lowercase" autocomplete="new-email">
                        <small id="email-help" class="help"></small>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="celular" name="celular" maxlength="10" type="text" placeholder="Telefono celular *10 dígitos" class="required uppercase">
                        <small id="celular-help" class="help"></small>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                        <input id="contraseña" name="contraseña" maxlength="25" type="password" placeholder="Crea tu contraseña" class="required" autocomplete="new-password">
                        <small id="contraseña-help" class="help"></small>
                        <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Minímo 8 caracteres</div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal" style="margin-bottom: 20px;">
                        <input id="confirmacion_contraseña" maxlength="25" name="confirmacion_contraseña" type="password" placeholder="Confirma tu contraseña" class="required" autocomplete="new-password">
                        <small id="confirmacion_contraseña-help" class="help"></small>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 text-left" style="margin: 20px 0px 0px 0px">
                    <input type="checkbox" id="acepto_avisotyc" name="acepto_avisotyc" class="required" value="1"/>
                    <a href="#" data-toggle="modal" data-target="#acepto_avisotyc" id="acepto_avisotyc" style="color: black; text-decoration: underline; font-style:normal;">
                        Acepto Términos y Condiciones del Aviso de Privacidad de FINANCIERA MONTE DE PIEDAD, S.A DE C.V., S.F.P.
                    </a>
                    <p id="acepto_avisotyc-error" class="msg-error"></p>

                </div>
                <div class="col-xs-12 text-left">
                    <small>Te enviaremos un código de verificación vía SMS para validar tu registro</small>
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesRegistro"></label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 calculadora-personal mt-0">
                    <div class="text-right">
                        <a class="general-button" onclick="getForm('simulador', 'registro')"><span>Regresar</span></a>
                        @if ($configuracion['configuracion']['alias'] == 'sindicalizados')
                            <a class="general-button" onclick="registroProspectoSindicalizado()"><span>Enviar</span></a>
                        @else
                            <a class="general-button" onclick="registroProspecto()"><span>Enviar</span></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
