
 <div class="registro__first-block-footer" id="restaurar_contraseña" style="display:none">
    <div class="col-xs-12">
        <h2 class="secondary-title txt-center">Actualizar contraseña</h2>
        <div class="calculadoras-productos"><small>Si tienes una cuenta con <b>Financiera Monte de Piedad</b>, te hemos enviado un SMS al celular <b id="celular_enviado"></b> con un código de verificación.</small></div>
        <div class="calculadoras-productos"><small>Ingresa ese código en el campo Código de verificación y tu nueva contraseña, después da click en actualizar contraseña</b></small></div>
    </div>
    <div class="row">
        <form id="form-restaurar_password">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-4 col-lg-offset-6 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 calculadora-personal">
                    <input type="text" placeholder="Código de verificación" id="codigo_verificacion" name="codigo_verificacion" class="required only_numbers" maxlength="6" style="text-align:center"/>
                    <input type="hidden" id="email_usuario" name="email_usuario"/>
                    <small id="codigo_verificacion-help" class="help"></small>
                </div>
            </div>
            <div class="col-lg-9 col-lg-offset-3 col-md-offset-2 col-xs-12">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
               <input id="new_password" name="new_password" maxlength="25" type="password" placeholder="Crea tu contraseña" class="required" autocomplete="new-password">
                <small id="new_password" class="help"></small>
                <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="Ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
                <div class="col-12 helperPassword" style="font-size:12px;"><span id="Lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
                <div class="col-12 helperPassword" style="font-size:12px;"><span id="Num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
                <div class="col-12 helperPassword" style="font-size:12px;"><span id="char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Minímo 8 caracteres</div>
            </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input type="password" placeholder="Confirma tu nueva contraseña" id="confirm_password" maxlength="50" name="confirm_password" class="required password" autocomplete="new-password"/>
                    <div class="linea-dato"></div>
                    <small id="confirm_password-help" class="help"></small>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-xs-12">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                <div class="text-center">
                    <a class="general-button" onclick="actualiza_contraseña()"><span>Actualizar contraseña</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
