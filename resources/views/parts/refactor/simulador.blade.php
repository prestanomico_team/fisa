<div id="simulador">
    <form id="formSimulador">
    @if ($configuracion['configuracion']['alias'] == 'sindicalizados')
        <div class="col-lg-5 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 calculadora-personal mb-0">
            <h2 class="secondary-title txt-center">Calcula tu préstamo</h2>
            <div class="calculadoras-productos">
                <small>Los campos marcados con (*) son obligatorios.</small>
                <div>
                    <input name="monto_prestamo" id="monto_prestamo" class="pre-registro-input" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraSindicalizados()"
                        data-thousands="," data-decimal="." data-prefix="$ "
                        placeholder="Ingresa monto entre ${{ number_format($configuracion['configuracion']['monto_minimo'], 0, '.', ',') }} y ${{ number_format($configuracion['configuracion']['monto_maximo'], 0, '.', ',') }}"
                        tabindex="1">
                    <input type="hidden" id="monto_minimo" name="monto_minimo" value="{{ $configuracion['configuracion']['monto_minimo'] }}"></input>
                    <input type="hidden" id="monto_maximo" name="monto_maximo" value="{{ $configuracion['configuracion']['monto_maximo'] }}"></input>
                </div>
                <div class="withDecoration">
                    <span>
                        <select class="pre-registro-input" name="plazo" id="plazo" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraSindicalizados()" tabindex="2">
                            <option value="" disabled="" selected="selected">Selecciona el plazo de pago*</option>
                            @foreach ($configuracion['plazos'] as $id => $plazo)
								<option value="{{ $id }}">{{ $plazo }}</option>
							@endforeach
                        </select>
                    </span>
                    <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                </div>
                <div class="withDecoration">
                    <span>
                        <select class="pre-registro-input" name="finalidad" id="finalidad" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraSindicalizados()" tabindex="3">
                            <option value="" disabled="" selected="selected">Selecciona la finalidad*</option>
                            @foreach ($configuracion['finalidades'] as $id => $finalidad)
                                <option value="{{ $id }}">{{ $finalidad }}</option>
                            @endforeach
                        </select>
                    </span>
                    <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                </div>
                <div class="col col-xs-10 col-xs-offset-1">
                    <p class="txt-center"><big><strong>Tu pago fijo mensual: <br><span id="prestamoPersonalPagoFIjo">$0.00</span></strong></big></p>
                    <input type="hidden" id="pago_estimado" name="pago_estimado"></input>
                    @if(Session::has('nueva_solicitud') === true)
                    <input type="hidden" id="nueva_solicitud" name="nueva_solicitud" value="true"></input>
                    @else
                    <input type="hidden" id="nueva_solicitud" name="nueva_solicitud" value="false"></input>
                    @endif
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-5 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 calculadora-personal mb-0">
            <h2 class="secondary-title txt-center">Calcula tu préstamo</h2>
            <div class="calculadoras-productos">
                <small>Los campos marcados con (*) son obligatorios.</small>
                <div>
                    <input name="monto_prestamo" id="monto_prestamo" class="pre-registro-input" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraPersonal()"
                        data-thousands="," data-decimal="." data-prefix="$ "
                        placeholder="Ingresa monto entre ${{ number_format($configuracion['configuracion']['monto_minimo'], 0, '.', ',') }} y ${{ number_format($configuracion['configuracion']['monto_maximo'], 0, '.', ',') }}"
                        tabindex="1">
                    <input type="hidden" id="monto_minimo" name="monto_minimo" value="{{ $configuracion['configuracion']['monto_minimo'] }}"></input>
                    <input type="hidden" id="monto_maximo" name="monto_maximo" value="{{ $configuracion['configuracion']['monto_maximo'] }}"></input>
                </div>
                <div class="withDecoration">
                    <span>
                        <select class="pre-registro-input" name="plazo" id="plazo" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraPersonal()" tabindex="2">
                            <option value="" disabled="" selected="selected">Selecciona el plazo de pago*</option>
                            @foreach ($configuracion['plazos'] as $id => $plazo)
								<option value="{{ $id }}">{{ $plazo }}</option>
							@endforeach
                        </select>
                    </span>
                    <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                </div>
                <div class="withDecoration">
                    <span>
                        <select class="pre-registro-input" name="finalidad" id="finalidad" aria-required="true" aria-invalid="false" required="required" onchange="changeCalculadoraPersonal()" tabindex="3">
                            <option value="" disabled="" selected="selected">Selecciona la finalidad*</option>
                            @foreach ($configuracion['finalidades'] as $id => $finalidad)
                                <option value="{{ $id }}">{{ $finalidad }}</option>
                            @endforeach
                        </select>
                    </span>
                    <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                </div>
                <div class="col col-xs-10 col-xs-offset-1">
                    <p class="txt-center"><big><strong>Tu pago fijo mensual: <br><span id="prestamoPersonalPagoFIjo">$0.00</span></strong></big></p>
                    <input type="hidden" id="pago_estimado" name="pago_estimado"></input>
                    @if(Session::has('nueva_solicitud') === true)
                    <input type="hidden" id="nueva_solicitud" name="nueva_solicitud" value="true"></input>
                    @else
                    <input type="hidden" id="nueva_solicitud" name="nueva_solicitud" value="false"></input>
                    @endif
                </div>
            </div>
        </div>
        @endif
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 calculadora-personal mb-0 finalidad_custom">
            <h2 class="secondary-title txt-center">Nos gustaría conocerte</h2>
            <div class="calculadoras-productos">
                <small>Cuéntanos un poco más acerca del préstamo que estás buscando.</small>
                <textarea id="finalidad_custom" name="finalidad_custom" rows="5" tabindex="4" class="uppercase"></textarea>
            </div>
            <small id="finalidad_custom-help" class="help"></small>
        </div>
        <div class="col-xs-12 txt-center help">
            <label id="validacionesSimulador"></label>
        </div>
        <div class="col-xs-12 calculadoras-productos txt-center">
            <a class="general-button" onclick="validarSimulador()" tabindex="5"><span>Inicia tu solicitud ahora</span></a>
        </div>
    </form>
</div>
