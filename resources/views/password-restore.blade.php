@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col col-sm-12 col-md-12 container product-header">
        <div class="item">
            <div class="slider-overlay"> </div>
            <img class="img-responsive" src="images/main/prestamo-personal.png" alt="Préstamo Personal">
            <div class="slider-info-block principal-title">
                <h2 class="slider-title alt-title">Préstamo<br>Personal</h2>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <br><br>
        <h2 class="secondary-title txt-center">¿Qué es el Préstamo Personal?</h2>
        <div class="page-product-description txt-center">
            <p><strong>Es un préstamo</strong> diseñado a tu medida con pagos fijos mensuales con el único propósito de ayudar a conseguir tus objetivos.
                <strong>Solicita tu crédito</strong> y construye un mejor futuro para ti y tu familia.</p> <br>
        </div>
    </div>
</div>
<div class="row">
    <h2 class="secondary-title txt-center">¿Cómo funciona?</h2>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_1.png">
            <p><strong>1. Completa tu solicitud</strong></p>
            <p>Todo nuestro proceso y soporte es en línea, sin necesidad de ir a sucursal.</p>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_2.png">
            <p><strong>2. Prepara tus documentos</strong></p>
            <p>Identificacion oficial, comprobante de domicilio y comprobante de ingresos.</p>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="page-product-description txt-center">
            <img src="images/icons/step_3.png">
            <p><strong>3. Recibe tu dinero</strong></p>
            <p>Haremos un depósito seguro a la cuenta de tu elección.</p>
        </div>
    </div>
</div>
<div class="row page-view gray">
    <div id="password-restore">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Restablecer contraseña</h2>
            <div class="calculadoras-productos"><small>¿Olvidaste tu contraseña?</small></div>
            <div class="calculadoras-productos"><small>No te preocupes, reestablecer tu contraseña es muy fácil.<br>Solo ingresa el correo electrónico con el que estás registrado en <b>Financiera Monte de Piedad</b></small></div>
        </div>
        <div class="registro__first-block-footer" >
            <form id="form-password-restore">
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <div class="col-12">
                            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset- col-xs-12 calculadora-personal">
                                <input id="email" name="email" type="text" maxlength="100" placeholder="Email" class="required lowercase" autocomplete="new-email">
                                <small id="email-help" class="help"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                        <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                            <div class="text-center">
                                <a class="general-button" onclick="emailPasswordRestore()"><span>Enviar</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="registro__first-block-footer" id="temporary-password" style="display:none">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Restablecer contraseña</h2>
            <div class="calculadoras-productos"><small>Si tienes una cuenta con <b>Financiera Monte de Piedad</b>, te hemos enviado un SMS al celular <b id="celular_enviado"></b> con un código de verificación.</small></div>
            <div class="calculadoras-productos"><small>Ingresa ese código en el campo Código de verificación y tu nueva contraseña, después da click en actualizar contraseña</b></small></div>
        </div>
        <div class="row">
            <form id="form-temporary-password">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                    <div class="col-lg-4 col-lg-offset-6 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 calculadora-personal">
                        <input type="text" placeholder="Código de verificación" id="codigo_verificacion" name="codigo_verificacion" class="required only_numbers" maxlength="6" style="text-align:center"/>
                        <input type="hidden" id="email_usuario" name="email_usuario"/>
                        <small id="codigo_verificacion-help" class="help"></small>
                    </div>
                </div>
                <div class="col-lg-9 col-lg-offset-3 col-md-offset-2 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                        <input type="password" placeholder="Crea tu nueva contraseña" id="new_password" maxlength="50" name="new_password" class="required password" autocomplete="new-password"/>
                        <div class="linea-dato"></div>
                        <small id="new_password-help" class="help"></small>
                        <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
    					<div class="col-12 helperPassword" style="font-size:12px;"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Minímo 8 caracteres</div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                        <input type="password" placeholder="Confirma tu nueva contraseña" id="confirm_password" maxlength="50" name="confirm_password" class="required password" autocomplete="new-password"/>
                        <div class="linea-dato"></div>
                        <small id="confirm_password-help" class="help"></small>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                    <div class="text-center">
                        <a class="general-button" onclick="validateTemporaryPassword()"><span>Actualizar contraseña</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="registro__first-block-footer" id="password-update" style="display:none">
        <div class="col-xs-12" id="password-restore">
            <h2 class="secondary-title txt-center">Actualizar contraseña</h2>
            <div class="calculadoras-productos"><small>Introduce tu nueva contraseña.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <form id="form-password-update">

                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                    <div class="text-center">
                        <a class="general-button" onclick="passwordUpdate()"><span>Actualizar contraseña</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="registro__first-block-footer" id="password-update-ok" style="display:none">
        <div class="col-xs-12" id="password-restore">
            <h2 class="secondary-title txt-center">Contraseña actualizada</h2>
        </div>
        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="text-align:center">
            <p>La contraseña se ha actualizado con éxito.</p>
            <p>Ya puedes ingresar con tu nueva contraseña.</p>
            <p>Asegurate de memorizarla o anotarla en un lugar seguro.</p>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                    <div class="text-center">
                        <a class="general-button" onclick="iniciarSesionRP()"><span>Iniciar sesión</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-10 col-xs-offset-1 calculadoras-productos">
        <div class="legal">
            <br><br>
            <p>CAT promedio 56% Tasa de interés mensual 4.79%/ Tasa de interés anual promedio: 57.50%. Las cantidades, tasas de interés, fechas, plazos y demás referencias que se señalan en este documento son meramente informativos para el día en que
                se expide y para la persona a quien se le formula la estimación, sin que genere obligación a cargo de FINANCIERA MONTE DE PIEDAD, S.A DE C.V., S.F.P</p>
        </div>
    </div>
</div>
<div class="col-xs-10 col-xs-offset-1">
    <h2 class="secondary-title txt-center uppercase">¿Dudas en el proceso? <span class="black lowercase">Contáctanos:</span></h2>
</div>
<div class="col-lg-2 col-md-2 col-md-offset-4 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
    <div class="page-product-description txt-center">
        <img src="images/icons/whatsapp.png">
        <p>55 4163 4806</p>
    </div>
</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
    <div class="page-product-description txt-center">
        <img src="images/icons/phone.png">
        <p>55 5206 3809</p>
    </div>
</div>
</div><br>
@endsection
