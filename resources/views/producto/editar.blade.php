@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
<link rel="stylesheet" href="/back//css/multi-select.css">
<link rel="stylesheet" href="/back/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<style>
.box.box-primary {
	border-top-color: #f79020;
}
label {
	text-transform: none;
}

.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;
    color: #333;
    background-color: #fff;
    border-color: #ccc;
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
.popover-content {
	background-color: #eaeaea;
}
.help-block {
    color: #dd4b39;
}
.alert-warning {
    color: #856404 !important;
    background-color: #fff3cd !important;
    border-color: #ffeeba !important;
}
.alert {
    position: relative;
    padding: .75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}
</style>

<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border" style="text-align:center;">
	            	<h3 class="box-title" style="font-weight: bold;">Modificar producto</h3>
	            </div>
				<div class="box-body">
					<form role="form" id="datos_producto">
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
								<a href="/productos/{{ $producto->id }}/cobertura" class="btn btn-primary"> Plazas Cobertura </a>
							</div>

							<hr>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Datos del producto</h3>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							                  	<label class="control-label">Nombre del producto</label>
												<input type="hidden" id="producto_id" name="producto_id" value="{{ $producto->id }}">
												<input type="text" class="form-control" id="nombre_producto" name="nombre_producto" value="{{ $producto->nombre_producto }}">
												<span id="error_nombre_producto" class="help-block"></span>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<label class="control-label">Tipo de producto</label>
												<select class="form-control" id="tipo" name="tipo">
													<option value="" disabled selected>Selecciona</option>
													<option value="Mercado abierto" @if($producto->tipo == 'Mercado abierto') selected @endif>Mercado abierto</option>
													<option value="Convenio" @if($producto->tipo == 'Convenio') selected @endif>Convenio</option>
													<option value="Nómina" @if($producto->tipo == 'Nómina') selected @endif>Nómina</option>
													<option value="Consumo" @if($producto->tipo == 'Consumo') selected @endif>Consumo</option>
												</select>
												<span id="error_tipo" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-8" id="div_empresa" @if($producto->tipo != 'Nómina' && $producto->tipo != 'Convenio') style="display:none" @endif>
												<label class="control-label">Nombre de la empresa</label>
												<input type="text" class="form-control" id="empresa" name="empresa" value="{{ $producto->empresa }}" placeholder="">
												<span id="error_empresa" class="help-block"></span>
							                </div>
										</div>
										<div class="row">
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="control-label">CAT</label>
												<div class="input-group">
								                	<input type="text" class="form-control" id="cat" name="cat" value="{{ $producto->cat }}">
													<span class="input-group-addon">%</span>
												</div>
												<span id="error_cat" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="control-label">Comisión apertura</label>
												<div class="input-group">
								                	<input type="text" class="form-control" id="comision_apertura" name="comision_apertura" value="{{ $producto->comision_apertura }}">
													<span class="input-group-addon">%</span>
												</div>
												<span id="error_comision_apertura" class="help-block"></span>
							                </div>
										</div>

										<div class="row">
											<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
												<label class="control-label">BC Score</label>
									            <input type="text" class="form-control" id="bc_score" name="bc_score" placeholder="580" value="{{ $producto->bc_score }}">
												<span id="error_bc_score" class="help-block"></span>
											</div>
											<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
												<label class="control-label">Condición</label>
									            <select class="form-control" id="condicion" name="condicion" >
													<option value="OR"  @if($producto->condicion == 'OR') selected @endif>o</option>
													<option value="AND" @if($producto->condicion == 'AND') selected @endif>y</option>
												</select>
											</div>
											<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
												<label class="control-label">Micro Score</label>
									            <input type="text" class="form-control" id="micro_score" name="micro_score" placeholder="580" value="{{ $producto->micro_score }}">
												<span id="error_micro_score" class="help-block"></span>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<label class="control-label">Consultas</label>
												<div class="form-check form-check-inline">
									                <label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									                    <input type="checkbox" id="consulta_alp" name="consulta_alp" @if($producto->consulta_alp) checked @endif> ALP
									                </label>
											  		<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								                    	<input type="checkbox" id="consulta_buro" name="consulta_buro" @if($producto->consulta_buro) checked @endif> Buró de crédito
														<span id="error_consulta_buro" class="help-block"></span>
													</label>
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								                    	<input type="checkbox" id="sin_cuentas_recientes" name="sin_cuentas_recientes" @if($producto->sin_cuentas_recientes) checked @endif> Sin Cuentas Recientes BC (-008)
								                  	</label>
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								                    	<input type="checkbox" id="sin_historial" name="sin_historial" @if($producto->sin_historial) checked @endif> Sin Historial BC (-009)
								                  	</label>
								                </div>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="control-label">Stored procdeure</label>
									            <input type="text" class="form-control" id="stored_procedure" name="stored_procedure" value="{{ $producto->stored_procedure }}">
												<span id="error_stored_procedure" class="help-block"></span>
											</div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="control-label">Campo cobertura</label>
									            <input type="text" class="form-control" id="campo_cobertura" name="campo_cobertura" value="{{ $producto->campo_cobertura }}" disabled>
												<span id="error_campo_cobertura" class="help-block"></span>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" onchange="cambioDobleOferta(this)" id="doble_oferta" name="doble_oferta" @if($producto->doble_oferta) checked @endif> Doble oferta
														<span id="error_doble_oferta" class="help-block"></span>
													</label>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<fieldset>
														<legend style="font-weight: bold; font-size: 14px;">Monto máximo:</legend>
														<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
															<input type="radio" id="tipo_monto_do_producto" onchange="cambioTipoMontoDO(this)" name="tipo_monto_do" value="producto" @if($producto->tipo_monto_do == 'producto') checked @endif @if(!$producto->doble_oferta) disabled @endif> Del producto
														</label>
														<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
															<div class="row">
																<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<input type="radio" id="tipo_monto_do_definido" onchange="cambioTipoMontoDO(this)" name="tipo_monto_do" value="definido" @if($producto->tipo_monto_do == 'definido') checked @endif  @if(!$producto->doble_oferta) disabled @endif> Definido
																</label>
															</div>
															<div class="row" id="div_monto_definido_do" @if($producto->tipo_monto_do != 'definido') style="display:none" @endif>
																<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<div class="col-lg-6 col-md-6 col-sm-12 col-xm-12">
																		<div class="input-group">
																			<span class="input-group-addon">$</span>
														                	<input type="text" class="form-control" placeholder="4,000" id="do_monto_maximo" name="do_monto_maximo" value="{{ $producto->do_monto_maximo }}">
														                	<span class="input-group-addon">.00</span>
																		</div>
																		<span id="error_do_monto_maximo" class="help-block"></span>
												                  	</div>
																</div>
															</div>
														</div>
														<span id="error_tipo_monto_do" class="help-block"></span>
													</fieldset>
												</div>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" onchange="cambioProcesoSimplificado(this)" id="proceso_simplificado" name="proceso_simplificado" @if($producto->proceso_simplificado) checked @endif> Proceso Simplificado
														<span id="error_proceso_simplificado" class="help-block"></span>
													</label>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<fieldset>
														<legend style="font-weight: bold; font-size: 14px;">Montos a los que aplica:</legend>
														<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
															<input type="radio" id="tipo_monto_simplificado_producto" onchange="cambioTipoMontoSimplificado(this)" name="tipo_monto_simplificado" value="producto" @if($producto->tipo_monto_simplificado == 'producto') checked @endif @if(!$producto->proceso_simplificado) disabled @endif> Del producto
														</label>
														<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
															<div class="row">
																<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<input type="radio" id="tipo_monto_simplificado_definido" onchange="cambioTipoMontoSimplificado(this)" name="tipo_monto_simplificado" value="definido" @if($producto->tipo_monto_simplificado == 'definido') checked @endif @if(!$producto->proceso_simplificado) disabled @endif> Definidos
																</label>
															</div>
															<div class="row" id="div_monto_definido_simplificado" @if($producto->tipo_monto_simplificado != 'definido') style="display:none" @endif >
																<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												                  	<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">De</label>
												                  	<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
																		<div class="input-group">
																			<span class="input-group-addon">$</span>
														                	<input type="text" class="form-control" placeholder="4,000" id="simplificado_monto_minimo" name="simplificado_monto_minimo" value="{{ $producto->simplificado_monto_minimo}}">
														                	<span class="input-group-addon">.00</span>
																		</div>
																		<span id="error_monto_minimo_simplificado" class="help-block"></span>
												                  	</div>
												                </div>
																<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">A</label>
												                  	<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
																		<div class="input-group">
																			<span class="input-group-addon">$</span>
														                	<input type="text" class="form-control" placeholder="40,000" id="simplificado_monto_maximo"  name="simplificado_monto_maximo" value="{{ $producto->simplificado_monto_maximo }}">
														                	<span class="input-group-addon">.00</span>
																		</div>
																		<span id="error_simplificado_monto_maximo" class="help-block"></span>
												                  	</div>
												                </div>
															</div>
														</div>
														<span id="error_tipo_monto_simplificado" class="help-block"></span>
													</fieldset>
												</div>
											</div>
											<hr>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="carga_identificacion_selfie_simplificado" name="carga_identificacion_selfie_simplificado" @if($producto->carga_identificacion_selfie_simplificado) checked @endif> Carga Identifiación/Selfie - Proceso Simplificado
														<span id="error_carga_identificacion_selfie_simplificado" class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="facematch_simplificado" name="facematch_simplificado" @if($producto->facematch_simplificado) checked @endif> Facematch - Proceso Simplificado
														<span id="error_facematch_simplificado" class="help-block"></span>
													</label>
												</div>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														Proceso Normal
													</label>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="carga_identificacion_selfie" name="carga_identificacion_selfie" @if($producto->carga_identificacion_selfie) checked @endif> Carga Identifiación/Selfie
														<span id="error_carga_identificacion_selfie" class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="facematch" name="facematch" @if($producto->facematch) checked @endif> Facematch
														<span id="error_facematch" class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="captura_referencias" name="captura_referencias" onchange="cambioCapturaReferencias(this)" @if($producto->captura_referencias) checked @endif> Captura Referencias
														<span  id="error_captura_referencias" class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="envio_sms_referencias" name="envio_sms_referencias" @if($producto->sms_referencias) checked @endif> Envio SMS Referencias
														<span id="error_envio_sms_referencias" class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="captura_cuenta_clabe" name="captura_cuenta_clabe" @if($producto->captura_cuenta_clabe) checked @endif> Captura Cuenta Clabe
														<span class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="carga_comprobante_domicilio" name="carga_comprobante_domicilio" @if($producto->carga_comprobante_domicilio) checked @endif> Carga Comprobante Domicilio
														<span class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="carga_comprobante_ingresos" name="carga_comprobante_ingresos" @if($producto->carga_comprobante_ingresos) checked @endif> Carga Comprobante Ingresos
														<span class="help-block"></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
														<input type="checkbox" id="carga_certificados_deuda" name="carga_certificados_deuda" @if($producto->carga_certificados_deuda) checked @endif> Carga Certificados Deuda
														<span class="help-block"></span>
													</label>
												</div>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="form-group">

												<div class="form-check form-check-inline">
													<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
														<input type="checkbox" id="garantia" name="garantia" @if($producto->garantia) checked @endif> Garantía
													</label>
													<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
														<input type="checkbox" id="seguro" name="seguro" @if($producto->seguro) checked @endif> Seguro
													</label>
												</div>
											</div>
										</div>

										<hr>

										<div class="row">
											<p class="alert alert-warning col-lg-12 col-md-12 col-sm-12 col-xs-12">Activar solo si se requiere que no se muestre oferta al final de la solicitud</p>
											<label class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<input type="checkbox" id="no_aplica_oferta" name="no_aplica_oferta" @if($producto->no_aplica_oferta) checked @endif> No mostrar oferta
											</label>
										</div>

										<hr>

										<div class="row" style="margin-bottom: 10px;">
											<div class="form-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
												<label class="control-label">Logotipo</label>

												<div class="input-group image-preview">
									                <input type="text" class="form-control image-preview-filename" disabled="disabled">
									                <span class="input-group-btn">

									                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
									                        <span class="glyphicon glyphicon-remove"></span> Limpiar
									                    </button>

									                    <div class="btn btn-default image-preview-input">
									                        <span class="glyphicon glyphicon-folder-open"></span>
									                        <span class="image-preview-input-title">Buscar</span>
									                        <input type="file" id="logo" name="logo" accept="image/png, image/jpeg, image/gif"/>
															<input type="hidden" id="logo_name" value="{{ $producto->logo }}"/>
									                    </div>
									                </span>
									            </div>
												<span id="error_logo" class="help-block"></span>
											</div>
										</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
												<label>Monto del préstamo</label>
											</div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
							                  	<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">De</label>

							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
														<span class="input-group-addon">$</span>
									                	<input type="text" class="form-control" placeholder="4,000" id="monto_minimo" name="monto_minimo" value="{{ $producto->monto_minimo }}">
									                	<span class="input-group-addon">.00</span>
													</div>
							                  	</div>
												<span id="error_monto_minimo" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">A</label>

							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
														<span class="input-group-addon">$</span>
									                	<input type="text" class="form-control" placeholder="40,000" id="monto_maximo"  name="monto_maximo" value="{{ $producto->monto_maximo }}">
									                	<span class="input-group-addon">.00</span>
													</div>
							                  	</div>
												<span id="error_monto_maximo" class="help-block"></span>
							                </div>
						              	</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
												<label>Edad</label>
											</div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
							                  	<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">De</label>

							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="18" id="edad_minima" name="edad_minima" value="{{ $producto->edad_minima }}">
														<span class="input-group-addon">años</span>
													</div>
							                  	</div>
												<span id="error_edad_minima" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">A</label>

							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="69" id="edad_maxima" name="edad_maxima" value="{{ $producto->edad_maxima }}">
														<span class="input-group-addon">años</span>
													</div>
							                  	</div>
												<span id="error_edad_maxima" class="help-block"></span>
							                </div>
						              	</div>

										<hr>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
												<label>Tasa de interes</label>
											</div>
											<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5">
							                  	<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">Mín.</label>
							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="19" id="tasa_minima" name="tasa_minima" value="{{ $producto->tasa_minima }}">
														<span class="input-group-addon">%</span>
													</div>
							                  	</div>
												<span id="error_tasa_minima" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
												<div class="form-check form-check-inline">
									                <label>
									                    <input type="checkbox" id="tasa_fija" name="tasa_fija" @if($producto->tasa_fija) checked @endif> Tasa fija
									                </label>
								                </div>
							                </div>
											<div id="div_tasa_max" class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5" @if($producto->tasa_fija) style="display:none" @endif>
												<label class="col-lg-2 col-md-2 col-sm-4 col-xm-4 control-label">Máx.</label>
							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="53" id="tasa_maxima" name="tasa_maxima" value="{{ $producto->tasa_maxima }}">
														<span class="input-group-addon">%</span>
													</div>
							                  	</div>
												<span id="error_tasa_maxima" class="help-block"></span>
							                </div>
						              	</div>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
												<label>Vigencia</label>
											</div>
											<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5">
							                  	<label class="col-lg-2 col-md-2 col-sm-2 col-xm-2 control-label">De</label>
												<div class="col-lg-10 col-md-10 col-sm-10 col-xm-10">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									                	<input type="text" class="form-control" placeholder="dd-M-yyyy" id="vigencia_de" value="{{ Carbon\Carbon::parse($producto->vigencia_de)->format('d-m-Y') }}">
														<input type="hidden" id="vigencia_de_format" name="vigencia_de">
													</div>
							                  	</div>
												<span id="error_vigencia_de" class="help-block"></span>
							                </div>
											<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
												<div class="form-check form-check-inline">
									                <label>
									                    <input type="checkbox" id="vigente" name="vigente" @if($producto->vigente) checked @endif> Vigente
									                </label>
								                </div>
							                </div>
											<div id="div_hasta" class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5" @if($producto->vigente) style="display:none" @endif>
												<label class="col-lg-2 col-md-2 col-sm-2 col-xm-2 control-label">A</label>
							                  	<div class="col-lg-10 col-md-10 col-sm-8 col-xm-8">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									                	<input type="text" class="form-control" placeholder="dd-M-yyyy" id="vigencia_hasta" value="{{ $producto->vigencia_hasta }}">
														<input type="hidden" id="vigencia_hasta_format" name="vigencia_hasta">
													</div>
							                  	</div>
												<span id="error_vigencia_hasta" class="help-block"></span>
							                </div>
						              	</div>
									</div>
								</div>
		    				</div>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Plazos</h3>
										<p class="alert alert-warning">Experian solo reconoce plazos mensuales (6, 12, 18, 24 y 36)</p>
									</div>

									<div class="box-body">
										<div class="row">
										    <div class="col-xs-5">
										        <select name="plazos" id="plazos" class="form-control" size="13" multiple="multiple">
													@foreach ($plazos as $plazo)
													<option value="{{ $plazo->id }}"> {{ $plazo->duracion }} {{ $plazo->plazo }} </option>
													@endforeach
										        </select>
										    </div>

										    <div class="col-xs-2">
										        <button type="button" id="plazos_undo" class="btn btn-default btn-block"><i class="fas fa-undo"></i></button>
										        <button type="button" id="plazos_rightAll" class="btn btn-default btn-block"><i class="fas fa-forward"></i></button>
										        <button type="button" id="plazos_rightSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-right"></i></button>
										        <button type="button" id="plazos_leftSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-left"></i></button>
										        <button type="button" id="plazos_leftAll" class="btn btn-default btn-block"><i class="fas fa-backward"></i></button>
										        <button type="button" id="plazos_redo" class="btn btn-default btn-block"><i class="fas fa-redo"></i></button>
										    </div>

										    <div class="col-xs-5">
										        <select id="plazos_to" class="form-control" size="13" multiple="multiple">
													@foreach ($producto->plazos as $plazo)
													<option value="{{ $plazo->id }}"> {{ $plazo->duracion }} {{ $plazo->plazo }} </option>
													@endforeach
												</select>
												<div class="row">
										            <div class="col-sm-6">
										                <button type="button" id="plazos_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
										            </div>
										            <div class="col-sm-6">
										                <button type="button" id="plazos_move_down" class="btn btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
										            </div>
										        </div>
											</div>

											<div class="col-xs-12">
												<span id="error_plazos" class="help-block"></span>
											</div>
										</div>
									</div>
								</div>

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Finalidades</h3>
									</div>

									<div class="box-body">
										<div class="row">
										    <div class="col-xs-5">
										        <select name="finalidades" id="finalidades" class="form-control" size="13" multiple="multiple">
													@foreach ($finalidades as $finalidad)
													<option value="{{ $finalidad->id }}"> {{ $finalidad->finalidad }} </option>
													@endforeach
										        </select>
										    </div>

										    <div class="col-xs-2">
										        <button type="button" id="finalidades_undo" class="btn btn-default btn-block"><i class="fas fa-undo"></i></button>
										        <button type="button" id="finalidades_rightAll" class="btn btn-default btn-block"><i class="fas fa-forward"></i></button>
										        <button type="button" id="finalidades_rightSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-right"></i></button>
										        <button type="button" id="finalidades_leftSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-left"></i></button>
										        <button type="button" id="finalidades_leftAll" class="btn btn-default btn-block"><i class="fas fa-backward"></i></button>
										        <button type="button" id="finalidades_redo" class="btn btn-default btn-block"><i class="fas fa-redo"></i></button>
										    </div>

										    <div class="col-xs-5">
										        <select id="finalidades_to" class="form-control" size="13" multiple="multiple">
													@foreach ($producto->finalidades as $finalidad)
													<option value="{{ $finalidad->id }}"> {{ $finalidad->finalidad }} </option>
													@endforeach
												</select>
											</div>

											<div class="col-xs-12">
												<span id="error_finalidades" class="help-block"></span>
											</div>
										</div>
									</div>
								</div>

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Ocupaciones Elegible</h3>
									</div>

									<div class="box-body">
										<div class="row">
										    <div class="col-xs-5">
										        <select name="ocupaciones" id="ocupaciones" class="form-control" size="13" multiple="multiple">
													@foreach ($ocupaciones as $ocupacion)
													<option value="{{ $ocupacion->id }}"> {{ $ocupacion->ocupacion }} </option>
													@endforeach
										        </select>
										    </div>

										    <div class="col-xs-2">
										        <button type="button" id="ocupaciones_undo" class="btn btn-default btn-block"><i class="fas fa-undo"></i></button>
										        <button type="button" id="ocupaciones_rightAll" class="btn btn-default btn-block"><i class="fas fa-forward"></i></button>
										        <button type="button" id="ocupaciones_rightSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-right"></i></button>
										        <button type="button" id="ocupaciones_leftSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-left"></i></button>
										        <button type="button" id="ocupaciones_leftAll" class="btn btn-default btn-block"><i class="fas fa-backward"></i></button>
										        <button type="button" id="ocupaciones_redo" class="btn btn-default btn-block"><i class="fas fa-redo"></i></button>
										    </div>

										    <div class="col-xs-5">
										        <select id="ocupaciones_to" name="ocupaciones_to" class="form-control" size="13" multiple="multiple">
													@foreach ($producto->ocupaciones as $ocupacion)
													<option value="{{ $ocupacion->id }}"> {{ $ocupacion->ocupacion }} </option>
													@endforeach
												</select>
											</div>

											<div class="col-xs-12">
												<span id="error_ocupaciones" class="help-block"></span>
											</div>
										</div>
									</div>
								</div>

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Experian</h3>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-check form-check-inline">
									                <label>
									                    <input type="checkbox" id="proceso_experian" name="proceso_experian" onchange="cambioProcesoExperian(this)" @if($producto->proceso_experian) checked @endif> Proceso Experian
									                </label>
								                </div>
							                </div>
											<div id="div_producto_experian" class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<label class="col-lg-6 col-md-6 col-sm-6 col-xm-6 control-label">Nombre producto Experian</label>
							                  	<div class="col-lg-6 col-md-6 col-sm-6 col-xm-6">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="CPPRODUCTO" id="producto_experian" name="producto_experian" value="{{ $producto->producto_experian }}" @if(!$producto->proceso_experian) disabled @endif>
													</div>
							                  	</div>
												<span class="col-lg-6 col-md-6 col-sm-6 col-xm-6 help-block" id="error_producto_experian"></span>
							                </div>
						              	</div>
									</div>
								</div>

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Redirección al terminar Solicitud</h3>
									</div>

									<div class="box-body">
										<div class="row">
											<div id="div_redireccion" class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="col-lg-6 col-md-6 col-sm-6 col-xm-6 control-label">Redirección Sitio</label>
							                  	<div class="col-lg-6 col-md-6 col-sm-6 col-xm-6">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="/pagina-redireccion" id="redireccion" name="redireccion" value="{{ $producto->redireccion }}">
													</div>
							                  	</div>
												<span id="error_producto_experian" class="help-block"></span>
							                </div>
											<div id="div_redireccion" class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<label class="col-lg-6 col-md-6 col-sm-6 col-xm-6 control-label">Redirección Portal</label>
							                  	<div class="col-lg-6 col-md-6 col-sm-6 col-xm-6">
													<div class="input-group">
									                	<input type="text" class="form-control" placeholder="/pagina-redireccion" id="redireccion_portal" name="redireccion_portal" value="{{ $producto->redireccion_portal }}">
													</div>
							                  	</div>
												<span id="error_producto_experian" class="help-block"></span>
							                </div>
						              	</div>
									</div>
								</div>

		    				</div>

		  				</div>
						<div class="row">
							<div class="footer" style="text-align: center">
								<button type="button" class="btn btn-primary" onclick="actualizarProducto()">Actualizar</button>
								<a href="{{ URL::route('productos') }}" class="btn btn-primary"> Regresar </a>
							</div>
						</div>
					</form>
	    		</div>
			</div>

		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/js/backoffice/multiselect.min.js"></script>
<script type="text/javascript" src="/js/backoffice/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.es.js"></script>
<script type="text/javascript" src="/js/backoffice/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/js/productos.js?v=<?php echo microtime(); ?>"></script>
<script>
	$('#plazos').multiselect({
		keepRenderingSort: true
	});
	$('#finalidades').multiselect({
		keepRenderingSort: true
	});
	$('#ocupaciones').multiselect({
		keepRenderingSort: true
	});

	$(function() {

		$('#vigencia_hasta').prop('disabled', false);
		var date_de = $('#vigencia_de').val();
		$('#vigencia_de').datetimepicker('update', date_de);
		date = $('#vigencia_hasta').val();
		$('#vigencia_hasta').datetimepicker('update', date);
		date_de = date_de.split("-");
		date_de = new Date(date_de[2], date_de[1] - 1, parseInt(date_de[0]) + 1);

		$('#vigencia_hasta').datetimepicker('setStartDate', date_de);
		$('#vigencia_hasta').datetimepicker('setInitialDate', date_de);

	});
</script>
@endsection
