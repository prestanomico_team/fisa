<!DOCTYPE html>
<style>

@import url(https://fonts.googleapis.com/css?family=Montserrat:400,500,80);

body{
  font-family: 'Montserrat', sans-serif;
}

.footer {
	background-color: #fff !important;
	color: #282828 !important;
	font-family: 'Montserrat';
}

a {
	color: #282828 !important;
}

.container {
	background-image: url(/images/landings/generica/background.png);
	background-size: 100% 102%;
	background-attachment: inherit;
	background-repeat: no-repeat;
}

.titulo {
	font-family: 'Montserrat';
	font-weight: lighter;
	font-size: 2.5vw;
}

.subtitulo {
	font-family: 'Montserrat';
	font-weight:  bold;
	font-size: 1.75vw;
}

.texto {
	font-family: 'Montserrat';
	font-weight: lighter;
	font-size: 1.15vw;
	margin-top: 30px;
	margin-bottom: 30px;
}

.checa-calificas {
	background-image: url(/images/landings/generica/btn-checa-calificas.png);
	width: 12vw;
	height: 2.75vw;
	background-repeat: no-repeat;
    background-size: contain;
	margin-bottom: 10px;
}

.leyenda {
	font-family: 'Montserrat';
	font-weight: lighter;
	font-size: 10px;
}

p {
    line-height: 1.5;
}

.div_leyenda {
  position: relative;
  margin-top: 10px;
}

.div_leyenda_header {
  position: relative;
  margin-top: 10px;
  text-align: center;
}

.leyenda_text {
  display: inline-block;
  font-weight: lighter;
  font-size: .9vw;
  font-family: 'Montserrat';
  line-height: 1.5;
}

.leyenda_image {
  display: inline-block;
  position: absolute;
  top: 1.55vw;
  margin-left: -9.4vw;
  width: 5vw;
}

.headerfb_text {
  display: inline-block;
  font-weight: bolder;
  font-size: 13px;
  font-family: 'Montserrat';
}

.headerfb_image {
  position: absolute;
  margin-left: 13px;
  width: 25px;
}

.logos {
	top: 30px;
	margin-bottom: 50px
}

.logo_prestanomico {
	width: 150px;
	margin-left: 40px;
}

.icon-inline {
	width: 17px !important;
	height: 12px !important;
	margin-right: 10px;
}

.img-left {
	width: 375px;
	height: 325.250px;
}

@media screen and (max-width: 670px) {

	div.description {
		display: none;
	}

	.logo_prestanomico {
		margin-left: 10px;
	}

	.img-left {
		width: 150px;
		height: 170px;
	}

	.titulo {
		font-family: 'Montserrat';
		font-weight: lighter;
		font-size: 4vw;
	}

	.subtitulo {
		font-family: 'Montserrat';
		font-weight:  bold;
		font-size: 3.5vw;
	}

	.texto {
		font-family: 'Montserrat';
		font-weight: lighter;
		font-size: 3.5vw;
		margin-top: 5px;
		margin-bottom: 5px;
        margin-left: 150px;
	}

	.checa-calificas {
		background-image: url(/images/landings/generica/btn-checa-calificas.png);
		width: 110px;
		height: 25px;
		background-repeat: no-repeat;
	    background-size: contain;
		margin-bottom: 10px;
	}

	.leyenda_text {
	  display: inline-block;
	  font-weight: lighter;
	  font-size: 2vw;
	  margin-left: 40px;
	  font-family: 'Montserrat';
	}

	.leyenda_image {
	  display: inline-block;
	  position: absolute;
	  top: 20px;
	  margin-left: -130px;
	  width: 60px;
	}

	.div_leyenda {
	  position: relative;
	  margin-top: 10px;
	}

	.div-img-left {
		margin-right: 75px;
	}

}

@media screen and (min-width: 671px) {
	div.description-mobile {
		display: none;
	}

    div.logo_empresa {
        display: none;
    }

}

@media screen and (min-width: 900px) and (max-width: 1100px) {
	.img-left {
		width: 320px;
	}

}

@media screen and (min-width: 671px) and (max-width: 899px) {
	.img-left {
		width: 250px;
	}
	.div-img-left {
		margin-right: 25px;
	}

    .div_leyenda_header {
		position: relative;
		margin-top: 10px;
		text-align: right;
	}

}

@media (orientation: landscape) {

	.div_leyenda_header {
		position: relative;
		margin-top: 10px;
		text-align: center;
	}

}

</style>
<html lang='en'>
	<head>
      	<meta charset="UTF-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1.0">
      	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      	<title>Prestanómico | {{ $producto->nombre_producto }}</title>
		<meta name="description" content="Somos una empresa que brinda préstamos flexibles con una tasa de interés menor a la de los bancos regulares.">
		<meta name="keywords" content="Prestamos personales, Préstamos personales, crédito personal, credito personal, préstamos en línea, prestamos en linea, prestamos sin aval, creditos rapidos, créditos rápidos, préstamos móvil, buró de crédito, buro de credito, CAT, IVA">
      	<link rel="icon" type="image/png" href="/images/favicon_prestanomico_negro.png">
{{--     	<link rel="stylesheet" href="/css/styles.css"/>
		<link rel="stylesheet" href="/css/bootstrap.css"/> 
		<link rel="stylesheet" href="/css/bootstrap-grid.min.css"/> --}}
   	</head>
   	<body>
   		<div class="container">
			<!-- Logos -->
			<div class="row col-12 col-sm-12 col-xs-12 logos">
				<div class="col-4 col-sm-2 col-xs-4" >
					<img class="logo_prestanomico" src="/images/landings/generica/logo-prestanomico.png"></img>
				</div>
				<div class="col-4 col-sm-2 col-xs-4 offset-lg-7 offset-md-7 offset-sm-5 offset-xs-4 offset-1 logo_empresa">
				    <img src="/images/landings/generica/logo-{{ $producto->alias }}.png" style="width:80px; height: 35px;" alt="/images/landings/generica/logo-{{ $producto->alias }}.png"></img>
				</div>
			</div>
			<!-- Descripción -->
			<div class="row col-12 description">
 				<div class="col-11 offset-1" style="max-width: 100%;">
					<div class="div-img-left" style="align-items: flex-end; display: flex; flex-direction: column; float: left;">
						<img class="img-left" src="/images/landings/generica/img-left-nomina.png"></img>
					</div>
					<div style="max-width: 90%; display: table-cell">
                        <img src="/images/landings/generica/deloitte_logo.png" style="height: 65px;" alt="/images/landings/generica/logo-{{ $producto->alias }}.png"></img>
						<p class="titulo">Pide tu préstamo con ¡aprobación en línea!</p>
						<p class="subtitulo">Te prestamos hasta 3 meses de tu sueldo</p>
						<p class="texto">Resuelve tus imprevistos o unifica tus deudas con <br> un préstamo con pagos fijos y accesibles</p>
						<button class="btn checa-calificas"> </button>
						<div class="div_leyenda">
							<p>
						    	<div class="leyenda_text">* El préstamo es otorgado por Prestanómico SAPI de CV.</div>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- Descripción Mobile-->
			<div class="row col-12 description-mobile">
 				<div class="col-12" style="max-width: 100%;">

					<p class="titulo">Pide tu préstamo con ¡aprobación en línea!</p>
					<p class="subtitulo">Te prestamos hasta 3 meses de tu sueldo</p>

					<div style="align-items: flex-end; display: flex; flex-direction: column; float: left; margin-right: -80px;">
						<img class="img-left" src="/images/landings/generica/img-left-nomina.png"></img>
					</div>

					<p class="texto" style="width: 50%;">Resuelve tus imprevistos o unifica tus deudas con un préstamo con pagos fijos y accesibles.</p>
					<center><button class="btn checa-calificas"> </button></center>
					<div class="div_leyenda">
						<p>
							<div class="leyenda_text">* El préstamo es otorgado por Prestanómico SAPI de CV.</div>
						</p>
					</div>
				</div>
			</div>

   		</div>
		<!-- Footer -->
		<div class="footer">
			<div class="footer__schema">
				<div>
				   <p style="display:inline-table; width: 250px;"><img src="/images/landings/generica/logo-whatsapp.png" class="icon-inline"> (55) 8128-9583</p>

				   <p style="display:inline-table; width: 350px;"><img src="/images/landings/generica/logo-email.png" class="icon-inline"> prestamosnomina@prestanomico.com</p>
			    </div>
		       <p> Avenida Insurgentes Sur No. 1425, Piso 9, Oficina 9D, Col. Insurgentes Mixcoac, Delegación Benito Juárez, C.P. 03920 CDMX</p>
		    </div>
		    <div class="footer__certificate">
		       <span id="siteseal">
		          <script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=fJzgGrLcXSBlMzn2cT5aU0SRFMGfCr7Tpo8P8xkf7gqUdaSDbIjWU7eUFfZ0"></script>
		       </span>
		       <span>
		          <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
		       </span>
		    </div>
		</div>
		<script src='/js/app_back_office.js' type="text/javascript" charset="utf-8"></script>
		<script src='/js/jquery.cookie.js' type="text/javascript" charset="utf-8"></script>
		<script src='/js/cookie_calificas.js?v=<?php echo microtime(); ?>' type="text/javascript" charset="utf-8"></script>
        @include('layouts.footer_analitycs')
    </body>
</html>
