@extends('crm.app')
@section('content')

<div class="container" style="width:100%">
	<div id="app" class="content">
		@php
			use App\CatalogoSepomex;
			use Carbon\Carbon;

			$estados = CatalogoSepomex::selectRAW('distinct(estado), id_estado')
				->orderBy('id_estado', 'asc')
				->get();
			$estados = (json_encode($estados));
			use App\MotivoRechazo;
			$motivos = MotivoRechazo::selectRAW('motivo, id')
				->orderBy('id', 'asc')
				->get();
			$motivos = (json_encode($motivos));
			$fecha = Carbon::now()->format('d/m/Y');
			$url = Request::fullUrl();

        @endphp
        {{dd($estados)}}
	    <personales :estados='{{ $estados }}' :motivos='{{ $motivos }}' :configuracion='{{ json_encode($configuracion) }}' fecha="{{ $fecha }}" url="{{ $url }}"></personales>

	</div>
</div>
@endsection
@section('app_backoffice')
	<script type="text/javascript" src="/js/app_backoffice.js"></script>
	<script type="text/javascript" src="/js/statusOferta.js"></script>
@append
