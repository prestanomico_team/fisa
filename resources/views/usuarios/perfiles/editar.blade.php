@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
<link rel="stylesheet" href="/back/css/multi-select.css">
<link rel="stylesheet" href="/back/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<style>
.box.box-primary {
	border-top-color: #f79020;
}
label {
	text-transform: none;
}

.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;
    color: #333;
    background-color: #fff;
    border-color: #ccc;
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
.popover-content {
	background-color: #eaeaea;
}
.help-block {
    color: #dd4b39;
}

</style>

<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border" style="text-align:center;">
	            	<h3 class="box-title" style="font-weight: bold;">Modificar rol</h3>
	            </div>
				@if(Auth::user()->can('administracion-usuarios'))
				<div class="box-body">
					<form role="form" id="datos_usuario">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Datos del perfil</h3>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							                  	<label class="control-label">Nombre:</label>
												<input type="hidden" id="usuario_id" name="rol_id" value="{{ $rol[0]->id }}">
												<input type="text" class="form-control" id="nombre_rol" name="nombre_rol" value="{{ $rol[0]->display_name }}" disabled>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							                  	<label class="control-label">Área:</label>
												<input type="text" class="form-control" id="email_rol" name="email_rol" value="{{ $rol[0]->area }}" disabled>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							                  	<label class="control-label">Description:</label>
												<input type="text" class="form-control" id="area_rol" name="area_rol" value="{{ $rol[0]->description }}" disabled>
											</div>
										</div>
									</div>
								</div>
		    				</div>

							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

								<div class="box box-primary">

									<div class="box-header with-border">
										<h3 class="box-title">Permisos</h3>
									</div>

									<div class="box-body">
										<div class="row">
										    <div class="col-xs-5">
										        <select name="permisos" id="permisos" class="form-control" size="13" multiple="multiple">
													@foreach ($permisos as $permiso)
													<option value="{{ $permiso->id }}"> {{ $permiso->display_name }} </option>
													@endforeach
										        </select>
										    </div>

										    <div class="col-xs-2">
										        <button type="button" id="permisos_undo" class="btn btn-default btn-block"><i class="fas fa-undo"></i></button>
										        <button type="button" id="permisos_rightAll" class="btn btn-default btn-block"><i class="fas fa-forward"></i></button>
										        <button type="button" id="permisos_rightSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-right"></i></button>
										        <button type="button" id="permisos_leftSelected" class="btn btn-default btn-block"><i class="fas fa-chevron-left"></i></button>
										        <button type="button" id="permisos_leftAll" class="btn btn-default btn-block"><i class="fas fa-backward"></i></button>
										        <button type="button" id="permisos_redo" class="btn btn-default btn-block"><i class="fas fa-redo"></i></button>
										    </div>

										    <div class="col-xs-5">
										        <select id="permisos_to" class="form-control" size="13" multiple="multiple">
													@foreach ($rol[0]->perms as $permiso)
													<option value="{{ $permiso->id }}"> {{ $permiso->display_name }} </option>
													@endforeach
												</select>
												<div class="row">
										            <div class="col-sm-6">
										                <button type="button" id="permisos_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
										            </div>
										            <div class="col-sm-6">
										                <button type="button" id="permisos_move_down" class="btn btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
										            </div>
										        </div>
											</div>

											<div class="col-xs-12">
												<span id="error_permisos" class="help-block"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
		  				</div>
						<div class="row">
							<div class="footer" style="text-align: center">
								<button type="button" class="btn btn-primary" onclick="actualizarPerfil()">Actualizar</button>
								<a href="{{ URL::route('perfiles') }}" class="btn btn-primary"> Regresar </a>
							</div>
						</div>
					</form>
	    		</div>
				@else
					<center>
						<br>
						<h4>No tienes privilegios para realizar esta acción</h4>
                        <br>
					</center>
                @endif
			</div>

		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/js/backoffice/multiselect.min.js"></script>
<script type="text/javascript" src="/back/js/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.es.js"></script>
<script type="text/javascript" src="/js/backoffice/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/js/perfiles.js?v=<?php echo microtime(); ?>"></script>
<script>
	$('#permisos').multiselect({
		keepRenderingSort: true
	});
</script>
@endsection
