@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<link rel="stylesheet" href="/css/webapp/bootstrap-grid.min.css">
<div id="solicitud" class="form-solicitud">
	<div class="row">
		<div class="col-12 col-lg-12" id="navigator">
			@if(isset($pasos_habilitados))
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" @if(count($pasos_habilitados) < 5) style="justify-content: center; display: flex;" @endif>
					@foreach ($pasos_habilitados as $paso)
						@if($paso == 'Identificación y Selfie' || $paso == 'Referencias' || $paso == 'Cuenta Clabe' || $paso == 'Comprobante de Domicilio'|| $paso == 'Comprobante de Ingresos')
							<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@elseif($paso == 'Certificados de deuda')
							<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@else
							<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@endif
					@endforeach
				</ul>
			@else
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-10 col-lg-10 offset-lg-1 offset-1">
			<div class="card">
				<div class="card-header">
					<p>Certificados de deuda</p>
					<ol>
                        <li>Sube tus certificados de deuda en PDF.</li>
                        <li>Puedes subir hasta 10 archivos.</li>
                    </ol>
                    <br>
                    <p>Solicitud de crédito</p>
					<ol>
                        <li>Sube tu solicitud de crédito en un solo documento. (Ambos lados de la hoja)</li>
					</ol>
					En caso de no contar con ellos, envíanos un whatsapp para ver alternativas.
				</div>
                <hr>
                <form id="formCertificadosDeuda" class="form-solicitud__content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10 col-lg-10 offset-lg-1 offset-1">
                                <div class="card">
                                    <div class="card-body">
                                        <p><strong>Certificados de Deuda</strong></p>
                                        <div class="col-lg-12">
                                            <div class="dropzone" id="myDropzone"></div>
                                        </div>
                                        <input id="num_certificados" name="num_certificados" type="hidden" value="">
                                        <input id="certificados" name="certificados" type="hidden" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-10 col-lg-10 offset-lg-1 offset-1">
                                <div class="card">
                                    <div class="card-body">
                                        <p><strong>Solicitud de crédito</strong></p>
                                        <div class="col-lg-12">
                                            <div class="dropzone" id="myDropzoneSolic"></div>
                                            <input id="solicitud" name="solicitud" type="hidden" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row subirarchivos" style="text-align:center">
                            <div class="form-group col-12 col-sm-12 col-md-12">
                                <button id="subirArchivos" type="button" class="general-button" style="display:none">Subir Archivos</button>
                            </div>
                        </div>
                        <br>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>

@endsection
