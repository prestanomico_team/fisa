@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<link rel="stylesheet" href="/css/webapp/bootstrap-grid.min.css">
<div id="solicitud" class="form-solicitud">
	<div class="row">
		<div class="col-12 col-lg-12" id="navigator">
			@if(isset($pasos_habilitados))
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" @if(count($pasos_habilitados) < 5) style="justify-content: center; display: flex;" @endif>
				@foreach ($pasos_habilitados as $paso)
					@if($paso == 'Identificación y Selfie' || $paso == 'Referencias' || $paso == 'Cuenta Clabe')
						<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@elseif($paso == 'Comprobante de Domicilio')
						<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@else
						<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@endif
				@endforeach
				</ul>
			@else
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-10 col-lg-10 offset-lg-1 offset-1">
			<div class="card">
				<div class="card-header">
					<p>Fotografía no mayor a 3 meses de tu comprobante de domicilio en:</br>
					@if ($domicilio->num_interior == '')
					<b><u style="color: #e4004d;">{{ $domicilio->calle }} {{ $domicilio->num_exterior }}, {{ $domicilio->colonia }}, {{ $domicilio->delegacion }}, C.P. {{ $domicilio->cp }}.</u></b>
					@else
					<b><u style="color: #e4004d;">{{ $domicilio->calle }} {{ $domicilio->num_exterior }} Interior  {{ $domicilio->num_interior }}, {{ $domicilio->colonia }}, {{ $domicilio->delegacion }}, C.P. {{ $domicilio->cp }}.</u></b>
					@endif
					</p>
					<b style="color: #e4004d;">Tips:</b>
					<ul>
						<li>CFE, Teléfono fijo, Gas natural, Recibo de agua, Recibo de TV de paga.</li>
						<li>No importa que no esten a tu nombre.</li>
					</ul>
					<p style="color: #e4004d;"><b>Debes subir 2 archivos:</b></p>
					<ol>
						<li>Comprobante completo con las 4 esquinas del documento. <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="comprobante('completo')"></i></li>
						<li>Enfoque solo a la dirección de tu comprobante. <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="comprobante('acercamiento')"></i></li>
					</ol>
				</div>
				<hr>
				<div class="card-body">
					<div class="col-lg-12">
						<label id="documentosFaltantes" style="color: #e4004d; text-align:center;"></label>
					</div>
					<div class="col-lg-12">
						<input type="hidden" id="latitud" value="{{ $latitud }}"></input>
						<input type="hidden" id="longitud" value="{{ $longitud }}"></input>
						<input type="hidden" id="location_error" value="{{ $location_error }}"></input>
						<input type="hidden" id="latitud_reverse" value=""></input>
						<input type="hidden" id="longitud_reverse" value=""></input>
						<input type="hidden" id="reverse_error" value=""></input>
						<input type="hidden" id="distancia" value="0"></input>
						<input type="hidden" id="device" value="{{ $device }}"></input>
						<input type="hidden" id="browser" value="{{ $browser }}"></input>
						<input type="hidden" id="platform" value="{{ $platform }}"></input>
						<div class="dropzone" id="myDropzone"></div>
					</div>
					<div class="row" style="text-align:center">
						<div class="form-group subirarchivos col-12 col-sm-12 col-md-12">
							<button id="subirArchivos" type="button" class="general-button" style="display:none">Subir Archivos</button>
						</div>
					</div>
					<br>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
