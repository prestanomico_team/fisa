@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<link rel="stylesheet" href="/css/webapp/bootstrap-grid.min.css">
<div id="solicitud" class="form-solicitud">
	<div class="row">
		<div class="col-12 col-lg-12" id="navigator">
			@if(isset($pasos_habilitados))
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" @if(count($pasos_habilitados) < 5) style="justify-content: center; display: flex;" @endif>
					@foreach ($pasos_habilitados as $paso)
						@if($paso == 'Identificación y Selfie' || $paso == 'Referencias' || $paso == 'Cuenta Clabe' || $paso == 'Comprobante de Domicilio')
							<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@elseif($paso == 'Comprobante de Ingresos')
							<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@else
							<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
								<p>{!! $paso !!}</p>
							</a></li>
						@endif
					@endforeach
				</ul>
			@else
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-10 col-lg-10 offset-lg-1 offset-1">
			<div class="card">
				<div class="card-header">
					<p>Fotografías de comprobante de ingresos.</p>
					<ol>
						<li>Estados de cuenta completos (no solo carátula).</li>
						<li>Recibos de nómina.</li>
					</ol>
					En caso de no contar con ellos, envíanos un whatsapp para ver alternativas.
				</div>
				<hr>
				<div class="card-body">
					<form id="formComprobanteIngresos" class="form-solicitud__content">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			                    <div class="calculadoras-productos">
									<label>Selecciona la frecuencia de tus comprobantes de ingresos</label>
									@if ($producto == 'sindicalizados')
									<div class="withDecoration min-select">
											<span>
												<select class="pre-registro-input required" id="frecuencia" name="frecuencia" onchange="dropZone('frecuencia')" aria-required="true" aria-invalid="false" required="required">
													<option value="" disabled="" selected="selected">Seleeciona*</option>
													<option value="2">Mensual</option>
													<option value="4">Quincenal</option>
													<option value="8">Semanal</option>
													<option value="2">Otro</option>
												</select>
											</span>
											<div class="decoration"><i class="fas fa-chevron-down"></i></div>
										</div>
									@else
										<div class="withDecoration min-select">
											<span>
												<select class="pre-registro-input required" id="frecuencia" name="frecuencia" onchange="dropZone('frecuencia')" aria-required="true" aria-invalid="false" required="required">
													<option value="" disabled="" selected="selected">Seleeciona*</option>
													<option value="3">Mensual</option>
													<option value="6">Quincenal</option>
													<option value="12">Semanal</option>
													<option value="2">Otro</option>
												</select>
											</span>
											<div class="decoration"><i class="fas fa-chevron-down"></i></div>
										</div>
									@endif
			                    </div>
			                    <small id="ocupacion-help" class="help"></small>
			                </div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			                    <div class="calculadoras-productos">
									<label>Selecciona el tipo de comprobante</label>
			                        <div class="withDecoration min-select">
			                            <span>
			                                <select class="pre-registro-input required" id="tipo_comprobante" name="tipo_comprobante" onchange="dropZone('comprobante')" aria-required="true" aria-invalid="false" required="required">
			                                    <option value="" disabled="" selected="selected">Selecciona*</option>
												<option class="regular" value="Estados de cuenta">Estados de cuenta</option>
												<option class="regular" value="Recibos de nómina">Recibos de nómina</option>
												<option class="alterno" value="Alterno" style="display:none">Alterno</option>
			                                </select>
			                            </span>
			                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
			                        </div>
			                    </div>
			                    <small id="ocupacion-help" class="help"></small>
			                </div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label id="documentosFaltantes" style="color: #e4004d;"></label>
							</div>
							<div class="col-lg-12">
								<label id="documentosAlternos" style="display:none"></label>
							</div>
							<div class="col-lg-12">
								<div id="myDropzone" class="dropzone" style="display:none"></div>
							</div>
						</div>
						<div class="row subirarchivos" style="text-align:center">
							<div class="form-group col-12 col-sm-12 col-md-12">
								<button id="subirArchivos" type="button" class="general-button" style="display:none">Subir Archivos</button>
							</div>
						</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
