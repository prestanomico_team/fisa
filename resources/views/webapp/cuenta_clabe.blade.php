@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<link rel="stylesheet" href="/css/webapp/bootstrap-grid.min.css">
<div id="solicitud" class="form-solicitud">
	<div class="row">
		<div class="col-12 col-lg-12" id="navigator">
			@if(isset($pasos_habilitados))
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" @if(count($pasos_habilitados) < 5) style="justify-content: center; display: flex;" @endif>
				@foreach ($pasos_habilitados as $paso)
					@if($paso == 'Identificación y Selfie' || $paso == 'Referencias')
						<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@elseif($paso == 'Cuenta Clabe')
						<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@else
						<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@endif
				@endforeach
				</ul>
			@else
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation" class="complete"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-10 col-lg-10 offset-lg-1 offset-1">
			<div class="card">
				<div class="card-header">
					<p>¿Dónde quieres que te depositemos?.</br>
					Compártenos la cuenta CLABE a tu nombre donde quieres que realicemos el depósito de tu crédito en caso de ser aprobado.</p>
				</div>
				<hr>
				<div class="card-body">
					<form id="formCuentaCable" class="form-solicitud__content">
						<div class="row justify-content-md-center">
							<div class="form-group col-12 col-sm-12 col-md-3">
								<label>Banco:</label>
								<select class="form-control" id="banco" name="banco" onchange="getBancoDescripcion()">
									<option value="">SELECCIONA</option>
									@foreach($bancos as $key => $banco)
										<option value="{{ $key }}">{{ $banco }}</option>
									@endforeach
								</select>
								<input type="hidden" id="descripcion_banco" name="descripcion_banco" class="form-control upper"></input>
								<small id="lerror_banco" class="form-text lerror"></small>
							</div>
						</div>
						<div class="row justify-content-md-center">
							<div class="form-group col-12 col-sm-12 col-md-3">
								<label>Cuenta CLABE:</label>
								<input type="text" id="clabe_interbancaria" name="clabe_interbancaria" class="form-control upper" maxlength="18"></input>
								<small id="lerror_clabe_interbancaria" class="form-text lerror"></small>
							</div>
						</div>
						<div class="row" style="text-align:center">
							<div class="form-group col-12 col-sm-12 col-md-12">
						    	<button type="button" class="general-button" onclick="guardarCuentaClabe()">Siguiente</button>
							</div>
					  	</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
