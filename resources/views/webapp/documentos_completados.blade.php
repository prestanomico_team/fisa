@extends('layouts.webapp')
@section('content')
<div id="statusSolicitud">
	<div class="row">
		<div id="loginModal" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-dialog-centered modal-login modal-md ">
			    <div class="modal-content">
			        <div class="modal-header" style="background: #92133e; color: #fff;">
			            <h2 class="titulo-dinamico" style="text-align:center">¡Gracias por subir tus documentos!</h2>
			        </div>
			        <div class="modal-body" style="text-align: center">
						<p>Te informamos que ya cuentas con una solicitud calificada.</p>
			            <p>Si aún no te ha contactado ninguno de nuestros
			               ejecutivos y han pasado más de 24 hrs desde tu
			               registro, por favor envíanos un correo a
			               <br><b class="bold">documentaciondigital@financieramontedepiedad.com.mx</b>
			               <br>para continuar con el proceso.</p>
			        </div>
					<div class="modal-footer">
			            <a href="/"><b>Inicio</b></a>
			        </div>
			    </div>
			</div>
		</div>
	</div>

</div>
@endsection
