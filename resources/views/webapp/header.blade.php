<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Facebook tagging -->
	<meta property="fb:app_id" content=""/>
	<meta property="og:type"   content="website" />
	<meta property="og:url"    content="https://financieramontedepiedad.com.mx" />
	<meta property="og:title"  content="Financiera Monte de Piedad | Crédito Personal"/>
	<meta property="og:image"  content="" />
	<meta property="og:description"  content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades."/>
	<!-- End of facebook tagging -->
	<link rel="shortcut icon" href="images/brand/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta name="description" content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades.">
	<meta name="keywords" content="">
	<style>
	:focus {
		/* outline: 0 !important; */
	}
	#laPaz #header .navbar-nav .dropdown-menu {
		padding: 5px 0 !important;
		background: rgb(230, 65, 64, 1);
		text-align: center;
	}
	#laPaz #header .navbar-nav .dropdown-menu .dropdown-item {
		color: #fff;
	}
	</style>
	<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="/css/generals.css">
	<link type="text/css" rel="stylesheet" href="/css/magnific-popup.css">
	<link type="text/css" rel="stylesheet" href="/css/sweetalert2.min.css">
	<link rel="canonical" href="https://solicitaloahora.financieramontedepiedad.com.mx/" />
	<link rel="stylesheet" href="/css/bootstrap-slider.css">
	<script src="/js/bootstrap-slider.js"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
	<title>Financiera Monte de Piedad | Crédito Personal</title>
	<link rel="icon" type="image/png" href="images/brand/favicon.png">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="/images/brand/financiera_monte_de_piedad_logo.png" alt="Financiera Monte de Piedad" title="">
				</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right mt-4">
					<li><a href="/">Inicio</a></li>
					@if (Auth::guard('prospecto')->check())
					<li class="nav-item dropdown">
						<a class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('prospecto')->user()->nombres }} {{ Auth::guard('prospecto')->user()->apellido_paterno }} {{ Auth::guard('prospecto')->user()->apellido_materno }}</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						  <a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a>
						</div>
					</li>
					@endif
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>
</header>
