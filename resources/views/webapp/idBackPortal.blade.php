@extends('layouts.webapp')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12" id="navigator">
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
          	<div class="col-12 col-lg-12">
				<div class="card">
	              <div class="card-header">
	                <div class="align-items-center">
	                  <div class="col" style="text-align: center">
	                    <h4 class="card-header-title">
							<b>INE REVERSO</b>
	                    </h5>
						@if($desktop === true)
						<span class="span-card">
							<ul style="display:inline-block">
								<li>Asegurate de que la imagen sea lo más clara y legible posible. <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="ayuda('tip1')"></i></li>
							</ul>
						</span>
						@else
						<span class="span-card">
							<ul style="display:inline-block">
								<li>Coloca tu credencial de elector sobre un fondo negro y tomale una foto a la parte de enfrente.</li>
								<li>Asegurate de que la imagen sea lo más clara  y legible posible. <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="ayuda('tip1')"></i></li>
							</ul>
						</span>
						@endif
	                  </div>
	                </div>
	              </div>
				  <hr>
	              <div class="card-body" id="tomar-foto">
				  	<div id="cargando" style="display:none">
					   <center>
					      <img src="/images/ajax-loader.gif" class="loading-gif">
					      <p> Cargando camara... </p>
					   </center>
					</div>
					@if($desktop === true)
				  		<div class="videoDiv desktop" id="videoDiv">
				  	    	<video id="videoInput" autoplay="true" class="desktop"></video>
							<div class="controls">
								<button class="btn btn-danger play" title="Tomar foto" onclick="IcarSDK.documentCapture.manualTrigger();"><i class="fas fa-camera fa-3x"></i></button>
							</div>
				  	    </div>
						@else
						<div class="videoDiv mobile" id="videoDiv">
							<video id="videoInput" autoplay="true" class="mobile"></video>
							<div class="controls">
								<button class="btn btn-danger play" title="Tomar foto" onclick="IcarSDK.documentCapture.manualTrigger();"><i class="fas fa-camera fa-3x"></i></button>
							</div>
				  	    </div>
						@endif
			  		<div>
			  			<input id="ChangeCamera" type="button" value="Change Camera"  style="visibility: hidden; padding: 10px 20px" onclick="askForChangeCameraFunction();"  />
			  		</div>
	              </div>

				  <div class="card-body" id="resultado">
					  	<canvas id="resultImage"></canvas>
					  	<hr>
  						<div class="align-items-center justify-content-between">
							<div class="col-xs-12 calculadoras-productos txt-center">
    							  <button id="repetirFoto" type="button" class="general-button" onclick="repetirPasoIne()">
    								  Repetir Foto
    							  </button>
    							  <button id="siguientePaso1" type="button" class="general-button" onclick="saveImagePortal('identificacion_oficial', 'back')">
    								  Siguiente
    							  </button>
    						</div>
  						</div>
						<hr>
				  	</div>
	            </div>
          	</div>
        </div>
	</div>
@endsection
<script>
	window.onload = function () {
		$('#cargando').show();
		iniciarProceso();
	}
</script>
