@extends('layouts.webapp')
@section('content')
<div id="statusSolicitud">

	<div class="row">
		<div id="loginModal" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-dialog-centered modal-login modal-sm ">
			    <div class="modal-content">
			        <div class="modal-header" style="background: #92133e; color: #fff;">
			            <h2 class="titulo-dinamico" style="text-align:center">Termina de cargar tus documentos</h2>
			        </div>
			        <div class="modal-body">
						A fin de agilizar tu trámite, te pedimos <a href="/#loginModal"><b>Inicies sesión</b></a> y cargues tus documentos de manera clara y correcta.
			            <form id="formLogin" style="display:none">
			                <div class="calculadoras-productos">
			                    <i class="fa fa-user"></i>
			                    <input id="email_login" type="text" class="pre-registro-input" placeholder="Correo Electrónico" autocomplete="new-email" name="email" required="required">
			                    <small id="email_login-help" class="help"></small>
			                </div>
			                <div class="calculadoras-productos">
			                    <i class="fa fa-lock"></i>
			                    <input id="password_login" type="password" class="pre-registro-input" placeholder="Contraseña" autocomplete="new-password" name="password" required="required">
			                    <small id="password_login-help" class="help"></small>
			                </div>
			                <div class="form-group" style="text-align:center">
			                    <input id="buttonLogin" type="button" onclick="iniciarSesion()" class="btn swal2-styled" value="Iniciar Sesión">
			                </div>
			            </form>
			            <span id="loginError" class="error" style="border-bottom: none !important; font-size: 13px;"></span>
			        </div>
			        <div class="modal-footer">
			            <a href="/password-restore" id="olvidePassword" style="display:none"><b>Olvide mi contraseña</b></a>
						<a href="/#loginModal"><b>Iniciar Sesión</b></a>
			        </div>
			    </div>
			</div>
		</div>
	</div>

</div>
@endsection
