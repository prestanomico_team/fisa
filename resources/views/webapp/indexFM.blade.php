@extends('layouts.webapp')
@section('content')
	<div class="container">
		PARA CONTINUAR CON TU SOLICITUD...
		Apóyanos a agilizar parte del proceso tomando una fotografía de tu identificación vigente <b>por ambos lados</b> y una selfie
		A continuación se solicitará el acceso a tu cámara, por favor permítelo para poder tomar las fotografías.

		Selecciona el tipo de identificación con el que realizaras el trámite:

		Credencial de Elector

		Licencia de Conducir

		Pasaporte

	</div>
@endsection
