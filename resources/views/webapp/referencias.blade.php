@extends('layouts.webapp')
@section('content')
@php
	use App\C_CUSTOMER_RELATION_TYPE;
	$tipos_relacion = []; //C_CUSTOMER_RELATION_TYPE::pluck('DESCRIPTION', 'ID_CUSTOMER_RELATION_TYPE');
@endphp
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<link rel="stylesheet" href="/css/webapp/bootstrap-grid.min.css">
<div id="solicitud" class="form-solicitud">
	<div class="row">
		<div class="col-12 col-lg-12" id="navigator">
			@if(isset($pasos_habilitados))
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" @if(count($pasos_habilitados) < 5) style="justify-content: center; display: flex;" @endif>
				@foreach ($pasos_habilitados as $paso)
					@if($paso == 'Identificación y Selfie')
						<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@elseif($paso == 'Referencias')
						<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@else
						<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
							<p>{!! $paso !!}</p>
						</a></li>
					@endif
				@endforeach
				</ul>
			@else
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation" class="active"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-10 col-lg-10 offset-lg-1 offset-1">
			<div class="card">
				<div class="card-header">
					<p>Apóyanos con el registro de 3 referencias personales.</br>
					Financiera Monte de Piedad nunca revelará cuánto pides ni la finalidad de tu crédito.</p>
				</div>
				<hr>
				<div class="card-body">
					<form id="formReferencias" class="form-solicitud__content">
						<div class="form-solicitud__row col-12 col-sm-12 col-md-12">
							<fieldset>
								<legend class="referencias">Familiar</legend>
								<div class="row">
									<div class="form-group col-12 col-sm-12 col-md-3">
										<div class="form-group">
											<label>Nombre(s):</label>
											<input type="text" name="nombre_ref1" class="form-control upper" maxlength="50"></input>
											<small id="lerror_nombre_ref1" class="form-text lerror"></small>
										</div>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Paterno:</label>
										<input type="text" name="apellido_paterno_ref1" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_paterno_ref1" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Materno:</label>
										<input type="text" name="apellido_materno_ref1" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_materno_ref1" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Teléfono:</label>
										<input type="text" id="telefono_ref1" name="telefono_ref1" class="form-control" maxlength="10"></input>
										<small id="lerror_telefono_ref1" class="form-text lerror"></small>
									</div>
								</div>
							</fieldset>
							<fieldset>
								<legend class="referencias">Amigo 1</legend>
								<div class="row">
									<div class="form-group col-12 col-sm-12 col-md-3">
										<div class="form-group">
											<label>Nombre(s):</label>
											<input type="text" name="nombre_ref2" class="form-control upper" maxlength="50"></input>
											<small id="lerror_nombre_ref2" class="form-text lerror"></small>
										</div>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Paterno:</label>
										<input type="text" name="apellido_paterno_ref2" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_paterno_ref2" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Materno:</label>
										<input type="text" name="apellido_materno_ref2" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_materno_ref2" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Teléfono:</label>
										<input type="text" id="telefono_ref2" name="telefono_ref2" class="form-control" maxlength="10"></input>
										<small id="lerror_telefono_ref2" class="form-text lerror"></small>
									</div>
								</div>
							</fieldset>
							<fieldset>
								<legend class="referencias">Amigo 2</legend>
								<div class="row">
									<div class="form-group col-12 col-sm-12 col-md-3">
										<div class="form-group">
											<label>Nombre(s):</label>
											<input type="text" name="nombre_ref3" class="form-control upper" maxlength="50"></input>
											<small id="lerror_nombre_ref3" class="form-text lerror"></small>
										</div>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Paterno:</label>
										<input type="text" name="apellido_paterno_ref3" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_paterno_ref3" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Apellido Materno:</label>
										<input type="text" name="apellido_materno_ref3" class="form-control upper" maxlength="50"></input>
										<small id="lerror_apellido_materno_ref3" class="form-text lerror"></small>
									</div>
									<div class="form-group col-12 col-sm-12 col-md-3">
										<label>Teléfono:</label>
										<input type="text" id="telefono_ref3" name="telefono_ref3" class="form-control" maxlength="10"></input>
										<small id="lerror_telefono_ref3" class="form-text lerror"></small>
									</div>
									<!--<div class="form-group col-3">
										<label>Tipo de Relación:</label>
										<select class="form-control">
											@foreach($tipos_relacion as $key => $tipo_relacion)
												<option value="{{ $key }}">{{ $tipo_relacion }}</option>
											@endforeach
										</select>
									</div>-->
								</div>
							</fieldset>
						</div>
						<div class="row" style="text-align:center">
							<div class="form-group col-12 col-sm-12 col-md-12">
						    	<button type="button" class="general-button" onclick="guardarReferencias()">Siguiente</button>
							</div>
					  	</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
