@extends('layouts.webappVue')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12" id="navigator">
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
						<p>Identificación y Selfie</p>
					</a></li>
					<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i></i>
						<p>Referencias</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Cuenta Clabe</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Domicilio</p>
					</a></li>
					<li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
						<p>Comprobante de Ingresos</p>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
          	<div class="col-12 col-lg-12">
				<div class="card">
	              <div class="card-header">
	                <div class="align-items-center">
	                  <div class="col" style="text-align: center">
	                    <h4 class="card-header-title">
							<b>SELFIE (Sonríe)</b>
	                    </h4>
						<span class="span-card">
							<ul style="display:inline-block">
								<li>Asegurate de que la imagen sea lo más clara  y legible posible. <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="ayuda('tip2')"></i></li>
							</ul>
						</span>
	                  </div>
	                </div>
	              </div>
				  <hr>
	              <div class="card-body" id="tomar-foto">
					  <div id="cargando" style="display:none">
					    <center>
					      <img src="/images/ajax-loader.gif" class="loading-gif">
					      <p> Cargando camara... </p>
					    </center>
    				  </div>
					  @if($desktop === true)
				  		<div class="videoDiv desktop" id="videoDiv">
				  	    	<video id="videoInput" autoplay="true" class="desktop"></video>
				  	    </div>
						@else
						<div class="videoDiv mobile" id="videoDiv">
							<video id="videoInput" autoplay="true" class="mobile"></video>
				  	    </div>
						@endif
			  		<div>
			  			<input id="ChangeCamera" type="button" value="Change Camera"  style="visibility: hidden; padding: 10px 20px" onclick="askForChangeCameraFunction();"  />
			  		</div>
	              </div>
				  <div class="card-body" id="resultado">
					  	<canvas id="resultImage"></canvas>
					  	<hr>
  						<div class="row align-items-center justify-content-between">
							<div class="col-xs-12 calculadoras-productos txt-center">
  								<button id="repetirFoto" type="button" class="general-button" onclick="repetirPasoSelfie()">
  									Repetir Foto
  								</button>
  								<button id="terminaFlujoPortal" type="button" class="general-button" onclick="saveSelfiePortal('identificacion_oficial', 'photo')">
  									Terminar
  								</button>
  						  	</div>
  						</div>
						<hr>
				  	</div>
	            </div>
          	</div>
        </div>
	</div>
@endsection
<script>
	window.onload = function () {
		iniciarProcesoSelfie();
	}
</script>
