<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Carbon\Carbon;
use App\Producto;


Auth::routes();

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'ProductoController@obtenerVista')->name('index');
    Route::get('/corporativo', 'ProductoController@obtenerVista');
    Route::get('/ReactivaNL', 'ProductoController@obtenerVista');
    Route::get('/CapitalParaTi', 'ProductoController@obtenerVista');
    Route::get('/capitalparati', 'ProductoController@obtenerVista');
    Route::get('/getForm', 'RegisterController@getForm');
    Route::post('/validaciones', 'RegisterController@validaciones');

    Route::get('/registro', 'ProductoController@obtenerVista');

    Route::get('/aviso-privacidad', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('aviso-privacidad', ['configuracion' => $configuracion]);
    });

    Route::get('/derechos-arco', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('derechos-arco', ['configuracion' => $configuracion]);
    });

    Route::get('/contratos', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('contratos', ['configuracion' => $configuracion]);
    });

    //=========== RESTAURA CONTRASEÑA =============
    Route::get('/password-restore', function () {
        $logo = Cookie::get('logo');
        if (!isset($logo)) {
            $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
            $configuracion['logo'] = $configuracion['logo'];
        } else {
            $configuracion['logo'] = $logo;
        }
        return view('password-restore', ['configuracion' => $configuracion]);
    });
    Route::post('/email-password-restore', 'RestorePasswordController@emailPasswordRestore');
    Route::post('/temporary-password', 'RestorePasswordController@validateTemporaryPassword');
    Route::post('/password-update', 'RestorePasswordController@passwordUpdate');

    Route::get('/login', function () {
        return redirect('auth/login');
    });

    Route::get('/api/geo/{cp}', 'SolicitudController@apiGeoCP');
    Route::get('/api/cp/{cp}', 'SolicitudController@apiGeoCP');
    Route::get('/api/ciudades/{estado}', 'SolicitudController@apiCiudades');

    // RUTAS REGISTRO
    Route::post('/solicitud/registro', 'RegisterController@registroProspecto');
    Route::post('/solicitud/registro/solicitud', 'RegisterController@registroSolicitud');
    Route::post('/solicitud/registro/ldap', 'RegisterController@registroLDAP');
    Route::post('/solicitud/registro/sms', 'RegisterController@envioSMS');

    Route::post('/solicitud/conf_sms_code', 'RegisterController@confSMSCode');
    Route::post('/solicitud/actualizar_contrasena', 'RegisterController@actualizarContraseña');
    Route::post('/solicitud/resend_code', 'RegisterController@resendSMSCode');
    Route::post('/solicitud/reg_domicilio', 'RegisterController@regDomicilio');
    Route::post('/solicitud/datos_personales', 'RegisterController@datosPersonales');
    Route::post('/solicitud/cuentas_de_credito', 'RegisterController@cuentasDeCredito');
    Route::post('/solicitud/correccionBC', 'RegisterController@correccionBC');
    Route::post('/solicitud/datos_adicionales', 'RegisterController@datosAdicionales');
    Route::post('/solicitud/datos_empleo', 'RegisterController@datosEmpleo');

    //LDAP LOGIN ROUTES
    Route::post('/ldap/register/', 'SessionController@Register_LDAP');
    Route::post('/login/prospecto', 'SessionController@loginLDAP');

    //SOLICITUD ROUTES
    Route::post('/solicitud_bc_score', 'BuroCreditoController@primeraLlamada');
    Route::post('/solicitud_hawk', 'BuroCreditoController@segundaLlamada');

    Route::post('/solicitud_bc_score_experian', 'BuroCreditoExperianController@primeraLlamada');
    Route::post('/solicitud_hawk_experian', 'BuroCreditoExperianController@segundaLlamada');
    Route::post('/solicitud_score_no_hit', 'BuroCreditoExperianController@segundaLlamadaScoreNoHit');

    Route::post('/solicitud_fico_alp', 'ALPController@solicitudFicoAlp');
    Route::post('/solicitud_fico_alp_experian', 'ALPExperianController@solicitudFicoAlp');

    //============== PRIVATE ROUTES - REQUIRE AUTHENTICATION =======================
    //admin home
    Route::get('/panel', 'BackOfficeController@panelDashboard')->middleware('auth')->name('panel');
    Route::post('/panel', 'BackOfficeController@panelDashboard')->middleware('auth');
    // Detalle del prospecto
    Route::get('/panel/prospecto/{prospecto_id}', 'BackOfficeV2Controller@detalleProspecto')->middleware('auth');
    Route::get('/panel/prospecto/getDetalleProspecto/{prospecto_id}', 'BackOfficeV2Controller@getDetalleProspecto')->middleware('auth');
    // Detalle de solicitud
    Route::get('/panel/prospecto/solicitud/{solicitud_id}', 'BackOfficeV2Controller@detalleSolicitud')->middleware('auth');
    Route::get('/panel/prospecto/getDetalleSolicitud/{solicitud_id}', 'BackOfficeV2Controller@getDetalleSolicitud')->middleware('auth');
    //prospect detail
    Route::get('/panelva/prospecto/{prospect_id}', 'BackOfficeController@panelProspectDetail')->middleware('auth');

    //download report csv for a single solicitud
    Route::get('/panel/report-csv/{prospect_id}/{solic_id}', 'BackOfficeController@downloadCsvReport')->middleware('auth');
    //download complete report for all solicitudes for a given month of year
    Route::get('/panel/report-completo-csv/', 'BackOfficeController@toViewFullCsvReport')->middleware('auth');
    Route::post('/panel/report-completo-csv/', 'BackOfficeController@postToViewFullCsvReport')->middleware('auth');
    Route::get('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');
    Route::post('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');
    //report csv daily
    Route::post('/panel/report-daily/download', 'BackOfficeController@downloadReportDaily')->name('downloadReport')->middleware('auth');
    Route::get('/panel/report-completo-csv-daily', 'BackOfficeController@viewCsvReportDaily')->middleware('auth');

    //encrypt any non encrypted values (sensetive fields only) in the Prospects Table
    Route::get('/panel/verify-encrypted/prospects', 'BackOfficeController@cleanEncryptProspects')->middleware('auth');
    //encrypt any non encrypted values (sensetive fields only) in the Solicitations Table
    Route::get('/panel/verify-encrypted/solics', 'BackOfficeController@cleanEncryptSolics')->middleware('auth');

    // //test buro report
    // Route::get('/panel/buro-reporte-prueba', ['middleware' => 'auth', function(){
    //     return view('crm.buro.buro_reporte_test');
    // }]);
    //test registro
    Route::get('/panel/registro-prueba', 'BackOfficeController@panelRegistroPrueba')->middleware('auth');
    //test calixta sms sending
    Route::get('/panel/calixta-prueba', 'BackOfficeController@panelCalixtaPrueba')->middleware('auth');

    //show register error log
    Route::get('/panel/registro-logs', 'BackOfficeController@showRegErrorLogs')->middleware('auth');

    //update the codigos postales table
    Route::get('/panel/cp-console', 'BackOfficeController@codigosPostalesConsole')->middleware('auth');
    Route::post('/panel/cp-get-state', 'BackOfficeController@codigosPostalesGetState')->middleware('auth');
    Route::post('/panel/cp-update-verify', 'BackOfficeController@codigosPostalesUpdateVerify')->middleware('auth');

    //============== AUTHENTICATION ROUTES =========================================
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    //=============== DEBUG ROUTES  (COMMENT WHEN NOT IN USE) ============
    // Route::post('/debug/full_report_parse', 'BackOfficeController@debugFullReportParse');
    Route::get('/panel/show-fico-xml/{solic_id}', 'BackOfficeController@showFicoXml')->middleware('auth');

    //=============== ANALYTICS ============
    Route::get('analytics', 'AnalyticsController@getDataAnalytics')->middleware('auth');

    //=============== ALTA CLIENTES T24 ============
    Route::get('alta-clientes', 'T24Controller@altaClientes')->middleware('auth');
    Route::post('carga-layout', 'T24Controller@cargaLayout')->middleware('auth');
    Route::post('alta-cliente-t24', 'T24Controller@altaCliente_T24')->middleware('auth');
    Route::post('alta-solicitud-t24', 'T24Controller@altaSolicitud_T24')->middleware('auth');
    Route::post('ligar-usuario-ldap', 'T24Controller@ligarUsuarioLDAP')->middleware('auth');
    Route::post('enviar-email', 'T24Controller@enviarEmail')->middleware('auth');
    Route::get('search/autocomplete', 'T24Controller@autoComplete');
    Route::get('alta-clientes/update_lista', 'T24Controller@updateLista');

    //============= CODIGOS POSTALES =================
    Route::get('/panel/cobertura', 'BackOfficeController@coberturaCodigos')->middleware('auth');
    Route::get('/panel/buscar_cobertura', 'BackOfficeController@buscarCobertura')->middleware('auth');
    Route::get('/decode-domicilios', 'BackOfficeController@decodeDomicilios')->middleware('auth');

    //============= CUESTIONARIO DINAMICO =================
    Route::get('/cuestionario_dinamico', 'SolicitudApiController@cuestionarioDinamico');
    Route::post('/saveCuestionarioDinamico', 'CuestionarioDinamicoController@save');

    //============= CUESTIONARIO FIJO =================
    Route::post('/saveCuestionarioFijo', 'CuestionarioFijoController@save');

    //============= CALENDARIO CITAS =================
    Route::get('/calendario-citas', 'CuestionarioFijoController@calendarioCitas')->middleware('auth');
    Route::get('/obtener-citas', 'CuestionarioFijoController@obtenerCitas')->middleware('auth');

    //============ OFERTA ============================
    Route::post('/solicitud/status_oferta', 'SolicitudController@statusOferta');
    Route::post('/solicitud/status_oferta_doble', 'SolicitudController@statusOfertaDoble');
    Route::post('/solicitud/rechazar_oferta', 'SolicitudController@rechazarOferta');
    Route::get('/solicitud/datosOferta', 'SolicitudApiController@getDatosOferta');
    Route::post('/solicitud/statusOferta', 'SolicitudApiController@statusOferta');
    Route::post('/solicitud/rechazarOferta', 'SolicitudApiController@rechazarOferta');
    Route::post('/solicitud/statusOfertaDoble', 'SolicitudApiController@statusOfertaDoble');
    Route::post('/solicitud/saveCuestionarioDinamico', 'SolicitudApiController@saveCuestionario');
    Route::get('/retry-job/{id}', function($id) {

        $retryJob = Artisan::call('queue:retry', ['id' => [$id]]);

    });

    //=========== PRODUCTOS =============
    Route::get('/productos', 'ProductoController@index')->name('productos')->middleware('auth');
    Route::get('/productos/nuevo', 'ProductoController@nuevo')->name('productos.nuevo')->middleware('auth');
    Route::post('/productos/save', 'ProductoController@save')->middleware('auth');
    Route::get('/productos/{id}', 'ProductoController@editar')->middleware('auth');
    Route::post('/productos/{id}', 'ProductoController@update')->middleware('auth');
    Route::get('/productos/{id}/cobertura', 'ProductoController@cobertura')->middleware('auth');
    Route::post('/productos/{id}/cobertura', 'ProductoController@updateCobertura')->middleware('auth');
    Route::post('/productos/plazos/agregar', 'ProductoController@savePlazos')->middleware('auth');
    Route::post('/productos/finalidad/agregar', 'ProductoController@saveFinalidad')->middleware('auth');
    Route::post('/productos/ocupacion/agregar', 'ProductoController@saveOcupacion')->middleware('auth');

    Route::get('/landing/{producto}', function ($alias) {

        Cookie::queue(Cookie::forget('checa_calificas'));
        $producto = Producto::where('alias', $alias)->get();

        if (count($producto) == 1) {

            $vista = $producto[0]->vista;
            if ($vista == null && $producto[0]->tipo == 'Nómina') {
                $vista = 'producto.landing.generica_nomina';
            } elseif ($vista == null && $producto[0]->tipo != 'Nómina') {
                $vista = 'producto.landing.generica';
            }

            if ($producto[0]->vigencia_hasta != null) {

                $hoy = Carbon::today();
                $vigencia_hasta = Carbon::createFromFormat('Y-m-d H:i:s', $producto[0]->vigencia_hasta. '00:00:00');
                $vigencia = $hoy->diffInDays($vigencia_hasta, false);

                if ($vigencia < 0) {

                    Cookie::queue(Cookie::forget('producto'));
                    Cookie::queue(Cookie::forget('logo'));
                    return redirect()->route('index');

                } else {

                    Cookie::queue(Cookie::forget('referencia'));
                    $response = new Illuminate\Http\Response(view($vista)->with('producto', $producto[0]));
                    $lifetime = Carbon::now()->addYear()->diffInMinutes();
                    $cookie = Cookie::make('producto', $alias, $lifetime);
                    return $response->cookie($cookie);

                }

            } else {

                Cookie::queue(Cookie::forget('referencia'));
                $response = new Illuminate\Http\Response(view($vista)->with('producto', $producto[0]));
                $lifetime = Carbon::now()->addYear()->diffInMinutes();
                $cookie = Cookie::make('producto', $alias, $lifetime);
                return $response->cookie($cookie);

            }

        } else {
            abort(404);
        }

    });

    Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

    Route::get('/panel/usuarios', 'UsersController@index')->name('usuarios');
    Route::get('/panel/usuarios/perfiles', 'RolesController@index')->name('perfiles');

    Route::post('/panel/usuarios/save/', 'UsersController@save');
    Route::get('/panel/usuarios/{id}', 'UsersController@editar');
    Route::post('/panel/usuarios/{id}', 'UsersController@actualizar');

    Route::post('/panel/usuarios/perfiles/save/', 'RolesController@save');
    Route::get('/panel/usuarios/perfiles/{id}', 'RolesController@editar');
    Route::post('/panel/usuarios/perfiles/{id}', 'RolesController@actualizar');
    Route::post('/solicitudExpress', 'RegisterController@solicitudExpress');
    Route::post('/solicitudPortalRegistro', 'RegistroPortalController@registroProspecto')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortalEditar', 'RegistroPortalController@editarProspecto')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortalSolicitud', 'RegistroPortalController@registroSolicitud')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/registro/sms', 'RegistroPortalController@envioSMS')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/registro/confirmarSMS', 'RegistroPortalController@confirmarSMS')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/registro/domicilio', 'RegistroPortalController@registroDomicilio')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/datos/personales', 'RegistroPortalController@datosPersonales')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/datos/adicionales', 'RegistroPortalController@datosAdicionales')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/datos/empleo', 'RegistroPortalController@datosEmpleo')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/cuentas_de_credito', 'RegistroPortalController@cuentasDeCredito')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/correccionBC', 'RegistroPortalController@correccionBC')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/getProspectos', 'RegistroPortalController@getProspectos')->middleware('auth:captura_convenio','throttle:10,1');
    Route::get('/solicitudPortal/getDetalleSolicitud/{term}', 'RegistroPortalController@getDetalleSolicitud')->middleware('auth:captura_convenio');
    Route::post('/solicitudPortal/tracking', 'RegistroPortalController@getTrackingSolicitud')->middleware('auth:captura_convenio');
    //=============== CONVENIOS ===============
    Route::get('/captura_solicitud/{producto}', function($producto) {
        if (Auth::guard('captura_convenio')->check()) {
            return redirect('/panel/producto/captura/'.$producto);
        } else {
            return view('auth.login_captura', ['producto' => $producto]);
        }

    });
    Route::post('/captura_solicitud/{producto}', 'Auth\AuthConvenioController@login');
    Route::get('/logout/captura/{producto}', 'Auth\LoginController@logoutCaptura');
    Route::get('/panel/producto/captura/{producto}', function($alias) {

        $producto = Producto::where('alias', $alias)->get();

        if (count($producto) == 1) {

            $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion', ['id_producto' => $producto[0]->id]);
            if ($producto[0]->vigencia_hasta != null) {

                $hoy = Carbon::today();
                $vigencia_hasta = Carbon::createFromFormat('Y-m-d H:i:s', $producto[0]->vigencia_hasta. '00:00:00');
                $vigencia = $hoy->diffInDays($vigencia_hasta, false);

                if ($vigencia < 0) {

                    return redirect()->route('index');

                } else {

                    return view('producto.landing.'.$alias.'-captura', ['configuracion' => $configuracion]);

                }

            } else {

                return view('producto.landing.'.$alias.'-captura', ['configuracion' => $configuracion]);

            }

        } else {
            abort(404);
        }

    })->middleware('auth:captura_convenio');

    Route::get('/descargaDatosBuro', 'BackOfficeController@descargaDatosBuro')->middleware('auth');

    //=============== CLIENTE ESTRELLA ===============
    Route::get('/panel/clienteEstrella', 'ClienteEstrellaController@index')->name('clienteEstrella')->middleware('auth');
    Route::post('/panel/uploadClienteEstrella', 'ClienteEstrellaController@uploadClienteEstrella');
    Route::get('/panel/listaClienteEstrella', 'ClienteEstrellaController@listaClienteEstrella');

    //=============== REPORTE DE EFICIENCIA ===============
    Route::get('/datosReporteEficiencia',  'ReporteDiarioController@datosReporteEficiencia')->middleware('auth');
    Route::get('/descargaReporte/{file}',  'ReporteDiarioController@descargaReporte')->middleware('auth');
    Route::get('/solicitudesDiarias',  'ReporteSolicitudesDiariasController@getSolicitudesDiarias')->middleware('auth');
    Route::get('/consultasBCDiarias',  'ReporteSolicitudesDiariasController@getConsultasBuroDiarias')->middleware('auth');


    //=============== WEB APP ==============
    Route::get('/webapp', 'WebAppController@index');
    Route::get('/webapp/facematch/id_front',  'FaceMatchController@capturaIdentificacionFront')->middleware('auth:prospecto');
    Route::get('/webapp/facematch/id_back',  'FaceMatchController@capturaIdentificacionBack')->middleware('auth:prospecto');
    Route::get('/webapp/facematch/selfie',  'FaceMatchController@capturaSelfie')->middleware('auth:prospecto');
    Route::post('/webapp/facematch/procesaFaceMatch', 'FaceMatchController@procesaFaceMatch')->middleware('auth');
    Route::post('/webapp/subirDocumento',  'FaceMatchController@subirDocumento')->middleware('auth:prospecto');
    Route::get('/webapp/subirDocumentoRepositorio',  'FaceMatchController@subirDocumentoRepositorio');

    Route::get('/webapp/referencias', 'ReferenciasController@index')->middleware('auth:prospecto');
    Route::post('/webapp/referencias/save', 'ReferenciasController@save')->middleware('auth:prospecto');
    Route::post('/webapp/referencias/procesar', 'ReferenciasController@procesar')->middleware('auth');

    Route::get('/webapp/cuenta_clabe', 'CuentaClabeController@index')->middleware('auth:prospecto');
    Route::post('/webapp/cuenta_clabe/save', 'CuentaClabeController@save')->middleware('auth:prospecto');
    Route::post('/webapp/cuenta_clabe/procesar', 'CuentaClabeController@procesar')->middleware('auth');

    Route::get('/webapp/comprobante_domicilio', 'ComprobanteDomicilioController@index')->middleware('auth:prospecto');
    Route::post('/webapp/comprobante_domicilio/save', 'ComprobanteDomicilioController@save')->middleware('auth:prospecto');
    Route::post('/webapp/comprobante_domicilio/procesar', 'ComprobanteDomicilioController@procesar')->middleware('auth');

    Route::get('/webapp/comprobante_ingresos', 'ComprobanteIngresosController@index')->middleware('auth:prospecto');
    Route::post('/webapp/comprobante_ingresos/save', 'ComprobanteIngresosController@save')->middleware('auth:prospecto');
    Route::post('/webapp/comprobante_ingresos/procesar', 'ComprobanteIngresosController@procesar')->middleware('auth');

    Route::get('/webapp/certificados_deuda', 'CertificadosDeudaController@index')->middleware('auth:prospecto');
    Route::post('/webapp/certificados_deuda/save', 'CertificadosDeudaController@save')->middleware('auth:prospecto');
    Route::post('/webapp/certificados_deuda/procesar', 'CertificadosDeudaController@procesar')->middleware('auth');

    //=============== WEB APP PORTALES===============
    Route::get('/webapp/portal/facematch/id_front/{id}', 'FaceMatchPortalController@capturaIdentificacionFront')->middleware('auth:captura_convenio');
    Route::get('/webapp/portal/facematch/id_back/{id}',  'FaceMatchPortalController@capturaIdentificacionBack')->middleware('auth:captura_convenio');
    Route::get('/webapp/portal/facematch/selfie/{id}',  'FaceMatchPortalController@capturaSelfie')->middleware('auth:captura_convenio');
    Route::post('/webapp/portal/subirDocumento',  'FaceMatchPortalController@subirDocumento')->middleware('auth:captura_convenio');
    //Route::get('/webapp/subirDocumentoRepositorio',  'FaceMatchController@subirDocumentoRepositorio');
    //Route::post('/webapp/procesaFaceMatch', 'FaceMatchController@procesaFaceMatch')->middleware('auth');
    //=============== RENOVACIONES ==============
    Route::get('/renovaciones', 'ProductoController@renovaciones')->name('index-renovaciones');
    Route::post('/renovaciones/buscarOferta', 'RenovacionesController@obtenerOferta');
    Route::post('/renovaciones/buscarRFC', 'RenovacionesController@buscarRFC');
    Route::post('/renovaciones/cargaLayout', 'RenovacionesController@cargaLayout');

    Route::get('/prospecto/logout', 'Auth\AuthProspectoController@logout');
    Route::get('/token', function () {

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        return $token;

    })->middleware('auth');

    Route::get('/token/prospecto', function () {

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        return $token;

    })->middleware('auth:prospecto');

    Route::get('/token/convenio', function () {

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        return $token;

    })->middleware('auth:captura_convenio');

    Route::group(['prefix'=> 'api', 'middleware' => 'jwt.verify'], function () {
        Route::post('/conf_sms', 'Api\ConfirmarSMSController@confSMSCode');
        Route::post('/conf_sms_portal', 'Api\ConfirmarSMSController@confSMSPortal');
        //Route::post('/solicitudPortal/getProspectos', 'Api\ConfirmarSMSController@getProspectos')->middleware('auth:captura_convenio','throttle:3,1'); 
    });


    Route::get('/consolida_deudas', 'ProductoController@sindicalizados')->name('index-sindicalizados');
    Route::post('/sindicalizados/registro', 'SindicalizadosController@registroProspectoSindicalizado');



});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
